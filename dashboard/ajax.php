<?php

isset($_COOKIE['ci_session']) OR exit('Access denied!');
if(!isset($_GET['q']) && !isset($_GET['u']) )
	exit('Invalid request!');

define('ENVIRONMENT', 'development');
define('FCPATH', dirname(__FILE__).'/');
if (file_exists(realpath(__DIR__).DIRECTORY_SEPARATOR.'app-config.php'))
{
	if (!defined('CONFIGPATH')){
		define('CONFIGPATH',  realpath(__DIR__).DIRECTORY_SEPARATOR.'app-config.php');
		require_once CONFIGPATH;
	}
} elseif (file_exists(realpath(__DIR__ . '/..').DIRECTORY_SEPARATOR.'app-config.php'))
{
	if (!defined('CONFIGPATH')){
		define('CONFIGPATH',  realpath(__DIR__ . '/..').DIRECTORY_SEPARATOR.'app-config.php');
	require_once CONFIGPATH;
	}
} else
{
	die('No app-config file found!');	
}

/* Database connection start */
$servername = DB_HOST;
$username = DB_USER;
$password = DB_PASSWORD;
$dbname = DB_NAME;

$conn = mysqli_connect($servername, $username, $password,DB_NAME) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */

$sql ="SELECT data from ci_sessions where id = '".$_COOKIE['ci_session']."'";
$query=mysqli_query($conn, $sql) or die("ajax.php");
$row=mysqli_fetch_array($query);
//var_dump($row);
if (!$row)
    die("Access denied!");
if (strpos(implode($row), 'logged_in') === false) {
    die("Access denied!");
}

// ON THIS POINT, YOU ARE A VALID USER!
// getting total number records without any search
if (isset($_GET['u']))
{
	$user = $_GET['u'];
	$sql = "CALL update_user_info(".$user.")";
	$sql = "CALL update_user_totals_fm(".$user.")";
	$query=mysqli_query($conn, $sql) or die("ajax.php");
	//die("User:$user updated!");\
	return;
}

$search = $_GET['q'];
$sql = "SELECT user_id, display_name  from sfm_uap_affiliates where 
user_id like '%".$search."%' or 
display_name like '%".$search."%' or 
email like '%".$search."%' 
limit 10";
$query=mysqli_query($conn, $sql) or die("ajax.php");
//$totalData = mysqli_num_rows($query);
//$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
	$data[] =array(
		'user_id' => $row["user_id"],
		'display_name' => $row["display_name"]);
}
echo json_encode($data);  // send data as json format
die;
