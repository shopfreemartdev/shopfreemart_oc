<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Config.php";
class SB_Config extends MX_Config {
	public $site_theme;
	public $theme_path;
	public function setSiteTheme($theme = 0)
	{
		
		// $this->site_theme =  'themes/shop';
		// ld($this->site_theme);
		//  get_instance()->session->set_userdata('site_theme',$this->site_theme);
		//  return; 
   

		$this->theme_path='';
		if ($theme)
		{
			$this->site_theme =$theme;
			get_instance()->session->set_userdata('site_theme',$this->site_theme);
			return ;
		}

		//return;
		
		if (isset($this->config['theme_path']))
			$this->theme_path=$this->config['theme_path'];
		else
			$this->theme_path='theme/';
			

		if (isset($_GET['st']))
		{
			$this->site_theme = $_GET['st'];
			get_instance()->session->set_userdata('site_theme',$this->site_theme);
			return ;
		}

		$is_admin = is_staff_logged_in(); // get_instance()->Users_model->is_admin;

		if ($is_admin)
		{
			$this->site_theme=$this->config['backend_theme'];
			get_instance()->session->set_userdata('site_theme',$this->site_theme);			
		}
		else
		{
			$this->site_theme=$this->config['site_theme'];

			if (isset(get_instance()->session))
			{
				if (get_instance()->session->userdata('site_theme'))
				{
					$this->site_theme = get_instance()->session->userdata('site_theme');
		
					return;
				}
			}

		}




		
	}
		
}