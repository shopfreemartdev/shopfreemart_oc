<?php //if (!defined('BASEPATH')) exit('No direct script access allowed');

defined('BASEPATH') or exit('No direct script access allowed');

class SB_Controller extends MX_Controller
{

     // Values to be obtained automatically from router
     protected $mModule = ''; // module name (empty = Frontend Website)
     protected $mCtrler = 'home'; // current controller
     protected $mAction = 'index'; // controller function being called
     protected $mMethod = 'GET'; // HTTP request method

     // Config values from config/ci_bootstrap.php
     protected $mConfig      = array();
     protected $mBaseUrl     = array(); 
     protected $mSiteName    = ''; 
     protected $mMetaData    = array();
     protected $mScripts     = array();
     protected $mStylesheets = array();

     // Values and objects to be overrided or accessible from child controllers
     protected $mPageTitlePrefix = '';
     protected $mPageTitle       = '';
     protected $mBodyClass       = '';
     protected $mMenu            = array();
     protected $mBreadcrumb      = array();

     // Multilingual
     protected $mMultilingual       = false;
     protected $mLanguage           = 'en';
     protected $mAvailableLanguages = array();

     // Data to pass into views
     public $mViewData = array();

     // Login user
     protected $mPageAuth   = array();
     protected $mUser       = null;
     protected $mUserGroups = array();
     protected $mUserMainGroup;

     public $info = array();
     public $login_user;

     public $data                      = array();
     public $access                    = array();
     public $layout_design_header_name = "Default";
     public $layout_design_site_theme  = "default";

     //arts_crafts,beach_one,blue_tour,bright_colorful,clean_corporate,clean_tour,cyborg,dark_corporate_tour,dark_tour,default,light_airy,light_blue,partners_green,slate

     public $user_id;
     public $affiliate_id;
     private $gridsettings = array();

     public function __construct()
     {

          global $current_user;
          $this->data = array(
               'pageTitle'      => '',
               'pageNote'       => '',
               'pageModule'     => '',
               'pageUrl'        => '',
               'module_filters' => array(),
               'access'         => array(),
          );
          parent::__construct();

          // For ci_bootstrap initial setup

          $this->load->model('Users_model');
          $this->load->model('Authentication_model');

          $this->load->helper('ajax_helper');

          $this->load->library('session');
          $this->load->library('user_agent');

          $this->load->model('Announcements_model');
          $this->load->model('Settings_model');
          $this->load->model('Messages_model');
          $this->load->model('Notifications_model');

          $enc_session = $this->session->userdata('session_id') . md5($this->config->item('encryption_key'));
          $this->config->set_item('enc_id', $enc_session);

// Settings
          $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0' OR store_id = '" . (int) $this->config->item('config_store_id') . "' ORDER BY store_id ASC");

          foreach ($query->result_array() as $result) {
               if (!$result['serialized']) {
                    $this->config->set_item($result['key'], $result['value']);
               } else {
                    $this->config->set_item($result['key'], json_decode($result['value'], true));
               }
          }

          //$this->_setup();
          return;

     }

     public function get_memberinfo($user_id = '')
     {

          //member's dashboard
          if ($user_id == '') {
               $user_id = ci_get_current_user_id();
          }

          if (!$user_id) {
               return array();
          }

          $member_data[] = array();

          $member_data['client_info'] = array();
          $member_data['client_id']   = 1;

          $member_data["show_invoice_statistics"] = true;
          $member_data["show_ticket_status"]      = true;
          $member_data["show_income_vs_expenses"] = true;
          $member_data["show_clock_status"]       = true;
          $member_data["show_timeline"]           = true;
          $member_data["show_attendance"]         = true;
          $member_data["show_event"]              = true;
          $member_data['show_invoice_info']       = true;
          $member_data["show_income_vs_expenses"] = true;
          $member_data["show_invoice_statistics"] = true;
          $member_data["show_ticket_status"]      = true;
          $member_data["show_clock_status"]       = true;
          $member_data['page_type']               = "dashboard";
          $member_data["custom_field_headers"]    = array();

          //check module availability and access permission to show any widget
          //$member_data['login_user'] = $this->login_user;
          $row = $this->Users_model->get_accountdetails($user_id, true);

          if ($this->info) {

               $member_data['subgrid'] = (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
               $member_data['fields']  = AjaxHelpers::fieldLang($this->info['config']['grid']);
               $member_data['id']      = $user_id;
               $member_data['setting'] = $this->info['setting'];
          }

          //load the variables

          $member_data['dashboard_name'] = $row['display_name'];
          $member_data['username']       = $row['username'];
          $member_data['register_date']  = $row['date_registered'];
          $member_data['current_rank']   = lang($row['rank']);
          $member_data['phone']          = $row['phone'];
          $member_data['image']          = $row['avatar'];

          if (!empty($row['next_rank'])) {
               $member_data['next_rank']       = lang($row['next_rank']);
               $member_data['next_rank_sales'] = $this->Users_model->get_next_rank_sales();
          } else {
               $member_data['next_rank']       = '--';
               $member_data['next_rank_sales'] = '--';
          }

          $member_data['current_rewards'] = array(

               array(

                    'id'          => '0',

                    'description' => 'Freedom Gram monthly Bonus <br> (for the month of march)',

                    'amount'      => '$100.00',

               ),

               array(

                    'id'          => '1',

                    'description' => 'Referral Commission Bonus <br> (by uuser3)',

                    'amount'      => '$50.00',

               ),

               array(

                    'id'          => '2',

                    'description' => 'Unilivel Bonus <br> (Level 1 by user1)',

                    'amount'      => '$80.00',

               ),

          );

          $member_data['total_orders']    = $row['orders'];
          $member_data['unpaid_invoices'] = to_currency($row['unpaid_invoices']);
          $member_data['paid_invoices']   = to_currency($row['paid_invoices']);

          $member_data['total_commissions']     = $row['commissions_overall'];
          $member_data['pending_commissions']   = $row['pending_commissions'];
          $member_data['available_commissions'] = $row['commissions'];
          $member_data['commissions_debits']    = $row['commissions_debits'];

          $member_data['total_coins']     = $row['coins_overall'];
          $member_data['pending_coins']   = $row['pending_coins'];
          $member_data['available_coins'] = $row['coins'];
          $member_data['coins_debits']    = $row['coins_debits'];

          $member_data['total_downlines']  = $row['downlines'];
          $member_data['paid_downlines']   = $row['referralsales'];
          $member_data['unpaid_referrals'] = $row['downlines'] - $row['referralsales'];

          $member_data['open_tickets']   = $row['open_tickets'];
          $member_data['closed_tickets'] = $row['closed_tickets'];

          $member_data['referral_link'] = $row['replicated_url'];

          $member_data['sponsorkey']              = $row['sponsorkey'];
          $member_data['upline_diamond_id']       = $row['upline_diamond_id'];
          $member_data['upline_doublediamond_id'] = $row['upline_doublediamond_id'];
          $member_data['upline_triplediamond_id'] = $row['upline_triplediamond_id'];
          $member_data['upline_ambassador_id']    = $row['upline_ambassador_id'];

          $member_data['upline_diamond']       = $row['upline_diamond'];
          $member_data['upline_doublediamond'] = $row['upline_doublediamond'];
          $member_data['upline_triplediamond'] = $row['upline_triplediamond'];
          $member_data['upline_ambassador']    = $row['upline_ambassador'];
          $member_data['email']                = $row['email'];

          //ld($row['email']);

          $member_data['rankprogress_min']   = 0;
          $member_data['rankprogress_max']   = 3;
          $member_data['rankprogress_value'] = 1;
          $width                             = ($member_data['rankprogress_value'] / $member_data['rankprogress_max']) * 100;
          $member_data['rankprogress_width'] = $width;
          return $member_data;
     }

     public function loadmodels()
     {

          $this->load->helper('app_files');
          //$this->load->model('member/Crud_model');

          //$this->load->model('member/Users_model');

          $this->load->model('member/Tickets_model');

          $this->load->model('member/Custom_Fields_model');

          $this->load->model('member/Settings_model');

          $this->load->model('member/Team_model');

          $this->load->model('member/Attendance_model');

          $this->load->model('member/Leave_types_model');

          $this->load->model('member/Leave_applications_model');

          $this->load->model('member/Events_model');

          $this->load->model('member/Clients_model');

          $this->load->model('member/Projects_model');

          $this->load->model('member/Tasks_model');

          $this->load->model('member/Project_comments_model');

          $this->load->model('member/Activity_logs_model');

          $this->load->model('member/Project_files_model');

          $this->load->model('member/Notes_model');

          $this->load->model('member/Project_members_model');

          $this->load->model('member/Ticket_types_model');

          $this->load->model('member/Tickets_model');

          $this->load->model('member/Ticket_comments_model');

          $this->load->model('member/Items_model');

          $this->load->model('member/Invoices_model');

          $this->load->model('member/Invoice_items_model');

          $this->load->model('member/Invoice_payments_model');

          $this->load->model('member/Payment_methods_model');

          $this->load->model('member/Posts_model');

          $this->load->model('member/Expenses_model');

          $this->load->model('member/Expense_categories_model');

          $this->load->model('member/Taxes_model');

          $this->load->model('member/Social_links_model');

          $this->load->model('member/Estimate_forms_model');

          $this->load->model('member/Estimate_requests_model');

          $this->load->model('member/Estimates_model');

          $this->load->model('member/Estimate_items_model');

          $this->load->model('member/Notification_settings_model');

          $this->load->model('member/Notifications_model');

          $this->load->model('member/Custom_fields_model');

          $this->load->model('member/Custom_field_values_model');

          $this->load->model('member/Timesheets_model');

          $this->load->model('member/Email_templates_model');

          $this->load->model('member/Roles_model');

          $this->load->model('member/Milestones_model');

          $this->load->model('member/Messages_model');

     }

     public function load_viewdata($value = '')
     {

          $arrayresult = array_replace_recursive(array(
               'custom_field_headers'                     => '',
               'total_current_page'                       => 0,
               'total_all_pages'                          => 0,
               'totaldebit_current_page'                  => 0,
               'totaldebit_all_pages'                     => 0,
               'totalcredit_current_page'                 => 0,
               'totalcredit_all_pages'                    => 0,
               'pageTitle'                                => '',
               'activemenu'                               => '',

               'logo'                                     => '',

               'top_menu'                                 => '',

               'account_menu'                             => '1',

               'footer_menu'                              => '',

               'cart_items'                               => 'Cart',

               'enable_links'                             => '1',

               'enable_affiliate_marketing'               => '1',

               'dashboard_name'                           => $this->session->userdata('fid'),

               'sts_affiliate_enable_profile_description' => '1',

               'sts_sec_enable_captcha_order_form'        => '1',

               'captcha_field'                            => '',

               'captcha'                                  => '',

               'lang_captcha_text'                        => '',

               'general_affiliate_link'                   => WP_URL . '/' . $this->session->userdata('username'),

               'mailing_lists'                            => '1',

               'member_hide_list_box'                     => '1',

               'signup_affiliate_link'                    => WP_URL . '/' . $this->session->userdata('username'),

               'show_articles'                            => '1',

               'articles'                                 => array(),

               'dashboard_unpaid_invoices'                => '$0.00',

               'dashboard_paid_invoices'                  => '$0.00',

               'dashboard_unpaid_commissions'             => '$0.00',

               'dashboard_paid_commissions'               => '$0.00',

               'dashboard_available_commissions'          => '$0.00',

               'dashboard_unpaid_coins'                   => '$0.00',

               'dashboard_paid_coins'                     => '$0.00',

               'dashboard_available_coins'                => '$0.00',

               'dashboard_unpaid_referrals'               => '0',

               'dashboard_paid_referrals'                 => '0',

               'dashboard_total_downlines'                => '0',

               'sts_support_enable'                       => '1',

               'dashboard_open_tickets'                   => '1',

               'dashboard_closed_tickets'                 => '1',

               'show_message'                             => '',

               'show_upload'                              => '0',

               'pagination_rows'                          => '',

               'pagehead'                                 => '',

               'filterbar'                                => '',

               'customcontent'                            => '',

               'newbuttontext'                            => 'Create',

               'user_strict_location'                     => '0',

               'filter_location'                          => '',

               'filter_locations'                         => array(

                    array(

                         'location_id'   => 1,

                         'location_name' => 1,

                    )),

               'filter_status'                            => '',

               'filter_statuses'                          => array(

                    array(

                         'status_id'   => 1,

                         'status_name' => 1,

                    )),

               'filter_paymentmethod'                     => '',

               'filter_paymentmethods'                    => array(

                    array(

                         'name'  => 1,

                         'title' => 1,

                    )),

               'filter_type'                              => array(),

               'filter_order_dates'                       => array(),

               'filter_date'                              => '',

               'filter_search'                            => '',

               'filter_from'                              => '',

               'filter_to'                                => '',

               'show_response'                            => '',

               'support_priority'                         => '',

               'support_categories'                       => '',

               'breadcrumbs'                              => '',

               ''                                         => '1',

               'ticket_id'                                => '1',

               's_ticket_status'                          => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

               ''                                         => '1',

          ), $this->data);

          $member_view_data = $this->get_memberinfo();

          $this->data = array_replace_recursive($member_view_data, $arrayresult);

          //          if(!is_null($this->input->get('search', true)))

          //          {

          //               $type = explode(" | ",$this->input->get('search', true));

          //               if(count($type) >= 1)

          //               {

          //                    foreach($type as $t)

          //                    {

          //                         $keys = explode(":",$t);

          //                         if ($keys[0] == 'search')

          //                         {

          //                              $this->data['filter_search'] = $keys[1];

          //                         }

          //                    }

          //               }

          //

          //          }

          if (!$this->data['breadcrumbs']) {

               $pagetitle = "";

               if (isset($this->data['pageTitle'])) {
                    $pagetitle = $this->data['pageTitle'];
               }

               $this->data['breadcrumbs'] = get_bread_crumbs($pagetitle);

          }

     }

     public function render_oc($view)
     {

          $this->data['content'] = $view;
          $this->load->view("themes/" . $this->config->site_theme . "/main", $this->data);

     }

     public function render_html($viewpath, $viewdata = 0)
     {

          if (isset($this->access)) {

               $this->data['access'] = $this->access;

          }

          if ($viewdata) {

               $this->data = array_replace_recursive($this->data, $viewdata);

          }

          $this->load_viewdata();

          $themepath = VIEWPATH . 'themes/' . $this->config->site_theme . '/';

          $this->data['content'] = $this->load->view($viewpath, $this->data, true);

          $this->load->view("themes/" . $this->config->site_theme . "/blank", $this->data);

     }

     //initialize the login user's permissions with readable format

     protected function init_permission_checker($module)
     {

          $info = $this->get_access_info($module);

          $this->access_type = $info->access_type;

          $this->allowed_members = $info->allowed_members;

     }

     //prepear the login user's permissions

     protected function get_access_info($group)
     {

          $info = new stdClass();

          $info->allowed_members = array();

          $info->access_type = "all";

          // //admin users has access to everything

          // if ($this->Users_model->login_user()->is_admin) {

          //     $info->access_type = "all";

          // } else {

          //     //not an admin user? check module wise access permissions

          //     $module_permission = get_array_value($this->Users_model->login_user()->permissions, $group);

          //     if ($module_permission === "all") {

          //         //this user's has permission to access / manage everything of this module (same as admin)

          //         $info->access_type = "all";

          //     } else if ($module_permission === "specific") {

          //         //this user's has permission to access / manage sepcific items of this module

          //         $info->access_type = "specific";

          //         $module_permission = get_array_value($this->Users_model->login_user()->permissions, $group . "_specific");

          //         $permissions = explode(",", $module_permission);

          //         //check the accessable users list

          //         if ($group === "leave" || $group === "attendance" || $group === "team_member_update_permission" || $group === "timesheet_manage_permission") {

          //             $info->allowed_members = array($this->Users_model->login_user()->id);

          //             $allowed_teams = array();

          //             foreach ($permissions as $vlaue) {

          //                 $permission_on = explode(":", $vlaue);

          //                 $type = get_array_value($permission_on, "0");

          //                 $type_value = get_array_value($permission_on, "1");

          //                 if ($type === "member") {

          //                     array_push($info->allowed_members, $type_value);

          //                 } else if ($type === "team") {

          //                     array_push($allowed_teams, $type_value);

          //                 }

          //             }

          //             if (count($allowed_teams)) {

          //                 $team = $this->Team_model->get_members($allowed_teams)->result();

          //                 foreach ($team as $value) {

          //                     $info->allowed_members += explode(",", $value->members);

          //                 }

          //             }

          //         }

          //     }

          // }

          return $info;

     }

     //only allowed to access for team members

     protected function access_only_team_members()
     {

          return;

          if ($this->Users_model->login_user()->user_type !== "staff") {

               redirect("forbidden");

          }

     }

     //only allowed to access for admin users

     protected function access_only_admin()
     {

          return;

          if (!$this->Users_model->login_user()->is_admin) {

               redirect("forbidden");

          }

     }

     //access only allowed team members

     protected function access_only_allowed_members()
     {

          return;

          if ($this->access_type !== "all") {

               redirect("forbidden");

          }

     }

     //access only allowed team members

     protected function access_only_allowed_members_or_client_contact($client_id)
     {

          return;

          if (!($this->access_type === "all" || $this->Users_model->login_user()->client_id === $client_id)) {

               redirect("forbidden");

          }

     }

     //allowed team members and clint himself can access

     protected function access_only_allowed_members_or_contact_personally($user_id)
     {

          return;

          if (!($this->access_type === "all" || $user_id === $this->Users_model->login_user()->id)) {

               redirect("forbidden");

          }

     }

     //access all team members and client contact

     protected function access_only_team_members_or_client_contact($client_id)
     {

          return;

          if (!($this->Users_model->login_user()->user_type === "staff" || $this->Users_model->login_user()->client_id === $client_id)) {

               redirect("forbidden");

          }

     }

     //only allowed to access for admin users

     protected function access_only_clients()
     {

          return;

          if ($this->Users_model->login_user()->user_type != "client") {

               redirect("forbidden");

          }

     }

     //check module is enabled or not

     protected function check_module_availability($module_name)
     {

          return;

          if (get_setting($module_name) != "1") {

               redirect("forbidden");

          }

     }

     //check who has permission to create projects

     protected function can_create_projects()
     {

          return true;

          if ($this->Users_model->login_user()->user_type == "staff") {

               if ($this->Users_model->login_user()->is_admin) {

                    return true;

               } else
               if (get_array_value($this->Users_model->login_user()->permissions, "can_create_projects") == "1") {

                    return true;

               }

          } else {

               if (get_setting("client_can_create_projects")) {

                    return true;

               }

          }

     }

     public function render_view($viewpath, $viewdata = array())
     {

          if ($this->config->item('enable_profiling')) {
               $this->output->enable_profiler(true);
               $sections = array(
                    'config'  => true,
                    'queries' => true,
               );
               $this->output->set_profiler_sections($sections);
          }

          //if (isset($this->info['id']))
          //{

          //}

          if (isset($this->access)) {
               $this->data['access'] = $this->access;
          }

          if ($viewdata) {

               $this->data = array_replace_recursive($this->data, $viewdata);
          }

          //$this->config->setSiteTheme();
          $themepath = VIEWPATH . 'themes/' . $this->config->site_theme . '/';

          if (is_client_logged_in()) {
               $this->load_viewdata();
               $this->data['breadcrumbs'] = get_bread_crumbs();

               if (empty($viewdata['pageTitle'])) {

                    if (isset($this->info['title'])) {
                         $viewdata['pageTitle'] = $this->info['title'];
                    }
               }
               if (isset($this->info['note'])) {
                    $viewdata['pageNote'] = $this->info['note'];
               }

               $member_view_data = $this->get_memberinfo();

               $viewdata = array_replace_recursive($member_view_data, $viewdata);

          }

          //        array_push($this->data,array('module_filters' => get_module_filters( $this->info['id'])));

          $this->data['content'] = $this->load->view($viewpath, $this->data, true);
          $viewdata              = array_replace_recursive($this->data, $viewdata);

          //$viewdata['access'] = $this->access;

          //$this->template->render_view($viewpath,$viewdata);

          $this->load->view("themes/" . $this->config->site_theme . "/main", $this->data);
          // $this->load->view("layout/main", $this->data);

     }

     public function render_view2($viewpath, $viewdata = array())
     {
          if (isset($this->access)) {
               $this->data['access'] = $this->access;
          }

          if ($viewdata) {

               $this->data = array_replace_recursive($this->data, $viewdata);
          }
          $this->load->view("themes/adminlte/main", $this->data);
     }

     public function render_content($viewpath)
     {

          $this->load_viewdata();

          $this->load->view($viewpath, $this->data);

     }

     public function get_wp_logged_user()
     {

          if (!defined('COOKIEHASH')) {

               define('COOKIEHASH', md5(WP_URL));

          }

          $cookiename = "wordpress_logged_in_" . COOKIEHASH;
          ld($cookiename);
          if (isset($_COOKIE[$cookiename])) {

               $items = explode('|', $_COOKIE[$cookiename]);

               if ($items[0]) {

                    return $items[0];

               }

          }

          return false;

     }

     public function search($value = '')
     {

          $keyword = $this->module;

          if (!is_null($this->input->get('keyword', true))) {

               $keyword = $this->module . '/?search=' . str_replace(' ', '_', $this->input->get('keyword', true));

          }
ld($keyword);
          return redirect($keyword);

     }

     public function download()
     {

          $info = $this->model->makeInfo($this->module);

          // Take param master detail if any

          $results = $this->model->getRows(array());

          $fields = $info['config']['grid'];

          $rows = $results['rows'];

          $content = $this->data['pageTitle'];

          $content .= '<table border="1">';

          $content .= '<tr>';

          foreach ($fields as $f) {

               if ($f['download'] == '1') {
                    $content .= '<th style="background:#f9f9f9;">' . $f['label'] . '</th>';
               }

          }

          $content .= '</tr>';

          foreach ($rows as $row) {

               $content .= '<tr>';

               foreach ($fields as $f) {

                    if ($f['download'] == '1'):

                         $conn = (isset($f['conn']) ? $f['conn'] : array());

                         $content .= '<td>' . SiteHelpers::gridDisplay($row->$f['field'], $f['field'], $conn) . '</td>';

                    endif;

               }

               $content .= '</tr>';

          }

          $content .= '</table>';

          @header('Content-Type: application/ms-excel');

          @header('Content-Length: ' . strlen($content));

          @header('Content-disposition: inline; filename="' . $title . ' ' . date("d/m/Y") . '.xls"');

          echo $content;

          exit;

     }

     public function multisearch()
     {

          //echo ' < pre > ';print_r($_POST);echo '</pre > ';exit;

          $post = $_POST;

          $items = '';

          foreach ($post as $item => $val):

               if ($_POST[$item] != '' and $item != '_token' and $item != 'md' && $item != 'id'):

                    $items .= $item . ': ' . trim($val) . '|';

               endif;

          endforeach;

          $surl = $this->module . '/index?search=' . substr($items, 0, strlen($items) - 1);

          redirect($surl);

     }

     public function filter()
     {

          $module = $this->module;

          $sort = (!is_null($this->input->get('sort', true)) ? $this->input->get('sort', true) : '');

          $order = (!is_null($this->input->get('order', true)) ? $this->input->get('order', true) : '');

          $rows = (!is_null($this->input->get('rows', true)) ? $this->input->get('rows', true) : '');

          $md = (!is_null($this->input->get('md', true)) ? $this->input->get('md', true) : '');

          $filter = '?';

          if ($sort != '') {
               $filter .= '&sort=' . $sort;
          }

          if ($order != '') {
               $filter .= '&order=' . $order;
          }

          if ($rows != '') {
               $filter .= '&rows=' . $rows;
          }

          if ($md != '') {
               $filter .= '&md=' . $md;
          }

          return redirect($module . $filter);

     }

     public function infoFieldSearch()
     {

          $info = $this->model->makeInfo($this->module);

          $forms = $info['config']['forms'];

          $data = array();

          foreach ($forms as $f) {

               if ($f['search'] == 1) {
                    if ($f['alias'] != '') {

                         $data[] = array('id' => $f['alias'] . "." . $f['field']);

                    }
               }

          }

          return $data;

     }

     public function buildSearch()
     {
          $arr      = array();
          $keywords = '';
          $fields   = '';
          $param    = '';

          $allowsearch = $this->info['config']['forms'];

          $gridcolumns = $this->info['config']['grid'];

          foreach ($allowsearch as $as) {
               $arr[$as['field']] = $as;
          }

          if ($this->input->get('search', true) != '') {

               $type = explode("|", $this->input->get('search', true));

               if (count($type) >= 1) {

                    foreach ($type as $t) {

                         $keys = explode(":", $t);

                         if ($keys[0] == 'search' && !empty($keys[1])) {

                              $searchparam = "";

                              foreach ($gridcolumns as $gs) {

                                   if ($gs['view'] == 1) {

                                        $fieldname = $gs['field'];

                                        if (in_array($fieldname, array_keys($arr))):

                                             if ($searchparam != "") {

                                                  $searchparam .= " OR ";

                                             }

                                             if ($arr[$fieldname]['type'] == 'select' || $arr[$fieldname]['type'] == 'radio') {

                                                  if (!empty($keys[1])) {

                                                       $searchparam .= $arr[$fieldname]['alias'] . "." . $fieldname . " = '" . $keys[1] . "' ";

                                                  }

                                             } else {

                                                  if (!empty($keys[1])) {

                                                       $searchparam .= $arr[$fieldname]['alias'] . "." . $fieldname . " REGEXP '" . $keys[1] . "' ";

                                                  }

                                             } else :

                                        endif;

                                   }

                              }

                              if ($searchparam != "") {
                                   if (!empty($param)) $param .= " AND ";
                                   $param .= " (" . $searchparam . ") ";

                              }

                         }

                         if (in_array($keys[0], array_keys($arr))):

                              if ($arr[$keys[0]]['type'] == 'select' || $arr[$keys[0]]['type'] == 'radio') {
                                   if (!empty($param)) $param .= " AND ";
                                   $param .= $arr[$keys[0]]['alias'] . "." . $keys[0] . " = '" . $keys[1] . "' ";

                              } else {

                                   if (!empty($keys[1])) {
                                        if (!empty($param)) $param .= " AND ";
                                        $param .= $arr[$keys[0]]['alias'] . "." . $keys[0] . " REGEXP '" . $keys[1] . "' ";

                                   }

                              }

                         endif;

                    }

               }

          }

          return $param;

     }

     public function buildMasterDetail($template = null)
     {
          // check if url contain $_GET['md'] , that mean master detail
          if (!is_null($this->input->get('md', true)) and $this->input->get('md', true) != '') {

               $values = array();
               $data   = explode(" ", $this->input->get('md', true));
               // Split all param get
               $master     = $data[0];
               $master_key = $data[1];
               $module     = $data[2];
               $key        = $data[3];
               $val        = $data[4];

               $model = $module . 'model';
               $this->load->model($model);
               $this->modeldetail = $this->$model;

               $values['row']  = $this->modeldetail->getRow($val);
               $loadInfo       = $this->modeldetail->makeInfo($master);
               $values['grid'] = $loadInfo['config']['grid'];
               $filter         = " AND  " . $this->info['table'] . "." . $key . "='" . $val . "ss' ";
               if ($template != null) {
                    //$view                       = $this->load->view( $template, $values ,true) ;
               } else {
                    //$view                       = $this->load->view( 'layouts/masterview', $values ,true) ;
               }
               $result = array(
                    'masterFilter' => $filter,
                    //'masterView' => $view
               );
               return $result;

          } else {
               $result = array(
                    'masterFilter' => '',
                    //'masterView' => ''
               );
               return $result;
          }

     }
     public function buildMasterDetail2($template = null)
     {

          // check if url contain $_GET['md'] , that mean master detail

          if (!is_null($this->input->get('md', true)) and $this->input->get('md', true) != '') {

               $values = array();

               $data = explode(" ", $this->input->get('md', true));

               // Split all param get

               $master     = $data[0];
               $master_key = $data[1];
               $module     = $data[2];
               $key        = $data[3];
               $val        = $data[4];

               $model = $master . 'model';

               $this->load->model($model);

               $this->modeldetail = $this->$model;

               $values['row'] = $this->modeldetail->getRow($val);

               $loadInfo = $this->modeldetail->makeInfo($master);

               $values['grid'] = $loadInfo['config']['grid'];

               $filter = " AND  " . $this->info['table'] . "." . $key . "='" . $val . "ss' ";

               if ($template != null) {

                    //$view = $this->load->view( $template, $values ,true) ;

               } else {

                    //$view = $this->load->view( 'themes / masterview', $values ,true) ;

               }

               $result = array(

                    'masterFilter' => $filter,

                    //'masterView'     => $view

               );

               return $result;

          } else {

               $result = array(

                    'masterFilter' => '',

                    //'masterView'     => ''

               );

               return $result;

          }

     }

     public function comboselect()
     {

          $param = explode(':', $this->input->get('filter', true));

          $rows = $this->model->getComboselect($param);

          $items = array();

          $fields = explode("|", $param[2]);

          foreach ($rows as $row) {

               $value = "";

               foreach ($fields as $item => $val) {

                    if ($val != "") {
                         $value .= $row->$val . " ";
                    }

               }

               $items[] = array($row->$param['1'], $value);

          }

          echo json_encode($items);

     }

     public function combotable()
     {

          $rows = $this->model->getTableList($this->db->database);

          $items = array();

          foreach ($rows as $row) {

               $items[] = array($row, $row);

          }

          echo json_encode($items);

     }

     public function combotablefield()
     {

          $items = array();

          $table = $this->input->get('table', true);

          if ($table != '') {

               $rows = $this->model->getTableField($this->input->get('table', true));

               foreach ($rows as $row) {
                    $items[] = array($row, $row);
               }

          }

          echo json_encode($items);

     }

     public function validateListError($rules)
     {

          $errMsg = $this->lang->line('core.note_error');

          $errMsg .= '<hr /> <ul>';

          foreach ($rules as $key => $val) {

               $errMsg .= '<li>' . $key . ' : ' . $val[0] . '</li>';

          }

          $errMsg .= '</li>';

          return $errMsg;

     }

     public function validateForm()
     {

          $forms = $this->info['config']['forms'];

          $rules = array();

          foreach ($forms as $form) {

               if ($form['required'] == '' || $form['required'] != '0') {

                    $rules[] = array('field' => $form['field'], 'label' => $form['label'], 'rules' => 'required');

               } elseif ($form['required'] == 'alpa') {

                    $rules[] = array('field' => $form['field'], 'label' => $form['label'], 'rules' => 'required|alpha');

               } elseif ($form['required'] == 'alpa_num') {

                    $rules[] = array('field' => $form['field'], 'label' => $form['label'], 'rules' => 'required|alpha_numeric');

               } elseif ($form['required'] == 'alpa_dash') {

                    $rules[] = array('field' => $form['field'], 'label' => $form['label'], 'rules' => 'required|alpha_dash');

               } elseif ($form['required'] == 'email') {

                    $rules[] = array('field' => $form['field'], 'label' => $form['label'], 'rules' => 'required|valid_email');

               } elseif ($form['required'] == 'numeric') {

                    $rules[] = array('field' => $form['field'], 'label' => $form['label'], 'rules' => 'required|numeric');

               } else {

               }

          }

          //echo ' < pre > ';print_r($rules);echo ' < pre > ';exit;

          return $rules;

     }

     public function validatePost($table = '')
     {

          $str = $this->info['config']['forms'];

          $data = array();

          foreach ($str as $f) {

               $field = $f['field'];

               if ($f['view'] == 1) {

                    if (!is_null($this->input->get($field, true))) {

                         $data[$field] = $this->input->get($field, true);

                    }

                    switch ($f['type']) {

                         case 'textarea_editor':

                         case 'textarea':

                              $content = $this->input->get_post($field) ? $this->input->get_post($field) : '';

                              $data[$field] = $content;

                              break;

                         case 'file':

                              $this->load->library('upload');

                              $destinationPath = "./" . $f['option']['path_to_upload'];

                              $config['upload_path'] = $destinationPath;

                              $config['allowed_types'] = 'gif|jpg|png';

                              $this->upload->initialize($config);

                              if ($this->upload->do_upload($field)) {

                                   $file_data = $this->upload->data();

                                   $filename = $file_data['file_name'];

                                   $extension = $file_data['file_ext']; //if you need extension of the file

                                   if ($f['option']['resize_width'] != '0' && $f['option']['resize_width'] != '') {

                                        if ($f['option']['resize_height'] == 0) {

                                             $f['option']['resize_height'] = $f['option']['resize_width'];

                                        }

                                        $origFile = $destinationPath . '/' . $filename;

                                        SiteHelpers::cropImage($f['option']['resize_width'], $f['option']['resize_height'], $orgFile, $extension, $orgFile);

                                   }

                                   $data[$field] = $filename;

                              } else {

                                   unset($data[$field]);

                              }

                              break;

                         case 'checkbox':

                              if (!is_null($this->input->get($field, true))) {

                                   $data[$field] = implode(",", $this->input->get_post($field, true));

                              }

                              break;

                         case 'date':

                              $data[$field] = date("Y-m-d", strtotime($this->input->get_post($field, true)));

                              break;

                         case 'select':

                              if (isset($f['option']['is_multiple']) && $f['option']['is_multiple'] == 1) {

                                   $data[$field] = implode(",", $this->input->get_post($field, true));

                              } else {

                                   $data[$field] = $this->input->get_post($field, true);

                              }

                              break;

                         case 'text':

                         default:

                              $data[$field] = $this->input->get_post($field, true);

                              break;

                    }

               }

          }

          $global = (isset($this->access['is_global']) ? $this->access['is_global'] : 0);

          if ($global == 0) {
               $data['entry_by'] = $this->session->userdata('uid');
          }

          return $data;

     }

     public function validAccess($methode)
     {

          if ($this->model->validAccess($methode, $this->info['id']) == false) {

               $this->session->setflashdata('message', SiteHelpers::alert('error', ' Your are not allowed to access the page '));

               return redirect('home');

          }

     }

     public function inputLogs($note = null)
     {

          $data = array(

               'module'    => $this->uri->segment(1),

               'task'      => $this->uri->segment(2),

               'user_id'   => $this->session->userdata('uid'),

               'ipaddress' => $this->input->ip_address(),

               'note'      => $note,

          );

          $this->db->insert('sfm_cms_logs', $data);

     }

     public function paginator($options = array())
     {

          $keepLive = '';

          $sort = (!is_null($this->input->get('sort', true)) ? $this->input->get('sort', true) : '');

          $order = (!is_null($this->input->get('order', true)) ? $this->input->get('order', true) : '');

          $rows = (!is_null($this->input->get('rows', true)) ? $this->input->get('rows', true) : '');

          $search = (!is_null($this->input->get('search', true)) ? $this->input->get('search', true) : '');

          $appends = array();

          if ($sort != '') {
               $keepLive .= '&sort=' . $sort;
          }

          if ($order != '') {
               $keepLive .= '&order=' . $order;
          }

          if ($rows != '') {
               $keepLive .= '&rows=' . $rows;
          }

          if ($search != '') {
               $keepLive .= '&search=' . $search;
          }

          $toptions = array_replace_recursive(array(

               'base_url'   => ci_site_url($this->module) . '?' . $keepLive,

               'total_rows' => 0,

               'per_page'   => $this->per_page,

          ), $options);

          $pagination = $this->pagination->initialize($toptions);

          return $this->pagination->create_links();

     }

     public function displayError($data)
     {

          $this->load->view('themes/errors', $data);

     }

     //BEGIN AJAX ADDITIONAL CODE

     public function export($t = 'excel')
     {

          $info = $this->model->makeInfo($this->module);

          $params = array(

          );

          $results = $this->model->getRows($params);

          $fields = $info['config']['grid'];

          $rows = $results['rows'];

          $content = array(

               'fields' => $fields,

               'rows'   => $rows,

               'title'  => $this->data['pageTitle'],

          );

          if ($t == 'word') {

               $this->load->view('cms/module/utility/word', $content);

          } else
          if ($t == 'csv') {

               $this->load->view('cms/module/utility/csv', $content);

          } else
          if ($t == 'print') {

               $this->load->view('cms/module/utility/print', $content);

          } else {

               $this->load->view('cms/module/utility/excel', $content);

          }

     }

     public function flysearch()
     {

          $this->data['tableForm'] = $this->info['config']['forms'];

          $this->data['tableGrid'] = $this->info['config']['grid'];

          $this->load->view('cms/module/utility/search', $this->data);

     }

     public function paginatorajax($options = array(), $page = 'data')
     {

          if ($page == '') {
               $page = 'data';
          }

          $keepLive = '';

          $sort = (!is_null($this->input->get('sort', true)) ? $this->input->get('sort', true) : '');

          $order = (!is_null($this->input->get('order', true)) ? $this->input->get('order', true) : '');

          $rows = (!is_null($this->input->get('rows', true)) ? $this->input->get('rows', true) : '');

          $search = (!is_null($this->input->get('search', true)) ? $this->input->get('search', true) : '');

          $appends = array();

          if ($sort != '') {
               $keepLive .= '&sort=' . $sort;
          }

          if ($order != '') {
               $keepLive .= '&order=' . $order;
          }

          if ($rows != '') {
               $keepLive .= '&rows=' . $rows;
          }

          if ($search != '') {
               $keepLive .= '&search=' . $search;
          }

          $toptions = array_replace_recursive(array(

               'base_url'   => ci_site_url($this->module) . '/' . $page . '?' . $keepLive,

               'total_rows' => 0,

               'per_page'   => $this->per_page,

          ), $options);

          $this->pagination->initialize($toptions);

          return $this->pagination->create_links();

     }

     public function detailview($mode = 'view')
     {

          if ($this->access['is_view'] == 0) {
               echo SiteHelpers::alert('error', Lang::get('core.note_restric'));die;
          }

          $sort = (!is_null($this->input->get_post('sort')) ? $this->input->get_post('sort') : $this->info['setting']['orderby']);

          $order = (!is_null($this->input->get_post('order')) ? $this->input->get_post('order') : $this->info['setting']['ordertype']);

          $filter = (!is_null($this->input->get_post('search')) ? $this->buildSearch() : '');

          $page = $this->input->get_post('page', 1);

          $master = $this->buildMasterDetail();

          $filter .= $master['masterFilter'];

          $params = array(

               'params' => $filter,

               'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),

          );

          $results = $this->model->getRows($params);

          $this->data['rowData'] = $results['rows'];

          $this->data['tableGrid'] = $this->info['config']['grid'];

          $this->data['tableForm'] = $this->info['config']['forms'];

          $this->data['filtermd'] = str_replace(" ", "+", $this->input->get_post('md'));

          $md = explode(" ", $this->input->get_post('md'));

          $this->data['foreignKey'] = $md[3];

          $this->data['foreignVal'] = $md[4];

          $this->data['access'] = $this->access;

          $this->data['primarykey'] = $this->info['key'];

          if ($mode == 'form') {

               $this->load->view('cms/module/utility/detailform', $this->data);

          } else {

               $this->load->view('cms/module/utility/detailview', $this->data);

          }

     }

     public function postCopy()
     {

          $table = $this->info['table'];

          $key = $this->info['key'];

          foreach (DB::select("SHOW COLUMNS FROM $table ") as $column) {

               if ($column->Field != $key) {
                    $columns[] = $column->Field;
               }

          }

          $toCopy = implode(",", Input::get('id'));

          $sql = "INSERT INTO $table (" . implode(",", $columns) . ") ";

          $sql .= " SELECT " . implode(",", $columns) . " FROM $table WHERE $key IN (" . $toCopy . ")";

          DB::insert($sql);

          return Redirect::to($this->data['pageModule'])->with('message', SiteHelpers::alert('success', Lang::get('core.note_success')));

     }

     public function getSearch()
     {

          $this->data['tableForm'] = $this->info['config']['forms'];

          $this->data['tableGrid'] = $this->info['config']['grid'];

          return View::make('cms/module/utility/search', $this->data);

     }

     public function quicksave($id = 0)
     {

          $rules = $this->validateForm();

          $this->form_validation->set_rules($rules);

          if ($this->form_validation->run()) {

               $data = $this->validatePost($this->info['table']);

               unset($data[$this->info['key']]);

               $ID = $this->model->insertRow($data, $this->input->post($this->info['key'], true));

               header('content-type:application/json');

               echo json_encode(array(

                    'status'  => 'success',

                    'message' => ' Data has been saved succesfuly !',

               ));

          } else {

               header('content-type:application/json');

               echo json_encode(array(

                    'status'  => 'error',

                    'message' => ' Ops , Something went wrong !',

               ));

          }

     }

     public function removerow($id)
     {

          $id = array('0' => $id);

          $this->model->destroy($id);

          echo json_encode(array(

               'status'  => 'success',

               'message' => ' Data has been saved succesfuly !',

          ));

     }

     public function unileveldetailview($mode = 'view')
     {

          if ($this->access['is_view'] == 0) {
               echo SiteHelpers::alert('error', Lang::get('core.note_restric'));die;
          }

          $sort = (!is_null($this->input->get_post('sort')) ? $this->input->get_post('sort') : $this->info['setting']['orderby']);

          $order = (!is_null($this->input->get_post('order')) ? $this->input->get_post('order') : $this->info['setting']['ordertype']);

          $filter = (!is_null($this->input->get_post('search')) ? $this->buildSearch() : '');

          $page = $this->input->get_post('page', 1);

          // check if url contain $_GET['md'] , that mean master detail

          if (!is_null($this->input->get('md', true)) and $this->input->get('md', true) != '') {

               $values = array();

               $data = explode(" ", $this->input->get('md', true));

               $master = $data[0];

               $master_key = $data[1];

               $module = $data[2];

               $key = $data[3];

               $val = $data[4];

               $filter .= " and subtree.level+1  = " . $val . " ";

          }

          $params = array(

               'params' => $filter,

          );

          $results = $this->model->getQueryRows($params);

          $this->data['rowData'] = $results['rows'];

          $this->data['tableGrid'] = $this->info['config']['grid'];

          $this->data['tableForm'] = $this->info['config']['forms'];

          $this->data['filtermd'] = str_replace(" ", "+", $this->input->get_post('md'));

          $md = explode(" ", $this->input->get_post('md'));

          $this->data['foreignKey'] = $md[3];

          $this->data['foreignVal'] = $md[4];

          $this->data['access'] = $this->access;

          $this->data['primarykey'] = $this->info['key'];

          if ($mode == 'form') {

               $this->load->view('cms/module/utility/detailform', $this->data);

          } else {

               $this->load->view('cms/module/utility/detailview', $this->data);

          }

     }

     public function create_members_jsonfile()
     {

          $this->db->select('id, username, email');

          $this->db->from('sfm_cms_users');

          $query = $this->db->get();

          $resultdata = $query->result_array();

          $jsonEvents = json_encode($resultdata);

          file_put_contents(FCPATH . 'members.json', $jsonEvents);

     }

     public function create_admin_members_jsonfile()
     {

          $ssql = "SELECT

          `sfm_uap_affiliates`.`id` AS `id`,

          `sfm_uap_affiliates`.`user_id` AS `user_id`,

          `sfm_uap_affiliates`.`display_name` AS `display_name`,

          `sfm_uap_affiliates`.`username` AS `username`,

          `sfm_uap_affiliates`.`firstname` AS `firstname`,

          `sfm_uap_affiliates`.`lastname` AS `lastname`,

          `sfm_uap_affiliates`.`email` AS `email`,

          `sfm_uap_affiliates`.`phone` AS `phone`,

          `sfm_uap_affiliates`.`rank` AS `rank`,

          `sfm_uap_affiliates`.`company` AS `company`,

          `sfm_uap_affiliates`.`address1` AS `address1`,

          `sfm_uap_affiliates`.`address2` AS `address2`,

          `sfm_uap_affiliates`.`city` AS `city`,

          `sfm_uap_affiliates`.`state` AS `state`,

          `sfm_uap_affiliates`.`zipcode` AS `zipcode`,

          `sfm_uap_affiliates`.`country` AS `country`,

          `sfm_uap_affiliates`.`shipping_address1` AS `shipping_address1`,

          `sfm_uap_affiliates`.`shipping_address2` AS `shipping_address2`,

          `sfm_uap_affiliates`.`shipping_city` AS `shipping_city`,

          `sfm_uap_affiliates`.`shipping_state` AS `shipping_state`,

          `sfm_uap_affiliates`.`shipping_zipcode` AS `shipping_zipcode`,

          `sfm_uap_affiliates`.`shipping_country` AS `shipping_country`,

          `sfm_uap_affiliates`.`isadmin` AS `isadmin`,

          `sfm_uap_affiliates`.`paid` AS `paid`,

          `sfm_uap_affiliates`.`replicated_url` AS `replicated_url`,

          `sfm_uap_affiliates`.`sponsorkey` AS `sponsorkey`,

          `sfm_uap_affiliates`.`foundingpositions` AS `foundingpositions`,

          `sfm_uap_affiliates`.`upline_diamond` AS `upline_diamond`,

          `sfm_uap_affiliates`.`upline_doublediamond` AS `upline_doublediamond`,

          `sfm_uap_affiliates`.`upline_triplediamond` AS `upline_triplediamond`,

          `sfm_uap_affiliates`.`upline_ambassador` AS `upline_ambassador`,

          `sfm_uap_affiliates`.`upline_crown` AS `upline_crown`,

          `sfm_uap_affiliates`.`date_registered` AS `date_registered`,

          `sfm_uap_affiliates`.`downlines` AS `downlines`,

          `sfm_uap_affiliates`.`referralsales` AS `referralsales`,

          `sfm_uap_affiliates`.`status` AS `status`,

          `sfm_uap_affiliates`.`rank_id` AS `rank_id`,

          `sfm_uap_affiliates`.`firstpurchasedate` AS `firstpurchasedate`

          FROM

          `sfm_uap_affiliates`";

          $this->db->query($ssql);

          $resultdata = $query->result();

          $jsonEvents = json_encode($resultdata);

          file_put_contents(FCPATH . 'memberdata.json', $jsonEvents);

     }

     public function getuserdropdown()
     {

          $username = trim($this->input->get('term')); //get term parameter sent via text field. Not sure how secure get() is

          $this->db->select('id,username as name, email');

          $this->db->from('sfm_cms_users');

          $this->db->like('username', $username);

          $this->db->or_like('email', $username);

          $this->db->limit('5');

          $query = $this->db->get();

          $resultdata = $query->result_array();

          $jsonEvents = json_encode($resultdata);

          echo $jsonEvents;

          die;

     }

     //     function checkusername()

     //     {

     //        $username = trim($this->input->get('term')); //get term parameter sent via text field. Not sure how secure get() is

     //        $row = $this->db->get_where('sfm_cms_users',array('username' => $usernam))->row();

     //

     //        $this->db->select('count(0) as total');

     //        $this->db->from('sfm_cms_users');

     //        $this->db->like('username', $username);

     //        $query = $this->db->get();

     //          $resultdata = $query->row_array();

     //          $jsonEvents = json_encode($resultdata);

     //          echo $jsonEvents;

     //          die;

     //     }

     //END AJAX ADDITIONAL CODE

     public function Sync_Member_Data($user_id = 0)
     {

          global $wpdb;

          $member_data = array();

          $user = $wpdb->get_row("Select user_login, user_registered from main_freemart.wp_users where id = $user_id");

          // $value = $totalcomm - get_total_reserved_comms($userid) ;

          // $value = $value - get_withdrawals_completepending($userid);

          // $value = $value - get_total_comm_purchase_excluded($userid);

          $member_data['total_commissions'] = get_overall_commissions($user_id);

          $member_data['available_commissions'] = get_current_comms($user_id);

          $member_data['register_date'] = $user->user_registered;

          $member_data['current_rank'] = get_rank($user_id);

          $totalwo = $wpdb->get_var("select count(0) from wp_posts

          left join  wp_trans_postmeta  on wp_trans_postmeta.post_id = wp_trans_posts.id  and wp_trans_postmeta.meta_key = '_customer_user'

          where post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing')

          and wp_trans_postmeta.meta_value = $user_id");

          if ($totalwo == "") {

               $totalwo = "0";

          }

          $member_data['total_orders'] = $totalwo;

          $member_data['total_coins'] = get_overall_coins($user_id);

          $member_data['available_coins'] = get_current_coins_2($user_id);

          $args = array(

               'meta_key'    => 'referral_id',

               'meta_value'  => $user_id,

               'count_total' => true,

          );

          $wp_user_query = new WP_User_Query($args);

          $member_data['total_downlines'] = $wp_user_query->total_users;

          $member_data['paid_downlines'] = get_totalsales($user_id);

          $member_data['unpaid_referrals'] = (int) $member_data['total_downlines'] - (int) $member_data['paid_downlines'];

          $sponsorinfo = new WP_User(get_diamond($user_id));

          $sponsor_diamond = $sponsorinfo->id;

          $sponsorinfo = new WP_User(get_doublediamond($user_id));

          $sponsor_doublediamond = $sponsorinfo->id;

          $sponsorinfo = new WP_User(get_triplediamond($user_id));

          $sponsor_triplediamond = $sponsorinfo->id;

          $sponsorinfo = new WP_User(get_ambassador($user_id));

          $sponsor_ambassador = $sponsorinfo->id;

          $member_data['referral_link'] = $user->user_login;

          $member_data['upline_diamond'] = $sponsor_diamond;

          $member_data['upline_doublediamond'] = $sponsor_doublediamond;

          $member_data['upline_triplediamond'] = $sponsor_triplediamond;

          $member_data['upline_ambassador'] = $sponsor_ambassador;

          $sql = "UPDATE fremartc_devshop7.sfm_uap_affiliates Set

          commissions_overall='" . $member_data['total_commissions'] . "'

          ,commissions='" . $member_data['available_commissions'] . "'

          ,date_registered='" . $member_data['register_date'] . "'

          ,rank='" . $member_data['current_rank'] . "'

          ,orders='" . $member_data['total_orders'] . "'

          ,coins_overall='" . $member_data['total_coins'] . "'

          ,coins='" . $member_data['available_coins'] . "'

          ,downlines='" . $member_data['total_downlines'] . "'

          ,referralsales='" . $member_data['paid_downlines'] . "'

          ,replicated_url='" . $member_data['referral_link'] . "'

          ,upline_diamond ='" . $member_data['upline_diamond'] . "'

          ,upline_doublediamond ='" . $member_data['upline_doublediamond'] . "'

          ,upline_triplediamond ='" . $member_data['upline_triplediamond'] . "'

          ,upline_ambassador ='" . $member_data['upline_ambassador'] . "'

          WHERE user_id =" . $user_id;

          echo "<pre>Done ({$user_id}): {$sql}</pre>";

          $rez = $wpdb->query($sql);

          return $member_data;

     }

     public function setadditionalsettings()
     {

          $keys = array(

               array('gs_checkbox', 'true'),

               array('gs_counter', 'true'),

               array('gs_buttonpanel', 'true'),

               array('gs_inlinebuttonpanel', 'true'),

               array('gs_portlet', 'true'),

               array('gs_rowsearch', 'true'),

               array('gs_css', 'table'),

               array('gs_toolbar', 'true'),

               array('gs_footer', 'true'),

               array('gs_filterbar', 'true'),

          );

          foreach ($keys as $key) {

               $keyname = $key[0];

               $defaultvalue = $key[1];

               $this->gridsettings[$keyname] = $this->input->post($keyname, $defaultvalue);

          }

          return $this->gridsettings;

     }

     public function backend_init()
     {

          $this->db->cache_off();
          $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
          $this->output->set_header("Cache-Control: private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0");
          $this->output->set_header("Pragma: no-cache");

          // if (SSL_ADMIN_AREA == true) {
          //       $this->config->set_item('base_url', $this->config->item('base_SSL_url'));
          // }

          $this->Authentication_model->autologin();

          if (!is_staff_logged_in()) {
               if (strpos(current_full_url(), 'authentication/admin') === false) {
                    $this->session->set_userdata(array(
                         'red_url' => $this->uri->uri_string(),
                    ));
               }
               redirect(ADMINLOGIN_URL, 301);
               // redirect(ci_site_url('authentication / admin'));
          }

          // FROM THIS LINE THE USER IS NOW LOGGED - IN
          if ($this->session->userdata('logged_in')) {

               $this->session->set_userdata('lang', 'en');

               $this->load->language('core', 'en');

               $this->load->language('core', $this->session->userdata('lang'));

          }

          // LOAD ADMIN LIBRARIES
          $this->load_backend_classes();

          // LOAD ADMIN LANGUAGES
          $language = load_perfex_language();

          $this->db->reconnect();
          $timezone = perfex_get_option('default_timezone');
          date_default_timezone_set($timezone);

          //    date_default_timezone_set(get_option('timezone', 'asia/jakarta'));
          //  load_extensions();
          //    $this->load->library(['aauth', 'cc_html', 'cc_page_element', 'cc_app']);

          // $user_data = array(
          //  'client_user_id'  => '1',
          //  'contact_user_id' => '1',
          //  'client_logged_in'=> true
          // );

          // $staff_data = array(
          //  'staff_user_id'  => '1',
          //  'staff_logged_in'=> true
          // );
          // $this->session->set_userdata($staff_data);

          // Check for just updates message
          perfex_add_action('before_start_render_content', 'show_just_updated_message');

          // Do not check on ajax requests
          if (!$this->input->is_ajax_request()) {
               if (ENVIRONMENT == 'production' && is_admin()) {
                    if ($this->config->item('encryption_key') === '') {
                         die('<h1>Encryption key not sent in application/config/config.php</h1>For more info visit <a href="http://www.perfexcrm.com/knowledgebase/encryption-key/">Encryption key explained</a> FAQ3');
                    } else
                    if (strlen($this->config->item('encryption_key')) != 32) {
                         die('<h1>Encryption key length should be 32 charachters</h1>For more info visit <a href="http://www.perfexcrm.com/knowledgebase/encryption-key/">Encryption key explained</a>');
                    }
               }

               perfex_add_action('before_start_render_content', 'show_development_mode_message');
               // Check if cron is required to be setup for some features
               perfex_add_action('before_start_render_content', 'is_cron_setup_required');

          }

          $this->init_quick_actions_links();

          if (is_mobile()) {
               $this->session->set_userdata(array(
                    'is_mobile' => true,
               ));
          } else {
               $this->session->unset_userdata('is_mobile');
          }

          $auto_loaded_vars = array(
               'page'                            => $this->input->get('page', true),
               '_staff'                          => $this->staff_model->get(get_staff_user_id()),
               '_notifications'                  => $this->misc_model->get_user_notifications(false),
               '_quick_actions'                  => $this->perfex_base->get_quick_actions_links(),
               '_started_timers'                 => $this->misc_model->get_staff_started_timers(),
               'google_api_key'                  => perfex_get_option('google_api_key'),
               'total_pages_newsfeed'            => total_rows('main_crm.crm_posts') / 10,
               'locale'                          => get_locale_key($language),
               'tinymce_lang'                    => get_tinymce_language(get_locale_key($language)),
               '_pinned_projects'                => $this->get_pinned_projects(),
               'total_undismissed_announcements' => $this->get_total_undismissed_announcements(),
               'current_version'                 => APP_VERSION,
               'tasks_filter_assignees'          => $this->get_tasks_distinct_assignees(),
               'task_statuses'                   => $this->tasks_model->get_statuses(),
          );

          $auto_loaded_vars = perfex_do_action('before_set_auto_loaded_vars_admin_area', $auto_loaded_vars);
          $this->load->vars($auto_loaded_vars);

          perfex_do_action('perfex_init');

          // $this->site_theme   = 'shop'; //  $this->config->item('site_theme');
          // $layout_design_header_name  = "Default";
          // if (isset($_GET['st']))
          // {
          //     $this->site_theme = $_GET['st'];
          // }

          //      $this->load->model('affiliate_marketing_model', 'aff');

          //      $this->load->model('commissions_model', 'comm');
          //      $this->load->model('support_model', 'support');
          //      $this->load->model('product_cat_model', 'prod_cat');
          //      $this->load->model('login_model', 'login');
          //      $this->load->model('members_model', 'members');
          //      $this->load->model('mailing_lists_model', 'lists');
          //      $this->load->model('emailing_model', 'emailing');
          $this->loadmodels();

          //check user's login status, if not logged in redirect to signin page
          // if (!$login_user_id) {
          //     $uri_string = uri_string();

          //     if (!$uri_string || $uri_string === "signin") {
          //         redirect('signin');
          //     } else {
          //         redirect('signin?redirect=' . get_uri($uri_string));
          //     }
          // }

          //        //initialize login users required information
          //
          //        initialize login users access permissions
          // if ($this->Users_model->login_user()->permissions) {
          //     $permissions = unserialize($this->Users_model->login_user()->permissions);
          //     $this->Users_model->login_user()->permissions = is_array($permissions) ? $permissions : array();
          // } else {
          //     $this->Users_model->login_user()->permissions = array();
          // }
          //Load admin template and theme

     }

     public function load_backend_classes()
     {
          if (defined('PERFEX_APPPATH')) {
               return;
          }

          // $this->load->library('database');
          $this->load->helper('crm/perfex_constants');
          $this->load->helper('crm/perfex_files');

          // $this->load->library('crm / user_agent');
          // $this->load->library('crm / encryption');
          // $this->load->library('crm / email');
          $this->load->library('crm/encoding_lib');
          $this->load->library('crm/perfex_base');

          //        $gateways = list_files(CRM_MODULE_PATH.'libraries / gateways');
          //        foreach($gateways as $gateway){
          //            $pathinfo = pathinfo($gateway);
          //            // Check if file is .php and do not starts with .dot
          //            // Offen happens Mac os user to have underscore prefixed files while unzipping the zip file.
          //            if($pathinfo['extension'] == 'php' && 0 !== strpos($gateway, '.')){
          //                $this->load->library(CRM_MODULE_PATH.'libraries / gateways / '.strtolower($pathinfo['filename']));
          //            }

          $this->load->language('crm/english');

          $this->load->helper('crm/perfex_misc');
          $this->load->helper('crm/perfex_custom_fields');
          $this->load->helper('crm/perfex_merge_fields');
          $this->load->helper('crm/perfex_html');
          $this->load->helper('crm/perfex_db');
          $this->load->helper('crm/perfex_upload');
          $this->load->helper('crm/perfex_sales');
          $this->load->helper('crm/perfex_themes');
          $this->load->helper('crm/perfex_theme_style');

          $this->load->model('crm/Staff_model');
          $this->load->model('crm/Projects_model');
          $this->load->model('crm/Perfex_settings_model');
          $this->load->model('crm/misc_model');
          $this->load->model('crm/roles_model');
          $this->load->model('crm/clients_model');
          $this->load->model('crm/Taxes_model');

          $this->load->model('crm/tasks_model');
          $this->load->model('crm/tickets_model');

          $this->load->model('crm/Estimates_model');
          $this->load->model('crm/Invoices_model');

          //  $this->load->model('crm / tasks_model');

     }

     public function is_allowed($perm, $redirect = true)
     {
          if (!$this->aauth->is_loggedin()) {
               if ($redirect) {
                    redirect('administrator/login', 'refresh');
               } else {
                    return false;
               }
          } else {
               if ($this->aauth->is_allowed($perm)) {
                    return true;
               } else {
                    if ($redirect) {
                         $this->session->set_flashdata('f_message', 'Sorry you do not have permission to access ');
                         $this->session->set_flashdata('f_type', 'warning');
                         redirect('administrator/dashboard', 'refresh');
                    }
                    return false;
               }
          }

     }

     public function upload_file2($data)
     {
          $default = [
               'uuid'          => '',
               'allowed_types' => '*',
               'max_size'      => '',
               'max_width'     => '',
               'max_height'    => '',
               'upload_path'   => './uploads/tmp/',
               'input_files'   => 'qqfile',
               'table_name'    => '',
          ];

          foreach ($data as $key => $value) {
               if (isset($default[$key])) {
                    $default[$key] = $value;
               }
          }

          $dir = FCPATH . $default['upload_path'] . $default['uuid'];
          if (!is_dir($dir)) {
               mkdir($dir);
          }

          if (empty($default['file_name'])) {
               $default['file_name'] = date('Y-m-d') . $default['table_name'] . date('His');
          }

          $config = [
               'upload_path'   => $default['upload_path'] . $default['uuid'] . '/',
               'allowed_types' => $default['allowed_types'],
               'max_size'      => $default['max_size'],
               'max_width'     => $default['max_width'],
               'max_height'    => $default['max_height'],
               'file_name'     => $default['file_name'],
          ];

          $this->load->library('upload', $config);
          $this->load->helper('file');

          if (!$this->upload->do_upload('qqfile')) {
               $result = [
                    'success' => false,
                    'error'   => $this->upload->display_errors(),
               ];

               return json_encode($result);
          } else {
               $upload_data = $this->upload->data();

               $result = [
                    'uploadName'  => $upload_data['file_name'],
                    'previewLink' => $dir . '/' . $upload_data['file_name'],
                    'success'     => true,
               ];

               return json_encode($result);
          }
     }

     public function delete_file($data = [])
     {
          $default = [
               'uuid'            => '',
               'delete_by'       => '',
               'field_name'      => 'image',
               'upload_path_tmp' => './uploads/tmp/',
               'table_name'      => 'test',
               'primaryKey'      => 'id',
               'upload_path'     => 'uploads/blog/',
          ];

          foreach ($data as $key => $value) {
               if (isset($default[$key])) {
                    $default[$key] = $value;
               }
          }

          if (!empty($default['uuid'])) {
               $this->load->helper('file');
               $delete_file = false;

               if ($default['delete_by'] == 'id') {
                    $row = $this->db->get_where($default['table_name'], [$default['primaryKey'] => $default['uuid']])->row();
                    if ($row) {
                         $path = FCPATH . $default['upload_path'] . $row->{$default['field_name']};
                    }

                    if (isset($default['uuid'])) {
                         if (is_file($path)) {
                              $delete_file = unlink($path);
                              $this->db->where($default['primaryKey'], $default['uuid']);
                              $this->db->update($default['table_name'], [$default['field_name'] => '']);
                         }
                    }
               } else {
                    $path        = FCPATH . $default['upload_path_tmp'] . $default['uuid'] . '/';
                    $delete_file = delete_files($path, true);
               }

               if (isset($default['uuid'])) {
                    if (is_dir($path)) {
                         rmdir($path);
                    }
               }

               if (!$delete_file) {
                    $result = [
                         'error' => 'Error delete file',
                    ];

                    return json_encode($result);
               } else {
                    $result = [
                         'success' => true,
                    ];

                    return json_encode($result);
               }
          }
     }

     public function get_file($data = [])
     {
          $default = [
               'uuid'            => '',
               'delete_by'       => '',
               'field_name'      => 'image',
               'table_name'      => 'test',
               'primaryKey'      => 'id',
               'upload_path'     => 'uploads/blog/',
               'delete_endpoint' => 'administrator/blog/delete_image_file',
          ];

          foreach ($data as $key => $value) {
               if (isset($default[$key])) {
                    $default[$key] = $value;
               }
          }

          $row = $this->db->get_where($default['table_name'], [$default['primaryKey'] => $default['uuid']])->row();

          if (!$row) {
               $result = [
                    'error' => 'Error getting file',
               ];

               return json_encode($result);
          } else {
               if (!empty($row->{$default['field_name']})) {
                    if (strpos($row->{$default['field_name']}, ',')) {
                         foreach (explode(',', $row->{$default['field_name']}) as $filename) {
                              $result[] = [
                                   'success'            => true,
                                   'thumbnailUrl'       => check_is_image_ext(base_url($default['upload_path'] . $filename)),
                                   'id'                 => 0,
                                   'name'               => $row->{$default['field_name']},
                                   'uuid'               => $row->{$default['primaryKey']},
                                   'deleteFileEndpoint' => base_url($default['delete_endpoint']),
                                   'deleteFileParams'   => ['by' => $default['delete_by']],
                              ];
                         }
                    } else {
                         $result[] = [
                              'success'            => true,
                              'thumbnailUrl'       => check_is_image_ext(base_url($default['upload_path'] . $row->{$default['field_name']})),
                              'id'                 => 0,
                              'name'               => $row->{$default['field_name']},
                              'uuid'               => $row->{$default['primaryKey']},
                              'deleteFileEndpoint' => base_url($default['delete_endpoint']),
                              'deleteFileParams'   => ['by' => $default['delete_by']],
                         ];
                    }

                    return json_encode($result);
               }
          }
     }

     public function get_tasks_distinct_assignees()
     {
          return $this->misc_model->get_tasks_distinct_assignees();
     }
     public function get_total_undismissed_announcements()
     {
          $this->load->model('crm/Adminannouncements_model');
          return $this->Adminannouncements_model->get_total_undismissed_announcements();
     }
     public function get_pinned_projects()
     {
          $this->db->select('main_crm.crm_projects.id,main_crm.crm_projects.name');
          $this->db->join('main_crm.crm_projects', 'main_crm.crm_projects.id=main_crm.crm_pinnedprojects.project_id');
          $this->db->where('main_crm.crm_pinnedprojects.staff_id', get_staff_user_id());
          $projects = $this->db->get('main_crm.crm_pinnedprojects')->result_array();
          $i        = 0;
          $this->load->model('projects_model');
          foreach ($projects as $project) {
               $projects[$i]['progress'] = $this->projects_model->calc_progress($project['id']);
               $i++;
          }
          return $projects;
     }

     public function init_quick_actions_links()
     {

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_invoice'),
               'permission' => 'invoices',
               'url'        => 'invoices/invoice',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_estimate'),
               'permission' => 'estimates',
               'url'        => 'estimates/estimate',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_new_expense'),
               'permission' => 'expenses',
               'url'        => 'expenses/expense',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('new_proposal'),
               'permission' => 'proposals',
               'url'        => 'proposals/proposal',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('new_project'),
               'url'        => 'projects/project',
               'permission' => 'projects',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'            => _l('qa_create_task'),
               'url'             => '#',
               'custom_url'      => true,
               'href_attributes' => array(
                    'onclick' => 'new_task();return false;',
               ),
               'permission'      => 'tasks',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_client'),
               'permission' => 'customers',
               'url'        => 'clients/client',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_contract'),
               'permission' => 'contracts',
               'url'        => 'contracts/contract',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'            => _l('qa_create_lead'),
               'url'             => '#',
               'custom_url'      => true,
               'permission'      => 'is_staff_member',
               'href_attributes' => array(
                    'onclick' => 'init_lead(); return false;',
               ),
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_new_goal'),
               'url'        => 'goals/goal',
               'permission' => 'goals',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_kba'),
               'permission' => 'knowledge_base',
               'url'        => 'knowledge_base/article',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_survey'),
               'permission' => 'surveys',
               'url'        => 'surveys/survey',
          ));

          $tickets = array(
               'name' => _l('qa_create_ticket'),
               'url'  => 'tickets/add',
          );
          if (perfex_get_option('access_tickets_to_none_staff_members') == 0 && !is_staff_member()) {
               $tickets['permission'] = 'is_staff_member';
          }
          $this->perfex_base->add_quick_actions_link($tickets);

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('qa_create_staff'),
               'url'        => 'staff/member',
               'permission' => 'staff',
          ));

          $this->perfex_base->add_quick_actions_link(array(
               'name'       => _l('utility_calendar_new_event_title'),
               'url'        => 'utilities/calendar?new_event=true&date=' . date('Y-m-d'),
               'permission' => '',
          ));

     }

     public function member_init()
     {

          // FROM THIS LINE THE USER IS NOW LOGGED - IN
          if ($this->session->userdata('logged_in')) {
               $this->session->set_userdata('lang', 'en');

               $this->load->language('core', 'en');

               $this->load->language('core', $this->session->userdata('lang'));

          }
          //

          $this->loadmodels();

          $enc_session = $this->session->userdata('session_id') . md5($this->config->item('encryption_key'));

          $this->config->set_item('enc_id', $enc_session);

          //Load admin template and theme
          $this->site_theme = $this->config->item("members_theme");

          $this->config->setSiteTheme($this->site_theme);

     }

     public function logout()
     {

          $this->Users_model->sign_out();

     }

     private function _setup()
     {

          // router info
          $this->mModule = $this->router->fetch_module();
          $this->mCtrler = $this->router->fetch_class();
          $this->mAction = $this->router->fetch_method();
          $this->mMethod = $this->input->server('REQUEST_METHOD');

          $config = $this->config->item('app_config');

          // load default values
          $this->mBaseUrl         = empty($this->mModule) ? base_url() : base_url($this->mModule) . '/';
          $this->mSiteName        = empty($config['site_name']) ? '' : $config['site_name'];
          $this->mPageTitlePrefix = empty($config['page_title_prefix']) ? '' : $config['page_title_prefix'];
          $this->mPageTitle       = empty($config['page_title']) ? '' : $config['page_title'];
          $this->mBodyClass       = empty($config['body_class']) ? '' : $config['body_class'];
          $this->mMenu            = empty($config['menu']) ? array() : $config['menu'];
          $this->mMetaData        = empty($config['meta_data']) ? array() : $config['meta_data'];
          $this->mScripts         = empty($config['scripts']) ? array() : $config['scripts'];
          $this->mStylesheets     = empty($config['stylesheets']) ? array() : $config['stylesheets'];
          $this->mPageAuth        = empty($config['page_auth']) ? array() : $config['page_auth'];

          // multilingual setup
          $lang_config = empty($config['languages']) ? array() : $config['languages'];
          if (!empty($lang_config)) {
               $this->mMultilingual = true;
               $this->load->helper('language');

               // redirect to Home page in default language
               // if (empty($this->uri->segment(1))) {
               //      $home_url = base_url($lang_config['default']) . '/';
               //      redirect($home_url);
               // }

               // get language from URL, or from config's default value (in ci_bootstrap.php)
               $this->mAvailableLanguages = $lang_config['available'];
               $language                  = array_key_exists($this->uri->segment(1), $this->mAvailableLanguages) ? $this->uri->segment(1) : $lang_config['default'];

               // append to base URL
               $this->mBaseUrl .= $language . '/';

               // autoload language files
               foreach ($lang_config['autoload'] as $file) {
                    $this->lang->load($file, $this->mAvailableLanguages[$language]['value']);
               }

               $this->mLanguage = $language;
          }

          // restrict pages
          $uri = ($this->mAction == 'index') ? $this->mCtrler : $this->mCtrler . '/' . $this->mAction;
          if (!empty($this->mPageAuth[$uri]) && !$this->ion_auth->in_group($this->mPageAuth[$uri])) {
               $page_404     = $this->router->routes['404_override'];
               $redirect_url = empty($this->mModule) ? $page_404 : $this->mModule . '/' . $page_404;
               redirect($redirect_url);
          }

          // push first entry to breadcrumb
          if ($this->mCtrler != 'home') {
               $page = $this->mMultilingual ? lang('home') : 'Home';
               $this->push_breadcrumb($page, '');
          }

          // get user data if logged in
          // if ($this->ion_auth->logged_in()) {
          //      $this->mUser = $this->ion_auth->user()->row();
          //      if (!empty($this->mUser)) {
          //           $this->mUserGroups = $this->ion_auth->get_users_groups($this->mUser->id)->result();

          //           // TODO: get group with most permissions (instead of getting first group)
          //           $this->mUserMainGroup = $this->mUserGroups[0]->name;
          //      }
          // }

          $this->mConfig = $config;
     }

     // Add script files, either append or prepend to $this->mScripts array
     // ($files can be string or string array)
     protected function add_script($files, $append = true, $position = 'foot')
     {
          $files    = is_string($files) ? array($files) : $files;
          $position = ($position === 'head' || $position === 'foot') ? $position : 'foot';

          if ($append) {
               $this->mScripts[$position] = array_merge($this->mScripts[$position], $files);
          } else {
               $this->mScripts[$position] = array_merge($files, $this->mScripts[$position]);
          }

     }

     // Add stylesheet files, either append or prepend to $this->mStylesheets array
     // ($files can be string or string array)
     protected function add_stylesheet($files, $append = true, $media = 'screen')
     {

          $files = is_string($files) ? array($files) : $files;

          if ($append) {
               $this->mStylesheets[$media] = array_merge($this->mStylesheets[$media], $files);
          } else {
               $this->mStylesheets[$media] = array_merge($files, $this->mStylesheets[$media]);
          }

     }

     // Render template
     public function render($view_file, $viewdata = array(), $layout = 'default')
     {

          $this->mViewData = array_replace_recursive($this->mViewData, $viewdata);

          // automatically generate page title
          if (empty($this->mPageTitle)) {
               if ($this->mAction == 'index') {
                    $this->mPageTitle = humanize($this->mCtrler);
               } else {
                    $this->mPageTitle = humanize($this->mAction);
               }

          }

          $this->mViewData['module'] = $this->mModule;
          $this->mViewData['ctrler'] = $this->mCtrler;
          $this->mViewData['action'] = $this->mAction;

          $this->mViewData['site_name']   = $this->mSiteName;
          $this->mViewData['page_title']  = $this->mPageTitlePrefix . $this->mPageTitle;
          $this->mViewData['current_uri'] = empty($this->mModule) ? uri_string() : str_replace($this->mModule . '/', '', uri_string());
          $this->mViewData['meta_data']   = $this->mMetaData;
          $this->mViewData['scripts']     = $this->mScripts;
          $this->mViewData['stylesheets'] = $this->mStylesheets;
          $this->mViewData['page_auth']   = $this->mPageAuth;

          $this->mViewData['base_url']   = $this->mBaseUrl;
          $this->mViewData['menu']       = $this->mMenu;
          $this->mViewData['user']       = $this->mUser;
          $this->mViewData['ga_id']      = empty($this->mConfig['ga_id']) ? '' : $this->mConfig['ga_id'];
          $this->mViewData['body_class'] = $this->mBodyClass;

          // automatically push current page to last record of breadcrumb
          $this->push_breadcrumb($this->mPageTitle);
          $this->mViewData['breadcrumb'] = $this->mBreadcrumb;

          // multilingual
          $this->mViewData['multilingual'] = $this->mMultilingual;
          if ($this->mMultilingual) {
               $this->mViewData['available_languages'] = $this->mAvailableLanguages;
               $this->mViewData['language']            = $this->mLanguage;
          }

          // debug tools - CodeIgniter profiler
          $debug_config = $this->mConfig['debug'];
          if (ENVIRONMENT === 'development' && !empty($debug_config)) {
               $this->output->enable_profiler($debug_config['profiler']);
          }

          //  $viewdata = array_replace_recursive($this->data, $viewdata);

          $this->mViewData['content'] = $this->load->view($view_file, $this->mViewData, true);
          //$this->mViewData['content'] = $this->load->view($viewpath,  $this->mViewData, true);

          $this->load->view('themes/adminlte/head', $this->mViewData);
          $this->load->view('themes/adminlte/_layouts/' . $layout, $this->mViewData);

          // debug tools - display view data
          if (ENVIRONMENT === 'development' && !empty($debug_config) && !empty($debug_config['view_data'])) {
               $this->output->append_output('<hr/>' . print_r($this->mViewData, true));
          }

          $this->load->view('themes/adminlte/foot', $this->mViewData);

          //   $this->render_view($view_file,$this->mViewData);

     }

     // Output JSON string
     protected function render_json($data, $code = 200)
     {
          $this->output
               ->set_status_header($code)
               ->set_content_type('application/json')
               ->set_output(json_encode($data));

          // force output immediately and interrupt other scripts
          global $OUT;
          $OUT->_display();
          exit;
     }

     // Add breadcrumb entry
     // (Link will be disabled when it is the last entry, or URL set as '#')
     protected function push_breadcrumb($name, $url = '#', $append = true)
     {
          $entry = array('name' => $name, 'url' => $url);

          if ($append) {
               $this->mBreadcrumb[] = $entry;
          } else {
               array_unshift($this->mBreadcrumb, $entry);
          }

     }

     // Initialize CRUD table via Grocery CRUD library
     // Reference: http://www.grocerycrud.com/
     protected function generate_crud($table, $subject = '')
     {
          // create CRUD object
          $this->load->library('Grocery_CRUD');
          $crud = new grocery_CRUD();
          $crud->set_table($table);

          // auto-generate subject
          if (empty($subject)) {
               $crud->set_subject(humanize(singular($table)));
          }

          // load settings from: application/config/grocery_crud.php
          $this->load->config('grocery_crud');

          $this->mCrudUnsetFields = $this->config->item('grocery_crud_unset_fields');

          if ($this->config->item('grocery_crud_unset_jquery')) {
               $crud->unset_jquery();
          }

          if ($this->config->item('grocery_crud_unset_jquery_ui')) {
               $crud->unset_jquery_ui();
          }

          if ($this->config->item('grocery_crud_unset_print')) {
               $crud->unset_print();
          }

          if ($this->config->item('grocery_crud_unset_export')) {
               $crud->unset_export();
          }

          if ($this->config->item('grocery_crud_unset_read')) {
               $crud->unset_read();
          }

          foreach ($this->config->item('grocery_crud_display_as') as $key => $value) {
               $crud->display_as($key, $value);
          }

          // other custom logic to be done outside
          $this->mCrud = $crud;
          return $crud;
     }

     // Set field(s) to color picker
     protected function set_crud_color_picker()
     {
          $args = func_get_args();
          if (isset($args[0]) && is_array($args[0])) {
               $args = $args[0];
          }
          foreach ($args as $field) {
               $this->mCrud->callback_field($field, array($this, 'callback_color_picker'));
          }
     }

     public function callback_color_picker($value = '', $primaryKey = null, $field = null)
     {
          $name = $field->name;
          return "<input type='color' name='$name' value='$value' style='width:80px' />";
     }

     // Append additional fields to unset from CRUD
     protected function unset_crud_fields()
     {
          $args = func_get_args();
          if (isset($args[0]) && is_array($args[0])) {
               $args = $args[0];
          }
          $this->mCrudUnsetFields = array_merge($this->mCrudUnsetFields, $args);
     }

     // Initialize CRUD album via Image CRUD library
     // Reference: http://www.grocerycrud.com/image-crud
     protected function generate_image_crud($table, $url_field, $upload_path, $order_field = 'pos', $title_field = '')
     {
          // create CRUD object
          $this->load->library('Image_crud');
          $crud = new image_CRUD();
          $crud->set_table($table);
          $crud->set_url_field($url_field);
          $crud->set_image_path($upload_path);

          // [Optional] field name of image order (e.g. "pos")
          if (!empty($order_field)) {
               $crud->set_ordering_field($order_field);
          }

          // [Optional] field name of image caption (e.g. "caption")
          if (!empty($title_field)) {
               $crud->set_title_field($title_field);
          }

          // other custom logic to be done outside
          $this->mCrud = $crud;
          return $crud;
     }

     // Render CRUD
     protected function render_crud()
     {
          // logic specific for Grocery CRUD only
          $crud_obj_name = strtolower(get_class($this->mCrud));
          if ($crud_obj_name === 'grocery_crud') {
               $this->mCrud->unset_fields($this->mCrudUnsetFields);
          }

          // render CRUD
          $crud_data = $this->mCrud->render();

          // append scripts
          $this->add_stylesheet($crud_data->css_files, false);
          $this->add_script($crud_data->js_files, true, 'head');

          // display view
          $this->mViewData['crud_output'] = $crud_data->output;
          //$this->render_view('crud',$this->mViewData);
          $this->render('crud');
     }

     public function savegridstate()
     {
          //ld($this->input->get_post);
     }

     public function loadgridstate()
     {
          //ld($this->input->get_post);
     }
     //FOR JQGRID
     public function jdata($criteria_filter = '0')
     {

          if ($this->access['is_view'] == 0) {echo SiteHelpers::alert('error', ' You are not allowed to view this page');die;}
          $output = $this->load_jdata($criteria_filter);
          echo json_encode($output);
     }

     private function load_jdata($criteria_filter)
     {

          // Grid Configuration
          //$this->data['param'] = $params;
          $this->data['tableGrid'] = $this->info['config']['grid'];
          $this->data['tableForm'] = $this->info['config']['forms'];
          $this->data['setting']   = $this->info['setting'];

          // Group users permission
          $this->data['access'] = $this->access;
          $datacolumns          = array();
          foreach ($this->data['tableGrid'] as $k => $t):
               if ($t['view'] == '1'):
                    $datacolumns[] = array(
                         'name'            => $t['field'],
                         'text'            => $t['field'],
                         'datafield'       => $t['field'],
                         'columngroup'     => 'Default',
                         'width'           => null,
                         'align'           => 'center',
                         'cellsalign'      => 'center',
                         'cellsrenderer'   => null,
                         'sortable'        => true,
                         'filtercondition' => 'starts_with');
               endif;
          endforeach;
          $this->data['datacolumns'] = $datacolumns;

          $where          = "";
          $new_total_rows = 0;
          $pagenum        = $_GET['pagenum'];
          $pagenum        = $pagenum + 1;
          $pagesize       = (isset($_GET['pagesize'])) ? $_GET['pagesize'] : $this->info['setting']['perpage'];

          $start = $pagenum * $pagesize;
          if ($pagesize == 'All') {
               $pagesize = 0;
          }
          // filter data.
          if (isset($_GET['filterscount'])) {
               $filterscount = $_GET['filterscount'];
               if ($filterscount > 0) {
                    $where             = " AND (";
                    $tmpdatafield      = "";
                    $tmpfilteroperator = "";
                    $valuesPrep        = "";
                    $value             = [];
                    for ($i = 0; $i < $filterscount; $i++) {
                         // get the filter's value.
                         $filtervalue = $_GET["filtervalue" . $i];

                         // get the filter's condition.
                         $filtercondition = $_GET["filtercondition" . $i];
                         // get the filter's column.
                         $filterdatafield = $_GET["filterdatafield" . $i];
                         // get the filter's operator.
                         $filteroperator = $_GET["filteroperator" . $i];
                         if ($tmpdatafield == "") {
                              $tmpdatafield = $filterdatafield;
                         } else if ($tmpdatafield != $filterdatafield) {
                              $where .= ") AND (";
                         } else if ($tmpdatafield == $filterdatafield) {
                              if ($tmpfilteroperator == 0) {
                                   $where .= " AND ";
                              } else {
                                   $where .= " OR ";
                              }
                         }
                         switch ($filtercondition) {
                              case "CONTAINS":
                                   $condition    = " LIKE ";
                                   $value[0][$i] = $newvalue = "%{$filtervalue}%";
                                   $values[]     = &$value[0][$i];
                                   break;

                              case "DOES_NOT_CONTAIN":
                                   $condition    = " NOT LIKE ";
                                   $value[1][$i] = $newvalue = "%{$filtervalue}%";
                                   $values[]     = &$value[1][$i];
                                   break;

                              case "EQUAL":
                                   $condition    = " = ";
                                   $value[2][$i] = $newvalue = $filtervalue;
                                   $values[]     = &$value[2][$i];
                                   break;

                              case "NOT_EQUAL":
                                   $condition    = " <> ";
                                   $value[3][$i] = $newvalue = $filtervalue;
                                   $values[]     = &$value[3][$i];
                                   break;

                              case "GREATER_THAN":
                                   $condition    = " > ";
                                   $value[4][$i] = $newvalue = $filtervalue;
                                   $values[]     = &$value[4][$i];
                                   break;

                              case "LESS_THAN":
                                   $condition    = " < ";
                                   $value[5][$i] = $newvalue = $filtervalue;
                                   $values[]     = &$value[5][$i];
                                   break;

                              case "GREATER_THAN_OR_EQUAL":
                                   $condition    = " >= ";
                                   $value[6][$i] = $newvalue = $filtervalue;
                                   $values[]     = &$value[6][$i];
                                   break;

                              case "LESS_THAN_OR_EQUAL":
                                   $condition    = " <= ";
                                   $value[7][$i] = $newvalue = $filtervalue;
                                   $values[]     = &$value[7][$i];
                                   break;

                              case "STARTS_WITH":
                                   $condition    = " LIKE ";
                                   $value[8][$i] = $newvalue = "'$filtervalue%";
                                   $values[]     = &$value[8][$i];
                                   break;

                              case "ENDS_WITH":
                                   $condition    = " LIKE ";
                                   $value[9][$i] = $newvalue = "%{$filtervalue}'";
                                   $values[]     = &$value[9][$i];
                                   break;

                              case "NULL":
                                   $condition     = " IS NULL ";
                                   $value[10][$i] = $newvalue = "%{$filtervalue}%";
                                   $values[]      = &$value[10][$i];
                                   break;

                              case "NOT_NULL":
                                   $condition     = " IS NOT NULL ";
                                   $value[11][$i] = $newvalue = "%{$filtervalue}%";
                                   $values[]      = &$value[11][$i];
                                   break;
                         }

                         $where .= " " . $filterdatafield . $condition . "'" . $newvalue . "'";
                         $valuesPrep = $valuesPrep . "s";
                         if ($i == $filterscount - 1) {
                              $where .= ")";
                         }
                         $tmpfilteroperator = $filteroperator;
                         $tmpdatafield      = $filterdatafield;
                    }
               }
          }

          $sortfield = (isset($_GET['sortdatafield']) ? $_GET['sortdatafield'] : 'id');
          $sortorder = (isset($_GET['sortorder']) ? $_GET['sortorder'] : ' DESC ');

          if (!empty($criteria_filter)) {
               $where .= " AND ( " . $criteria_filter . " )";
          }
          //$page = max(1, (int) $start);
          $params = array(
               'page'   => $pagenum,
               'limit'  => $pagesize,
               'sort'   => $sortfield,
               'order'  => $sortorder,
               'params' => $where,
               'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),
          );

          $results = $this->model->getRows($params);

          $data[] = array(
               "TotalRows" => $results['total'],
               "Rows"      => array(),
          );

          foreach ($results['rows'] as $row_key => $row_val) {
               $col_array = array();
               $ctr       = 0;
               foreach ($row_val as $col_key => $col_val) {
                    $field = $this->data['tableGrid'][$ctr];
                    if ($field['view'] == '1'):
                         $conn  = (isset($field['conn']) ? $field['conn'] : array());
                         $value = $col_val;
                         //$value = AjaxHelpers::gridFormater($col_val, $row_val , $field['attribute'],$conn);
                         $col_array[$field['field']] = $value; // col_key  $value;
                    endif;
                    $ctr++;
               }
               //$col_array[] = AjaxHelpers::buttonAction('rewardscomparison',$this->data['access'] ,'1',$this->data['setting']);

               $data['Rows'][] = $col_array;
          }
          return $data;
     }

     public function modulefilterdata($moduleid)
     {
          return $this->info('filters');
     }


     public function load_data3($parameter = '')
     {
          if ($this->access['is_view'] == 0) {echo SiteHelpers::alert('error', ' You are not allowed to view this page');die;}
          // Filter sort and order for query
          $sOrder    = '';
          $sOrderdir = '';
          $Data      = $this->input->post('columns');

          if ($this->input->post('order')) {
               foreach ($this->input->post('order') as $key) {
                    if (!empty($sOrder)) {
                         $sOrder .= ',';
                    }

                    $sOrder = $Data[$key['column']]['data'];
                    if (!empty($sOrderdir)) {
                         $sOrderdir .= ',';
                    }

                    $sOrderdir = $key['dir'];
               }
          }
          if (empty($sOrder)) {
               $sOrder = $this->info['setting']['orderby'];
          }

          if (empty($sOrderdir)) {
               $sOrderdir = $this->info['setting']['ordertype'];
          }

          $sort       = (($this->input->get('sort', true) != null) ? $this->input->get('sort', true) : $this->info['setting']['orderby']);
          $order      = (($this->input->get('order', true) != null) ? $this->input->get('order', true) : $this->info['setting']['ordertype']);
          $rowperpage = (($this->input->get('rows', true) != null) ? $this->input->get('rows', true) : $this->info['setting']['perpage']);
          if ($rowperpage == 'All') {
               $rowperpage = 0;
          }

          $filter = (($this->input->get('search', true) != null) ? $this->input->get('search', true) : '');

          //$sort = ($this->input->get('sort', true) !='' ? $this->input->get('sort', true) :  $this->info['setting']['orderby']);
          //$order = ($this->input->get('order', true) !='' ? $this->input->get('order', true) :  $this->info['setting']['ordertype']);

          // End Filter sort and order for query
          // Filter Search for query
          $filter = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
          // End Filter Search for query

          $page   = max(1, (int) $this->input->get('page', 1));
          $params = array(
               'page'   => $page,
               'limit'  => $rowperpage,
               'sort'   => $sort,
               'order'  => $order,
               'params' => $filter,
               'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),
          );
          // Get Query
          $results = $this->model->getRows($params);

          // Build pagination setting
          $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;

          $this->data['rowData'] = $results['rows'];
          // Build Pagination

          $pagination = $this->paginatorajax(array(
               'total_rows' => $results['total'],
               'per_page'   => $params['limit'],
          ), $parameter);
          $this->data['pagination'] = $pagination;
          // Row grid Number
          $this->data['i'] = ($page * $params['limit']) - $params['limit'];
          // Grid Configuration
          $this->data['param']     = $params;
          $this->data['tableGrid'] = $this->info['config']['grid'];
          $this->data['tableForm'] = $this->info['config']['forms'];
          $this->data['colspan']   = SiteHelpers::viewColSpan($this->info['config']['grid']);
          $this->data['setting']   = $this->info['setting'];

          $this->data['total_rows'] = $results['total'];
          $this->data['page_from']  = (int) $page;
          $this->data['page_to']    = (int) ($page) + (int) $params['limit'];

          // Group users permission
          $this->data['access'] = $this->access;
          // Render into template

     }

     /**
      * Generates the LIMIT portion of the query
      *
      * @return mixed
      */
     private function get_paging()
     {
          $iStart  = $this->input->post('start');
          $iLength = $this->input->post('length');

          if ($iLength != '' && $iLength != '-1') {
               $this->db->limit($iLength, ($iStart) ? $iStart : 0);
          }

     }

     /**
      * Generates the ORDER BY portion of the query
      *
      * @return mixed
      */
     private function get_ordering()
     {

          $Data = $this->input->post('columns');

          if ($this->input->post('order')) {
               $sOrder = '';
          }

          foreach ($this->input->post('order') as $key) {
               if ($this->check_cType()) {
                    $this->db->order_by($Data[$key['column']]['data'], $key['dir']);
               } else {
                    $this->db->order_by($this->columns[$key['column']], $key['dir']);
               }
          }

     }

     /**
      * Generates a %LIKE% portion of the query
      *
      * @return mixed
      */
     private function get_filtering()
     {

          $mColArray = $this->input->post('columns');

          $sWhere  = '';
          $search  = $this->input->post('search');
          $sSearch = $this->db->escape_like_str(trim($search['value']));
          $columns = array_values(array_diff($this->columns, $this->unset_columns));

          if ($sSearch != '') {
               for ($i = 0; $i < count($mColArray); $i++) {
                    if ($mColArray[$i]['searchable'] == 'true' && !array_key_exists($mColArray[$i]['data'], $this->add_columns)) {
                         if ($this->check_cType()) {
                              $sWhere .= $this->select[$mColArray[$i]['data']] . " LIKE '%" . $sSearch . "%' OR ";
                         } else {
                              $sWhere .= $this->select[$this->columns[$i]] . " LIKE '%" . $sSearch . "%' OR ";
                         }
                    }
               }
          }

          $sWhere = substr_replace($sWhere, '', -3);
          return $sWhere;

     }


     public function load_ajaxdata($criteria_filter = '')
     {

          if ($this->access['is_view'] == 0) {echo SiteHelpers::alert('error', ' You are not allowed to view this page');die;}

          // Filter sort and order for query
          $start = ((isset($_POST['start'])) ? $_POST['start'] : 0);

          $sortcol = ((isset($_POST['order']['0']['column'])) ? $_POST['order']['0']['column'] : 0);
          $sortdir = ((isset($_POST['order']['0']['dir'])) ? $_POST['order']['0']['dir'] : 'asc');
          $length  = ((isset($_POST['length'])) ? $_POST['length'] : 10);

          //$sort = (($sortcol != null) ? $sortcol : $this->info['setting']['orderby']);
          $order      = (($sortdir != null) ? $sortdir : $this->info['setting']['ordertype']);
          $rowperpage = (($length != null) ? $length : $this->info['setting']['perpage']);

          if ($rowperpage == 'All') {
               $rowperpage = 0;
          }

          //COLUMN ORDERS
          $Data = $this->input->post('columns');
          if ($this->input->post('order')) {
               foreach ($this->input->post('order') as $key) {
                    if (!empty($sOrder)) {
                         $sOrder .= ',';
                    }

                    $sOrder = $Data[$key['column']]['data'];
                    if (!empty($sOrderdir)) {
                         $sOrderdir .= ',';
                    }

                    $sOrderdir = $key['dir'];
               }
          }
          if (empty($sOrder)) {
               $sOrder = $this->info['setting']['orderby'];
          }

          if (empty($sOrderdir)) {
               $sOrderdir = $this->info['setting']['ordertype'];
          }

          //SEARCH
          $search = ((isset($_POST['search']['value'])) ? trim($_POST['search']['value']) : '');

          //COLUMN FILTERS
          $ctr       = 0;
          $sortorder = '';
          $tableGrid = $this->info['config']['grid'];
          $criteria  = "";
          $criteria2 = "";
          
          foreach ($tableGrid as $k => $t):
               $tablename = $t['alias'];
               $fieldname = $t['field'];
               $prefix    = $tablename . '_';
               $fieldname = str_replace($prefix, "", $fieldname);                         

               if (isset($Data[$ctr]))
               {
                    $mcol = $Data[$ctr];
                    $mval = (isset($mcol['search']['value']) ? $mcol['search']['value'] : '');

                    if ($mcol['searchable'] && !empty($mval)) {
                         if (!empty($criteria)) {
                              $criteria .= " AND ";
                         }
                         $criteria .= $t['alias'] . '.' . $fieldname . " LIKE '" . $mval . "%'";
                    }

               }

               if (!empty($search)) {
                    if (!empty($criteria2)) {
                         $criteria2 .= " OR ";
                    }

                    $criteria2 .= $t['alias'] . '.' . $fieldname . " LIKE '$search%'";

               }
               if ($ctr == $sOrder) {
                    if (!empty($sortorder)) {
                         $sortorder .= ",";
                    }

                    $sortorder .= $t['alias'] . '.' . $fieldname;
               }               

               $ctr++;
          endforeach;

          $allcriteria = $criteria_filter;

          if (!empty($criteria)) {
               if (!empty($allcriteria)) {
                    $allcriteria .= " AND ";
               }

               $allcriteria .= " ( " . $criteria . " ) ";
          }
          if (!empty($criteria2)) {
               if (!empty($allcriteria)) {
                    $allcriteria .= " AND ";
               }

               $allcriteria .= " ( " . $criteria2 . " ) ";
          }

          $filter     = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
          $filtertype = ((isset($_GET['linkfilter'])) ? $_GET['linkfilter'] : '');

          if (empty($filtertype))
               $filtertype = ((isset($_POST['filtertype'])) ? $_POST['filtertype'] : '');

          $filtertype_criteria = (isset($this->model->get_Typefilter($filtertype)['where']) ? $this->model->get_Typefilter($filtertype)['where'] : '');

          if (!empty($filtertype_criteria)) {
               if (!empty($allcriteria)) {
                    $allcriteria .= " AND ";
               }

               $allcriteria .= " ( " . $filtertype_criteria . " ) ";
          }

          if (!empty($filter)) {
               if (!empty($allcriteria)) {
                    $allcriteria .= " AND ";
               }

               $allcriteria .= " ( " . $filter . " ) ";
          }
          if (!empty($allcriteria)) {
               $allcriteria = " AND (" . $allcriteria . ") ";
          }
//ld($allcriteria);
          //$search = $this->model->getAdditionalCriteria($search);

          $page   = max(1, (int) $start);
          $params = array(
               'page'   => $page,
               'limit'  => $rowperpage,
               'sort'   => $sortorder,
               'order'  => $sOrderdir,
               'params' => $allcriteria,
               'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),
          );

          // Get Query
          $this->model->filter = $allcriteria;
          $results             = $this->model->getRows($params);

          // Build pagination setting
          $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
          if (isset($results['rows'])) {
               $this->data['rowData'] = $results['rows'];
          } else if (!empty($results)) {
               $this->data['rowData'] = $results;
          }

          // Row grid Number
          $this->data['i'] = ($page * $params['limit']) - $params['limit'];

          // Grid Configuration
          $this->data['param']      = $params;
          $this->data['tableGrid']  = $this->info['config']['grid'];
          $this->data['tableForm']  = $this->info['config']['forms'];
          $this->data['colspan']    = SiteHelpers::viewColSpan($this->info['config']['grid']);
          $this->data['setting']    = $this->info['setting'];
          $this->data['total_rows'] = $results['total'];
          $this->data['page_from']  = (int) $page;
          $this->data['page_to']    = (int) ($page) + (int) $params['limit'];

          // Group users permission
          $this->data['access'] = $this->access;

          return array("rows" => $this->data['rowData'],
               "total"             => $results['total'],
               "filtered"          => $results['total'],
          );

     }

     public function load_ajaxdata11($criteria_filter = 'all')
     {

          if ($this->access['is_view'] == 0) {echo SiteHelpers::alert('error', ' You are not allowed to view this page');die;}

          // Filter sort and order for query
          $start = ((isset($_POST['start'])) ? $_POST['start'] : 0);

          $sortcol = ((isset($_POST['order']['0']['column'])) ? $_POST['order']['0']['column'] : 0);
          $sortdir = ((isset($_POST['order']['0']['dir'])) ? $_POST['order']['0']['dir'] : 'asc');
          $length  = ((isset($_POST['length'])) ? $_POST['length'] : 10);

          //$sort = (($sortcol != null) ? $sortcol : $this->info['setting']['orderby']);
          $order      = (($sortdir != null) ? $sortdir : $this->info['setting']['ordertype']);
          $rowperpage = (($length != null) ? $length : $this->info['setting']['perpage']);

          if ($rowperpage == 'All') {
               $rowperpage = 0;
          }

          $filter     = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
          $filtertype = ((isset($_POST['filtertype'])) ? $_POST['filtertype'] : '');
          $filter .= $this->model->get_Typefilter($filtertype);

          $search = ((isset($_POST['search']['value'])) ? $_POST['search']['value'] : '');
          //$search = $this->model->getAdditionalCriteria($search);

          $ctr       = 0;
          $tableGrid = $this->info['config']['grid'];
          $criteria  = "";
          foreach ($tableGrid as $k => $t):
               //if($t['view'] =='1'):
               $tablename = $t['alias'];
               $fieldname = $t['field'];
               $prefix    = $tablename . '_';
               $fieldname = str_replace($prefix, "", $fieldname);

               if (!empty($criteria)) {
                    $criteria .= " OR ";
               }

               $criteria .= $tablename . '.' . $fieldname . " LIKE '%$search%'";

               if ($ctr == $sortcol) {
                    $sort = $tablename . '.' . $fieldname;
               }
               $ctr++;
               //endif;
          endforeach;
          if (!empty($criteria)) {
               $search = " AND ( " . $criteria . " )";
          }

          $page   = max(1, (int) $start);
          $params = array(
               'page'   => $page,
               'limit'  => $rowperpage,
               'sort'   => $sort,
               'order'  => $order,
               'params' => $filter . $search,
               'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),
          );

          // Get Query
          $this->model->filter = $criteria_filter;
          $results             = $this->model->getRows($params);

          // Build pagination setting
          $page                  = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
          $this->data['rowData'] = $results['rows'];

          // Row grid Number
          $this->data['i'] = ($page * $params['limit']) - $params['limit'];

          // Grid Configuration
          $this->data['param']      = $params;
          $this->data['tableGrid']  = $this->info['config']['grid'];
          $this->data['tableForm']  = $this->info['config']['forms'];
          $this->data['colspan']    = SiteHelpers::viewColSpan($this->info['config']['grid']);
          $this->data['setting']    = $this->info['setting'];
          $this->data['total_rows'] = $results['total'];
          $this->data['page_from']  = (int) $page;
          $this->data['page_to']    = (int) ($page) + (int) $params['limit'];

          // Group users permission
          $this->data['access'] = $this->access;

          return array("rows" => $this->data['rowData'],
               "total"             => $results['total'],
               "filtered"          => $results['total'],
          );
     }


}
