<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class SB_Model extends CI_Model
{


    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    /**
     * The database connection object. Will be set to the default
     * connection. This allows individual models to use different DBs
     * without overwriting CI's global $this->db connection.
     */

    /**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
    protected $primaryKey = 'id';

    /**
     * Support for soft deletes and this model's 'deleted' key
     */
    protected $soft_delete = FALSE;
    protected $soft_delete_key = 'deleted';
    protected $_temporary_with_deleted = FALSE;
    protected $_temporary_only_deleted = FALSE;

    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */
    protected $before_create = array();
    protected $after_create = array();
    protected $before_update = array();
    protected $after_update = array();
    protected $before_get = array();
    protected $after_get = array();
    protected $before_delete = array();
    protected $after_delete = array();

    protected $callback_parameters = array();

    /**
     * Protected, non-modifiable attributes
     */
    protected $protected_attributes = array();

    /**
     * Relationship arrays. Use flat strings for defaults or string
     * => array to customise the class name and primary key
     */
    protected $belongs_to = array();
    protected $has_many = array();

    protected $_with = array();

    /**
     * An array of validation rules. This needs to be the same format
     * as validation rules passed to the Form_validation library.
     */
    protected $validate = array();

    /**
     * Optionally skip the validation. Used in conjunction with
     * skip_validation() to skip data validation for any future calls.
     */
    protected $skip_validation = FALSE;

    /**
     * By default we return our results as objects. If we need to override
     * this, we can, or, we could use the `as_array()` and `as_object()` scopes.
     */
    protected $return_type = 'object';
    protected $_temporary_return_type = NULL;

    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */

    /**
     * Initialise the model, tie into the CodeIgniter superobject and
     * try our best to guess the table name.
     */

	public $db_prefix_oc='oc';
	public $table;
    public $order = '';
	public $_limit         = '0';
	public $_max         = '10';
	public $_offset        ='';
	public $_setting     = array();
	public $_config        = array();
	public $_grid         = array();
	public $_forms         = array();

    private $log_activity = false;
    private $log_type = "";
    private $log_for = "";
    private $log_for_key = "";
    private $log_for2 = "";
    private $log_for_key2 = "";

	public $search_fields 	= array();

	public function __construct($_table = null) {
		parent::__construct();

		if ($_table)
		{
			$this->table = $_table;
        	$this->use_table($_table);
        }

        $this->load->helper('inflector');

        array_unshift($this->before_create, 'protect_attributes');
        array_unshift($this->before_update, 'protect_attributes');

        $this->_temporary_return_type = $this->return_type;

        if (empty($this->table))
        	$this->_fetch_table();
	}

    private function _fetch_table()
    {
        if ($this->table == NULL)
        {
            $this->table = plural(preg_replace('/(_m|_model)?$/', '', strtolower(get_class($this))));
        }
    }

    protected function use_table($table) {
        $this->table = $table;
    }

	public function getQueryRows(  $args )
	{
		$table = $this->table;
		$key = $this->primaryKey;

		extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> '1'
		), $args ));

		//$offset = ($page-1) * $limit ;
		$offset = $page-1 ;
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';
		$orderConditional = ($sort !='' && $order !='') ?  " ORDER BY {$sort} {$order} " : '';

		// Update permission global / own access new ver 1.1
		$table = $this->table;
		if($global == 0 )
				$params .= " AND {$table}.entry_by ='".$this->session->userdata('uid')."'";
		// End Update permission global / own access new ver 1.1

		$rows = array();
		$querysql = FormatHelper::validatesql($this->querySelect() . $this->queryWhere(). "
			{$params} ". $this->queryGroup() ." {$orderConditional}  {$limitConditional} ");
		$query = $this->db->query($querysql);
		$result = $query->result();
		$query->free_result();

		//GET THE ROW COUNT
		$pattern = '/(.*)limit (.*)/i';
		$new_sql = FormatHelper::validatesql($this->querySelect() . $this->queryWhere()." {$params} ". $this->queryGroup());
		$new_query = preg_replace ($pattern, '', $new_sql);
		$pattern = '/(.*)SELECT *(.*)/';
		$new_query = preg_replace ($pattern, 'SELECT ', $new_query);
		$new_query = preg_replace('~[\r\n]+~', ' ', $new_query);
		$new_query = str_replace("SELECT FROM", "SELECT '' FROM ", $new_query);
		$new_query = str_replace("SELECT  FROM", "SELECT '' FROM ", $new_query);
		$new_query = 'Select Count(*) AS res From (' . $new_query . ') v__dynamic;';
		$new_row = $this->db->query($new_query)->row();
		//$total = $new_row->res;


		if($key =='' ) { $key ='*'; } else { $key = $table.".".$key ; }
		$counter_select = preg_replace( '/[\s]*SELECT(.*)FROM/Usi', 'SELECT count('.$key.') as total FROM', $this->querySelect() );
		//echo 	$counter_select; exit;
		$query = $this->db->query( FormatHelper::validatesql($counter_select . $this->queryWhere()." {$params} ". $this->queryGroup()));
		$res = $query->result();
		if ($res)
		{
			$total = $res[0]->total;
		}
		$query->free_result();

		return $results = array('rows'=> $result , 'total' => $total);


	}

	public function getRows( $args ,$rowArray = 0 )
	{
		$table = $this->table;
		$key = $this->primaryKey;


		extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> '1'
		), $args ));


		//$offset = ($page-1) * $limit ;
		$offset = $page;
		if ($page == 1) {
			$offset = $page-1 ;
		}	
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';
		$orderConditional = ($sort !='' && $order !='') ?  " ORDER BY {$sort} {$order} " : '';

		// Update permission global / own access new ver 1.1
		$table = $this->table;
		if($global == 0 )
				$params .= " AND {$table}.entry_by ='".$this->session->userdata('uid')."'";
		// End Update permission global / own access new ver 1.1

		$rows = array();
		$msql = FormatHelper::validatesql($this->querySelect() . $this->queryWhere()."{$params} ". $this->queryGroup() ." {$orderConditional}  {$limitConditional} ");

		//l($msql);

		$query = $this->db->query($msql);

		if ($rowArray)
			$result = $query->row_array();
		else
			$result = $query->result();

		$query->free_result();

		// if($key =='' ) {
		// 	$key ='*';
		// } else {
		// 	if (empty($table))
		// 	{
		// 		$key = '0' ;
		// 	}
		// 	else
		// 	{
		// 		$key = $table.".".$key ;
		// 	}

		// }
		// if ($table == 'temp_downlines')
		// 	{ $key = '0' ; }

		// $counter_select = preg_replace( '/[\s]*SELECT(.*)FROM/Usi', 'SELECT count('.$key.') as total FROM', $this->querySelect() );
		// //echo 	$counter_select; exit;
		// $ssql = FormatHelper::validatesql($counter_select . $this->queryWhere()." {$params} ". $this->queryGroup());
		// $query = $this->db->query($ssql);
		// $res = $query->result();
		// $total=0;
		// if ($res)
		// {
		// 	$total = $res[0]->total;
		// }


		//GET THE ROW COUNT
		$pattern = '/(.*)limit (.*)/i';
		$new_sql = FormatHelper::validatesql($this->querySelect() . $this->queryWhere()." {$params} ". $this->queryGroup());
		$new_query = preg_replace ($pattern, '', $new_sql);
		//$pattern = '/(.*)SELECT *(.*)/';
		$new_query = preg_replace ($pattern, 'SELECT ', $new_query);
		$new_query = preg_replace('~[\r\n]+~', ' ', $new_query);
		$new_query = str_replace("SELECT FROM", "SELECT '' FROM ", $new_query);
		$new_query = str_replace("SELECT  FROM", "SELECT '' FROM ", $new_query);
		$new_query = 'Select Count(*) AS res From (' . $new_query . ') v__dynamic;';
		$new_row = $this->db->query($new_query)->row();
		$total = $new_row->res;
		//$total = 0;


		$query->free_result();
		return $results = array('rows'=> $result , 'total' => $total);

	}

	public function getRow( $id )
	{
		$table = $this->table;
		$key = $this->primaryKey;

		$query = $this->db->query( FormatHelper::validatesql($this->querySelect() .
			$this->queryWhere().
			" AND ".$table.".".$key." = '{$id}' ".
			$this->queryGroup())
		);
		$result = $query->row_array();
		return $result;
	}

	public function insertRow( $data , $id, $defaults = true)
	{
		$table = $this->table;
		$key = $this->primaryKey;

		if($id == NULL )
		{
			// die("test".$id);
			if($defaults) {
				$data=$this->set_defaults($data);
			}
			// Insert Here
			if(isset($data['createdOn'])) $data['createdOn'] = date("Y-m-d H:i:s");
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");
			$data[$this->primaryKey] = NULL;
			$this->db->insert($table,$data);
			$id = $this->db->insert_id();

		} else {
			// Update here
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");
			 $this->db->where( array( $key => $id ));
			 $this->db->update( $table , $data );
		}
		return $id;
	}
	public function set_defaults($data)
	{
		return $data;
	}

	function getComboselect( $params , $nested = array())
	{
		if(isset($params[3]) AND !empty($params[4]) ){
			$table = $params[0];
			$query = $this->db->get_where( $table , array( $param[3] => $param[4] ));
			$rows = $query->result();
		}else{
			$table = $params[0];
			$query = $this->db->get( $table );
			$rows = $query->result();
		}
		$query->free_result();
		return $rows;

	}

	function getColumnTable( $table )
	{
		$query = $this->db->query("SHOW COLUMNS FROM $table");
		$columns = array();
		foreach( $query->result() as $row)
			{
			$columns[$row->Field] = '';
			}
		return $columns;
	}

	function getTableList( $db )
	{
	 	$t = array();
		$dbname = 'Tables_in_'.$db ;
		$query = $this->db->query("SHOW TABLES FROM {$db}");
		foreach( $query->result() as $table)
		{
			$t[$table->$dbname] = $table->$dbname;
		}
		return $t;
	}

	function getTableField( $table )
	{
		$columns = array();
		$query = $this->db->query("SHOW COLUMNS FROM $table");
		foreach( $query->result() as $column){
			$columns[$column->Field] = $column->Field;
		}
		return $columns;
	}

	function getColoumnInfo( )
	{
		$coll = array();

		if( $this->db->dbdriver=='mysqli'){
			while ($field = mysqli_fetch_field($this->db->result_id ))
			{
				$coll[] = $field;
			}
		} else if( $this->db->dbdriver=='mysql'){
			while ($field = mysql_fetch_field($this->db->result_id))
			{
			$coll[] = $field;
			}
		}else{
			$coll = $this->db->list_fields();
		}
		return $coll;
	}

	function builColumnInfo( $statement )
	{
		$driver 	= $this->db->dbdriver;
		$db 		= $this->db->database;
		$dbuser 	= $this->db->username;
		$dbpass 	= $this->db->password;
		$dbhost 	= $this->db->hostname;

		$data = array();
		$mysqli = new mysqli($dbhost,$dbuser,$dbpass,$db);
		if ($result = $mysqli->query($statement)) {

			/* Get field information for all columns */
			while ($finfo = $result->fetch_field()) {
				$data[] = (object) array(
							'Field'	=> $finfo->name,
							'Table'	=> $finfo->table,
							'Type'	=> $finfo->type
							);
			}
			$result->close();
		}
		$mysqli->close();
		return $data;
	}

	function validAccess( $id)
	{

		if (is_staff_logged_in() && $this->session->userdata("logged_in_as_client"))
		{
     		$permissions = array(
              'is_global' => true,
              'is_view' => true,
              'is_detail' => true,
              'is_add' => true,
              'is_edit' => true,
              'is_remove'=> true,
              'is_excel'=> true
          );
			return $permissions;
		}

		$query = $this->db->get_where( 'main_shopfreemart.sfm_crm_rolepermissions', array(
		  'permissionid' => $id ,
			'roleid' => $this->session->userdata("gid"),
		));

		$row = $query->result();
		$query->free_result();

		if(count($row) >= 1)
		{
			$row = $row[0];

          $permissions = array(
              'is_global' => true,
              'is_view' => $row->can_view,
              'is_detail' => $row->can_view,
              'is_add' => $row->can_create,
              'is_edit' => $row->can_edit,
              'is_remove'=> $row->can_delete,
              'is_excel'=> $row->can_view
          );


		} else {
     		$permissions = array(
              'is_global' => true,
              'is_view' => true,
              'is_detail' => true,
              'is_add' => true,
              'is_edit' => true,
              'is_remove'=> true,
              'is_excel'=> true
          );
		}

		return $permissions;
	}

	function delete2( $where = array() ) {

		// if empty, nothing to do
		if( empty( $where )){
			return ;
		}

		$_where = array();
		foreach( $where as $fld => $val ){
			if( is_array( $val )){
				if( is_numeric( $val )){
					$_where[] = " `{$fld}` in ( ". implode(",", $val )." ) ";
				}else{
					$_where[] = " `{$fld}` in ( '". implode("','", $val )."' ) ";
				}
			} else {
				if( is_numeric( $val )){
					$_where[] = " `{$fld}` = {$val} ";
				}else{
					$_where[] = " `{$fld}` = '{$val}' ";
				}
			}
		}

		$sql = " DELETE FROM $this->table WHERE ( ". implode(' ) AND ( ', $_where )." ) ";

		if(!( $query = $this->db->query($sql))){
			return false;
		}

	}

	function destroy( $id ) {
		$where = array( $this->primaryKey => $id );
		$this->delete( $where );

	}

	// FOR DATATABLES

   private function _get_datatables_query()
    {

        $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
		    if ( is_array($order) )
	        {
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
	        else
	        {
	            $this->db->order_by($order);
	        }
        }
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $this->_get_custom_field();
        $query = $this->db->get();
        return $query->num_rows();
    }



    public function get_datatables()
    {

        $this->_get_datatables_query();
        $this->_get_custom_field();

        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();

        return $query->result();
    }



    public function _get_custom_field()
    {
       // if(isset($_POST['columns'][5]['search']['value']) and $_POST['columns'][5]['search']['value'] !=''){
       //     $this->db->where($this->table,$_POST['columns'][5]['search']['value']);
       // }
    }

    public function delete_by_id($id)
    {
        $this->db->where($primarykey, $id);
        $this->db->delete($this->table);
    }

	function get_string_between($string, $start, $end){
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}

    function getAdditionalCriteria($param){
    	if (!empty($param))
    	{
    	$i=0;
        foreach ($this->column_search as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {

                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

    	}
    	$where = $this->get_string_between($this->db->get_compiled_select(), "WHERE   (",")");
    	ld($where);
    	if (!empty($where))
        	return ' AND ('.$where.')';
        else
        	return '';
    }

    public function get_ajaxdatatables()
    {

    	$start =   ((isset($_POST['start'])) ? $_POST['start'] : 0);
		$sortcol= ((isset($_POST['order']['0']['column'])) ? $_POST['order']['0']['column'] : 0);

		$sortdir=((isset($_POST['order']['0']['dir'])) ? $_POST['order']['0']['dir'] : 'asc');
		$length=  ((isset($_POST['length'])) ? $_POST['length'] : $this->_max);
		$sort = (($sortcol != null) ? $sortcol: $this->order);
		$order = (($sortdir != null) ? $sortdir : '');
		$rowperpage = (($length != null) ? $length : 10);

		if ($rowperpage == 'All') $rowperpage = 0;

		$filter = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');

		//$filtertype = ((isset($_POST['filtertype'])) ? $_POST['filtertype'] : '');
		//$filter .= $this->model->getTypefilter($filtertype);

		$search = ((isset($_POST['search']['value'])) ? $_POST['search']['value'] : '');
        $tableGrid = $this->_grid ;
        //ld($_POST['columns']);
        $ctr = 0;
        $criteria = "";
		foreach ($tableGrid as $k => $t) :
			//if($t['view'] =='1'):
				if (!empty($criteria))  $criteria .= " OR ";
				$criteria .= $t['alias'].'.'.$t['field']." LIKE '$search%'";

				if ($ctr == $sortcol)
				{
					$sort =  $t['alias'].'.'.$t['field'];
				}
				$ctr++;
			//endif;
		endforeach;
		if (!empty($criteria))
		{
			$search = " AND ( ".$criteria." )";
		}

        $page = max(1, (int) $start);
        $params = array(
            'page'   => $page,
            'limit'  => $rowperpage,
            'sort'   => $sort + 1 ,
            'order'  => $order,
            'params' => $filter . $search,
            'global' => 1 //(isset($this->access['is_global']) ? $this->access['is_global'] : 1),
        );

		// $page = max(1, (int) $start);
		// $params = array(
		// 	'page'		=> $page ,
		// 	'limit'		=> $rowperpage ,
		// 	'sort'		=> $sort + 1 ,
		// 	'order'		=> $order,
		// 	'params'	=> $filter.$search,
		// 	'global'	=>  1
		// );

	// Get Query
		$results = $this->getRows( $params );
		return  $results;
    }


    protected function usetable($table) {
        $this->table = $table;
    }

    protected function init_activity_log($log_type = "", $log_type_title_key = "", $log_for = "", $log_for_key = 0, $log_for2 = "", $log_for_key2 = 0) {
        if ($log_type) {
            $this->log_activity = true;
            $this->log_type = $log_type;
            $this->log_type_title_key = $log_type_title_key;
            $this->log_for = $log_for;
            $this->log_for_key = $log_for_key;
            $this->log_for2 = $log_for2;
            $this->log_for_key2 = $log_for_key2;
        }
    }

    function get_one($id = 0) {
        return $this->get_one_where(array('id' => $id));
    }

    function get_one_where($where = array()) {
        $result = $this->db->get_where($this->table, $where, 1);
        if ($result->num_rows()) {
            return $result->row();
        } else {
            $db_fields = $this->db->list_fields($this->table);
            $fields = new stdClass();
            foreach ($db_fields as $field) {
                $fields->$field = "";
            }
            return $fields;
        }
    }

    function get_all2($include_deleted = false) {
        $where = array("deleted" => 0);
        if (!$include_deleted) {
            $where = array();
        }
        return $this->get_all_where($where);
    }

    function get_all_where($where = array(), $limit = 1000000, $offset = 0) {
        $where_in = get_array_value($where, "where_in");
        if ($where_in) {
            foreach ($where_in as $key => $value) {
                $this->db->where_in($key, $value);
            }
            unset($where["where_in"]);
        }
        return $this->db->get_where($this->table, $where, $limit, $offset);
    }

    function save($data = array(), $id = 0) {
        if ($id) {
            //update
            $where = array("id" => $id);

            //to log an activity we have to know the changes. now collect the data before update anything
            if ($this->log_activity) {
                $data_before_update = $this->get_one($id);
            }

            $success = $this->update_where($data, $where);
            if ($success) {
                if ($this->log_activity) {
                    //to log this activity, check the changes
                    $fields_changed = array();
                    foreach ($data as $field => $value) {
                        if ($data_before_update->$field != $value) {
                            $fields_changed[$field] = array("from" => $data_before_update->$field, "to" => $value);
                        }
                    }
                    //has changes? log the changes.
                    if (count($fields_changed)) {
                        $log_for_id = 0;
                        if ($this->log_for_key) {
                            $log_for_key = $this->log_for_key;
                            $log_for_id = $data_before_update->$log_for_key;
                        }

                        $log_for_id2 = 0;
                        if ($this->log_for_key2) {
                            $log_for_key2 = $this->log_for_key2;
                            $log_for_id2 = $data_before_update->$log_for_key2;
                        }

                        $log_type_title_key = $this->log_type_title_key;
                        $log_type_title = isset($data_before_update->$log_type_title_key) ? $data_before_update->$log_type_title_key : "";

                        $log_data = array(
                            "action" => "updated",
                            "log_type" => $this->log_type,
                            "log_type_title" => $log_type_title,
                            "log_type_id" => $id,
                            "changes" => serialize($fields_changed),
                            "log_for" => $this->log_for,
                            "log_for_id" => $log_for_id,
                            "log_for2" => $this->log_for2,
                            "log_for_id2" => $log_for_id2,
                        );
                        $this->Activity_logs_model->save($log_data);
                        $activity_log_id = $this->db->insert_id();
                        $data["activity_log_id"] = $activity_log_id;
                    }
                }
            }
            return $success;
        } else {
            //insert
            if ($this->db->insert($this->table, $data)) {
                $insert_id = $this->db->insert_id();
                if ($this->log_activity) {
                    //log this activity
                    $log_for_id = 0;
                    if ($this->log_for_key) {
                        $log_for_id = get_array_value($data, $this->log_for_key);
                    }

                    $log_for_id2 = 0;
                    if ($this->log_for_key2) {
                        $log_for_id2 = get_array_value($data, $this->log_for_key2);
                    }

                    $log_type_title = get_array_value($data, $this->log_type_title_key);
                    $log_data = array(
                        "action" => "created",
                        "log_type" => $this->log_type,
                        "log_type_title" => $log_type_title ? $log_type_title : "",
                        "log_type_id" => $insert_id,
                        "log_for" => $this->log_for,
                        "log_for_id" => $log_for_id,
                        "log_for2" => $this->log_for2,
                        "log_for_id2" => $log_for_id2,
                    );
                    $this->Activity_logs_model->save($log_data);
                    $activity_log_id = $this->db->insert_id();
                    $data["activity_log_id"] = $activity_log_id;
                }
                return $insert_id;
            }
        }
    }

    function update_where($data = array(), $where = array()) {
        if (count($where)) {
            if ($this->db->update($this->table, $data, $where)) {
                $id = get_array_value($where, "id");
                if ($id) {
                    return $id;
                } else {
                    return true;
                }
            }
        }
    }

    function delete($id = 0, $undo = false) {

        $this->trigger('before_delete', $id);
        $this->db->where($this->primaryKey, $id);

        if ($this->soft_delete)
        {
	        $data = array('deleted' => 1);
	        if ($undo === true) {
	            $data = array('deleted' => 0);
	        }
	        $success = $this->db->update($this->table, $data);
        }
        else
        {
            $success = $this->db->delete($this->table);
        }

        if ($success) {
            if ($this->log_activity) {
                if ($undo) {
                    // remove previous deleted log.
                    $this->Activity_logs_model->delete_where(array("action" => "deleted", "log_type" => $this->log_type, "log_type_id" => $id));
                } else {
                    //to log this activity check the title
                    $model_info = $this->get_one($id);
                    $log_for_id = 0;
                    if ($this->log_for_key) {
                        $log_for_key = $this->log_for_key;
                        $log_for_id = $model_info->$log_for_key;
                    }
                    $log_type_title_key = $this->log_type_title_key;
                    $log_type_title = $model_info->$log_type_title_key;
                    $log_data = array(
                        "action" => "deleted",
                        "log_type" => $this->log_type,
                        "log_type_title" => $log_type_title ? $log_type_title : "",
                        "log_type_id" => $id,
                        "log_for" => $this->log_for,
                        "log_for_id" => $log_for_id,
                    );
                    $this->Activity_logs_model->save($log_data);
                }
            }
        }
        return $success;
    }

    function get_dropdown_list($option_fields = array(), $key = "id", $where = array()) {
        $where["deleted"] = 0;
        $list_data = $this->get_all_where($where)->result();
        $result = array();
        foreach ($list_data as $data) {
            $text = "";
            foreach ($option_fields as $option) {
                $text.=$data->$option . " ";
            }
            $result[$data->$key] = $text;
        }
        return $result;
    }

    //prepare a query string to get custom fields like as a normal field
    protected function prepare_custom_field_query_string($related_to, $custom_fields, $related_totable) {

        $join_string = "";
        $select_string = "";
        $custom_field_valuestable = $this->db->dbprefix('custom_field_values');


        if ($related_to && $custom_fields) {
            foreach ($custom_fields as $cf) {
                $cf_id = $cf->id;
                $virtualtable = "cfvt_$cf_id"; //custom field values virtual table

                $select_string.=" , $virtualtable.value AS cfv_$cf_id ";
                $join_string .= " LEFT JOIN $custom_field_valuestable AS $virtualtable ON $virtualtable.related_to_type='$related_to' AND $virtualtable.related_to_id=$related_totable.id AND $virtualtable.deleted=0 AND $virtualtable.custom_field_id=$cf_id ";
            }
        }

        return array("select_string" => $select_string, "join_string" => $join_string);
    }





    /* --------------------------------------------------------------
     * CRUD INTERFACE
     * ------------------------------------------------------------ */

    /**
     * Fetch a single record based on the primary key. Returns an object.
     */
    public function get($primary_value)
    {
		return $this->get_by($this->primaryKey, $primary_value);
    }

    /**
     * Fetch a single record based on an arbitrary WHERE call. Can be
     * any valid value to $this->db->where().
     */
    public function get_by()
    {
        $where = func_get_args();

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->db->where($this->soft_delete_key, (bool)$this->_temporary_only_deleted);
        }

		$this->_set_where($where);

        $this->trigger('before_get');

        $row = $this->db->get($this->table)
                        ->{$this->_return_type()}();
        $this->_temporary_return_type = $this->return_type;

        $row = $this->trigger('after_get', $row);

        $this->_with = array();
        return $row;
    }

    /**
     * Fetch an array of records based on an array of primary values.
     */
    public function get_many($values)
    {
        $this->db->where_in($this->primaryKey, $values);

        return $this->get_all();
    }

    /**
     * Fetch an array of records based on an arbitrary WHERE call.
     */
    public function get_many_by()
    {
        $where = func_get_args();

        $this->_set_where($where);

        return $this->get_all();
    }

    /**
     * Fetch all the records in the table. Can be used as a generic call
     * to $this->db->get() with scoped methods.
     */
    public function get_all()
    {
        $this->trigger('before_get');

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->db->where($this->soft_delete_key, (bool)$this->_temporary_only_deleted);
        }

        $result = $this->db->get($this->table)
                           ->{$this->_return_type(1)}();
        $this->_temporary_return_type = $this->return_type;

        foreach ($result as $key => &$row)
        {
            $row = $this->trigger('after_get', $row, ($key == count($result) - 1));
        }

        $this->_with = array();
        return $result;
    }

    /**
     * Insert a new row into the table. $data should be an associative array
     * of data to be inserted. Returns newly created ID.
     */
    public function insert($data, $skip_validation = FALSE)
    {
        if ($skip_validation === FALSE)
        {
            $data = $this->validate($data);
        }

        if ($data !== FALSE)
        {
            $data = $this->trigger('before_create', $data);

            $this->db->insert($this->table, $data);
            $insert_id = $this->db->insert_id();

            $this->trigger('after_create', $insert_id);

            return $insert_id;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Insert multiple rows into the table. Returns an array of multiple IDs.
     */
    public function insert_many($data, $skip_validation = FALSE)
    {
        $ids = array();

        foreach ($data as $key => $row)
        {
            $ids[] = $this->insert($row, $skip_validation, ($key == count($data) - 1));
        }

        return $ids;
    }

    /**
     * Updated a record based on the primary value.
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $data = $this->trigger('before_update', $data);

        if ($skip_validation === FALSE)
        {
            $data = $this->validate($data);
        }

        if ($data !== FALSE)
        {
            $result = $this->db->where($this->primaryKey, $primary_value)
                               ->set($data)
                               ->update($this->table);

            $this->trigger('after_update', array($data, $result));

            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Update many records, based on an array of primary values.
     */
    public function update_many($primary_values, $data, $skip_validation = FALSE)
    {
        $data = $this->trigger('before_update', $data);

        if ($skip_validation === FALSE)
        {
            $data = $this->validate($data);
        }

        if ($data !== FALSE)
        {
            $result = $this->db->where_in($this->primaryKey, $primary_values)
                               ->set($data)
                               ->update($this->table);

            $this->trigger('after_update', array($data, $result));

            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Updated a record based on an arbitrary WHERE clause.
     */
    public function update_by()
    {
        $args = func_get_args();
        $data = array_pop($args);

        $data = $this->trigger('before_update', $data);

        if ($this->validate($data) !== FALSE)
        {
            $this->_set_where($args);
            $result = $this->db->set($data)
                               ->update($this->table);
            $this->trigger('after_update', array($data, $result));

            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Update all records
     */
    public function update_all($data)
    {
        $data = $this->trigger('before_update', $data);
        $result = $this->db->set($data)
                           ->update($this->table);
        $this->trigger('after_update', array($data, $result));

        return $result;
    }


    /**
     * Delete a row from the database table by an arbitrary WHERE clause
     */
    public function delete_by()
    {
        $where = func_get_args();

	    $where = $this->trigger('before_delete', $where);

        $this->_set_where($where);


        if ($this->soft_delete)
        {
            $result = $this->db->update($this->table, array( $this->soft_delete_key => TRUE ));
        }
        else
        {
            $result = $this->db->delete($this->table);
        }

        $this->trigger('after_delete', $result);

        return $result;
    }

    /**
     * Delete many rows from the database table by multiple primary values
     */
    public function delete_many($primary_values)
    {
        $primary_values = $this->trigger('before_delete', $primary_values);

        $this->db->where_in($this->primaryKey, $primary_values);

        if ($this->soft_delete)
        {
            $result = $this->db->update($this->table, array( $this->soft_delete_key => TRUE ));
        }
        else
        {
            $result = $this->db->delete($this->table);
        }

        $this->trigger('after_delete', $result);

        return $result;
    }


    /**
     * Truncates the table
     */
    public function truncate()
    {
        $result = $this->db->truncate($this->table);

        return $result;
    }

    /* --------------------------------------------------------------
     * RELATIONSHIPS
     * ------------------------------------------------------------ */

    public function with($relationship)
    {
        $this->_with[] = $relationship;

        if (!in_array('relate', $this->after_get))
        {
            $this->after_get[] = 'relate';
        }

        return $this;
    }

    public function relate($row)
    {
		if (empty($row))
        {
		    return $row;
        }

        foreach ($this->belongs_to as $key => $value)
        {
            if (is_string($value))
            {
                $relationship = $value;
                $options = array( 'primaryKey' => $value . '_id', 'model' => $value . '_model' );
            }
            else
            {
                $relationship = $key;
                $options = $value;
            }

            if (in_array($relationship, $this->_with))
            {
                $this->load->model($options['model'], $relationship . '_model');

                if (is_object($row))
                {
                    $row->{$relationship} = $this->{$relationship . '_model'}->get($row->{$options['primaryKey']});
                }
                else
                {
                    $row[$relationship] = $this->{$relationship . '_model'}->get($row[$options['primaryKey']]);
                }
            }
        }

        foreach ($this->has_many as $key => $value)
        {
            if (is_string($value))
            {
                $relationship = $value;
                $options = array( 'primaryKey' => singular($this->table) . '_id', 'model' => singular($value) . '_model' );
            }
            else
            {
                $relationship = $key;
                $options = $value;
            }

            if (in_array($relationship, $this->_with))
            {
                $this->load->model($options['model'], $relationship . '_model');

                if (is_object($row))
                {
                    $row->{$relationship} = $this->{$relationship . '_model'}->get_many_by($options['primaryKey'], $row->{$this->primaryKey});
                }
                else
                {
                    $row[$relationship] = $this->{$relationship . '_model'}->get_many_by($options['primaryKey'], $row[$this->primaryKey]);
                }
            }
        }

        return $row;
    }

    /* --------------------------------------------------------------
     * UTILITY METHODS
     * ------------------------------------------------------------ */

    /**
     * Retrieve and generate a form_dropdown friendly array
     */
    function dropdown()
    {
        $args = func_get_args();

        if(count($args) == 2)
        {
            list($key, $value) = $args;
        }
        else
        {
            $key = $this->primaryKey;
            $value = $args[0];
        }

        $this->trigger('before_dropdown', array( $key, $value ));

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->db->where($this->soft_delete_key, FALSE);
        }

        $result = $this->db->select(array($key, $value))
                           ->get($this->table)
                           ->result();

        $options = array();

        foreach ($result as $row)
        {
            $options[$row->{$key}] = $row->{$value};
        }

        $options = $this->trigger('after_dropdown', $options);

        return $options;
    }

    /**
     * Fetch a count of rows based on an arbitrary WHERE call.
     */
    public function count_by()
    {
        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->db->where($this->soft_delete_key, (bool)$this->_temporary_only_deleted);
        }

        $where = func_get_args();
        $this->_set_where($where);

        return $this->db->count_all_results($this->table);
    }

    /**
     * Fetch a total count of rows, disregarding any previous conditions
     */
    public function count_all()
    {

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->db->where($this->soft_delete_key, (bool)$this->_temporary_only_deleted);
        }
	$r = $this->db->count_all($this->table);

        return $r;
    }

    /**
     * Tell the class to skip the insert validation
     */
    public function skip_validation()
    {
        $this->skip_validation = TRUE;
        return $this;
    }

    /**
     * Get the skip validation status
     */
    public function get_skip_validation()
    {
        return $this->skip_validation;
    }

    /**
     * Return the next auto increment of the table. Only tested on MySQL.
     */
    public function get_next_id()
    {
        return (int) $this->db->select('AUTO_INCREMENT')
            ->from('information_schema.TABLES')
            ->where('TABLE_NAME', $this->table)
            ->where('TABLE_SCHEMA', $this->db->database)->get()->row()->AUTO_INCREMENT;
    }

    /**
     * Getter for the table name
     */
    public function table()
    {
        return $this->table;
    }

    /* --------------------------------------------------------------
     * GLOBAL SCOPES
     * ------------------------------------------------------------ */

    /**
     * Return the next call as an array rather than an object
     */
    public function as_array()
    {
        $this->_temporary_return_type = 'array';
        return $this;
    }

    /**
     * Return the next call as an object rather than an array
     */
    public function as_object()
    {
        $this->_temporary_return_type = 'object';
        return $this;
    }

    /**
     * Don't care about soft deleted rows on the next call
     */
    public function with_deleted()
    {
        $this->_temporary_with_deleted = TRUE;
        return $this;
    }

    /**
     * Only get deleted rows on the next call
     */
    public function only_deleted()
    {
        $this->_temporary_only_deleted = TRUE;
        return $this;
    }

    /* --------------------------------------------------------------
     * OBSERVERS
     * ------------------------------------------------------------ */

    /**
     * MySQL DATETIME created_at and updated_at
     */
    public function created_at($row)
    {
        if (is_object($row))
        {
            $row->created_at = date('Y-m-d H:i:s');
        }
        else
        {
            $row['created_at'] = date('Y-m-d H:i:s');
        }

        return $row;
    }

    public function updated_at($row)
    {
        if (is_object($row))
        {
            $row->updated_at = date('Y-m-d H:i:s');
        }
        else
        {
            $row['updated_at'] = date('Y-m-d H:i:s');
        }

        return $row;
    }

    /**
     * Serialises data for you automatically, allowing you to pass
     * through objects and let it handle the serialisation in the background
     */
    public function serialize($row)
    {
        foreach ($this->callback_parameters as $column)
        {
            $row[$column] = serialize($row[$column]);
        }

        return $row;
    }

    public function unserialize($row)
    {
        foreach ($this->callback_parameters as $column)
        {
            if (is_array($row))
            {
                $row[$column] = unserialize($row[$column]);
            }
            else
            {
                $row->$column = unserialize($row->$column);
            }
        }

        return $row;
    }

    /**
     * Protect attributes by removing them from $row array
     */
    public function protect_attributes($row)
    {
        foreach ($this->protected_attributes as $attr)
        {
            if (is_object($row))
            {
                unset($row->$attr);
            }
            else
            {
                unset($row[$attr]);
            }
        }

        return $row;
    }

    /* --------------------------------------------------------------
     * QUERY BUILDER DIRECT ACCESS METHODS
     * ------------------------------------------------------------ */

    /**
     * A wrapper to $this->db->order_by()
     */
    public function order_by($criteria, $order = 'ASC')
    {
        if ( is_array($criteria) )
        {
            foreach ($criteria as $key => $value)
            {
                $this->db->order_by($key, $value);
            }
        }
        else
        {
            $this->db->order_by($criteria, $order);
        }
        return $this;
    }

    /**
     * A wrapper to $this->db->limit()
     */
    public function limit($limit, $offset = 0)
    {
        $this->db->limit($limit, $offset);
        return $this;
    }

    /* --------------------------------------------------------------
     * INTERNAL METHODS
     * ------------------------------------------------------------ */

    /**
     * Trigger an event and call its observers. Pass through the event name
     * (which looks for an instance variable $this->event_name), an array of
     * parameters to pass through and an optional 'last in interation' boolean
     */
    public function trigger($event, $data = FALSE, $last = TRUE)
    {
        if (isset($this->$event) && is_array($this->$event))
        {
            foreach ($this->$event as $method)
            {
                if (strpos($method, '('))
                {
                    preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);

                    $method = $matches[1];
                    $this->callback_parameters = explode(',', $matches[3]);
                }

                $data = call_user_func_array(array($this, $method), array($data, $last));
            }
        }

        return $data;
    }

    /**
     * Run validation on the passed data
     */
    public function validate($data)
    {
        if($this->skip_validation)
        {
            return $data;
        }

        if(!empty($this->validate))
        {
            foreach($data as $key => $val)
            {
                $_POST[$key] = $val;
            }

            $this->load->library('form_validation');

            if(is_array($this->validate))
            {
                $this->form_validation->set_rules($this->validate);

                if ($this->form_validation->run() === TRUE)
                {
                    return $data;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                if ($this->form_validation->run($this->validate) === TRUE)
                {
                    return $data;
                }
                else
                {
                    return FALSE;
                }
            }
        }
        else
        {
            return $data;
        }
    }

    /**
     * Guess the primary key for current table
     */
    private function _fetch_primaryKey()
    {
        if($this->primaryKey == NULl)
        {
            $this->primaryKey = $this->db->query("SHOW KEYS FROM `".$this->table."` WHERE Key_name = 'PRIMARY'")->row()->Column_name;
        }
    }

    /**
     * Set WHERE parameters, cleverly
     */
    protected function _set_where($params)
    {
        if (count($params) == 1 && is_array($params[0]))
        {
            foreach ($params[0] as $field => $filter)
            {
                if (is_array($filter))
                {
                    $this->db->where_in($field, $filter);
                }
                else
                {
                    if (is_int($field))
                    {
                        $this->db->where($filter);
                    }
                    else
                    {
                        $this->db->where($field, $filter);
                    }
                }
            }
        }
        else if (count($params) == 1)
        {
            $this->db->where($params[0]);
        }
    	else if(count($params) == 2)
		{
            if (is_array($params[1]))
            {
                $this->db->where_in($params[0], $params[1]);
            }
            else
            {
                $this->db->where($params[0], $params[1]);
            }
		}
		else if(count($params) == 3)
		{
			$this->db->where($params[0], $params[1], $params[2]);
		}
        else
        {
            if (is_array($params[1]))
            {
                $this->db->where_in($params[0], $params[1]);
            }
            else
            {
                $this->db->where($params[0], $params[1]);
            }
        }
    }

    /**
     * Return the method name for the current return type
     */
    protected function _return_type($multi = FALSE)
    {
        $method = ($multi) ? 'result' : 'row';
        return $this->_temporary_return_type == 'array' ? $method . '_array' : $method;
    }



	function makeInfo( $id )
	{
		$query =  $this->db->get_where('sfm_cms_module', array('module_name'=> $id));
		$data = array();
		foreach($query->result() as $r)

		$query =  $this->db->get_where('sfm_cms_module', array('module_name'=> $id));
		$data = array();
		foreach($query->result() as $r)
		{
			$langs = (json_decode($r->module_lang,true));
			$data['id']		= $r->module_id;
			$data['title'] 	= SiteHelpers::infoLang($r->module_title,$langs,'title');
			$data['note'] 	= SiteHelpers::infoLang($r->module_note,$langs,'note');
			$data['table'] 	= $r->module_db;
			$data['key'] 	= $r->module_db_key;
			$data['config'] = SiteHelpers::CF_decode_json($r->module_config);
			$data['module_filters'] = get_module_filters($r->module_id);
			$field = array();
			usort($data['config']['grid'], "SiteHelpers::_sort");
			$datacolumns=array();
			if ($r->module_db != 'notable')
			{
				foreach($data['config']['grid'] as $fs)
				{
					if($fs['view'] =='1'):
						array_push($datacolumns, array(
							'header' =>  $fs['label'],
		                    'text' =>  $fs['field'],
		                    'datafield' => $fs['field'],
		                    'columngroup' =>  'Default',
		                    'cellsformat' =>$this->getCellformat($fs),
		                    'width' =>  $fs['width'],
		                    'cellsalign' => $fs['align'],
		                    'cellsrenderer' => null, //  $this->getCellrenderer($fs),
		                    'sortable' => $fs['sortable'],
		                    'filtercondition' =>  'starts_with',
		                    'predefined' => (isset($fs['attribute']['predefined']['value']) ? $fs['attribute']['predefined']['value'] : null),
		                    ));
					endif;

					foreach($fs as $f)
						$field[] = $fs['field'];
				}

				$data['field'] = $field;
				$data['setting'] = array(
					'gridtype'		=> (isset($data['config']['setting']['gridtype']) ? $data['config']['setting']['gridtype'] : 'native'  ),
					'orderby'		=> (isset($data['config']['setting']['orderby']) ? $data['config']['setting']['orderby'] : $r->module_db_key),
					'ordertype'		=> (isset($data['config']['setting']['ordertype']) ? $data['config']['setting']['ordertype'] : 'asc'  ),
					'perpage'		=> (isset($data['config']['setting']['perpage']) ? $data['config']['setting']['perpage'] : '10'  ),
					'frozen'		=> (isset($data['config']['setting']['frozen']) ? $data['config']['setting']['frozen'] : 'false'  ),
					'form-method'   => (isset($data['config']['setting']['form-method'])  ? $data['config']['setting']['form-method'] : 'native'  ),
					'view-method'   => (isset($data['config']['setting']['view-method'])  ? $data['config']['setting']['view-method'] : 'native'  ),
					'inline'        => (isset($data['config']['setting']['inline'])  ? $data['config']['setting']['inline'] : 'false'  ),
					'datatable'        => (isset($data['config']['setting']['datatable'])  ? $data['config']['setting']['datatable'] : 0  ),
					'gridsettings'        => (isset($data['config']['setting']['gridsettings'])  ? $data['config']['setting']['gridsettings'] : 'false'  ),
					'gridcolumns' => $datacolumns
				);

			}
		}
		$query->free_result();
		$this->_setting     = $data['setting'] ;
		$this->_config        = $data['config'];
		$this->_grid         = $data['config']['grid'] ;
		return $data;
	}

	function getCellformat($fs)
	{
		if (!$fs) return;
		$defined = (isset($fs['attribute']['predefined']['value']) ? $fs['attribute']['predefined']['value'] : null);
		switch ($defined) {
			case 'currency':
				$ret = "f2";
				break;
			case 'date':
				$ret = "d";
				break;

			default:
				$ret = null;
				# code...
				break;
		}
		return $ret;
	}

     public function get_linkfilters($module_id)
     {
          $sql = "SELECT id, title, description, count, rankfilters, sql_count FROM sfm_filters WHERE module_id = $module_id and linkfilter = 1";
          return db_get_results($sql);
     }


    public function get_Typefilter($filtertype='')
    {
     if (empty($filtertype))  return '';
        $sql = FormatHelper::validatesql("SELECT `where`,rankfilters FROM sfm_filters WHERE id = $filtertype");
        $row = db_get_rowarray($sql);
        return $row;
    }

// 	Formats:
	// $formats = array(
	// 		'date'	=> 'Date',
	// 		'image'	=> 'Image',
	// 		'link'	=> 'Link',
	// 		'email'	=> 'Email',
	// 		'phonelink'	=> 'Phone Link',
	// 		'currency'	=> 'Currency',
	// 		'checkbox'	=> 'Checkbox/Radio',
	// 		'radio'	=> 'Radio',
	// 		'file'	=> 'Files',
	// 		'function'=> 'Function',
	// 		'database'	=> 'Database',
	// 		'avatar'	=> 'Avatar',
	// 		'userlinktooltip'=> 'Userlink w/Tooltip',
	// 		'orderstatus'=> 'Order Status',
	// 		'status'=> 'Status',
	// 		'custom'	=> 'Custom',
	// 	);
// 		Number format strings:
// "d" - decimal numbers.
// "f" - floating-point numbers.
// "n" - integer numbers.
// "c" - currency numbers.
// "p" - percentage numbers.

// For adding decimal places to the numbers, add a number after the formatting string.
// For example: "c3" displays a number in this format $25.256
// Built-in Date formats:

// // short date pattern
// "d" - "M/d/yyyy",
// // long date pattern
// "D" - "dddd, MMMM dd, yyyy",
// // short time pattern
// "t" - "h:mm tt",
// // long time pattern
// "T" - "h:mm:ss tt",
// // long date, short time pattern
// "f" - "dddd, MMMM dd, yyyy h:mm tt",
// // long date, long time pattern
// "F" - "dddd, MMMM dd, yyyy h:mm:ss tt",
// // month/day pattern
// "M" - "MMMM dd",
// // month/year pattern
// "Y" - "yyyy MMMM",
// // S is a sortable format that does not vary by culture
// "S" - "yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss"

// Date format strings:

// "d"-the day of the month;
// "dd"-the day of the month;
// "ddd"-the abbreviated name of the day of the week;
// "dddd"- the full name of the day of the week;
// "h"-the hour, using a 12-hour clock from 1 to 12;
// "hh"-the hour, using a 12-hour clock from 01 to 12;
// "H"-the hour, using a 24-hour clock from 0 to 23;
// "HH"- the hour, using a 24-hour clock from 00 to 23;
// "m"-the minute, from 0 through 59;
// "mm"-the minutes,from 00 though59;
// "M"- the month, from 1 through 12;
// "MM"- the month, from 01 through 12;
// "MMM"-the abbreviated name of the month;
// "MMMM"-the full name of the month;
// "s"-the second, from 0 through 59;
// "ss"-the second, from 00 through 59;
// "t"- the first character of the AM/PM designator;
// "tt"-the AM/PM designator;
// "y"- the year, from 0 to 99;
// "yy"- the year, from 00 to 99;
// "yyy"-the year, with a minimum of three digits;
// "yyyy"-the year as a four-digit number;
// "yyyyy"-the year as a four-digit number.


		function appendToSelectStr() {
				return NULL;
		}

		function fromTableStr() {
			return 'store s';
		}

	    function joinArray(){
	    	return NULL;
	    }

    	function whereClauseArray(){
    		return NULL;
    	}



}

?>