<?php 
//if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller  extends SB_Controller
{
	public $limit_page = 10;
	public function __construct()
	{

        parent::__construct();

        $this->Authentication_model->autologin();
        
        $this->backend_init();

        //Load admin template and theme
        $this->site_theme = $this->config->item("backend_theme");
        $this->config->setSiteTheme($this->site_theme);
        
	}



}
