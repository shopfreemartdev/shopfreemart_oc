<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| COPYRIGHT NOTICE
| Copyright 2008 JROX Technologies, Inc.  All Rights Reserved.
| -------------------------------------------------------------------------
| This script may be only used and modified in accordance to the license
| agreement attached (license.txt) except where expressly noted within
| commented areas of the code body. This copyright notice and the
| comments above and below must remain intact at all times.  By using this
| code you agree to indemnify JROX Technologies, Inc, its corporate agents
| and affiliates from any liability that might arise from its use.
|
| Selling the code for this program without prior written consent is
| expressly forbidden and in violation of Domestic and International
| copyright laws.
|
| -------------------------------------------------------------------------
| FILENAME - module_payment_gateway_paypalpro_lang.php
| -------------------------------------------------------------------------
|
| This is the language file the greenbyphone gateway
|
*/

$lang['paypal_pro'] = 'paypal pro payment gateway';
$lang['module_payment_gateway_paypalpro_gateway_url'] = 'paypal gateway URL';
$lang['module_payment_gateway_paypalpro_api_username'] = 'client ID';
$lang['module_payment_gateway_paypalpro_api_password'] = 'API password';
$lang['module_payment_gateway_paypalpro_signature'] = 'API signature';
$lang['module_payment_gateway_paypalpro_currency_code'] = 'currency_code';
$lang['module_payment_gateway_paypalpro_require_cvv'] = 'require CVV';
$lang['module_payment_gateway_paypalpro_enable_debug_email'] = 'enable debug email';
$lang['desc_module_payment_gateway_paypalpro_gateway_url'] = 'paypal gateway URL';
$lang['desc_module_payment_gateway_paypalpro_api_username'] = 'client ID';
$lang['desc_module_payment_gateway_paypalpro_api_password'] = 'API password';
$lang['desc_module_payment_gateway_paypalpro_signature'] = 'API signature';
$lang['desc_module_payment_gateway_paypalpro_currency_code'] = 'currency_code';
$lang['desc_module_payment_gateway_paypalpro_require_cvv'] = 'require CVV';
$lang['desc_module_payment_gateway_paypalpro_enable_debug_email'] = 'enable debug email';
$lang['pp_cc_number'] = 'credit card number';
$lang['pp_cc_type'] = 'credit card type';
$lang['pp_cvv_code'] = 'cvv code';
$lang['pp_expiration_date'] = 'expiration date';
?>