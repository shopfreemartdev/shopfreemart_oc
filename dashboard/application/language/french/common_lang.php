<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| AVIS DE DROIT D\'AUTEUR
| Copyright 2008 JROX Technologies, Inc.  Tous droits de reproduction réservés.
| -------------------------------------------------------------------------
| Ce script ne pourra être utilisé et modifié que conformément à l\'accord de licence
  y contenu (license.txt) exception faite des endroits où cela a été expressément mentionné
| dans les espaces commentés du code interne. Cet avis de droit d\'auteur ainsi que 
  les commentaires énoncés ci-dessous et ci-dessus doivent à tout moment rester intacts. 
| En utilisant ce code vous acceptez de dégager JROX Technologies, Inc, ses employés,agents
| et affiliés de toute responsabilité qui surviendrait du fait de son utilisation.
|
|
| La vente du code de ce proramme sans permission écrite préalable est
| formellement interdite et est en violation des lois nationales et internationales matière
| de droit d\'auteur.
|
|
| -------------------------------------------------------------------------
| FILENAME - common_lang.php
| -------------------------------------------------------------------------
|
| Ceci représente le fichier des termes usuels
Traduit de l\'Anglais par Mathurin Youmssi cleanwaxcam@yahoo.fr
|
|
*/

$lang['UPS_1DA'] = 'Next Day Air';
$lang['UPS_1DAL'] = 'Next Day Air Letter';
$lang['UPS_1DAPI'] = 'Next Day Air Intra (Puerto Rico)';
$lang['UPS_1DM'] = 'Next Day Air Early AM';
$lang['UPS_1DML'] = 'Next Day Air Early AM Letter';
$lang['UPS_1DP'] = 'Next Day Air Saver';
$lang['UPS_1DPL'] = 'Next Day Air Saver Letter';
$lang['UPS_2DA'] = '2nd Day Air';
$lang['UPS_2DAL'] = '2nd Day Air Letter';
$lang['UPS_2DM'] = '2nd Day Air Early AM';
$lang['UPS_2DML'] = '2nd Day Air AM Letter';
$lang['UPS_3DS'] = '3 Day Select';
$lang['UPS_GND'] = 'Ground';
$lang['UPS_GNDCOM'] = 'Ground Commercial';
$lang['UPS_GNDRES'] = 'Ground Residential';
$lang['UPS_STD'] = 'Canada Standard';
$lang['UPS_XDM'] = 'Worldwide Express Plus';
$lang['UPS_XDML'] = 'Worldwide Express Plus Letter';
$lang['UPS_XPD'] = 'Worldwide Expedited';
$lang['UPS_XPR'] = 'Worldwide Express';
$lang['UPS_XPRL'] = 'worldwide Express Letter';
$lang['USPS_EXPRESS'] = 'Express Mail';
$lang['USPS_FIRST CLASS'] = 'First Class';
$lang['USPS_LIBRARY'] = 'Library Mail';
$lang['USPS_MEDIA'] = 'Media Mail';
$lang['USPS_PARCEL'] = 'Parcel Post';
$lang['USPS_BPM'] = 'Bound Printed Matter (BPM)';
$lang['USPS_PRIORITY'] = 'Priority Mail';
$lang['about_us'] = 'A propos de nous';
$lang['account_confirmation'] = 'confirmation de compte';
$lang['account_confirmation_invalid'] = 'confirmation compte invalide';
$lang['account_confirmation_required'] = 'confirmation compte obligatoire.  veuilleur vérifier votre boîte email et suivez les instructions';
$lang['account_details'] = 'détails compte';
$lang['account_info'] = 'info compte';
$lang['account_information'] = 'information compte';
$lang['account_successfully_confirmed'] = 'votre compte a été confirmé avec succès';
$lang['account_updated_successfully'] = 'compte mis à jour avec succès';
$lang['actions'] = 'actions';
$lang['active'] = 'actif';
$lang['add_admin'] = 'ajouter admin';
$lang['add_admin_success'] = 'admin ajouté avec succès';
$lang['add_administrator'] = 'ajoutez administrateur';
$lang['add_aff_ad_categories_success'] = 'catégorie d\'annonce d\'affiliation ajoutée avec succès';
$lang['add_bookmark'] = 'marquez cette page';
$lang['add_cart_for_price'] = 'ajouter au panier pour voir le prix';
$lang['add_items_to_cart'] = 'ajouter articles au panier';
$lang['add_member'] = 'ajouter membre';
$lang['add_new_ticket'] = 'ajoutez nouvelle requête';
$lang['add_product'] = 'ajoutez produit';
$lang['add_review'] = 'ajoiutez une revue';
$lang['add_support_ticket'] = 'ajoutez une requête destinée au support';
$lang['add_to_cart'] = 'ajoutez au panier';
$lang['add_to_profile_recommended'] = 'ce produit va être ajouté à votre liste de produits recommandés.  cliquez okay pour continuer...';
$lang['added_by'] = 'ajouté(e) par';
$lang['added_on'] = 'ajouté le';
$lang['address_1'] = 'adresse 1';
$lang['address_2'] = 'adresse 2';
$lang['address_information'] = 'information adresse';
$lang['admin_alerts'] = 'alertes à l\'admin';
$lang['admin_email_taken'] = 'adresse email de l\'admin déjà pris';
$lang['admin_id'] = 'id admin';
$lang['admin_information'] = 'information admin';
$lang['admin_name'] = 'nom admin';
$lang['admin_photo'] = 'photo admin';
$lang['admin_quick_links'] = 'liens rapides admin';
$lang['admin_response_by'] = 'réponse de l\'admin par';
$lang['admin_username_taken'] = 'identifiant de l\'admin pris';
$lang['admin_users'] = 'utilisateurs admin';
$lang['aff_ad_categories'] = 'catégories d\'annonces d\'affiliation';
$lang['aff_ad_categories_add'] = 'ajouter une catégorie d\'annonces d\'affiliation';
$lang['aff_ad_categories_update'] = 'mettre à jour une catégorie d\'annonces d\'affiliation';
$lang['affiliate_activation'] = 'activation de l\'affilié';
$lang['affiliate_account_activated'] = 'Votre compte d\'affilié a été activé.  Vous pouvez gagner de grosses commissions en recommandant nos produits';
$lang['affiliate_account_activation'] = 'activation compte affilié';
$lang['affiliate_admin_approval_required'] = 'votre compte nécessite l\'approbation de l\'administrateur.  Vous serez notifié une fois votre compte approuvé';
$lang['affiliate_coupons'] = 'coupons d\'affiliation';
$lang['affiliate_groups'] = 'groupes d\'affiliation';
$lang['affiliate_info'] = 'info affilié';
$lang['affiliate_information'] = 'information affilié';
$lang['affiliate_payments'] = 'paiements d\'affilié';
$lang['affiliate_program'] = 'programme d\'affiliation';
$lang['affiliate_signup_bonus_note'] = 'bonus d\'inscription';
$lang['agree_with_tos'] = 'termes d\'utilisation du service';
$lang['agree_with_tos_yes'] = 'J\'accepte tous les termes et conditions d\'utilisation du service';
$lang['agree_with_tos_no'] = 'Je n\'accepte pas les termes et conditions d\'utilisation du service';
$lang['alert_downline_signup'] = 'alerter en cas d\'enregistrement d\'un filleul';
$lang['alert_new_commission'] = 'alerter en cas de nouvelle commission';
$lang['alert_on_failed_admin_login'] = 'alerter en cas d\'échec de connexion de l\'admin';
$lang['alert_on_new_affiliate_commission'] = 'alerter cas de nouvelle commission d\'affiliation';
$lang['alert_on_new_affiliate_signup'] = 'alerter en cas d\'enregistrement d\'un nouvel affilié';
$lang['alert_on_new_customer_order'] = 'alerter en cas de commande d\'un nouveau client';
$lang['alert_on_store_inventory'] = 'alerter sur l\'état des stocks';
$lang['alertpay_id'] = 'ID AlertPay';
$lang['alertpay_id_taken'] = 'ID alertpay pris';
$lang['all_categories'] = 'aucun(e)';
$lang['all_regions'] = 'toutes les régions';
$lang['allowed'] = 'autorisé(e)';
$lang['alphabetically'] = 'par odre alphabétique';
$lang['already_have_an_account_with_us'] = 'disposez-vous déjà d\'un compte chez nous?';
$lang['amount'] = 'montant';
$lang['amount_paid'] = 'montant payé';
$lang['apply'] = 'appliquez';
$lang['apply_coupon'] = 'appliquez le coupon';
$lang['approved'] = 'approuvé';
$lang['are_you_sure_you_want_to_delete'] = 'êtes-vous sûr de vouloir supprimer?';
$lang['are_you_sure_you_want_to_delete_this_user'] = 'êtes-vous sûr de vouloir supprimer cet utilisateur ?';
$lang['article_ad'] = 'article publicitaire';
$lang['article_ads'] = 'articles publicitaires';
$lang['attach_file'] = 'attachez le fichier';
$lang['authorize_net'] = 'Authorize.Net';
$lang['balance_due'] = 'solde dû';
$lang['bank_transfer'] = 'virement bancaire';
$lang['banner'] = 'bannière';
$lang['banners'] = 'bannières';
$lang['banners_rotator'] = 'bannière rotative';
$lang['banners_rotator_code'] = 'code de bannière rotative';
$lang['banners_rotator_description'] = 'copier le code de bannière rotative pour faire rôter notre bannière sur votre site';
$lang['be_the_first_review'] = 'soyez le premier à rédiger une revue pour ce produit';
$lang['billing_info'] = 'info facturation';
$lang['billing_information'] = 'information facturation';
$lang['blog'] = 'blog';
$lang['brands'] = 'marques';
$lang['browser'] = 'navigateur';
$lang['buy_now'] = 'achetez maintenant';
$lang['by'] = 'by';
$lang['calculating_shipping_costs'] = 'Veuillez patienter.  Calcul des frais de port en cours...';
$lang['cannot_delete_superadmin'] = 'vous ne pouvez supprimer l\'administrateur principal';
$lang['captcha_verification_code'] = 'mettez le code';
$lang['cardholder_name'] = 'nom du détenteur de la carte';
$lang['cart'] = 'panier';
$lang['cart_is_empty'] = 'votre panier est vide';
$lang['cart_totals'] = 'total panier';
$lang['cat_id'] = 'id catégorie';
$lang['cat_name'] = 'nom de la catégorie';
$lang['cat_status'] = 'statut de la catégorie';
$lang['categories'] = 'catégories';
$lang['category'] = 'catégorie';
$lang['category_id'] = 'ID catégorie';
$lang['category_name'] = 'nom de la catégorie';
$lang['category_name_taken'] = 'nom de la catégorie pris';
$lang['cc'] = 'cc';
$lang['cc_month'] = 'mois de la carte de crédit';
$lang['cc_number'] = 'Numéro de la carte de crédit';
$lang['cc_year'] = 'année carte de crédit';
$lang['change_permissions_for'] = 'changez les autorisations pour';
$lang['check'] = 'cochez';
$lang['check_email_settings'] = 'vérifiez les paramètres d\'email';
$lang['check_uncheck'] = 'cochez / décochez';
$lang['checkout'] = 'caisse';
$lang['checkout_check_your_account'] = 'veuillez vérifier votre compte pour consulter le statut de la commande';
$lang['checkout_free_trial'] = 'essai gratuit';
$lang['checkout_invoice'] = 'facture';
$lang['checkout_recurring_transaction'] = 'transaction récurrente';
$lang['checkout_thank_you_order'] = 'Merci.  votre commande a été reçue.';
$lang['city'] = 'ville';
$lang['click_here'] = 'cliquez ici';
$lang['click_here_activate'] = 'cliquez ici pour activer votre compte';
$lang['click_here_forwarding'] = 'cliquez ici si vous n\'êtes pas redirigé automatiquement';
$lang['click_here_to_continue'] = 'cliquez ici pour continuer';
$lang['click_here_to_go_back'] = 'cliquez ici pour retourner';
$lang['click_here_to_login'] = 'cliquez ici pour vous connecter';
$lang['click_image_view'] = 'cliquez sur une image pour avoir une vue agrandie';
$lang['click_on_each_to_view_downline'] = 'cliquez sur chaque filleul pour voir son propre réseau de  filleuls';
$lang['click_subscribe'] = 'cliquez ici pour sosucrire';
$lang['click_to_add_administrator'] = 'cliquez pour ajouter un administrateur';
$lang['click_to_forward'] = 'please wait... vous allez être redirigé automatiquement.';
$lang['click_to_login'] = 'cliquez pour vous connecter';
$lang['click_to_login_to_account'] = 'cliquez ici pour vous connecter à votre compte';
$lang['click_to_pay'] = 'cliquez pour jouer';
$lang['click_to_pay_invoice'] = 'cliquez pour payer cette facture';
$lang['click_to_proceed_to_membership'] = 'cliquez pour vous connecter à l\'adhésion souscrite que vous avez souscrite';
$lang['click_to_register'] = 'cliquez ici pour vous enregistrer';
$lang['click_view_commissions'] = 'cliquez pour voir les commissions';
$lang['click_view_invoices'] = 'cliquez pour voir les factures';
$lang['click_view_payments'] = 'cliquez pour voir les paiements d\'affiliés';
$lang['click_view_videos'] = 'cliquez pour regarder chaque vidéo';
$lang['clicks'] = 'clics';
$lang['close'] = 'clore';
$lang['close_ticket'] = 'clore la requête';
$lang['closed_tickets'] = 'requêtes closes';
$lang['comment'] = 'commentaire';
$lang['comment_posted_successfully'] = 'commentaire posté avec succès';
$lang['comment_waiting_moderation'] = 'votre commentaire attend d\'être modéré';
$lang['comments'] = 'commentaires';
$lang['commission_id'] = 'ID';
$lang['commission_level'] = 'niveau';
$lang['commission_notes'] = 'notes sur commission';
$lang['commission_reversal'] = 'rétraction de commission';
$lang['commission_total'] = 'total';
$lang['commissions'] = 'commissions';
$lang['company'] = 'société';
$lang['completed'] = 'traité(e)';
$lang['confirm_password'] = 'confirmez mot de passe';
$lang['confirmation_payment'] = 'confirmation et paiement';
$lang['contact_me'] = 'me contacter';
$lang['contact_us'] = 'nous contacter';
$lang['content'] = 'contenu';
$lang['content_categories'] = 'catégories de contenu';
$lang['content_feed'] = 'pourvoyeur de contenu';
$lang['continue'] = 'continuez >> ';
$lang['continue_shopping'] = 'continuez à faire vos courses';
$lang['could_not_add_admin'] = 'n\'a pu ajouter l\'admin';
$lang['could_not_add_comment'] = 'n\'a pu ajouter de commentaire';
$lang['could_not_add_member'] = 'n\'a pu ajouter de membre';
$lang['could_not_connect_gateway'] = 'n\'a pu connecter au terminal de paiement';
$lang['could_not_delete_admin'] = 'n\'a pu supprimer l\'admin';
$lang['could_not_delete_image'] = 'n\'a pu supprimer l\'image';
$lang['could_not_delete_member'] = 'n\'a pu supprimer le membre';
$lang['could_not_send_email'] = 'n\'a pu envoyer l\'email';
$lang['could_not_send_email'] = 'n\'a pu envoyer l\'email';
$lang['could_not_send_invoice'] = 'n\'a pu envoyer la facture';
$lang['could_not_update_admin'] = 'n\'a pu mettre à jour l\'admin';
$lang['could_not_update_aff_ad_category'] = 'n\'a pu mettre à jour la catégorie d\'annonce d\'affiliation';
$lang['could_not_update_member'] = 'n\'a pu mettre à jour le membre';
$lang['could_not_upload_image'] = 'n\'a pu attacher l\'image';
$lang['could_not_upload_photo'] = 'n\'a pu attacher la photo';
$lang['country'] = 'pays';
$lang['country'] = 'pays';
$lang['coupon'] = 'coupon';
$lang['coupon_applied_successfully'] = 'coupon appliqué avec succès';
$lang['coupon_code'] = 'code coupon';
$lang['coupon_code_text'] = 'Entrez votre code de coupon si vous en avez un.  Un seul coupon est autorisé par commande';
$lang['coupon_discount'] = 'coupon de rabais';
$lang['coupon_expired'] = 'coupon expiré';
$lang['coupons'] = 'coupons';
$lang['create_account'] = 'créez un compte';
$lang['create_acount_text'] = 'créez-vous un compte aujourd\'hui pour pouvoir accéder à de nombreuses fonctions sur notre site. faites vos paiements en toute facilité et accédez aux outils de promotion et de marketing disponibles dans notre espace membre.';
$lang['create_support_ticket'] = 'soumettre une requête au service de support et d\'assistance';
$lang['credit'] = 'crédit';
$lang['credit_overpayment_note'] = 'trop-perçu chez l\'utilisateur';
$lang['currencies'] = 'devises';
$lang['current_photo'] = 'photo actuelle';
$lang['custom_id'] = 'ID défini';
$lang['custom_id_taken'] = 'ID spécifié pris';
$lang['custom_information'] = 'information définie';
$lang['custom_url_link'] = 'lien d\'URL défini';
$lang['cvv_code'] = 'code CVV';
$lang['dashboard'] = 'tableau de bord';
$lang['dashboard'] = 'tableau de bord';
$lang['dashboard_account_details'] = 'compte<br /> details';
$lang['dashboard_affiliate_payments'] = 'paiements d\'affiliés';
$lang['dashboard_affiliate_signup_link'] = 'lien de référencement/parrainage';
$lang['dashboard_closed_tickets'] = 'requêtes closes';
$lang['dashboard_commissions'] = 'affiliate <br /> commissions';
$lang['dashboard_content'] = 'membres <br /> contenu';
$lang['dashboard_coupons'] = 'boutique<br />coupons';
$lang['dashboard_downline'] = 'voir <br />filleul';
$lang['dashboard_downloads'] = 'produit <br /> téléchargements';
$lang['dashboard_general_affiliate_link'] = 'lien de parrainage général';
$lang['dashboard_invoices'] = 'factures &<br/> commandes';
$lang['dashboard_mailing_lists'] = 'listes de diffusion';
$lang['dashboard_marketing'] = 'marketing <br /> outils';
$lang['dashboard_memberships'] = 'current <br />souscriptions d\'adhésion';
$lang['dashboard_open_tickets'] = 'requêtes ouvertes';
$lang['dashboard_paid_commissions'] = 'commissions payées';
$lang['dashboard_paid_invoices'] = 'factures réglées';
$lang['dashboard_payments'] = 'affiliés <br /> paiements';
$lang['dashboard_quickstats'] = 'stats sommaires';
$lang['dashboard_reports'] = 'rapports & <br /> stats';
$lang['dashboard_support'] = 'support <br /> requêtes';
$lang['dashboard_support_tickets'] = 'requêtes adressées au support';
$lang['dashboard_traffic'] = 'parrainnage <br /> traffic';
$lang['dashboard_unpaid_commissions'] = 'commissions impayées';
$lang['dashboard_unpaid_invoices'] = 'factures impayées';
$lang['date'] = 'date';
$lang['date_paid'] = 'date de paiement';
$lang['date_purchased'] = 'date d\'achat';
$lang['day_of_month'] = 'jour du mois';
$lang['day_s'] = 'jour(s)';
$lang['delete'] = 'supprimer';
$lang['delete_admin'] = 'supprimer admin';
$lang['delete_admin_success'] = 'admin supprimé avec succès';
$lang['delete_administrator'] = 'supprimer administrateur';
$lang['delete_aff_ad_categories_success'] = 'catégorie d\'annonce de l\'affilié supprimée avec succès';
$lang['delete_bookmark'] = 'supprimez des bookmarks';
$lang['delete_member'] = 'supprimez le membre';
$lang['delete_photo'] = 'supprimez la photo';
$lang['delete_product'] = 'supprimez le produit';
$lang['delete_profile_recommended'] = 'ceci va enlever ce produit de la liste des produits recommandés.  Cliquez okay pour continuer...';
$lang['deleted'] = 'supprimé(e)';
$lang['desc_alert_on_failed_admin_login'] = 'envoyer une aletre email à l\'administrateur lorsque quelqu\'un se connecte avec succès à l\'espace admin';
$lang['desc_alert_on_new_affiliate_commission'] = 'envoyer à l\'admin une alerte email lorsqu\'un nouvel affilié génère une commission';
$lang['desc_alert_on_new_affiliate_signup'] = 'envoyer à l\'admin une alerte email lorsqu\'un nouvel affilié s\'enregistre au programme d\'affiliation';
$lang['desc_alert_on_new_customer_order'] = 'envoyer à l\'admin une alerte email lorsqu\'un nouveau client fait une commande';
$lang['desc_alert_on_store_inventory'] = 'envoyer une alerte email à l\'admin lorsque le niveau des stocks est bas';
$lang['desc_confirm_password'] = 'confirmez le mot de passe';
$lang['desc_enable_custom_url'] = 'redirigez ce lien d\'affiliation vers l\'URL défini';
$lang['desc_enter_address_1'] = 'mettez l\'adresse';
$lang['desc_enter_address_2'] = 'mettez l\'adresse';
$lang['desc_enter_alertpay_id'] = 'mettez l\'ID AlertPay';
$lang['desc_enter_bank_transfer'] = 'mettez les cordoonées de virement bancaire de l\'affilié ici';
$lang['desc_enter_city'] = 'mettez le nom de la ville';
$lang['desc_enter_company'] = 'mettez le nom de la société';
$lang['desc_enter_country'] = 'sélectionez le pays de la liste suivante';
$lang['desc_enter_custom_id'] = 'si vous utilisez un ID de paiement spécifique, mettez-le ici';
$lang['desc_enter_custom_url_link'] = 'mettez toute l\'adresse URL en commençant par http://';
$lang['desc_enter_fax'] = 'mettez le numéro de fax';
$lang['desc_enter_first_name'] = 'mettez le prénom';
$lang['desc_enter_home_phone'] = 'mettez le numéro de téléphone du domicile';
$lang['desc_enter_last_name'] = 'mettez le nom';
$lang['desc_enter_mobile_phone'] = 'mettez le numéro du mobile';
$lang['desc_enter_moneybookers_id'] = 'mettez l\'ID de moneybookers';
$lang['desc_enter_payment_name'] = 'mettez le nom utilisé pour le paiement';
$lang['desc_enter_paypal_id'] = 'mettez l\'adresse email paypal';
$lang['desc_enter_postal_code'] = 'mettez le code zip ou postal';
$lang['desc_enter_safepay_id'] = 'mettez l\'ID de safepay';
$lang['desc_enter_sms_contact'] = 'si vous voulez être alerté par sms, mettez l\'email équivalent aux alertes sms. Par exemple 2125551212@my.att.net';
$lang['desc_enter_sponsor'] = 'mettez l\'identifiant du sponsor qui a parrainné ce membre';
$lang['desc_enter_state'] = 'mettez l\'Etat/la Province/Région';
$lang['desc_enter_valid_email'] = 'entrez une adresse email valide';
$lang['desc_enter_website'] = 'mettez son site d\'affilié';
$lang['desc_enter_work_phone'] = 'mettez le numéro de téléphone professionnel';
$lang['desc_payment_name'] = 'mettez le nom de l\'utilisateur pour les commissions';
$lang['desc_photo_file'] = 'nom du fichier photo s\'il y en a';
$lang['desc_required_password_length'] = 'les caractères sont obligatoires pour le mot de passe';
$lang['desc_required_username_length'] = 'les caractères sont obligatoires pour l\'dentifiant';
$lang['desc_rows_per_page'] = 'vous pouvez spécifier le le nombre de rangées de données à montrer par défaut';
$lang['desc_select_affiliate_group'] = 'sélectionnez le groupe d\'affiliation pour cet utilisateur';
$lang['desc_select_discount_group'] = 'sélectionnez le groupe bénéficiant de rabais pour cet utilisateur';
$lang['desc_select_email_mailing_lists'] = 'sélectionnez les listes de diffusion auxquelles cet utilisateur est inscrit';
$lang['desc_select_membership_products'] = 'sélectionnez les produits d\'adhésion dont cet utilisateur va être membre';
$lang['desc_select_payment_preference'] = 'sélectionnez la préférence de paiement pour cet utilisateur';
$lang['desc_select_permissions_for_admin'] = 'sélectionnez les autorisations que vous ne voulez PAS ACCORDER à cet admin';
$lang['desc_send_sms_alerts'] = 'envoyez un sms en même temps que l\'alerte email correspondante';
$lang['desc_send_welcome_email'] = 'envoyez l\'email de bienvenue avec ses coordonnées de connexion à l\'utilisateur';
$lang['desc_send_welcome_email'] = 'envoyez au membre l\'email de bienvenue et ses coordonnées de connexion';
$lang['desc_status_user'] = 'mettez cet utilisateur en mode actif ou inactif';
$lang['desc_upload_photo'] = 'attachez une image photo image à cet utilisateur si vous voulez';
$lang['description'] = 'description';
$lang['design'] = 'design';
$lang['details'] = 'details';
$lang['disable'] = 'désactiver';
$lang['discount_amount'] = 'montant de rabais';
$lang['discount_groups'] = 'groupes bénéficiant de rabais';
$lang['discounts'] = 'rabais';
$lang['discounts_credits'] = 'rabais / crédits';
$lang['download'] = 'téléchargement';
$lang['downloads'] = 'téléchargements';
$lang['download_expired'] = 'téléchargement expiré';
$lang['download_file_1'] = 'cliquez pour télécharger le fichier 1';
$lang['download_file_10'] = 'cliquez pour télécharger le fichier 10';
$lang['download_file_2'] = 'cliquez pour télécharger le fichier 2';
$lang['download_file_3'] = 'cliquez pour télécharger le fichier 3';
$lang['download_file_4'] = 'cliquez pour télécharger le fichier 4';
$lang['download_file_5'] = 'cliquez pour télécharger le fichier 5';
$lang['download_file_6'] = 'cliquez pour télécharger le fichier 6';
$lang['download_file_7'] = 'cliquez pour télécharger le fichier';
$lang['download_file_8'] = 'cliquez pour télécharger le fichier';
$lang['download_file_9'] = 'cliquez pour télécharger le fichier';
$lang['downloads'] = 'téléchargements';
$lang['downloads_allowed'] = 'autorisé(e)';
$lang['downloads_left'] = 'gauche';
$lang['due_date'] = 'date de réclamation';
$lang['edit'] = 'éditez';
$lang['edit_details_for'] = 'éditez les details pour';
$lang['email'] = 'email';
$lang['email_ad'] = 'email publicitaire';
$lang['email_address'] = 'adresse';
$lang['email_admin'] = 'email admin';
$lang['email_ads'] = 'email publicitaires';
$lang['email_body'] = 'texte de l\'email';
$lang['email_downline'] = 'envoyez l\'email au filleul';
$lang['email_queued_successfully'] = 'email mis avec succès en file d\'attente';
$lang['email_sent_successfully'] = 'email envoyé avec succès';
$lang['embed_code'] = 'code intégré';
$lang['enable'] = 'autorisez';
$lang['enable_custom_url'] = 'autorisez l\'URL défini';
$lang['enter_coupon_code'] = 'entrez le code du coupon';
$lang['enter_discount_code'] = 'entrez code de rabais';
$lang['enter_search_here'] = 'mettez votre recherche ici';
$lang['error'] = 'erreur';
$lang['error_404'] = 'Oops!  la page que vous recherhez est inexistante.  veuillez cliquer l\'un des liens ci-dessous pour voir le site';
$lang['error_page_not_found'] = 'page introuvable';
$lang['equivalent_to'] = 'converti à';
$lang['estimate_shipping'] = 'taxes et frais de port estimatifs';
$lang['estimate_shipping_text'] = 'sélectionnez votre destination pour avoir une estimation des taxes/frais de port';
$lang['every'] = 'chaque';
$lang['expiration_date'] = 'Date d\'expiration';
$lang['expires'] = 'expire';
$lang['faq_feed'] = 'entrées faq';
$lang['faqs'] = 'FAQs';
$lang['FAQ_categories'] = 'Catégories FAQ';
$lang['fax'] = 'fax';
$lang['featured_products'] = 'produits VIP';
$lang['featured_products_feed'] = 'entrée produits VIP';
$lang['features'] ='fonctions';
$lang['file_attachments'] = 'fichiers attachés';
$lang['finances'] = 'finances';
$lang['first'] = 'premier';
$lang['first_name'] = 'prénom';
$lang['flat'] = 'forfaitaire';
$lang['for'] = 'pour';
$lang['free'] = 'gratuit';
$lang['free_email_accounts_not_allowed'] = 'les comptes d\'email gratuits ne sont pas autorisés.  veuillez utiliser le compte d\'adresse email que vous a assigné votre fournisseur d\'accès internet';
$lang['free_trial'] = 'esssai gratuit';
$lang['frequently_asked_questions_faqs'] = 'Foire aux questions (FAQs)';
$lang['from'] = 'de';
$lang['get_code'] = 'obtenez le code';
$lang['get_feed'] = 'obtenez un feed';
$lang['get_quote'] = 'obtenez une quotation';
$lang['go'] = 'soumettez';
$lang['go_back'] = '<< retournez';
$lang['go_up_one_level'] = 'montez d\'un niveau';
$lang['grand_total'] = 'total général';
$lang['group'] = 'groupe';
$lang['group_discount'] = 'rabais groupé';
$lang['group_membership_required_to_view'] = 'une souscription d\'adhésion de groupe est obligatoire pour accéder au contenu';
$lang['groups'] = 'groupes';
$lang['has_maximum_quantity_of'] = 'possède  une quantité maximum de';
$lang['hello'] = 'salut';
$lang['high'] = 'haut';
$lang['home'] = 'accueil';
$lang['home_phone'] = 'téléphone domicile';
$lang['html_ads'] = 'annonces HTML';
$lang['id'] = 'id';
$lang['image_deleted_successfully'] = 'image supprimée avec succès';
$lang['image_uploaded_successfully'] = 'image attachée avec succès';
$lang['in_stock'] = 'en stock';
$lang['inactive'] = 'inactif';
$lang['installments'] = 'installation(s)';
$lang['invalid_card_number'] = 'numéro de carte invalide';
$lang['invalid_coupon'] = 'coupon invalide';
$lang['invalid_data'] = 'données invalides';
$lang['invalid_data_entered'] = 'les données entrées sont invalides';
$lang['invalid_demo_payment_sent'] = 'démo de paiement envoyé invalide';
$lang['invalid_email'] = 'votre email est invalide';
$lang['invalid_email_address'] = 'adresse email invalide';
$lang['invalid_email_username'] = 'email invalide';
$lang['invalid_gateway_configuration'] = 'configuration du terminal invalide';
$lang['invalid_login'] = 'vos cordonnées de connexion sont invalides';
$lang['invalid_password'] = 'mot de passe';
$lang['invalid_permissions'] = 'vous n\'avez pas les autorisations nécessaires pour accéder à cette ressource';
$lang['invalid_product'] = 'produit invalide';
$lang['invalid_security_code'] = 'code de sécurité invalide';
$lang['invalid_security_hash'] = 'hash de sécurité invalide';
$lang['invalid_sponsor'] = 'sponsor invalide';
$lang['invalid_username'] = 'identifiant invalide';
$lang['invalid_verification_code'] = 'code de vérification invalide';
$lang['invoice'] = 'facture';
$lang['invoice_amount'] = 'montant facture';
$lang['invoice_details'] = 'détails facture';
$lang['invoice_emailed_successfully'] = 'facture envoyée avec succès';
$lang['invoice_id'] = 'ID facture';
$lang['invoice_marked_paid'] = 'cette facture a été marquée comme déjà réglée';
$lang['invoice_notes'] = 'notes sur factures';
$lang['invoice_total'] = 'total facture';
$lang['invoices'] = 'factures';
$lang['ip_address'] = 'Adresse IP';
$lang['item_in_cart'] = 'article';
$lang['items_in_cart'] = 'articles';
$lang['join_our_aff_program'] = 'voulez-vous rejoindre notre programme d\'affiliation?';
$lang['language'] = 'langue';
$lang['last'] = 'dernier(e)';
$lang['last_login'] = 'dernière connexion';
$lang['last_login_date'] = 'date dernière connexion';
$lang['last_login_date_ip'] = 'date et adresse IP dernière connexion';
$lang['last_login_ip'] = 'IP dernière connexion';
$lang['last_name'] = 'prénom';
$lang['left_ad_block'] = 'bloc d\'annonces à gauche';
$lang['level_s'] = 'niveau(x)';
$lang['licensed_to'] = 'licence attribuée à';
$lang['link_to_product'] = 'lien vers ce produit';
$lang['links'] = 'liens';
$lang['list_product_categories'] = 'listez les catégories de produits';
$lang['loading...'] = 'Veuillez patienter. chargement en cours...';
$lang['login'] = 'connexion';
$lang['login_for_price'] = 'connexion pour accéder au prix';
$lang['login_required'] = 'connexion obligatoire';
$lang['login_to_account'] = 'connectez-vous au compte';
$lang['login_to_comment'] = 'vous devez vous connecter ou vous enregistrer pour poster un commentaire';
$lang['login_to_review'] = 'vous devez vous connecter ou vous enregistrer pour poster une revue';
$lang['logout'] = 'déconnexion';
$lang['low'] = 'bas';
$lang['mailing_list_subscription'] = 'souscription à la liste de diffusion';
$lang['mailing_lists'] = 'listes de diffusion';
$lang['main_categories'] = 'catégories principales';
$lang['main_product_categories'] = 'catégories principales de produit';
$lang['manage_admin'] = 'gérez l\'admin';
$lang['manage_admins'] = 'gérez les admins';
$lang['manage_aff_ad_categories'] = 'gérez les catégories d\'annonces d\'affiliation';
$lang['manage_member'] = 'gérez le membre';
$lang['manage_members'] = 'gérez les membres';
$lang['manufacturer'] = 'fournisseur/fabricant';
$lang['manufacturers'] = 'fourniseurs/fabricants';
$lang['mark_checked_as'] = 'marquez ce qui est coché comme';
$lang['marketing'] = 'marketing';
$lang['marketing_tools'] = 'outils de marketing';
$lang['maximum_download_reached'] = 'nombre de téléchargements maximum atteint';
$lang['maximum_quantity_allowed'] = 'quantité maximum autorisée';
$lang['maximum_redemption_used'] = 'nombre d\'utilisation maximum du coupon atteint';
$lang['member'] = 'membre';
$lang['member_details'] = 'détails du membre';
$lang['member_email_taken'] = 'adresse email déjà enregistrée. Veuillez cliquer sur le lien de connexion pour réinitialiser votre mot de passe';
$lang['member_information'] = 'information membre';
$lang['member_login'] = 'connexion membre';
$lang['member_photo'] = 'photo membre';
$lang['member_photos'] = 'photos membres';
$lang['member_quick_links'] = 'liens sommaires membre';
$lang['member_reports'] = 'rapports membre';
$lang['member_response_by'] = 'réponse au membre par';
$lang['member_stats'] = 'stats membre';
$lang['member_tab_custom_field_1'] = 'champ défini 1';
$lang['member_tab_custom_field_10'] = 'champ défini 10';
$lang['member_tab_custom_field_2'] = 'champ défini 2';
$lang['member_tab_custom_field_3'] = 'champ défini 3';
$lang['member_tab_custom_field_4'] = 'champ défini 4';
$lang['member_tab_custom_field_5'] = 'champ défini 5';
$lang['member_tab_custom_field_6'] = 'champ défini 6';
$lang['member_tab_custom_field_7'] = 'champ défini 7';
$lang['member_tab_custom_field_8'] = 'champ défini 8';
$lang['member_tab_custom_field_9'] = 'champ défini 9';
$lang['member_tab_custom_fields'] = 'champs définis';
$lang['member_username_taken'] = 'identifiant déjà pris';
$lang['members'] = 'membres';
$lang['members_area'] = 'espace membre';
$lang['members_content'] = 'contenu membres';
$lang['members_dashboard'] = 'tableau de bord membres';
$lang['membership'] = 'adhésion';
$lang['memberships'] = 'adhésions';
$lang['membership_info'] = 'info adhésion';
$lang['membership_information'] = 'information d\'adhésion';
$lang['membership_products'] = 'produits d\'adhésion';
$lang['membership_required_to_view'] = 'ce contenu n\'est accessible qu\'avec un certain type de souscription d\'adhésion';
$lang['membership_registration'] = 'souscription d\'adhésion';
$lang['memberships'] = 'adhésions';
$lang['message'] = 'message';
$lang['minimum_order_not_reached'] = 'le volume de commandes minimum n\'a pas été atteint';
$lang['minimum_quantity_required'] = 'quantité minimum requise';
$lang['mobile_phone'] = 'téléphone mobile';
$lang['moneybookers_id'] = 'ID Moneybookers';
$lang['moneybookers_id_taken'] = 'ID moneybookers pris ID';
$lang['month_s'] = 'mois';
$lang['monthly_click_stats_by_day'] = 'stats clics mensuels de référencement/ jour';
$lang['monthly_commission_stats_by_day'] = 'stats commissions mensuelles/ jour';
$lang['monthly_sales_stats_by_day'] = 'stats de ventes mensuelles par parrainage/ jour';
$lang['more_articles'] = 'plus d\'articles';
$lang['more_featured_products'] = 'plus de produits VIP';
$lang['more_info'] = 'plus d\'info';
$lang['my_account'] = 'mon compte';
$lang['my_website'] = 'mon site web';
$lang['name'] = 'nom';
$lang['new_products'] = 'nouveaux produits';
$lang['new_products_feed'] = 'entrée nouveaux produits';
$lang['no'] = 'auun';
$lang['no_articles_found'] = 'aucun article trouvé';
$lang['no_categories_found'] = 'aucune catégorie trouvée';
$lang['no_comments'] = 'aucun  commentaire';
$lang['no_commissions_found'] = 'aucune commission trouvée';
$lang['no_content_found'] = 'aucun contenu trouvé';
$lang['no_coupons_found'] = 'aucun coupon trouvé';
$lang['no_currencies_found'] = 'aucune devise trouvée';
$lang['no_data_found'] = 'données introuvables';
$lang['no_downloads_found'] = 'téléchargements introuvables';
$lang['no_featured_products_home_page'] = '<strong>No Featured Products Selected</strong><br /><br />Pour ajouter un produit VIP, sélectionnez-le dans votre espace Admin et mettez-le en VIP à partir du box';
$lang['no_invoices_found'] = 'aucune facture trouvée';
$lang['no_items_in_cart'] = 'votre panier est vide';
$lang['no_manufacturers_found'] = 'aucun fournisseur/fabricant trouvé';
$lang['no_memberships_found'] = 'aucune souscription d\'adhésion trouvée';
$lang['no_products'] = 'aucun produit';
$lang['no_products_found'] = 'aucun produit trouvé';
$lang['no_reports_found'] = 'aucun rapport trouvé';
$lang['no_shipping_available'] = 'aucune expédition disponible';
$lang['no_tickets_found'] = 'aucune requête trouvée';
$lang['no_tools_found'] = 'aucun outil marketing trouvé';
$lang['no_traffic_found'] = 'aucun traffic trouvé';
$lang['no_uploaded_file_found'] = 'aucun fichier attaché trouvé';
$lang['none'] = 'aucun(e)';
$lang['normal'] = 'normal';
$lang['not_listed'] = 'Etat non listé';
$lang['note_code'] = 'copiez et collez le code suivant dans votre page web ou dans votre email de promotion';
$lang['off_at_checkout'] = 'après remise à la caisse';
$lang['off_total_order_at_checkout'] = 'remise sur commande totale à la caisse';
$lang['offline_order_form_link'] = 'cliquez pour télécharger le bon de commande hors site';
$lang['one_time'] = 'une fois';
$lang['open_tickets'] = 'requêtes ouvertes';
$lang['order_completed'] = 'terminé(e)';
$lang['order_information'] = 'information commande';
$lang['order_instructions'] = 'mettez les instructions spéciales ou les commentaires';
$lang['order_pending'] = 'en attente';
$lang['order_processing'] = 'en cours de traitement';
$lang['order_status'] = 'statut de la commande';
$lang['orders'] = 'commandes';
$lang['os'] = 'OS';
$lang['out_of_stock'] = 'stock épuisé';
$lang['overview'] = 'aperçu';
$lang['pagination_next'] = '&gt';
$lang['pagination_prev'] = '&lt';
$lang['paid'] = 'paid';
$lang['paid_commissions'] = 'commissions payées';
$lang['paid_invoices'] = 'factures réglées';
$lang['password'] = 'mot de passe';
$lang['password_reset_successfully'] = 'votre mot de passe a été réinitialisé.  veuillez vérifier votre boîte email';
$lang['pay'] = 'payez';
$lang['pay_invoice'] = 'réglez la facture';
$lang['payjunction'] = 'Payjunction';
$lang['payment'] = 'paiement';
$lang['payment_information'] = 'information paiement';
$lang['payment_name'] = 'nom du paiement';
$lang['payment_options'] = 'options de paiement';
$lang['payment_preference'] = 'préférence de paiement';
$lang['payments'] = 'paiements';
$lang['paypal_id'] = 'ID Paypal';
$lang['paypal_id_taken'] = 'ID paypal pris';
$lang['pending'] = 'en attente';
$lang['percent'] = 'pourcent';
$lang['personal_info'] = 'infos personnelles';
$lang['photo_file'] = 'fichier photo';
$lang['photos'] = 'photos';
$lang['please_open_new_ticket'] = 'veuillez soumettre une nouvelle requête au centre d\'assistance et de support';
$lang['please_select'] = 'sélectionnez svp';
$lang['please_select_option'] = 'sélectionnez une option svp';
$lang['please_select_required_attributes'] = 'veuillez sélectionner toutes les options requises';
$lang['please_wait'] = 'patientez svp...';
$lang['please_wait_forwarding'] = 'patientez svp. vous êtes en train d\'être redirigé automatiquement';
$lang['post_comment'] = 'postez un commentaire';
$lang['postal_code'] = 'code postal';
$lang['posted_in'] = 'posté dans';
$lang['preview'] = 'prévisualisez';
$lang['price'] = 'prix';
$lang['price_high_to_low'] = 'du plus grand au plus petit prix';
$lang['price_low_to_high'] = 'du plus petit prix au plus grand';
$lang['primary_email'] = 'email primaire';
$lang['print'] = 'imprimez';
$lang['print_invoice'] = 'imprimez la facture';
$lang['print_invoice_balance_due'] = 'solde dû';
$lang['print_invoice_click_here_to_make_payment'] = 'cliquez ici pour effectuer un paiement';
$lang['print_invoice_credit_invoice'] = 'crédit facture';
$lang['print_invoice_has_been_paid'] = 'la facture a été réglée';
$lang['print_invoice_payment_date'] = 'date de paiement';
$lang['print_invoice_payment_method'] = 'méthode de paiement';
$lang['print_invoice_payment_type'] = 'type de paiement';
$lang['print_invoice_payments'] = 'paiements';
$lang['print_invoice_product_name'] = 'nom du produit';
$lang['print_invoice_quantity'] = 'quantité';
$lang['print_invoice_quantity_amount'] = 'quantité';
$lang['print_invoice_sku'] = 'SKU';
$lang['print_invoice_sku'] = 'SKU du produit';
$lang['print_invoice_total_payments'] = 'total paiements';
$lang['print_invoice_transaction_id'] = 'ID transaction';
$lang['print_invoice_unit_price'] = 'prix unitaire';
$lang['priority'] = 'priorité';
$lang['privacy_policy'] = 'Politique de confidentialité';
$lang['proceed_to_checkout'] = 'continuez à la caisse';
$lang['processing'] = 'en cours de traitement';
$lang['processing_order'] = 'commande en cours de traitement';
$lang['product'] = 'produit';
$lang['product_categories'] = 'catégories de produit';
$lang['product_name'] = 'nom du produit';
$lang['product_options'] = 'options de produits';
$lang['product_s'] = 'produit(s)';
$lang['product_type'] = 'type de produit';
$lang['products'] = 'produits';
$lang['products_per_page'] = 'produits par page';
$lang['products_purchased_on_commission'] = 'produits achetés sur cette commission';
$lang['profile'] = 'profil';
$lang['profile_description'] = 'description du profil';
$lang['profile_page'] = 'page de profil';
$lang['profiles'] = 'profils';
$lang['psigate'] = 'PSIGate';
$lang['purchase'] = 'achat';
$lang['qty'] = 'qté';
$lang['quantity'] = 'quantité';
$lang['rating'] = 'notation';
$lang['ratings'] = 'notations';
$lang['read_more'] = 'lisez la suite';
$lang['recommend_this'] = 'recommandez ceci';
$lang['recommended_products'] = 'produits recommandés';
$lang['referral_signup_bonus_note'] = 'bonus pour parrainnage d\'affilié';
$lang['referral_traffic'] = 'traffic du référencement';
$lang['referred_by'] = 'référencé par';
$lang['referring_url'] = 'URL de référencement';
$lang['refund'] = 'remboursez';
$lang['region'] = 'région';
$lang['register'] = 'enregistrez';
$lang['registration'] = 'enregistrement';
$lang['related_articles'] = 'articles similaires';
$lang['remove'] = 'retirez';
$lang['remove_coupon'] = 'retirez le coupon';
$lang['remove_coupon_text'] = 'êtes-vous sûr de retirer ce coupon?';
$lang['replication_contact_me'] = 'contactez-moi';
$lang['report_name'] = 'nom du rapport';
$lang['reports'] = 'rapports';
$lang['reports_and_stats'] = 'rapports et stats';
$lang['required'] = 'obligatoire';
$lang['required_field'] = 'champ obligatoire';
$lang['requires_min_quantity_of'] = 'exige une quantité minimale de';
$lang['reset_form'] = 'réinitialisez le formulaire';
$lang['reset_image'] = 'réinitialisez l\'image';
$lang['reset_password'] = 'réinitialisez le mot de passe';
$lang['reset_password_sent'] = 'votre mot de passe a été réinitialisé.  vérifiez votre boîte email svp';
$lang['review_waiting_moderation'] = 'votre revue a été soumise pour approbation';
$lang['reviewed_by'] = 'revue faite par';
$lang['reviews'] = 'revues';
$lang['right_ad_block'] = 'bloc d\'annonces à droite';
$lang['right_layout_menu'] = 'menu de mise en page à droite';
$lang['rows_per_page'] = 'rangées par page';
$lang['rss'] = 'RSS';
$lang['rss_feeds'] = 'RSS feeds';
$lang['s_and_h'] = 'Frais de port  & de traitement';
$lang['safepay_id'] = 'ID Safepay';
$lang['safepay_id_taken'] = 'ID safepay pris';
$lang['sale_total'] = 'total vente';
$lang['sales'] = 'ventes';
$lang['search'] = 'cherchez';
$lang['search_category'] = 'chercher catégorie';
$lang['search_term'] = 'cherchez le terme';
$lang['search_type'] = 'cherchez le type';
$lang['security_permissions'] = 'autorisations de securité';
$lang['select_a_shipping_option'] = 'sélectionnez une option d\'expédition';
$lang['select_category'] = 'sélectionnez une catégorie';
$lang['select_language'] = 'sélectionnez la langue';
$lang['select_payment'] = 'sélectionez les options de paiement';
$lang['select_payment_option'] = 'sélectionnez l\'option de paiement';
$lang['select_payment_option'] = 'sélectionnez votre option de paiemen';
$lang['select_permissions_for_admin'] = 'restreindre les autorisations de l\'admin';
$lang['select_rows_per_page'] = 'sélectionnez les rangées par page';
$lang['select_shipping_option'] = 'sélectionnez l\'option d\'expédition';
$lang['select_shipping_options'] = 'sélectionnez les options d\'expédition';
$lang['select_shipping_payment'] = 'sélectionnez les options de paiement et d\'expédition';
$lang['send_downline_email'] = 'envoyez email au filleul';
$lang['send_email'] = 'envoyez email';
$lang['send_email_to'] = 'envoyez email à';
$lang['send_invoice'] = 'envoyez facture';
$lang['send_invoice_font_family'] = 'Arial, Helvetica, sans-serif';
$lang['send_invoice_font_size'] = '-1';
$lang['send_sms_alerts'] = 'envoyez alertes sms';
$lang['send_to'] = 'envoyez à';
$lang['send_welcome_email'] = 'voulez-vous envoyer l\'email de bienvenue?';
$lang['set_billing_to_payment'] = 'voulez-vous que votre info de facturation soit utilisée comme info de paiement d\'affiliation ?';
$lang['settings'] = 'paramètres admin';
$lang['settings'] = 'paramètres';
$lang['ship_to_billing_address'] = 'voulez-vous que nous vous expédions cette commande à votre adresse de facturation?';
$lang['shipping'] = 'Port';
$lang['shipping_amount'] = 'frais de port';
$lang['shipping_estimate'] = 'estimation de frais de port';
$lang['shipping_info'] = 'info expédition';
$lang['shipping_information'] = 'information d\'expédition';
$lang['shipping_option_error'] = 'Option Non Disponible';
$lang['shipping_options'] = 'options d\'envoi';
$lang['shipping_options_text'] = 'sélectionez votre option d\'expédition';
$lang['shipping_selection_required'] = 'veuillez sélectionner une option d\'expédition';
$lang['shopping_cart'] = 'panier de courses';
$lang['show'] = 'montrez';
$lang['showing'] = 'montrant';
$lang['similar_products'] = 'produits similaires';
$lang['sitemap'] = 'plan du site';
$lang['sms_contact'] = 'contact sms';
$lang['sort_products_by'] = 'ressortez les produits';
$lang['sponsor'] = 'sponsor';
$lang['star'] = 'étoile';
$lang['stars'] = 'étoiles';
$lang['state'] = 'Etat';
$lang['status'] = 'statut';
$lang['step_one_of_three'] = 'étape 1 sur 3';
$lang['step_one_of_two'] = 'étape 1 sur 2';
$lang['step_three_of_three'] = 'étape 3 sur 3';
$lang['step_two_of_three'] = 'étape 2 sur 3';
$lang['step_two_of_two'] = 'étape 2 sur 2';
$lang['store'] = 'boutique';
$lang['store_config'] = 'configuration de la boutique';
$lang['store_maintenance'] = 'maintenance du site';
$lang['store_maintenance_enabled'] = 'le site est en maintenance.  veuillez retourner plus tard';
$lang['store_receipt'] = 'reçu de la boutique';
$lang['sub_categories'] = 'sous-catégories';
$lang['sub_total_amount'] = 'montant sous-total';
$lang['subject'] = 'objet';
$lang['subject'] = 'objet';
$lang['submission_maximum_limit_reached'] = 'soumission maximale quotidienne atteinte';
$lang['submit'] = 'soumettre';
$lang['submit_a_review'] = 'soumettre une revue de produit';
$lang['submit_order'] = 'soumettre la commande';
$lang['submit_order_here'] = 'cliquez pour finaliser votre commande ici';
$lang['submit_review'] = 'soumettez une revue';
$lang['subtotal'] = 'sous-total';
$lang['success_message'] = 'succès';
$lang['successfully_added_to_cart'] = 'a été ajouté avec succès dans votre panier';
$lang['successfully_added_items_to_cart'] = 'les articles ont été ajoutés avec succès dans votre panier';
$lang['support']  = 'support';
$lang['support_desk_disabled'] = 'centre d\'assistance et de support non connecté';
$lang['support_ticket_added_successfully'] = 'requêtes au support ajoutées avec succès';
$lang['support_ticket_updated_successfully'] = 'requêtes au support mises à jour avec succès';
$lang['support ticket closed successfully'] = 'requêtes au support closes avec succès';
$lang['support_tickets'] = 'requêtes au support';
$lang['switch_product_view'] = 'commencer la visualisation des produits';
$lang['tax_amount'] = 'montant de la taxe';
$lang['taxes'] = 'taxes';
$lang['tell_a_friend'] = 'informez votre ami sur ce produit';
$lang['terms_of_service'] = 'termes et conditions d\'utilisation du service';
$lang['text_ads'] = 'annonces textuelles';
$lang['text_links'] = 'liens textuels';
$lang['thank_you'] = 'Merci';
$lang['thank_you_order'] = 'Merci pour votre commande';
$lang['thanks_for_logging_in'] = 'Merci de vous être connecté!';
$lang['then'] = 'puis';
$lang['ticket_body'] = 'message du texte';
$lang['ticket_has_been_closed'] = 'Cette requête est close';
$lang['ticket_id'] = 'ID';
$lang['ticket_subject'] = 'titre de la requête';
$lang['timer_expired'] = 'votre session d\'admin a expiré';
$lang['to'] = 'à';
$lang['tool_name'] = 'nom';
$lang['tool_type'] = 'type d\'outil';
$lang['tools'] = 'outils';
$lang['total'] = 'total';
$lang['total_amount'] = 'montant total';
$lang['total_discounts'] = 'total remises';
$lang['total_levels'] = 'total niveaux';
$lang['total_pages'] = 'total page(s)';
$lang['total_users'] = 'total utilisateurs';
$lang['totals'] = 'totaux';
$lang['traffic'] = 'traffic';
$lang['trans_id_performance_bonus'] = 'bonus de rendement' . ' - ' . date('M d Y');
$lang['trans_id_referral_signup_bonus'] = 'bonus d\'inscription' . ' - ' . date('M d Y');
$lang['trans_id_signup_bonus'] = 'bonus d\'inscription' . ' - ' . date('M d Y');
$lang['transaction_id'] = 'ID transaction';
$lang['transactions'] = 'transactions';
$lang['trial'] = 'essai';
$lang['type'] = 'type';
$lang['unit_price'] = 'prix unitaire';
$lang['unknown_site'] = 'site inconnu et anonyme';
$lang['unlimited'] = 'illimité';
$lang['unpaid_commissions'] = 'commissions impayées';
$lang['unpaid_invoices'] = 'factures impayées';
$lang['unsubscribe'] = 'se désinscrire';
$lang['unsubscribe_here_now'] = 'cliquez ici pour vous désinscrire de la liste de diffusion'; 
$lang['unsubscribe_html_1'] = '<br /><br /><p align="center">';
$lang['unsubscribe_html_2'] = '</p>';
$lang['unsubscribe_text_1'] = "\n\n\n" . 'cliquez le lien ci-dessous pour vous désinscrire de notre liste de diffusion' . "\n\n";
$lang['unsubscribe_text_2'] = "\n\n";
$lang['unsubscribe_link'] = 'lien de désinscription';
$lang['update_admin'] = 'mettez à jour l\'admin';
$lang['update_admin_success'] = 'admin mis à jour avec succès';
$lang['update_administrator'] = 'mettez à jour l\'administrateur';
$lang['update_administrators'] = 'mettez à jour les administrateurs';
$lang['update_aff_ad_categories_success'] = 'catégorie d\'annonce d\'affilié mise à jour avec succès';
$lang['update_member'] = 'mettez le membre à jour';
$lang['update_members'] = 'mettez les membres à jour';
$lang['update_photo'] = 'mettez à jour la photo';
$lang['update_product'] = 'mettez à jour le produit';
$lang['update_products'] = 'mettez à jour les produits';
$lang['update_shopping_cart'] = 'mettez à jour le panier d\'achat';
$lang['update_subscriptions'] = 'mettez à jour les souscriptions';
$lang['update_ticket'] = 'mettez à jour la requête au support';
$lang['update_ticket_id'] = 'mettez à jour l\'ID de la requête au support';
$lang['update_totals'] = 'mettez à jour les totaux';
$lang['updated_by'] = 'mis à jour par';
$lang['updated_on'] = 'mis à jour le';
$lang['upload_photo'] = 'attachez la photo';
$lang['url'] = 'URL';
$lang['user_already_subscribed'] = 'vous êtes déjà souscrit à cette adhésion';
$lang['user_unsubscribed_successfully'] = 'vous avez été retiré avec succès de la liste de diffusion';
$lang['username'] = 'identifiant';
$lang['users'] = 'utilisateurs';
$lang['verification_code'] = 'code de vérification';
$lang['videos'] = 'vidéos';
$lang['view'] = 'visualisez';
$lang['view_administrators'] = 'visualisez les administrateurs';
$lang['view_closed_tickets'] = 'visualisez les requêtes closes';
$lang['view_comments'] = 'visualisez les commentaires';
$lang['view_downline'] = 'visualisez le filleul';
$lang['view_downline_for'] = 'visualisez le filleul au sujet de';
$lang['view_grid'] = 'visualisez la grille des produits';
$lang['view_list'] = 'visualisez la liste des produits';
$lang['view_member_photos'] = 'visuasilez les photos des membres';
$lang['view_members'] = 'visualisez les membres';
$lang['view_open_tickets'] = 'visualisez les requêtes ouvertes';
$lang['view_paid_commissions'] = 'visualisez les commissions payées';
$lang['view_paid_invoices'] = 'visualisez les factures payées';
$lang['view_products'] = 'visualisez les produits';
$lang['view_products_per_page'] = 'visualisez les produits par page';
$lang['view_profile'] = 'visualisez le profil';
$lang['view_unpaid_commissions'] = 'visualisez les commissions impayées';
$lang['view_unpaid_invoices'] = 'visualisez les factures impayées';
$lang['viral_pdfs'] = 'PDFs viraux';
$lang['viral_videos'] = 'vidéos virales';
$lang['we_dont_ship_to_country'] = 'désolé, nous n\'expédions pas vers ce pays';
$lang['website'] = 'site web';
$lang['week_s'] = 'semaine(s)';
$lang['welcome'] = 'bienvenue';
$lang['work_phone'] = 'téléphone professionnel';
$lang['write_a_review'] = 'écrivez la première revue';
$lang['write_review'] = 'écrivez une revue';
$lang['year'] = 'année';
$lang['year_s'] = 'année(s)';
$lang['yearly_click_stats_by_month'] = 'clis annuels de référencement /mois';
$lang['yearly_commission_stats_by_month'] = 'stats de commission de référencement annuelles /mois';
$lang['yearly_sales_stats_by_month'] = 'stats de ventes annuelles par parrainnage de chaque mois';
$lang['yes'] = 'yes';
$lang['you_must_agree_tos'] = 'vous devez accepter les termes et conditions d\'utilisation du service';
$lang['your_cart'] = 'votre panier';
$lang['your_cart_contents'] = 'votre panier de courses';
$lang['your_shopping_cart'] = 'votre panier de courses';

// 1.0.7.80
$lang['UPS_EXP'] = 'UPS exprès';
$lang['fedex_PRIORITYOVERNIGHT'] = 'priorité de Fedex durant la nuit';
$lang['fedex_STANDARDOVERNIGHT'] = 'norme de Fedex durant la nuit';
$lang['fedex_FIRSTOVERNIGHT'] = 'Fedex d\'abord durant la nuit';
$lang['fedex_FEDEX2DAY'] = 'jour de Fedex 2';
$lang['fedex_FEDEXEXPRESSSAVER'] = 'épargnant exprès de Fedex';
$lang['fedex_INTERNATIONALPRIORITY'] = 'priorité internationale de Fedex';
$lang['fedex_INTERNATIONALECONOMY'] = 'économie internationale de Fedex';
$lang['fedex_INTERNATIONALFIRST'] = 'International de Fedex d\'abord';
$lang['fedex_FEDEX1DAYFREIGHT'] = 'fret durant la nuit de Fedex';
$lang['fedex_FEDEX2DAYFREIGHT'] = 'Fedex fret de 2 jours';
$lang['fedex_FEDEX3DAYFREIGHT'] = 'Fedex fret de 3 jours';
$lang['fedex_FEDEXGROUND'] = 'Fedex a rectifié';
$lang['fedex_GROUNDHOMEDELIVERY'] = 'livraison à domicile de Fedex';
$lang['group_membership_required_to_view'] = 'adhésion de groupe est exigé pour regarder ce contenu';
$lang['html_code'] = 'code de HTML';
$lang['initial_fee'] = 'honoraires initiaux';
$lang['minimum_cart_purchase_required'] = 'quantité minimum est exigé avant contrôle';
$lang['no_shipping_options'] = 'aucunes options d\'expédition permises. opérateur de site Web de contact';
$lang['select_country'] = 'pays choisi';
$lang['souscrivez'] = 'souscrivent';
$lang['user_subscribed_successfully'] = 'a été avec succès souscrit à la liste d\'adresses';
$lang['shipping_first_name'] = 'prénom d\'expédition';
$lang['shipping_last_name'] = 'nom de famille d\'expédition';
$lang['shipping_address_1'] = 'adresse 1 d\'expédition';
$lang['shipping_address_2'] = 'adresse 2 d\'expédition';
$lang['shipping_city'] = 'ville d\'expédition';
$lang['shipping_country'] = 'pays d\'expédition';
$lang['shipping_state'] = 'état d\'expédition';
$lang['shipping_postal_code'] = 'code postal de expédition';
$lang['payment_name'] = 'nom de paiement';
$lang['payment_address_1'] = 'adresse 1 de paiement';
$lang['payment_address_2'] = 'adresse 2 de paiement';
$lang['payment_city'] = 'ville de paiement';
$lang['payment_country'] = 'pays de paiement';
$lang['payment_state'] = 'état de paiement';
$lang['payment_postal_code'] = 'code postal de paiement';
$lang['payment_preference_amount'] = 'quantité de préférence de paiement';
?>