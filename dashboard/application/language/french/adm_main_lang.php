<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| AVIS DE DROIT d\'aUTEUR
| Copyright 2008 JROX Technologies, Inc.  Tous droits de reproduction réservés.
| -------------------------------------------------------------------------
| Ce script ne pourra être utilisé et modifié que conformément à l\'accord de licence
  y contenu (license.txt) exception faite des endroits où cela a été expressément mentionné
| dans les espaces commentés du code interne. Cet avis de droit d\'auteur ainsi que 
  les commentaires énoncés ci-dessous et ci-dessus doivent à tout moment rester intacts. 
| En utilisant ce code vous acceptez de dégager JROX Technologies, Inc, ses employés,agents
| et affiliés de toute responsabilité qui surviendrait du fait de son utilisation.
|
| La vente du code de ce proramme sans permission écrite préalable est
| formellement interdite et est en violation des lois nationales et internationales matière
| de droit d\'auteur.
|
| -------------------------------------------------------------------------
| FILENAME - admin_main_lang.php
| -------------------------------------------------------------------------
|
| Ceci représente le fichier de langue de l\'ensemble de l\'espace administrateur
Traduit de l\'anglais par Mathurin Youmssi cleanwaxcam@yahoo.fr
|
*/

$lang['2checkout_payments'] = 'paiements 2checkout';
$lang['about_us'] = 'a notre propos';
$lang['account_info'] = ' info compte';
$lang['account_information'] = 'information compte';
$lang['actions'] = 'actions';
$lang['activate_deactivate'] = 'activer/ désactiver';
$lang['activate_deactivate_module'] = 'activer ou désactiver ce module';
$lang['activate_email_alert'] = 'activer / désactiver l\'alerte email';
$lang['activate_send_welcome_email'] = 'activer et envoyer email de bienvenue à';
$lang['active'] = 'actif';
$lang['ad_blocks'] = 'blocs d\'annonces';
$lang['ad_blocks_license_required'] = 'il faut une licence pour changer les blocs d\'annonces. Acheter d\'abord une licence';
$lang['ad_blocks_updated_successfully'] = 'blocs d\'annonces mis à jour avec succès';
$lang['ad_trackers'] = 'outils de suivi d\'annonces';
$lang['add'] = 'ajouter';
$lang['add_admin'] = 'ajouter admin';
$lang['add_admin_success'] = 'admin ajouté avec succès';
$lang['add_administrator'] = 'ajouter administrateur';
$lang['add_aff_ad_categories_success'] = 'catégorie d\'annonce de l\'affilié ajoutée avec succès';
$lang['add_affiliate_group'] = 'ajouter groupe d\'affiliés';
$lang['add_affiliate_payment'] = 'ajouter paiement affilié';
$lang['add_article_ad'] = 'ajouter article publicitaire';
$lang['add_article_ad_success'] = 'article publicitaire ajouté avec';
$lang['add_attribute'] = 'ajouter attribut';
$lang['add_attributes'] = 'ajouter attributs';
$lang['add_banner'] = 'ajouter bannière';
$lang['add_banner_success'] = 'bannière ajoutée avec succès';
$lang['add_bookmark'] = 'marquer cette page';
$lang['add_cart_for_price'] = 'ajouter au panier pour voir le prix';
$lang['add_category'] = 'ajouter catégorie';
$lang['add_commission'] = 'ajouter commission';
$lang['add_content'] = 'ajouter contenu';
$lang['add_content_article'] = 'ajouter l\'article au contenu';
$lang['add_content_category'] = 'ajouter la catégorie de contenu';
$lang['add_country'] = 'ajouter pays';
$lang['add_coupon'] = 'ajouter coupon';
$lang['add_coupon_success'] = 'coupon ajouté avec succès';
$lang['add_credit'] = 'ajouter crédit';
$lang['add_currency'] = 'ajouter devise';
$lang['add_currency_success'] = 'devise ajoutée avec succès';
$lang['add_custom_layout_box'] = 'ajouter un box de mise en page client';
$lang['add_discount_group'] = 'ajouter un groupe bénéficiant de rabais';
$lang['add_download'] = 'ajouter téléchargement';
$lang['add_email_ad'] = 'ajouter email publicitaire';
$lang['add_email_ad_success'] = 'email publicitaire ajouté avec succès';
$lang['add_email_template'] = 'ajouter template pour email';
$lang['add_faq'] = 'ajouter article pour la FAQ';
$lang['add_faq_article'] = 'supprimer la catégorie faq ';
$lang['add_faq_category'] = 'ajouter catégorie FAQ';
$lang['add_follow_up'] = 'ajouter lettre de suivi';
$lang['add_gateway_module'] = 'ajouter module de paiement';
$lang['add_group_success'] = 'groupe ajouté avec succès';
$lang['add_html_ad'] = 'ajouter annonce html';
$lang['add_html_ad_success'] = 'annonce html ajoutée avec succès';
$lang['add_invoice'] = 'ajouter facture';
$lang['add_invoice'] = 'ajouter facture';
$lang['add_invoice_payment'] = 'ajouter facture de règlement';
$lang['add_language'] = 'ajouter langue';
$lang['add_mailing_list'] = 'ajouterliste de diffusion';
$lang['add_manufacturer'] = 'ajouter fournisseur';
$lang['add_manufacturer_success'] = 'fournisseur ajouté avec succès';
$lang['add_member'] = 'ajouter membre';
$lang['add_member_credit'] = 'ajouter crédit au membre';
$lang['add_member_success'] = 'membre ajouté avec succès';
$lang['add_membership'] = 'ajouter une adhésion';
$lang['add_module'] = 'ajouter module';
$lang['add_new_row'] = 'ajouter un nouveau';
$lang['add_new_video'] = 'ajouter une nouvelle vidéo';
$lang['add_order'] = 'ajouter une commande';
$lang['add_payment'] = 'ajouter paiement';
$lang['add_product'] = 'ajouter produit';
$lang['add_product_category'] = 'ajouter catégorie de produit';
$lang['add_region'] = 'ajouter région';
$lang['add_report'] = 'ajouter rapport';
$lang['add_simple_layout_box'] = 'ajouter un box de design simple';
$lang['add_support_category'] = 'ajouter catégorie au support';
$lang['add_tax_class'] = 'ajouter type de taxe';
$lang['add_tax_zone'] = 'ajouter zone pour taxe';
$lang['add_text_ad'] = 'ajouter annonce textuelle';
$lang['add_text_ad_success'] = 'annonce textuelle ajoutée avec succès';
$lang['add_text_link'] = 'ajouter lien textuel';
$lang['add_text_link_success'] = 'lien textuel ajouté avec succès';
$lang['add_theme'] = 'ajouter thème';
$lang['add_to_affiliate_group'] = 'ajouter au groupe d\'affiliation';
$lang['add_to_discount_group'] = 'ajouter au groupe bénéficiant de rabais';
$lang['add_to_faq'] = 'ajouter à la FAQ';
$lang['add_to_mailing_list'] = 'ajouter à la liste de diffusion';
$lang['add_to_membership'] = 'ajouter à une adhésion';
$lang['add_tracking'] = 'ajouter l\'outil de suivi';
$lang['add_vendor'] = 'ajouter vendeur';
$lang['add_viral_pdf'] = 'ajouter pdf viral';
$lang['add_viral_pdf_success'] = 'pdf viral ajouté avec succès';
$lang['add_viral_video'] = 'ajouter vidéo virale';
$lang['add_viral_video_success'] = 'vidéo virale ajoutée avec succès';
$lang['add_zone'] = 'ajouter zone';
$lang['added_by'] = 'ajouté par';
$lang['added_on'] = 'ajouté le';
$lang['added_successfully'] = 'ajouté avec succès';
$lang['address_1'] = 'adresse 1';
$lang['address_2'] = 'adresse 2';
$lang['address_information'] = 'info adresse';
$lang['admin'] = 'admin';
$lang['admin_alerts'] = 'alertes admin';
$lang['admin_dashboard'] = 'espace admin';
$lang['admin_email_taken'] = 'email admin déjà pris';
$lang['admin_fname'] = 'prénom admin';
$lang['admin_id'] = 'identifiant admin ';
$lang['admin_information'] = 'info admin';
$lang['admin_login_url'] = 'URLlogin admin';
$lang['admin_name'] = 'nom admin';
$lang['admin_photo'] = 'photo admin';
$lang['admin_quick_links'] = 'liens rapides admin';
$lang['admin_response_by'] = 'réponse admin par';
$lang['admin_settings'] = 'Paramètres admin';
$lang['admin_username_taken'] = 'id admin déjà pris';
$lang['admin_users'] = 'utilisateurs administrateurs';
$lang['admin_window_height'] = 'hauteur fenêtre admin';
$lang['admin_window_width'] = 'largeur fenêtre admin';
$lang['advanced'] = 'supérieur(e)';
$lang['advertising'] = 'publicité';
$lang['aff_ad_categories'] = 'catégorie d\'annonce affilié';
$lang['aff_ad_categories_add'] = 'ajouter une catégorie d\'annonce de l\'affilié';
$lang['aff_ad_categories_update'] = 'mise à jour catégorie d\'annonce de l\'affilié';
$lang['affiliate'] = 'affilié';
$lang['affiliate_group'] = 'groupe d\'affiliation';
$lang['affiliate_group_name'] = 'nom groupe d\'affiliation';
$lang['affiliate_groups'] = 'groupes d\'affiliation';
$lang['affiliate_info'] = 'info affilié';
$lang['affiliate_information'] = 'information affilié';
$lang['affiliate_link'] = 'lien d\'affiliation';
$lang['affiliate_marketing'] = 'marketing d\'affiliation';
$lang['affiliate_marketing_tools'] = 'outils marketing d\'affiliation';
$lang['affiliate_member_options'] = 'options pour affilié';
$lang['affiliate_note'] = 'notes de l\'affilié';
$lang['affiliate_options'] = 'options d\'affiliation';
$lang['affiliate_payment'] = 'paiement d\'affilié';
$lang['affiliate_payment_added_successfully'] = 'paiement de l\'affilié ajouté avec succès';
$lang['affiliate_payment_deleted_successfully'] = 'paiement de l\'affilié supprimé avec succès';
$lang['affiliate_payment_deleted_successfully'] = 'paiement de l\'affilié supprimé avec succès';
$lang['affiliate_payment_invoice'] = 'Facture de règlement de l\'affilié';
$lang['affiliate_payment_updated_successfully'] = 'paiement de l\'affilié mis à jour avec succès';
$lang['affiliate_payments'] = 'paiements de l\'affilié';
$lang['affiliate_payments_updated_successfully'] = 'paiements affiliés mis à jour avec succès';
$lang['affiliate_settings'] = 'Paramètres affilation';
$lang['affiliate_tools'] = 'outils affilié';
$lang['ago'] = 'ago';
$lang['alert'] = 'alert';
$lang['alert_admin_article_comment'] = 'alerte sur commentaire d\'article';
$lang['alert_admin_product_review'] = 'alerte sur revue de produit';
$lang['alert_downline_signup'] = 'alerte sur enregistrement filleul';
$lang['alert_new_commission'] = 'alerte sur nouvelle commission';
$lang['alert_on_failed_admin_login'] = 'alerte échec de connexion admin';
$lang['alert_on_new_affiliate_commission'] = 'alerte nouvelle commission d\'affiliation';
$lang['alert_on_new_affiliate_signup'] = 'alerte d\'enregistrement nouvel affilié';
$lang['alert_on_new_customer_order'] = 'alerte nouvelle commande client';
$lang['alert_on_store_inventory'] = 'alerte sur stocks en boutique';
$lang['alert_payment_sent'] = 'alerte sur paiement envoyé';
$lang['alert_ticket_response'] = 'alerte sur réponse à une requête';
$lang['alertpay_id'] = 'iD AlertPay';
$lang['alertpay_id_taken'] = 'iD Alertpay déjà pris';
$lang['alertpay_payments'] = 'paiements AlertPay';
$lang['all'] = 'tout(e)';
$lang['all_categories'] = 'toutes les catégories';
$lang['all_countries'] = 'Tous les pays';
$lang['all_menu_sort_order_numeric'] = 'toutes les données ressorties doivent être numériques';
$lang['all_regions'] = 'Toutes les Régions';
$lang['allow_downline_email'] = 'autoriser l\'envoi d\'email aux filleuls';
$lang['allow_downline_view'] = 'permettre de visualiser les filleuls';
$lang['allowed_tags'] = 'autoriser les Tags Dynamiques';
$lang['amount'] = 'montant';
$lang['applied_to'] = 'appliqué à';
$lang['applied_to_invoice'] = 'appliqué à la facture';
$lang['apply_credits'] = 'appliqué aux crédits';
$lang['approve_checked_comments'] = 'approuver commentaires vérifiés';
$lang['approve_checked_reviews'] = 'approuver revues vérifiées';
$lang['approved'] = 'approuvé';
$lang['approved'] = 'approuvé';
$lang['archive_email'] = 'archives emails';
$lang['archive_report'] = 'archives rapports';
$lang['archived_report_deleted_successfully'] = 'rapports archivés supprimés avec succès';
$lang['archived_report_name'] = 'nom du rapport archivé';
$lang['archived_reports'] = 'rapports archivés';
$lang['are_you_sure_delete_language'] = 'supprimer cette langue du système ne supprime pas les fichiers de cette langue. Vous devez supprimer manuellement le fichier de langue si voulez vraiment la retirer du site';
$lang['are_you_sure_you_want_to_delete'] = 'êtes-vous sûr de vouloir supprimer?';
$lang['are_you_sure_you_want_to_delete_commission'] = 'êtes-vous sûr de vouloir supprimer cette commission?';
$lang['are_you_sure_you_want_to_delete_invoice'] = 'êtes-vous sûr de vouloir supprimer cette facture?';
$lang['are_you_sure_you_want_to_delete_module'] = 'êtes-vous sûr de vouloir supprimer ce module?  Il ne sera plus disponible dans l\'ensemble votre site.';
$lang['are_you_sure_you_want_to_delete_support_ticket'] = 'ETES-VOUS SUR DE VOULOIR SUPPRIMER CETTE REQUETE? supprimer cette requête enverra un email à l\'utilisateur pour l\'informer que sa requête a été supprimée et qu\'aucune solution n\'y a été apportée.  nous vous recommandons de résoudre le problème avec de mettre fin à cette requête.  Si vous tenez à supprimer cette requête, cliquez Okay.';
$lang['are_you_sure_you_want_to_delete_this_download'] = 'êtes-vous sûr de vouloir supprimer ce téléchargement?';
$lang['are_you_sure_you_want_to_delete_this_group'] = 'êtes-vous sûr de vouloir supprimer ce groupe?';
$lang['are_you_sure_you_want_to_delete_this_member'] = 'ETES-VOUS SUR DE VOULOIR SUPPRIMER CET UTILISATEUR?  Supprimer cet utilisateur supprimera tout ce qui lui est associé.  Ceci comprend les factures, les paiements, les commissions et les stats liés au traffic.  Si vous tenez à garder intacts les factures et les états de paiement, nous vous suggérons tout simplement de changer son statut en inactif au lieu de le supprimer.  Si malgré tout vous voulez supprimer cet utilisateur, cliquez ok.';
$lang['are_you_sure_you_want_to_delete_this_membership'] = 'êtes-vous sûr de vouloir supprimer cette sosuscription d\'adhésion?';
$lang['are_you_sure_you_want_to_delete_this_product'] = 'êtes-vous sûr de vouloir supprimer ce produit?';
$lang['are_you_sure_you_want_to_delete_this_user'] = 'êtes-vous sûr de vouloir supprimer cet utilisateur?';
$lang['are_you_sure_you_want_to_do_delete_affiliate_payment'] = 'êtes-vous sûr de vouloir supprimer ce paiement?  quand vous supprimez ceci, cela n\'affectera pas le statut des commissions associées à ce paiement . Cliquez sur OK pour continuer ou supprimer la fermeture';
$lang['are_you_sure_you_want_to_do_delete_mailing_list'] = 'êtes-vous sûr de vouloir supprimer cette liste de diffusion?';
$lang['are_you_sure_you_want_to_do_this'] = 'êtes-vous sûr de vouloir le faire?';
$lang['article_ad_body'] = 'le texte de l\'article publicitaire';
$lang['article_ad_details'] = 'détails de l\'article';
$lang['article_ad_name'] = 'nom de l\'article';
$lang['article_ad_options'] = 'options d\'articles publiciaires';
$lang['article_ad_title'] = 'titre de l\'article';
$lang['article_ad_width'] = 'largeur de l\'article';
$lang['article_ads'] = 'articles publicitaires';
$lang['article_comment'] = 'commentaire de l\'article';
$lang['article_id'] = 'iD article';
$lang['article_url'] = 'URL article';
$lang['assign_ticket'] = 'assigner une requête';
$lang['assign_ticket_to'] = 'assigner la requête à';
$lang['assign_to_member'] = 'assigner au membre';
$lang['assign_to_you'] = 's\'assigner à soi';
$lang['assigned_to'] = 'assigné à';
$lang['attribute'] = 'attribut';
$lang['attribute_callback'] = 'attribut du callback';
$lang['attribute_name'] = 'nom de l\'attribut';
$lang['attribute_required'] = 'obligatoire';
$lang['attribute_status'] = 'statut de l\'attribut';
$lang['attribute_type'] = 'type d\'attribut';
$lang['attribute_updated_successfully'] = 'attribut mis à jour avec succès';
$lang['authorize_net_aim'] = 'authorize.Net AIM';
$lang['authorize_net_payments'] = 'Paiements Authorize.Net (AIM)';
$lang['available_credits'] = 'crédits disponibles';
$lang['backup'] = 'sauvegarde';
$lang['backup_db'] = 'sauvegarde base de données';
$lang['backup_now'] = 'sauvegarder maintenant';
$lang['backup_settings'] = 'paramètres de sauvegarde';
$lang['balance_due'] = 'solde_dû';
$lang['bank_transfer'] = 'virement bancaire';
$lang['banner_details'] = 'détails bannière';
$lang['banner_enable_redirect'] = 'autoriser l\'url de redirection pour client';
$lang['banner_file_name'] = 'nom ficier de la bannière';
$lang['banner_file_required'] = 'fichier bannière obligatoire';
$lang['banner_height'] = 'hauteur bannière';
$lang['banner_id'] = 'id bannière';
$lang['banner_name'] = 'nom bannière';
$lang['banner_notes'] = 'notes bannière';
$lang['banner_options'] = 'options bannières';
$lang['banner_width'] = 'largeur bannière';
$lang['banners'] = 'bannières';
$lang['base_physical_path'] = 'chemin d\'accès au classeur physique';
$lang['base_url'] = 'base url';
$lang['bcc'] = 'bcc';
$lang['bg_body_color'] = 'couleur d\'habillage et de fond';
$lang['bg_title_color'] = 'titre couleur de fond';
$lang['billing'] = 'facturation';
$lang['billing_address_1'] = 'adresse de facturation 1';
$lang['billing_address_2'] = 'adresse de facturation 2';
$lang['billing_city'] = 'ville de facturation';
$lang['billing_country'] = 'pays de facturation';
$lang['billing_info'] = 'info de facturation';
$lang['billing_information'] = 'information facturation';
$lang['billing_postal_code'] = 'code postal de facturation';
$lang['billing_state'] = 'Etat de facturation';
$lang['blog'] = 'blog';
$lang['border_color'] = 'couleur du bord';
$lang['bottom'] = 'derrière';
$lang['bottom_block'] = 'bloc arrière';
$lang['bottom_menu'] = 'menu arrière';
$lang['bottom_menu_links'] = 'liens menu arrière';
$lang['by'] = 'par';
$lang['cannot_be_empty'] = 'ne peut être vide';
$lang['cannot_deactivate'] = 'ne peut désactiver';
$lang['cannot_delete'] = 'ne peut supprimer';
$lang['cannot_delete_superadmin'] = 'vous ne pouvez pas supprimer l\'administrateur principal';
$lang['cart'] = 'panier';
$lang['cart_settings'] = 'paramètres panier';
$lang['cat_id'] = 'id catégorie';
$lang['cat_name'] = 'nom catégorie';
$lang['cat_status'] = 'statut catégorie';
$lang['categories'] = 'catégories';
$lang['category'] = 'catégorie'; 
$lang['category_deleted_successfully'] = 'catégorie supprimée avec succès';
$lang['category_description'] = 'description de la catégorie';
$lang['category_keywords'] = 'mots-clés de la catégorie';
$lang['category_name'] = 'nom de la catégorie';
$lang['category_name_taken'] = 'nom de la catégorie déjà pris';
$lang['category_path'] = 'chemin vers la catégorie';
$lang['category_status'] = 'statut de la catégorie';
$lang['category_updated_successfully'] = 'catégorie mise à jour avec succès';
$lang['cc'] = 'cc';
$lang['change_permissions_for'] = 'changer les autorisatios pour';
$lang['changing_fields_only_zero_one'] = 'vous essayez de changer les champs qui ne peuvent être que zéro ou un';
$lang['charge_shipping_by_percent'] = 'facturer l\'expédition par pourcentage';
$lang['charge_shipping_flat_rate'] = 'taux forfaitaire d\'expédition';
$lang['charge_shipping_per_item'] = 'facturer l\'expédition par objet';
$lang['charge_shipping_UPS'] = 'Options d\'expédition par UPS';
$lang['charge_shipping_USPS'] = 'Options d\'expédition par USPS';
$lang['check'] = 'vérifier';
$lang['check_email_settings'] = 'vérifier les paramètres email';
$lang['check_uncheck'] = 'vérifié / non-vérifié';
$lang['checkbox'] = 'case à cocher';
$lang['checking_update_files'] = 'veuillez patienter... vérification en cours des fichiers mis à jour';
$lang['checkout'] = 'caisse checkout';
$lang['checkout_form'] = 'formulaire de caisse';
$lang['city'] = 'ville';
$lang['click_here_to_add'] = 'cliquez ici our ajouter un';
$lang['click_here_to_download_pdf'] = 'cliquez ici pour télécharger le doc PDF';
$lang['click_here_to_view'] = 'cliquez ici pour voir';
$lang['click_here_to_view_member_details'] = 'cliquez ic pour voir les détails sur le membre et ajouter un autre';
$lang['click_on_each_to_view_downline'] = 'cliquez sur chaque utilisateur pour visualiser ses filleuls';
$lang['click_to_add_administrator'] = 'cliquez pour ajouter un administrateur';
$lang['click_to_close_or_add'] = 'cliquez ici pour fermer ou pour ajouter un autre';
$lang['click_to_continue'] = 'cliquez ici pour continuer';
$lang['click_to_download'] = 'cliquez ici pour télécharger';
$lang['click_to_view'] = 'cliquez ici pour voir';
$lang['click_view_commissions'] = 'cliquez pour voir les commissions';
$lang['click_view_invoices'] = 'cliquez pour voir les factures';
$lang['click_view_payments'] = 'cliquez pour vous les paiements des affiliés';
$lang['click_view_click_stats'] = 'cliquez pour voir les stats liés au traffic';
$lang['click_view_sales_stats'] = 'cliquez pour voir les stats de vente';
$lang['clicks'] = 'clics';
$lang['clicks_deleted_successfully'] = 'clics supprimés avec succès';
$lang['clone'] = 'cloner';
$lang['close'] = 'fermer';
$lang['close_ticket'] = 'clore la requête';
$lang['closed'] = 'mis fin';
$lang['closed'] = 'mis fin';
$lang['cm'] = 'CM';
$lang['code'] = 'code';
$lang['code'] = 'code';
$lang['column_options'] = 'options pour colonnes';
$lang['comm'] = 'comm';
$lang['comment_posted_successfully'] = 'commentaire posté avec succès';
$lang['comment_to_approve'] = 'article commentaire à approuver';
$lang['comment_updated_successfully'] = 'commentaire mis à jour avec succès';
$lang['comments'] = 'commentaires';
$lang['comments_to_approve'] = 'commentaires d\'article à approuver';
$lang['comments_updated_successfully'] = 'commentaires mis jour avec succès';
$lang['commission'] = 'commission';
$lang['commission_added_successfully'] = 'commission ajoutée avec succès';
$lang['commission_amount'] = 'montant commission';
$lang['commission_date'] = 'date de la commission';
$lang['commission_deleted_successfully'] = 'commission supprimée avec succès';
$lang['commission_fields'] = 'champs pour commission';
$lang['commission_level'] = 'niveau de commission';
$lang['commission_level_1'] = 'niveau 1';
$lang['commission_level_10'] = 'niveau 10';
$lang['commission_level_2'] = 'niveau 2';
$lang['commission_level_3'] = 'niveau 3';
$lang['commission_level_4'] = 'niveau 4';
$lang['commission_level_5'] = 'niveau 5';
$lang['commission_level_6'] = 'niveau 6';
$lang['commission_level_7'] = 'niveau 7';
$lang['commission_level_8'] = 'niveau 8';
$lang['commission_level_9'] = 'niveau 9';
$lang['commission_notes'] = 'notes sur commission';
$lang['commission_options'] = 'options de commission';
$lang['commission_per_levels'] = 'commission par niveaux';
$lang['commission_reversal'] = 'rétractiono de la commission';
$lang['commission_stats'] = 'stats commission';
$lang['commission_type'] = 'type de commission';
$lang['commission_updated_successfully'] = 'commission mise à jour avec succès';
$lang['commissions'] = 'commissions';
$lang['commissions_made'] = 'commissions générées';
$lang['commissions_made_by'] = 'commissions générées par';
$lang['commissions_paid_successfully'] = 'commissions payées avec succès';
$lang['company'] = 'société';
$lang['completed'] = 'effectué';
$lang['configuration'] = 'configuration';
$lang['confirm_link'] = 'lien de confirmation';
$lang['confirm_password'] = 'confirmer mot de passe';
$lang['contact_email'] = 'email de contact';
$lang['contact_fields'] = 'champs de contact';
$lang['contact_message_html'] = 'message de contact en html';
$lang['contact_message_text'] = 'message de contact en texte';
$lang['contact_name'] = 'nom de contact';
$lang['contact_us'] = 'nous contacter';
$lang['content'] = 'contenu';
$lang['content'] = 'contenu';
$lang['content_ad_block_1'] = 'bloc de contenu d\'annonce 1';
$lang['content_ad_block_2'] = 'bloc de contenu d\'annonce 2';
$lang['content_article_added_successfully'] = 'contenu ajouté avec succès';
$lang['content_article_updated_successfully'] = 'contenu mis à jour avec succès';
$lang['content_articles'] = 'catégories de contenu';
$lang['content_body'] = 'corps du content body';
$lang['content_categories'] = 'contenu';
$lang['content_category'] = 'catégorie de contenu';
$lang['content_category_added_successfully'] = 'catégorie de contenu ajoutée avec succès';
$lang['content_category_updated_successfully'] = 'catégorie de contenu mise à jour avec succès';
$lang['content_comment'] = 'commentaire sur le contenu';
$lang['content_description'] = 'meta description';
$lang['content_html'] = 'contenu html';
$lang['content_options'] = 'options de contenu';
$lang['content_pages'] = 'pages du contenu';
$lang['content_path'] = 'chemin vers le contenu';
$lang['content_settings'] = 'paramètres du contenu';
$lang['content_text'] = 'contenu du texte';
$lang['content_title'] = 'titre du contenu';
$lang['content_type'] = 'type de contenu';
$lang['could_not_add_admin'] = 'n\'a pu ajouter l\'administrateur';
$lang['could_not_add_affiliate_payment'] = 'n\'a pu ajouter le paiement de l\'affilié';
$lang['could_not_add_affiliate_payment'] = 'n\'a pu ajouter le paiement de l\'affilié';
$lang['could_not_add_article_ad'] = 'n\'a pu ajouter l\'article publicitaire';
$lang['could_not_add_attribute'] = 'n\'a pu ajouter d\'attribut';
$lang['could_not_add_banner'] = 'n\'a pu ajouter la bannière';
$lang['could_not_add_banner'] = 'n\'a pu ajouter la bannière';
$lang['could_not_add_commission'] = 'n\'a pu ajouterla commission';
$lang['could_not_add_content_category'] = 'n\'a pu ajouter la catégorie du contenu';
$lang['could_not_add_credit'] = 'n\'a pu ajouter le crédit';
$lang['could_not_add_currency'] = 'n\'a pu ajouter la devise';
$lang['could_not_add_email_ad'] = 'n\'a pu ajouter l\'email publicitaire';
$lang['could_not_add_faq_article'] = 'n\'a pu ajouter la FAQ pour article';
$lang['could_not_add_faq_category'] = 'n\'a pu ajouter la catégorie FAQ';
$lang['could_not_add_follow_up'] = 'n\'a pu ajouterle suivi';
$lang['could_not_add_html_ad'] = 'n\'a pu ajouter l\'annonce html';
$lang['could_not_add_invoice'] = 'n\'a pu ajouter la facture';
$lang['could_not_add_invoice_payment'] = 'n\'a pu ajouterla facture de paiement';
$lang['could_not_add_language'] = 'n\'a pu ajouter la langue';
$lang['could_not_add_mailing_list'] = 'n\'a pu ajouterla liste de diffusion';
$lang['could_not_add_manufacturer'] = 'n\'a pu ajouter le fournisseur';
$lang['could_not_add_member'] = 'n\'a pu ajouterle membre';
$lang['could_not_add_product'] = 'n\'a pu ajouterle produit';
$lang['could_not_add_region'] = 'n\'a pu ajouterla région';
$lang['could_not_add_template'] = 'n\'a pu ajouterle template';
$lang['could_not_add_text_ad'] = 'n\'a pu ajouterle l\'annonce textuelle';
$lang['could_not_add_text_link'] = 'n\'a pu ajouter le lien textuel';
$lang['could_not_add_theme'] = 'n\'a pu ajouterle thème';
$lang['could_not_add_tracking'] = 'n\'a pu ajouter le tracker';
$lang['could_not_add_viral_pdf'] = 'n\'a pu ajouter le pdf viral';
$lang['could_not_add_viral_video'] = 'n\'a pu ajouter la vidéo virale';
$lang['could_not_add_zone'] = 'n\'a pu ajouterla zone';
$lang['could_not_backup_db'] = 'n\'a pu sauvegarder la base de données';
$lang['could_not_connect_database'] = 'n\'a pu connecter à la base de données';
$lang['could_not_connect_database_server'] = 'n\'a pu connecter au serveur de la base des données';
$lang['could_not_copy_image'] = 'n\'a pu copier l\'image';
$lang['could_not_delete_admin'] = 'n\'a pu supprimer l\'administrateur';
$lang['could_not_delete_affiliate_payment'] = 'n\'a pu supprimer le paiement de l\'affilié';
$lang['could_not_delete_article_ad'] = 'n\'a pu supprimer l\'article publicitaire';
$lang['could_not_delete_banner'] = 'n\'a pu supprimer la bannière';
$lang['could_not_delete_commission'] = 'n\'a pu supprimerla commission';
$lang['could_not_delete_content_category'] = 'n\'a pu supprimerla catégorie du contenu';
$lang['could_not_delete_country'] = 'n\'a pu supprimerle pays';
$lang['could_not_delete_currency'] = 'n\'a pu supprimerla devise';
$lang['could_not_delete_email'] = 'n\'a pu supprimer l\'email';
$lang['could_not_delete_email_ad'] = 'n\'a pu supprimer l\'email publicitaire';
$lang['could_not_delete_faq_article'] = 'n\'a pu supprimer l\'article pour FAQ';
$lang['could_not_delete_faq_category'] = 'n\'a pu supprimer la catégorie de FAQ';
$lang['could_not_delete_follow_up'] = 'n\'a pu supprimer emails de suivi';
$lang['could_not_delete_group'] = 'n\'a pu supprimer le groupe';
$lang['could_not_delete_html_ad'] = 'n\'a pu supprimerl\'annonce html';
$lang['could_not_delete_image'] = 'n\'a pu supprimer l\'image';
$lang['could_not_delete_invoice'] = 'n\'a pu supprimerla facture';
$lang['could_not_delete_invoice_payment'] = 'n\'a pu supprimer la facture de règlement';
$lang['could_not_delete_language'] = 'n\'a pu supprimerla langue';
$lang['could_not_delete_mailing_list'] = 'n\'a pu supprimer la liste de diffusion';
$lang['could_not_delete_manufacturer'] = 'n\'a pu supprimerle fournisseur';
$lang['could_not_delete_member'] = 'n\'a pu supprimerle membre';
$lang['could_not_delete_region'] = 'n\'a pu supprimer la région';
$lang['could_not_delete_template'] = 'n\'a pu supprimer le template';
$lang['could_not_delete_text_ad'] = 'n\'a pu supprimerl\'annonce textuelle';
$lang['could_not_delete_text_link'] = 'n\'a pu supprimer le lien textuel';
$lang['could_not_delete_theme'] = 'n\'a pu supprimerle thème';
$lang['could_not_delete_ticket'] = 'n\'a pu supprimerla requête';
$lang['could_not_delete_tracking'] = 'n\'a pu supprimer l\'outil de suivi';
$lang['could_not_delete_traffic'] = 'n\'a pu supprimer le traffic';
$lang['could_not_delete_viral_pdf'] = 'n\'a pu supprimer le pdf viral';
$lang['could_not_delete_viral_video'] = 'n\'a pu supprimer la vidéo virale';
$lang['could_not_delete_zone'] = 'n\'a pu supprimer la zone';
$lang['could_not_send_email'] = 'n\'a pu envoyer l\'email';
$lang['could_not_send_email_alert'] = 'n\'a pu envoyer l\'email';
$lang['could_not_send_invoice'] = 'n\'a pu envoyer la facture';
$lang['could_not_uninstall_module'] = 'n\'a pu désinstaller le module';
$lang['could_not_uninstall_shipping'] = 'n\'a pu désinstaller l\'expédition';
$lang['could_not_update_admin'] = 'n\'a pu mettre à jour l\'admin';
$lang['could_not_update_aff_ad_category'] = 'n\'a pu mettre à jour la catégorie d\'annonce pour l\'affilié';
$lang['could_not_update_affiliate_payment'] = 'n\'a pu mettre à jour le paiement de l\'affilié';
$lang['could_not_update_article_ad'] = 'n\'a pu mettre à jour l\'article publicitaire';
$lang['could_not_update_banner'] = 'n\'a pu mettre à jour la bannière';
$lang['could_not_update_commission'] = 'n\'a pu mettre à jour la commission';
$lang['could_not_update_content_category'] = 'n\'a pu mettre à jourla catégorie de contenu';
$lang['could_not_update_content_comment'] = 'n\'a pu mettre à jour le commentaire du contenu';
$lang['could_not_update_country'] = 'n\'a pu mettre à jourle pays';
$lang['could_not_update_credit'] = 'n\'a pu mettre à jour le crédit';
$lang['could_not_update_currency'] = 'n\'a pu mettre à jourla devise';
$lang['could_not_update_custom_affiliate_group_pricing'] = 'n\'a pu mettre à jour le prix défini pour le groupe d\'affiliation';
$lang['could_not_update_email_ad'] = 'n\'a pu mettre à jour l\'email publicitaire';
$lang['could_not_update_faq_article'] = 'n\'a pu mettre à jour l\'article de la FAQ';
$lang['could_not_update_faq_category'] = 'n\'a pu mettre à jourla catégorie de la FAQ';
$lang['could_not_update_form_fields'] = 'n\'a pu mettre à jourles champs du formulaire';
$lang['could_not_update_html_ad'] = 'n\'a pu mettre à jourles annonces html';
$lang['could_not_update_invoice'] = 'n\'a pu mettre à jourla facture';
$lang['could_not_update_invoice_payment'] = 'n\'a pu mettre à jourla facture de règlement';
$lang['could_not_update_language_file'] = 'n\'a pu mettre à jourle fichier de langue';
$lang['could_not_update_layout_boxes'] = 'n\'a pu mettre à jour des cases de design';
$lang['could_not_update_manufacturer'] = 'n\'a pu mettre à jourle fournisseur';
$lang['could_not_update_member'] = 'n\'a pu mettre à jourle membre';
$lang['could_not_update_region'] = 'n\'a pu mettre à jour la région';
$lang['could_not_update_review'] = 'n\'a pu mettre à jourla revue';
$lang['could_not_update_settings'] = 'n\'a pu mettre à jourles paramètres';
$lang['could_not_update_shipping'] = 'n\'a pu mettre à jour l\'expédition';
$lang['could_not_update_support_ticket'] = 'n\'a pu mettre à jour la requête au support';
$lang['could_not_update_support_tickets'] = 'n\'a pu mettre à jour les requêtes au support';
$lang['could_not_update_template'] = 'n\'a pu mettre à jour le template';
$lang['could_not_update_text_ad'] = 'n\'a pu mettre à jour l\'annonce textuelle';
$lang['could_not_update_text_link'] = 'n\'a pu mettre à jour le lien textuel';
$lang['could_not_update_viral_pdf'] = 'n\'a pu mettre à jour le pdf viral';
$lang['could_not_update_viral_video'] = 'n\'a pu mettre à jour la vidéo virale';
$lang['could_not_update_zone'] = 'n\'a pu mettre à jour la zone';
$lang['could_not_upload_image'] = 'n\'a pu attacher l\'image';
$lang['could_not_upload_photo'] = 'n\'a pu attacher la photo';
$lang['countries_updated_successfully'] = 'pays mis à jour avec succès';
$lang['country'] = 'pays';
$lang['country_name'] = 'nom du pays';
$lang['coupon_amount'] = 'montant du coupon';
$lang['coupon_code'] = 'code coupon';
$lang['coupon_code_taken'] = 'code coupon déjà pris';
$lang['coupon_expire_date'] = 'date d\'expiration du coupon';
$lang['coupon_minimum_order'] = 'commande minimum pour ce coupon';
$lang['coupon_product_type'] = 'type de produit pour coupon';
$lang['coupon_recurring'] = 'coupon récurrent';
$lang['coupon_start_date'] = 'Début de validité du coupon';
$lang['coupon_type'] = 'type de coupon';
$lang['coupon_uses'] = 'nombre d\'usages';
$lang['coupons'] = 'coupons';
$lang['coupons'] = 'coupons';
$lang['create_affiliate_commission'] = 'créer commission pour affilié';
$lang['create_affiliate_payment'] = 'créer paiement d\'affilié';
$lang['create_custom_commissions'] = 'créer commissions définies';
$lang['create_invoice'] = 'créer facture';
$lang['credit'] = 'crédit';
$lang['credit_added_successfully'] = 'crédit ajouté avec succès';
$lang['credit_amount'] = 'montant crédit';
$lang['credit_amount_updated'] = 'montant crédit mis à jour';
$lang['credit_deleted_successfully'] = 'crédit supprimé avec succès';
$lang['credit_not_used'] = 'le crédit n\'a pas été utilisé';
$lang['credit_overpayment_note'] = 'trop-perçu sur un utilisaeur';
$lang['credit_updated_successfully'] = 'crédit mis à jour avec succès';
$lang['credit_upline'] = 'crediter parrain';
$lang['credit_used'] = 's\'il y a vérification, le crédit a été utilisé jusqu\'à';
$lang['credits_applied_successfully'] = 'crédits pris en compte avec succès';
$lang['cron_backup'] = 'cron pour la sauvegarde de la base de données. il est recommandé de l\'exécuter chaque nuit 1 fois';
$lang['cron_email_queue'] = 'cron pour email en attente d\'envoi. il est recommandé de l\'exécuter chaque 30 minutes';
$lang['cron_follow_ups'] = 'cron pour les suivis des listes de diffusion. il est recommandé de l\'exécuter une fois par jour';
$lang['cron_generate_member_coupons'] = 'cron pour générer les coupons pour membres.  devra être exécuté une fois par mois';
$lang['cron_invoice'] = 'cron pour la création de la facture. obligatoire uniquement si vous avez mis en place le système de souscriptions d\'adhésion. il est recommandé de l\'exécuter une fois par jour';
$lang['cron_notify_update_services'] = 'cron pour les notifications de mise à jour du service . optionnel. il est recommandé de l\'exécuter une fois par jour';
$lang['cron_process'] = 'exécuter quotidiennement les tâches de gestion du cron, il est recommandé de l\'exécuter une fois par jour à 1h du matin';
$lang['cron_settings'] = 'paramètres cron job';
$lang['csv_txt_file_only'] = '.csv ou fichiers .txt files uniquement';
$lang['ctrl_A_select'] = 'ctrl + A pour sélectionner';
$lang['ctrl_click_select'] = 'Ctrl + Clic pour Selectionner';
$lang['currencies'] = 'devises';
$lang['currency'] = 'devise';
$lang['currency_name'] = 'nom de la devise';
$lang['current_date'] = 'date_actuelle';
$lang['current_inventory'] = 'stock actuel';
$lang['current_photo'] = 'photo actuelle';
$lang['current_theme'] = 'actuel';
$lang['current_time'] = 'heure actuelle';
$lang['custom_affiliate_commissions_per_product'] = 'commissions d\'affiliation définies par produit';
$lang['custom_commission_per_affiliate_group'] = 'commission définie par groupe d\'affiliation';
$lang['custom_commission_value_1'] = 'niveau 1 commission';
$lang['custom_commission_value_10'] = 'niveau 10 commission définie';
$lang['custom_commission_value_2'] = 'niveau 2 commission définie';
$lang['custom_commission_value_3'] = 'niveau 3 commission définie';
$lang['custom_commission_value_4'] = 'niveau 4 commission définie';
$lang['custom_commission_value_5'] = 'niveau 5 commission définie';
$lang['custom_commission_value_6'] = 'niveau 6 commission définie';
$lang['custom_commission_value_7'] = 'niveau 7 commission définie';
$lang['custom_commission_value_8'] = 'niveau 8 commission définie';
$lang['custom_commission_value_9'] = 'niveau 9 commission définie';
$lang['custom_css'] = 'personnaliser CSS';
$lang['custom_field_1'] = 'champ défini 1';
$lang['custom_field_10'] ='champ défini 10';
$lang['custom_field_2'] = 'champ défini 2';
$lang['custom_field_3'] = 'champ défini 3';
$lang['custom_field_4'] = 'champ défini 4';
$lang['custom_field_5'] = 'champ défini 5';
$lang['custom_field_6'] = 'champ défini 6';
$lang['custom_field_7'] = 'champ défini 7';
$lang['custom_field_8'] = 'champ défini 8';
$lang['custom_field_9'] = 'champ défini 9';
$lang['custom_form'] = 'formulaire défini';
$lang['custom_form_fields'] = 'champs du formulaire défini';
$lang['custom_group_pricing_numeric'] = 'le prix défini pour un groupe doit avoir une valeur numérique';
$lang['custom_group_pricing_updated_successfully'] = 'prix défini pour groupe mis à jour avec succès';
$lang['custom_id'] = 'iD dédini';
$lang['custom_id_taken'] = 'iD défini pris';
$lang['custom_information'] = 'information définie';
$lang['custom_modules'] = 'modules définis';
$lang['custom_payout_amount'] = 'le montant de décaissement défini pour l\'affilié';
$lang['custom_pricing_per_discount_group'] = 'prix défini pour groupe bénéficiant de rabais';
$lang['custom_url_link'] = 'lien URL défini';
$lang['customer_name'] = 'nom client';
$lang['customers_address_1'] = 'client_adresse_1';
$lang['customers_address_2'] = 'client_adresse_2';
$lang['customers_city'] = 'client_ville';
$lang['customers_company'] = 'client_société';
$lang['customers_country'] = 'client_pays';
$lang['customers_name'] = 'nom du client';
$lang['customers_postal_code'] = 'client_postal_code';
$lang['customers_primary_email'] = 'client_principal_email';
$lang['customers_state'] = 'client_état';
$lang['customers_telephone'] = 'client_téléphone';
$lang['customize'] = 'customize';
$lang['daily'] = 'quotidien';
$lang['daily_ad_tracking_stats_by_month'] = 'stats mensuels de suivi quotidien';
$lang['dashboard'] = 'home';
$lang['dashboard_default_reseller_msg'] = 'Si vous avez une question ou des difficultés, bien vouloir visiter notre site web en cliquant sur le lien ci-dessous:';
$lang['dashboard_get_started_go_to_post_install'] = 'Pour commencer, veuillez parcourir la checklist post-installation afin de pouvoir configurer correctement votre site web';
$lang['dashboard_thanks_for_installing_our_software'] = 'Merci d\'avoir installé notre logiciel!';
$lang['dashboard_visit_our_site'] = 'cliquez ici pour visiter notre site';
$lang['data_export'] = 'exportation données';
$lang['data_feeds'] = 'données entrantes';
$lang['data_import'] = 'importation données';
$lang['data_imported_successfully'] = 'données importées avec succès';
$lang['database_imported_successfully'] = 'base de données importée avec succès';
$lang['date'] = 'date';
$lang['date_added'] = 'date ajout';
$lang['date_available'] = 'date disponibilité';
$lang['date_created'] = 'date création';
$lang['date_expires'] = 'date expiration';
$lang['date_generated'] = 'date où c\'est généré';
$lang['date_modified'] = 'date modification';
$lang['date_paid'] = 'date paiement';
$lang['date_purchased'] = 'date d\'achat';
$lang['day'] = 'jour';
$lang['day'] = 'jour';
$lang['day_of_month'] = 'jour du mois';
$lang['days'] = 'jours';
$lang['days_apart'] = 'jours à part';
$lang['db_backed_up_successfully'] = 'base de données sauvegardée avec succès';
$lang['deactivate_user'] = 'désactiver utilisateur';
$lang['debug_info'] = 'info debogage';
$lang['decimal_places'] = 'chiffre décimal';
$lang['decimal_point'] = 'point décimal';
$lang['default'] = 'par défaut';
$lang['delete'] = 'supprimer';
$lang['delete_admin'] = 'supprimer admin';
$lang['delete_admin_success'] = 'admin supprimé avec succès';
$lang['delete_administrator'] = 'supprimer administrateur';
$lang['delete_aff_ad_categories_success'] = 'catégorie d\'annonce d\'affilié supprimée avec succès';
$lang['delete_affiliate_group'] = 'supprimer groupe d\'affiliation';
$lang['delete_affiliate_payment'] = 'supprimer paiement affilié';
$lang['delete_article_ad_success'] = 'article publicitaire supprimé avec succès';
$lang['delete_attribute'] = 'supprimer attribut';
$lang['delete_bookmark'] = 'supprimer le signet';
$lang['delete_category'] = 'supprimer catégorie';
$lang['delete_checked_comments'] = 'supprimer commentaires vérifiés';
$lang['delete_checked_reviews'] = 'supprimer revues vérifiées';
$lang['delete_commission'] = 'supprimer commission';
$lang['delete_content_article'] = 'supprimer contenu de l\'article';
$lang['delete_content_category'] = 'supprimer catégorie de contenu';
$lang['delete_country'] = 'supprimer pays';
$lang['delete_coupon'] = 'supprimer catégorie de contenu';
$lang['delete_currency'] = 'supprimer devise';
$lang['delete_currency_success'] = 'devise supprimée avec succès';
$lang['delete_discount_group'] = 'supprimer catégorie de remise';
$lang['delete_email_ad_success'] = 'email publictaire supprimé avec succès';
$lang['delete_faq_article'] = 'supprimer catégorie de la FAQ';
$lang['delete_faq_category'] = 'supprimer catégorie de la faq';
$lang['delete_group'] = 'supprimer groupe';
$lang['delete_html_ad_success'] = 'annonce html supprimée avec succès';
$lang['delete_invoice'] = 'supprimer facture';
$lang['delete_invoice_payment'] = 'supprimer facture de règlement';
$lang['delete_language'] = 'supprimer langue';
$lang['delete_mailing_list'] = 'supprimer mailing list';
$lang['delete_manufacturer'] = 'supprimer fournisseur';
$lang['delete_manufacturer_success'] = 'fournisseur supprimé avec succès';
$lang['delete_member'] = 'supprimer membre';
$lang['delete_member_success'] = 'membre ajouté avec succès';
$lang['delete_option_message'] = 'pour supprimer une option, laisser just le nom de l\'option vide et sauvegarder';
$lang['delete_product'] = 'supprimer produit';
$lang['delete_product_category'] = 'supprimer catégorie de produit';
$lang['delete_product_category'] = 'supprimer catégorie de produit';
$lang['delete_product_success'] = 'produit supprimé avec succès';
$lang['delete_region'] = 'supprimer région';
$lang['delete_tax_zone'] = 'supprimer zone fiscale';
$lang['delete_text_ad_success'] = 'annonce textuelle supprimée avec succès';
$lang['delete_text_link_success'] = 'lien textuel supprimé avec succès';
$lang['delete_tracking_url'] = 'supprimer';
$lang['delete_update_files'] = 'assurez-vous de supprimer le fichier à partir de /import/updates folder';
$lang['delete_vendor'] = 'supprimer vendeur';
$lang['delete_viral_pdf_success'] = 'pdf viral supprimé avec succès ';
$lang['delete_viral_video_success'] = 'vidéo virale supprimée avec succès';
$lang['delete_zone'] = 'supprimer zone';
$lang['deleted'] = 'supprimé';
$lang['desc_add_cart_for_price'] = 'on ne peut voir le prix que lorsque le produit est ajouté au panier';
$lang['desc_add_products_to_invoice'] = 'pour ajouter un produit, entrer un ID de produit valide.  si vous voulez utiliser les valeurs par défaut pour le nom et le prix du produit, laissez ces champs vides, ou bien mettez votre propre nom et prix.  finissez par la quantité des produits pour achat';
$lang['desc_add_to_affiliate_group'] = 'lorsque quelqu\'un achète ce produit, il va être ajouté au groupe d\'affiliation sélectionné. Note:  un utilisateur ne peut être membre de plus d\'un groupe d\'affiliation à la fois';
$lang['desc_add_to_discount_group'] = 'lorsque quelqu\'un achète ce produit, il va être ajouté au groupe bénéficiant de remise sélectionné. Note:  un utilisateur ne peut être membre d\'un groupe bénéficiant de rabais à la fois';
$lang['desc_add_to_mailing_list'] = 'lorsque quelqu\'un achète ce produit, il va être ajouté à la liste de diffusion sélectionnée';
$lang['desc_add_to_membership'] = 'ajouter ce produit numérique/incorporel pour que ceux qui ont acheté une souscription d\'adhésion sélectionné aient accès';
$lang['desc_admin_window_height'] = 'la hauteur de la fenêtre pendant que vous gérez les détails de ce module';
$lang['desc_admin_window_width'] = 'la largeur de la fenêtre pendant que vous gérez les détails de ce module';
$lang['desc_alert_admin_article_comment'] = 'envoyer des alertes email lorsque quelqu\'un commente un article';
$lang['desc_alert_admin_product_review'] = 'envoyer des alertes email lorsque quelqu\'un fait la revue d\'un produit';
$lang['desc_alert_downline_signup'] = 'envoyer une alerte email à l\'utilisateurlorsque quelqu\'un prospect référencé s\'inscrit dans son sous-réseau';
$lang['desc_alert_new_commission'] = 'envoyer une alerte email à l\'utilisateur lorsqu\'une nouvelle commission a été générée';
$lang['desc_alert_on_failed_admin_login'] = 'envoyer une alerte email à l\'administrateur lorsque quelqu\'un n\'arrive pas à se logguer dans la console admin';
$lang['desc_alert_on_new_affiliate_commission'] = 'envoyer une alerte email à l\'administrateur lorsqu\'un nouvel affilié génère une commission';
$lang['desc_alert_on_new_affiliate_signup'] = 'envoyer une alerte email à l\'administrateur lorsqu\'un nouvel affilié s\'enregistre dans le programme d\'affiliation';
$lang['desc_alert_on_new_customer_order'] = 'envoyer une alerte email à l\'administrateur lorsqu\'un nouveau client fait une commande';
$lang['desc_alert_on_store_inventory'] = 'envoyer une alerte email à l\'administrateur lorsque le stock d\'un produit est bas';
$lang['desc_alert_payment_sent'] = 'alerter lorsqu\'a lieu le paiement d\'une commission';
$lang['desc_alert_ticket_response'] = 'envoyer une notification email à l\'administrateur lorsqu\'on a répondu à une requêtes';
$lang['desc_allow_downline_email'] = 'autoriser à un utilisateur d\'envoyer directement des emails à ses filleuls';
$lang['desc_allow_downline_view'] = 'autoriser un utilisateur à voir les infos détaillés sur ses filleuls';
$lang['desc_assign_to_member'] = 'entrer l\'identifiant du membre pour l\'assigner à un affilié';
$lang['desc_attribute_callback'] = 'exécuter ce module callback au moment où vous validez ce champ';
$lang['desc_attribute_name'] = 'nom de votre attribut voici ce qui est présenté au client lorsqu\'il regarde votre produit';
$lang['desc_attribute_required'] = 'option obligatoire';
$lang['desc_attribute_status'] = 'si voulez cacher cet attribut mettre en mode inactif';
$lang['desc_attribute_type'] = 'sélectionner le type d\'attribut que voici';
$lang['desc_banner_enable_redirect'] = 'rediriger cette bannière vers une URL différente de celle par défaut à la page d\'accueil';
$lang['desc_category_description'] = 'une description de la catégorie';
$lang['desc_category_keywords'] = 'mots-clés pour votre catégorie';
$lang['desc_category_name'] = 'entrer un nom pour votre catégorie';
$lang['desc_category_path'] = 'sélectioner le chemin du répertoire vers votre catégorie';
$lang['desc_category_status'] = 'mettre cette catégorie en mode inactif pour la dissimuler';
$lang['desc_commission_levels'] = 'les montants de commission à payer par niveau';
$lang['desc_commission_per_levels'] = 'montant de commissions par défaut à payer par niveau';
$lang['desc_commission_type'] = 'commission_type';
$lang['desc_confirm_password'] = 'confirmer mot de passe';
$lang['desc_content_category'] = 'categorie de contenu';
$lang['desc_content_path'] = 'uniquement pour type de contenu avancé. le chemin physique vers votre dossier.  l\'exemple devrait ressembler à /home/public/html/file.php.  seuls .php, .phtml, .html, et les extensions .htm sont autorisés';
$lang['desc_content_type'] = 'sélectionner le contenu standard ou avancé pour utiliser un fichier spécifique';
$lang['desc_country_name'] = 'mettre le nom de votre pays';
$lang['desc_coupon_amount'] = 'montant pour le coupon, entrer une decimale équivalente si vous utilisez les pourcentages.  par exemple, pour 10 pourcent, entrer .10';
$lang['desc_coupon_code'] = 'code de coupon unique';
$lang['desc_coupon_expire_date'] = 'quand le coupon expire';
$lang['desc_coupon_minimum_order'] = 'commande minimum obligatoire avant que le coupon puisse être ajouté';
$lang['desc_coupon_product_type'] = 'restreindre ce coupon aux souscriptions d\'adhésion ou aux produits habituels';
$lang['desc_coupon_recurring'] = 'appliquer le montant de ce coupon à tous les paiements récurrents à l\'instar des souscriptions d\'adhésion';
$lang['desc_coupon_start_date'] = 'début de validité du coupon';
$lang['desc_coupon_type'] = 'montant fixe ou pourcentage';
$lang['desc_coupon_uses'] = 'le nombre de fois que ce coupon a été utilisé';
$lang['desc_credit_upline'] = 'générer des commissions pour le reste des affiliés parrains s\'il y a des';
$lang['desc_cron_backup'] = 'cron pour la sauvegarde des bases de données';
$lang['desc_cron_email_queue'] = 'cron pour emails en cours d\'envoi';
$lang['desc_cron_follow_ups'] = 'cron pour l\'envoi des emails de suivi des listes de diffusion';
$lang['desc_cron_generate_member_coupons'] = 'cron pour générer les coupons pour membres.  devra être exécuté une fois par mois.  exécuter ceci uniquement si vous voulez générer des crédits aux membres sous forme de codes de coupon';
$lang['desc_cron_invoice'] = 'cron pour la création des factures. vous en avez besoin seulement si vous avez configuré le système de souscriptions d\'adhésion';
$lang['desc_cron_notify_update_services'] = 'cron pour notifier des services de mise à jour sur le nouveau contenu';
$lang['desc_cron_process'] = 'exécuter les tâches de gestion quotidienne de cron nécessaires';
$lang['desc_custom_commission_value'] = 'commission définie de ce produit sur ce niveau';
$lang['desc_custom_payout_amount'] = 'si l\'affilié veut être payé un montant différent de la limite qui est configurée par défaut , mettez -le ici';
$lang['desc_custom_pricing_per_discount_group'] = 'fixer un prix défini pour ce produit à chaque groupe bénéficiant de rabais';
$lang['desc_date_available'] = 'sélectionner une date où ce produit sera disponible pour achat';
$lang['desc_date_expires'] = 'sélectionner une date où vous voulez que ce produit expire et ne soit pas diponible pour achat';
$lang['desc_delete_link'] = 'pour supprimer un lien de menu, laisser le nom du lien vide et cliquer \'soumettre\'';
$lang['desc_delete_products_to_invoice'] = 'pour supprimer un produit de la facture, laisser le champ de l\'iD  du produit vide et sauvegarder';
$lang['desc_directory_path'] = 'selectionner le chemin vers cette catégorie';
$lang['desc_discount_group_amount'] = 'attribuer un montant à ce groupe qui soit pris en compte chaque fois qu\'il effectue un achat';
$lang['desc_download_expires_days'] = 'le nombre de jours suivant l\'achat après lesquels le téléchargement expire';
$lang['desc_download_location'] = 'mettre l\'URL ou le chemin physique où se trouve les fichiers téléchargeables.';
$lang['desc_ecommerce_manager_version'] = 'la version actuelle de ce logiciel';
$lang['desc_enable_affiliate_marketing'] = 'autoriser /ou non le marketing d\'affiliation pour cet utilisateur';
$lang['desc_enable_custom_commission'] = 'autoriser des commissions définies de paiement pour ce produit';
$lang['desc_enable_custom_url'] = 'rediriger ce lien d\'affiliation vers une URL personnalisée';
$lang['desc_enable_product_inventory'] = 'autoriser linventaire de stock pour ce produit';
$lang['desc_enable_product_trial'] = 'autoriser un prix à l\'essai pour ce produit';
$lang['desc_enter_address_1'] = 'mettre l\'adresse';
$lang['desc_enter_address_2'] = 'mettre l\'adresse';
$lang['desc_enter_alertpay_id'] = 'mettre l\'iD de AlertPay';
$lang['desc_enter_bank_transfer'] = 'mettre les cordonnées bancaires pour cet affilié ici';
$lang['desc_enter_city'] = 'mettre le  nom de la ville';
$lang['desc_enter_company'] = 'mettre le nom de la société';
$lang['desc_enter_country'] = 'selectionner un pays dans la liste des pays suivants';
$lang['desc_enter_custom_id'] = 'si vous utilisez un ID défini, mettez-le ici';
$lang['desc_enter_custom_url_link'] = 'mettre toute l\'adresse URL en començant par http://';
$lang['desc_enter_fax'] = 'mettre le numéro de fax ';
$lang['desc_enter_first_name'] = 'mettre le prénom';
$lang['desc_enter_home_phone'] = 'mettre téléphone du domicile';
$lang['desc_enter_last_name'] = 'mettre nom';
$lang['desc_enter_mobile_phone'] = 'mettre numéro de téléphone mobile';
$lang['desc_enter_moneybookers_id'] = 'mettre l\'iD de moneybookers';
$lang['desc_enter_payment_name'] = 'mettre le nom utilisé pour le paiement';
$lang['desc_enter_paypal_id'] = 'mettre l\'adresse email paypal';
$lang['desc_enter_postal_code'] = 'mettre le code zip ou postal';
$lang['desc_enter_safepay_id'] = 'mettre l\'iD de safepay';
$lang['desc_enter_sms_contact'] = 'si vous voulez être alerté par sms, mettre l\'email qui convient pour les alertes sms. par exemple 2125551212@my.att.net';
$lang['desc_enter_sponsor'] = 'mettre l\'iD du sponsor qui a parrainé ce membre';
$lang['desc_enter_state'] = 'mettre l\'Etat ou la Province';
$lang['desc_enter_valid_email'] = 'mettre une adresse email valide';
$lang['desc_enter_website'] = 'mettre le site internet de cet affilié';
$lang['desc_enter_work_phone'] = 'mettre le numéro de téléphone professionnel';
$lang['desc_faq_category'] = 'catégorie de la FAQ';
$lang['desc_file_path'] = 'si vous voulez importer en utilisant le fichier qui se trouve sur votre serveur, mettre tout le chemin physique qui y mène ici';
$lang['desc_free_shipping'] = 'offrir une expédition gratuite pour ce produit';
$lang['desc_group_description'] = 'une courte description de votre groupe';
$lang['desc_group_name'] = 'mettre un nom pour votre groupe';
$lang['desc_installation_code'] = 'code d\'installation exigé pendant que vous achetez une licence';
$lang['desc_iso_2'] = 'entrer le code ISO2 pour ce pays';
$lang['desc_iso_3'] = 'entrer le code ISO3 pour ce pays';
$lang['desc_layout_box_description'] = 'description du box de mise en page';
$lang['desc_layout_box_file_name'] = 'nom du fichier du box.  ne pas inclure le "box_" au début du fichier ou ".php" à la fin';
$lang['desc_layout_box_name'] = 'mise en page_box_nom';
$lang['desc_layout_design_custom_css'] = 'si voulez utiliser votre propre code défini CSS, mettez le ici en entier et il sera appliqué à votre site';
$lang['desc_layout_design_custom_home_page_redirect'] = 'si vous voulez rediriger votre page d\'accueil vers une URL différente, mettez la totalité de cettte URL ici';
$lang['desc_layout_design_enable_footer_menu'] = 'autoriser / ou non les liens de pied de page de votre site';
$lang['desc_layout_design_enable_left_column_checkout'] = 'autoriser/ ou non les espaces de colonne gauche/droite dans l\'espace caisse';
$lang['desc_layout_design_enable_left_column_content_pages'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur le contenu de page';
$lang['desc_layout_design_enable_left_column_faq_pages'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur les pages de la FAQ';
$lang['desc_layout_design_enable_left_column_home_page'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur la page d\'accueil';
$lang['desc_layout_design_enable_left_column_members_area'] = 'autoriser/ ou non les espaces de colonne gauche/droite dans l\'espace membre';
$lang['desc_layout_design_enable_left_column_product_details'] = 'autoriser/ ou non les espaces de colonne gauche/droite dans les pages relatives au détail sur le produit';
$lang['desc_layout_design_enable_left_column_profile_pages'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur les pages de profil d\'affiliés';
$lang['desc_layout_design_enable_left_column_standard_pages'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur les pages web';
$lang['desc_layout_design_enable_left_column_store'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur votre boutique';
$lang['desc_layout_design_enable_left_column_support_desk'] = 'autoriser/ ou non les espaces de colonne gauche/droite dans le centre d\'assistance et de support';
$lang['desc_layout_design_enable_right_checkout'] = 'autoriser/ ou non les espaces de colonne gauche/droite dans l\'espace réservé à la caisse';
$lang['desc_layout_design_enable_right_column_home_page'] = 'autoriser/ ou non l\'espace de colonne droite sur ta page d\'accueil';
$lang['desc_layout_design_enable_right_column_store'] = 'autoriser/ ou non les espaces de colonne gauche/droite sur ta boutique';
$lang['desc_layout_design_enable_right_content_pages'] = 'autoriser/ ou non les espaces de colonne droite sur les pages de contenu';
$lang['desc_layout_design_enable_right_members_area'] = 'autoriser/ ou non l\'espace de colonne droite dans l\'espace membre';
$lang['desc_layout_design_enable_right_product_details'] = 'autoriser/ ou non l\'espace de colonne droite dans les pages relatives au détail sur le produit';
$lang['desc_layout_design_enable_right_support_desk'] = 'autoriser/ ou non l\'espace de colonne droite dans le centre d\'assistance et de support';
$lang['desc_layout_design_enable_top_menu'] = 'autoriser/ ou non le top menu dans votre site';
$lang['desc_layout_design_header_name'] = 'mettre le nom de votre site si vous n\'utilisez pas de bannière';
$lang['desc_layout_design_header_tag_line'] = 'si vous voulez utiliser un tag dans votre en-tête, mettez-le ici';
$lang['desc_layout_design_home_page_redirect'] = 'configurer votre page d\'accueil pour montrer un certain module';
$lang['desc_layout_design_site_logo'] = 'le logo de votre site';
$lang['desc_layout_design_site_logo_height'] = 'la hauteur de l\'image de votre logo';
$lang['desc_layout_design_site_logo_width'] = 'la largeur de l\'image de votre logo';
$lang['desc_login_for_price'] = 'l\'utilisateur doit se logguer pour voir le prix';
$lang['desc_login_status'] = 'le compte a été vérifié par email';
$lang['desc_mailing_list_name'] = 'nom de la liste de diffusion';
$lang['desc_manufacturer'] = 'mettre le nom du fournisseur';
$lang['desc_manufacturer_description'] = 'mettre une description de votre fournisseur';
$lang['desc_manufacturer_page_title'] = 'mettre le titre de la page du fournisseur';
$lang['desc_max_downloads_per_user'] = 'le maximum de fois qu\'un utilisateur peut télécharger un produit';
$lang['desc_max_quantity_ordered'] = 'le maximum de fois que l\'on peut passer la commande à la fois';
$lang['desc_membership_url_redirect'] = 'URL de redirection lorsqu\'un utilisateur se loggue pour une souscription d\'adhésion';
$lang['desc_membership_products'] = 'produits de souscription d\'adhésion';
$lang['desc_meta_description'] = 'mettre votre méta description ici';
$lang['desc_meta_keywords'] = 'mettre vos méta mots ici';
$lang['desc_min_quantity_ordered'] = 'la quantité minimum à commander';
$lang['desc_module_affiliate_marketing_banners_file_types'] = 'types de fichiers autorisés à être chargés dans votre bannière. séparer chaque instension avec 1 |';
$lang['desc_module_affiliate_marketing_html_ads_default_bg_body_color'] = 'couleur de fond par défaut du texte';
$lang['desc_module_affiliate_marketing_html_ads_default_bg_title_color'] = 'couleur de fond par défaut du titre';
$lang['desc_module_affiliate_marketing_html_ads_default_border_color'] = 'couleur par défaut du pourtous';
$lang['desc_module_affiliate_marketing_html_ads_default_font_body_color'] = 'couleur de face par défaut du texte';
$lang['desc_module_affiliate_marketing_html_ads_default_font_title_color'] = 'couleur de face par défaut du titre';
$lang['desc_module_affiliate_marketing_html_ads_default_html_ad_width'] = 'largeur par défaut de l\'annonce html';
$lang['desc_module_affiliate_marketing_text_ads_default_bg_body_color'] = 'couleur de fond par défaut du texte';
$lang['desc_module_affiliate_marketing_text_ads_default_bg_title_color'] = 'couleur de fond par défaut du titre';
$lang['desc_module_affiliate_marketing_text_ads_default_border_color'] = 'couleur par défaut du pourtous';
$lang['desc_module_affiliate_marketing_text_ads_default_font_body_color'] = 'couleur de face par défaut du texte';
$lang['desc_module_affiliate_marketing_text_ads_default_font_title_color'] = 'couleur de face par défaut du titre';
$lang['desc_module_affiliate_marketing_text_ads_default_text_ad_width'] = 'largeur de l\'annonce publicitaire par défaut';
$lang['desc_module_data_import_jam_affiliate_limit'] = 'la limite à donner aux rangées d\'affiliés';
$lang['desc_module_data_import_jam_affiliate_offset'] = 'le nombre de rangées pour commencer une requête';
$lang['desc_module_data_import_jam_commission_limit'] = 'la limite à donner au nombre de rangées';
$lang['desc_module_data_import_jam_commission_offset'] = 'le nombre de rangées pour commencer une requête';
$lang['desc_module_data_import_jam_payment_limit'] = 'la limite à donner au nombre de rangées';
$lang['desc_module_data_import_jam_payment_offset'] = 'le nombre de rangées pour commencer une requête';
$lang['desc_module_data_import_jam_segment_affiliates'] = 'autoriser une migration segmentée pour le tableau d\'affiliés';
$lang['desc_module_data_import_jam_segment_commissions'] = 'autoriser une migration segmentée pour le tableau de commissions';
$lang['desc_module_data_import_jam_segment_payments'] = 'autoriser une migration segmentée pour le tableau de paiements';
$lang['desc_module_data_import_jam_segment_traffic'] = 'autoriser une migration segmentée pour le tableau de traffic';
$lang['desc_module_data_import_jam_traffic_limit'] = 'la limite à donner au nombre de rangées';
$lang['desc_module_data_import_jam_traffic_offset'] = 'le nombre de rangées pour commencer une requête';
$lang['desc_module_data_import_mass_edit_products_delimiter'] = 'délimiteur de fichiers';
$lang['desc_module_data_import_mass_edit_products_heading_first_row'] = 'mettre la première rangée comme titre';
$lang['desc_module_data_import_mass_edit_products_use_skus'] = 'utiliser les SKUs de produit au lieu des IDs';
$lang['desc_module_data_import_members_delimiter'] = 'délimiteur de fichiers';
$lang['desc_module_data_import_members_generate_new_ids'] = 'générer de nouveaux IDs';
$lang['desc_module_data_import_members_heading_first_row'] = 'mettre la première rangée comme titre';
$lang['desc_module_data_import_product_images_delete_photos'] = 'autoriser cette option fera que le système essayera de supprimer l\'image provenant du classeur /import/products après avoir été importée';
$lang['desc_module_data_import_product_images_use_skus'] = 'utiliser les SKUs de produit au lieu des IDs';
$lang['desc_module_data_import_products_delimiter'] = 'délimiteur à utiliser pour votre fichier';
$lang['desc_module_data_import_products_generate_new_ids'] = 'générer de nouveaux IDs de rangées pour chaque rangée';
$lang['desc_module_data_import_products_heading_first_row'] = 'mettre la première rangée en temps que titre de la colonne';
$lang['desc_module_description'] = 'description de l\'usage de ce module';
$lang['desc_module_file_name'] = 'nom du fichier du module';
$lang['desc_module_name'] = 'nom du module';
$lang['desc_module_payment_gateway_2checkout_enable_debug_email'] = 'déboggage d\'email';
$lang['desc_module_payment_gateway_2checkout_enable_testing'] = 'autoriser l\'option de démo pour 2Checkout';
$lang['desc_module_payment_gateway_2checkout_id'] = '2Checkout Id';
$lang['desc_module_payment_gateway_2checkout_md5_hash'] = 'autoriser le hash de sécurité md5 de 2Checkout';
$lang['desc_module_payment_gateway_2checkout_url'] = 'URL du formulaire de 2Checkout';
$lang['desc_module_payment_gateway_alertpay_currency_code'] = 'code de devise AlertPay';
$lang['desc_module_payment_gateway_alertpay_disable_verification'] = 'si vous voulez autoriser le code de sécurité AlertPay, mettez-le ici';
$lang['desc_module_payment_gateway_alertpay_email'] = 'alertPay account';
$lang['desc_module_payment_gateway_alertpay_enable_debug_email'] = 'autoriser l\'email de mise au point';
$lang['desc_module_payment_gateway_alertpay_url'] = 'URL du formulaire AlertPay form URl';
$lang['desc_module_payment_gateway_authorize_net_aim_authorization_type'] = 'sélectionner le type de permission';
$lang['desc_module_payment_gateway_authorize_net_aim_enable_arb'] = 'autoriser l\'option de Facturation récurrente de Authorize.Net';
$lang['desc_module_payment_gateway_authorize_net_aim_enable_debug_email'] = 'autoriser cette option pour envoyer le mail de mise au point';
$lang['desc_module_payment_gateway_authorize_net_aim_enable_testing'] = 'autoriser l\'URL de test';
$lang['desc_module_payment_gateway_authorize_net_aim_md5_hash'] = 'si voulez utiliser le hash de md5, entrer la valeur du hash ici';
$lang['desc_module_payment_gateway_authorize_net_aim_merchant_id'] = 'code marchand Authorize.Net';
$lang['desc_module_payment_gateway_authorize_net_aim_require_cvv'] = 'autoriser le code de sécurité / CVV code';
$lang['desc_module_payment_gateway_authorize_net_aim_send_customer_notification'] = 'permettre à Authorize.Net d\'envoyer un email de notification au client';
$lang['desc_module_payment_gateway_authorize_net_aim_send_merchant_notification'] = 'permettre à Authorize.Net d\'envoyer un email de notification au marchand';
$lang['desc_module_payment_gateway_authorize_net_aim_transaction_key'] = 'clé de transaction Authorize.Net';
$lang['desc_module_payment_gateway_authorize_net_aim_url'] = 'URL de paiement Authorize.Net pour poster les données à';
$lang['desc_module_payment_gateway_chargeback_guardian_authorization_type'] = 'type de permission';
$lang['desc_module_payment_gateway_chargeback_guardian_enable_debug_email'] = 'autoriser un email de mise au point';
$lang['desc_module_payment_gateway_chargeback_guardian_password'] = 'mot de passe de rétraction de transaction';
$lang['desc_module_payment_gateway_chargeback_guardian_require_cvv'] = 'autoriser le champ CVV';
$lang['desc_module_payment_gateway_chargeback_guardian_url'] = 'URL de paiement';
$lang['desc_module_payment_gateway_chargeback_guardian_username'] = 'identifiant de rétraction guardian';
$lang['desc_module_payment_gateway_egold_enable_debug_email'] = 'autoriser un email de mise au point';
$lang['desc_module_payment_gateway_egold_id'] = 'iD e-gold';
$lang['desc_module_payment_gateway_egold_payment_units'] = 'unités de paiement . par défaut c\'est 1';
$lang['desc_module_payment_gateway_egold_secret_word'] = 'mot secret ou alternate passphrase de e-Gold';
$lang['desc_module_payment_gateway_egold_url'] = 'URL formulaire e-gold';
$lang['desc_module_payment_gateway_eway_enable_debug_email'] = 'autoriser un email de mise au point';
$lang['desc_module_payment_gateway_eway_enable_testing'] = 'autoriser le test';
$lang['desc_module_payment_gateway_eway_merchant_id'] = 'iD terminal marchand';
$lang['desc_module_payment_gateway_eway_payment_method'] = 'méthode de paiement, les options sont REAL_TIME, REAL_TIME_CVN, GEO_IP_ANTI_FRAUd';
$lang['desc_module_payment_gateway_eway_require_cvv'] = 'exiger le code de sécurité CVV';
$lang['desc_module_payment_gateway_eway_url'] = 'URL du formulaire du terminal';
$lang['desc_module_payment_gateway_firstdata_connect_enable_debug'] = 'autoriser la mise au point';
$lang['desc_module_payment_gateway_firstdata_connect_enable_debug_email'] = 'autoriser un email de mise au point';
$lang['desc_module_payment_gateway_firstdata_connect_mode'] = 'le mode peut être payplus, fullpay, ou payonly';
$lang['desc_module_payment_gateway_firstdata_connect_storename'] = 'nom de la boutique';
$lang['desc_module_payment_gateway_firstdata_connect_txntype'] = 'Le type de transaction peut être sale, preauth, postauth';
$lang['desc_module_payment_gateway_firstdata_connect_url'] = 'URL de connexion';
$lang['desc_module_payment_gateway_liberty_reserve_currency_code'] = 'code de la devise.  par défaut c\'est LRUSd';
$lang['desc_module_payment_gateway_liberty_reserve_enable_debug_email'] = 'autoriser un email de mise au point';
$lang['desc_module_payment_gateway_liberty_reserve_id'] = 'liberty reserve Id';
$lang['desc_module_payment_gateway_liberty_reserve_secret_word'] = 'mot secret de vérification';
$lang['desc_module_payment_gateway_liberty_reserve_store_name'] = 'nom boutique';
$lang['desc_module_payment_gateway_liberty_reserve_url'] = 'URL du formulaire de soumission';
$lang['desc_module_payment_gateway_nochex_disable_verification'] = 'désactiver la verification';
$lang['desc_module_payment_gateway_nochex_enable_debug_email'] = 'autoriser déboggage d\'email';
$lang['desc_module_payment_gateway_nochex_enable_testing'] = 'autoriser le mode test';
$lang['desc_module_payment_gateway_nochex_id'] = 'NoChex Id';
$lang['desc_module_payment_gateway_nochex_url'] = 'NoChex form URl';
$lang['desc_module_payment_gateway_offline_payment_download_link'] = 'relier à votre bon de commande hors site, tels les docs Word ou les fichiers PDF';
$lang['desc_module_payment_gateway_offline_payment_message'] = 'message à montrer sur la page de remerciements pour les instructions afférantes au mode de paiement hors site';
$lang['desc_module_payment_gateway_paypal_standard_currency_code'] = 'code de devise paypal';
$lang['desc_module_payment_gateway_paypal_standard_disable_verification'] = 'désactiver l\'option de vérification IPN pour les paiements paypal. nous vous suggérons de laisser ceci activé à moins que vous n\'y trouviez un inconvénient';
$lang['desc_module_payment_gateway_paypal_standard_email'] = 'email paypal';
$lang['desc_module_payment_gateway_paypal_standard_enable_debug_email'] = 'autoriser cette option pour envoyer un email de déboggage';
$lang['desc_module_payment_gateway_paypal_standard_header_image'] = 'fichier d\'image à placer au dessus de la page de paiement paypal.  rassurez-vous que vous utilisez une URL sécurisée (https://)';
$lang['desc_module_payment_gateway_paypal_standard_url'] = 'URL formulaire paypal';
$lang['desc_module_payment_gateway_psigate_enable_debug_email'] = 'autoriser un email de déboggage';
$lang['desc_module_payment_gateway_psigate_enable_testing'] = 'autoriser mode test';
$lang['desc_module_payment_gateway_psigate_merchant_id'] = 'iD marchand PSIGate merchant';
$lang['desc_module_payment_gateway_psigate_passphrase'] = 'PSIGate passphrase';
$lang['desc_module_payment_gateway_psigate_require_cvv'] = 'exiger code de sécurité CVV';
$lang['desc_module_payment_gateway_psigate_url'] = 'URL de formulaire de soumission PSIGate';
$lang['desc_module_shipping_by_percent_amount'] = 'montant du pourcentage par vente';
$lang['desc_module_shipping_by_percent_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['desc_module_shipping_by_percent_handling_fee'] = 'frais de traitement';
$lang['desc_module_shipping_by_percent_tax_zone'] = 'sélectionner zone fiscale';
$lang['desc_module_shipping_flat_rate_amount'] = 'montant fixe à imputer au total des achats';
$lang['desc_module_shipping_flat_rate_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['desc_module_shipping_flat_rate_handling_fee'] = 'frais de traitement';
$lang['desc_module_shipping_flat_rate_tax_zone'] = 'sélectionner zone fiscale';
$lang['desc_module_shipping_free_shipping_enable_tax_zone'] = 'utiliser zone ficale';
$lang['desc_module_shipping_free_shipping_handling_fee'] = 'frais de traitement';
$lang['desc_module_shipping_free_shipping_quantity_type'] = 'selectionner la méthode de calcul de l\'option d\'expédition gratuite';
$lang['desc_module_shipping_free_shipping_required_amount'] = 'le montant que doit avoir le client avant de pouvoir bénéficier de l\'expédition gratuite';
$lang['desc_module_shipping_free_shipping_tax_zone'] = 'sélectionner zone fiscale';
$lang['desc_module_shipping_per_item_amount'] = 'montant à imputer pour chaque objet à expédier';
$lang['desc_module_shipping_per_item_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['desc_module_shipping_per_item_handling_fee'] = 'frais de traitement';
$lang['desc_module_shipping_per_item_tax_zone'] = 'sélectionner zone ficale';
$lang['desc_module_shipping_UPS_delivery_type'] = 'selectioner le type de livraison de UPS- RES pour résidentiel or COM pour commercial';
$lang['desc_module_shipping_UPS_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['desc_module_shipping_UPS_handling_fee'] = 'frais de traitement';
$lang['desc_module_shipping_UPS_packaging'] = 'the type de colissage UPS à utiliser ';
$lang['desc_module_shipping_UPS_pickup_method'] = 'type de méthode de livraison à utiliser';
$lang['desc_module_shipping_UPS_shipping_types'] = 'sélectionner le type de livraison que vous voulez utiliser';
$lang['desc_module_shipping_UPS_tax_basis'] = 'base d\'imputation de la taxe - adresse de facturation ou adresse d\'expédition';
$lang['desc_module_shipping_UPS_tax_zone'] = 'zone fiscale à utiliser pour imputer les taxes d\'expédition';
$lang['desc_module_shipping_USPS_container'] = 'container';
$lang['desc_module_shipping_USPS_domestic_shipping_types'] = 'types d\'expédition nationale';
$lang['desc_module_shipping_USPS_enable_tax_zone'] = 'utiliser la zone fiscale';
$lang['desc_module_shipping_USPS_gateway_url'] = 'URL terminal';
$lang['desc_module_shipping_USPS_handling_fee'] = 'frais de traitement';
$lang['desc_module_shipping_USPS_international_shipping_types'] = 'types d\'expédition à l\'international';
$lang['desc_module_shipping_USPS_machinable'] = 'machinable';
$lang['desc_module_shipping_USPS_max_weight'] = 'poids maximum du colis';
$lang['desc_module_shipping_USPS_size'] = 'volume';
$lang['desc_module_shipping_USPS_tax_zone'] = 'sélectionner zone fiscale';
$lang['desc_module_shipping_USPS_userid'] = 'identifiant  USPS';
$lang['desc_module_shipping_zone_based_calculation_type'] = 'sélectionner la méthode de calcul à utiliser: objet, prix, ou poids';
$lang['desc_module_shipping_zone_based_enable_tax_zone'] = 'utiliser la zone fiscale';
$lang['desc_module_shipping_zone_based_skip_countries'] = 'Par exemple: US,CA,GB';
$lang['desc_module_shipping_zone_based_tax_calc_type'] = 'sélectionner le type d\'adresse sur lequel doit être basé le calcul des frais de port';
$lang['desc_module_shipping_zone_based_tax_zone'] = 'selectionner la zone fiscale pour ajouter la taxe d\'expédition';
$lang['desc_module_shipping_zone_based_zone_1_countries'] = 'Par exemple: US,CA,GB';
$lang['desc_module_shipping_zone_based_zone_1_handling_fee'] = 'ajouter des frais de traitement à toutes les expéditions à destination de cette zone';
$lang['desc_module_shipping_zone_based_zone_1_table'] = 'Frais de port vers les destinations de la Zone 1 basés sur un ensemble de commandes maximum en poids/prix. Par Exemple: 3:8.50,7:10.50,...Poids/Prix inférieur ou égal à 3 devra coûter 8.50 pour les destinations  de la Zone 1.';
$lang['desc_module_shipping_zone_based_zone_2_countries'] = 'liste séparée de virgule des codes ISO à deux caractères des pays faisant partie de la zone 2. Mettre  00 pour indiquer tous les codes ISO pays à 2 caractères qui ne sont pas définis de façon spécifique.';
$lang['desc_module_shipping_zone_based_zone_2_handling_fee'] = 'ajouter des frais de traitement à toutes les expéditions à destinatioin de cette zone';
$lang['desc_module_shipping_zone_based_zone_2_table'] = 'Frais de port vers les destinations de la Zone 2 basés sur un ensemble de commandes maximum en poids/prix. Par Exemple: 3:8.50,7:10.50,...Poids/Prix inférieur ou égal à 3 devra coûter 8.50 pour les destination  de la Zone 2';
$lang['desc_module_shipping_zone_based_zone_3_countries'] = 'liste séparée de virgule des codes ISO à deux caractères des pays faisant partie de la zone 2. Mettre  00 pour indiquer tous les codes ISO pays à 2 caractères qui ne sont pas définis de façon spécifique.';
$lang['desc_module_shipping_zone_based_zone_3_handling_fee'] = 'ajouter des frais de traitement à toutes les expéditions à destinatioin de cette zone';
$lang['desc_module_shipping_zone_based_zone_3_table'] = 'Frais de port vers les destinations de la Zone 3 basés sur un ensemble de commandes maximum en poids/prix. Par Exemple: 3:8.50,7:10.50,... Par Exemple: 3:8.50,7:10.50,...Poids/Prix inférieur ou égal à 3 devra coûter 8.50 pour les destination  de la Zone 3.';
$lang['desc_module_shipping_zone_based_zone_4_handling_fee'] = 'frais de traitement pour la zone 4';
$lang['desc_module_shipping_zone_based_zone_4_table'] = 'Frais de port pour tous les autres pays non listés dans la Zone 1,2, ou 3';
$lang['desc_module_type'] = 'le type de module comment cela est censé fonctionner';
$lang['desc_mysql_version'] = 'la version MySQL que vous utilisez actuellement';
$lang['desc_no_commission'] = 'ne pas imputer de commission pour ce produit';
$lang['desc_notify_update_services'] = 'envoyer une notification maintenant pour mettre à jour les services ou bien laisser les tâches programmées le faire plus tard.  l\'effectuer maintenant pourrait prendre du temps après avoir sauvegardé cet artice';
$lang['desc_payment_name'] = 'mettre le nom de l\'utilisateur pour le paiement des commissions';
$lang['desc_payment_preference_amount'] = 'le montant que votre membre veut d\'abord cumuler avant de se faire payer';
$lang['desc_photo_file'] = 'le fichier de la photo, s\'il y en a une';
$lang['desc_php_version'] = 'la version PHP actuelle que vous utilisez';
$lang['desc_post_to_ping_fm'] = 'poster à Ping.FM';
$lang['desc_product_featured'] = 'montrer sur la page des produits VIP et sur la page d\'accueil';
$lang['desc_product_inventory'] = 'mettre la quantité du  produit en stock';
$lang['desc_product_keywords'] = 'séparer les mots-clés de ce produit par une virgule';
$lang['desc_product_meta_description'] = 'description méta tag de votre produit';
$lang['desc_product_name'] = 'nom de votre produit';
$lang['desc_product_new'] = 'montrer sur la page des nouveaux produits';
$lang['desc_product_price'] = 'prix de votre  produit';
$lang['desc_product_title'] = 'titre de votre produit';
$lang['desc_product_trial_interval'] = 'intervalle d\'essai';
$lang['desc_product_trial_interval_type'] = 'intervalle d\'essai tel que hebdomadaire, mensuel, ou annuel';
$lang['desc_product_trial_price'] = 'prix pour l\'option d\'essai';
$lang['desc_product_type'] = 'type de produit, tel que produit physique, téléchargement de produit numérique, ou souscription d\'adhésion';
$lang['desc_product_views'] = 'le nombre total de pages vues pour ce produit';
$lang['desc_product_weight'] = 'le poids du produit';
$lang['desc_profile_description'] = 'courte description faite par les utilisateurs sur page de profil';
$lang['desc_publish_wordpress'] = 'si ceci est destiné à un blog, vous pouvez le publier sur votre blog WordPress';
$lang['desc_recommended_product'] = 'montrer sur la page recommandée';
$lang['desc_recurring_interval'] = 'intervalle récurrent';
$lang['desc_recurring_interval_ends'] = 'terminer les paiements après un certain nombre d\'intervalles';
$lang['desc_recurring_interval_type'] = 'intervalle récurrent tel que hebdomadaire, mensuel, ou annuel';
$lang['desc_recurring_onetime'] = 'imputer des frais uniquement pour une souscription unique';
$lang['desc_redirect_custom_url'] = 'rediriger l\'url défini vers cet outil marketing';
$lang['desc_region_code'] = 'mettre code région';
$lang['desc_region_name'] = 'nom région';
$lang['desc_require_membership'] = 'souscription d\'adhésion obligatoire pour accéder à ce contenu';
$lang['desc_required_password_length'] = 'les caractères sont obligatoires pour le mot de passe';
$lang['desc_required_username_length'] = 'les caractères sont obligatoires pour l\'identifiant';
$lang['desc_rotator_group'] = 'mettre cette bannière dans l\'option de bannière rotative';
$lang['desc_rows_per_page'] = 'vous pouvez spécifier le nombre de rangées de données à faire voir par défaut à l\'utilisateur';
$lang['desc_select_affiliate_group'] = 'sélectionner un groupe d\'affiliation pour cet utilisateur';
$lang['desc_select_country'] = 'sélectionner un pays';
$lang['desc_select_discount_group'] = 'sélectionner un groupe bénéficiant de rabais pour cet utilisateur';
$lang['desc_select_email_mailing_lists'] = 'sélectionner les listes de diffusion auxquelles cet utilisaleur va être inscrit';
$lang['desc_select_expires_module'] = 'exécuter ce module lorsque le compte expire.';
$lang['desc_select_manufacturer'] =  'sélectionner le fournisseur';
$lang['desc_select_membership_products'] = 'selectionner les produits d\'adhésion dont cet utilisateur va être membre';
$lang['desc_select_module'] = 'selectionner un module à exécuter une fois que quelqu\'un paie pour ce produit';
$lang['desc_select_payment_preference'] = 'selectionner la préférence de décaissement pour cet utilisateur';
$lang['desc_select_payment_preference_amount'] = 'si vous voulez établir une limite différente pour les commissions de cet utilisateur, entrer le montant ici';
$lang['desc_select_permissions_for_admin'] = 'sélectionner les autorisations que vous ne voulez pas donner à cet administrateur';
$lang['desc_select_post_purchase_module'] = 'si vous voulez exécuter un module après que le paiement ait été effectué pour ce produit, sélectionnez-le ici';
$lang['desc_select_regions'] = 'sélectionner les régions pour cette zone fiscale';
$lang['desc_select_signup_module'] = 'si vous voulez éxécuter un module chaque fois qu\'un membre se loggue, sélectionnez-le ici';
$lang['desc_select_vendor'] = 'sélectionner un vendeur pour ce produit. si vous avez autorisé l\'alerte pour vendeur, le système va envoyer au vendeur un email lorsque quelqu\'un achète ce produit, tout comme pour le dropshipping';
$lang['desc_send_email_alert'] = 'envoyer à l\'affilié et son parrain (upline) une alerte commission';
$lang['desc_send_sms_alerts'] = 'envoyer une alerte sms ensemble avec l\'alerte email correspondante';
$lang['desc_send_welcome_email'] = 'envoyer à l\'utilisateur un email de bienvenue avec ses données de connexion';
$lang['desc_send_welcome_email'] = 'envoyer au membre un email de bienvenue avec les données de connexion';
$lang['desc_server_software'] = 'serveur logiciel';
$lang['desc_show_affiliate'] = 'montrer le box seulement si l\'outil de suivi d\'affilié est autorisé';
$lang['desc_show_assigned_tickets_only'] = 'montrer à cet admin uniquement les requêtes au support qui lui sont adressées';
$lang['desc_show_on_login'] = 'montrer le box uniquement si un utilisateur est loggué';
$lang['desc_status_updates'] = 'Statut mise à jour du texte';
$lang['desc_status_user'] = 'mettre cet utilisateur en mode actif ou inactif';
$lang['desc_sts_admin_date_format'] = 'format de la date dans l\'espace d\'admin';
$lang['desc_sts_admin_default_language'] = 'la langue par défaut utilisée dans l\'espace d\'admin';
$lang['desc_sts_admin_enable_hostip_lookup'] = 'autoriser ou non la capacité à montrer de quelle ville ou de quel pays l\'admin s\'est loggué';
$lang['desc_sts_admin_enable_wysiwyg_content'] = 'autoriser ou non l\'éditeur wysiwyg dans l\'espace de l\'admin pour la gestion du contenu';
$lang['desc_sts_admin_enable_wysiwyg_email'] = 'autoriser ou non l\'éditeur wysiwyg dans l\'espace admin c\'est pour les emails';
$lang['desc_sts_admin_image_height'] = 'hauteur des images de l\'admin lorsqu\'elles sont redimensionnées';
$lang['desc_sts_admin_image_resize'] = 'redimensionner automatiquement les images de l\'admin à la hauteur et à la largeur spécifiées ci-dessous';
$lang['desc_sts_admin_image_width'] = 'largeur des images de l\'admin redimensionnée';
$lang['desc_sts_admin_time_format'] = 'format d\'heure utilisant le format d\'heure standard PHP.  veuillez consulter www.php.net/date.  par défaut c\'est:m-d-Y h:i:s A';
$lang['desc_sts_affiliate_admin_approval_required'] = 'les utilisateurs qui font la demande pour devenir des affiliés doivent d\'abord être approuvés par l\'admin';
$lang['desc_sts_affiliate_alert_downline_signup'] = 'envoyer une alerte email au membre lorsque quelqu\'un s\'enregistre dans son réseau d\'affiliation';
$lang['desc_sts_affiliate_alert_new_commission'] = 'envoyer une alerte email à l\'affilié lorsqu\'une commission est générée';
$lang['desc_sts_affiliate_alert_payment_sent'] = 'envoyer une alerte email à un affilié lorsque sa facture de règlement est marquée comme payée';
$lang['desc_sts_affiliate_allow_downline_email'] = 'autoriser l\'affilié à envoyer des emails à ses filleuls';
$lang['desc_sts_affiliate_allow_downline_view'] = 'autoriser l\'affilié à visualiser ses filleuls dans un format graphique';
$lang['desc_sts_affiliate_allow_expandable_downlines'] = 'autoriser un membre à avoir une vue détaillée sur chaque filleul référencé dans son réseau';
$lang['desc_sts_affiliate_allow_upload_photos'] = 'autoriser l\'affilié à charger sa photo dans son compte';
$lang['desc_sts_affiliate_auto_approve_commissions'] = 'auto-approuver les commissions après un certain nombre de jours, mettre le nombre de jours ici. mettre zéro (0) pour désactiver';
$lang['desc_sts_affiliate_auto_generate_coupon'] = 'générer les coupons d\'affiliation qui peuvent être utilisés pour les promotions';
$lang['desc_sts_affiliate_auto_generate_coupon_amount'] = 'montant utilisé pour le coupon de remise d\'affiliation';
$lang['desc_sts_affiliate_auto_generate_coupon_credits'] = 'si un membre dispose de crédit, cette option va générer pour lui chaque mois des coupons équivalents';
$lang['desc_sts_affiliate_auto_generate_coupon_recurring'] = 'permettre d\'utiliser le coupon d\'affiliation uniquement pour les souscriptions d\'adhésion';
$lang['desc_sts_affiliate_auto_generate_coupon_type'] = 'faire de ceci un coupon basé sur un taux fixe ou un pourcentage';
$lang['desc_sts_affiliate_banner_directory'] = 'lieu où les bannières des affiliés vont être chargées.  soyez sûr de disposer des autorisations qu\'il faut pour charger les fichiers dans ce classeur';
$lang['desc_sts_affiliate_categories_per_page'] = 'le nombre de catégories d\'outils marketing à montrer sur une page';
$lang['desc_sts_affiliate_commission_levels'] = 'le nombre de niveaux d\'affiliation que vous voulez attribuer à votre programme';
$lang['desc_sts_affiliate_commission_levels_restrict_view'] = 'montrer seulement un certain nombre de niveaux pendant qu\'on visualise les filleuls';
$lang['desc_sts_affiliate_commission_type'] = 'type de commission d\'affiliation, en taux fixe ou en pourcentage';
$lang['desc_sts_affiliate_cookie_timer'] = 'établir le nombre de jours pendant lesquels le cookie de traçage d\'affiliation sera actif';
$lang['desc_sts_affiliate_copy_path'] = 'classeur de reproduction à partir duquel copier les fichiers de templates';
$lang['desc_sts_affiliate_coupon_expires'] = 'le coupon d\'affiliation expire après le nombre de jours fixé ici';
$lang['desc_sts_affiliate_coupon_limit_use'] = 'limiter le nombre de fois qu\'un coupon d\'affiliation peut être utilisé';
$lang['desc_sts_affiliate_custom_payment_id'] = 'le texte à montrer dans la zone de l\'iD défini pour chaque affilié';
$lang['desc_sts_affiliate_delete_commission_on_checkout'] = 'cette option va supprimer le cookie d\'affiliation sur l\'ordinateur du client après que le paiement ait été effectué.';
$lang['desc_sts_affiliate_enable_affiliate_marketing'] = 'autoriser ou non le module de marketing d\'affiliation';
$lang['desc_sts_affiliate_enable_articles'] = 'autoriser ou non les articles d\'affiliation';
$lang['desc_sts_affiliate_enable_auto_affiliates'] = 'les clients deviennent automatiquement des affiliés quand ils achètent de votre boutique';
$lang['desc_sts_affiliate_enable_banners'] = 'autoriser ou non les bannières d\'affiliés';
$lang['desc_sts_affiliate_enable_direct_product_code'] = 'autoriser ou non le code de lien que peuvent copier les affiliés et coller à partir des pages de produits pour la promotion du produit';
$lang['desc_sts_affiliate_enable_email_ads'] = 'autoriser ou non les annonces par email';
$lang['desc_sts_affiliate_enable_forced_matrix'] = 'autoriser ou non le paramétrage de la matrice à parachutage forcé';
$lang['desc_sts_affiliate_enable_hostip_lookup'] = 'autoriser ou non la possibilité de montrer la ville et le pays à partir desquels le membre se connecte';
$lang['desc_sts_affiliate_enable_hover_ads'] = 'autoriser ou non les annonces volantes';
$lang['desc_sts_affiliate_enable_javascript_code'] = 'autoriser ou non le code javascript que peuvent copier les affiliés et coller à partir des pages du produit pour la promotion de ce dernier';
$lang['desc_sts_affiliate_enable_landing_pages'] = 'autoriser ou non les pages d\'aterissage';
$lang['desc_sts_affiliate_enable_performance_bonus'] = 'octroyer des bonus à chaque affilié sur la base des critères de rendement établis';
$lang['desc_sts_affiliate_enable_profile_description'] = 'permettre à l\'utilisateur de faire une courte description sur les pages de profil';
$lang['desc_sts_affiliate_enable_profiles'] = 'autoriser ou non les pages de profil d\'affiliés';
$lang['desc_sts_affiliate_enable_recommended_profile'] = 'autoriser les produits recommendés sur les pages de profil';
$lang['desc_sts_affiliate_enable_recurring_commission'] = 'autoriser les commissions récurrentes sur les souscriptions d\'adhésion';
$lang['desc_sts_affiliate_enable_referral_signup_bonus'] = 'octroyer un bonus à l\'affilié qui parrainne quelqu\'un';
$lang['desc_sts_affiliate_enable_referral_signup_bonus_amount'] = 'montant à octroyer à celui parraine quelqu\'un';
$lang['desc_sts_affiliate_enable_replication'] = 'autoriser ou non le répliquage du site';
$lang['desc_sts_affiliate_enable_signup_bonus'] = 'octroyer un bonus à celui qui s\'enregistre';
$lang['desc_sts_affiliate_enable_signup_bonus_amount'] = 'montant à octroyer au nouvel affilié pour s\'être enregistré';
$lang['desc_sts_affiliate_enable_text_ads'] = 'autoriser ou non les articles publicitaires';
$lang['desc_sts_affiliate_enable_text_links'] = 'autoriser ou non les liens textuels';
$lang['desc_sts_affiliate_enable_trackers'] = 'autoriser ou non l\'outil de suivi de la publicité';
$lang['desc_sts_affiliate_enable_videos'] = 'autoriser ou non les vidéos';
$lang['desc_sts_affiliate_enable_viral_pdfs'] = 'autoriser ou non les PDFs viraux';
$lang['desc_sts_affiliate_enable_wysiwyg_content'] = 'autoriser les affiliés à utiliser l\'éditeur HTML pour éditer le contenu';
$lang['desc_sts_affiliate_enable_wysiwyg_email'] = 'autoriser les affiliés à utiliser l\'éditeur HTML pour éditer les emails';
$lang['desc_sts_affiliate_image_auto_resize'] = 'redimendionner automatiquement les images de membre à la hauteur et à la largeur spécifiées ci-dessous';
$lang['desc_sts_affiliate_image_height'] = 'hauteur des images de membre lorsq\'elles sont redimensionnées';
$lang['desc_sts_affiliate_image_width'] = 'largeur des images de membre lorsqu\'elles sont redimensionnées';
$lang['desc_sts_affiliate_intro_text'] = 'texte à montrer aux utilisateurs lorsqu\'ils sont référencés par un affilié';
$lang['desc_sts_affiliate_lifetime_sponsor'] = 'autoriser cette option vous permettra de payer les commissions sur tous les achats actuels et futurs du client de l\'affilié l\'ayant référencé';
$lang['desc_sts_affiliate_link_type'] = 'selectionner le type de liens d\'affiliation à générer pour les utilisateurs';
$lang['desc_sts_affiliate_matrix_width'] = 'la largeur maximale de votre matrice à parachutage forcé';
$lang['desc_sts_affiliate_min_payment'] = 'le minimum de commissions d\'affiliation qu\'un utilisateur doit générer avant de recevoir un paiement';
$lang['desc_sts_affiliate_new_commission'] = 'les nouvelles commissions recevront ce statut par défaut';
$lang['desc_sts_affiliate_performance_bonus_amount'] = 'le bonus surle rendement est octroyé si le montant du paiement est sélectionné comme étant un type de bonus de rendement';
$lang['desc_sts_affiliate_performance_bonus_required'] = 'critères obligatoires à remplir avant de recevoir une prime de rendement';
$lang['desc_sts_affiliate_performance_bonus_required_amount'] = 'montant requis pour générer les bonus de rendement';
$lang['desc_sts_affiliate_performance_bonus_type'] = 'type de bonus de rendement à octroyer';
$lang['desc_sts_affiliate_program_type'] = 'le type de programme d\'affiliation que vous voulez mettre en place';
$lang['desc_sts_affiliate_refer_friend_fields'] = 'selectionner le nombre de champs à montrer lorsque le formulaire  Envoyez-à-un ami est généré';
$lang['desc_sts_affiliate_refer_friend_footer'] = 'texte de bas pied de page à ajouter aux emails envoyés de Envoyez-à-un ami';
$lang['desc_sts_affiliate_refer_friend_header'] = 'texte d\'en-tête à ajouter aux emails envoyés à partir de Envoyez-à-un ami.';
$lang['desc_sts_affiliate_refund_commission_type'] = 'établir les commissions correspondantes pour ceci lorsqu\'a lieu un remboursement';
$lang['desc_sts_affiliate_replication_enable_header_footer'] = 'autoriser ou non le thème d\'en-tête et de pied de page sur le répliquage de toutes les pages';
$lang['desc_sts_affiliate_require_referral_code'] = 'les clients doivent mettre un code de parrainage à la caisse s\'ils n\'ont pas encore été déjà parrainnés par quelqu\'un';
$lang['desc_sts_affiliate_restrict_self_commission'] = 'ne pas permettre que les affiliés génèrent des commissions sur leur propre compte';
$lang['desc_sts_affiliate_restrict_subdomains'] = 'les sous-domaines  ne peuvent pas être utilisés pour les URLs d\'affiliation';
$lang['desc_sts_affiliate_save_path'] = 'le classeur de répliquage du site où sauvegarder les pages répliquées';
$lang['desc_sts_affiliate_show_downline_details'] = 'autoriser l\'affilié à visualiser tous les détails concernant ses filleuls';
$lang['desc_sts_affiliate_show_downline_emails'] = 'autoriser l\'affilié à visualiserles adresses email de ses filleuls';
$lang['desc_sts_affiliate_show_downline_photos'] = 'autoriser l\'affilié à visualiser les photos de ses filleuls';
$lang['desc_sts_affiliate_show_javascript_code'] = 'montrer le code javascript sur les détails de chaque produit pour l\'usage et la promotion de l\'affilié';
$lang['desc_sts_affiliate_show_name'] = 'montrer l\'identifiant de l\'affilié sur chaque page de votre site';
$lang['desc_sts_affiliate_show_pending_comms_members'] = 'montrer les commissions en cours dans l\'espace membre';
$lang['desc_sts_affiliate_tools_per_page'] = 'le nombre d\'outils marketing à montrer sur une page';
$lang['desc_sts_affiliate_total_sale_commissions'] = 'payer les commissions sur le montant total dans le panier ou par produit';
$lang['desc_sts_backup_enable'] = 'sauvegarder vos tableaux de la base de données';
$lang['desc_sts_backup_path'] = 'entrer dans le répertoire physique pour y placer les sauvegardes.  nous vous suggérons pour des raisons de sécurité d\'utiliser le répertoire extérieur de votre classeur public root';
$lang['desc_sts_backup_schedule_daily'] = 'selectionner les sauvegardes quotidiennes, hebdomadaires, ou mensuelles';
$lang['desc_sts_backup_schedule_interval'] = 's\'applique uniquement aux sauvegardes hebdomadaires ou mensuelles.  mettre le jour en nombre(exemple: 1 pour dimanche, 4 pour mercredi) si vous voulez sauvegarder hebdomadairement, ou le jour du mois (exemple: 15) pour les sauvegardes mensuelles';
$lang['desc_sts_backup_schedule_time'] = 'selectioner le temps pour générer les sauvegardes (format d\'heure de 24 heures). doit être dans :15 ou :30 minutes d\'intervalle, par exemple: 01:30';
$lang['desc_sts_cart_add_account_group'] = 'le groupe d\'affiliation auquel ajouter l\'utilisateur lorsqu\'il s\'enregistre';
$lang['desc_sts_cart_add_account_mailing_list'] = 'la liste de diffusion à laquelle vous voulez ajouter le client après qu\'il se soit enregistré';
$lang['desc_sts_cart_apply_credits_checkout'] = 'appliquer les crédits inutilisés par le membre à son total à la caisse';
$lang['desc_sts_cart_number_recommended_products'] = 'le nombre de produits recommandés à faire voir à la caisse.';
$lang['desc_sts_cart_number_upsell_products'] = 'le nombre de produits en vente additionnelle à montrer immédiatement avant que le client n\'aille à la caisse.';
$lang['desc_sts_cart_recommend_products'] = 'montrer au client quelques produits recommandés avant la caisse';
$lang['desc_sts_cart_recommended_products_keywords'] = 'montrer les produits recommandés sur la base des mots-clés du produit sélectionné';
$lang['desc_sts_cart_remove_account_mailing_list'] = 'si le client est inscrit sur une liste différente, vous pouvez l\'en extraire quand il effectue un achat';
$lang['desc_sts_cart_show_upsell_page'] = 'montrer au client une page de vente additionnelle avant qu\'il effectue son paiement';
$lang['desc_sts_cart_ssl_on_checkout'] = 'envoyer au client une URL sécurisée SSL quand il va à la caisse';
$lang['desc_sts_cart_ssl_on_checkout_url'] = 'mettre une URL pour l\'envoi de SSL sécurisé pour la caisse. Ce logiciel ne supporte de URLS SSL partagées. Vous devez avoir un SSL installé sur le même domaine.';
$lang['desc_sts_cart_upsell_products_keywords'] = 'mettre ici des mots-clés séparés de virgule pour les produits en vente additionnelle';
$lang['desc_sts_content_articles_home_page'] = 'si vous voulez que les articles de contenu figurent sur la page d\'accueil,ils s\'afficheront dans cette proportion';
$lang['desc_sts_content_articles_per_page'] = 'le nombre d\'articles de contenu à montrer sur chaque page';
$lang['desc_sts_content_auto_generate_xml_sitemap'] = 'générer automatiquement  le plan de site XML pour Google et les autres moteurs de recherche';
$lang['desc_sts_content_categories_per_page'] = 'nombre de catégories de contenu à montrer sur chaque page';
$lang['desc_sts_content_document_charset'] = 'le document de jeu de caractères pour votre site. certaines valeurs par défaut sont UTF-8 et ISO-8859-1';
$lang['desc_sts_content_enable_comments'] = 'autoriser les membres à poster des commenataires sur chaque contenu de vos pages';
$lang['desc_sts_content_enable_disqus_form'] = 'si vous voulez utiliser des systèmes de gestion des commentaires sur votre blog, vous pouvez mettre le code du système ici';
$lang['desc_sts_content_enable_facebook_share'] = 'autoriser l\'icône partagée de facebook sur chacune des annonces sur votre blog';
$lang['desc_sts_content_enable_ping_fm'] = 'autoriser les mises à jour de Ping.FM';
$lang['desc_sts_content_enable_related_articles'] = 'autoriser des articles relatifs au contenu';
$lang['desc_sts_content_enable_tweetmeme_code'] = 'autoriser TweetMeme ReTweets sur chacune des annonces postées sur votre blog';
$lang['desc_sts_content_enable_wordpress_publishing'] = 'autoriser que vous puissiez poster vos articles de blog à un API éloigné de Wordpress ou de MetawebBlog autorisé';
$lang['desc_sts_content_html_editor'] = 'sélectionner l\'éditeur de contenu HTML à utiliser pour éditer';
$lang['desc_sts_content_members_dashboard_enable'] = 'autoriser ou non les icônes sur le tableau de bord de l\'espace membre';
$lang['desc_sts_content_members_dashboard_enable_account_details'] = 'autoriser l\'icône de détails du compte sur le tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_commissions'] = 'autoriser l\'icône de commissions sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_content'] = 'autoriser l\'icône de contenu sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_coupons'] = 'autoriser l\'icône de coupons sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_downline'] = 'autoriser l\'icône des filleuls sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_downloads'] = 'autoriser l\'icône de téléchargement sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_invoices'] = 'autoriser l\'icône de facture sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_memberships'] = 'autoriser l\'icône des souscriptions d\'adhésion sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_reports'] = 'autoriser l\'icône des rapports sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_support'] = 'autoriser l\'icône de support sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_tools'] = 'autoriser l\'icône des outils sur tableau de bord';
$lang['desc_sts_content_members_dashboard_enable_traffic'] = 'autoriser l\'icône de traffic sur tableau de bord';
$lang['desc_sts_content_members_dashboard_num_articles'] = 'nombre d\'articles à montrer sur le tableau de bord des membres';
$lang['desc_sts_content_ping_fm_app_key'] = 'Clé d\'application Ping.FM';
$lang['desc_sts_content_require_comment_moderation'] = 'exige que les admins modèrent tout commentaire fait au contenu';
$lang['desc_sts_content_rss_per_page'] = 'nombre d\'articles à montrer par RSS feed';
$lang['desc_sts_content_translate_menus'] = 'en autorisant cette option, le menu des articles sera traduit conformément aux entrées faites dans le fichier langue';
$lang['desc_sts_content_update_services'] = 'chaque fois que vous créez / mettez à jour le contenu, le système va notifier(ping) ces services';
$lang['desc_sts_content_wordpress_password'] = 'Mot de passe du Blog WordPress';
$lang['desc_sts_content_wordpress_url'] = 'URL XML-RPC du Blog wordpress ';
$lang['desc_sts_content_wordpress_username'] = 'identifiant du Blog WordPress';
$lang['desc_sts_email_auto_prune_archive'] = 'auto prune / suprimer les emails archivés après un certain nombre de jours qu\'on a fixés';
$lang['desc_sts_email_bounce_address'] = 'l\'adresse email pour les mails refusés et retournés';
$lang['desc_sts_email_bounce_password'] = 'mot de passe POP3 pour emails refusés';
$lang['desc_sts_email_bounce_port'] = 'port POP3 pour email refusé';
$lang['desc_sts_email_bounce_server'] = 'serveur POP3 pour emails refusés';
$lang['desc_sts_email_bounce_service_flags'] = 'service optionel de marquage pour les emails refusés';
$lang['desc_sts_email_bounce_username'] = 'identifiant POP3  pour email rejeté';
$lang['desc_sts_email_charset'] = 'jeu de caractères à utiliser lorsque vous envoyez des emails. pour le Français utiliser\'iso-8859-1\'';
$lang['desc_sts_email_enable_archive'] = 'archiver tous les emails qui sont envoyés à partir de ce système';
$lang['desc_sts_email_enable_debugging'] = 'montrer les messages de déboggage d\'email';
$lang['desc_sts_email_enable_ssl'] = 'Utiliser SSL ou TLS';
$lang['desc_sts_email_limit_mass_mailing'] = 'limiter l\'envoi en masse à celui de la mise en fil d\'attente envoyé à ce nombre';
$lang['desc_sts_email_mailer_type'] = 'le type de logiciel d\'envoi à utiliser pour envoyer les emails.  pour des grands volumes d\'emails de masse, nous vous suggérons d\'utiliser le smtp';
$lang['desc_sts_email_require_confirmation_on_signup'] = 'exige que les utilisateurs confirment leurs comptes via email avant d\'être autorisé à se logguer dans l\'espace membre';
$lang['desc_sts_email_send_queue'] = 'envoyer les emails en masse immédiatement au lieu de les mettre dans la file d\'attente et d\'attendre l\'exécution des tâches planifiées';
$lang['desc_sts_email_show_email_content_queue'] = 'autoriser ceci pour voir le contenu de chaque email pendant que vous visualisez la file d\'attente et/ou les archives d\'email';
$lang['desc_sts_email_smtp_host'] = 'smtp host ou nom du serveur';
$lang['desc_sts_email_smtp_password'] = 'mot de passe pour authentification avec le smtp host';
$lang['desc_sts_email_smtp_port'] = 'smtp port à utiliser.  par défaut c\'est 25';
$lang['desc_sts_email_smtp_timeout'] = 'valeur du délai de réponse pour envoyer les emails via smtp';
$lang['desc_sts_email_smtp_username'] = 'identifiant pour authentification avec le smtp host';
$lang['desc_sts_email_use_smtp_authentication'] = 'utiliser l\'authentification smtp pendant que vous envoyez les emails';
$lang['desc_sts_faq_articles_per_page'] = 'le nombre d\'articles FAQ à montrer sur chaque page';
$lang['desc_sts_form_enable_custom_fields_checkout'] = 'autoriser les champs définis sur le formulaire de caisse';
$lang['desc_sts_form_enable_custom_fields_contact_us'] = 'autoriser les champs définis sur le formulaire de contact';
$lang['desc_sts_form_enable_custom_fields_membership'] = '\'autoriser les champs définis sur le formulaire de paiement d\'adhésion';
$lang['desc_sts_form_enable_custom_fields_registration'] = 'autoriser les champs définis sur le formulaire d\'enregistrement';
$lang['desc_sts_form_enable_tos_checkbox'] = 'autoriser la case à cocher des termes et conditiond d\'utilisation du service sur le formulaire d\'enregistrement';
$lang['desc_sts_form_membership_product_id'] = 'mettre l\'iD du produit de souscription d\'adhésion exigé';
$lang['desc_sts_form_require_membership'] = 'exiger la souscription d\'adhésion à l\'enregistrement, ne pas permettre aux utilisateurs de s\'enregistrer gratuitement';
$lang['desc_sts_image_enable_flash_rotator'] = 'utiliser le rotateur interne d\'images en flash pour montrer les images de produits';
$lang['desc_sts_image_enable_thickbox'] = 'autoriser ou non la case à cocher des images javascript à utiliser.  si ce n\'est pas autorisé, les fenêtres popup habituelles vont être utilisées';
$lang['desc_sts_image_flash_rotator_height'] = 'hauteur du rotateur d\'images fash';
$lang['desc_sts_image_flash_rotator_width'] = 'largeur rotateur d\'images flash';
$lang['desc_sts_image_library'] = 'bibliothèque d\'image à utiliser pour la conversion d\'images. par défaut c\'est GD2';
$lang['desc_sts_image_maintain_ratio'] = 'spécifie si oui ou non maintenir le ratio de l\'aspect original pendant que vous redimensionnez ou utiliez les valeurs absolues.';
$lang['desc_sts_image_max_photo_size'] = 'la taille maximale en kilobytes autorisée pendant que vous attachez une photo.  Par défaut c\'est 1024';
$lang['desc_sts_invoice_autload_print_window'] = 'montrer automatiquement la fenêtre d\'impression lorsque vous cliquez sur le bouton d\'impression des factures';
$lang['desc_sts_invoice_auto_generate_invoice_day'] = 'le nombre de jours avant qu\'une facture soit due à partir duquel le système va générer la facture';
$lang['desc_sts_invoice_due_date_days'] = 'fixer le nombre de jours par défaut à partir duquel la facture sera due';
$lang['desc_sts_invoice_enable_auto_generate_invoice'] = 'autoriser l\'auto-création des factures';
$lang['desc_sts_invoice_enable_logo'] = 'montrer le logo sur le formulaire d\'impression de la facture';
$lang['desc_sts_invoice_new_order_status_digital'] = 'donner ce statut aux nouveaux produits incorporels/contenus numériques lorsque le paiement est effectué';
$lang['desc_sts_invoice_new_order_status_membership'] = 'donner ce statut aux nouvelles adhésions lorsque le paiement est effectué';
$lang['desc_sts_invoice_new_order_status_physical'] = 'donner ce statut aux nouvelles commandes lorsque le paiement est effectué';
$lang['desc_sts_products_alert_inventory'] = 'alerter les admins lorsque les niveaux de stock tombent en dessous du niveau spécifié ci-dessous';
$lang['desc_sts_products_alert_inventory_level'] = 'le montant des stocks pour déclencher une alerte de stock';
$lang['desc_sts_products_bookmarks_code'] = 'voici le code utilisé pour générer l\'option de marquage(bookmark).  si vous voulez utiliser un widget de bookmarks, mettez-le ici';
$lang['desc_sts_products_categories_image_auto_resize'] = 'redimensionner automatiquement les images de catégorie de produit telles que spécifiées ci-dessous';
$lang['desc_sts_products_categories_image_height'] = 'hauteur des images de catégorie de produit lorsqu\'elles sont redimensionnées';
$lang['desc_sts_products_categories_image_width'] = 'largeur des images de catégorie de produit lorsqu\'elles sont redimensionnées';
$lang['desc_sts_products_description_1_name'] = 'le nom du tableau de description de chaque produit. par exemple: aperçu';
$lang['desc_sts_products_description_2_name'] = 'le nom du tableau de description de chaque produit. par exemple: fonctionnalités';
$lang['desc_sts_products_enable_bookmarks'] = 'autoriser la possibilité de bookmarker les produits en utilisant les sites de réseaux sociaux de bookmarking comme digg';
$lang['desc_sts_products_enable_global discounts'] = 'autoriser les remises globales à utiliser sur tous les produits';
$lang['desc_sts_products_enable_inventory'] = 'utiliser le système d\'inventaire de stock pour les produits physiques qui sont expédiés';
$lang['desc_sts_products_enable_reviews'] = 'autoriser les membres à noter les produits et à faire des revues';
$lang['desc_sts_products_images_per_product'] = 'le nombre de produits à montrer par page de produit';
$lang['desc_sts_products_listing_style'] = 'sélectionner la façon dont les produits vont être listés dans votre boutique, que ce soit en grille ou en format liste';
$lang['desc_sts_products_manufacturer_category_name'] = 'choisir le nom que vous voulez utiliser pour les fournisseurs/fabricant sur les pages de la boutique. Par exemple: les marques';
$lang['desc_sts_products_manufacturer_image_auto_resize'] = 'redimensionner les images des fournisseurs telles que spécifiées ci-dessous';
$lang['desc_sts_products_manufacturer_image_height'] = 'hauteur des images des fournisseurs lorqu\'elles sont redimensionnées';
$lang['desc_sts_products_manufacturer_image_width'] = 'largeur des images des fournisseurs lorsqu\'elles sont redimensionnées';
$lang['desc_sts_products_number_similar_products_details_page'] = 'le nombre de produits similaires à montrer sur la page de détails des produits';
$lang['desc_sts_products_per_home_page'] = 'nombre de produits à montrer sur la page d\'accueil de la boutique';
$lang['desc_sts_products_per_page'] = 'nombre de produits à montrer par page';
$lang['desc_sts_products_random_home_page'] = 'montrer de façon aléatoire les produits VIP sur la page  d\'accueil';
$lang['desc_sts_products_show_similar_products_details_page'] = 'montrer les produits similaires sur chaque page de détails de produit en fonction des mots-clés';
$lang['desc_sts_sec_admin_failed_login_email'] = 'adresse email à laquelle envoyer les alertes email comme l\'échec de connexion et le déboggage d\'emails';
$lang['desc_sts_sec_admin_restrict_ip'] = 'restreindre l\'accès à l\'espace d\'admin à des adresses IP spécifiques.  séparer chaque adresse avec une virgule';
$lang['desc_sts_sec_auto_ip_block_interval'] = 'le nombre de fois que quelqu\'un peut soumettre une requête incorrecte avant d\'être automatiquement bloqué';
$lang['desc_sts_sec_enable_admin_restrict_ip'] = 'autoriser cette option pour restreindre la console d\'admin aux adresses IP spécifiées ci-dessous';
$lang['desc_sts_sec_enable_auto_ip_block'] = 'ajouter automatiquement cette ip à la liste d\'adresses ip bloquées pendant que sont envoyés les formulaires avec données erronnées tels que spécifé dans l\'intervalle ci-dessous';
$lang['desc_sts_sec_enable_captcha'] = 'autoriser les images CAPTCHA pendant que vous vous mettez les commentaires et revues sur des produits';
$lang['desc_sts_sec_enable_captcha_order_form'] = 'autoriser les images CAPTCHA sur les formulaires de commande et de facturation';
$lang['desc_sts_sec_enable_max_mind'] = 'autoriser le système de prévention de la fraude maxmind';
$lang['desc_sts_sec_max_mind_key'] = 'mettre votre clé max mind ici';
$lang['desc_sts_sec_site_block_free_email_accounts'] = 'si vous voulez empêcher l\'enregistrement à partir certains types d\'emails, à l\'exemple de hotmail.com, mettre le domaine de l\'email sur une ligne ici';
$lang['desc_sts_sec_site_enable_block_free_email_accounts'] = 'autoriser le bloquage des domaines d\'emails gratuits pendant l\'enregistrement des comptes';
$lang['desc_sts_sec_site_enable_form_flood_control'] = 'autoriser uniquement les utilisateurs à soumettre certains formulaires à raison du total de n fois par intervalle ci-dessous';
$lang['desc_sts_sec_site_form_flood_control_interval'] = 'le nombre de fois qu\'un utilisateur peut envoyer le mail à son filleul en 24 heures';
$lang['desc_sts_sec_site_restrict_ips'] = 'si vous voulez bloquer l\'accès de votre site à certaines adresses IP, mettre chaque IP sur une ligne et taper sur ENTREE';
$lang['desc_sts_shipping_send_vendor_email_alert'] = 'Lorsqu\'un client achète un produit chez un vendeur, lui envoyer une alerte';
$lang['desc_sts_shipping_use_billing_taxes'] = 'utiliser le code postal de facturation du client pour calculer les taxes.  si cela est désactivé, le code postal d\'expédition sera utilisé en remplacement';
$lang['desc_sts_shipping_use_vendor_zip'] = 'utiliser le code zip du marchand pendant le calcul des frais d\'expédition';
$lang['desc_sts_store_affiliate_id'] = 'iD de parrainage';
$lang['desc_sts_store_cache_minutes'] = 'le nombre de minutes pour garder la page en cache avant de générer une nouvelle web';
$lang['desc_sts_store_categories_per_page'] = 'le nombre de catégories à montrer par page';
$lang['desc_sts_store_categories_per_page'] = 'le nombre de catégories à montrer par page';
$lang['desc_sts_store_categories_show_image'] = 'montrer une image par catégorie sur la page de catégorie de la boutique';
$lang['desc_sts_store_default_country'] = 'pays par défaut à utiliser pour les formulaires';
$lang['desc_sts_store_default_currency'] = 'valeurs de devise par défaut à utiliser pour le calcul des montants';
$lang['desc_sts_store_default_keywords'] = 'mots-clés par défaut à utiliser pour votre boutique';
$lang['desc_sts_store_default_language'] = 'langue par défaut à montrer sur votre boutique';
$lang['desc_sts_store_default_timezone'] = 'mettre le fuseau horaire par défaut à utiliser pour générer les fonctions de date et de temps';
$lang['desc_sts_store_default_weight'] = 'l\'unité de poids à utiliser pour afficher les produits avec leurs poids';
$lang['desc_sts_store_description'] = 'une brève descripton de votre boutique';
$lang['desc_sts_store_disable_cart_maintenance'] = 'désactiver toute commande et rediriger les utilisateurs vers une page temporaire';
$lang['desc_sts_store_domain_name'] = 'nom de domaine disposant d\'une licence';
$lang['desc_sts_store_email'] = 'adresse email utilisée pour générer les emails';
$lang['desc_sts_store_enable_cache'] = 'autoriser la cache du site à cacher les sites web.  ne pas autoriser ceci à moins de savoir ce que vous êtes en train de faire';
$lang['desc_sts_store_enable_downline_cache'] = 'autoriser la cache pour qu\'on puisse visualiser les filleuls';
$lang['desc_sts_store_enable_downline_cache_minutes'] = 'la cache des filleuls en quelques minutes';
$lang['desc_sts_store_enable_language_selector'] = 'autoriser les utilisateurs à choisir la langue par défaut';
$lang['desc_sts_store_google_analytics_code'] = 'si vous voulez autoriser google analytics pour vos pages, entrer votre ID ici';
$lang['desc_sts_store_image_auto_resize'] = 'redimensionner automatiquement les images de produit à la hauteur et à la largeur spécifiées ci-dessous';
$lang['desc_sts_store_image_height'] = 'hauteur des images de produit lorsqu\'elles sont redimensionnées';
$lang['desc_sts_store_image_width'] = 'largeur des images de produit lorsqu\'elles sont redimensionnées';
$lang['desc_sts_store_key'] = 'votre clé de licence en cours';
$lang['desc_sts_store_name'] = 'le nom de votre site de commerce éléctronique';
$lang['desc_sts_store_order_id_prefix'] = 'le préfixe  attaché à la facture générée par chaque boutique';
$lang['desc_sts_store_phone_number'] = 'garder le numéro de téléphone';
$lang['desc_sts_store_refer_friend_code'] = 'ce code est utiisé pour générer la case de Dites-à-un ami.  si vous voulez utiliser un widget différent de Dites-à-un ami, mettez ledit code ici';
$lang['desc_sts_store_refer_friend_enable'] = 'autoriser ou non le code du tell-a-friend';
$lang['desc_sts_store_set_curl_timeout'] = 'fixer la durée de temps utilisée pour se connecter via curl';
$lang['desc_sts_store_shipping_address'] = 'adresse boutique';
$lang['desc_sts_store_shipping_city'] = 'ville de la boutique';
$lang['desc_sts_store_shipping_country'] = 'pays de la boutique. utilisé pour le calcul à l\'expédition';
$lang['desc_sts_store_shipping_state'] = 'Etat/Région de la boutique';
$lang['desc_sts_store_shipping_zip'] = 'code zip ou postal de la boutique. utilisé pour le calcul à l\'expédition';
$lang['desc_sts_store_thank_you_page_code'] = 'code optionnel de page de remerciements que vous pouvez ajouter à votre page de remerciements';
$lang['desc_sts_store_time_format'] = 'format de temps utilisant le format standard PHP.  veuillez consulter www.php.net/date.  par défaut c\'est: m-d-Y h:i:s A';
$lang['desc_sts_store_upload_photo_types'] = 'type de fichiers autorisés que les utilisateurs peuvent attacher.  séparer chacun d\'eux par un délimiteur \'|\'';
$lang['desc_sts_store_use_daylight_savings_time'] = 'autoriser une économie de temps de jour';
$lang['desc_sts_support_auto_close'] = 'auto-clore les requêtes actives au support n\'ayant pas reçu des réponses';
$lang['desc_sts_support_auto_close_interval'] = 'intervalle (en jours)  après lequel les requêtes vont s\'auto-clore';
$lang['desc_sts_support_enable'] = 'si vous voulez utiliser le système d\'assistance et de support intégré dans le système, l\'autoriser ici';
$lang['desc_sts_support_enable_email_on_assigned_ticket'] = 'envoyer au concerné un email lorsque qu\'une requête lui est assignée';
$lang['desc_sts_support_enable_file_uploads'] = 'permettre aux utilisateurs d\'attacher des fichiers à leurs requêtes au support';
$lang['desc_sts_support_max_upload_size'] = 'taille maximale de fichiers autorisée qui peuvent être attachés au requêtes envoyées au support';
$lang['desc_sts_support_tickets_per_page'] = 'le nombre de rangées de requêtes au support à afficher par page';
$lang['desc_sts_support_upload_download_types'] = 'mettre le type de fichiers autorisés pour être attachés.  séparer chacun d\'eux avec un \'|\'';
$lang['desc_sts_support_upload_folder_path'] = 'le classeur où les utilisateurs devront attacher leurs fichiers destinés au centre d\'assistance et de support.  nous vous suggérons de créer, pour des raisons de sécurité,  un classeur externe au répertoire  internet public';
$lang['desc_sts_support_url_redirect'] = 'si vous voulez que les utilisateurs soient redirigés vers l\'adresse URL d\'un autre centre d\'assistance et de support , la mettre ici';
$lang['desc_sts_tracking_auto_prune_days'] = 'le nombre de jour après lesquels les données de l\'outil de suivi des annonces seront élaguées';
$lang['desc_sts_video_player_allow_fullscreen'] = 'permettre à l\'utilisateur de mettre la vidéo en plein écran sur le lecteur fash';
$lang['desc_sts_video_player_autostart'] = 'autoriser cette option pour que vos vidéos flash soient automatiquement jouées';
$lang['desc_sts_video_player_back_color'] = 'couleur de fond du lecteur vidéo';
$lang['desc_sts_video_player_control_bar'] = 'localisation de la barre de contrôle vidéo';
$lang['desc_sts_video_player_default_link'] = 'URL par défaut pour aller aux vidéos';
$lang['desc_sts_video_player_display_click'] = 'choisir de jouer soit de cliquer un lien sur la vidéo';
$lang['desc_sts_video_player_front_color'] = 'couleur d\'accueil du lecteur vidéo';
$lang['desc_sts_video_player_height'] = 'la hauteur même du lecteur flash';
$lang['desc_sts_video_player_logo'] = 'si vous voulez ajouter un petit logo à l\'angle supérieur du lecteur vidéo, le mettre ici';
$lang['desc_sts_video_player_screen_color'] = 'couleur d\'écran du lecteur vidéo';
$lang['desc_sts_video_player_width'] = 'la largeur même du lecteur vidéo';
$lang['desc_tax_amount'] = 'mettre le montant des taxes';
$lang['desc_tax_zone_name'] = 'mettre le nom de la zone fiscale';
$lang['desc_theme_description'] = 'description du thème';
$lang['desc_theme_image_preview'] = 'fichier de prévisualisation du thème';
$lang['desc_theme_name'] = 'nom du thème';
$lang['desc_upload_photo'] = 'attacher une image photo à cet utilisateur si vous le voulez';
$lang['desc_upload_site_logo'] = 'charger le logo de votre propre site';
$lang['desc_upsell_product'] = 'montrer sur la page de vente aditionnelle';
$lang['desc_use_external_embed'] = 'si vous voulez utiliser un service de vidéo externe comme YouTube, mettre à oui et copier le code pour le coller dans la case.  à défaut, mettre l\'adresse complète à votre fichier .flv pour le streaming en utilisant notre lecteur vidéo';
$lang['desc_use_external_image'] = 'si vous voulez relier à une image externe, mettre le chemin complet vers le fichier de l\'image ici.  Ceci devrait annuler la bannière déjà chargée';
$lang['desc_use_group_defaults'] = 'si vous voulez que le logiciel calcule les commissions, transformer ceci en oui';
$lang['desc_use_percentage'] = 'desc_use pourcentage pour calculer les rabais';
$lang['desc_use_tax_zone'] = 'sélectionner une zone fiscale pour imputer de façon spécifique les taxes pour cet article';
$lang['desc_use_video_default'] = 'montrer la vidéo par défaut sur la page de détails du produit à la place de la photo par défaut';
$lang['desc_uses_per_coupon'] = 'usages par coupon';
$lang['desc_vendor_address'] = 'mettre adresse';
$lang['desc_vendor_address_1'] = 'mettre adresse 1';
$lang['desc_vendor_address_2'] = 'mettre adresse 2';
$lang['desc_vendor_city'] = 'mettre ville';
$lang['desc_vendor_contact_email'] = 'adresse email à laquelle envoyer les commandes ou les alertes';
$lang['desc_vendor_contact_name'] = 'la personne de contact chez votre vendeur';
$lang['desc_vendor_country'] = 'mettre pays';
$lang['desc_vendor_name'] = 'mettre le nom du vendeur';
$lang['desc_vendor_phone'] = 'mettre téléphone';
$lang['desc_vendor_send_alert'] = 'envoyer les alertes email lorsque les commandes sont reçues pour ce vendeur';
$lang['desc_vendor_state'] = 'mettre état/ province/région';
$lang['desc_vendor_zip'] = 'mettre code zip ou postal';
$lang['desc_video_default'] = 'faire de cette vidéo la vidéo par défaut';
$lang['desc_viral_video_link'] = 'URL complète du chemin vers votre fichier .flv file ou URl';
$lang['desc_zone_country'] = 'nom de la zone de pays';
$lang['desc_zone_region'] = 'nom de la zone de région';
$lang['description'] = 'description';
$lang['descsts_affiliate_commission_type'] = 'établir le type de commission d\'affiliation que vous utiliserez';
$lang['design'] = 'design';
$lang['digital'] = 'numérique';
$lang['directory_path'] = 'chemin répertoire';
$lang['disable'] = 'désactiver';
$lang['disable_affiliate_marketing'] = 'désactiver le marketing d\'affiliation';
$lang['disable_wysiwyg'] = 'désactiver l\'éditeur HTMl';
$lang['disabled'] = 'désactivé';
$lang['disapprove_checked_comments'] = 'désapprouver les commentaires vérifiés';
$lang['disapprove_checked_reviews'] = 'désapprouver les revues vérifiées';
$lang['discount_amount'] = 'montant du rabais';
$lang['discount_group'] = 'groupe bénéficiant de la vente au rabais'; 
$lang['discount_group_amount'] = 'montant de remise au groupe bénéficiant de la vente au rabais';
$lang['discount_group_name'] = 'nom du groupe bénéficiant de la vente au rabais';
$lang['discount_groups'] = 'groupes bénéficiant de la vente au rabais';
$lang['do_nothing'] = 'ne rien faire';
$lang['docs'] = 'docs';
$lang['documentation'] = 'documentation';
$lang['done_finished'] = 'effectué / terminé';
$lang['downline_email'] = 'email filleul';
$lang['downline_member_affiliate_link'] = 'filleul_membre_affilié_lien';
$lang['downline_member_email'] = 'filleul_membre_email';
$lang['downline_member_id'] = 'filleul_membre_id';
$lang['downline_member_name'] = 'filleul_membre_nom';
$lang['downline_member_username'] = 'filleul_membre_identifiant';
$lang['downline_message_html'] = 'message html au filleul';
$lang['downline_message_text']= 'message texte au filleul';
$lang['downline_name'] = 'nom filleul';
$lang['downline_options'] = 'options filleuls';
$lang['downline_sponsor_affiliate_link'] = 'lien d\'affiliation du sponsor de l\'affilié';
$lang['downline_sponsor_email'] = 'email du sponsor de l\'affilié';
$lang['downline_sponsor_name'] = 'nom du sponsor du filleul';
$lang['download_added_successfully'] = 'téléchargement ajouté avec succès';
$lang['download_deleted_successfully'] = 'téléchargement ajouté avec succès';
$lang['download_expires_days'] = 'le téléchargement expire dans jours';
$lang['download_location_1'] = 'lieu de téléchargement 1';
$lang['download_location_10'] = 'lieu de téléchargement 10';
$lang['download_location_2'] = 'lieu de téléchargement2';
$lang['download_location_3'] = 'lieu de téléchargement 3';
$lang['download_location_4'] = 'lieu de téléchargement 4';
$lang['download_location_5'] = 'lieu de téléchargement 5';
$lang['download_location_6'] = 'lieu de téléchargement 6';
$lang['download_location_7'] = 'lieu de téléchargement 7';
$lang['download_location_8'] = 'lieu de téléchargement 8';
$lang['download_location_9'] = 'lieu de téléchargement';
$lang['download_new_themes'] = 'télécharger de nouveaux thèmes';
$lang['download_theme'] = 'thème du téléchargement';
$lang['download_themes'] = 'thèmes de téléchargement';
$lang['download_updated_successfully'] = 'téléchargement mis à jour avec succès';
$lang['downloads'] = 'téléchargements';
$lang['dropdown'] = 'menu';
$lang['due_date'] = 'date de ';
$lang['due_date'] = 'date de réclamation';
$lang['duplicate_emails'] = 'dupliquer les emails non ajoutés';
$lang['each_video_needs_name'] = 'chaque vidéo doit avoir un nom';
$lang['ecommerce_manager_version'] = 'version du ecommerce manager';
$lang['edit'] = 'editer';
$lang['edit'] = 'editer';
$lang['edit_attribute'] = 'éditer attribut';
$lang['edit_category'] = 'éditer la catégorie';
$lang['edit_country'] = 'éditer le pays';
$lang['edit_currency'] = 'éditer la devise';
$lang['edit_details_for'] = 'éditer les details pour';
$lang['edit_group'] = 'éditer le groupe';
$lang['edit_layout_box'] = 'éditer le box de mise en page';
$lang['edit_manufacturer'] = 'éditer le fournisseur';
$lang['edit_membership_for'] = 'éditer la souscription d\'adhésion pour';
$lang['edit_module_settings'] = 'éditer les paramètres du module';
$lang['edit_region'] = 'éditer la région';
$lang['edit_shipping'] = 'éditer l\'expédition';
$lang['edit_vendor'] = 'éditer le vendeur';
$lang['edit_zone'] = 'éditer la région';
$lang['edit_zone'] = 'éditer la zone';
$lang['egold_payments'] = 'paiements e-gold';
$lang['email'] = 'email';
$lang['email_ad_body'] = 'texte de l\'email publicitaire';
$lang['email_ad_details'] = 'détails de l\'email publicitaire';
$lang['email_ad_name'] = 'nom de l\'email publicitaire';
$lang['email_ad_options'] = 'options d\'email publicitaire';
$lang['email_ad_title'] = 'titre de l\'email publicitaire';
$lang['email_ad_width'] = 'largeur email publicitaire';
$lang['email_address'] = 'adresse email';
$lang['email_admin'] = 'email admin';
$lang['email_ads'] = ' envoyer des annonces par email';
$lang['email_downline'] = 'envoyer email au filleul';
$lang['email_options'] = 'options email';
$lang['email_options'] = 'options email';
$lang['email_queue_updated_successfully'] = 'emails en file d\'attente mis à jour avec succès';
$lang['email_queued_successfully'] = 'emails mis à la file d\'attente avec succès';
$lang['email_sent_successfully'] = 'email envoyé avec succès';
$lang['email_settings'] = 'paramètres email';
$lang['email_template_added_successfully'] = 'template d\'email ajouté avec succès';
$lang['email_template_html'] = 'type d\'email';
$lang['email_template_name'] = 'nom du template d\'email';
$lang['email_template_type'] = 'type d\'email';
$lang['email_template_updated_successfully'] = 'template d\'email mis à jour avec succès';
$lang['email_templates'] = 'templates d\'email';
$lang['email_type'] = 'type d\'email';
$lang['emails_sent_successfully'] = 'emails envoyés avec succès';
$lang['enable'] = 'autoriser';
$lang['enable_affiliate_marketing'] = 'autoriser le marketing d\'affiliation';
$lang['enable_affiliate_marketing_send_email'] = 'autoriser le marketing d\'affiliation et envoyer une notification email';
$lang['enable_custom_commission'] = 'autoriser les commissions définies';
$lang['enable_custom_url'] = 'autoriser l\'URL défini';
$lang['enable_file_path'] = 'utiliser le chemin du fichier pour importer';
$lang['enable_product_inventory'] = 'autoriser l\'inventaire de stock pour le produit';
$lang['enable_product_trial'] = 'autoriser prix d\'essai';
$lang['enable_redirect'] = 'autoriser la redirection';
$lang['enable_wysiwyg'] = 'autoriser l\'éditeur HTMl';
$lang['end'] = 'fin';
$lang['enter_affiliate_note_here'] = 'mettre la note d\'affilié';
$lang['enter_option'] = 'mettre une option';
$lang['enter_product_id'] = 'mettre l\'id du produit';
$lang['enter_product_name'] = 'mettre le nom du  produit';
$lang['enter_search_here'] = 'mettre la recherche ici';
$lang['enter_username'] = 'mettre un identifiant valide';
$lang['eway_payments'] = 'paiements eWay';
$lang['expiration'] = 'expiration';
$lang['expiration_date'] = 'date d\'expiration';
$lang['export_affiliate_payments'] = 'exporter les paiements d\'affiliés';
$lang['export_commissions'] = 'exporter les commissions';
$lang['export_invoice_payments'] = 'exporter les factures de règlement';
$lang['export_invoices'] = 'exporter les factures';
$lang['export_members'] = 'exporter les membres';
$lang['export_module'] = 'exporter le module';
$lang['export_products'] = 'exporter les produits';
$lang['faq_article_added_successfully'] = 'article de la FAQ ajouté avec succès';
$lang['faq_article_updated_successfully'] = 'article de la FAQ mis à jour avec succès';
$lang['faq_categories'] = 'catégories de la FAQ';
$lang['faq_category'] = 'catégorie de la FAQ';
$lang['faq_category_added_successfully'] = 'catégorie de la FAQ ajoutée avec succès';
$lang['faq_category_updated_successfully'] = 'catégorie de la FAQ ajoutée avec succès';
$lang['faq_options'] = 'options de la FAQ';
$lang['faq_pages'] = 'pages de la FAQ';
$lang['fax'] = 'fax';
$lang['featured'] = 'en VIP';
$lang['file_attachments'] = 'fichiers attachés';
$lang['file_path'] = 'importer à l\'aide d\'un fichier sur votre serveur';
$lang['filename_export_affiliate_payments'] = 'exporter-paiements-affiliés';
$lang['filename_export_commissions'] = 'exporter-commissions';
$lang['filename_export_invoice_payments'] = 'exporter-factures-règlement';
$lang['filename_export_invoices'] = 'exporter-factures';
$lang['filename_export_members'] = 'exporter-membres';
$lang['filename_export_products'] = 'exporter-produits';
$lang['filter'] = 'filtrer';
$lang['filter_category'] = 'filtrer catégorie';
$lang['filter_this_page'] = 'filtrer cette page';
$lang['finances'] = 'finances';
$lang['first'] = 'premier';
$lang['first_name'] = 'prénom';
$lang['firstdata_connect_payment'] = 'connecter FirstData';
$lang['flat'] = 'forfait';
$lang['floating_div'] = 'div flottant';
$lang['flush_queue'] = 'file d\'attente chaude';
$lang['fname'] = 'prénom';
$lang['fname_email_required'] = 'le prénom et l\'adresse email sont toujours obligatoires';
$lang['folder_protection'] = 'protection du classeur';
$lang['folder_protection_instructions'] = 'pour commencer à protéger certains dossiers dans votre site, vous devez créer et éditer votre fichier .htaccess file avec le code suivant';
$lang['follow_up_added_successfully'] = ' suivi ajouté avec succès';
$lang['follow_up_deleted_successfully'] = 'suivi supprimé avec succès';
$lang['follow_up_name'] = 'nom du suivi';
$lang['follow_up_updated_successfully'] = 'suivi mis à jour avec succès';
$lang['follow_ups'] = 'suivis';
$lang['font_body_color'] = 'couleur du texte';
$lang['font_title_color'] = 'couleur du titre';
$lang['form_field_name'] = 'nom du champ du formulaire';
$lang['form_field_values'] = 'valeurs du formulaire par défaut';
$lang['form_fields'] = 'champs du formulaire';
$lang['form_settings'] = 'paramètres du formulaire';
$lang['forums'] = 'forums';
$lang['free_shipping'] = 'expédition gratuite';
$lang['from_credit_id'] = 'à partir de l\'id de crédit';
$lang['from_email'] = 'à partir de l\'email';
$lang['from_name'] = 'à partir du nom';
$lang['generate_commission'] = 'générer commission';
$lang['generate_commission_for_invoice'] = 'générer la commission pour la facture';
$lang['generate_file'] = 'générer';
$lang['generate_menu'] = 'générer menu html';
$lang['generate_report'] = 'générer rapport';
$lang['get_javascript_add_to_cart'] = 'ajouter code add-to-cart javascript';
$lang['global'] = 'global';
$lang['global_configuration'] = 'configuration générale';
$lang['go'] = 'aller';
$lang['go_back'] = 'retourner';
$lang['go_up_one_level'] = 'monter d\'un niveau';
$lang['goemerchant_payments'] = 'paiements goemerchant';
$lang['google_ad_sense'] = 'google adsense';
$lang['graph_y_legend'] = 'totaux';
$lang['group_deleted_successfully'] = 'groupe supprimé avec succès';
$lang['group_description'] = 'description du groupe';
$lang['group_id'] = 'iD groupe';
$lang['group_information'] = 'information sur le groupe';
$lang['group_name'] = 'nom du groupe';
$lang['group_pricing'] = 'prix du groupe';
$lang['group_upgrade'] = 'upgrader le groupe';
$lang['groups'] = 'groupes';
$lang['height'] = 'hauteur';
$lang['help'] = 'aide';
$lang['help_documentation'] = 'aide et documentation';
$lang['help_search'] = 'chercher la documentation d\'aide';
$lang['hidden'] = 'caché';
$lang['hide'] = 'cacher';
$lang['high'] = 'haut';
$lang['home'] = 'accueil';
$lang['home_page'] = 'page d\'accueil';
$lang['home_phone'] = 'téléphone domicile';
$lang['homepage'] = 'page d\'accueil';
$lang['hour'] = 'heure';
$lang['hours'] = 'heures';
$lang['html'] = 'html';
$lang['html_ad_body'] = 'corps d\'annonce en html';
$lang['html_ad_details'] = 'détails d\'annonce en html';
$lang['html_ad_name'] = 'titre d\'annonce en html';
$lang['html_ad_options'] = 'options d\'annonce en html';
$lang['html_ad_title'] = 'titre d\'annonce en html';
$lang['html_ad_type'] = 'type d\'annonce html';
$lang['html_ad_width'] = 'largeur annonce html';
$lang['html_ads'] = 'annonces html';
$lang['html_body'] = 'contenu html';
$lang['html_body'] = 'contenu html';
$lang['html_content'] = 'contenu html';
$lang['html_email'] = 'email en html';
$lang['html_email_format'] = 'format d\'email html';
$lang['html_follow_up'] = 'suivi en html';
$lang['html_message'] = 'message html';
$lang['html_snippet'] = 'onglet html';
$lang['html_template'] = 'template html';
$lang['id'] = 'id';
$lang['if_not_forwarded_click_here'] = 'si vous n\'êtes pas transféré automatiquement, cliquer ici';
$lang['image'] = 'image';
$lang['image_deleted_successfully'] = 'image supprimée avec succès';
$lang['image_file'] = 'fichier d\'image';
$lang['image_settings'] = 'paramétrages d\'image';
$lang['image_uploaded_successfully'] = 'image attachée avec succès';
$lang['import_export'] = 'importer / exporter';
$lang['import_jam_data'] = 'importer les données de JAM';
$lang['import_jam_database'] = 'importer la base de données JAM';
$lang['import_members'] = 'importer les membres';
$lang['import_module'] = 'importer le module';
$lang['import_products'] = 'importer les produits';
$lang['import_products'] = 'importer les produits';
$lang['inactive'] = 'inactif';
$lang['increase_decrease_price'] = 'ajouter/soustraire prix';
$lang['increase_decrease_weight'] = 'ajouter/soustraire le poids';
$lang['info'] = 'info';
$lang['install_affiliate_marketing_module'] = 'installer le module de marketing d\'affiliation';
$lang['install_export_module'] = 'installer le module d\'exportation';
$lang['install_gateways_module'] = 'installer le terminal de paiement';
$lang['install_import_module'] = 'installer le module d\'importation';
$lang['install_module'] = 'installer le module';
$lang['install_shipping_module'] = 'installer le module d\'expédition';
$lang['installation_code'] = 'code d\'installation';
$lang['installed_modules'] = 'modules installés';
$lang['invalid'] = 'invalide';
$lang['invalid_banner_height'] = 'hauteur bannière invalide';
$lang['invalid_banner_width'] = 'largeur bannière invalide';
$lang['invalid_bcc'] = 'bcc invalide';
$lang['invalid_cc'] = 'cc invalide';
$lang['invalid_date_available_format'] = 'format de date disponible invalide';
$lang['invalid_date_expires_format'] = 'format de date d\'expiration invalide';
$lang['invalid_date_format'] = 'format de date invalide';
$lang['invalid_date_published_format'] = 'format de date de publication invalide';
$lang['invalid_email'] = 'votre email est invalide';
$lang['invalid_email_address'] = 'adresse email invalide';
$lang['invalid_file_ext'] = 'extension de fichier invalide';
$lang['invalid_file_path'] = 'chemin d\'accès au fichier invalide';
$lang['invalid_file_uploaded'] = 'fichier attaché invalide';
$lang['invalid_form_field_names'] = 'vous devez remplir tous les noms de champ du formulaire';
$lang['invalid_invoice_id'] = 'id de facture invalide';
$lang['invalid_language_image_path'] = 'accès à l\'image de langue invalide';
$lang['invalid_language_path'] = 'chemin d\'accès à la langue invalide';
$lang['invalid_layout_box_controller_file_path'] = 'chemin d\'accès au box de contrôle de mise en page';
$lang['invalid_layout_box_view_file_path'] = 'chemin d\'accès à la visualisation du box de mise en page invalide';
$lang['invalid_license'] = 'clé de licence invalide';
$lang['invalid_login'] = 'votre connexion est invalide';
$lang['invalid_member_username'] = 'identifiant membre invalide';
$lang['invalid_model_file_path'] = 'chemin d\'accès au modèle de fichier invalide';
$lang['invalid_module_file_path'] = 'chemin d\'accès au fichier de module invalide';
$lang['invalid_password'] = 'mot de passe invalide';
$lang['invalid_permissions'] = 'vous n\'avez pas les autorisations nécessaires pour accéder à cette ressource';
$lang['invalid_product_id'] = 'id produit invalide';
$lang['invalid_referring_affiliate'] = 'affilié de parrainnage invalide';
$lang['invalid_sponsor'] = 'sponsor invalide';
$lang['invalid_theme_file_path'] = 'chemin d\'accès au thème du fichier invalide';
$lang['invalid_tracking_id'] = 'iD de l\'outil de suivi invalide';
$lang['invalid_username'] = 'identifiant invalide';
$lang['inventory_alert_level'] = 'niveau d\'alerte sur le stock';
$lang['invoice'] = 'facture';
$lang['invoice_added_successfully'] = 'facture ajoutée avec succès';
$lang['invoice_amount'] = 'montant facture';
$lang['invoice_date'] = 'date facture';
$lang['invoice_deleted_successfully'] = 'facture supprimée avec succès';
$lang['invoice_emailed_successfully'] = 'facture envoyée par email avec succès';
$lang['invoice_fields'] = 'champs facture';
$lang['invoice_for'] = 'facture pour';
$lang['invoice_id'] = 'id facture';
$lang['invoice_information'] = 'information facture';
$lang['invoice_notes'] = 'notes facturation';
$lang['invoice_options'] = 'options de facturation';
$lang['invoice_payment_added_successfully'] = 'règlement facture ajouté avec succès';
$lang['invoice_payment_deleted_successfully'] = 'règlement facture supprimé avec succès';
$lang['invoice_payment_updated_successfully'] = 'règlement facture mis à jour avec succès';
$lang['invoice_payments'] = 'règlements factures';
$lang['invoice_payments_updated_successfully'] = 'règlement facture mis à jour avec succès';
$lang['invoice_settings'] = 'paramètres facture';
$lang['invoice_updated_successfully'] = 'facture mise à jour avec succès';
$lang['invoices'] = 'factures';
$lang['invoices_updated_successfully'] = 'facture mise à jour avec succès';
$lang['ip_address'] = 'adresse IP';
$lang['iso_2'] = 'iSO2';
$lang['iso_3'] = 'iSO3';
$lang['item'] = 'article';
$lang['JAM_supported_browsers'] = 'la console d\'administration de JAM requiert Firefox ou + ou Internet Explorer 7 ou +';
$lang['jrox_custom_field_1'] = 'champ défini membre 1';
$lang['jrox_custom_field_10'] = 'champ défini membre 10';
$lang['jrox_custom_field_2'] = 'champ défini membre 2';
$lang['jrox_custom_field_3'] = 'champ défini membre 3';
$lang['jrox_custom_field_4'] = 'champ défini membre 4';
$lang['jrox_custom_field_5'] = 'champ défini membre 5';
$lang['jrox_custom_field_6'] = 'champ défini membre 6';
$lang['jrox_custom_field_7'] = 'champ défini membre 7';
$lang['jrox_custom_field_8'] = 'champ défini membre 8';
$lang['jrox_custom_field_9'] = 'champ défini membre 9';
$lang['keywords'] = 'mots-clés';
$lang['kg'] = 'kg';
$lang['landing_page'] = 'page d\'aterrissage';
$lang['language'] = 'langue';
$lang['language_added_successfully'] = 'langue ajoutée avec succès';
$lang['language_file_not_writeable'] = 'le fichier langue ne peut être ré-enregistré.  vous ne pouvez éditer aucun champ linguistique à moins d\'autoriser ce fichier au ré-enregistrement ou chmod 777';
$lang['language_file_writeable'] = 'le fichier langue est ré-enregistrable';
$lang['language_name'] = 'nom de langue';
$lang['languages'] = 'langues';
$lang['last'] = 'dernier(e)';
$lang['last_login'] = 'dernière connexion';
$lang['last_login_date'] = 'date de la dernière connexion';
$lang['last_login_date_ip'] = 'date et IP de la dernière connexion';
$lang['last_login_ip'] = 'iP dernière connexion';
$lang['last_modified'] = 'modifié pour la dernière fois';
$lang['last_name'] = 'nom';
$lang['last_response'] = 'dernière réponse';
$lang['last_updated_by'] = 'mis à jour la dernière fois par';
$lang['last_updated_on'] = 'mis à jour la dernière fois le';
$lang['layout_box_code'] = 'code box de mise en page';
$lang['layout_box_deleted_successfully'] = 'box de mise en page supprimé avec succès';
$lang['layout_box_description'] = 'description du box de la mise en page';
$lang['layout_box_file_name'] = 'nom du fichier du box de mise en page';
$lang['layout_box_location'] = 'localisation du box';
$lang['layout_box_manager'] = 'gestionnaire du box de mise en page';
$lang['layout_box_name'] = 'nom du box de mise en page';
$lang['layout_box_name'] = 'mise_en_page_box_name';
$lang['layout_boxes_updated_successfully'] = 'box de layout mis à jour avec succès';
$lang['layout_design_custom_css'] = 'mise en page définie du CSS pour le site';
$lang['layout_design_custom_home_page_redirect'] = 'mise en page de redirection de la page d\'accueil';
$lang['layout_design_enable_column_home_page'] = 'autoriser les espaces de colonne sur la page d\'accueil';
$lang['layout_design_enable_footer_menu'] = 'autoriser le menu de pied de page';
$lang['layout_design_enable_left_checkout'] = 'autoriser un espace à gauche de la colonne dans la zone de caisse';
$lang['layout_design_enable_left_column_store'] = 'autoriser un espace à gauche de la colonne dans la boutique';
$lang['layout_design_enable_left_content_pages'] = 'autoriser un espace à gauche de la colonne dans le contenu des pages';
$lang['layout_design_enable_left_members_area'] = 'autoriser un espace à gauche de la colonne dans l\'espace membre';
$lang['layout_design_enable_left_product_details'] = 'autoriser l\'espace à gauche de la colonne dans les détails de produit';
$lang['layout_design_enable_left_support_desk'] = 'autoriser un espace à gauche de la colonne dans le desk de support';
$lang['layout_design_enable_right_checkout'] = 'autoriser l\'espace de colonne droite dans l\'espace de la caisseright column area in checkout area';
$lang['layout_design_enable_right_column_home_page'] = 'autoriser un espace à droite de la colonne sur la page d\'accueil';
$lang['layout_design_enable_right_column_store'] = 'autoriser un espace à droite de la colonne dans la boutique';
$lang['layout_design_enable_right_content_pages'] = 'autoriser un espace à droite de la colonne dans le contenu des pages';
$lang['layout_design_enable_right_members_area'] = 'autoriser un espace à droite de la colonne dans l\'espace membre';
$lang['layout_design_enable_right_product_details'] = 'autoriser un espace à droite de la colonne dans les détails de produit';
$lang['layout_design_enable_right_support_desk'] = 'autoriser un espace à droite de la colonne dans le desk de support';
$lang['layout_design_enable_top_menu'] = 'autoriser le top menu';
$lang['layout_design_header_name'] = 'nom du site';
$lang['layout_design_header_tag_line'] = 'ligne tag du site';
$lang['layout_design_home_page_redirect'] = 'page d\'accueil par défaut';
$lang['layout_design_site_logo'] = 'logo site';
$lang['layout_design_site_logo_height'] = 'hauteur logo';
$lang['layout_design_site_logo_width'] = 'largeurlogo';
$lang['layout_menus_updated_successfully'] = 'menus de mise en page mis à jour avec succès';
$lang['layout_options'] = 'options de mise en page';
$lang['layout_themes']  = 'thèmes de mise en page';
$lang['lbs'] = 'lbs';
$lang['left'] = 'gauche';
$lang['left_block'] = 'bloc gauche';
$lang['left_boxes'] = 'cases gauches';
$lang['left_column'] = 'colonne gauche';
$lang['left_menu'] = 'menu gauche';
$lang['left_menu_links'] = 'liens de menu gauches';
$lang['level'] = 'niveau';
$lang['level_1'] = '1';
$lang['level_10'] = '10';
$lang['level_2'] = '2';
$lang['level_3'] = '3';
$lang['level_4'] = '4';
$lang['level_5'] = '5';
$lang['level_6'] = '6';
$lang['level_7'] = '7';
$lang['level_8'] = '8';
$lang['level_9'] = '9';
$lang['levels'] = 'niveau(x)';
$lang['liberty_reserve_payments'] = 'paiements liberty reserve';
$lang['license_settings'] = 'paramétrages licence';
$lang['licensed_by'] = 'licence octroyée par';
$lang['link_name'] = 'lier le nom';
$lang['link_url'] = 'URL de lien';
$lang['list_ids'] = 'ids de liste';
$lang['lname'] = 'nom';
$lang['load_custom_template'] = 'attacher le template défini';
$lang['load_template'] = 'attacher template';
$lang['loading_please_wait'] = 'chargement en cours.... Veuillez patienter';
$lang['logged_by'] = 'connecté par';
$lang['logged_in_as'] = 'connecté en temps que'; 
$lang['login'] = 'connexion';
$lang['login_for_price'] = 'se connecter pour voir le prix';
$lang['login_status'] = 'compte accepté';
$lang['login_to_members_area_as'] = 'connexion à l\'espace membre en temps que';
$lang['login_update_stats'] = 'connexions et mise à jour des statistiques';
$lang['login_url'] = 'URL de conexion';
$lang['logo'] = 'logo';
$lang['logo_options'] = 'options logo';
$lang['logout'] = 'déconnexion';
$lang['low'] = 'bas';
$lang['mailing_list'] = 'liste de diffusion';
$lang['mailing_list_added_successfully'] = 'liste de diffusion ajoutée avec succès';
$lang['mailing_list_deleted_successfully'] = 'liste de diffusion supprimée avec succès';
$lang['mailing_list_id'] = 'iD de la liste de diffusion';
$lang['mailing_list_name'] = 'nom de la liste  de diffusion';
$lang['mailing_list_updated_successfully'] = 'liste de diffusion mise à jour avec succès';
$lang['mailing_lists'] = 'listes de diffusion';
$lang['make_affiliate_payments'] = 'effectuer les paiements d\'affiliation';
$lang['make_payments'] = 'effectuer des paiements';
$lang['manage'] = 'gérer';
$lang['manage_admin'] = 'gérer admin';
$lang['manage_admins'] = 'gérer admins';
$lang['manage_aff_ad_categories'] = 'gérer les catégories d\'annonces d\'affiliation';
$lang['manage_affiliate_groups'] = 'gérer les groupes d\'affiliation';
$lang['manage_affiliate_marketing_modules'] = 'gérer les modules du marketing d\'affiliation';
$lang['manage_affiliate_payment'] = 'gérer le paiement d\'affiliation';
$lang['manage_affiliate_payments'] = 'gérer les paiements d\'affiliation';
$lang['manage_affiliate_tools'] = 'gérer outils d\'affiliation';
$lang['manage_affiliate_traffic'] = 'gérer le traffic d\'affiliation';
$lang['manage_article_ad'] = 'gérer l\'article publicitaire';
$lang['manage_article_ads'] = 'gérer les articles publicitaires';
$lang['manage_attributes'] = 'gérer les attributs';
$lang['manage_banner'] = 'gérer la bannière';
$lang['manage_banners'] = 'gérer les bannières';
$lang['manage_boxes'] = 'gérer les layout boxes';
$lang['manage_categories'] = 'gérer catégories';
$lang['manage_comments'] = 'gérer les commentaires';
$lang['manage_commission'] = 'gérer commission';
$lang['manage_commissions'] = 'gérer commissions';
$lang['manage_content'] = 'gérer contenu';
$lang['manage_content_categories'] = 'gérer les categories de contenu';
$lang['manage_countries'] = 'gérer les pays';
$lang['manage_coupon'] = 'gérer coupon';
$lang['manage_coupons'] = 'gérer coupons';
$lang['manage_credit'] = 'gérer crédit';
$lang['manage_currencies'] = 'gérer devises';
$lang['manage_discount_groups'] = 'gérer groupes bénéficiant de rabais';
$lang['manage_email_ad'] = 'gérer email publicitaire';
$lang['manage_email_ads'] = 'gérer emails publicitaires';
$lang['manage_email_archive'] = 'gérer archives d\'email';
$lang['manage_email_queue'] = 'gérer email à la file d\'attente';
$lang['manage_email_templates'] = 'gérer les templates d\'email';
$lang['manage_export_modules'] = 'gérer modules d\'exportation';
$lang['manage_faq'] = 'gérer la FAQ';
$lang['manage_faq_categories'] = 'gérer les catégories de FAQ';
$lang['manage_follow_up'] = 'gérer suivi';
$lang['manage_follow_ups'] = ' gérer les courriers de suivi';
$lang['manage_forms'] = 'gérer les formulaires';
$lang['manage_gateways'] = 'gérer les terminaux de paiement';
$lang['manage_gateways_modules'] = 'gérer les modules des terminaux';
$lang['manage_groups'] = 'gérer groupes';
$lang['manage_html_ad'] = 'gérer annonce html';
$lang['manage_html_ads'] = 'gérer annonces html';
$lang['manage_import_export'] = 'gérer l\'importation et l\'exportation';
$lang['manage_import_modules'] = 'gérer modules d\'importation';
$lang['manage_invoice'] = 'gérer facture';
$lang['manage_invoice_payment'] = 'gérer règlement de facture';
$lang['manage_invoice_payments'] = 'gérer règlements de factures';
$lang['manage_invoices'] = 'gérer factures';
$lang['manage_language'] = 'gérer langue';
$lang['manage_languages'] = 'gérer langues';
$lang['manage_layout'] = 'gérer mise en page';
$lang['manage_layout_boxes'] = 'gérer les box de mise en page';
$lang['manage_mailing_list'] = 'gérer liste de diffusion';
$lang['manage_mailing_lists'] = 'gérer listes de diffusion';
$lang['manage_manufacturers'] = 'gérer fournisseurs';
$lang['manage_member'] = 'gérer membre';
$lang['manage_member_credits'] = 'gérer les crédits de membre';
$lang['manage_members'] = 'gérer membre';
$lang['manage_modules'] = 'gérer modules';
$lang['manage_options'] = 'gérer options';
$lang['manage_orders'] = 'gérer commandes';
$lang['manage_payment'] = 'gérer paiement';
$lang['manage_payments'] = 'gérer paiements';
$lang['manage_product_attributes'] = 'gérer les attributs du produit';
$lang['manage_product_categories'] = 'gérer les catégories de produit';
$lang['manage_products'] = 'gérer les produits';
$lang['manage_region'] = 'gérer la région';
$lang['manage_regions'] = 'gérer les régions';
$lang['manage_reviews'] = 'gérer les revues';
$lang['manage_settings'] = 'gérer les paramètres';
$lang['manage_shipping'] = 'gérer l\'expédition';
$lang['manage_shipping_modules'] = 'gérer les modules d\'expédition';
$lang['manage_sub_menus'] = 'gérer les sous-menus';
$lang['manage_sub_menus'] = 'gérer les sous-menus';
$lang['manage_sub_menus_for_category'] = 'gérer les sous- menus pour cette entrée';
$lang['manage_support_categories'] = 'gérer les catégories de support';
$lang['manage_support_tickets'] = 'gérer les requêtes au support';
$lang['manage_tax_classes'] = 'gérer les types de taxe';
$lang['manage_tax_zones'] = 'gérer les zones fiscales';
$lang['manage_taxes'] = 'gérer zone fiscale';
$lang['manage_taxes'] = 'gérer les taxes';
$lang['manage_text_ad'] = 'gérer l\'annonces textuelle';
$lang['manage_text_ads'] = 'gérer les annonces textuelles';
$lang['manage_text_link'] = 'gérer le lien textuel';
$lang['manage_text_links'] = 'gérer les liens textuels';
$lang['manage_themes'] = 'gérer les thèmes';
$lang['manage_ticket'] = 'gérer requête';
$lang['manage_tracking'] = 'gérer les outils de suivi d\'annonce';
$lang['manage_vendors'] = 'gérer marchands';
$lang['manage_viral_pdf'] = 'gérer pdf viral';
$lang['manage_viral_pdfs'] = 'gérer pdfs viraux';
$lang['manage_viral_video'] = 'gérer vidéo virale';
$lang['manage_viral_videos'] = 'gérer vidéos virales';
$lang['manage_zone'] = 'gérer zone';
$lang['manage_zones'] = 'gérer les zones';
$lang['manufacturer'] = 'fournisseur';
$lang['manufacturer_added_successfully'] = 'fournisseur ajouté avec succès';
$lang['manufacturer_deleted_successfully'] = 'fournisseur supprimer avec succès';
$lang['manufacturer_description'] = 'description fournisseur';
$lang['manufacturer_image_file'] = 'fichier d\'image fournisseur';
$lang['manufacturer_page_title'] = 'titre de la page';
$lang['manufacturer_photo_for'] = 'logo du fournisseur pour';
$lang['manufacturer_updated_successfully'] = 'fournisseur mis à jour avec succès';
$lang['manufacturers'] = 'fournisseurs';
$lang['mark_checked_as'] = 'marquer vérifié comme';
$lang['mark_checked_as_active'] = 'marquer ce qui est coché comme étant actif';
$lang['mark_checked_as_approved'] = 'marquer ce qui est coché comme approuvé';
$lang['mark_checked_as_approved_for_payment'] = 'marquer ce qui est coché comme paiement approuvé';
$lang['mark_checked_as_closed'] = 'marquer ce qui est coché comme clos';
$lang['mark_checked_as_completed_order'] = 'marquer ce qui est coché comme étant une commande  aboutie';
$lang['mark_checked_as_completed_order_email'] = 'marquer ce qui est coché comme étant terminé - statut d\'envoi de l\'email';
$lang['mark_checked_as_featured'] = 'montrer ce qui est coché sur la page VIP';
$lang['mark_checked_as_inactive'] = 'marquer ce qui est coché comme étant inactif';
$lang['mark_checked_as_member_active'] = 'marquer ce qui est coché comme étant actif - ne pas envoyer de email';
$lang['mark_checked_as_member_deleted'] = 'marquer ce qui est coché pour suppression- pas de données';
$lang['mark_checked_as_member_inactive'] = 'marquer ce qui est coché comme inactif - pas de connexion';
$lang['mark_checked_as_not_ship_to'] = 'désactiver l\'expédition pour les pays sélectionnés';
$lang['mark_checked_as_open'] = 'marquer ce qui est coché comme étant ouvert';
$lang['mark_checked_as_pending'] = 'marquer ce qui est coché comme étant en attente';
$lang['mark_checked_as_pending_order'] = 'marquer ce qui est coché comme étant une commande pendante';
$lang['mark_checked_as_processing_order'] = 'marquer ce qui est  coché comme étant une commande en cours de traitement';
$lang['mark_checked_as_processing_order_email'] = 'marquer ce qui est coché comme en cours de traitement- statut d\'envoi de l\'email';
$lang['mark_checked_as_ship_to'] = 'autoriser l\'expdition vrs les pays sélectionnés';
$lang['mark_checked_as_unapproved'] = 'marquer ce qui est coché comme non approuvé';
$lang['mark_checked_as_unpaid'] = 'marquer ce qui est sélectionné comme impayé';
$lang['mark_checked_for_deletion'] = 'marquer e qui est coché pour la suppression';
$lang['mark_commissions_as_paid'] = 'marquer ce qui est coché comme étant payé';
$lang['marketing'] = 'marketing';
$lang['marketing_tools'] = 'ouils marketing';
$lang['mass_import_product_images'] = 'importation en masse d\'images de produits';
$lang['mass_import_product_images_notes'] = 'Pour importer en masse les images de produits ,vous devez créer un classeur séparé pour chaque produit dans le classeur /import/products de l\'installation de votre boutique. Chaque classeur doit porter soit le nom de l\'iD du produit soit le numéro SKU du produit.  Chargez les images de chaque produit dans chaque sous-classeur via FTP ou n\'importe quel outil de chargement. Une fois les images chargées, cliquez sur le bouton pour soumettre';
$lang['max_downloads_per_user'] = 'téléchargements max par utilisateur';
$lang['max_file_upload'] = 'le maximum de fichier à attacher est fixé par votre serveur.  normalement c\'est 2MB';
$lang['max_quantity_ordered'] = 'quantité de commande maximum';
$lang['media'] = 'media';
$lang['member'] = 'membre';
$lang['member_credits'] = 'crédits du membre';
$lang['member_details'] = 'détails du membre';
$lang['member_email_taken'] = 'adresse email du membre prise';
$lang['member_fields'] = 'champs du membre';
$lang['member_id'] = 'iD membre';
$lang['member_information'] = 'information membre';
$lang['member_name'] = 'nom du membre';
$lang['member_photo'] = 'photo membre';
$lang['member_photos'] = 'photos membre';
$lang['member_profile'] = 'profi membre';
$lang['member_quick_links'] = 'liens rapides membre';
$lang['member_response_by'] = 'réponse qu membre par';
$lang['member_stats'] = 'stats membre';
$lang['member_tab_custom_field_1'] = 'personnaliser champ 1';
$lang['member_tab_custom_field_10'] = 'personnaliser champ 10';
$lang['member_tab_custom_field_2'] = 'personnaliser champ 2';
$lang['member_tab_custom_field_3'] = 'personnaliser champ 3';
$lang['member_tab_custom_field_4'] = 'personnaliser champ 4';
$lang['member_tab_custom_field_5'] = 'personnaliser champ 5';
$lang['member_tab_custom_field_6'] = 'personnaliser champ 6';
$lang['member_tab_custom_field_7'] = 'personnaliser champ 7';
$lang['member_tab_custom_field_8'] = 'personnaliser champ 8';
$lang['member_tab_custom_field_9'] = 'personnaliser champ9';
$lang['member_tab_custom_fields'] = 'champs';
$lang['member_username'] = 'identifiant membre';
$lang['member_username_taken'] = 'identifiant membre pris';
$lang['members'] = 'membres';
$lang['members_area'] = 'espace membres';
$lang['members_credited'] = 'membre(s) crédités';
$lang['members_login'] = 'connexion membere';
$lang['members_menu'] = 'menu membre';
$lang['members_menu_links'] = 'liens de menu pour membres';
$lang['members_updated_successfully'] = 'membres mis à jour avec succès';
$lang['membership'] = 'adhésion';
$lang['membership_added_successfully'] = 'adhésion ajoutée avec succès';
$lang['membership_deleted_successfully'] = 'adhésion supprimée avec succès';
$lang['membership_form'] = 'formulaire d\'ahésion';
$lang['membership_information'] = 'information sur la souscription d\'adhésion';
$lang['membership_pricing'] = 'prix de la souscription d\'adhésion';
$lang['membership_pricing_options'] = 'prix et options de la souscription d\'adhésion';
$lang['membership_product_required'] = 'produit d\'adhésion obligatoire';
$lang['membership_products'] = 'produits d\'adhésion';
$lang['membership_updated_successfully'] = 'souscription d\'adhésion mise à jour avec succès';
$lang['membership_url_redirect'] = 'URL de redirection de la souscription d\'adhésion';
$lang['memberships'] = 'adhésions';
$lang['memberships_groups'] = 'adhésions et groupes';
$lang['menu_maker'] = 'générateur de menu';
$lang['method'] = 'méthode';
$lang['min_amount'] = 'minimum';
$lang['min_quantity_ordered'] = 'quantité minimum à commander';
$lang['minute'] = 'minute';
$lang['minutes'] = 'minutes';
$lang['mobile'] = 'mobile';
$lang['mobile_phone'] = 'téléphone mobile';
$lang['moderate_comments'] = 'modérer les commentaires';
$lang['module'] = 'module';
$lang['module_affiliate_marketing_banners_file_types'] = 'autoriser les types de bannières';
$lang['module_affiliate_marketing_html_ads_default_bg_body_color'] = 'couleur d\'arrière plan par défaut du texte';
$lang['module_affiliate_marketing_html_ads_default_bg_title_color'] = 'couleur d\'arrière plan par défaut du titre';
$lang['module_affiliate_marketing_html_ads_default_border_color'] = 'couleur de bordure par défaut';
$lang['module_affiliate_marketing_html_ads_default_font_body_color'] = 'couleur font body color';
$lang['module_affiliate_marketing_html_ads_default_font_title_color'] = 'couleur par défaut du titre';
$lang['module_affiliate_marketing_html_ads_default_html_ad_width'] = 'largeur par défaut de l\'annonce html';
$lang['module_affiliate_marketing_text_ads_default_bg_body_color'] = 'couleur d\'arrière-plan par défaut';
$lang['module_affiliate_marketing_text_ads_default_bg_title_color'] = 'couleur d\'arrière-plan par défaut du titre';
$lang['module_affiliate_marketing_text_ads_default_border_color'] = 'couleur par défaut bordures';
$lang['module_affiliate_marketing_text_ads_default_font_body_color'] = 'couleur par défaut du titre';
$lang['module_affiliate_marketing_text_ads_default_font_title_color'] = 'couleur par défaut du titre';
$lang['module_affiliate_marketing_text_ads_default_text_ad_width'] = 'largeur par défaut de l\'article publicitaire';
$lang['module_affiliate_marketing_viral_pdfs_orientation'] = 'orientation PDF viral';
$lang['module_affiliate_marketing_viral_pdfs_paper_size'] = 'taille PDF viral';
$lang['module_already_installed'] = 'module_already_installed';
$lang['module_data_export_affiliate_payments_delimiter'] = 'delimiteur d\'exportation';
$lang['module_data_export_affiliate_payments_starting_rows'] = 'numéro 1ère rangée';
$lang['module_data_export_affiliate_payments_total_rows'] = 'total rangées à exporter';
$lang['module_data_export_commissions_delimiter'] = 'délimiteur d\'exportation';
$lang['module_data_export_commissions_starting_rows'] = 'numéro 1ère rangée';
$lang['module_data_export_commissions_total_rows'] = 'total rangées à exporter';
$lang['module_data_export_invoice_payments_delimiter'] = 'export delimiter';
$lang['module_data_export_invoice_payments_ending_date'] = 'date clôture';
$lang['module_data_export_invoice_payments_starting_date'] = 'date début';
$lang['module_data_export_invoice_payments_starting_rows'] = 'numéro de 1ère rangée';
$lang['module_data_export_invoice_payments_total_rows'] = 'total rangées à exporter';
$lang['module_data_export_invoices_delimiter'] = 'délimiteur d\'exportation';
$lang['module_data_export_invoices_ending_date'] = 'date clôture';
$lang['module_data_export_invoices_starting_date'] = 'date de début';
$lang['module_data_export_invoices_starting_rows'] = 'numéro de 1ère rangée';
$lang['module_data_export_invoices_total_rows'] = 'total rangées à exporter';
$lang['module_data_export_members_delimiter'] = 'délimiteur d\'exportation';
$lang['module_data_export_members_starting_rows'] = 'numéro de 1ère rangée';
$lang['module_data_export_members_total_rows'] = 'total rangées à exporter';
$lang['module_data_export_products_delimiter'] = 'délimiteur d\'exportation';
$lang['module_data_export_products_starting_rows'] = 'numéro de 1ère rangée';
$lang['module_data_export_products_total_rows'] = 'total rangées à exporter';
$lang['module_data_import_jam_affiliate_limit'] = 'nombre de rangées à limiter';
$lang['module_data_import_jam_affiliate_offset'] = 'intervertir le numéro des rangées';
$lang['module_data_import_jam_commission_limit'] = 'nombre de rangées de commission à limiter';
$lang['module_data_import_jam_commission_offset'] = 'intervertir le numéro des rangées';
$lang['module_data_import_jam_database'] = 'nom base de données JAM';
$lang['module_data_import_jam_last_id'] = '1er ID de membre dans JAM';
$lang['module_data_import_jam_password'] = 'mot de passe base de données de JAM';
$lang['module_data_import_jam_payment_limit'] = 'nombre de rangées de paiement à limiter';
$lang['module_data_import_jam_payment_offset'] = 'intervertir le numéro des rangées';
$lang['module_data_import_jam_segment_affiliates'] = 'autoriser la migration segmentée pour le tableau d\'affiliation';
$lang['module_data_import_jam_segment_commissions'] = 'autoriser la migration segmentée pour le tableau des commissions';
$lang['module_data_import_jam_segment_payments'] = 'migration segmentée pour tableau de paiement';
$lang['module_data_import_jam_segment_traffic'] = 'autoriser migration segmentée pour le tableau de traffic';
$lang['module_data_import_jam_server'] = 'serveur base de données JAM';
$lang['module_data_import_jam_traffic_limit'] = 'nombre de rangées de traffic à limiter';
$lang['module_data_import_jam_traffic_offset'] = 'intervertir le numéro des rangées';
$lang['module_data_import_jam_username'] = 'identifiant base de données JAM';
$lang['module_data_import_mass_edit_products_delimiter'] = 'délimiteur de fichier';
$lang['module_data_import_mass_edit_products_heading_first_row'] = 'mettre la 1ère rangée comme tête de colonne';
$lang['module_data_import_mass_edit_products_use_skus'] = 'utiliser les SKUs de produit en lieu et place des numéros de produit';
$lang['module_data_import_members_delimiter'] = 'délimiteur de fichier';
$lang['module_data_import_members_generate_new_ids'] = 'générer nouveaux numéros';
$lang['module_data_import_members_heading_first_row'] = 'mettre la 1ère rangée comme tête de colonne';
$lang['module_data_import_product_images_delete_photos'] = 'supprimer les images du classeur d\'importation après les avoir attachés';
$lang['module_data_import_product_images_use_skus'] = 'utiliser les SKUs de produit en lieu et place des numéros de produit';
$lang['module_data_import_products_delimiter'] = 'délimiteur de fichier';
$lang['module_data_import_products_generate_new_ids'] = 'générer de nouveaux numéros';
$lang['module_data_import_products_heading_first_row'] = 'mettre la 1ère rangée comme tête de colonne';
$lang['module_description'] = 'description module';
$lang['module_file_name'] = 'nom fichier module';
$lang['module_installed_successfully'] = 'module installé avec succès';
$lang['module_name'] = 'nom module';
$lang['module_name'] = 'nom module';
$lang['module_not_correctly_installed'] = 'module installé de façon incorrecte';
$lang['module_payment_gateway_2checkout_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_2checkout_enable_testing'] = 'autoriser démo';
$lang['module_payment_gateway_2checkout_id'] = 'iD 2Checkout';
$lang['module_payment_gateway_2checkout_md5_hash'] = 'mot secret 2checkout';
$lang['module_payment_gateway_2checkout_url'] = 'formulaire URL 2Checkout';
$lang['module_payment_gateway_alertpay_currency_code'] = 'code devise AlertPay ';
$lang['module_payment_gateway_alertpay_disable_verification'] = 'code sécurité AlertPay';
$lang['module_payment_gateway_alertpay_email'] = 'email AlertPay';
$lang['module_payment_gateway_alertpay_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_alertpay_url'] = 'URL formulaire AlertPay';
$lang['module_payment_gateway_authorize_net_aim_authorization_type'] = 'type de permission';
$lang['module_payment_gateway_authorize_net_aim_enable_arb'] = 'autoriser facturation récurrente (BETA)';
$lang['module_payment_gateway_authorize_net_aim_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_authorize_net_aim_enable_testing'] = 'autoriser le test';
$lang['module_payment_gateway_authorize_net_aim_md5_hash'] = 'autoriser le hash md5';
$lang['module_payment_gateway_authorize_net_aim_merchant_id'] = 'iD marchand';
$lang['module_payment_gateway_authorize_net_aim_require_cvv'] = 'autoriser code de sécurité / CVV';
$lang['module_payment_gateway_authorize_net_aim_send_customer_notification'] = 'envoyer notification au client';
$lang['module_payment_gateway_authorize_net_aim_send_merchant_notification'] = 'envoyer notification au marchand';
$lang['module_payment_gateway_authorize_net_aim_transaction_key'] = 'clé de transaction';
$lang['module_payment_gateway_authorize_net_aim_url'] = 'URL de paiement';
$lang['module_payment_gateway_chargeback_guardian_authorization_type'] = 'type de permission';
$lang['module_payment_gateway_chargeback_guardian_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_chargeback_guardian_password'] = 'mot de passe pour rétracterla transaction';
$lang['module_payment_gateway_chargeback_guardian_require_cvv'] = 'autoriser champ CVV';
$lang['module_payment_gateway_chargeback_guardian_url'] = 'URL de paiement';
$lang['module_payment_gateway_chargeback_guardian_username'] = 'identifiant pour rétracter la transaction';
$lang['module_payment_gateway_egold_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_egold_id'] = 'iD e-gold';
$lang['module_payment_gateway_egold_payment_units'] = 'unités de paiement';
$lang['module_payment_gateway_egold_secret_word'] = 'alternate passphrase';
$lang['module_payment_gateway_egold_url'] = 'URL formulaire e-gold';
$lang['module_payment_gateway_eway_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_eway_enable_testing'] = 'autoriser test';
$lang['module_payment_gateway_eway_merchant_id'] = 'iD marchand';
$lang['module_payment_gateway_eway_payment_method'] = 'méthode';
$lang['module_payment_gateway_eway_require_cvv'] = 'CVV obligatoire';
$lang['module_payment_gateway_eway_url'] = 'URL formulaire';
$lang['module_payment_gateway_firstdata_connect_enable_debug'] = 'autoriser déboggage';
$lang['module_payment_gateway_firstdata_connect_enable_debug_email'] = 'autoriser déboggage Email';
$lang['module_payment_gateway_firstdata_connect_mode'] = 'Mode';
$lang['module_payment_gateway_firstdata_connect_storename'] = 'Nom Boutique';
$lang['module_payment_gateway_firstdata_connect_txntype'] = 'Type Transaction';
$lang['module_payment_gateway_firstdata_connect_url'] = 'URL de Connexion';
$lang['module_payment_gateway_goemerchant_authorization_type'] = 'Type de permission';
$lang['module_payment_gateway_goemerchant_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_goemerchant_merchant_id'] = 'connexion';
$lang['module_payment_gateway_goemerchant_require_cvv'] = 'autoriser code de sécurité / CVV';
$lang['module_payment_gateway_goemerchant_transaction_key'] = 'clé de transaction';
$lang['module_payment_gateway_goemerchant_url'] = 'URL de Connexion';
$lang['module_payment_gateway_liberty_reserve_currency_code'] = 'code devise';
$lang['module_payment_gateway_liberty_reserve_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_liberty_reserve_id'] = 'iD liberty reserve';
$lang['module_payment_gateway_liberty_reserve_secret_word'] = 'mot secret';
$lang['module_payment_gateway_liberty_reserve_store_name'] = 'nom boutique';
$lang['module_payment_gateway_liberty_reserve_url'] = 'URL formulaire';
$lang['module_payment_gateway_nochex_disable_verification'] = 'désactiver vérification';
$lang['module_payment_gateway_nochex_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_nochex_enable_testing'] = 'autoriser mode test';
$lang['module_payment_gateway_nochex_id'] = 'iD NoChex';
$lang['module_payment_gateway_nochex_url'] = 'url formulaire NoChex ';
$lang['module_payment_gateway_offline_payment_download_link'] = 'lien du bon de commande hors site';
$lang['module_payment_gateway_offline_payment_message'] = 'message de remerciements';
$lang['module_payment_gateway_payjunction_password'] = 'mot de passe Payjunction';
$lang['module_payment_gateway_payjunction_require_cvv'] = 'autoriser code de sécurité / CVV';
$lang['module_payment_gateway_payjunction_transaction_type'] = 'type de transaction';
$lang['module_payment_gateway_payjunction_url'] = 'URL de paiement';
$lang['module_payment_gateway_payjunction_username'] = 'identifiant Payjunction';
$lang['module_payment_gateway_paypal_standard_currency_code'] = 'code devise paypal';
$lang['module_payment_gateway_paypal_standard_disable_verification'] = 'désactiver IPN de vérification paypal';
$lang['module_payment_gateway_paypal_standard_email'] = 'email paypal';
$lang['module_payment_gateway_paypal_standard_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_paypal_standard_header_image'] = 'image en-tête';
$lang['module_payment_gateway_paypal_standard_url'] = 'URL formulaire paypal';
$lang['module_payment_gateway_psigate_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_psigate_enable_testing'] = 'autoriser le test';
$lang['module_payment_gateway_psigate_merchant_id'] = 'iD marchand';
$lang['module_payment_gateway_psigate_passphrase'] = 'passphrase';
$lang['module_payment_gateway_psigate_require_cvv'] = 'CVV obligatoire';
$lang['module_payment_gateway_psigate_url'] = 'URL formulaire PSIGate';
$lang['module_payment_gateway_vcs_currency_code'] = 'code devise';
$lang['module_payment_gateway_vcs_enable_debug_email'] = 'autoriser déboggage email';
$lang['module_payment_gateway_vcs_id'] = 'iD VCS';
$lang['module_payment_gateway_vcs_url'] = 'URL de paiement';
$lang['module_shipping_by_percent_amount'] = 'pourcentage par vente';
$lang['module_shipping_by_percent_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_by_percent_handling_fee'] = 'frais de traitement';
$lang['module_shipping_by_percent_tax_zone'] = 'sélectioner zone fiscale';
$lang['module_shipping_flat_rate_amount'] = 'taux fixe';
$lang['module_shipping_flat_rate_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_flat_rate_handling_fee'] = 'frais de traitement';
$lang['module_shipping_flat_rate_tax_zone'] = 'sélectionner zone fiscale';
$lang['module_shipping_free_shipping_amount'] = 'montant pour expédition gratuite';
$lang['module_shipping_free_shipping_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_free_shipping_handling_fee'] = 'frais de traitement';
$lang['module_shipping_free_shipping_quantity_type'] = 'type utilisé pour calculer l\'expédition gratuite';
$lang['module_shipping_free_shipping_required_amount'] = 'montant obligatoire pour bénéficier de l\'expédition gratuite';
$lang['module_shipping_free_shipping_tax_zone'] = 'sélectionner zone fiscale';
$lang['module_shipping_per_item_amount'] = 'frais d\'expédition par article';
$lang['module_shipping_per_item_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_per_item_handling_fee'] = 'frais de traitement';
$lang['module_shipping_per_item_tax_zone'] = 'sélectionner zone fiscale';
$lang['module_shipping_UPS_delivery_type'] = 'sélectionner méthode de livraison';
$lang['module_shipping_UPS_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_UPS_handling_fee'] = 'frais de traitement';
$lang['module_shipping_UPS_packaging'] = 'UPS Packaging';
$lang['module_shipping_UPS_pickup_method'] = 'méthode de collecte UPS';
$lang['module_shipping_UPS_shipping_types'] = 'sélectionner types d\'expédition UPS';
$lang['module_shipping_UPS_tax_basis'] = 'base fiscale';
$lang['module_shipping_UPS_tax_zone'] = 'sélectionner zone fiscale';
$lang['module_shipping_USPS_container'] = 'container';
$lang['module_shipping_USPS_domestic_shipping_types'] = 'types d\'expédition dans le pays';
$lang['module_shipping_USPS_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_USPS_gateway_url'] = 'URL terminal';
$lang['module_shipping_USPS_handling_fee'] = 'frais de traitement';
$lang['module_shipping_USPS_international_shipping_types'] = 'types d\'expédition à l\'international';
$lang['module_shipping_USPS_machinable'] = 'machinable';
$lang['module_shipping_USPS_max_weight'] = 'poids maximum colis';
$lang['module_shipping_USPS_size'] = 'taille';
$lang['module_shipping_USPS_tax_zone'] = 'sélectionner zone fiscale';
$lang['module_shipping_USPS_userid'] = 'identifiant USPS';
$lang['module_shipping_zone_based_calculation_type'] = 'sélectionner méthode de calcul';
$lang['module_shipping_zone_based_enable_tax_zone'] = 'utiliser zone fiscale';
$lang['module_shipping_zone_based_skip_countries'] = 'Désactiver les pays - mettre ici les pays séparés de virgule en utilisant le code ISO à 2 chiffres pour qu\'ils soient dispensés d\'expédition gratuite.  l\'expédition ne sera pas autorisée vers ces pays';
$lang['module_shipping_zone_based_tax_calc_type'] = 'type d\'adresse pour le calcul des frais d\'expédition';
$lang['module_shipping_zone_based_tax_zone'] = 'sélectionner zone fiscale';
$lang['module_shipping_zone_based_zone_1_countries'] = 'pays zone 1- séparés par le code ISO à 2 chiffres';
$lang['module_shipping_zone_based_zone_1_handling_fee'] = 'frais de traitement de la zone 1';
$lang['module_shipping_zone_based_zone_1_table'] = 'tableau d\'expédition zone 1, Par exemple - 1:5.00,3:8.50';
$lang['module_shipping_zone_based_zone_2_countries'] = 'Pays zone 2 - séparés du code ISO à 2 chiffres';
$lang['module_shipping_zone_based_zone_2_handling_fee'] = 'frais de traitement zone 2';
$lang['module_shipping_zone_based_zone_2_table'] = 'tableau d\'expédition zone 2,  Par exemple - 1:5.00,3:8.50';
$lang['module_shipping_zone_based_zone_3_countries'] = 'Pays zone 3 - séparés du code ISO à 2 chiffres';
$lang['module_shipping_zone_based_zone_3_handling_fee'] = 'frais de traitement zone 3';
$lang['module_shipping_zone_based_zone_3_table'] = 'tableau d\'expédition zone 3,  Par exemple - 1:5.00,3:8.50';
$lang['module_shipping_zone_based_zone_4_handling_fee'] = 'frais de traitement zone 4';
$lang['module_shipping_zone_based_zone_4_table'] = 'tableau d\'expédition zone 4,  Par exemple - 1:5.00,3:8.50';
$lang['module_type'] = 'type module ';
$lang['moneybookers_id'] = 'iD Moneybookers';
$lang['moneybookers_id_taken'] = 'iD moneybookers pris';
$lang['month'] = 'mois';
$lang['month_top_affiliate_clicks'] = 'top affiliés par clic  par mois';
$lang['month_top_affiliate_commissions'] = 'top affiliés par commissions par mois';
$lang['month_top_affiliate_sales'] = 'top affiliés par ventes par mois';
$lang['monthly'] = 'mensuel';
$lang['monthly_ad_tracking_stats_by_year'] = 'stats mensuels de traçage /an';
$lang['monthly_affiliate_clicks_stats_by_day'] = 'clics quotidiens d\'affilé /mois';
$lang['monthly_commission_stats_by_day'] = 'commissions quotidiennes /mois';
$lang['monthly_invoice_payments_stats_by_day'] = 'règlements de facture quotidien/mois';
$lang['monthly_reports'] = 'rapports mensuels';
$lang['monthly_sales_stats_by_day'] = 'ventes quotidiennes /mois';
$lang['monthly_support_ticket_stats_by_day'] = 'requêtes quotidiennes au support /mois';
$lang['monthly_user_registrations_by_day'] = 'enregistrements quotidiens des utilisateurs/mois';
$lang['move_box_left'] = 'déplacer la case sur la colonne gauche';
$lang['move_box_right'] = 'déplacer la case sur la colonne droite';
$lang['mysql_version'] = 'version mysql';
$lang['name'] = 'nom';
$lang['needs_write_access'] = 'besoin d\'enregistrer l\'accès';
$lang['new_credit_amount'] = 'nouveau montant crédit';
$lang['new_order_status'] = 'statut nouvelle commande';
$lang['new_password'] = 'nouveau mot de passe';
$lang['new_products'] = 'nouveaux produits';
$lang['new_ticket'] = 'nouvelle requête';
$lang['next_invoice'] = 'facture suivante';
$lang['next_send_date'] = 'date d\'envoi suivante';
$lang['no'] = 'no';
$lang['no_admins_found'] = 'aucun admin trouvé sur cette page';
$lang['no_affiliate_payments_found'] = 'aucun paiement d\'affilié trouvé sur cette page';
$lang['no_affiliate_payments_found'] = 'aucun paiement d\'affilié retrouvé';
$lang['no_alert'] = 'pas d\'alerte';
$lang['no_article_ads_found'] = 'pas d\'article publicitaire trouvé sur cette page';
$lang['no_articles_found'] = 'aucun article trouvé sur cette page';
$lang['no_attributes_found'] = 'aucun attribut trouvé sur cette page';
$lang['no_banners_found'] = 'aucune bannière trouvée sur cette page';
$lang['no_categories_found'] = 'aucune catégorie trouvée sur cette page';
$lang['no_comments_found'] = 'aucun commentaire trouvé';
$lang['no_commission'] = 'désactiver commissions';
$lang['no_commissions_found'] = 'aucune commission trouvée';
$lang['no_commissions_made'] = 'aucune commission générée';
$lang['no_coupons_found'] = 'aucun coupon trouvé sur cette page';
$lang['no_credits_found'] = 'aucun crédit trouvé';
$lang['no_data_found'] = 'aucune donnée disponible sur cette page';
$lang['no_downline_members_found'] = 'aucun filleul trouvé';
$lang['no_downloadable_products_found'] = 'aucun produit téléchargeable trouvé';
$lang['no_email_ads_found'] = 'aucun email publicitaire trouvé sur cette page';
$lang['no_email_queue_found'] = 'aucun email en fil d\'attente trouvé sur cette page';
$lang['no_emails_found'] = 'aucun email trouvé sur cette page';
$lang['no_faq_articles_found'] = 'aucun article FAQ trouvé sur cette page';
$lang['no_faq_categories_found'] = 'aucune catégorie FAQ trouvée sur cette page';
$lang['no_follow_ups_found'] = 'aucune courriel de suivi trouvé sur cette page';
$lang['no_gateways_modules_found'] = 'aucun module de terminal de paiement trouvé sur cette page';
$lang['no_groups_found'] = 'aucun groupe trouvé sur cette page';
$lang['no_html_ads_found'] = 'aucune annonce html trouvée sur cette page';
$lang['no_image'] = 'pas d\'image';
$lang['no_invoice_found'] = 'aucune facture trouvée';
$lang['no_invoice_payments_found'] = 'aucune facture de règlement trouvée page';
$lang['no_invoices_found'] = 'aucune facture trouvée sur cette page';
$lang['no_language_found'] = 'aucune langue trouvée';
$lang['no_languages_found'] = 'aucune langue trouvée sur cette page';
$lang['no_list_users_found'] = 'aucun utilsateur listé trouvé';
$lang['no_lists_selected'] = 'aucune liste sélectionnée';
$lang['no_logs_found'] = 'aucun détail de connexion trouvé';
$lang['no_mailing_lists_found'] = 'aucune liste de diffusion trouvée sur cette page';
$lang['no_manufacturers_found'] = 'aucun fournisseur/fabricant trouvé sur cette page';
$lang['no_member_found'] = 'aucun membre trouvé';
$lang['no_members_found'] = 'aucun membre trouvé cette page';
$lang['no_members_selected'] = 'aucun membre sélectionné';
$lang['no_membership_products_found'] = 'aucune souscription de produits trouvée';
$lang['no_modules_found'] = 'aucun module trouvé sur cette page';
$lang['no_options_found'] = 'aucune option trouvée sur cette page';
$lang['no_payments_found'] = 'aucun paiement trouvé';
$lang['no_product_attributes_found'] = 'aucun attribut de produit trouvé sur cette page';
$lang['no_product_categories_found'] = 'aucune catégorie de produit trouvée sur cette page';
$lang['no_products_found'] = 'aucun produit trouvé sur cette page';
$lang['no_regions_found'] = 'aucune région trouvée';
$lang['no_reports_found'] = 'aucun rapport trouvé sur cette page';
$lang['no_reviews_found'] = 'aucune revue trouvée';
$lang['no_sales_made'] = 'aucune vente effectuée';
$lang['no_settings_available'] = 'pas de paramètres disponibles';
$lang['no_shipping_modules_found'] = 'pas de module d\'expédition sur cette page';
$lang['no_support_tickets_found'] = 'aucune requête adressée au support';
$lang['no_text_ads_found'] = 'aucune annonce textuelle trouvée sur cette page';
$lang['no_text_links_found'] = 'aucun lien textuel trouvé sur cette page';
$lang['no_theme_found'] = 'aucun thème trouvé sur cette page';
$lang['no_themes_found'] = 'aucun thème trouvé sur cette page';
$lang['no_tickets_found'] = 'aucune requête au support trouvée sur cette page';
$lang['no_tracking_found'] = 'aucun outil de suivi d\'annonces trouvé';
$lang['no_tracking_referrals_found'] = 'aucun suivi de filleuls trouvé';
$lang['no_traffic_found'] = 'aucun trafic d\'affilié trouvé sur cette page';
$lang['no_updates_found'] = 'aucune mise à jour trouvée';
$lang['no_uploaded_file_found'] = 'aucun fichier attaché trouvé';
$lang['no_users_found'] = 'aucun utilisateur trouvé';
$lang['no_vendors_found'] = 'aucun vendeur trouvé sur cette page';
$lang['no_viral_pdfs_found'] = 'pas de pdfs viraux trouvés sur cette page';
$lang['no_viral_videos_found'] = 'pas de vidéos virales trouvées sur cette page';
$lang['no_zones_found'] = 'aucune zone trouvée sur cette page';
$lang['nochex_payments'] = 'Paiements NoChex';
$lang['none'] = 'aucun(e)';
$lang['normal'] = 'normal';
$lang['not_approved'] = 'non approuvé';
$lang['not_assigned'] = 'pas encore assigné';
$lang['note_sts_affiliate_commission_levels'] = 'NB:  certains processeurs de paiement n\'acceptent pas de marketing d\'affiliation au délà d\'un niveau.  il est de votre responsabilité d\'être en règle avec les termes et conditions d\'utilisation du service de votre processeur de paiement';
$lang['note_trial_interval_type'] = 'NB: Certains terminaux de paiement ne permettent pas différents types d\'intervalles d\'essai à l\'exemple de Authorize.Net';
$lang['note_trial_pricing_only_certain_gateways'] = 'NB: le prix d\'essai marche seulement pour certains terminaux de paiement comme Paypal.  ne pas autoriser ceci si vous avez d\'autres options de paiement qui ne permettent pas le prix d\'essai';
$lang['notes'] = 'notes';
$lang['notify_update_services'] = 'notifier les services de mise à jour';
$lang['now'] = 'maintenant';
$lang['number_forward_slash_only'] = 'seuls les chiffres et barres obliques sont autorisés';
$lang['offline_payments'] = 'paiements hors internet';
$lang['old_credit_amount'] = 'ancien montant crédit';
$lang['onetime'] = 'une seule fois';
$lang['open'] = 'ouvert';
$lang['open_ticket'] = 'requêtes ouvertes';
$lang['option_name'] = 'nom d\'option';
$lang['option_names_must_be_alphanumeric'] = 'le nom de l\'option doit être alphanumerique';
$lang['options'] = 'options';
$lang['or'] = 'ou';
$lang['order_completed'] = 'terminée';
$lang['order_date'] = 'date commande';
$lang['order_pending'] = 'en attente';
$lang['order_processing'] = 'en cours de traitement';
$lang['order_status'] = 'statut commande';
$lang['orders'] = 'commandes';
$lang['overview'] = 'aprçu';
$lang['pagination_next'] = '&gt';
$lang['pagination_prev'] = '&lt';
$lang['paid'] = 'payé(e)';
$lang['parent_directory'] = 'répertoire-souche';
$lang['password'] = 'mot de passe';
$lang['pay-per-click'] = 'paiement au clic';
$lang['pay-per-lead'] = 'paiement par lead';
$lang['pay-per-sale'] = 'paiement par vente';
$lang['payment_added_successfully'] = 'paiement ajouté avec succès';
$lang['payment_address_1'] = 'adresse paiement 1';
$lang['payment_address_2'] = 'adresse paiement 2';
$lang['payment_amount'] = 'montant paiement';
$lang['payment_city'] = 'ville de paiement';
$lang['payment_country'] = 'pays de paiement';
$lang['payment_date'] = 'date paiement';
$lang['payment_details'] = 'détails paiement';
$lang['payment_fields'] = 'champs paiement';
$lang['payment_gateways'] = 'terminaux de paiement';
$lang['payment_gateways'] = 'terminaux de paiement';
$lang['payment_information'] = 'information de paiement';
$lang['payment_link'] = 'lien de paiement';
$lang['payment_method'] = 'méthode de paiement';
$lang['payment_name'] = 'nom du paiement';
$lang['payment_notes'] = 'notes paiement';
$lang['payment_options'] = 'options paiement';
$lang['payment_postal_code'] = 'code paiement postal';
$lang['payment_preference'] = 'préférence de paiement';
$lang['payment_preference_amount'] = 'montant de paiement préféré';
$lang['payment_state'] = 'Etat paiement';
$lang['payment_status'] = 'statut paiement';
$lang['payment_type'] = 'type paiement';
$lang['payments'] = 'paiements';
$lang['payments_html'] = 'paiements_html';
$lang['payments_text'] = 'paiements_text';
$lang['paypal_id'] = 'iD Paypal';
$lang['paypal_id_taken'] = 'iD paypal ID pris';
$lang['paypal_standard_payments'] = 'paiements standard paypal';

$lang['pending'] = 'en attente';
$lang['pending_commission'] = 'commission d\'affilié en attente';
$lang['pending_commissions'] = 'commissions d\'affilié en attente';
$lang['pending_invoice'] = 'facture en attente';
$lang['pending_invoices'] = 'factures en attente';
$lang['pending_no_email'] = 'en attente - pas d\'alerte';
$lang['pending_product_review'] = 'revues de produit en attente';
$lang['pending_product_reviews'] = 'revues de produit en attente';
$lang['pending_send_email'] = 'en attente - envoyer alerte';
$lang['pending_ticket'] = 'requête au support ouverte';
$lang['pending_tickets'] = 'requêtes au support ouvertes';
$lang['per_product'] = 'par produit';
$lang['percent'] = 'pourcent';
$lang['performance_bonus_earned'] = 'bonus de rendement gagné';
$lang['performance_bonuses'] = 'bonus de rendement';
$lang['personal_info'] = 'info personnelle';
$lang['photo_deleted_successfully'] = 'photo supprimée avec succès';
$lang['photo_file'] = 'fichier photo';
$lang['photo_updated_successfully'] = 'photo supprimée avec succès';
$lang['photos'] = 'photos';
$lang['php_version'] = 'version php';
$lang['physical'] = 'physique';
$lang['please_wait'] = 'veuillez patienter...';
$lang['please_wait_downloading_currency_updates'] = 'veuillez patienter pendant que nous téléchargeons le taux des devises';
$lang['please_wait_email_being_sent'] = 'veuilez patienter pendant que nous envoyons tous les emails';
$lang['please_wait_while_backing_up_data'] = 'veuillez patienter pendant que nous sauvegardons votre base de données';
$lang['post_to_ping_fm'] = 'poster à Ping.FM';
$lang['postal_code'] = 'code postal';
$lang['previous_invoice'] = 'facture précédente';
$lang['price'] = 'prix';
$lang['price_must_be_numeric'] = 'le prix doit être numérique';
$lang['primary_email'] = 'email primaire';
$lang['print'] = 'imprimer';
$lang['print_invoice'] = 'imprimer facture';
$lang['print_invoice_balance_due'] = 'solde dû';
$lang['print_invoice_click_here_to_make_payment'] = 'cliquer ici pour effectuer un paiement';
$lang['print_invoice_credit_invoice'] = 'crédit facture';
$lang['print_invoice_has_been_paid'] = 'la facture a été réglée';
$lang['print_invoice_payment_date'] = 'date paiement';
$lang['print_invoice_payment_method'] = 'méthode paiement';
$lang['print_invoice_payment_type'] = 'type paiement';
$lang['print_invoice_payments'] = 'paiements';
$lang['print_invoice_product_name'] = 'nom produit';
$lang['print_invoice_quantity'] = 'quantité';
$lang['print_invoice_quantity_amount'] = 'quantité';
$lang['print_invoice_sku'] = 'SKU';
$lang['print_invoice_total_payments'] = 'total paiements';
$lang['print_invoice_transaction_id'] = ' Numéro transaction';
$lang['print_invoice_unit_price'] = 'prix unitaire';
$lang['print_report'] = 'imprimer rapport';
$lang['priority'] = 'priorité';
$lang['privacy_policy'] = 'politique de confidentialité';
$lang['process_now'] = 'traiter maintenant';
$lang['processing'] = 'en cours de traitement';
$lang['product'] = 'produit';
$lang['product_attributes'] = 'attributs produit';
$lang['product_categories'] = 'catégories produit';
$lang['product_cloned_successfully'] = 'produit cloné avec succès';
$lang['product_commissions'] = 'commissions produit';
$lang['product_description'] = 'description produit';
$lang['product_details'] = 'détails produit';
$lang['product_featured'] = 'produit VIP';
$lang['product_id'] = 'i produit';
$lang['product_info'] = 'info produit';
$lang['product_information'] = 'information produit';
$lang['product_inventory'] = 'stock produit';
$lang['product_keywords'] = 'mots-clés du produit';
$lang['product_meta_description'] = 'meta description du produit';
$lang['product_name'] = 'nom produit';
$lang['product_new'] = 'nouveau produit';
$lang['product_notes'] = 'notes sur produit';
$lang['product_options'] = 'options de produit';
$lang['product_overview'] = 'aperçu produit';
$lang['product_photos'] = 'photos produit';
$lang['product_price'] = 'prix produit';
$lang['product_pricing'] = 'fixation prix produit';
$lang['product_quick_links'] = 'liens sommaires produit';
$lang['product_review'] = 'revue produit';
$lang['product_reviews'] = 'revues produits';
$lang['product_settings'] = 'paramètres produits';
$lang['product_sku'] = 'sku produit';
$lang['product_stats'] = 'stats produit';
$lang['product_status'] = 'statut produit';
$lang['product_title'] = 'titre produit';
$lang['product_tools'] = 'outils produit';
$lang['product_trial_interval'] = 'intervalle d\'essai';
$lang['product_trial_interval_type'] = 'type d\'intervalle d\'essai';
$lang['product_trial_price'] = 'prix à l\'essai';
$lang['product_type'] = 'type produit';
$lang['product_url'] = 'URL produit';
$lang['product_videos'] = 'vidéos produit';
$lang['product_views'] = 'vues sur produit';
$lang['product_weight'] = 'poids produit';
$lang['products'] = 'produits';
$lang['products_html'] = 'produits_html';
$lang['product_images_imported_successfully'] = 'images de produit importées avec succès';
$lang['products_purchased_on_commission'] = 'produits achétés pour cette commission';
$lang['products_text'] = 'produits_texte';
$lang['products_updated_successfully'] = 'produits mis à jour avec succès';
$lang['profile_description'] = 'description profil';
$lang['profile_pages'] = 'pages de profil';
$lang['psigate_payments'] = 'paiements PSIGate';
$lang['publish_date'] = 'date publication';
$lang['publish_wordpress'] = 'publier sur WordPress';
$lang['purchase_link'] = 'lien d\'achat';
$lang['quantity'] = 'quantité';
$lang['quantity_must_be_numeric'] = 'la quantité doit être en nombre entier';
$lang['queue_email'] = 'email en file d\'attente';
$lang['quick_add'] = 'ajout rapide';
$lang['quick_add_products'] = 'ajout rapide des produits';
$lang['quick_links'] = 'liens rapides';
$lang['quick_stats'] = 'stats sommaires';
$lang['ratings'] = 'notations';
$lang['recommended_product'] = 'produit recommandé';
$lang['recurring_interval'] = 'intervalle récurrent';
$lang['recurring_interval_ends'] = 'limites d\'intervalle récurrent';
$lang['recurring_interval_type'] = 'type d\'intervalle récurrent';
$lang['recurring_onetime'] = 'souscription unique';
$lang['redirect_custom_url'] = 'rediriger l\'url défini';
$lang['referral_url'] = 'url référenceur';
$lang['referred_by'] = 'référencé par';
$lang['referring_affiliate'] = 'affilié référenceur';
$lang['referring_username'] = 'affilié référence';
$lang['refund'] = 'rembourser';
$lang['region'] = 'région';
$lang['region_added_successfully'] = 'région ajoutée avec succès';
$lang['region_code'] = 'code région';
$lang['region_deleted_successfully'] = 'région supprimée avec succès';
$lang['region_edited_successfully'] = 'région éditée avec succès';
$lang['region_name'] = 'nom région';
$lang['registration'] = 'enregistrement';
$lang['registration_form'] = 'formulaire d\'enregistrement';
$lang['regular'] = 'regulier';
$lang['remove_checked_from_featured'] = 'enlever ce qui est coché de la page VIP';
$lang['replicated_site'] = 'repliqué_site';
$lang['replied_on'] = 'réponse le';
$lang['report_name'] = 'nom rapport';
$lang['reports'] = 'rapports';
$lang['reports_archive'] = 'archive rapports';
$lang['req'] = 'req';
$lang['required'] = 'obligatoire';
$lang['require_membership'] = 'souscription obligatoire pour accéder';
$lang['reset'] = 'réactualiser';
$lang['reset_form'] = 'réactualiser formulaire';
$lang['reset_password'] = 'réactualiser mot de passe';
$lang['reset_password_sent'] = 'votre mot de passe a été réinitialisé. veuillez vérifier votre adresse email';
$lang['review_comment'] = 'commentaire sur revue';
$lang['review_updated_successfully'] = 'revue mise à jour avec succès';
$lang['reviews_updated_successfully'] = 'revues mises à jour avec succès';
$lang['right'] = 'droite';
$lang['right_block'] = 'bloc droit';
$lang['right_boxes'] = 'cases droites';
$lang['right_column'] = 'colonne droite';
$lang['right_menu'] = 'menu droit';
$lang['right_menu_links'] = 'liens de menu à droite';
$lang['root'] = 'ROOT';
$lang['rotator_group'] = 'ajouter bannière rotative';
$lang['rotator_group'] = 'groupe de bannière rotative';
$lang['rows_imported_successfully'] = 'rangées importées avec succès';
$lang['rows_per_page'] = 'rangées par page';
$lang['run_module'] = 'éxécuter module';
$lang['run_notify_google_sitemap'] = 'ceci va exécuter le générateur sitemap de google.  sélectionner OK pour démarrer, ou supprimer pour fermer';
$lang['run_updates'] = 'éxécuter les mises à jour de la base de données';
$lang['safepay_id'] = 'Safepay Id';
$lang['safepay_id_taken'] = 'safepay ID pris';
$lang['sale_amount'] = 'montant vente';
$lang['sales'] = 'ventes';
$lang['sales_amount'] = 'montant ventes';
$lang['sales_made'] = 'ventes effectuées';
$lang['sales_made_by'] = 'ventes effectuées par';
$lang['sales_quantity'] = 'quantité ventes';
$lang['sales_stats'] = 'stats ventes';
$lang['scheduled'] = 'programmé(e)';
$lang['search'] = 'chercher';
$lang['search_docs'] = 'chercher docs d\'aide';
$lang['search_help_docs'] = 'entrer le mot à chercher';
$lang['search_term_greater_than'] = 'les mots à chercher doivent être supérieurs à 3 caractères';
$lang['second'] = 'seconde';
$lang['seconds'] = 'secondes';
$lang['section'] = 'section';
$lang['security'] = 'securité';
$lang['security_permissions'] = 'autorisation de sécurité';
$lang['security_settings'] = 'paramètres de sécurité';
$lang['select'] = 'sélectioner';
$lang['select_category'] = 'sélectionner une catégorie';
$lang['select_expires_module'] = 'sélectionner le module d\'expiration de compte';
$lang['select_log_file'] = 'sélectionner le fichier de log';
$lang['select_manufacturer'] = 'sélectionner fournisseur/fabricant';
$lang['select_membership_product'] = 'sélectionner souscription de produit';
$lang['select_module'] = 'module post-achat à exécuter';
$lang['select_permissions_for_admin'] = 'restreindre les autorisations de l\'admin';
$lang['select_post_purchase_module'] = 'sélectionner le module post-achat';
$lang['select_regions'] = 'sélectioner les régions fiscales';
$lang['select_rows_per_page'] = 'sélectionner les rangées par page';
$lang['select_signup_module'] = 'sélectionner module d\'enregistrement';
$lang['select_support_category'] = 'sélectionner catégorie de support';
$lang['select_vendor'] = 'sélectionner marchand';
$lang['send_date'] = 'date d\'envoi';
$lang['send_email'] = 'envoyer email';
$lang['send_email_alert'] = 'envoyer alerte email';
$lang['send_email_to'] = 'envoyer email à';
$lang['send_invoice'] = 'envoyer facture';
$lang['send_invoice_font_family'] = 'arial, Helvetica, sans-serif';
$lang['send_invoice_font_size'] = '-1';
$lang['send_mass_email'] = 'envoyer email en masse';
$lang['send_now'] = 'envoyer maintenant';
$lang['send_sms_alerts'] = 'envoyer alertes sms';
$lang['send_to'] = 'envoyer à';
$lang['send_welcome_email'] = 'voulez-vous envoyer un email de bienvenue?';
$lang['seq'] = 'seq';
$lang['sequence'] = 'séquence';
$lang['server_software'] = 'serveur logiciel';
$lang['set_as_default'] = 'mettre par défaut';
$lang['set_status_active_inactive'] = 'mettre en mode actif / inactif';
$lang['settings'] = 'paramètres admin';
$lang['settings'] = 'paramètres';
$lang['seven_day_quick_stats_commissions'] = 'commissions des 7 derniers jours';
$lang['seven_day_quick_stats_sales'] = 'ventes des 7 derniers jours';
$lang['ship_to'] = 'expédier à';
$lang['shipping'] = 'expédition';
$lang['shipping'] = 'expédition';
$lang['shipping_address_1'] = 'expédition_adresse_1';
$lang['shipping_address_2'] = 'expédition_adresse_2';
$lang['shipping_amount'] = 'montant frais expédition';
$lang['shipping_city'] = 'expédition_ville';
$lang['shipping_company'] = 'Société expéditrice';
$lang['shipping_country'] = 'pays d\'expédition';
$lang['shipping_fname'] = 'prénom d\'expédition';
$lang['shipping_info'] = 'info expédition';
$lang['shipping_information'] = 'information expédition';
$lang['shipping_lname'] = 'nom d\'expédition';
$lang['shipping_method'] = 'méthode d\'expédition';
$lang['shipping_module'] = 'module d\'expédition';
$lang['shipping_name'] = 'nom d\'expédition';
$lang['shipping_postal_code'] = 'expédition_postal_code';
$lang['shipping_settings'] = 'paramètres expédition';
$lang['shipping_state'] = 'expédition_état';
$lang['shipping_tracking_id'] = 'iD de suivi d\'expédition';
$lang['shipping_updated_successfully'] = 'expédition mise à jour avec succès';
$lang['show'] = 'montrer';
$lang['show_affiliate'] = 'montrer affilié';
$lang['show_all_affiliate_payment_balances'] = 'inclure tous les utilisateurs avec un solde';
$lang['show_all_commissions'] = 'montrer toutes les commissions';
$lang['show_all_invoices'] = 'montrer toutes les factures';
$lang['show_all_tickets'] = 'montrer toutes les requêtes';
$lang['show_assigned_tickets_only'] = 'montrer uniquement les requêtes assignées';
$lang['show_closed_tickets'] = 'montrer les requêtes closes';
$lang['show_completed_invoices'] = 'montrer les factures liquidées';
$lang['show_min_affiliate_payment_balances'] = 'inclure tous les utilisateurs avec un solde minimum';
$lang['show_on_login'] = 'montrer à la connexion';
$lang['show_open_tickets'] = 'montrer requêtes ouvertes';
$lang['show_paid_commissions'] = 'montrer commissions payées';
$lang['show_pending_commissions'] = 'montrer commissions en cours';
$lang['show_pending_invoices'] = 'montrer factures en attente';
$lang['show_processing_invoices'] = 'montrer factures en cours de traitement';
$lang['show_unpaid_commissions'] = 'montrer commissions impayées';
$lang['showing'] = 'montrant';
$lang['signup_date'] = 'date enregistrement';
$lang['signup_for_an_account'] = 'enregistrement d\'un compte';
$lang['signup_ip'] = 'iP d\'enregistrement';
$lang['signup_link'] = 'lien d\'inscription';
$lang['site'] = 'site';
$lang['site_design'] = 'design site';
$lang['site_layout'] = 'mise en page du site';
$lang['site_layout_updated_successfully'] = 'mise en page du site mis à jour avec succès';
$lang['site_map_notify'] = 'notification de la carte du site';
$lang['site_settings'] = 'paramètres site';
$lang['sku'] = 'SKU';
$lang['sm'] = 'SM';
$lang['sms_contact'] = 'contact sms';
$lang['social_marketing'] = 'marketing social';
$lang['sort'] = 'trier';
$lang['sort_order'] = 'trier commande';
$lang['sort_order_must_be_numeric'] = 'la commande triée doit être un chiffre';
$lang['sponsor'] = 'sponsor';
$lang['standard'] = 'standard';
$lang['standard_pages'] = 'pages standard';
$lang['start'] = 'démarrer';
$lang['state'] = 'état';
$lang['stats_and_reporting'] = 'Stats et Reporting';
$lang['stats_and_reports'] = 'stats et rapports';
$lang['stats_reports'] = 'stats et rapports';
$lang['status'] = 'statut';
$lang['status_updates'] = 'Statut texte de mise à jour';
$lang['store'] = 'boutique';
$lang['store_address'] = 'adresse boutique';
$lang['store_city'] = 'ville boutique';
$lang['store_config'] = 'configuration boutique';
$lang['store_country'] = 'pays boutique';
$lang['store_credit'] = 'crédit boutique';
$lang['store_credit_transaction_id'] = 'appliqué à la boutique_credit';
$lang['store_link'] = 'lien boutique';
$lang['store_name'] = 'nom boutique';
$lang['store_phone'] = 'téléphone boutique';
$lang['store_postal_code'] = 'code postal boutique';
$lang['store_settings'] = 'paramètres boutique';
$lang['store_shipping_address'] = 'boutique_expédition_adresse';
$lang['store_shipping_city'] = 'boutique_expédition_ville';
$lang['store_shipping_country'] = 'boutique_expédition_pays';
$lang['store_shipping_postal_code'] = 'boutique_expédition_postal_code';
$lang['store_shipping_state'] = 'boutique_expédition_état';
$lang['store_state'] = 'Etat de la boutique';
$lang['sts_admin_date_format'] = 'format de date de l\'admin';
$lang['sts_admin_default_language'] = 'langue par défaut admin';
$lang['sts_admin_enable_hostip_lookup'] = 'admin enable hostip lookup';
$lang['sts_admin_enable_wysiwyg_content'] = 'autoriser l\'admin à la gestion du contenu par wysiwyg';
$lang['sts_admin_image_height'] = 'hauteur image admin';

$lang['sts_admin_image_resize'] = 'redimensionner images de l\'admin';
$lang['sts_admin_image_width'] = 'largeur image de l\'admin';
$lang['sts_admin_time_format'] = 'format horaire pour l\'admin';
$lang['sts_affiliate_admin_approval_required'] = 'exige l\'approbation de l\'admin pour les affiliations';
$lang['sts_affiliate_alert_downline_signup'] = 'alerter sur l\'inscription du filleul';
$lang['sts_affiliate_alert_new_commission'] = 'alerter en cas de nouvelle commission';
$lang['sts_affiliate_alert_payment_sent'] = 'alerter en cas d\'envoi de paiement';
$lang['sts_affiliate_allow_downline_email'] = 'autoriser l\'affilié à envoyer l\'email au filleul';
$lang['sts_affiliate_allow_downline_view'] = 'autoriser la visualisation des filleuls';
$lang['sts_affiliate_allow_expandable_downlines'] = 'autoriser une visualisation étendue des filleuls';
$lang['sts_affiliate_allow_upload_photos'] = 'autoriser l\'affilié à attacher les photos';
$lang['sts_affiliate_auto_approve_commissions'] = 'auto- approbation des commissions';
$lang['sts_affiliate_auto_generate_coupon'] = 'auto-génération des coupons d\'affiliation';
$lang['sts_affiliate_auto_generate_coupon_amount'] = 'montant des coupons auto-générés';
$lang['sts_affiliate_auto_generate_coupon_credits'] = 'utiliser les crédits de membre pour générer les coupons';
$lang['sts_affiliate_auto_generate_coupon_recurring'] = 'coupon récurrent';
$lang['sts_affiliate_auto_generate_coupon_type'] = 'type de coupons auto-générés';
$lang['sts_affiliate_banner_directory'] = 'répertoire de bannières d\'affiliation';
$lang['sts_affiliate_categories_per_page'] = 'catégories d\'outils par page';
$lang['sts_affiliate_commission_levels'] = 'niveaux de commission d\'affiliation';
$lang['sts_affiliate_commission_levels_restrict_view'] = 'restreindre les niveaux de visualisation des filleuls';
$lang['sts_affiliate_commission_type'] = 'type de commission d\'affiliation';
$lang['sts_affiliate_cookie_timer'] = 'minuteur  de tracking d\'affiliés en jours';
$lang['sts_affiliate_copy_path'] = 'classeur source de répliquage';
$lang['sts_affiliate_coupon_expires'] = 'intervalle d\'expiration des coupons d\'affiliés';
$lang['sts_affiliate_coupon_limit_use'] = 'limiter l\'usage du coupon d\'affiliation';
$lang['sts_affiliate_custom_payment_id'] = 'Numéro de paiement défini de l\'affilié';
$lang['sts_affiliate_delete_commission_on_checkout'] = 'supprimer le cookie d\'affiliation après le paiement';
$lang['sts_affiliate_enable_affiliate_marketing'] = 'autoriser le marketing d\'affiliation';
$lang['sts_affiliate_enable_articles'] = 'autoriser les articles d\'affiliation';
$lang['sts_affiliate_enable_auto_affiliates'] = 'les clients deviennent automatiquemet des affiliés';
$lang['sts_affiliate_enable_banners'] = 'autoriser les bannières';
$lang['sts_affiliate_enable_direct_product_code'] = 'autoriser le code direct de produit';
$lang['sts_affiliate_enable_email_ads'] = 'autoriser les emails publicitaires';
$lang['sts_affiliate_enable_forced_matrix'] = 'autoriser la matrice d\'affilition à parachutage forcé';
$lang['sts_affiliate_enable_hostip_lookup'] = 'autoriser le hostip lookup d\'affiliation';
$lang['sts_affiliate_enable_hover_ads'] = 'autoriser les annonces flottantes';
$lang['sts_affiliate_enable_javascript_code'] = 'autoriser le code javascript';
$lang['sts_affiliate_enable_landing_pages'] = 'autoriser les pages d\'aterrissage';
$lang['sts_affiliate_enable_performance_bonus'] = 'autoriser les bonus de rendement';
$lang['sts_affiliate_enable_profile_description'] = 'autoriser la description du profil de l\'affilié';
$lang['sts_affiliate_enable_profiles'] = 'autoriser les profils f\'affilés';
$lang['sts_affiliate_enable_recommended_profile'] = 'produits recommandés sur les pages de profil';
$lang['sts_affiliate_enable_recurring_commission'] = 'autoriser les commissions récurrentes';
$lang['sts_affiliate_enable_referral_signup_bonus'] = 'autoriser le bonus de parrainnage';
$lang['sts_affiliate_enable_referral_signup_bonus_amount'] = 'montant bonus de parrainnage';
$lang['sts_affiliate_enable_replication'] = 'autoriser le répliquage';
$lang['sts_affiliate_enable_signup_bonus'] = 'autoriser le bonus de parrainnage';
$lang['sts_affiliate_enable_signup_bonus_amount'] = 'montant bonus de parrainnage';
$lang['sts_affiliate_enable_text_ads'] = 'autoriser les articles publictaires';
$lang['sts_affiliate_enable_text_links'] = 'autoriser les liens textuels';
$lang['sts_affiliate_enable_trackers'] = 'autoriser les outils de traçage de publicité';
$lang['sts_affiliate_enable_videos'] = 'autoriser vidéos';
$lang['sts_affiliate_enable_viral_pdfs'] = 'autoriser PDFs viraux';
$lang['sts_affiliate_enable_wysiwyg_content'] = 'autoriser éditeur HTML pour le contenu';
$lang['sts_affiliate_enable_wysiwyg_email'] = 'autoriser éditeur HTML pour envoi d\'email';
$lang['sts_affiliate_image_auto_resize'] = 'redimensionner les images de membre';
$lang['sts_affiliate_image_height'] = 'hauteur image membre';
$lang['sts_affiliate_image_width'] = 'largeur image membre';
$lang['sts_affiliate_intro_text'] = 'texte d\'introduction de l\'affilié';
$lang['sts_affiliate_lifetime_sponsor'] = 'payer les commissions à vie';
$lang['sts_affiliate_link_type'] = 'type de lien d\'affiliation';
$lang['sts_affiliate_matrix_width'] = 'largeur de la matrice à parachutage forcé';
$lang['sts_affiliate_min_payment'] = 'minimum de paiement de l\'affilié';
$lang['sts_affiliate_new_commission'] = 'fixer de nouvelles commissions pour';
$lang['sts_affiliate_performance_bonus_amount'] = 'montant bonus de rendement';
$lang['sts_affiliate_performance_bonus_required'] = 'bonus de rendement obligatoire';
$lang['sts_affiliate_performance_bonus_required_amount'] = 'critère du montant du rendement';
$lang['sts_affiliate_performance_bonus_type'] = 'type de bonus de rendement';
$lang['sts_affiliate_program_type'] = 'type de programme d\'affiliation';
$lang['sts_affiliate_refer_friend_fields'] = 'champs de Dites-à-un ami';
$lang['sts_affiliate_refer_friend_footer'] = 'pied de page du Dites-à-un ami';
$lang['sts_affiliate_refer_friend_header'] = 'en-tête du Dites-à-un ami';
$lang['sts_affiliate_refund_commission_type'] = 'statut de la commission sur remboursement';
$lang['sts_affiliate_replication_enable_header_footer'] = 'autoriser en-tête / pied de page sur les pages répliquées';
$lang['sts_affiliate_require_referral_code'] = 'code d\'affiliation obligatoire à la caisse';
$lang['sts_affiliate_restrict_self_commission'] = 'refuser les commissions à soi-même';
$lang['sts_affiliate_restrict_subdomains'] = 'refuser les sous-domaines';
$lang['sts_affiliate_save_path'] = 'repliquage sauvegardé dans le classeur';
$lang['sts_affiliate_show_downline_details'] = 'montrer les détails des filleuls';
$lang['sts_affiliate_show_downline_emails'] = 'montrer les emails des filleuls';
$lang['sts_affiliate_show_downline_photos'] = 'montrer les photos des filleuls';
$lang['sts_affiliate_show_javascript_code'] = 'autoriser le code d\'affiliation javascript';
$lang['sts_affiliate_show_name'] = 'montrer l\'identifiant de l\'affilié sur les pages';
$lang['sts_affiliate_show_pending_comms_members'] = 'montrer les commissions en attente dans l\'espace membre';
$lang['sts_affiliate_tools_per_page'] = 'outils marketing par page';
$lang['sts_affiliate_total_sale_commissions'] = 'payer commission sur le montant total de la vente';
$lang['sts_backup_enable'] = 'autoriser la sauvegarde de la base de données';
$lang['sts_backup_path'] = 'chemin d\'accès au répertoire pour sauvegardes';
$lang['sts_backup_schedule_daily'] = 'selectionner une programmation de sauvegarde';
$lang['sts_backup_schedule_interval'] = 'intervalle de programmation de sauvegarde';
$lang['sts_backup_schedule_time'] = 'temps de programmation de la sauvegarde';
$lang['sts_cart_add_account_group'] = 'ajouter client à un groupe d\'affiliation';
$lang['sts_cart_add_account_mailing_list'] = 'ajouter client à une liste de diffusion';
$lang['sts_cart_apply_credits_checkout'] = 'applique le crédit de membre à la caisse';
$lang['sts_cart_number_recommended_products'] = 'nombre de produits recommandés à montrer';
$lang['sts_cart_number_upsell_products'] = 'nombre de produits en vente additionnelle à montrer';
$lang['sts_cart_recommend_products'] = 'autoriser les produits recommandés avant d\'aller à la caisse';
$lang['sts_cart_recommended_products_keywords'] = 'montrer les produits recommandés sur la base des mots-clés';
$lang['sts_cart_remove_account_mailing_list'] = 'retirer le client de la liste de diffusion';
$lang['sts_cart_show_upsell_page'] = 'autoriser la page de ventes additionnelles avant le paiement à la caisse';
$lang['sts_cart_ssl_on_checkout'] = 'uiliser le SSL sur la caisse';
$lang['sts_cart_ssl_on_checkout_url'] = 'URL à utiliser pour le SSL de la caisse';
$lang['sts_cart_upsell_products_keywords'] = 'montrer les produits de ventes additionnelles sur la base des mots-clés';
$lang['sts_content_articles_home_page'] = 'articles de contenu sur la page d\'accueil';
$lang['sts_content_articles_per_page'] = 'contenu d\'articles par page';
$lang['sts_content_auto_generate_xml_sitemap'] = 'auto-générer le XML sitemap';
$lang['sts_content_categories_per_page'] = 'catégories de contenu par page';
$lang['sts_content_document_charset'] = 'document du jeu de caractères charset';
$lang['sts_content_enable_comments'] = 'autoriser commentaires sur contenu';
$lang['sts_content_enable_disqus_form'] = 'Utiliser le système de commentaire Disqus';
$lang['sts_content_enable_facebook_share'] = 'autoriser le partage avec facebook';
$lang['sts_content_enable_ping_fm'] = 'autoriser les mises à jour Ping.FM';
$lang['sts_content_enable_related_articles'] = 'autoriser les articles relatifs au contenu';
$lang['sts_content_enable_tweetmeme_code'] = 'autoriser les TweetMeme ReTweets';
$lang['sts_content_enable_wordpress_publishing'] = 'autoriser la publication à distance sur WordPress';
$lang['sts_content_html_editor'] = 'édieur de contenu HTMl';
$lang['sts_content_members_dashboard_enable'] = 'autoriser les icônes de tableau de bord du membre';
$lang['sts_content_members_dashboard_enable_account_details'] = 'autoriser l\'icône de détails sur compte au tableau de bord';
$lang['sts_content_members_dashboard_enable_commissions'] = 'autoriser l\'icône des commissions au tableau de bord';
$lang['sts_content_members_dashboard_enable_content'] = 'autoriser l\'icône de contenu sur la tableau de bord';
$lang['sts_content_members_dashboard_enable_coupons'] = 'autoriser l\'icône des coupons sur le tableau de bord';
$lang['sts_content_members_dashboard_enable_downline'] = 'autoriser l\'icône de filiation sur tableau de bord';
$lang['sts_content_members_dashboard_enable_downloads'] = 'autoriser l\'icône de téléchargements sur tableau de bord';
$lang['sts_content_members_dashboard_enable_invoices'] = 'autoriser l\'icône des factures sur tableau de bord';
$lang['sts_content_members_dashboard_enable_memberships'] = 'autoriser l\'icône des souscriptions d\'adhésions sur tableau de bord';
$lang['sts_content_members_dashboard_enable_reports'] = 'autoriser l\'icône des rapports sur tableau de bord';
$lang['sts_content_members_dashboard_enable_support'] = 'autoriser l\'icône de support sur tableau de bord';
$lang['sts_content_members_dashboard_enable_tools'] = 'autoriser l\'icône des outils sur tableau de bord';
$lang['sts_content_members_dashboard_enable_traffic'] = 'autoriser l\'icône de traffic sur tableau de bord';
$lang['sts_content_members_dashboard_num_articles'] = 'nombre d\'articles du tableau de bord';
$lang['sts_content_ping_fm_app_key'] = 'cle ce l\'application Ping.FM';
$lang['sts_content_require_comment_moderation'] = 'modérer les commentaires';
$lang['sts_content_rss_per_page'] = 'articles RSS par page';
$lang['sts_content_translate_menus'] = 'autoriser le menu de traduction linguistique';
$lang['sts_content_update_services'] = 'services de mise à jour du contenu';
$lang['sts_content_wordpress_password'] = 'Mot de passe du Blog WordPress';
$lang['sts_content_wordpress_url'] = 'XML-RPC URL du Blog Wordpress';
$lang['sts_content_wordpress_username'] = 'identifiant du BLOG WordPresS';
$lang['sts_email_auto_prune_archive'] = 'auto prune les mails archivés en jours';
$lang['sts_email_bounce_address'] = 'adresse pour les mails refusés';
$lang['sts_email_bounce_password'] = 'mot de passe POP3 pour email refusé';
$lang['sts_email_bounce_port'] = 'port POP3 pour email refusé';
$lang['sts_email_bounce_server'] = 'serveur POP3 pour emails refusés';
$lang['sts_email_bounce_service_flags'] = 'service optionnel de marquage des emails refusés';
$lang['sts_email_bounce_username'] = 'identifiant POP3 popur emails refusés';
$lang['sts_email_charset'] = 'jeu de caractères pour email';
$lang['sts_email_enable_archive'] = 'archiver tous les emails sortants';
$lang['sts_email_enable_debugging'] = 'autoriser déboggeur d\'emails';
$lang['sts_email_enable_ssl'] = 'Utiliser SSl';
$lang['sts_email_limit_mass_mailing'] = 'limiter l\'envoi en masse d\'emails à l\'envoi de ce qui est en file d\'attente';
$lang['sts_email_mailer_type'] = 'emailer type';
$lang['sts_email_require_confirmation_on_signup'] = 'confirmation par email obligatoire à l\'enregistrement';
$lang['sts_email_send_queue'] = 'envoyer immédiatemment les emails en file d\'attente';
$lang['sts_email_show_email_content_queue'] = 'montrer le contenu de l\'email dans les archives et en file d\'attente';
$lang['sts_email_smtp_host'] = 'serveur smtp';
$lang['sts_email_smtp_password'] = 'mot de passe smtp';
$lang['sts_email_smtp_port'] = 'port smtp';
$lang['sts_email_smtp_timeout'] = 'dépassement de délai smtp';
$lang['sts_email_smtp_username'] = 'identifiant smtp';
$lang['sts_email_use_smtp_authentication'] = 'utiliser l\'authentification smtp';
$lang['sts_faq_articles_per_page'] = 'articles FAQ par page';
$lang['sts_form_enable_custom_fields_checkout'] = 'autoriser les champs définis sur le formulaire de caisse';
$lang['sts_form_enable_custom_fields_contact_us'] = 'autoriser les champs définis sur le formulaire de contact';
$lang['sts_form_enable_custom_fields_membership'] = 'autoriser les champs définis sur le formulaire de soucription d\'adhésion';
$lang['sts_form_enable_custom_fields_registration'] = 'autoriser les champs définis sur le formulaire denregistrement';
$lang['sts_form_enable_tos_checkbox'] = 'rendre obligatoires les conditions  de services sur le formulaire d\'enregistrement';
$lang['sts_form_membership_product_id'] = 'mettre le ID du produit d\'adhésion exigé';
$lang['sts_form_require_membership'] = 'Souscription d\'adhésion obligatoire à l\'enregistrement';
$lang['sts_image_enable_flash_rotator'] = 'autoriser le rotateur d\'images flash pour lesimages de produits';
$lang['sts_image_enable_thickbox'] = 'permettre de cocher/décocher les cases';
$lang['sts_image_flash_rotator_height'] = 'hauteur rotateur d\'images flash';
$lang['sts_image_flash_rotator_width'] = 'largeur rotateur d\'images flash';
$lang['sts_image_library'] = 'bibliothèque d\'images';
$lang['sts_image_maintain_ratio'] = 'maintenir le ratio d\'image';
$lang['sts_image_max_photo_size'] = 'taille maximum de photos à attacher';
$lang['sts_invoice_autload_print_window'] = 'auto- charger la fenêtre d\'impression de la facture';
$lang['sts_invoice_auto_generate_invoice_day'] = 'jours avant la date de réclamation pour générer la facture';
$lang['sts_invoice_due_date_days'] = 'date de réclamation (en jours)';
$lang['sts_invoice_enable_auto_generate_invoice'] = 'autoriser l\'auto-génération des factures';
$lang['sts_invoice_enable_logo'] = 'montrer le logo sur le formulaire de facturation';
$lang['sts_invoice_new_order_status_digital'] = 'nouveau statut de la commande des produits numériques/téléchargeables';
$lang['sts_invoice_new_order_status_membership'] = 'nouveau statut de la commande des souscriptions d\'adhésion';
$lang['sts_invoice_new_order_status_physical'] = 'nouveau statut de la commande des produits physiques';
$lang['sts_products_alert_inventory'] = 'autoriser les alertes sur stock globlal des produits';
$lang['sts_products_alert_inventory_level'] = 'alerte en cas de stock bas';
$lang['sts_products_bookmarks_code'] = 'code social bookmarks';
$lang['sts_products_categories_image_auto_resize'] = 'redimensionner les images des catégories de produits';
$lang['sts_products_categories_image_height'] = 'hauteur image catégorie produit';
$lang['sts_products_categories_image_width'] = 'largeur image catégorie produit';
$lang['sts_products_description_1_name'] = 'description produit tab 1';
$lang['sts_products_description_2_name'] = 'description produit tab 2';
$lang['sts_products_enable_bookmarks'] = 'autoriser le bookmarking social par produit';
$lang['sts_products_enable_global discounts'] = 'autoriser les remises globales';
$lang['sts_products_enable_inventory'] = 'autoriser le système d\'inventaire du produit';
$lang['sts_products_enable_reviews'] = 'auoriser les membres à poster les revues sur les produits';
$lang['sts_products_images_per_product'] = 'images par produit';
$lang['sts_products_listing_style'] = 'type de listage des produits';
$lang['sts_products_manufacturer_category_name'] = 'nom de la catégorie des fournisseurs/fabricants';
$lang['sts_products_manufacturer_image_auto_resize'] = 'redimensionner la taille des images du fournisseur';
$lang['sts_products_manufacturer_image_height'] = 'hauteur image du fournisseur';
$lang['sts_products_manufacturer_image_width'] = 'largeur image fournisseur';
$lang['sts_products_number_similar_products_details_page'] = 'nombre de produits similaires à montrer';
$lang['sts_products_per_home_page'] = 'produits sur la page d\'accueil';
$lang['sts_products_per_page'] = 'produits par page';
$lang['sts_products_random_home_page'] = 'montrer de façon alétoire les images VIP sur la page d\'accueil';
$lang['sts_products_show_similar_products_details_page'] = 'montrer les produits similaires sur la page de détails';
$lang['sts_sec_admin_failed_login_email'] = 'adresse email pour alertes';
$lang['sts_sec_admin_restrict_ip'] = 'admin restreint aux IPs';
$lang['sts_sec_auto_ip_block_interval'] = 'intervalle de blocage automatique de l\'ip';
$lang['sts_sec_enable_admin_restrict_ip'] = 'autoriser la restriction d\'accès par IP à l\'espace administrateur';
$lang['sts_sec_enable_auto_ip_block'] = 'autoriser l\'auto-blocage de l\'ip sur les erreurs de connexion';
$lang['sts_sec_enable_captcha'] = 'autoriser le CAPTCHA sur le commentaire et les revues';
$lang['sts_sec_enable_captcha_order_form'] = 'autoriser le  CAPTCHA au formulaire de caisse';
$lang['sts_sec_enable_max_mind'] = 'autoriser le système de prévention de la fraude max mind';
$lang['sts_sec_max_mind_key'] = 'cle de la licence max mind';
$lang['sts_sec_site_block_free_email_accounts'] = 'bloquer les domaines d\' email';
$lang['sts_sec_site_enable_block_free_email_accounts'] = 'bloquer l\'enregistrement des adresses email gratuites';
$lang['sts_sec_site_enable_form_flood_control'] = 'autoriser la prévention de saturation du formulaire';
$lang['sts_sec_site_form_flood_control_interval'] = 'intervalle de prévention de la saturation';
$lang['sts_sec_site_restrict_ips'] = 'bloquer adresse IP';
$lang['sts_shipping_send_vendor_email_alert'] = 'envoyer alerte au marchand à l\'achat de produit';
$lang['sts_shipping_use_billing_taxes'] = 'utitiser le code postal de facturationcomme pour les taxes';
$lang['sts_shipping_use_vendor_zip'] = 'utiliser le code zip du vendeur pour la calcul des frais d\'expédition';
$lang['sts_store_affiliate_id'] = 'iD de parrainnage';
$lang['sts_store_cache_minutes'] = 'cache minutes';
$lang['sts_store_categories_per_page'] = 'catégories de produit par page';
$lang['sts_store_categories_per_page'] = 'catégories de produit par page';
$lang['sts_store_categories_show_image'] = 'montrer les images par catégorie';
$lang['sts_store_default_country'] = 'pays par défaut';
$lang['sts_store_default_currency'] = 'devise par défaut';
$lang['sts_store_default_keywords'] = 'mots-clés de la boutique par défaut';
$lang['sts_store_default_language'] = 'langue par défaut';
$lang['sts_store_default_timezone'] = 'fixer le fuseau horaire par défaut';
$lang['sts_store_default_weight'] = 'unité de mesure du poids';
$lang['sts_store_description'] = 'description boutique';
$lang['sts_store_disable_cart_maintenance'] = 'désactiver la caisse pour maintenance';
$lang['sts_store_domain_name'] = 'nom du domaine auquel est attaché la licence';
$lang['sts_store_email'] = 'email de la boutique';
$lang['sts_store_enable_cache'] = 'autoriser la cache du site';
$lang['sts_store_enable_downline_cache'] = 'autoriser la cache du filleul';
$lang['sts_store_enable_downline_cache_minutes'] = 'cache du filleul en minutes';
$lang['sts_store_enable_language_selector'] = 'autoriser sélectionneur de langue';
$lang['sts_store_google_analytics_code'] = 'mettre le code google analytics';
$lang['sts_store_image_auto_resize'] = 'redimensionner les images de produit';
$lang['sts_store_image_height'] = 'hauteur image produit';
$lang['sts_store_image_width'] = 'largeur image produit';
$lang['sts_store_key'] = 'clé licence';
$lang['sts_store_name'] = 'nom boutique';
$lang['sts_store_order_id_prefix'] = 'préfixe facture boutique';
$lang['sts_store_phone_number'] = 'numéro téléphone boutique';
$lang['sts_store_refer_friend_code'] = 'code Envoyez-à-un_ami';
$lang['sts_store_refer_friend_enable'] = 'autoriser le système Envoyez-à-un_ami';
$lang['sts_store_set_curl_timeout'] = 'fixer la valeur de dépassement de délai de connexion curl';
$lang['sts_store_shipping_address'] = 'adresse boutique';
$lang['sts_store_shipping_city'] = 'ville boutique';
$lang['sts_store_shipping_country'] = 'pays boutique';
$lang['sts_store_shipping_state'] = 'Etat/Région  boutique';
$lang['sts_store_shipping_zip'] = 'code zip ou postal boutique';
$lang['sts_store_thank_you_page_code'] = 'code de page de remerciements optionnel';
$lang['sts_store_time_format'] = 'format d\'heure boutiques';
$lang['sts_store_upload_photo_types'] = 'type de fichiers à attacher';
$lang['sts_store_use_daylight_savings_time'] = 'autoriser l\'économi du temps du jour';
$lang['sts_support_auto_close'] = 'auto-clore les requêtes';
$lang['sts_support_auto_close_interval'] = 'intervalle d\'auto-fermeture des requêtes';
$lang['sts_support_enable'] = 'autoriser le système d\'assistance et de support';
$lang['sts_support_enable_email_on_assigned_ticket'] = 'envoyer email à l\'admin à qui la requête est assignée';
$lang['sts_support_enable_file_uploads'] = 'autoriser que les fichiers puissent être attachés';
$lang['sts_support_max_upload_size'] = 'taille maximum des documents à attacher';
$lang['sts_support_tickets_per_page'] = 'requêtes au support par page support';
$lang['sts_support_upload_download_types'] = 'types de fichiers autorisés';
$lang['sts_support_upload_folder_path'] = 'accepte le chemin d\'accès aux docs attachés';
$lang['sts_support_url_redirect'] = 'accepte la redirecton d\'URl';
$lang['sts_tracking_auto_prune_days'] = 'prune ad tracking referrals in days';
$lang['sts_video_player_allow_fullscreen'] = 'autoriser la vidéo en plein écran';
$lang['sts_video_player_autostart'] = 'démarrer automatiquement les vidéos flash locales';
$lang['sts_video_player_back_color'] = 'couleur de fond de lecteur vidéo';
$lang['sts_video_player_control_bar'] = 'bar de contrôle vidéo';
$lang['sts_video_player_default_link'] = 'URL de lecteur vidéo par défaut';
$lang['sts_video_player_display_click'] = 'type de clic sur la vidéo';
$lang['sts_video_player_front_color'] = 'couleur lecteur vidéo';
$lang['sts_video_player_height'] = 'hauteur lecteur video flash';
$lang['sts_video_player_logo'] = 'logo vidéo';
$lang['sts_video_player_screen_color'] = 'couleur écran lecteur video';
$lang['sts_video_player_width'] = 'largeur lecteur video flash';
$lang['sub'] = 'sous';
$lang['sub_total'] = 'sous-total';
$lang['sub_total_amount'] = 'montant sous-total';
$lang['subdomain'] = 'sous-domaine';
$lang['subject'] = 'objet';
$lang['submit'] = 'soumettre';
$lang['subscription_id'] = 'iD souscription';
$lang['subtotal_amount'] = 'montant sous-total';
$lang['success_message'] = 'succès';
$lang['support'] = 'support';
$lang['support_categories'] = 'catégories de support';
$lang['support_desk'] = 'centre d\'assistance et de support';
$lang['support_settings'] = 'paramétrage support';
$lang['support_templates'] = 'template de support';
$lang['support_ticket_updated_successfully'] = 'requête au support mise à jour avec succès';
$lang['support_tickets'] = 'requêtes au support';
$lang['support_tickets_updated_successfully'] = 'requêtes au support mises à jour avec succès';
$lang['symbol_left'] = 'symbole à gauche';
$lang['symbol_right'] = 'symbole à droite';
$lang['system'] = 'système';
$lang['system_settings'] = 'Paramétrages du système';
$lang['tags'] = 'tags';
$lang['tax_amount'] = 'montant taxe';
$lang['tax_zone'] = 'zone fisclae';
$lang['tax_zone_amount'] = 'montant zone fiscale';
$lang['tax_zone_name'] = 'nom zone fiscale';
$lang['tax_zones'] = 'zones fiscales';
$lang['taxes_zones'] = 'taxes et zones';
$lang['template_name'] = 'nom template';
$lang['terms_of_service'] = 'termes d\'utilisation du service';
$lang['text'] = 'texte';
$lang['text_ad_body'] = 'corps de l\'annonce textuelle';
$lang['text_ad_details'] = 'détails de l\'annonce textuelle';
$lang['text_ad_name'] = 'nom annonce textuelle';
$lang['text_ad_options'] = 'options d\'annonces textuelles';
$lang['text_ad_title'] = 'titre annonce textuelle';
$lang['text_ad_width'] = 'largeur annonce textuelle';
$lang['text_ads'] = 'annonces textuelles';
$lang['text_body'] = 'corps de l\'annonce';
$lang['text_content'] = 'texte_contenu';
$lang['text_email'] = 'texte de l\'email';
$lang['text_email_format'] = 'format du texte de l\'email';
$lang['text_follow_up'] = 'texte de suivi';
$lang['text_link_details'] = 'détails liens textuels';
$lang['text_link_name'] = 'nom lien textuel';
$lang['text_link_options'] = 'options de lien textuel';
$lang['text_link_title'] = 'titre lien textuel';
$lang['text_links'] = 'liens textuels';
$lang['text_message'] = 'message texte';
$lang['text_template'] = 'template de texte';
$lang['textarea'] = 'espace de texte';
$lang['theme'] = 'thème';
$lang['theme_description'] = 'description du thème';
$lang['theme_file_name_in_use'] = 'nom du fichier thème en cours d\'utilisation';
$lang['theme_image_preview'] = 'fichier de prévisualisation d\'image du thème';
$lang['theme_name'] = 'nom du thème';
$lang['thousands_point'] = 'milliers de points';
$lang['ticket_assigned_successfully'] = 'requêtes assignées avec succès';
$lang['ticket_body'] = 'corps de la requête';
$lang['ticket_id'] = 'iD requête';
$lang['ticket_logged'] = 'requête consultée';
$lang['ticket_message'] = 'message de la requête';
$lang['ticket_priority'] = 'priorité requête';
$lang['ticket_subject'] = 'objet';
$lang['tickets'] = 'requêtes';
$lang['tier'] = 'nieaux';
$lang['timer_expired'] = 'votre session d\'admin a expiré';
$lang['title'] = 'titre';
$lang['to'] = 'à';
$lang['to_delete_remove_code'] = 'pour supprimer ,retirez tout simplement le code de l\'espace texte puis cliquez pour soumettre';
$lang['to_do_list'] = 'tâches à faire';
$lang['tool_type'] = 'type d\'outils';
$lang['top'] = 'top';
$lang['top_block'] = 'top blocage';
$lang['top_menu'] = 'top menu';
$lang['top_menu_links'] = 'top liens de menu';
$lang['top_product_views'] = 'top vues';
$lang['total'] = 'total';
$lang['total_amount'] = ' montant total';
$lang['total_commissions'] = 'total commissions';
$lang['total_credits'] = 'total credits';
$lang['total_levels'] = 'total niveau(x)';
$lang['total_pages'] = 'total page(s)';
$lang['total_payments'] = 'total paiements';
$lang['total_sale'] = 'total vente';
$lang['total_users'] = ' total utilisateur(s)';
$lang['totals'] = 'totaux';
$lang['tracking'] = 'tracking';
$lang['tracking_clicks'] = 'clics';
$lang['tracking_get_code'] = 'obtenir code de suivi';
$lang['tracking_id'] = 'iD de suivi d\'annonce';
$lang['tracking_name'] = 'nom de suivi';
$lang['tracking_url'] = 'URL de suivi';
$lang['traffic'] = 'traffic';
$lang['traffic_reports'] = 'traffic et rapports';
$lang['trans_id_performance_bonus'] = 'bonus de rendement\' . \' - \' . date(\'M d Y\')';
$lang['transaction_id'] = 'id transaction';
$lang['transactions'] = 'transactions';
$lang['txt_file_only'] = '.fichiers txt uniquement';
$lang['type'] = 'type';
$lang['uninstall_module'] = 'désinstaller le module';
$lang['unit_price'] = 'prix unitaire';
$lang['unknown'] = 'inconnu';
$lang['unpaid'] = 'impayé(e)';
$lang['unpaid_no_email'] = 'impayé(e)- pas d\'alerte';
$lang['unpaid_send_email'] = 'impayé(e) - envoyer alerte';
$lang['unsubscribe_here_now'] = 'cliquez ici pour vous désinscrire de la liste de diffusion'; 
$lang['unsubscribe_html_1'] = '<br /><br /><p align="center">';
$lang['unsubscribe_html_2'] = '</p>';
$lang['unsubscribe_link'] = 'lien de désinscription';
$lang['unsubscribe_text_1'] = "\n\n\n" . 'cliquez sur le lien ci-dessous pour vous désinscrire de la liste de diffusion' . "\n\n";
$lang['unsubscribe_text_2'] = "\n\n";
$lang['update'] = 'update';
$lang['update_account_info'] = 'mise à jour info compte';
$lang['update_admin'] = 'mise à jour admin';
$lang['update_admin_success'] = 'admin mis à jour avec succès';
$lang['update_administrator'] = 'mettre à jour l\'administrateur';
$lang['update_administrators'] = 'mettre à jour les administrateurs';
$lang['update_aff_ad_categories_success'] = 'catégorie de l\'annonce de l\'affilié mise à jour avec succès';
$lang['update_affiliate_group'] = 'mettre à jour le groupe d\'affiliation';
$lang['update_affiliate_group'] = 'mettre à jour le groupe d\'affiliation';
$lang['update_affiliate_payment'] = 'mettre à jour le paiement de l\'affilié';
$lang['update_affiliate_tools_success'] = 'outils d\'affiliation mis à jour avec succès';
$lang['update_article_ad'] = 'mettre à jour l\'article publicitaire';
$lang['update_article_ad_success'] = 'article publicitaire mis à jour avec succès';
$lang['update_attribute'] = 'mettre à jour attribut';
$lang['update_banner'] = 'mettre à jour la bannière';
$lang['update_banner_success'] = 'bannière mise à jour avec succès';
$lang['update_category'] = 'mettre à jour la catégorie';
$lang['update_category_success'] = 'categorie mise à jour avec succès';
$lang['update_comment'] = 'mettre à jour commentaires';
$lang['update_commission'] = 'mettre à jour commission';
$lang['update_content'] = 'mettre à jour contenu';
$lang['update_content_article'] = 'mettre à jour article de contenu';
$lang['update_content_category'] = 'mettre à jour la catégorie du contenu';
$lang['update_countries'] = 'mettre à jour pays';
$lang['update_country'] = 'mettre à jour pays';
$lang['update_country_success'] = 'pays mis à jour avec succès';
$lang['update_coupon'] = 'supprimer la atégorie de contenu';
$lang['update_coupon_success'] = 'coupon mis à jour avec succès';
$lang['update_credit'] = 'mettre à jour crédit';
$lang['update_currencies'] = 'mettre à jour devises';
$lang['update_currency'] = 'mettre à jour devise';
$lang['update_currency_success'] = 'devise mise à jour avec succès';
$lang['update_discount_group'] = 'mettre à jour le groupe au rabais';
$lang['update_download'] = 'mettre à jour du téléchargement';
$lang['update_email_ad'] = 'mettre à jour l\'email de publicité';
$lang['update_email_ad_success'] = 'email publicitaire mis àjour avec succès';
$lang['update_email_template'] = 'mettre à jour template d\'email';
$lang['update_faq_article'] = 'supprimer la catégorie de faq';
$lang['update_faq_category'] = 'mettre à jour la catégorie de FAQ';
$lang['update_file_ran_successfully'] = 'mise à jour du fichier réussie.';
$lang['update_follow_up'] = 'mettre à jour le suivi';
$lang['update_form_fields_success'] = 'champs du formulaire mis à jour avec succès';
$lang['update_group_success'] = 'groupe mis à jour avec succès';
$lang['update_html_ad'] = 'mettre à jour annonce html';
$lang['update_html_ad_success'] = 'annonce html mise à jour avec succès';
$lang['update_invoice'] = 'mettre à jour la facture';
$lang['update_invoice_payment'] = 'mettre à jour le règlement de la facture';
$lang['update_language_success'] = 'fichier de langue mis à jour avec succès';
$lang['update_mailing_list'] = 'mettre à jour liste de diffusion';
$lang['update_mailing_list_success'] = 'liste de diffusion mise à jour avec succès';
$lang['update_manufacturer'] = 'mettre à jour fournisseur/fabricant';
$lang['update_manufacturer_success'] = 'fournisseur/fabricant mis à jour avec succès';
$lang['update_member'] = 'mettre à jour membre';
$lang['update_member_success'] = 'membre mis jour avec succès';
$lang['update_members'] = 'mettre à jour membres';
$lang['update_membership'] = 'mettre à jour les souscriptions d\'adhésion';
$lang['update_module'] = 'mettre à jour module';
$lang['update_module_success'] = 'module mis à jour avec succès';
$lang['update_modules_success'] = 'module mis à jour avec succès';
$lang['update_options_success'] = 'options mises à jour avec succès';
$lang['update_product'] = 'mise à jour produit';
$lang['update_product_category'] = 'mise à jour catégorie produit';
$lang['update_product_success'] = 'produit mis à jour avec succès';
$lang['update_products'] = 'mettre à jour produits';
$lang['update_region'] = 'mettre à jour région';
$lang['update_region_success'] = 'region mise à jour avec succès';
$lang['update_regions'] = 'mettre à jour régions';
$lang['update_review'] = 'mettre à jour revue';
$lang['update_sequence'] = 'mettre à jour séquence';
$lang['update_services'] = 'services de mise à jour du contenu';
$lang['update_settings_success'] = 'paramètres mis à jour avec succèss';
$lang['update_shipping_info'] = 'mettre à jour info expédition';
$lang['update_shipping_module'] = 'mettre à jour module d\'expédition';
$lang['update_sort_order'] = 'mettre à jour tri de commandes';
$lang['update_tax_classes'] = 'mettre à jour catégories de taxe';
$lang['update_tax_zone'] = 'mettre à jour zone fiscale';
$lang['update_tax_zones'] = 'mettre à jour zones fiscales';
$lang['update_text_ad'] = 'mettre à jour annonce textuelle';
$lang['update_text_ad_success'] = 'annonce textuelle mise à jour avec succès';
$lang['update_text_link'] = 'mettre à jour lien textuel';
$lang['update_text_link_success'] = 'lien textuel mis à jour avec succès';
$lang['update_ticket'] = 'mettre à jour requête';
$lang['update_tiers'] = 'mettre à jour niveaux';
$lang['update_tracking'] = 'mettre à jour l\'outil de suivi';
$lang['update_vendor'] = 'mettre à jour vendeur';
$lang['update_vendor_success'] = 'vendeur mis à jour avec succès';
$lang['update_video_success'] = 'vidéos mises à jour avec succès';
$lang['update_viral_pdf'] = 'mettre à jour pdf viral';
$lang['update_viral_pdf_success'] = 'pdf viral mis à jour avec succès';
$lang['update_viral_video'] = 'metre à jour vidéo virale';
$lang['update_viral_video_success'] = 'vidéo virale mise à jour avec succès';
$lang['update_zone'] = 'mettre à jour zone';
$lang['update_zone_success'] = 'zone mise à jour avec succès';
$lang['update_zones'] = 'mettre à jour zones';
$lang['updated_by'] = 'mis à jour par';
$lang['updated_on'] = 'mis àjour par';
$lang['upgraded_affiliate_group'] = 'groupe d\'affiliation mis à jour';
$lang['upload_banner'] = 'char ger la bannière';
$lang['upload_file'] = 'attacher fichier';
$lang['upload_photo'] = 'attacher photo';
$lang['upload_site_logo'] = 'upload site logo';
$lang['upsell_product'] = 'produit de vente additionnelle';
$lang['url'] = 'url';
$lang['use_external_embed'] = 'utiliser code attaché externe';
$lang['use_external_image'] = 'utiliser image externe';
$lang['use_group_defaults'] = 'utiliser groupe par défaut';
$lang['use_percentage'] = 'utiliser pourcentage';
$lang['use_tax_zone'] = 'utiliser zone fiscale';
$lang['use_video_default'] = 'utiliser vidéo comme média par défaut';
$lang['user'] = 'utilisateur';
$lang['user_already_subscribed'] = 'utilisateur déjà inscrit';
$lang['user_guide'] = 'guide de l\'utilisateur';
$lang['user_settings'] = 'utilisateurs';
$lang['username'] = 'identifiant';
$lang['users'] = 'utilisateurs';
$lang['users_imported_successfully'] = 'utilisateurs importés avec succès';
$lang['uses_per_coupon'] = 'usages par coupon';
$lang['value'] = 'valeur';
$lang['vcs_payments'] = 'paiements vcs';
$lang['vendor'] = 'marchand';
$lang['vendor_address'] = 'adresse vendeur';
$lang['vendor_address_1'] = 'adresse 1';
$lang['vendor_address_2'] = 'adresse 2';
$lang['vendor_city'] = 'ville';
$lang['vendor_city'] = 'ville du vendeur';
$lang['vendor_contact_email'] = 'adresse email';
$lang['vendor_contact_name'] = 'personne à contacter';
$lang['vendor_country'] = 'pays';
$lang['vendor_email_address'] = 'email vendeur';
$lang['vendor_name'] = 'nom vendeur';
$lang['vendor_phone'] = 'téléphone';
$lang['vendor_send_alert'] = 'envoyer alerte email';
$lang['vendor_state'] = 'Etat ou province';
$lang['vendor_zip'] = 'code zip ou postal';
$lang['vendors'] = 'vendeurs';
$lang['video_name'] = 'nom de la vidéo';
$lang['video_settings'] = 'paramètres vidéo';
$lang['videos'] = 'vidéos';
$lang['view'] = 'voir';
$lang['view_administrators'] = 'voir administrateurs';
$lang['view_affiliate_commissions'] = 'voir commissions d\'affiliés';
$lang['view_affiliate_groups'] = 'voir groupes d\'affiliation';
$lang['view_affiliate_marketing_modules'] = 'voir modules de marketing d\'affiliation';
$lang['view_affiliate_payments'] = 'voir paiements d\'affiliés';
$lang['view_affiliate_payments'] = 'voir paiements d\'affilié';
$lang['view_affiliate_payments'] = 'voir paiements d\'affilié';
$lang['view_affiliate_tools'] = 'voir outils d\'affiliation';
$lang['view_article_ad'] = 'voir article publicitaire';
$lang['view_article_ads'] = 'voir articles publictaires';
$lang['view_attributes'] = 'voir attributs';
$lang['view_banner'] = 'voir bannière';
$lang['view_banners'] = 'voir bannières';
$lang['view_cart'] = 'voir panier';
$lang['view_comments'] = 'voir commentaires';
$lang['view_comments_for'] = 'view comments for';
$lang['view_commissions'] = 'voir commissions';
$lang['view_commissions_for'] = 'voir commissions pour';
$lang['view_content_articles'] = 'voir articles de contenu';
$lang['view_content_categories'] = 'supprimer catégorie de contenu';
$lang['view_coupons'] = 'supprimer catégorie de contenu';
$lang['view_credits'] = 'voir crédits';
$lang['view_credits_for'] = 'voir crédits pour';
$lang['view_currencies'] = 'voir devises';
$lang['view_custom_commissions'] = 'voir commissions habituelles';
$lang['view_discount_groups'] = 'voir groupes au rabais';
$lang['view_documentation'] = 'Cliquez ici pour la docuementation en ligne';
$lang['view_downline'] = 'voir fileul';
$lang['view_email_ad'] = 'voir email publicitaire';
$lang['view_email_ads'] = 'voir emails publicitaires';
$lang['view_email_archive'] = 'voir archive email';
$lang['view_email_queue'] = 'voir email en file d\'attente';
$lang['view_export_modules'] = 'voir module d\'exportation';
$lang['view_faq_articles'] = 'supprimer catégorie de faq';
$lang['view_faq_categories'] = 'voir cétégorie faq';
$lang['view_follow_ups'] = 'voir lettres de suivi';
$lang['view_gateways_modules'] = 'voir terminaux de paiement';
$lang['view_html_ad'] = 'voir annonces html';
$lang['view_html_ads'] = 'voir annonces html';
$lang['view_import_modules'] = 'voir modules d\'importation';
$lang['view_invoice_payments'] = 'voir règlements de paiement';
$lang['view_invoices'] = 'voir factures';
$lang['view_invoices_for'] = 'voir factures pour';
$lang['view_list_members'] = 'voir liste des membres';
$lang['view_logs'] = 'voir traces de connexion';
$lang['view_mailing_lists'] = 'voir liste de diffusion';
$lang['view_manufacturers'] = 'voir fournisseurs';
$lang['view_member_dowline'] = 'voir membre filleul';
$lang['view_member_photos'] = 'voir photos membre';
$lang['view_members'] = 'voir membres';
$lang['view_memberships'] = 'voir les souscriptions d\'adhésion';
$lang['view_monthly_reports'] = 'voir les rapports mensuels';
$lang['view_options'] = 'voir options';
$lang['view_payments_to'] = 'voir paiements à';
$lang['view_phpinfo'] = 'voir information PHP';
$lang['view_product_categories'] = 'voir catégories de produit';
$lang['view_product_link'] = 'voir lien de produit';
$lang['view_product_reports'] = 'voir rapports de produit';
$lang['view_product_reviews'] = 'voir revues produit';
$lang['view_products'] = 'voir produits';
$lang['view_public_site'] = 'voir site';
$lang['view_quick_start_guide'] = 'commencer rapidement avec le guide de démarrage rapide';
$lang['view_referrals'] = 'voir filleuls';
$lang['view_report'] = 'voir rapport';
$lang['view_reports'] = 'voir rapports';
$lang['view_reports_for'] = 'voir rapports pour';
$lang['view_reviews'] = 'voir revues';
$lang['view_reviews_for'] = 'voir revues pour';
$lang['view_shipping'] = 'voir expédition';
$lang['view_shipping_modules'] = 'voir modules d\'expédition';
$lang['view_site'] = 'voir';
$lang['view_support_categories'] = 'voir catégories de support';
$lang['view_support_tickets'] = 'voir requêtes au support';
$lang['view_text_ad'] = 'voir annonce textuelle';
$lang['view_text_ads'] = 'voir annonces textuelles';
$lang['view_text_link'] = 'voir lien textuel';
$lang['view_text_links'] = 'voir liens textuels';
$lang['view_themes'] = 'voir thèmes';
$lang['view_tracking'] = 'voir le suivi';
$lang['view_traffic_stats'] = 'voir stats du traffic';
$lang['view_vendors'] = 'voir vendeurs';
$lang['view_video'] = 'voir vidéo';
$lang['view_video_tutorials'] = 'Cliquer ici pour visionner quelques tutoriels en ligne';
$lang['view_viral_pdf'] = 'voir pdf viral';
$lang['view_viral_pdfs'] = 'voir pdfs viraux';
$lang['view_viral_video'] = 'voir vidéo virale';
$lang['view_viral_videos'] = 'voir vidéos virales';
$lang['view_yearly_reports'] = 'voir rapports annuels';
$lang['view_zones'] = 'voir zones';
$lang['viral_pdf_body'] = 'texte pdf viral';
$lang['viral_pdf_details'] = 'détails pdfs viraux';
$lang['viral_pdf_name'] = 'nom pdf viral';
$lang['viral_pdf_options'] = 'options pdfs viraux';
$lang['viral_pdf_title'] = 'titre pdf viral';
$lang['viral_pdfs'] = 'pdfs viraux';
$lang['viral_video_body'] = 'contenu vidéo virale';
$lang['viral_video_details'] = 'détails vidéo virale';
$lang['viral_video_link'] = 'lien vidéo virale';
$lang['viral_video_name'] = 'nom vidéo virale';
$lang['viral_video_options'] = 'options de vidéo virale';
$lang['viral_video_title'] = 'titre vidéo virale';
$lang['viral_video_width'] = 'largeur vidéo virale';
$lang['viral_videos'] = 'vidéos virales';
$lang['wait_downline_generation'] = 'veuillez patienter pendant que le système génère le filleul';
$lang['wait_site_map_notify'] = 'veuillez patienter pendant que nous envoyons une notification à Google au sujet du plan du site	';
$lang['website'] = 'site internet';
$lang['week'] = 'semaine';
$lang['weekly'] = 'hebdomadaire';
$lang['weight'] = 'weight';
$lang['weight_must_be_numeric'] = 'le poids doit être numérique';
$lang['width'] = 'largeur';
$lang['work_phone'] = 'téléphone professionnel';
$lang['xml_site_map'] = 'XML Site Map';
$lang['year'] = 'année';
$lang['yearly_affiliate_clicks_stats_by_month'] = 'clics mensuels de l\'affilié/année';
$lang['yearly_commissions_stats_by_month'] = 'commissions mensuelles/ année';
$lang['yearly_invoice_payments_stats_by_month'] = 'règlements de facture mensuels/année';
$lang['yearly_reports'] = 'rapports annuels';
$lang['yearly_sales_stats_by_month'] = 'ventes mensuelles /année';
$lang['yearly_support_ticket_stats_by_month'] = 'requêtes mensuelles au support/ année';
$lang['yes'] = 'yes';
$lang['you_want_to_apply_credits'] = 'sélectioner une facture impayée à laquelle appliquer ce crédit';
$lang['zone_added_successfully'] = 'zone ajoutée avec succès';
$lang['zone_based_shipping'] = 'expédition basée sur la zone';
$lang['zone_country'] = 'zone de pays';
$lang['zone_deleted_successfully'] = 'zone supprimée avec succès';
$lang['zone_edited_successfully'] = 'zone éditée avec succès';
$lang['zone_name'] = 'nom de zone';
$lang['zone_region'] = 'zone région';

// 1.0.7.80
$lang['add_to_category'] = 'ajoutent les produits vérifiés à la catégorie';
$lang['affiliate_group_required'] = 'groupe de filiale a exigé';
$lang['all_amounts_must_be_numeric'] = 'tous les montants doit être numérique';
$lang['authorize_net_sim_payments'] = 'paiements d\'Authorize.Net (SIM)';
$lang['average_sales_day'] = 'ventes moyennes/jour';
$lang['category_add'] = 's\'ajoutent à la catégorie';
$lang['content_updated_successfully'] = 'contenu a mis à jour avec succès';
$lang['could_not_delete_global_discount'] = 'ne pourrait pas supprimer l\'escompte global';
$lang['delete_discount_success'] = 'escompte global a supprimé avec succès';
$lang['desc_enable_affiliate_group_permissions'] = 'permettent des permissions de groupe de filiale sur ce contenu';
$lang['desc_enable_recurring_coupon'] = 'si vous voulez que ce bon soit employé pour des adhésions périodiques, permettent cette option';
$lang['desc_list_on_registration'] = 'ajoutent l\'utilisateur à la liste d\'adresses suivante quand l\'utilisateur est ajouté à ce groupe de filiale sur l\'enregistrement';
$lang['desc_module_payment_gateway_authorize_net_sim_url'] = 'URL de paiement';
$lang['desc_module_payment_gateway_authorize_net_sim_merchant_id'] = 'identification d\'ouverture d\'api';
$lang['desc_module_payment_gateway_authorize_net_sim_transaction_key'] = 'clef de transaction';
$lang['desc_module_payment_gateway_authorize_net_sim_enable_testing'] = 'permettent l\'essai';
$lang['desc_module_payment_gateway_authorize_net_sim_authorization_type'] = 'type d\'autorisation';
$lang['desc_module_payment_gateway_authorize_net_sim_header_html_payment_form'] = 'HTML d\'en-tête pour la forme de paiement';
$lang['desc_module_payment_gateway_authorize_net_sim_footer_html_payment_form'] = 'HTML de titre de bas de page pour la forme de paiement';
$lang['desc_module_payment_gateway_authorize_net_sim_logo_url'] = 'URL de logo';
$lang['desc_module_payment_gateway_authorize_net_sim_send_customer_notification'] = 'envoient l\'avis de client';
$lang['desc_module_payment_gateway_authorize_net_sim_send_merchant_notification'] = 'envoient l\'avis marchand';
$lang['desc_module_payment_gateway_authorize_net_sim_enable_debug_email'] = 'permettent corrigent l\'email';
$lang['desc_module_payment_gateway_authorize_net_sim_md5_hash'] = 'permettent le gâchis md5';
$lang['desc_module_payment_gateway_payjunction_password'] = 'mot de passe de Payjunction';
$lang['desc_module_payment_gateway_payjunction_require_cvv'] = 'permettent le code la sécurité/CVV';
$lang['desc_module_payment_gateway_payjunction_transaction_type'] = 'type de transaction';
$lang['desc_module_payment_gateway_payjunction_url'] = 'URL de paiement';
$lang['desc_module_payment_gateway_payjunction_username'] = 'username de Payjunction';
$lang['desc_module_payment_gateway_payjunction_quick_shop_url'] = 'URL de forme de payjunction';
$lang['desc_module_payment_gateway_payjunction_quick_shop_merchant_id'] = 'username de payjunction';
$lang['desc_module_payment_gateway_payjunction_quick_shop_enable_debug_email'] = 'permettent corrigent l\'email';
$lang['desc_module_shipping_fedex_server_url'] = 'URL de serveur de Federal Express';
$lang['desc_module_shipping_fedex_account_number'] = 'numéro de compte';
$lang['desc_module_shipping_fedex_meter_number'] = 'nombre de mètre';
$lang['desc_module_shipping_fedex_carrier_code'] = 'code de porteur';
$lang['desc_module_shipping_fedex_dropoff_type'] = 'laissent tomber au loin le type';
$lang['desc_module_shipping_fedex_packaging'] = 'empaquetant';
$lang['desc_module_shipping_fedex_handling_fee'] = 'honoraires de manipulation';
$lang['desc_module_shipping_fedex_enable_debug'] = 'permettent corrigent';
$lang['desc_module_shipping_fedex_enable_tax_zone'] = 'permettent la zone d\'impôts';
$lang['desc_module_shipping_fedex_tax_zone'] = 'zone choisie d\'impôts';
$lang['desc_module_shipping_fedex_shipping_types'] = 'embarquant dactylographie';
$lang['desc_module_shipping_UPS_gateway_url'] = 'URL de passage';
$lang['desc_module_shipping_UPS_access_key'] = 'clef d\'accès d\'UPS';
$lang['desc_module_shipping_UPS_username'] = 'username d\'UPS';
$lang['desc_module_shipping_UPS_password'] = 'mot de passe d\'UPS';
$lang['desc_module_shipping_UPS_enable_debug'] = 'permettent corrigent';
$lang['desc_product_sale_price'] = 'prix de vente de produit';
$lang['desc_restrict_access_to'] = 'limitent l\'accès à la filiale suivante groupent';
$lang['desc_shipping_amount'] = 'si vous voulez facturer un montant spécifique d\'expédition pour chaque unité de ce produit, écrivent la quantité ici';
$lang['desc_sts_affiliate_enable_mlm_forced_matrix'] = 'permettent ou désactivent l\'arrangement obligatoire de matrice';
$lang['desc_sts_affiliate_mlm_matrix_width'] = 'la largeur maximum de votre matrice obligatoire';
$lang['desc_sts_cart_minimum_purchase_checkout'] = 'si vous voulez avoir une quantité minimum dans le chariot d\'utilisateurs avant contrôle, écrivent une quantité ici';
$lang['desc_sts_form_require_initial_fee_checkout'] = 'si vous voulez charger des clients de première fois des honoraires sur leur achat initial, écrivent l\'identité de produit pour l\'achat initial';
$lang['desc_tax_zone'] = 'si vous voulez prélever spécifiques pour ce produit, choisissent la zone d\'impôts pour employer';
$lang['discount'] = 'escompte';
$lang['enable_affiliate_group_permissions'] = 'permettent des permissions de groupe de filiale';
$lang['enable_recurring_coupon'] = 'permettent le bon périodique';
$lang['global_discounts'] = 'escomptes globaux';
$lang['JEM_supported_browsers'] = 'la région d\'admin de JEM exige Firefox 3 ou chrome ou Internet Explorer 7 plus grand, de Google ou plus grand';
$lang['latest_videos'] = 'vidéos';
$lang['list_on_registration'] = 'a placé la liste d\'adresses sur l\'enregistrement';
$lang['manage_global_discounts'] = 'contrôlent des escomptes globaux';
$lang['maximum_amount'] = 'quantité maximum';
$lang['minimum_amount'] = 'quantité minimum';
$lang['module_payment_gateway_authorize_net_aim_merchant_id'] = 'identification d\'ouverture d\'api';
$lang['module_payment_gateway_authorize_net_sim_url'] = 'URL de paiement';
$lang['module_payment_gateway_authorize_net_sim_merchant_id'] = 'identification d\'ouverture d\'api';
$lang['module_payment_gateway_authorize_net_sim_transaction_key'] = 'clef de transaction';
$lang['module_payment_gateway_authorize_net_sim_enable_testing'] = 'permettent l\'essai';
$lang['module_payment_gateway_authorize_net_sim_authorization_type'] = 'type d\'autorisation';
$lang['module_payment_gateway_authorize_net_sim_header_html_payment_form'] = 'HTML d\'en-tête pour la forme de paiement';
$lang['module_payment_gateway_authorize_net_sim_footer_html_payment_form'] = 'HTML de titre de bas de page pour la forme de paiement';
$lang['module_payment_gateway_authorize_net_sim_logo_url'] = 'URL de logo';
$lang['module_payment_gateway_authorize_net_sim_send_customer_notification'] = 'envoient l\'avis de client';
$lang['module_payment_gateway_authorize_net_sim_send_merchant_notification'] = 'envoient l\'avis marchand';
$lang['module_payment_gateway_authorize_net_sim_enable_debug_email'] = 'permettent corrigent l\'email';
$lang['module_payment_gateway_authorize_net_sim_md5_hash'] = 'permettent le gâchis md5';
$lang['module_payment_gateway_payjunction_quick_shop_url'] = 'URL de forme de payjunction';
$lang['module_payment_gateway_payjunction_quick_shop_merchant_id'] = 'username de payjunction';
$lang['module_payment_gateway_payjunction_quick_shop_enable_debug_email'] = 'permettent corrigent l\'email';
$lang['module_shipping_fedex_server_url'] = 'URL de serveur de Federal Express';
$lang['module_shipping_fedex_account_number'] = 'numéro de compte';
$lang['module_shipping_fedex_meter_number'] = 'nombre de mètre';
$lang['module_shipping_fedex_carrier_code'] = 'code de porteur';
$lang['module_shipping_fedex_dropoff_type'] = 'laissent tomber au loin le type';
$lang['module_shipping_fedex_packaging'] = 'empaquetant';
$lang['module_shipping_fedex_handling_fee'] = 'honoraires de manipulation';
$lang['module_shipping_fedex_enable_debug'] = 'permettent corrigent';
$lang['module_shipping_fedex_enable_tax_zone'] = 'permettent la zone d\'impôts';
$lang['module_shipping_fedex_tax_zone'] = 'zone choisie d\'impôts';
$lang['module_shipping_fedex_shipping_types'] = 'embarquant dactylographie';
$lang['module_shipping_UPS_gateway_url'] = 'URL de passage';
$lang['module_shipping_UPS_access_key'] = 'clef d\'accès d\'UPS';
$lang['module_shipping_UPS_username'] = 'username d\'UPS';
$lang['module_shipping_UPS_password'] = 'mot de passe d\'UPS';
$lang['module_shipping_UPS_enable_debug'] = 'permettent corrigent';
$lang['monthly_sales_revenue'] = 'revenu de ventes mensuel';
$lang['new_user_registrations'] = 'nouveaux enregistrements d\'utilisateur';
$lang['news_updates'] = 'nouvelles et mises à jour';
$lang['permissions'] = 'permissions';
$lang['product_sale_price'] = 'prix de vente de produit';
$lang['remove_affiliate_group_restrictions'] = 'enlèvent toutes les restrictions de groupe de filiale';
$lang['remove_membership_restrictions'] = 'enlèvent toutes les restrictions d\'adhésion';
$lang['restrict_access_to'] = 'limitent l\'accès à';
$lang['restrict_to_affiliate_group'] = 'limitent l\'accès au groupe de filiale';
$lang['restrict_to_membership'] = 'limitent l\'accès à l\'adhésion';
$lang['select_country'] = 'pays choisi';
$lang['select_option'] = 'choisissent une option';
$lang['stats'] = 'stat';
$lang['sts_affiliate_enable_mlm_forced_matrix'] = 'filiale permettent la matrice obligatoire';
$lang['sts_affiliate_mlm_matrix_width'] = 'a forcé la largeur de matrice';
$lang['sts_cart_minimum_purchase_checkout'] = 'exigent la quantité minimum de chariot sur le contrôle';
$lang['sts_form_require_initial_fee_checkout'] = 'exigent les honoraires initiaux sur le premier achat';
$lang['support_tickets_submitted'] = 'appui étiquette soumis';
$lang['type_category_name'] = 'introduisent dedans un nom de catégorie';
$lang['update_discount_success'] = 'des escomptes globaux a mis à jour avec succès';
$lang['video_description'] = 'description visuelle';
$lang['view_global_discounts'] = 'escomptes globaux de vue';
$lang['video_image'] = 'images vidéo facultatives';
$lang['video_image_only_255_chars'] = 'des images vidéo doit être moins de 255 caractères';
?>