<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| COPYRIGHT NOTICE
| Copyright 2008 JROX Technologies, Inc.  All Rights Reserved.
| -------------------------------------------------------------------------
| This script may be only used and modified in accordance to the license
| agreement attached (license.txt) except where expressly noted within
| commented areas of the code body. This copyright notice and the
| comments above and below must remain intact at all times.  By using this
| code you agree to indemnify JROX Technologies, Inc, its corporate agents
| and affiliates from any liability that might arise from its use.
|
| Selling the code for this program without prior written consent is
| expressly forbidden and in violation of Domestic and International
| copyright laws.
|
| -------------------------------------------------------------------------
| FILENAME - module_payment_gateway_moneybookers_lang.php
| -------------------------------------------------------------------------
|
| This is the language file the moneybookers gateway
|
*/

$lang['moneybookers'] = 'moneybookers payment gateway';
$lang['module_payment_gateway_moneybookers_email'] = 'moneybookers email';
$lang['module_payment_gateway_moneybookers_url'] = 'moneybookers payment gateway URL';
$lang['module_payment_gateway_moneybookers_currency_code'] = 'currency code';
$lang['module_payment_gateway_moneybookers_language'] = 'language code';
$lang['module_payment_gateway_moneybookers_logo_url'] = 'website logo URL';
$lang['module_payment_gateway_moneybookers_secret_word'] = 'moneybookers secret word';
$lang['module_payment_gateway_moneybookers_enable_debug_email'] = 'enable debug email';
$lang['module_payment_gateway_moneybookers_enable_verification'] = 'enable status URL verification';
?>