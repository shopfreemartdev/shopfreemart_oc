<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| COPYRIGHT NOTICE
| Copyright 2008 JROX Technologies, Inc.  All Rights Reserved.
| -------------------------------------------------------------------------
| This script may be only used and modified in accordance to the license
| agreement attached (license.txt) except where expressly noted within
| commented areas of the code body. This copyright notice and the
| comments above and below must remain intact at all times.  By using this
| code you agree to indemnify JROX Technologies, Inc, its corporate agents
| and affiliates from any liability that might arise from its use.
|
| Selling the code for this program without prior written consent is
| expressly forbidden and in violation of Domestic and International
| copyright laws.
|
| -------------------------------------------------------------------------
| FILENAME - module_payment_gateway_paypalpro_lang.php
| -------------------------------------------------------------------------
|
| This is the language file the greenbyphone gateway
|
*/

$lang['module_shipping_easypost_api_key'] = 'api key';
$lang['module_shipping_easypost_height'] = 'package height';
$lang['module_shipping_easypost_length'] = 'package length';
$lang['module_shipping_easypost_width'] = 'package width';
$lang['module_shipping_easypost_handling_fee'] = 'handling fee';
$lang['module_shipping_easypost_enable_tax_zone'] = 'enable tax zone';
$lang['module_shipping_easypost_tax_zone'] = 'tax zone';
$lang['module_shipping_easypost_enable_debug'] = 'enable debug';