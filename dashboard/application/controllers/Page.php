<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends SB_Controller {

	protected $_key 	= 'id';
	protected $_class	= 'page';
	protected $layout = "layouts/main";

    public $use_submenu = true;
    public $use_navigation = true;

	
	function __construct()
	{
		parent::__construct();	
		
	}
		
	public function index( $page = null)
	{
		if($page != null) :
			$row = $this->db->query("SELECT * FROM sfm_cms_pages WHERE alias ='$page' and status='enable' ")->row();

			if(count($row) >=1)
			{

				$this->data['pageTitle'] = $row->title;
				$this->data['pageNote'] = $row->note;		
				$this->data['breadcrumb'] = 'active';	
				$this->data['show_sidemenu'] = false;				
				if($row->access !='')
				{
					$access = json_decode($row->access,true)	;	
				} else {
					$access = array();
				}	

				// If guest not allowed 
				// if($row->allow_guest !=1)
				// {	

				// 	$group_id = $this->session->userdata('gid');				
				// 	$isValid =  (isset($access[$group_id]) && $access[$group_id] == 1 ? 1 : 0 );	
				// 	if($isValid ==0)
				// 	{
				// 		redirect('',301);				
				// 	}
				// }	
			
				$filename = "application/views/pages/".$row->filename.".php";
				if(file_exists($filename))
				{
					$page = $row->filename;
				} else {
					redirect('',301);						
				}
				
			} else {
				redirect('',301);			
			}
			
			
		else :

			$this->data['pageTitle'] = 'Home';
			$this->data['pageNote'] = 'Welcome To Our Site';
			$this->data['breadcrumb'] = 'inactive';			
			$page = 'home';
		endif;	

       $this->data['content'] = $this->load->view('pages/'.$page,$this->data,true );
	   $this->load->view("themes/shop/main", $this->data );
    //	$this->render_view('pages/'.$page,$this->data);
		
	}

	function  submitcontact()
	{
	
		$rules = array(
			array('field'   => 'name','label'   => ' Please Fill Name','rules'   => 'required'),
			array('field'   => 'email','label'   => 'email ','rules'   => 'required|email'),
			array('field'   => 'message','label'   => 'message','rules'   => 'required'),
		);	


		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() )
		{
			
			$data = array(
				'name'=>$this->input->post('name',true),
				'email'=>$this->input->post('email',true),
				'subject'=> 'New Form Submission',
				'notes'=>$this->input->post('message',true)
			); 
			$message = $this->load->view('emails/contact', $data,true); 
			
			
			$to 		= 	CNF_EMAIL;
			$subject 	= 'New Form Submission';
			$headers  	= 'MIME-Version: 1.0' . "\r\n";
			$headers 	.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers 	.= 'From: '.$this->input->post('name',true).' <'.$this->input->post('sender',true).'>' . "\r\n";
				//mail($to, $subject, $message, $headers);			
			$message = "Thank You , Your message has been sent !";
			$this->session->set_flashdata('message',SiteHelpers::alert('success',$message));
			redirect('contact-us',301);
			
				
		} else {
			$message = "The following errors occurred";
			$this->session->set_flashdata(array(
					'message'=>SiteHelpers::alert('error',$message),
					'errors'	=> validation_errors('<li>', '</li>')
			));
			redirect('contact-us',301);	
		}		
	}

	public function lang($lang)
	{

		$this->session->set_userdata('lang',$lang);	
		 redirect($_SERVER['HTTP_REFERER']);  	
	}	


	public function skin($skin = 'sximo')
	{

		$this->session->set_userdata('themes',$skin);	
		 redirect($_SERVER['HTTP_REFERER']);  	
	}	

	public function survey($id, $hash, $userid = 0, $sendcronid = 0)
    {
        if (!$hash || !$id) {
            die('No survey specified');
        }
        $this->load->model('crm/surveys_model');
        $survey = $this->surveys_model->get($id);
        if (!$survey || ($survey->hash != $hash)) {
            show_404();
        }
        if ($survey->active == 0) {
            // Allow users with permission manage surveys to preview the survey even if is not active
            if (!has_permission('Surveys', '', 'view')) {
                die('Survey not active');
            }
        }
        // Check if survey is only for logged in participants / staff / clients
        if ($survey->onlyforloggedin == 1) {
            if (!is_logged_in()) {
                die('This survey is only for logged in users');
            }
        }
        // Ip Restrict check
        if ($survey->iprestrict == 1) {
            $this->db->where('surveyid', $id);
            $this->db->where('ip', $this->input->ip_address());
            $total = $this->db->count_all_results('sfm_crm_surveyresultsets');
            if ($total > 0) {
                die('Already participated on this survey. Thanks');
            }
        }

    	$_this = & get_Instance();
		$uid = $_this->session->userdata('uid');
		$is_staff = $_this->session->userdata('staff_username');

    	if(isset($uid)){
    		$data['userid'] = $uid;
    		if(isset($is_staff)){
    			$data['is_staff'] = false;
    		}
    		else{
    			$data['is_staff'] = false;
    		}
    	}
    	else{
    		$data['userid'] = 0;
    		$data['is_staff'] = false;
    	}
    	if($userid != 0){
    		$data['userid'] = $userid;
    	}

       $survey_staffid=0;
        if($sendcronid != 0){
            $this->load->model('managesurvey/managesurveymodel');
            $recipient = $this->managesurveymodel->getrecipientemail($sendcronid);
            $survey_staffid = $this->managesurveymodel->getsurveystaffid($sendcronid);
            $data['email'] = $recipient;
        }

            if ($this->input->post()) {
                $success = $this->surveys_model->add_survey_result($id, $this->input->post());
                if ($success) {
                    $survey = $this->surveys_model->get($id);
                    if ($survey->redirect_url !== '') {
                        redirect($survey->redirect_url);
                    }
                    //set_alert('success', 'Thank you for participating in this survey. Your answers are very important to us.');
                    $default_redirect = perfex_do_action('survey_default_redirect', WP_URL."/dashboard/page/surveydone");
                    redirect($default_redirect);
                }
            }
        $this->use_navigation = false;
        $this->use_submenu    = false;
        $data['survey']       = $survey;
        $data['title']        = $data['survey']->subject;
        $this->load->model('crm/staff_model');
        $data['stafflist'] = $this->staff_model->getstaffsupport($survey_staffid);
        $this->data           = $data;
        $this->view           = 'survey_view';
        $this->layout();
    }

    public function layout()
    {

        $this->data['use_navigation'] = true;
        if ($this->use_navigation == false) {
            $this->data['use_navigation'] = false;
        }

        $this->data['use_submenu'] = true;
        if ($this->use_submenu == false) {
            $this->data['use_submenu'] = false;
        }

        // $this->template['head'] = '';
        // if ($this->use_head == true) {
        //     $this->template['head'] = $this->load->view('themes/' . active_clients_theme() . '/head', $this->data, true);
        // }

        // $this->template['view'] = $this->load->view('themes/' . active_clients_theme() . '/views/' . $this->view, $this->data, true);

        // $this->template['footer'] = '';
        // if ($this->use_footer == true) {
        //     $this->template['footer'] = $this->load->view('themes/' . active_clients_theme() . '/footer', $this->data, true);
        // }

        // $this->template['scripts'] = '';
        // if ($this->add_scripts == true) {
        //     $this->template['scripts'] = $this->load->view('themes/' . active_clients_theme() . '/scripts', $this->data, true);
        // }

        $this->render_public('crm/survey_view');
    }

    public function surveydone()
    {
        
        $this->data['use_navigation'] = true;
        if ($this->use_navigation == false) {
            $this->data['use_navigation'] = false;
        }

        $this->data['use_submenu'] = true;
        if ($this->use_submenu == false) {
            $this->data['use_submenu'] = false;
        }
        $this->view           = 'survey_done';
       	
       	$this->render_public('crm/survey_done');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/page.php */ 