
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adminpage extends  Admin_Controller
{


	protected $_key 	= 'id';
	protected $_class	= 'page';

	function __construct() {
		parent::__construct();
		
		// $this->load->model('foundingmembershipmodel');
		// $this->load->helper(ucfirst('foundingmembership'));
		// $this->model = $this->foundingmembershipmodel;
		
		// $this->info = $this->model->makeInfo( $this->module);
		// $this->access = $this->model->validAccess($this->info['id']);	
		// $this->data = array_merge( $this->data, array(
		// 	'pageTitle'	=> 	$this->info['title'],
		// 	'pageNote'	=>  $this->info['note'],
		// 	'pageModule'	=> 'foundingmembership',
		// 	'pageUrl'			=>  base_url('foundingmembership'),
		// ));
		
		//if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		
	}

	public function index( $page = null)
	{



		if($page != null) :
			$row = $this->db->query("SELECT * FROM sfm_cms_pages WHERE alias ='$page' and status='enable' ")->row();

			if(count($row) >=1)
			{

				$this->data['pageTitle'] = $row->title;
				$this->data['pageNote'] = $row->note;		
				$this->data['breadcrumb'] = 'active';	
				$this->data['show_sidemenu'] = false;				
				if($row->access !='')
				{
					$access = json_decode($row->access,true)	;	
				} else {
					$access = array();
				}	
		

				$filename = "application/views/pages/".$row->filename.".php";
				if(file_exists($filename))
				{
					$page = $row->filename;
				} else {
					redirect('',301);						
				}
				
			} else {
				redirect('',301);			
			}
			
			
		else :

			$this->data['pageTitle'] = 'Home';
			$this->data['pageNote'] = 'Welcome To Our Site';
			$this->data['breadcrumb'] = 'inactive';			
			$page = 'home';
		endif;	

    	$this->render_view('pages/'.$page,$this->data);
		
	}

}