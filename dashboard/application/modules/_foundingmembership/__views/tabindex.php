	<div class="panel">
        <div class="panel-default panel-heading">
            <h4> Memberships</h4>
        </div>
        <div class="panel-body">			
		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="foundingmembershipView"></div>			
		<div id="foundingmembershipGrid"></div>		
		</div>
	</div>


<script>


	var displayfoundingmembershipFilterPanel = Cookies.set('ti_displayfoundingmembershipFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displayfoundingmembershipFilterPanel = 'false';
		} else
		{
			displayfoundingmembershipFilterPanel = 'true';
			$( '#foundingmembershipFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displayfoundingmembershipFilterPanel', displayfoundingmembershipFilterPanel);

		reloadData( '#foundingmembership',"<?php echo base_url();?>foundingmembership/tabdata?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displayfoundingmembershipFilterPanel);
			if (displayfoundingmembershipFilterPanel == null)
			{
				displayfoundingmembershipFilterPanel = 'true';	
				Cookies.set('displayfoundingmembershipFilterPanel', displayfoundingmembershipFilterPanel);
			}
			console.log(displayfoundingmembershipFilterPanel);				
			if (displayfoundingmembershipFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#foundingmembership','<?php echo base_url();?>foundingmembership/tabdata');	

		});
		
	
</script>
