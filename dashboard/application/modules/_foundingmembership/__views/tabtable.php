<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');?>

		<form
			action='<?php echo ci_site_url('foundingmembership/destroy') ?>' class='form-horizontal' id ='foundingmembershipForm' method="post" <?php echo $setting['gridsettings']['gs_css']; ?> >

			<div class="panel panel-default panel-table">
		
			<!-- gs_filterbar -->
			<?php if($setting['gridsettings']['gs_filterbar']) {
				echo $filterbar;
			} ?>

				<!-- Grid Table Row -->
				<div class="table-responsive">

						<table class="display dataTable table table-striped table-hover " id="foundingmembershipTable" >
								<?php if(empty($rowData)) : ?>
									<tr>
										<div class="alert alert-warning">
											<center>
											<h4><?=lang('no_records_found')?></h4>
											</center>
										</div>
									</tr>

								<?php else: ?>	
						
						
							<thead>
								<tr>
									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<th>
										No
									</th>
									<?php } ?>

									<?php if($setting['gridsettings']['gs_checkbox']) { ?>
									<th>
										<input type="checkbox" class="checkall" />
									</th>
									<?php } ?>

									<?php if($setting['view-method']=='expand') { ?>
									<th>
									</th> <?php } ?>
									<?php foreach ($tableGrid as $k => $t) : ?>
									<?php if($t['view'] =='1'): ?>
									<th>
										<?php echo SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array()))  ?>
									</th>
									<?php endif; ?>
									<?php endforeach; ?>

									<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
									<th width="70">
										Action
									</th>
									<?php } ?>

								</tr>
							</thead>

							<tbody>
							
							<?php if($access['is_add'] =='1' && $setting['inline']=='true'): ?>
																																								<tr id="form-0" >

									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<td>
										#
									</td>
									<?php } ?>

									<?php if($setting['gridsettings']['gs_checkbox']) { ?>
									<th>
									</th>
									<?php } ?>

									<?php if($setting['view-method']=='expand') { ?>
									<td>
									</td> <?php } ?>
									<?php foreach ($tableGrid as $t) :
									if($t['view'] =='1') : ?>
									<td data-form="<?php echo $t['field'];?>" data-form-type="<?php echo  AjaxHelpers::inlineFormType($t['field'],$tableForm);?>">
										<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>
									</td>
									<?php endif;
									endforeach ?>

									<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
									<td style="width:50px;">

										<button type="button"  class=" btn btn-xs btn-info" rel="#<?php echo $pageModule ;?>Form"
						onclick="ajaxInlineSave('#<?php echo $pageModule ;?>','<?php echo ci_site_url('foundingmembership/quicksave') ;?>','<?php echo ci_site_url('foundingmembership/data?md=') ;?>')">
											<i class="fa fa-save">
											</i>
										</button>
									</td>
									
								
									<?php } ?>

								</tr>
									<?php endif;?>

									<?php foreach ( $rowData as $i => $row ) :
									$id = $row->id;
									?>
																																															<tr id="form-<?php echo $row->id ;?>">

									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<td width="50">
										<?php echo ($i+1+$page) ?>
									</td>
									<?php } ?>

									<?php if($setting['gridsettings']['gs_checkbox']) { ?>
									<td width="50">
										<input type="checkbox" class="ids" name="id[]" value="<?php echo $row->id ?>" />
									</td>
									<?php } ?>

									<?php if($setting['view-method']=='expand'): ?>
									<td>
										<a href="javascript:void(0)" class="expandable" rel="#row-<?php echo $row->id ;?>" data-url="<?php echo ci_site_url('foundingmembership/show/'.$id) ;?>">
											<i class="fa fa-plus " >
											</i>
										</a>
									</td>
									<?php endif;?>
									<?php foreach ( $tableGrid as $j => $field ) : ?>
									<?php if($field['view'] =='1'):
									$conn = (isset($field['conn']) ? $field['conn'] : array() );
									$value = AjaxHelpers::gridFormater($row->$field['field'], $row , $field['attribute'],$conn);

									?>
									<td align="<?php echo $field['align'];?>" data-values="<?php echo $row->$field['field'] ;?>" data-field="<?php echo  $field['field'] ;?>" data-format="<?php echo htmlentities($value);?>">
										<?php echo  $value;?>
									</td>
									<?php endif; ?>
									<?php endforeach; ?>

									<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
									<td data-values="action" data-key="<?php echo $row->id ;?> " >
										<?php if($setting['gridsettings']['gs_inlinebuttonpanel'])
										echo AjaxHelpers::buttonActionButtonInline('foundingmembership',$access,$row->id ,$setting);
										else
										echo AjaxHelpers::buttonAction('foundingmembership',$access,$row->id ,$setting);
										?>
									</td>
									<?php } ?>

								</tr>
									<?php if($setting['view-method']=='expand'): ?>
																						<tr style="display:none" class="expanded" id="row-<?php echo $row->id;?>">

									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<td class="number">
									</td>
									<?php } ?>

									<td>
									</td>
									<td>
									</td>
									<td colspan="<?php echo $colspan;?>" class="data">
									</td>
									<td>
									</td>
								</tr>
									<?php endif;?>
									<?php endforeach; ?>

							</tbody>
								<?php endif;?>							
							
						</table>

				</div>

				<?php if($setting['gridsettings']['gs_footer']) : ?>
				<?php $this->load->view('ajaxfooter'); ?>
			<?php endif; ?>				
			
			</div>
		</form>
		

<script>

	function dofilterbutton_click()
	{
		$('#panel-filter').toggle('slow');
	
	}

	function filterList()
	{
		$('#filter-form').submit();
	}


	$(document).ready(function()
		{
			$('.tips').tooltip();
			
			loadControlvalues();
			
			$('input[type="checkbox"],input[type="radio"]').iCheck(
				{
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
				});
			$('#foundingmembershipTable .checkall').on('ifChecked',function()
				{
					$('#foundingmembershipTable input[type="checkbox"]').iCheck('check');
				});
			$('#foundingmembershipTable .checkall').on('ifUnchecked',function()
				{
					$('#foundingmembershipTable input[type="checkbox"]').iCheck('uncheck');
				});
			$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true});
			
			$('#foundingmembershipPaginate .pagination li a').click(function()
				{
					var url = $(this).attr('href');
					if(url != '')
					{
						reloadData('#foundingmembership',url);
					}
					return false ;
				});



		});
		
	
</script>

