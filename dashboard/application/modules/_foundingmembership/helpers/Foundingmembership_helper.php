<?php

perfex_add_action('foundingmembership_render_table_header','_foundingmembership_render_table_header');
function _foundingmembership_render_table_header(){

}

perfex_add_action('foundingmembership_before_render_table_toolbar','_foundingmembership_before_render_table_toolbar');
function _foundingmembership_before_render_table_toolbar(){

}

perfex_add_action('foundingmembership_render_table_toolbar','_foundingmembership_render_table_toolbar');
function _foundingmembership_render_table_toolbar(){
   echo get_instance()->load->view('managemembers/toolbar');
}

perfex_add_action('foundingmembership_after_render_table_toolbar','_foundingmembership_after_render_table_toolbar');
function _foundingmembership_after_render_table_toolbar(){

}

perfex_add_action('foundingmembership_before_render_table_pagehead','_foundingmembership_before_render_table_pagehead');
function _foundingmembership_before_render_table_pagehead(){

}

perfex_add_action('foundingmembership_after_render_table_pagehead','_foundingmembership_after_render_table_pagehead');
function _foundingmembership_after_render_table_pagehead(){

}

perfex_add_action('foundingmembership_render_table_row_action_buttons','_foundingmembership_render_table_row_action_buttons');
function _foundingmembership_render_table_row_action_buttons($param = array()){
	$row = $param['row'];
	$access = $param['access'];
	$setting = $param['setting'];
	//return AjaxHelpers::buttonAction('foundingmembership',$access,$row->id ,$setting);

	$html = "";
	$module = 'foundingmembership';
	$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
	$id = $row->id;
	$balance = $row->balance;
	$status = $row->status;

	$html = '<div class="btn-group">';


	$html = '<a href="'.base_url($module.'/show/'.$id).'" '.$onclick.' data-toggle="tooltip" title="" class="btn btn-default" data-original-title="View" style="margin-right: 10px;"><i class="fa fa-search"></i> View</a>';

	if ($status != 'Completed') {
		$user_to_check = array(6062);
		if ( in_array(get_client_user_id(), $user_to_check) ) {
			//var_dump($row);
			$html .= '<a href="'.base_url().'../product/joint-venture-partner-founding-membership/?fref_id='.$id.'&bal='.$balance.'" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Pay"><i class="fa fa-check-circle-o"></i> Pay</a>';
		}
	}

	$html .= '</div>';
	
	return $html;
}

perfex_add_action('foundingmembership_after_render_table_thead','_foundingmembership_after_render_table_thead');
function _foundingmembership_after_render_table_thead(){

}
perfex_add_action('foundingmembership_before_render_table_tfoot','_foundingmembership_before_render_table_tfoot');
function _foundingmembership_before_render_table_tfoot(){

}
perfex_add_action('foundingmembership_render_table_tfoot','_foundingmembership_render_table_tfoot');
function _foundingmembership_render_table_tfoot(){

}
perfex_add_action('foundingmembership_after_render_table_tfoot','_foundingmembership_after_render_table_tfoot');
function _foundingmembership_after_render_table_tfoot(){

}
perfex_add_action('foundingmembership_render_table_footer','_foundingmembership_render_table_footer');
function _foundingmembership_render_table_footer($param){
	$setting = $param['setting'];
	$rowData = $param['rowData'];
	$total_rows = $param['total_rows'];

	if($setting['gridtype'] == 'JQGrid') {	?>

		<!-- JQGRID INIT START -->
		<script type="text/javascript" src="<?=assets_url()?>perfex/plugins/jqwidgets/jqx-all.js" charset="UTF-8"></script>
		<script>
		var gridID = "foundingmembershipjqxgrid";
		/*
		 * ***************************************************************
		 * Definitions
		 * ***************************************************************
		 */
		/*
		 * function for get Grid data adapter
		 */
		function getAdapter() {
		    var source ={
		        datatype: "json",
		        datafields: [
		            { name: 'user_id'},
		            { name: 'display_name'},
		            { name: 'commissions'},
		            { name: 'coins'},
		            { name: 'Active'}
		        ],
		        sortcolumn: 'user_id',
		        sortdirection: 'DESC',
		        cache: false,
		        url: '<?php echo base_url();?>foundingmembership/listdata',
		        filter: function(){
		            $("#"+gridID).jqxGrid('updatebounddata', 'filter');
		        },
		        sort: function(){
		         $("#"+gridID).jqxGrid('updatebounddata', 'sort');
		        },
		        root: 'Rows',
		        beforeprocessing: function(data){		
		            if (data != null){
		                source.totalrecords =  data[0].TotalRows;					
		            }
		        }
		    };
		    var dataAdapter = new $.jqx.dataAdapter(source, {
		        loadError: function(xhr, status, error){
		            alert(error);
		        }
		    });
		    return dataAdapter;
		}//End:getAdapter()
			/*
			 * ***************************************************************
			 * Implementaions
			 * ***************************************************************
			 */
			$(document).ready(function () {
			    var editRenderer = function (row, datafield, value, defaultHtml, columnSettings, rowData) {return '<a href="#" class="tooltipT"  title="Click to Edit." style="margin-left:5px;position:relative; top:5px">Edit</a>';}
			    var deleteRenderer = function (row, datafield, value, defaultHtml, columnSettings, rowData) {return '<a href="#"  style="margin-left: 5px;" title="Click to Delete.">Delete</a>';}
			    var statusRenderer = function (row, datafield, value, defaultHtml, columnSettings, rowData) {
			        var str_status = ""; 
			        if(rowData.Active ===  "1"){
			            str_status = 'Deactive';
			            return '<a href="javascript:void(0);" onclick="#" style="margin-left:5px;" class="status" title="Click to Deactivate.">'+str_status+'</a>';
			        }else{
			            str_status = 'Active';
			            return '<a href="javascript:void(0);"  onclick="#" style="margin-left:5px;" class="status" title="Click to Activate.">'+str_status+'</a>';
			        }
			    }
			    $("#"+gridID).jqxGrid({
			        width: '100%',
			        source: getAdapter(),
			        theme: 'default',
			        pageable: true, 
			        
			        pagesize:20,
			        pagesizeoptions: ['20', '40', '60','100'],
			        autoheight: true,
			        sortable: true,
			        altrows: true,
			        enabletooltips: true,
			        filterable: true,
			        showfilterrow: true,
			        virtualmode: true,
			        enablehover: true,
			        selectionmode: 'checkbox',
			        showstatusbar: true,
			        rendergridrows: function(obj){
			            return obj.data;    
			        },
			        renderstatusbar: function (statusbar) {
			            // appends buttons to the status bar.
			            var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
			            var reloadButton = $("<div style='float: left; margin-left: 5px;'><button type='button' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-refresh'></span> Reload</button></div>");
			            var clearButton = $("<div style='float: left; margin-left: 5px;'><button type='button' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-filter'></span>Clear</button></div>");
			            container.append(reloadButton);
			            container.append(clearButton);
			            statusbar.append(container);
			            //reloadButton.jqxButton({ theme: theme, width: 65 });
			            // reload grid data.
			            reloadButton.click(function (event) {
			                //alert("stoper here");
			                $("#"+gridID).jqxGrid({ source: getAdapter() });
			                //$("#"+gridID).jqxGrid('sortby', 'OrderNumber', 'desc');
			            });
			            clearButton.click(function (event) {
			                $("#"+gridID).jqxGrid('clearfilters');
			            });
			        },
			        columns: [
			            { text: 'User ID', datafield: 'user_id', columngroup: 'CustomersDetail',width:'100',filtercondition: 'starts_with'},
			            { text: 'Name', datafield: 'display_name',columngroup: 'CustomersDetail', filtercondition: 'starts_with'},
			            { text: 'Commissions', datafield: 'commissions',columngroup: 'CustomersDetail', filtercondition: 'starts_with'},
			            { text: 'Coins', datafield: 'coins',columngroup: 'CustomersDetail', filtercondition: 'starts_with'},
			            { text: 'Edit',cellsalign: 'center',filterable: false,sortable: false,columngroup: 'Action',cellsrenderer: editRenderer, width:'60',cellsalign: 'center'},
			            { text: 'Delete',cellsalign: 'center',filterable: false,sortable: false,align: 'center',columngroup: 'Action',cellsrenderer: deleteRenderer, width:'60',cellsalign: 'center'},
			            { text: 'Status',cellsalign: 'center',filterable: false,sortable: false ,align: 'center',columngroup: 'Action',cellsrenderer: statusRenderer, width:'100',cellsalign: 'center'},
			        ],
			        columngroups: [
			            { text: 'Customer Detail', align: 'center', name: 'CustomersDetail'},
			            { text: 'Actions', align: 'center', name: 'Action' }
			        ]
			    });
			});
		</script>
			<!--End:JQGRID INIT() -->

		<?php } elseif ($setting['gridtype'] == 'Datatable') { ?>

			<!--Start:DATATABLE INIT() -->
		<script>
			var gridtable;
				gridtable = $('#foundingmembershipTable').DataTable(
					{
						"dom": '<fi<t><lp>>',
						"processing": true,
						"serverSide": true,
						"autoWidth": false,
						"lengthMenu": [[10, 25, 50, 100, 0], [10, 25, 50, 100, "All"]],
						"deferLoading":<?=$total_rows?>,
						"stateSave": true,
					    "language": 'Hang on. Waiting for response...',
						"searchDelay": 350,
						"ajax":
						{
							"url": "<?php echo base_url();?>foundingmembership/listdata",
							"type": "POST",
							"data": function ( data )
							{
								data.filtertype = $('#filtertype').val();
							},
							"dataSrc": function ( json )
							{
								$('#total_all_pages').html(json.total_all_pages);
								$('#total_current_page').html(json.total_current_page);
								for ( var i=0, ien=json.data.length ; i<ien ; i++ )
								{
									json.data[i][0] = '<a href=#>'+json.data[i][0]+'</a>';
								}
								return json.data;
							}
						},
				       "createdRow": function ( row, data, index ) 
				       				 {
							            if ( data[5].replace(/[\$,]/g, '') * 1 < 0 ) 
							            {
							                $('td', row).eq(5).addClass('highlight');
							            }
								     }
						// initComplete: function () {
						// this.api().columns().every( function () {
						//     var column = this;
						//     if (column[0] == 2)
						//     {
						//     var select = $('filtertype')
						//         .appendTo( $(column.footer()).empty() )
						//      // column.data().unique().sort().each( function ( d, j ) {
						//      //     typeselect.append( '<option value="'+d+'">'+d+'</option>' )
						//      // } );

						// 	}
						// } );
						//}
						// "initComplete": function () {
						// 	   var api = this.api();

						// 	         // Initialize Typeahead plug-in
						// 	         $('.dataTables_filter input[type="search"]', api.table().container())
						// 	            .typeahead({
						// 	               source: function(query, process){
						// 	                  $.getJSON('<?=base_url()?>ajax.php', { q: query }, function (data){
						// 	                     return process(data);
						// 	                  });
						// 	               },
						// 	               afterSelect: function(value){
						// 	                  api.search(value).draw();
						// 	               }
						// 	            }
						// 	         );
						// 	}					
					});

				var typeselect = $('#filtertype').on( 'change', function ()
					{
						var val = $(this).val();
						gridtable.ajax.reload(null,false);
						//gridtable.columns(2).search(val ? '^'+val+'$' : '',true, false).draw();
					} );


				$(document).ready(function()
				{
					$('.tips').tooltip();

					$('input[type="checkbox"],input[type="radio"]').iCheck(
						{
							checkboxClass: 'icheckbox_square-green',
							radioClass: 'iradio_square-green',
						});
					$('#foundingmembershipTable .checkall').on('ifChecked',function()
						{
							$('#foundingmembershipTable input[type="checkbox"]').iCheck('check');
						});
					$('#foundingmembershipTable .checkall').on('ifUnchecked',function()
						{
							$('#foundingmembershipTable input[type="checkbox"]').iCheck('uncheck');
						});

					$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
					$('#foundingmembershipPaginate .pagination li a').click(function()
						{
							var url = $(this).attr('href');
							if(url != '')
							{
								reloadData('#foundingmembership',url);
							}
							return false ;
						});
					loadControlvalues();

				});

				function filterList()
				{
					$('#filter-form').submit();
				}
				function dosearchbutton_click()
				{
					dofilterList();
				}
				function dofilterbutton_click()
				{
					$('#panel-filter').toggle('slow');
				}

		</script>

	<?php } 

	}  ?>