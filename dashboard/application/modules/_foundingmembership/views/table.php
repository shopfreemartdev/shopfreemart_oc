<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');?>

<?php perfex_do_action('foundingmembership_render_table_header'); ?>
	<div class="member-page-header">
	<?php perfex_do_action('foundingmembership_before_render_table_pagehead'); ?>
	<?php $this->load->view('foundingmembership/pagehead');?>
	<?php perfex_do_action('foundingmembership_after_render_table_pagehead'); ?>
	</div>
		
<?php echo $this->session->flashdata('message');?>
	
<div id="ajax"><?=$show_message?></div>

<?php 
$setting['gridtype'] = 'Datatable';
if($setting['gridtype'] == 'JQGrid') {	?>
	<!-- IF JQGRID -->
    <link rel="stylesheet" href="<?=assets_url()?>perfex/plugins/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="<?=assets_url()?>perfex/plugins/jqwidgets/styles/jqx.classic.css" type="text/css" />	
	<div id='foundingmembershipjqxWidget'">
		<div class="grid" id="foundingmembershipjqxgrid"></div>
	</div>

<?php } elseif ($setting['gridtype'] == 'Datatable') { ?>
	
	<!-- IF Datatable -->
	<?php usort($tableGrid, "SiteHelpers::_sort"); ?>
	<?php perfex_do_action('foundingmembership_before_render_table_toolbar'); ?>
	<?php if($setting['gridsettings']['gs_toolbar']) : ?>
	<?php perfex_do_action('foundingmembership_render_table_toolbar'); ?>
	<?php endif;?>
	<?php perfex_do_action('foundingmembership_after_render_table_toolbar'); ?>

	<?php if($setting['gridsettings']['gs_portlet']) : ?>
	<div class="sbox">
		<div class="sbox-title">

			<h4 class="pull-left">
				<span>
				<i class="fa fa-table" style="font-size: 18px !important"></i>
				<?php echo $pageTitle ;?>&nbsp;
				<small>
					<?php echo $pageNote ;?>
				</small>
				</span>
			</h4>
			<div class="btn-group pull-right">				

				<a href="javascript:void(0)" class="" 
				onclick="reloadData('#foundingmembership','foundingmembership/data')"  title="Reload Data"><i class="fa fa-refresh" style="font-size: 17px; margin-top: 5px;" ></i></a>	
				<?php if(is_staff_logged_in() && perfex_is_admin()) : ?>	
						<a href="<?php echo base_url('cms/module/rebuild/'.$pageModule.'?rurl='.$pageModule) ?>"  title="Rebuild">
						<i class="fa fa-cog" style="font-size: 17px; margin-top: 5px;" ></i>&nbsp;</a>				
						<a href="<?php echo base_url('cms/module/config/'.$pageModule) ?>" title="Configuration">
						<i class="fa fa-wrench" style="font-size: 17px; margin-top: 5px;"></i>&nbsp;</a>
				<?php endif; ?>			
			</div>

		</div>
		<div class="sbox-content">
			<?php endif;?>
			<form
				action='<?php echo base_url('foundingmembership/destroy') ?>' class='form-horizontal' id ='foundingmembershipForm' method="post" <?php echo $setting['gridsettings']['gs_css']; ?> >

				<div class="panel panel-default panel-table">
			
				<!-- gs_filterbar -->
				<?php if($setting['gridsettings']['gs_filterbar']) {
					echo $filterbar;
				} ?>

				<!-- Grid Table Row -->
					<div class="table-responsive">

							<table class="display dataTable table table-striped table-bordered table-hover table-checkable table-responsive" id="foundingmembershipTable" >
									<?php if(empty($rowData)) : ?>
										<tr>
											<div class="info-box">
												<center>
												<h4><?=lang('No Records Found.')?></h4>
												</center>
											</div>
										</tr>

									<?php else: ?>	
								<?php perfex_do_action('foundingmembership_before_render_table_thead'); ?>
								<thead>
									<tr>
										<?php if($setting['gridsettings']['gs_counter']) { ?>
										<th>
											No
										</th>
										<?php } ?>

										<?php if($setting['gridsettings']['gs_checkbox']) { ?>
										<th>
											<input type="checkbox" class="checkall" />
										</th>
										<?php } ?>

										<?php if($setting['view-method']=='expand') { ?>
										<th>
										</th> <?php } ?>
										<?php foreach ($tableGrid as $k => $t) : ?>
										<?php if($t['view'] =='1'): ?>
										<th>
											<?php echo SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array()))  ?>
										</th>
										<?php endif; ?>
										<?php endforeach; ?>

										<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
										<th>
											Action
										</th>
										<?php } ?>

									</tr>
								</thead>
								<?php perfex_do_action('foundingmembership_after_render_table_thead'); ?>
								<tbody>
								
								<?php if($access['is_add'] =='1' && $setting['inline']=='true'): ?>

										<?php if($setting['gridsettings']['gs_counter']) { ?>
										<td>
											#
										</td>
										<?php } ?>

										<?php if($setting['gridsettings']['gs_checkbox']) { ?>
										<th>
										</th>
										<?php } ?>

										<?php if($setting['view-method']=='expand') { ?>
										<td>
										</td> <?php } ?>
										<?php foreach ($tableGrid as $t) :
										if($t['view'] =='1') : ?>
										<td  <?php echo (isset($t['width'])? 'width='.$t['width'] : '') ?> data-form="<?php echo $t['field'];?>" data-form-type="<?php echo  AjaxHelpers::inlineFormType($t['field'],$tableForm);?>">
											<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>
										</td>
										<?php endif;
										endforeach ?>

										<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
										<td  width="40" align="center" >

											<button type="button"  class=" btn btn-xs btn-info" rel="#<?php echo $pageModule ;?>Form"
							onclick="ajaxInlineSave('#<?php echo $pageModule ;?>','<?php echo base_url('foundingmembership/quicksave') ;?>','<?php echo base_url('foundingmembership/data?md=') ;?>')">
												<i class="fa fa-save">
												</i>
											</button>
										</td>

									<?php } ?>

									</tr>
										<?php endif;?>

										<?php foreach ( $rowData as $i => $row ) :
										$id = $row->id;
										?>
										<?php if($setting['gridsettings']['gs_counter']) { ?>
										<td width="50">
											<?php echo ($i+1+$page) ?>
										</td>
										<?php } ?>

										<?php if($setting['gridsettings']['gs_checkbox']) { ?>
										<td width="50">
											<input type="checkbox" class="ids" name="id[]" value="<?php echo $row->id ?>" />
										</td>
										<?php } ?>

										 <?php if($setting['view-method']=='expand'): ?>
										<td>
											<a href="javascript:void(0)" class="expandable" rel="#row-<?php echo $row->id ;?>" data-url="<?php echo base_url('foundingmembership/show/'.$id) ;?>">
												<i class="fa fa-plus " >
												</i>
											</a>
										</td>
										<?php endif;?>
										<?php foreach ( $tableGrid as $j => $field ) : ?>
										<?php if($field['view'] =='1'):
										$conn = (isset($field['conn']) ? $field['conn'] : array() );
										$value = AjaxHelpers::gridFormater($row->$field['field'], $row , $field['attribute'],$conn);

										?>
										<td align="<?php echo $field['align'];?>" data-values="<?php echo $row->$field['field'] ;?>" data-field="<?php echo  $field['field'] ;?>" data-format="<?php echo htmlentities($value);?>">
											<?php echo  $value;?>
										</td>
										<?php endif; ?>
										<?php endforeach; ?>

										<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
										<td data-values="action" data-key="<?php echo $row->id ;?> " >
											<?php 
												echo perfex_do_action('foundingmembership_render_table_row_action_buttons',array( 'row' => $row,'access' => $access,'setting' => $setting)); 
											?>
										</td>
										<?php } ?>

									</tr>
										<?php if($setting['view-method']=='expand'): ?>
																							<tr style="display:none" class="expanded" id="row-<?php echo $row->id;?>">

										<?php if($setting['gridsettings']['gs_counter']) { ?>
										<td class="number">
										</td>
										<?php } ?>

										<td>
										</td>
										<td>
										</td>
										<td colspan="<?php echo $colspan;?>" class="data">
										</td>
										<td>
										</td>
									</tr>
										<?php endif;?>
										<?php endforeach; ?>

								</tbody>
									<?php endif;?>							
								<?php perfex_do_action('foundingmembership_before_render_table_tfoot'); ?>
								<tfoot>
								<?php perfex_do_action('foundingmembership_render_table_tfoot'); ?>
								</tfoot>
								<?php perfex_do_action('foundingmembership_after_render_table_tfoot'); ?>
							</table>

					</div>

					<?php if($setting['gridsettings']['gs_footer']) : ?>
					<?php $this->load->view('ajaxfooter'); ?>
				<?php endif; ?>				
				
				</div>
			</form>
			
			<?php if($setting['gridsettings']['gs_portlet']) : ?>
		</div>
	</div>

	<?php endif;?>

	<?php if($setting['inline'] =='true')  $this->load->view('cms/module/utility/inlinegrid') ;?>
	<?php if($setting['view-method'] =='expand')  $this->load->view('cms/module/utility/extendgrid') ;?>

<?php }	?>

<?php perfex_do_action('foundingmembership_render_table_footer',array('setting'=>$setting,'rowData' => $rowData,'total_rows'=>$total_rows)); ?>



  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->