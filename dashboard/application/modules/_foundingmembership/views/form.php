	<?php if($setting['form-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('foundingmembership/pagehead');?>
	</div>
		
			
	
	<div class="sbox">

	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo  $pageNote ;?></small> 

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#foundingmembership')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		 <form action="<?php echo base_url('foundingmembership/save/'.$row['id']); ?>" class='form-horizontal'  id="foundingmembershipFormAjax"
		 parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 
		 
<div class="col-md-12">
						<fieldset><legend> Founding Membership</legend>
									
								  <div class="form-group  " >
									<label for="Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <textarea name='id' rows='2' id='id' class='form-control '  
				            ><?php echo $row['id'] ;?></textarea>
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Lastdate" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Lastdate', (isset($fields['lastdate']['language'])? $fields['lastdate']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <textarea name='lastdate' rows='2' id='lastdate' class='form-control '  
				           Disabled ><?php echo $row['lastdate'] ;?></textarea>
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Payment" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Payment', (isset($fields['payment']['language'])? $fields['payment']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <textarea name='payment' rows='2' id='payment' class='form-control '  
				            ><?php echo $row['payment'] ;?></textarea>
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Balance" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Balance', (isset($fields['balance']['language'])? $fields['balance']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <textarea name='balance' rows='2' id='balance' class='form-control '  
				            ><?php echo $row['balance'] ;?></textarea>
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Status Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Status Id', (isset($fields['status_id']['language'])? $fields['status_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <textarea name='status_id' rows='2' id='status_id' class='form-control '  
				            ><?php echo $row['status_id'] ;?></textarea>
									  <i> <small></small></i>
									 </div> 
								  </div> </fieldset>
			</div>
			
			
		
			<div style="clear:both"></div>	
				
 		<div class="toolbar-line text-center">		
			<input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo lang('Submit'); ?>" />
			<a href="javascript:void(0)" class="btn-sm btn btn-warning" data-dismiss="modal" aria-hidden="true" onclick="ajaxViewClose('#foundingmembership')"><?php echo lang('Cancel'); ?></a>
 		</div>
			  		
		</form>

	</div>	
		<?php foreach($subgrid as $md) : ?>
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid"></div>
			</div>	
		</div>
		<?php endforeach;?>	
	
	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo base_url($md['module'].'/detailview/form?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid' ).html( data );
	});		
<?php endforeach ?>
});
</script>				 
<script type="text/javascript">
$(document).ready(function() { 
	 
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('.markItUp').markItUp(mySettings );	

	var form = $('#foundingmembershipFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});	
 	 
});

function showRequest()
{
	$('.formLoading').show();	
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		if(data.action =='submit')
		{
			ajaxViewClose('#foundingmembership');	
		}	
		
		ajaxFilter('#foundingmembership','foundingmembership/data');
		notyMessage(data.message);		
	} else {	
		var n = noty({				
			text: data.message,
			type: 'error',
			layout: 'topRight',
		});	
		return false;
	}	
}	
</script>		 


  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->