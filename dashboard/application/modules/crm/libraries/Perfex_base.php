<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

        
class perfex_Base
{
    private $options = array();
    // Quick actions aide
    private $quick_actions = array();
    // Instance CI
    private $_instance;
    // Dynamic options
    private $dynamic_options = array('next_invoice_number', 'next_estimate_number');
    // Show or hide setup menu
    private $show_setup_menu = true;
    // Currently reminders
    private $available_reminders = array('customer', 'lead', 'estimate', 'invoice', 'proposal', 'expense');
    // Tables where currency id is used
    private $tables_with_currency = array();
    // Media folder
    private $media_folder;
    // Available languages
    private $available_languages = array();
    

    function __construct()
    {
        


        $this->_instance =& get_instance();
        $options = $this->_instance->db->get('main_shopfreemart.sfm_crm_options')->result_array();
        // Loop the options and store them in a array to prevent fetching again and again from database
        foreach ($options as $option) {
            $this->options[$option['name']] = $option['value'];
        }

        $this->tables_with_currency = perfex_do_action('tables_with_currency',array(
            array(
                'table' => 'main_crm.crm_invoices',
                'field' => 'currency'
            ),
            array(
                'table' => 'main_crm.crm_expenses',
                'field' => 'currency'
            ),
            array(
                'table' => 'main_crm.crm_proposals',
                'field' => 'currency'
            ),
            array(
                'table' => 'main_crm.crm_estimates',
                'field' => 'currency'
            ),
            array(
                'table' => 'main_shopfreemart.sfm_uap_affiliates',
                'field' => 'default_currency'
            )
        ));

        $this->media_folder = perfex_do_action('before_set_media_folder', 'media');
        
        foreach (list_folders(CRM_MODULE_PATH . 'language') as $language) {
            if(is_dir(CRM_MODULE_PATH.'language/'.$language)){
                array_push($this->available_languages, $language);
            }
        }

        perfex_do_action('app_base_after_construct_action');
    }

function load_classes()
    {

       // $this->load->library('database');
        $this->_instance->load->helper('crm/perfex_constants');       
        $this->_instance->load->helper('crm/perfex_files');
        $this->_instance->load->helper('crm/perfex_action_hooks');                
        
        //$this->_instance->load->library('crm/user_agent');
       // $this->_instance->load->library('crm/encryption');
        $this->_instance->load->library('email');
        $this->_instance->load->library('crm/encoding_lib');

//        $gateways = list_files(CRM_MODULE_PATH.'libraries/gateways');
//        foreach($gateways as $gateway){
//            $pathinfo =  pathinfo($gateway);
//            // Check if file is .php and do not starts with .dot
//            // Offen happens Mac os user to have underscore prefixed files while unzipping the zip file.
//            if($pathinfo['extension'] == 'php' && 0 !== strpos($gateway, '.')){
//                $this->load->library(CRM_MODULE_PATH.'libraries/gateways/'.strtolower($pathinfo['filename']));
//            }
//        }

        $this->_instance->load->language('crm/english');

        $this->_instance->load->helper('crm/perfex_general');
        $this->_instance->load->helper('crm/perfex_misc');
        $this->_instance->load->helper('crm/perfex_func');
        $this->_instance->load->helper('crm/perfex_custom_fields');
        $this->_instance->load->helper('crm/perfex_merge_fields');
        $this->_instance->load->helper('crm/perfex_html');
        $this->_instance->load->helper('crm/perfex_db');
        $this->_instance->load->helper('crm/perfex_upload');
        $this->_instance->load->helper('crm/perfex_sales');
        $this->_instance->load->helper('crm/perfex_themes');
        $this->_instance->load->helper('crm/perfex_theme_style');

        // $this->_instance->load->model('crm/payment_modes_model');
        // $this->_instance->load->model('crm/perfex_settings_model');
        // $this->_instance->load->model('crm/misc_model');
        // $this->_instance->load->model('crm/roles_model');
        // $this->_instance->load->model('crm/staff_model');
        // $this->_instance->load->model('crm/clients_model');
        // $this->_instance->load->model('crm/projects_model');
        // $this->_instance->load->model('crm/tasks_model');

    }

    private function init()
    {

       $language = load_admin_language();
        $this->load->model('authentication_model');
        $this->authentication_model->autologin();

        if (!is_staff_logged_in()) {
            if (strpos(current_full_url(), 'authentication/admin') === FALSE) {
                $this->session->set_userdata(array(
                    'red_url' => $this->uri->uri_string()
                ));
            }
           // redirect(ci_site_url('authentication/admin'));
        }

        // In case staff have setup logged in as client - This is important dont change it
        // $this->session->unset_userdata('client_user_id');
        // $this->session->unset_userdata('client_logged_in');
        // $this->session->unset_userdata('logged_in_as_client');

        // Check for just updates message
        perfex_add_action('before_start_render_content', 'show_just_updated_message');

        // Do not check on ajax requests
        if(!$this->input->is_ajax_request()){
            if (ENVIRONMENT == 'production' && perfex_is_admin()) {
                if ($this->config->item('encryption_key') === '') {
                    die('<h1>Encryption key not sent in application/config/config.php</h1>For more info visit <a href="http://www.perfexcrm.com/knowledgebase/encryption-key/">Encryption key explained</a> FAQ3');
                } else if (strlen($this->config->item('encryption_key')) != 32) {
                    die('<h1>Encryption key length should be 32 charachters</h1>For more info visit <a href="http://www.perfexcrm.com/knowledgebase/encryption-key/">Encryption key explained</a>');
                }
            }

            //perfex_add_action('before_start_render_content', 'show_development_mode_message');
            // Check if cron is required to be setup for some features
            perfex_add_action('before_start_render_content', 'is_cron_setup_required');

        }

        $this->load->model('staff_model');
        $this->init_quick_actions_links();

        if (is_mobile()) {
            $this->session->set_userdata(array(
                'is_mobile' => true
            ));
        } else {
            $this->session->unset_userdata('is_mobile');
        }
    
        $auto_loaded_vars = array(
            '_staff' => $this->staff_model->get(get_staff_user_id()),
            '_notifications' => $this->misc_model->get_user_notifications(false),
            '_quick_actions' => $this->perfex_base->get_quick_actions_links(),
            '_started_timers' => $this->misc_model->get_staff_started_timers(),
            'google_api_key' => perfex_get_option('google_api_key'),
            'total_pages_newsfeed' => total_rows('main_crm.crm_posts') / 10,
            'locale' => get_locale_key($language),
            'tinymce_lang' => get_tinymce_language(get_locale_key($language)),
            '_pinned_projects' => $this->get_pinned_projects(),
            'total_undismissed_announcements' => $this->get_total_undismissed_announcements(),
            'current_version' => APP_VERSION,
            'tasks_filter_assignees' => $this->get_tasks_distinct_assignees(),
            'task_statuses' => $this->tasks_model->get_statuses(),
        );
       // ld($auto_loaded_vars);

        $auto_loaded_vars = perfex_do_action('before_set_auto_loaded_vars_admin_area', $auto_loaded_vars);
        $this->load->vars($auto_loaded_vars);
        
        $this->load->library('template');

    }
     /* Return all available languages in the application/language folder
     * @return array
     */
    public function get_available_languages()
    {
        return $this->available_languages;
    }
    /**
     * Function that will parse table data from the tables folder for amin area
     * @param  string $table  table filename
     * @param  array  $params additional params
     * @return void
     */
    public function get_table_data($table, $params = array())
    {
        $hook_data = perfex_do_action('before_render_table_data', array(
            'table' => $table,
            'params' => $params
        ));
        foreach ($hook_data['params'] as $key => $val) {
            $$key = $val;
        }
        $table = $hook_data['table'];
        if (file_exists(CRM_MODULE_PATH . 'views/admin/tables/my_' . $table . '.php')) {
            include_once(CRM_MODULE_PATH . 'views/admin/tables/my_' . $table . '.php');
        } else {
            include_once(CRM_MODULE_PATH . 'views/admin/tables/' . $table . '.php');
        }
        echo json_encode($output);
        die;
    }
    /**
     * All available reminders keys for the features
     * @return array
     */
    public function get_available_reminders_keys()
    {
        return $this->available_reminders;
    }
    /**
     * Get all db options
     * @return array
     */
    public function perfex_get_options()
    {
        return $this->options;
    }
    /**
     * Function that gets option based on passed name
     * @param  string $name
     * @return string
     */
    public function perfex_get_option($name)
    {
        if ($name == 'number_padding_invoice_and_estimate') {
            $name = 'number_padding_prefixes';
        }

        $name = trim($name);

        if (isset($this->options[$name])) {
            if (in_array($name, $this->dynamic_options)) {
                $this->_instance->db->where('name', $name);
                return $this->_instance->db->get('main_shopfreemart.sfm_crm_options')->row()->value;
            } else {
                return $this->options[$name];
            }
        }
        return '';
    }
    /**
     * Add new quick action data
     * @param array $item
     */
    public function add_quick_actions_link($item = array())
    {
        $this->quick_actions[] = $item;
    }
    /**
     * Quick actions data set from admin_controller.php
     * @return array
     */
    public function get_quick_actions_links()
    {
        $this->quick_actions = perfex_do_action('before_build_quick_actions_links', $this->quick_actions);
        return $this->quick_actions;
    }
    /**
     * Predefined contact permission
     * @return array
     */
    public function get_contact_permissions()
    {
        $permissions = array(
            array(
                'id' => 1,
                'name' => _l('customer_permission_invoice'),
                'short_name' => 'invoices'
            ),
            array(
                'id' => 2,
                'name' => _l('customer_permission_estimate'),
                'short_name' => 'estimates'
            ),
            array(
                'id' => 3,
                'name' => _l('customer_permission_contract'),
                'short_name' => 'contracts'
            ),
            array(
                'id' => 4,
                'name' => _l('customer_permission_proposal'),
                'short_name' => 'proposals'
            ),
            array(
                'id' => 5,
                'name' => _l('customer_permission_support'),
                'short_name' => 'support'
            ),
            array(
                'id' => 6,
                'name' => _l('customer_permission_projects'),
                'short_name' => 'projects'
            )
        );
        return perfex_do_action('get_contact_permissions', $permissions);
    }
    /**
     * Aside.php will set the menu visibility here based on few conditions
     * @param int $total_setup_menu_items total setup menu items shown to the user
     */
    public function set_setup_menu_visibility($total_setup_menu_items)
    {
        if ($total_setup_menu_items == 0) {
            $this->show_setup_menu = false;
        } else {
            $this->show_setup_menu = true;
        }
    }
    /**
     * Check if should the script show the setup menu or not
     * @return boolean
     */
    public function show_setup_menu()
    {
        return perfex_do_action('show_setup_menu',$this->show_setup_menu);
    }
    /**
     * Return tables that currency id is used
     * @return array
     */
    public function get_tables_with_currency()
    {
        return perfex_do_action('tables_with_currencies',$this->tables_with_currency);
    }
    /**
     * Return the media folder name
     * @return string
     */
    public function get_media_folder()
    {
        return perfex_do_action('get_media_folder',$this->media_folder);
    }
}
