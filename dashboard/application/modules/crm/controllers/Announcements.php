<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Announcements extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Adminannouncements_model');
    }
    /* List all announcements */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('announcements');
        }
        $data['title'] = lang('announcements');
        $this->load->view('crm/admin/announcements/manage', $data);
    }
    /* Edit announcement or add new if passed id */
    public function announcement($id = '')
    {
        if (!perfex_is_admin()) {
            access_denied('Announcement');
        }
        if ($this->input->post()) {
            $data            = $this->input->post();
            $data['message'] = $this->input->post('message', FALSE);
            if ($id == '') {
                $id = $this->Adminannouncements_model->add($data);
                if ($id) {
                    set_alert('success', lang('added_successfuly', lang('announcement')));
                    redirect(perfex_admin_url('announcements/view/' . $id));
                }
            } else {
                $success = $this->Adminannouncements_model->update($data, $id);
                if ($success) {
                    set_alert('success', lang('updated_successfuly', lang('announcement')));
                }
                redirect(perfex_admin_url('announcements/view/' . $id));
            }
        }
        if ($id == '') {
            $title = lang('add_new', lang('announcement_lowercase'));
        } else {
            $data['announcement'] = $this->Adminannouncements_model->get($id);
            $title                = lang('edit', lang('announcement_lowercase'));
        }
        $data['title'] = $title;
        $this->load->view('crm/admin/announcements/announcement', $data);
    }

    public function view($id)
    {
        if (is_staff_member()) {
            $announcement = $this->Adminannouncements_model->get($id);
            if (!$announcement) {
                blank_page(lang('announcement_not_found'));
            }
            $data['announcement']         = $announcement;
            $data['recent_announcements'] = $this->Adminannouncements_model->get('', array(
                'announcementid !=' => $id
            ), 4);
            $data['title']                = $announcement->name;
            $this->load->view('crm/admin/announcements/view', $data);
        }
    }
    /* Delete announcement from database */
    public function delete($id)
    {
        if (!$id) {
            redirect(perfex_admin_url('announcements'));
        }
        if (!perfex_is_admin()) {
            access_denied('Announcement');
        }
        $response = $this->Adminannouncements_model->delete($id);
        if ($response == true) {
            set_alert('success', lang('deleted', lang('announcement')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('announcement_lowercase')));
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
}
