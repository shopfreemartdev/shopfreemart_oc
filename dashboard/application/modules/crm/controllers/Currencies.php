<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Currencies extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('currencies_model');
        if (!perfex_is_admin()) {
            access_denied('Currencies');
        }
    }
    /* List all currencies */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('currencies');
        }
        $data['title'] = lang('currencies');
        $this->load->view('crm/admin/currencies/manage', $data);
    }
    /* Update currency or add new / ajax */
    public function manage()
    {
        if ($this->input->post()) {
            $data = $this->input->post();
            if ($data['currencyid'] == '') {
                $success = $this->currencies_model->add($data);
                $message = '';
                if ($success == true) {
                    $message = lang('added_successfuly', lang('currency'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
            } else {
                $success = $this->currencies_model->edit($data);
                $message = '';
                if ($success == true) {
                    $message = lang('updated_successfuly', lang('currency'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
            }
        }
    }
    /* Make currency your base currency */
    public function make_base_currency($id)
    {
        if (!$id) {
            redirect(perfex_admin_url('currencies'));
        }
        $response = $this->currencies_model->make_base_currency($id);
        if (is_array($response) && isset($response['has_transactions_currency'])) {
            set_alert('danger', lang('has_transactions_currency_base_change'));
        } else if ($response == true) {
            set_alert('success', lang('base_currency_set'));
        }
        redirect(perfex_admin_url('currencies'));
    }
    /* Delete currency from database */
    public function delete($id)
    {
        if (!$id) {
            redirect(perfex_admin_url('currencies'));
        }
        $response = $this->currencies_model->delete($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', lang('is_referenced', lang('currency_lowercase')));
        } else if (is_array($response) && isset($response['is_default'])) {
            set_alert('warning', lang('cant_delete_base_currency'));
        } else if ($response == true) {
            set_alert('success', lang('deleted', lang('currency')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('currency_lowercase')));
        }
        redirect(perfex_admin_url('currencies'));
    }
    /* Get symbol by currency id passed */
    public function get_currency_symbol($id)
    {
        if ($this->input->is_ajax_request()) {
            echo json_encode(array(
                'symbol' => $this->currencies_model->get_currency_symbol($id)
            ));
        }
    }
}
