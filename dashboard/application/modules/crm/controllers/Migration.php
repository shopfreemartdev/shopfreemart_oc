<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    public function make()
    {
        $this->load->config('migration');
        if ($this->config->item('migration_enabled') == true) {
            if (!$this->input->get('old_base_url')) {
                echo '<h1>You need to pass old base url in the url like: ' . ci_site_url('migration/make?old_base_url=http://myoldbaseurl.com/') . '</h1>';
                die;
            }

            $old_url = $this->input->get('old_base_url');
            $new_url = $this->config->item('base_url');
            if (!endsWith($old_url, '/')) {
                $old_url = $old_url . '/';
            }

            $tables       = array(
                array(
                    'table' => 'main_shopfreemart.sfm_crm_adminnotifications',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_adminnotifications',
                    'field' => 'additional_data'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_notes',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_emailtemplates',
                    'field' => 'message'
                ),
                array(
                    'table' => 'main_crm.crm_posts',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_crm.crm_postcomments',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_options',
                    'field' => 'value'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_staff',
                    'field' => 'email_signature'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_predifinedreplies',
                    'field' => 'message'
                ),
                array(
                    'table' => 'main_crm.crm_projectdiscussioncomments',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_crm.crm_projectdiscussions',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_projectnotes',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_crm.crm_projects',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_reminders',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_stafftasks',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_stafftaskcomments',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_crm.crm_surveys',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_surveys',
                    'field' => 'viewdescription'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_ticketreplies',
                    'field' => 'message'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_tickets',
                    'field' => 'message'
                ),
                array(
                    'table' => 'main_crm.crm_todoitems',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_proposalcomments',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_crm.crm_proposals',
                    'field' => 'content'
                ),
                array(
                    'table' => 'main_crm.crm_leadactivitylog',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_knowledgebasegroups',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_shopfreemart.sfm_crm_knowledgebase',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_invoices',
                    'field' => 'terms'
                ),
                array(
                    'table' => 'main_crm.crm_invoices',
                    'field' => 'clientnote'
                ),
                array(
                    'table' => 'main_crm.crm_invoices',
                    'field' => 'adminnote'
                ),
                array(
                    'table' => 'main_crm.crm_salesactivity',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_salesactivity',
                    'field' => 'additional_data'
                ),
                array(
                    'table' => 'main_crm.crm_estimates',
                    'field' => 'terms'
                ),
                array(
                    'table' => 'main_crm.crm_estimates',
                    'field' => 'clientnote'
                ),
                array(
                    'table' => 'main_crm.crm_estimates',
                    'field' => 'adminnote'
                ),
                array(
                    'table' => 'main_crm.crm_goals',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_contracts',
                    'field' => 'description'
                ),
                array(
                    'table' => 'main_crm.crm_contracts',
                    'field' => 'content'
                )
            );
            $affectedRows = 0;
            foreach($tables as $t){
                $this->db->query('UPDATE `'.$t['table'].'` SET `'.$t['field'].'` = replace('.$t['field'].', "' . $old_url . '", "' . $new_url . '")');
            $affectedRows += $this->db->affected_rows();
            }
            echo '<h1>Total links replaced: ' . $affectedRows . '</h1>';
        } else {
            echo '<h1>Set migration_enabled to TRUE in application/config/migration.php</h1>';
        }
    }
}
