<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Goals extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('goals_model');
    }
    /* List all announcements */
    public function index()
    {
        if (!has_permission('goals', '', 'view')) {
            access_denied('goals');
        }
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('goals');
        }
        $data['circle_progress_asset'] = true;
        $data['title']                 = lang('goals_tracking');
        $this->load->view('crm/admin/goals/manage', $data);
    }
    public function goal($id = '')
    {
        if (!has_permission('goals', '', 'view')) {
            access_denied('goals');
        }
        if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('goals', '', 'create')) {
                    access_denied('goals');
                }
                $id = $this->goals_model->add($this->input->post());
                if ($id) {
                    set_alert('success', lang('added_successfuly', lang('goal')));
                    redirect(perfex_admin_url('goals/goal/' . $id));
                }
            } else {
                if (!has_permission('goals', '', 'edit')) {
                    access_denied('goals');
                }
                $success = $this->goals_model->update($this->input->post(), $id);
                if ($success) {
                    set_alert('success', lang('updated_successfuly', lang('goal')));
                }
                redirect(perfex_admin_url('goals/goal/' . $id));
            }
        }
        if ($id == '') {
            $title = lang('add_new', lang('goal_lowercase'));
        } else {
            $data['goal']        = $this->goals_model->get($id);
            $data['achievement'] = $this->goals_model->calculate_goal_achievement($id);
            $title               = lang('edit', lang('goal_lowercase'));
        }
        $this->load->model('contracts_model');
        $data['contract_types']        = $this->contracts_model->get_contract_types();
        $data['title']                 = $title;
        $data['circle_progress_asset'] = true;
        $this->load->view('crm/admin/goals/goal', $data);
    }
    /* Delete announcement from database */
    public function delete($id)
    {
        if (!has_permission('goals', '', 'delete')) {
            access_denied('goals');
        }
        if (!$id) {
            redirect(perfex_admin_url('goals'));
        }
        $response = $this->goals_model->delete($id);
        if ($response == true) {
            set_alert('success', lang('deleted', lang('goal')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('goal_lowercase')));
        }
        redirect(perfex_admin_url('goals'));
    }
    public function notify($id, $notify_type)
    {
        if (!has_permission('goals', '', 'edit') && !has_permission('goals', '', 'create')) {
            access_denied('goals');
        }
        if (!$id) {
            redirect(perfex_admin_url('goals'));
        }
        $success = $this->goals_model->notify_staff_members($id, $notify_type);
        if ($success) {
            set_alert('success', lang('goal_notify_staff_notified_manualy_success'));
        } else {
            set_alert('warning', lang('goal_notify_staff_notified_manualy_fail'));
        }
        redirect(perfex_admin_url('goals/goal/' . $id));
    }
}
