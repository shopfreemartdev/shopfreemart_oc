<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Staff extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    /* List all staff members */
    public function index()
    {
        if (!has_permission('staff', '', 'view')) {
            access_denied('staff');
        }
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('staff');
        }
        $data['staff_members'] = $this->staff_model->get('',1);
        $data['title'] = lang('staff_members');
        //log staff activity - Manage staff opened
        $activitytype = 29; // Open Manage Staff
        $staffid = get_staff_user_id();
        crmlogActivity('Staff Opened Manage Staff module', $activitytype, $staffid);
        $this->load->view('crm/admin/staff/manage', $data);
    }
    /* Add new staff member or edit existing */
    public function member($id = '')
    {
        perfex_do_action('staff_member_edit_view_profile',$id);

        $this->load->model('departments_model');
        if ($this->input->post()) {
            $data = $this->input->post();
            // Dont do XSS clean here.
            $data['email_signature'] = $this->input->post('email_signature',FALSE);

            if ($id == '') {
                if (!has_permission('staff', '', 'create')) {
                    access_denied('staff');
                }
                $id = $this->staff_model->add($data);
                if ($id) {
                    handle_staff_profile_image_upload($id);
                    set_alert('success', lang('added_successfuly', lang('staff_member')));
                    //log staff activity - Manage staff add
                    $activitytype = 37; // Add staff
                    $staffid = get_staff_user_id();
                    crmlogActivity('Staff Add New Staff. ID: '.$id.' Name: '.$data['firstname'].' '.$data['lastname'], $activitytype, $staffid);
                    redirect(perfex_admin_url('staff/member/' . $id));
                }
            } else {
                if (!has_permission('staff', '', 'edit')) {
                    access_denied('staff');
                }
                handle_staff_profile_image_upload($id);
                $response = $this->staff_model->update($data, $id);
                if(is_array($response)){
                    if(isset($response['cant_remove_main_admin'])){
                        set_alert('warning', lang('staff_cant_remove_main_admin'));
                    } else if(isset($response['cant_remove_yourself_from_admin'])){
                        set_alert('warning', lang('staff_cant_remove_yourself_from_admin'));
                    }
                } elseif ($response == true) {
                   set_alert('success', lang('updated_successfuly', lang('staff_member')));
                   //log staff activity - Manage staff update
                    $activitytype = 38; // update staff
                    $staffid = get_staff_user_id();
                    crmlogActivity('Staff Updated Staff. ID: '.$id.' Name: '.$data['firstname'].' '.$data['lastname'], $activitytype, $staffid);
               }
               redirect(perfex_admin_url('staff/member/' . $id));
            }
        }
        if ($id == '') {
            $title = lang('add_new', lang('staff_member_lowercase'));
        } else {
            $member                    = $this->staff_model->get($id);
            if(!$member){
                blank_page('Staff Member Not Found','danger');
            }
            $data['member']            = $member;
            $title                     = $member->firstname . ' ' . $member->lastname;
            $data['staff_permissions'] = $this->roles_model->get_staff_permissions($id);
            $data['staff_departments'] = $this->departments_model->get_staff_departments($member->staffid);

            $ts_filter_data = array();
            if($this->input->get('filter')){
                if($this->input->get('range') != 'period'){
                    $ts_filter_data[$this->input->get('range')] = true;
                } else {
                    $ts_filter_data['period-from'] = $this->input->get('period-from');
                    $ts_filter_data['period-to'] = $this->input->get('period-to');
                }
            } else {
                $ts_filter_data['this_month'] = true;
            }

            $data['logged_time'] = $this->staff_model->get_logged_time_data($id,$ts_filter_data);
            $data['timesheets'] = $data['logged_time']['timesheets'];


        }
        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['roles']       = $this->roles_model->get();
        $data['activity_type'] = $this->db->get('main_shopfreemart.sfm_crm_activitytypes')->result_array();
        $data['permissions'] = $this->roles_model->get_permissions();
        $data['user_notes'] = $this->misc_model->get_notes($id, 'staff');
        $data['departments'] = $this->departments_model->get();
        $data['staff_activitylog'] = $this->misc_model->get_staff_activitylog($id);
        $this->load->model('surveys_model');
        $data['staff_survey'] = $this->getsurveyinfowithnames($id);
        $data['title']       = $title;
        $this->load->view('crm/admin/staff/member', $data);
    }
    function getsurveyinfowithnames($id){
        $info = $this->surveys_model->get_staff_surveys($id);
        $set = array();
        foreach($info as $inf => $value){
            if($value['is_staff']==1){
                $this->db->select('firstname,lastname');
                $this->db->where('staffid',$value['userid']);
                $staffinf = $this->db->get('main_shopfreemart.sfm_crm_staff')->row_array();
                $value['fullname'] = $staffinf['firstname'] ." ". $staffinf['lastname'];
            }
            else{
                if($value['userid'] == 0){
                    $value['fullname'] == "not a member";
                }
                else{
                    $this->load->model('clients_model');
                $clientinfo = $this->clients_model->get($value['userid']);
                $value['fullname'] = $clientinfo->firstname ." ". $clientinfo->lastname;
                }
                
            }
            array_push($set, $value);
        }
        return $set;

    }
    public function change_language($lang = ''){
        $lang = perfex_do_action('before_staff_change_language',$lang);
        $this->db->where('staffid',get_staff_user_id());
        $this->db->update('main_shopfreemart.sfm_crm_staff',array('default_language'=>$lang));
        if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect(perfex_admin_url());
        }
    }
    public function timesheets(){

        $data['view_all'] = false;
        if(perfex_is_admin() && $this->input->get('view') == 'all'){
            $data['staff_members_with_timesheets'] = $this->db->query('SELECT DISTINCT staff_id FROM main_crm.crm_taskstimers WHERE staff_id !='.get_staff_user_id())->result_array();
            $data['view_all'] = true;
        }

        if($this->input->is_ajax_request()){
            $this->perfex_base->get_table_data('staff_timesheets',array('view_all'=>$data['view_all']));
        }

        if($data['view_all'] == false){
            unset($data['view_all']);
        }
        $data['chart_js_assets']   = true;
        $data['logged_time'] = $this->staff_model->get_logged_time_data(get_staff_user_id());
        $data['title'] = '';

        $projects = array();

        $projects_where = array();

        if(!has_permission('projects','','create')){
            $projects_where = '';
            $projects_where .= 'main_crm.crm_projects.id IN (SELECT project_id FROM main_crm.crm_projectmembers WHERE staff_id=' . get_staff_user_id() .')';
        }

        $data['projects'] =  $this->projects_model->get('',$projects_where);


        $this->load->view('crm/admin/staff/timesheets',$data);

    }
    public function delete(){
        if(has_permission('staff','','delete')){
            $success = $this->staff_model->delete($this->input->post('id'),$this->input->post('transfer_data_to'));
            if($success){
                set_alert('success',lang('deleted',lang('staff_member')));
                //log staff activity - Manage staff delete
                $activitytype = 39; // delete staff
                $staffid = get_staff_user_id();
                crmlogActivity('Staff Deleted Staff. ID: '.$id.' Name: '.$data['firstname'].' '.$data['lastname'], $activitytype, $staffid);
            }
        }
        redirect(perfex_admin_url('staff'));
    }
    /* When staff edit his profile */
    public function edit_profile()
    {
        if ($this->input->post()) {
            handle_staff_profile_image_upload();
            $data = $this->input->post();
            // Dont do XSS clean here.
            $data['email_signature'] = $data['email_signature'] = $this->input->post('email_signature',FALSE);

            $success = $this->staff_model->update_profile($data, get_staff_user_id());
            if ($success) {
                set_alert('success', lang('staff_profile_updated'));
                //log staff activity - staff own profile update
                $activitytype = 40; // update own profile
                $staffid = get_staff_user_id();
                crmlogActivity('Staff Update Own Profile Staff. ID: '.$staffid, $activitytype, $staffid);
            }
            redirect(perfex_admin_url('staff/edit_profile/' . get_staff_user_id()));
        }
        $member = $this->staff_model->get(get_staff_user_id());
        $this->load->model('departments_model');
        $data['member']            = $member;
        $data['departments']       = $this->departments_model->get();
        $data['staff_departments'] = $this->departments_model->get_staff_departments($member->staffid);
        $data['title']             = $member->firstname . ' ' . $member->lastname;
        $this->load->view('crm/admin/staff/profile', $data);
    }
    /* Remove staff profile image / ajax */
    public function remove_staff_profile_image($id = '')
    {
        $staff_id = get_staff_user_id();
        if(is_numeric($id) && (has_permission('staff','','create') || has_permission('staff','','edot'))){
            $staff_id = $id;
        }
        perfex_do_action('before_remove_staff_profile_image');
        $member = $this->staff_model->get($staff_id);
        if (file_exists(get_upload_path_by_type('staff') . $staff_id)) {
            delete_dir(get_upload_path_by_type('staff') . $staff_id);
        }
        $this->db->where('staffid', $staff_id);
        $this->db->update('main_shopfreemart.sfm_crm_staff', array(
            'profile_image' => NULL
        ));

        if(!is_numeric($id)){
            redirect(perfex_admin_url('staff/edit_profile/' .$staff_id));
        } else {
            redirect(perfex_admin_url('staff/member/' . $staff_id));
        }
    }
    /* When staff change his password */
    public function change_password_profile()
    {
        if ($this->input->post()) {
            $response = $this->staff_model->change_password($this->input->post(), get_staff_user_id());
            if (is_array($response) && isset($response[0]['passwordnotmatch'])) {
                set_alert('danger', lang('staff_old_password_incorect'));
            } else {
                if ($response == true) {
                    set_alert('success', lang('staff_password_changed'));
                    //log staff activity - staff own password update
                    $activitytype = 41; // update own password
                    $staffid = get_staff_user_id();
                    crmlogActivity('Staff Changed Own Password. ID: '.$staffid, $activitytype, $staffid);

                } else {
                    set_alert('warning', lang('staff_problem_changing_password'));
                }
            }
            redirect(perfex_admin_url('staff/edit_profile'));
        }
    }
    /* View public profile. If id passed view profile by staff id else current user*/
    public function profile($id = '')
    {
        if ($id == '') {
            $id = get_staff_user_id();
        }

        perfex_do_action('staff_profile_access',$id);

        $data['logged_time'] = $this->staff_model->get_logged_time_data($id);
        $data['staff_p'] = $this->staff_model->get($id);

        if(!$data['staff_p']){
            blank_page('Staff Member Not Found','danger');
        }
        $data['staff_activitylog'] = $this->misc_model->get_staff_activitylog($id);
        $data['activity_type'] = $this->db->get('main_shopfreemart.sfm_crm_activitytypes')->result_array();
        $this->load->model('departments_model');
        $data['staff_departments'] = $this->departments_model->get_staff_departments($data['staff_p']->staffid);
        $data['departments']       = $this->departments_model->get();
        $data['title']             = lang('staff_profile_string') . ' - ' . $data['staff_p']->firstname . ' ' . $data['staff_p']->lastname;
        // notifications
        $total_notifications       = total_rows('main_shopfreemart.sfm_crm_adminnotifications', array(
            'touser_id' => get_staff_user_id()
        ));
        $data['total_pages']       = ceil($total_notifications / $this->misc_model->notifications_limit);
        $this->load->view('admin/staff/myprofile', $data);
    }
    /* Change status to staff active or inactive / ajax */
    public function change_staff_status($id, $status)
    {
        if (has_permission('staff', '', 'edit')) {
            if ($this->input->is_ajax_request()) {
                $this->staff_model->change_staff_status($id, $status);
                //log staff activity - Manage Staff Change staff status
                    $activitytype = 30; // Change staff status
                    $staffid = get_staff_user_id();
                    crmlogActivity('Staff Changed Staff Status. Staff ID: '.$staffid, $activitytype, $staffid);

            }
        }
    }
    /* Logged in staff notifications*/
    public function notifications()
    {
        $this->load->model('misc_model');
        if ($this->input->post()) {
            $page = $this->input->post('page');
            $offset = ($page * $this->misc_model->get_notifications_limit());
            $this->db->limit($this->misc_model->get_notifications_limit(), $offset);
            $this->db->where('touser_id', get_staff_user_id());
            $this->db->order_by('created_at', 'desc');
            $notifications = $this->db->get('main_shopfreemart.sfm_crm_adminnotifications')->result_array();
            $i             = 0;
            foreach ($notifications as $notification) {
                if (($notification['fromcompany'] == null && $notification['fromuser_id'] != 0) || ($notification['fromcompany'] == null && $notification['fromclientid'] != 0)) {
                    if ($notification['fromuser_id'] != 0) {
                        $notifications[$i]['profile_image'] = '<a href="' . admin_url('staff/profile/' . $notification['fromuser_id']) . '">' . staff_profile_image($notification['fromuser_id'], array(
                        'staff-profile-image-small',
                        'img-circle',
                        'pull-left'
                    )) . '</a>';
                    } else {
                        $notifications[$i]['profile_image'] = '<a href="' . admin_url('clients/client/' . $notification['fromclientid']) . '">
                    <img class="client-profile-image-small img-circle pull-left" src="' . contact_profile_image_url($notification['fromclientid']) . '"></a>';
                    }
                } else {
                    $notifications[$i]['profile_image'] = '';
                    $notifications[$i]['full_name']     = '';
                }
                $additional_data = '';
                if (!empty($notification['additional_data'])) {
                    $additional_data = unserialize($notification['additional_data']);
                    $x               = 0;
                    foreach ($additional_data as $data) {
                        if (strpos($data, '<lang>') !== false) {
                            $lang = get_string_between($data, '<lang>', '</lang>');
                            $temp = _l($lang);
                            if (strpos($temp, 'project_status_') !== false) {
                                $status = get_project_status_by_id(strafter($temp, 'project_status_'));
                                $temp = $status['name'];
                            }
                            $additional_data[$x] = $temp;
                        }
                        $x++;
                    }
                }
                $notifications[$i]['description'] = _l($notification['description'], $additional_data);
                $notifications[$i]['created_at']        = time_ago($notification['created_at']);
                $i++;
            } //$notifications as $notification
            echo json_encode($notifications);
            die;
        }
    }
}
