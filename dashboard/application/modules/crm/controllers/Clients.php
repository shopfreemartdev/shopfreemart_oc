<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Clients extends Admin_Controller
{
    private $not_importable_clients_fields = array('user_id', 'id', 'active', 'password', ' date_registered', 'last_ip', 'last_login', 'last_password_change', 'active', 'new_pass_key', 'new_pass_key_requested', 'leadid', 'default_currency', 'profile_image', 'default_language', 'direction','show_primary_contact');
    public $pdf_zip;
    
    // public $template = array();
     public $data = array();
    function __construct()
    {
        parent::__construct();
    }
    /* List all clients */
    public function index()
    {
        redirect('managemembers');
        if (!has_permission('customers', '', 'view')) {
            if (!have_assigned_customers() && !has_permission('customers','','create')) {
                access_denied('customers');
            }
        }
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('clients');
        }
        $this->load->model('contracts_model');
        $data['contract_types'] = $this->contracts_model->get_contract_types();
        $data['groups']         = $this->clients_model->get_groups();
        $data['title']          = lang('clients');

        $this->load->model('proposals_model');
        $data['proposal_statuses'] = $this->proposals_model->get_statuses();

        $this->load->model('invoices_model');
        $data['invoice_statuses'] = $this->invoices_model->get_statuses();

        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();

        $this->load->model('projects_model');
        $data['project_statuses'] = $this->projects_model->get_project_statuses();

        $data['customer_admins'] = $this->clients_model->get_customers_admin_unique_ids();

        $this->load->view('crm/admin/clients/manage', $data);
    }
    /* Edit client or add new client*/
    public function client($id = '')
    {
        $this->session->set_userdata('admin_client',$id); 

        $this->load->model('authentication_model');
        $this->Authentication_model->logged_client($id,true);

        $this->db->query("CALL update_user_info(".$id.")");

        $data['subtable']  = null;

        if (!has_permission('managemembers', '', 'view')) {
            if ($id != '' && !is_customer_admin($id)) {
                access_denied('managemembers');
            }
        }
        if ($this->input->post() && !$this->input->is_ajax_request()) {
            if ($id == '') {
                if (!has_permission('managemembers', '', 'create')) {
                    access_denied('managemembers');
                }
                $data                 = $this->input->post();
                $save_and_add_contact = false;
                if (isset($data['save_and_add_contact'])) {
                    unset($data['save_and_add_contact']);
                    $save_and_add_contact = true;
                }
                $id = $this->clients_model->add($data);
                if (!has_permission('customers', '', 'view')) {
                    $assign['customer_admins']   = array();
                    $assign['customer_admins'][] = get_staff_user_id();
                    $this->clients_model->assign_admins($assign, $id);
                }
                if ($id) {
                    set_alert('success', lang('added_successfuly', lang('client')));
                    if ($save_and_add_contact == false) {
                        redirect(perfex_admin_url('clients/client/' . $id));
                    } else {
                        redirect(perfex_admin_url('clients/client/' . $id . '?new_contact=true'));
                    }
                }
            } else {
                if (!has_permission('customers', '', 'edit')) {
                    if (!is_customer_admin($id)) {
                        access_denied('customers');
                    }
                }
                $success = $this->clients_model->update($this->input->post(), $id);
                if ($success == true) {
                    set_alert('success', lang('updated_successfuly', lang('client')));
                }
                redirect(perfex_admin_url('clients/client/' . $id));
            }
        }
        if ($id == '') {
            $title = lang('add_new', lang('client_lowercase'));
        } else {
            $client = $this->clients_model->get($id);
            if (!$client) {
                blank_page('Client Not Found');
            }

            $data['lightbox_assets'] = true;
            $this->load->model('staff_model');
            $data['staff']           = $this->staff_model->get('', 1);
            $data['customer_admins'] = $this->clients_model->get_admins($id);
            $this->load->model('payment_modes_model');
            $data['payment_modes'] = $this->payment_modes_model->get();
            $data['attachments']   = $this->clients_model->get_all_customer_attachments($id);
            $data['client']        = $client;
            $title                 = $client->company;
            // Get all active staff members (used to add reminder)
            $this->load->model('staff_model');
            $data['members'] = $this->staff_model->get('', 1);
            if ($this->input->is_ajax_request()) {
                $this->perfex_base->get_table_data('tickets', array(
                    'user_id' => $id
                ));
            }
            $data['customer_groups'] = $this->clients_model->get_customer_groups($id);

            $this->load->model('estimates_model');
            $data['estimate_statuses'] = $this->estimates_model->get_statuses();

            $this->load->model('invoices_model');
            $data['invoice_statuses'] = $this->invoices_model->get_statuses();

            if (!empty($data['client']->company)) {
                // Check if is realy empty client company so we can set this field to empty
                // The query where fetch the client auto populate firstname and lastname if company is empty
                if (is_empty_customer_company($data['client']->user_id)) {
                    $data['client']->company = '';
                }
            }
        }
        if (!$this->input->get('group')) {
            $group = 'profile';
        } else {
            $group = $this->input->get('group');
        }

        if ($group == 'downlines')
        {
            Modules::run('downlines/data');
        }
        elseif ($group == 'orders')
        {
            $data['subtable']  = modules::run('shoporders2/index_admin');
        }
        elseif ($group == 'transactions')
        {
            $data['subtable']  = modules::run('transactions/totals_admin');
        }
        elseif ($group == 'downlinesales')
        {
            $data['subtable']  = modules::run('rewards/downlinesales_admin');
        }
        elseif ($group == 'commissions')
        {
            $data['subtable']  = modules::run('transactions/profits_admin');
        }
        elseif ($group == 'coins')
        {
            $data['subtable']  = modules::run('transactions/coins_admin');
        }
        elseif ($group == 'transfers')
        {
            $data['subtable']  = modules::run('transfers/index_admin');
        }
        elseif ($group == 'withdrawals')
        {
            $data['subtable']  = modules::run('withdrawals/index_admin');
        }
        elseif ($group == 'personalreferral')
        {
            $data['subtable']  = modules::run('personalreferral/data_admin');
        }
        elseif ($group == 'rankadvancement')
        {
            $data['subtable']  = modules::run('rewards/rank_admin');
        }
        elseif ($group == 'unilevellist')
        {
            $data['subtable']  = modules::run('genealogy/index_admin');
        }
        elseif ($group == 'genealogy')
        {
            $data['subtable']  = modules::run('genealogy/tree_admin');
        }
        elseif ($group == 'sponsortree')
        {
            $data['subtable']  = modules::run('sponsortree/index_admin');
        }
        elseif ($group == 'founding')
        {
            $data['subtable']  = modules::run('founding/data');
        }
        elseif ($group == 'membercalllogs')
        {
            $tableHTML = modules::run('membercalllogs/data_admin');
            $data['subtable']  = $tableHTML;
        }

        $data['group']  = $group;
        $data['groups'] = $this->clients_model->get_groups();
        $this->load->model('currencies_model');
        $data['currencies'] = $this->currencies_model->get();
        $data['user_notes'] = $this->misc_model->get_notes($id, 'customer');
        $data['bodyclass'] = 'customer-profile';
        $this->load->model('projects_model');
        $data['project_statuses'] = $this->projects_model->get_project_statuses();
        $data['contacts']         = $this->clients_model->get_contacts($id);

        $data['title'] = $title;
        $this->load->view('crm/admin/clients/client', $data);
    }

    public function load_page($id = ''){

        $this->session->set_userdata('admin_client',$id); 

        $this->load->model('authentication_model');
        $this->Authentication_model->logged_client($id,true);

        $data['subtable']  = null;

        if (!$this->input->get('group')) {
            $group = 'profile';
        } else {
            $group = $this->input->get('group');
        }

        if ($group == 'downlines')
        {
            Modules::run('downlines/data');
        }
        elseif ($group == 'orders')
        {
            $data['subtable'] = modules::run('shoporders2/index_admin');
            ld($data['subtable']);
        }
        elseif ($group == 'transactions')
        {
            $data['subtable']  = modules::run('transactions/totals_admin');
        }
        elseif ($group == 'downlinesales')
        {
            $data['subtable']  = modules::run('rewards/downlinesales_admin');
        }
        elseif ($group == 'commissions')
        {
            $data['subtable']  = modules::run('transactions/profits_admin');
        }
        elseif ($group == 'coins')
        {
            $data['subtable']  = modules::run('transactions/coins_admin');
        }
        elseif ($group == 'transfers')
        {
            $data['subtable']  = modules::run('transfers/index_admin');
        }
        elseif ($group == 'withdrawals')
        {
            $data['subtable']  = modules::run('withdrawals/index_admin');
        }
        elseif ($group == 'personalreferral')
        {
            $data['subtable']  = modules::run('personalreferral/data_admin');
        }
        elseif ($group == 'rankadvancement')
        {
            $data['subtable']  = modules::run('rewards/rank_admin');
        }
        elseif ($group == 'unilevellist')
        {
            $data['subtable']  = modules::run('genealogy/index_admin');
        }
        elseif ($group == 'genealogy')
        {
            $data['subtable']  = modules::run('genealogy/tree_admin?');
        }
        elseif ($group == 'sponsortree')
        {
            $data['subtable']  = modules::run('sponsortree/index_admin');
        }
        elseif ($group == 'founding')
        {
            $data['subtable']  = modules::run('founding/data');
        }

        $data['group']  = $group;

        $this->load->view('crm/admin/clients/client_data', $data);
        //$this->render_content('crm/admin/clients/client_data', $data);

    }

    public function show_data($id = null){
        $data['data'] = $this->Users_model->get_user_data($id);
        $this->load->view('crm/admin/clients/show_data', $data);
    }

    public function contact($customer_id, $contact_id = '')
    {
        if (!has_permission('customers', '', 'view')) {
            if (!is_customer_admin($customer_id)) {
                echo lang('access_denied');
                die;
            }
        }
        $data['customer_id'] = $customer_id;
        $data['contactid']   = $contact_id;
        if ($this->input->post()) {
            $data = $this->input->post();
            unset($data['contactid']);
            if ($contact_id == '') {
                if (!has_permission('customers', '', 'create')) {
                    if (!is_customer_admin($customer_id)) {
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode(array(
                            'success' => false,
                            'message' => lang('access_denied')
                        ));
                        die;
                    }
                }
                $id      = $this->clients_model->add_contact($data, $customer_id);
                $message = '';
                $success = false;
                if ($id) {
                    handle_contact_profile_image_upload($id);
                    $success = true;
                    $message = lang('added_successfuly', lang('contact'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
                die;
            } else {
                if (!has_permission('customers', '', 'edit')) {
                    if (!is_customer_admin($customer_id)) {
                        header('HTTP/1.0 400 Bad error');
                        echo json_encode(array(
                            'success' => false,
                            'message' => lang('access_denied')
                        ));
                        die;
                    }
                }
                $original_contact = $this->clients_model->get_contact($contact_id);
                $success          = $this->clients_model->update_contact($data, $contact_id);
                $message          = '';
                $proposal_warning = false;
                $original_email   = '';
                $updated          = false;
                if (is_array($success)) {
                    if (isset($success['set_password_email_sent'])) {
                        $message = lang('set_password_email_sent_to_client');
                    } else if (isset($success['set_password_email_sent_and_profile_updated'])) {
                        $updated = true;
                        $message = lang('set_password_email_sent_to_client_and_profile_updated');
                    }
                } else {
                    if ($success == true) {
                        $updated = true;
                        $message = lang('updated_successfuly', lang('contact'));
                    }
                }
                if (handle_contact_profile_image_upload($contact_id) && !$updated) {
                    $message = lang('updated_successfuly', lang('contact'));
                    $success = true;
                }
                if ($updated == true) {
                    $contact = $this->clients_model->get_contact($contact_id);
                    if (total_rows('main_crm.crm_proposals', array(
                        'rel_type' => 'customer',
                        'rel_id' => $contact->user_id,
                        'email' => $original_contact->email
                    )) > 0 && ($original_contact->email != $contact->email)) {
                        $proposal_warning = true;
                        $original_email   = $original_contact->email;
                    }
                }
                echo json_encode(array(
                    'success' => $success,
                    'proposal_warning' => $proposal_warning,
                    'message' => $message,
                    'original_email' => $original_email
                ));
                die;
            }
        }
        if ($contact_id == '') {
            $title = lang('add_new', lang('contact_lowercase'));
        } else {
            $data['contact'] = $this->clients_model->get_contact($contact_id);

            if (!$data['contact']) {
                header('HTTP/1.0 400 Bad error');
                echo json_encode(array(
                    'success' => false,
                    'message' => 'Contact Not Found'
                ));
                die;
            }
            $title = $data['contact']->firstname . ' ' . $data['contact']->lastname;
        }

        $data['customer_permissions'] = $this->perfex_base->get_contact_permissions();
        $data['title']                = $title;
        $this->load->view('crm/admin/clients/modals/contact', $data);
    }
    public function update_file_share_visibility()
    {
        if ($this->input->post()) {

            $file_id           = $this->input->post('file_id');
            $share_contacts_id = array();

            if ($this->input->post('share_contacts_id')) {
                $share_contacts_id = $this->input->post('share_contacts_id');
            }

            $this->db->where('file_id', $file_id);
            $this->db->delete('main_crm.crm_customerfiles_shares');

            foreach ($share_contacts_id as $share_contact_id) {
                $this->db->insert('main_crm.crm_customerfiles_shares', array(
                    'file_id' => $file_id,
                    'contact_id' => $share_contact_id
                ));
            }

        }
    }
    public function delete_contact_profile_image($contact_id)
    {
        perfex_do_action('before_remove_contact_profile_image');
        if (file_exists(get_upload_path_by_type('contact_profile_images') . $contact_id)) {
            delete_dir(get_upload_path_by_type('contact_profile_images') . $contact_id);
        }
        $this->db->where('id', $contact_id);
        $this->db->update('sfm_uap_affiliates', array(
            'profile_image' => NULL
        ));
    }
    public function mark_as_active($id)
    {
        $this->db->where('user_id', $id);
        $this->db->update('sfm_uap_affiliates', array(
            'active' => 1
        ));
        redirect(perfex_admin_url('clients/client/' . $id));
    }
    public function update_all_proposal_emails_linked_to_customer($contact_id)
    {

        $success = false;
        $email   = '';
        if ($this->input->post('update')) {
            $this->load->model('proposals_model');

            $this->db->select('email,user_id');
            $this->db->where('id', $contact_id);
            $contact = $this->db->get('sfm_uap_affiliates')->row();

            $proposals     = $this->proposals_model->get('', array(
                'rel_type' => 'customer',
                'rel_id' => $contact->user_id,
                'email' => $this->input->post('original_email')
            ));
            $affected_rows = 0;

            foreach ($proposals as $proposal) {
                $this->db->where('id', $proposal['id']);
                $this->db->update('main_crm.crm_proposals', array(
                    'email' => $contact->email
                ));
                if ($this->db->affected_rows() > 0) {
                    $affected_rows++;
                }
            }

            if ($affected_rows > 0) {
                $success = true;
            }

        }
        echo json_encode(array(
            'success' => $success,
            'message' => lang('proposals_emails_updated', array(
                lang('contact_lowercase'),
                $contact->email
            ))
        ));
    }

    public function assign_admins($id)
    {
        if (!has_permission('customers', '', 'create') && !has_permission('customers', '', 'edit')) {
            access_denied('customers');
        }
        $success = $this->clients_model->assign_admins($this->input->post(), $id);
        if ($success == true) {
            set_alert('success', lang('updated_successfuly', lang('client')));
        }

        redirect(perfex_admin_url('clients/client/' . $id . '?tab=customer_admins'));

    }

    public function delete_customer_admin($customer_id,$staff_id){

        if (!has_permission('customers', '', 'create') && !has_permission('customers', '', 'edit')) {
            access_denied('customers');
        }

        $this->db->where('customer_id',$customer_id);
        $this->db->where('staff_id',$staff_id);
        $this->db->delete('main_crm.crm_customeradmins');
        redirect(perfex_admin_url('clients/client/'.$customer_id).'?tab=customer_admins');
    }
    public function delete_contact($customer_id, $id)
    {
        if (!has_permission('customers', '', 'delete')) {
            if (!is_customer_admin($customer_id)) {
                access_denied('customers');
            }
        }

        $this->clients_model->delete_contact($id);
        redirect(perfex_admin_url('clients/client/' . $customer_id . '?tab=contacts'));
    }
    public function contacts($client_id)
    {
        $this->perfex_base->get_table_data('contacts', array(
            'client_id' => $client_id
        ));
    }
    public function upload_attachment($id)
    {
        handle_client_attachments_upload($id);
    }
    public function add_external_attachment()
    {
        if ($this->input->post()) {
            $this->misc_model->add_attachment_to_database($this->input->post('clientid'), 'customer', $this->input->post('files'), $this->input->post('external'));
        }
    }
    public function delete_attachment($customer_id, $id)
    {
        if (has_permission('customers', '', 'delete') || is_customer_admin($customer_id)) {
            $this->clients_model->delete_attachment($id);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }
    /* Delete client */
    public function delete($id)
    {
        if (!has_permission('customers', '', 'delete')) {
            access_denied('customers');
        }
        if (!$id) {
            redirect(perfex_admin_url('clients'));
        }
        $response = $this->clients_model->delete($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', lang('client_delete_invoices_warning'));
        } else if ($response == true) {
            set_alert('success', lang('deleted', lang('client')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('client_lowercase')));
        }
        redirect(perfex_admin_url('clients'));
    }
    /* Staff can login as client */
    public function login_as_client($id)
    {  
        $this->load->helper('cookie'); 
        set_cookie(array(
            'name' => 'client_user_id',
            'value' => $id,
            'expire' => 60 * 60 * 24 * 31 * 2 // 2 months
        ));            
        
        //log staff activity - Switch to
        $activitytype = 8; // 8 = Switch to
        $staffid = get_staff_user_id();
        crmlogActivity("Staff logged in as client: ".$id, $activitytype, $staffid);

        if (perfex_is_admin()) {
               //  echo get_client_user_id();
            $this->clients_model->login_as_client($id,true);
        }
        perfex_do_action('after_contact_login');
        $this->Authentication_model->logged_client($id,true);
      
        $this->session->set_userdata(array(
            'logged_in_as_client' => true
         ));   

        
                               
        redirect(MEMBERDASHBOARD_URL.'?logged_as_client=1');
    }

    /* Staff can login as client */
    public function addfounding($id)
    {  
        $this->load->helper('cookie'); 
        set_cookie(array(
            'name' => 'client_user_id',
            'value' => $id,
            'expire' => 60 * 60 * 24 * 31 * 2 // 2 months
        ));            
        
        //log staff activity - Switch to
        $activitytype = 8; // 8 = Switch to
        $staffid = get_staff_user_id();
        crmlogActivity("Staff logged in as client: ".$id, $activitytype, $staffid);

        if (perfex_is_admin()) {
               //  echo get_client_user_id();
            $this->clients_model->login_as_client($id,true);
        }
        perfex_do_action('after_contact_login');
        $this->Authentication_model->logged_client($id,true);
      
        $this->session->set_userdata(array(
            'logged_in_as_client' => true
         ));   
                    
        redirect(MEMBERDASHBOARD_URL.'/../../product/joint-venture-partner-founding-membership/?alfredbypassfounding=1');
    }

    public function get_customer_billing_and_shipping_details($id)
    {
        echo json_encode($this->clients_model->get_customer_billing_and_shipping_details($id));
    }
    /* Change client status / active / inactive */
    public function change_contact_status($id, $status)
    {
        if (has_permission('customers', '', 'edit')) {
            if ($this->input->is_ajax_request()) {
                $this->clients_model->change_contact_status($id, $status);
            }
        }
    }
    /* Change client status / active / inactive */
    public function change_client_status($id, $status)
    {

        if ($this->input->is_ajax_request()) {
            $this->clients_model->change_client_status($id, $status);
        }

    }
    /* Since version 1.0.2 zip client invoices */
    public function zip_invoices($id)
    {
        $has_permission_view = has_permission('invoices', '', 'view');
        if (!$has_permission_view && !has_permission('invoices', '', 'view_own')) {
            access_denied('Zip Customer Invoices');
        }
        if ($this->input->post()) {
            $status        = $this->input->post('invoice_zip_status');
            $zip_file_name = $this->input->post('file_name');
            if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
                $from_date = to_sql_date($this->input->post('zip-from'));
                $to_date   = to_sql_date($this->input->post('zip-to'));
                if ($from_date == $to_date) {
                    $this->db->where('date', $from_date);
                } else {
                    $this->db->where('date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
                }
            }
            $this->db->select('id');
            $this->db->from('main_crm.crm_invoices');
            if ($status != 'all') {
                $this->db->where('status', $status);
            }
            $this->db->where('clientid', $id);
            $this->db->order_by('number,YEAR(date)', 'desc');

            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }

            $invoices = $this->db->get()->result_array();
            $this->load->model('invoices_model');
            $this->load->helper('file');
            if (!is_really_writable(TEMP_FOLDER)) {
                show_error('/temp folder is not writable. You need to change the permissions to 755');
            }
            $dir = TEMP_FOLDER . $zip_file_name;
            if (is_dir($dir)) {
                delete_dir($dir);
            }
            if (count($invoices) == 0) {
                set_alert('warning', lang('client_zipcode_no_data_found', lang('invoices')));
                redirect(perfex_admin_url('clients/client/' . $id . '?group=invoices'));
            }
            mkdir($dir, 0777);
            foreach ($invoices as $invoice) {
                $invoice_data    = $this->invoices_model->get($invoice['id']);
                $this->pdf_zip   = invoice_pdf($invoice_data);
                $_temp_file_name = slug_it(format_invoice_number($invoice_data->id));
                $file_name       = $dir . '/' . strtoupper($_temp_file_name);
                $this->pdf_zip->Output($file_name . '.pdf', 'F');
            }
            $this->load->library('zip');
            // Read the invoices
            $this->zip->read_dir($dir, false);
            // Delete the temp directory for the client
            delete_dir($dir);
            $this->zip->download(slug_it(perfex_get_option('companyname')) . '-invoices-' . $zip_file_name . '.zip');
            $this->zip->clear_data();
        }
    }
    /* Since version 1.0.2 zip client invoices */
    public function zip_estimates($id)
    {
        $has_permission_view = has_permission('estimates', '', 'view');
        if (!$has_permission_view && !has_permission('estimates', '', 'view_own')) {
            access_denied('Zip Customer Estimates');
        }


        if ($this->input->post()) {
            $status        = $this->input->post('estimate_zip_status');
            $zip_file_name = $this->input->post('file_name');
            if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
                $from_date = to_sql_date($this->input->post('zip-from'));
                $to_date   = to_sql_date($this->input->post('zip-to'));
                if ($from_date == $to_date) {
                    $this->db->where('date', $from_date);
                } else {
                    $this->db->where('date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
                }
            }
            $this->db->select('id');
            $this->db->from('main_crm.crm_estimates');
            if ($status != 'all') {
                $this->db->where('status', $status);
            }
            if (!$has_permission_view) {
                $this->db->where('addedfrom', get_staff_user_id());
            }
            $this->db->where('clientid', $id);
            $this->db->order_by('number,YEAR(date)', 'desc');
            $estimates = $this->db->get()->result_array();
            $this->load->helper('file');
            if (!is_really_writable(TEMP_FOLDER)) {
                show_error('/temp folder is not writable. You need to change the permissions to 777');
            }
            $this->load->model('estimates_model');
            $dir = TEMP_FOLDER . $zip_file_name;
            if (is_dir($dir)) {
                delete_dir($dir);
            }
            if (count($estimates) == 0) {
                set_alert('warning', lang('client_zipcode_no_data_found', lang('estimates')));
                redirect(perfex_admin_url('clients/client/' . $id . '?group=estimates'));
            }
            mkdir($dir, 0777);
            foreach ($estimates as $estimate) {
                $estimate_data   = $this->estimates_model->get($estimate['id']);
                $this->pdf_zip   = estimate_pdf($estimate_data);
                $_temp_file_name = slug_it(format_estimate_number($estimate_data->id));
                $file_name       = $dir . '/' . strtoupper($_temp_file_name);
                $this->pdf_zip->Output($file_name . '.pdf', 'F');
            }
            $this->load->library('zip');
            // Read the invoices
            $this->zip->read_dir($dir, false);
            // Delete the temp directory for the client
            delete_dir($dir);
            $this->zip->download(slug_it(perfex_get_option('companyname')) . '-estimates-' . $zip_file_name . '.zip');
            $this->zip->clear_data();
        }
    }
    public function zip_payments($id)
    {
        if (!$id) {
            die('No user id');
        }

        $has_permission_view = has_permission('payments', '', 'view');
        if (!$has_permission_view && !has_permission('invoices', '', 'view_own')) {
            access_denied('Zip Customer Payments');
        }

        if ($this->input->post('zip-to') && $this->input->post('zip-from')) {
            $from_date = to_sql_date($this->input->post('zip-from'));
            $to_date   = to_sql_date($this->input->post('zip-to'));
            if ($from_date == $to_date) {
                $this->db->where('main_crm.crm_invoicepaymentrecords.date', $from_date);
            } else {
                $this->db->where('main_crm.crm_invoicepaymentrecords.date BETWEEN "' . $from_date . '" AND "' . $to_date . '"');
            }
        }
        $this->db->select('main_crm.crm_invoicepaymentrecords.id as paymentid');
        $this->db->from('main_crm.crm_invoicepaymentrecords');
        $this->db->where('sfm_uap_affiliates.user_id', $id);
        if (!$has_permission_view) {
            $this->db->where('invoiceid IN (SELECT id FROM main_crm.crm_invoices WHERE addedfrom=' . get_staff_user_id() . ')');
        }
        $this->db->join('main_crm.crm_invoices', 'main_crm.crm_invoices.id = main_crm.crm_invoicepaymentrecords.invoiceid', 'left');
        $this->db->join('sfm_uap_affiliates', 'sfm_uap_affiliates.user_id = main_crm.crm_invoices.clientid', 'left');
        if ($this->input->post('paymentmode')) {
            $this->db->where('paymentmode', $this->input->post('paymentmode'));
        }
        $payments      = $this->db->get()->result_array();
        $zip_file_name = $this->input->post('file_name');
        $this->load->helper('file');
        if (!is_really_writable(TEMP_FOLDER)) {
            show_error('/temp folder is not writable. You need to change the permissions to 777');
        }
        $dir = TEMP_FOLDER . $zip_file_name;
        if (is_dir($dir)) {
            delete_dir($dir);
        }
        if (count($payments) == 0) {
            set_alert('warning', lang('client_zipcode_no_data_found', lang('payments')));
            redirect(perfex_admin_url('clients/client/' . $id . '?group=payments'));
        }
        mkdir($dir, 0777);
        $this->load->model('payments_model');
        $this->load->model('invoices_model');
        foreach ($payments as $payment) {
            $payment_data               = $this->payments_model->get($payment['paymentid']);
            $payment_data->invoice_data = $this->invoices_model->get($payment_data->invoiceid);
            $this->pdf_zip              = payment_pdf($payment_data);
            $file_name                  = $dir;
            $file_name .= '/' . strtoupper(lang('payment'));
            $file_name .= '-' . strtoupper($payment_data->paymentid) . '.pdf';
            $this->pdf_zip->Output($file_name, 'F');
        }
        $this->load->library('zip');
        // Read the invoices
        $this->zip->read_dir($dir, false);
        // Delete the temp directory for the client
        delete_dir($dir);
        $this->zip->download(slug_it(perfex_get_option('companyname')) . '-payments-' . $zip_file_name . '.zip');
        $this->zip->clear_data();
    }
    public function import()
    {
        if (!has_permission('customers', '', 'create')) {
            access_denied('customers');
        }
        $simulate_data  = array();
        $total_imported = 0;
        if ($this->input->post()) {
            if (isset($_FILES['file_csv']['name']) && $_FILES['file_csv']['name'] != '') {
                // Get the temp file path
                $tmpFilePath = $_FILES['file_csv']['tmp_name'];
                // Make sure we have a filepath
                if (!empty($tmpFilePath) && $tmpFilePath != '') {
                    // Setup our new file path
                    $newFilePath = TEMP_FOLDER . $_FILES['file_csv']['name'];
                    if (!file_exists(TEMP_FOLDER)) {
                        mkdir(TEMP_FOLDER, 777);
                    }
                    if (move_uploaded_file($tmpFilePath, $newFilePath)) {
                        $import_result = true;
                        $fd            = fopen($newFilePath, 'r');
                        $rows          = array();
                        while ($row = fgetcsv($fd)) {
                            $rows[] = $row;
                        }

                        $data['total_rows_post'] = count($rows);
                        fclose($fd);
                        if (count($rows) <= 1) {
                            set_alert('warning', 'Not enought rows for importing');
                            redirect(perfex_admin_url('clients/import'));
                        }
                        unset($rows[0]);
                        if ($this->input->post('simulate')) {
                            if (count($rows) > 500) {
                                set_alert('warning', 'Recommended splitting the CSV file into smaller files. Our recomendation is 500 row, your CSV file has ' . count($rows));
                            }
                        }
                        $client_contacts_fields = $this->db->list_fields('sfm_uap_affiliates');
                        $i                      = 0;
                        foreach ($client_contacts_fields as $cf) {
                            if ($cf == 'phone') {
                                $client_contacts_fields[$i] = 'contact_phone';
                            }
                            $i++;
                        }
                        $db_temp_fields = $this->db->list_fields('sfm_uap_affiliates');
                        $db_temp_fields = array_merge($client_contacts_fields, $db_temp_fields);
                        $db_fields      = array();
                        foreach ($db_temp_fields as $field) {
                            if (in_array($field, $this->not_importable_clients_fields)) {
                                continue;
                            }
                            $db_fields[] = $field;
                        }
                        $custom_fields = get_custom_fields('customers');
                        $_row_simulate = 0;

                        $required = array(
                            'firstname',
                            'lastname',
                            'email'
                        );

                        if (perfex_get_option('company_is_required') == 1) {
                            array_push($required, 'company');
                        }
                        foreach ($rows as $row) {
                            // do for db fields
                            $insert    = array();
                            $duplicate = false;
                            for ($i = 0; $i < count($db_fields); $i++) {
                                if (!isset($row[$i])) {
                                    continue;
                                }
                                if ($db_fields[$i] == 'email') {
                                    $email_exists = total_rows('sfm_uap_affiliates', array(
                                        'email' => $row[$i]
                                    ));
                                    // dont insert duplicate emails
                                    if ($email_exists > 0) {
                                        $duplicate = true;
                                    }
                                }
                                // Avoid errors on required fields;
                                if (in_array($db_fields[$i], $required) && $row[$i] == '') {
                                    $row[$i] = '/';
                                } else if ($db_fields[$i] == 'country' || $db_fields[$i] == 'billing_country' || $db_fields[$i] == 'shipping_country') {
                                    if ($row[$i] != '') {
                                        $this->db->where('iso2', $row[$i]);
                                        $this->db->or_where('short_name', $row[$i]);
                                        $this->db->or_where('long_name', $row[$i]);
                                        $country = $this->db->get('main_shopfreemart.sfm_crm_countries')->row();
                                        if ($country) {
                                            $row[$i] = $country->country_id;
                                        } else {
                                            $row[$i] = 0;
                                        }
                                    } else {
                                        $row[$i] = 0;
                                    }
                                }
                                $insert[$db_fields[$i]] = $row[$i];
                            }

                            if ($duplicate == true) {
                                continue;
                            }
                            if (count($insert) > 0) {
                                $total_imported++;
                                $insert[' date_registered'] = date('Y-m-d H:i:s');
                                if ($this->input->post('default_pass_all')) {
                                    $insert['password'] = $this->input->post('default_pass_all');
                                }
                                if (!$this->input->post('simulate')) {
                                    $insert['donotsendwelcomeemail'] = true;
                                    $clientid                        = $this->clients_model->add($insert, true);
                                    if ($clientid) {
                                        if ($this->input->post('groups_in[]')) {
                                            $groups_in = $this->input->post('groups_in[]');
                                            foreach ($groups_in as $group) {
                                                $this->db->insert('main_crm.crm_customergroups_in', array(
                                                    'customer_id' => $clientid,
                                                    'groupid' => $group
                                                ));
                                            }
                                        }
                                        if (!has_permission('customers', '', 'view')) {
                                            $assign['customer_admins']   = array();
                                            $assign['customer_admins'][] = get_staff_user_id();
                                            $this->clients_model->assign_admins($assign, $clientid);
                                        }
                                    }
                                } else {
                                    $simulate_data[$_row_simulate] = $insert;
                                    $clientid                      = true;
                                }
                                if ($clientid) {
                                    $insert = array();
                                    foreach ($custom_fields as $field) {
                                        if (!$this->input->post('simulate')) {
                                            if ($row[$i] != '') {
                                                $this->db->insert('main_crm.crm_customfieldsvalues', array(
                                                    'relid' => $clientid,
                                                    'fieldid' => $field['id'],
                                                    'value' => $row[$i],
                                                    'fieldto' => 'customers'
                                                ));
                                            }
                                        } else {
                                            $simulate_data[$_row_simulate][$field['name']] = $row[$i];
                                        }
                                        $i++;
                                    }
                                }
                            }
                            $_row_simulate++;
                            if ($this->input->post('simulate') && $_row_simulate >= 100) {
                                break;
                            }
                        }
                        unlink($newFilePath);
                    }
                } else {
                    set_alert('warning', lang('import_upload_failed'));
                }
            }
        }
        if (count($simulate_data) > 0) {
            $data['simulate'] = $simulate_data;
        }
        if (isset($import_result)) {
            set_alert('success', lang('import_total_imported', $total_imported));
        }
        $data['groups']         = $this->clients_model->get_groups();
        $data['not_importable'] = $this->not_importable_clients_fields;
        $data['title']          = 'Import';
        $this->load->view('crm/admin/clients/import', $data);
    }
    public function groups()
    {
        if (!perfex_is_admin()) {
            access_denied('Customer Groups');
        }
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('customers_groups');
        }
        $data['title'] = lang('customer_groups');
        $this->load->view('crm/admin/clients/groups_manage', $data);
    }
    public function group()
    {
        if ($this->input->is_ajax_request()) {
            $data = $this->input->post();
            if ($data['id'] == '') {
                $success = $this->clients_model->add_group($data);
                $message = '';
                if ($success == true) {
                    $message = lang('added_successfuly', lang('customer_group'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
            } else {
                $success = $this->clients_model->edit_group($data);
                $message = '';
                if ($success == true) {
                    $message = lang('updated_successfuly', lang('customer_group'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
            }
        }
    }
    public function delete_group($id)
    {
        if (!perfex_is_admin()) {
            access_denied('Delete Customer Group');
        }
        if (!$id) {
            redirect(perfex_admin_url('clients/groups'));
        }
        $response = $this->clients_model->delete_group($id);
        if ($response == true) {
            set_alert('success', lang('deleted', lang('customer_group')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('customer_group_lowercase')));
        }
        redirect(perfex_admin_url('clients/groups'));
    }

    public function bulk_action()
    {
        perfex_do_action('before_do_bulk_action_for_customers');
        $total_deleted = 0;
        if ($this->input->post()) {
            $ids    = $this->input->post('ids');
            $groups = $this->input->post('groups');

            if (is_array($ids)) {
                foreach ($ids as $id) {
                    if ($this->input->post('mass_delete')) {
                        if ($this->clients_model->delete($id)) {
                            $total_deleted++;
                        }
                    } else {

                        if (!is_array($groups)) {
                            $groups = false;
                        }
                        $this->clients_model->handle_update_groups($id, $groups);
                    }
                }
            }
        }

        if ($this->input->post('mass_delete')) {
            set_alert('success', lang('total_clients_deleted', $total_deleted));
        }
    }

    public function survey($id, $hash, $userid = 0, $sendcronid = 0)
    {
        if (!$hash || !$id) {
            die('No survey specified');
        }
        $this->load->model('surveys_model');
        $survey = $this->surveys_model->get($id);
        if (!$survey || ($survey->hash != $hash)) {
            show_404();
        }

        // Allow users with permission manage surveys to preview the survey even if is not active
        if ($survey->active == 0) {
            if (!has_permission('Surveys', '', 'view')) {
                die('Survey not active');
            }
        }

        // Check if survey is only for logged in participants / staff / clients
        if ($survey->onlyforloggedin == 1) {
            if (!is_logged_in()) {
                die('This survey is only for logged in users');
            }
        }

        // Ip Restrict check
        if ($survey->iprestrict == 1) {
            $this->db->where('surveyid', $id);
            $this->db->where('ip', $this->input->ip_address());
            $total = $this->db->count_all_results('sfm_crm_surveyresultsets');
            if ($total > 0) {
                die('Already participated on this survey. Thanks');
            }
        }

        if(isset($uid)){
            $data['userid'] = $uid;
            if(isset($is_staff))
            {
                $data['is_staff'] = true;
            }
            else{
                $data['is_staff'] = false;
            }
        }
        else{
            $data['userid'] = 0;
            $data['is_staff'] = false;
        }

        if($userid != 0){
            $data['userid'] = $userid;
        }

        $survey_staffid=0;
        if($sendcronid != 0){
            $this->load->model('managesurvey/managesurveymodel');
            $recipient = $this->managesurveymodel->getrecipientemail($sendcronid);
            $survey_staffid = $this->managesurveymodel->getsurveystaffid($sendcronid);
            $data['email'] = $recipient;
        }

        if($this->form_validation->run()){
            if ($this->input->post()) {
                $success = $this->surveys_model->add_survey_result($id, $this->input->post());
                if ($success) {
                    $survey = $this->surveys_model->get($id);
                    if ($survey->redirect_url !== '') {
                        redirect($survey->redirect_url);
                    }
                    set_alert('success', 'Thank you for participating in this survey. Your answers are very important to us.');
                    $default_redirect = perfex_do_action('survey_default_redirect', WP_URL."/dashboard");
                    redirect($default_redirect);
                }
            }
        }
        $this->use_navigation = false;
        $this->use_submenu    = false;
        $data['survey']       = $survey;
        $data['title']        = $data['survey']->subject;

        $this->load->model('staff_model');

        $data['stafflist'] = $this->staff_model->getstaffsupport($survey_staffid);
        $this->data           = $data;
        $this->view           = 'survey_view';
        $this->layout();
    }

    public function layout()
    {
        $this->data['use_navigation'] = true;
        if ($this->use_navigation == false) {
            $this->data['use_navigation'] = false;
        }

        $this->data['use_submenu'] = true;
        if ($this->use_submenu == false) {
            $this->data['use_submenu'] = false;
        }

        // $this->template['head'] = '';
        // if ($this->use_head == true) {
        //     $this->template['head'] = $this->load->view('themes/' . active_clients_theme() . '/head', $this->data, true);
        // }

        // $this->template['view'] = $this->load->view('themes/' . active_clients_theme() . '/views/' . $this->view, $this->data, true);

        // $this->template['footer'] = '';
        // if ($this->use_footer == true) {
        //     $this->template['footer'] = $this->load->view('themes/' . active_clients_theme() . '/footer', $this->data, true);
        // }

        // $this->template['scripts'] = '';
        // if ($this->add_scripts == true) {
        //     $this->template['scripts'] = $this->load->view('themes/' . active_clients_theme() . '/scripts', $this->data, true);
        // }

        $this->render_public('crm/survey_view');
    }
}
