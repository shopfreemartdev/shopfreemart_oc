<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contracts extends Admin_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('contracts_model');
    }
    /* List all contracts */
    public function index($clientid = false)
    {
        if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
            access_denied('contracts');
        }
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('contracts', array(
                'clientid' => $clientid
            ));
        }
        $data['chart_types']        = json_encode($this->contracts_model->get_contracts_types_chart_data());
        $data['chart_types_values'] = json_encode($this->contracts_model->get_contracts_types_values_chart_data());
        $data['contract_types']     = $this->contracts_model->get_contract_types();
        $data['years']              = $this->contracts_model->get_contracts_years();
        $data['title']              = lang('contracts');
        $data['chart_js_assets']   = true;
        $this->load->view('crm/admin/contracts/manage', $data);
    }
    /* Edit contract or add new contract */
    public function contract($id = '')
    {
        if ($this->input->post()) {
            if ($id == '') {
                if (!has_permission('contracts', '', 'create')) {
                    access_denied('contracts');
                }
                $id = $this->contracts_model->add($this->input->post());
                if ($id) {
                    set_alert('success', lang('added_successfuly', lang('contract')));
                    redirect(perfex_admin_url('contracts/contract/' . $id));
                }
            } else {
                if (!has_permission('contracts', '', 'edit')) {
                    access_denied('contracts');
                }
                $success = $this->contracts_model->update($this->input->post(), $id);
                if ($success) {
                    set_alert('success', lang('updated_successfuly', lang('contract')));
                }
                redirect(perfex_admin_url('contracts/contract/' . $id));
            }
        }
        if ($id == '') {
            $title = lang('add_new', lang('contract_lowercase'));
        } else {
            $data['contract']                 = $this->contracts_model->get($id, array(), true);
            $data['contract_renewal_history'] = $this->contracts_model->get_contract_renewal_history($id);
            if (!$data['contract'] || (!has_permission('contracts', '', 'view') && $data['contract']->addedfrom != get_staff_user_id())) {
                blank_page(lang('contract_not_found'));
            }
            $contract_merge_fields  = get_available_merge_fields();
            $_contract_merge_fields = array();
            foreach ($contract_merge_fields as $key => $val) {
                foreach ($val as $type => $f) {
                    if ($type == 'contract') {
                        foreach ($f as $available) {
                            foreach ($available['available'] as $av) {
                                if ($av == 'contract') {
                                    array_push($_contract_merge_fields, $f);
                                    break;
                                }
                            }
                            break;
                        }
                    } else if ($type == 'other') {
                        array_push($_contract_merge_fields, $f);
                    } else if ($type == 'clients') {
                        array_push($_contract_merge_fields, $f);
                    }
                }
            }
            $data['contract_merge_fields'] = $_contract_merge_fields;
            $title                         = lang('edit', lang('contract_lowercase'));

            $contact = $this->clients_model->get_contact(get_primary_contact_user_id($data['contract']->client));
            $email   = '';
            if ($contact) {
                $email = $contact->email;
            }

            $template_name         = 'send-contract';
            $data['template']      = get_email_template_for_sending($template_name, $email);
            $data['template_name'] = 'send-contract';
        }

        if ($this->input->get('customer_id')) {
            $data['customer_id']        = $this->input->get('customer_id');
            $data['do_not_auto_toggle'] = true;
        }

        $this->load->model('currencies_model');
        $data['base_currency'] = $this->currencies_model->get_base_currency();
        $data['types']         = $this->contracts_model->get_contract_types();

        $where_clients = 'sfm_uap_affiliates.active=1';

        if (!has_permission('customers', '', 'view')) {
            $where_clients .= ' AND sfm_uap_affiliates.user_id IN (SELECT customer_id FROM main_crm.crm_customeradmins WHERE staff_id=' . get_staff_user_id() . ')';
        }

        $data['clients'] = $this->clients_model->get('', $where_clients);
        if ($id != '') {
            if (total_rows('sfm_uap_affiliates', array(
                'active' => 0,
                'user_id' => $data['contract']->client
            )) > 0 || (total_rows('main_crm.crm_customeradmins', array(
                'staff_id' => get_staff_user_id(),
                'customer_id' => $data['contract']->client
            )) == 0 && !has_permission('customers', '', 'view'))) {
                $data['clients'][] = $this->clients_model->get($data['contract']->client, array(), 'row_array');
            }
        }

        $data['title'] = $title;
        $this->load->view('crm/admin/contracts/contract', $data);
    }
    public function pdf($id)
    {
        if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
            access_denied('contracts');
        }
        if (!$id) {
            redirect(perfex_admin_url('contracts'));
        }
        $contract = $this->contracts_model->get($id);
        $pdf      = contract_pdf($contract);
        $type     = 'D';
        if ($this->input->get('print')) {
            $type = 'I';
        }
        $pdf->Output(slug_it($contract->subject) . '.pdf', $type);
    }
    public function send_to_email($id)
    {
        if (!has_permission('contracts', '', 'view') && !has_permission('contracts', '', 'view_own')) {
            access_denied('contracts');
        }
        $success = $this->contracts_model->send_contract_to_client($id, $this->input->post('attach_pdf'), $this->input->post('cc'));
        if ($success) {
            set_alert('success', lang('contract_sent_to_client_success'));
        } else {
            set_alert('danger', lang('contract_sent_to_client_fail'));
        }
        redirect(perfex_admin_url('contracts/contract/' . $id));
    }
    public function save_contract_data()
    {
        if (!has_permission('contracts', '', 'edit') && !has_permission('contracts', '', 'create')) {
            header('HTTP/1.0 400 Bad error');
            echo json_encode(array(
                'success' => false,
                'message' => lang('access_denied')
            ));
            die;
        }

        $success = false;
        $message = '';
        if ($this->input->post('content')) {
            $this->db->where('id', $this->input->post('contract_id'));
            $this->db->update('main_crm.crm_contracts', array(
                'content' => $this->input->post('content', FALSE)
            ));

            if ($this->db->affected_rows() > 0) {
                $success = true;
                $message = lang('updated_successfuly', lang('contract'));
            }
        }
        echo json_encode(array(
            'success' => $success,
            'message' => $message
        ));
    }
    public function renew()
    {
        if (!has_permission('contracts', '', 'create') && !has_permission('contracts', '', 'edit')) {
            access_denied('contracts');
        }
        if ($this->input->post()) {
            $data    = $this->input->post();
            $success = $this->contracts_model->renew($data);
            if ($success) {
                set_alert('success', lang('contract_renewed_successfuly'));
            } else {
                set_alert('warning', lang('contract_renewed_fail'));
            }
            redirect(perfex_admin_url('contracts/contract/' . $data['contractid'] . '?tab=tab_renewals'));
        }
    }
    public function delete_renewal($renewal_id, $contractid)
    {
        $success = $this->contracts_model->delete_renewal($renewal_id, $contractid);
        if ($success) {
            set_alert('success', lang('contract_renewal_deleted'));
        } else {
            set_alert('warning', lang('contract_renewal_delete_fail'));
        }
        redirect(perfex_admin_url('contracts/contract/' . $contractid . '?tab=tab_renewals'));
    }
    /* Delete contract from database */
    public function delete($id)
    {
        if (!has_permission('contracts', '', 'delete')) {
            access_denied('contracts');
        }
        if (!$id) {
            redirect(perfex_admin_url('contracts'));
        }
        $response = $this->contracts_model->delete($id);
        if ($response == true) {
            set_alert('success', lang('deleted', lang('contract')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('contract_lowercase')));
        }
        redirect(perfex_admin_url('contracts'));
    }
    /* Manage contract types Since Version 1.0.3 */
    public function type($id = '')
    {
        if (!perfex_is_admin()) {
            access_denied('contracts');
        }
        if ($this->input->post()) {
            if (!$this->input->post('id')) {
                $id = $this->contracts_model->add_contract_type($this->input->post());
                if ($id) {
                    $success = true;
                    $message = lang('added_successfuly', lang('contract_type'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
            } else {
                $data = $this->input->post();
                $id   = $data['id'];
                unset($data['id']);
                $success = $this->contracts_model->update_contract_type($data, $id);
                $message = '';
                if ($success) {
                    $message = lang('updated_successfuly', lang('contract_type'));
                }
                echo json_encode(array(
                    'success' => $success,
                    'message' => $message
                ));
            }
        }
    }
    public function types()
    {
        if (!perfex_is_admin()) {
            access_denied('contracts');
        }
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('contract_types');
        }
        $data['title'] = lang('contract_types');
        $this->load->view('crm/admin/contracts/manage_types', $data);
    }
    /* Delete announcement from database */
    public function delete_contract_type($id)
    {
        if (!$id) {
            redirect(perfex_admin_url('contracts/types'));
        }
        if (!perfex_is_admin()) {
            access_denied('contracts');
        }
        $response = $this->contracts_model->delete_contract_type($id);
        if (is_array($response) && isset($response['referenced'])) {
            set_alert('warning', lang('is_referenced', lang('contract_type_lowercase')));
        } else if ($response == true) {
            set_alert('success', lang('deleted', lang('contract_type')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('contract_type_lowercase')));
        }
        redirect(perfex_admin_url('contracts/types'));
    }
    public function add_contract_attachment($id)
    {
        handle_contract_attachment($id);
    }
    public function add_external_attachment()
    {
        if ($this->input->post()) {
            $this->misc_model->add_attachment_to_database($this->input->post('contract_id'), 'contract', $this->input->post('files'), $this->input->post('external'));
        }
    }
    public function delete_contract_attachment($attachment_id)
    {
        $file = $this->misc_model->get_file($attachment_id);
        if ($file->staffid == get_staff_user_id() || perfex_is_admin()) {
            echo json_encode(array(
                'success' => $this->contracts_model->delete_contract_attachment($attachment_id)
            ));
        }
    }
}
