<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Custom_fields extends Admin_Controller
{
    private $pdf_fields = array();
    private $client_portal_fields = array();
    function __construct()
    {
        parent::__construct();
        $this->load->model('custom_fields_model');
        if (!perfex_is_admin()) {
            access_denied('Access Custom Fields');
        }
        // Add the pdf allowed fields
        $this->pdf_fields           = $this->custom_fields_model->get_pdf_allowed_fields();
        $this->client_portal_fields = $this->custom_fields_model->get_client_portal_allowed_fields();
    }
    /* List all custom fields */
    public function index()
    {
        if ($this->input->is_ajax_request()) {
            $this->perfex_base->get_table_data('custom_fields');
        }
        $data['title'] = lang('custom_field');
        $this->load->view('crm/admin/custom_fields/manage', $data);
    }
    public function field($id = '')
    {
        if ($this->input->post()) {
            if ($id == '') {
                $id = $this->custom_fields_model->add($this->input->post());
                if ($id) {
                    set_alert('success', lang('added_successfuly', lang('custom_field')));
                    redirect(perfex_admin_url('custom_fields/field/' . $id));
                }
            } else {
                $success = $this->custom_fields_model->update($this->input->post(), $id);
                if (is_array($success) && isset($success['cant_change_option_custom_field'])) {
                    set_alert('warning', lang('cf_option_in_use'));
                } else if ($success === true) {
                    set_alert('success', lang('updated_successfuly', lang('custom_field')));
                }
                redirect(perfex_admin_url('custom_fields/field/' . $id));
            }
        }
        if ($id == '') {
            $title = lang('add_new', lang('custom_field_lowercase'));
        } else {
            $data['custom_field'] = $this->custom_fields_model->get($id);
            $title                = lang('edit', lang('custom_field_lowercase'));
        }
        $data['pdf_fields']           = $this->pdf_fields;
        $data['client_portal_fields'] = $this->client_portal_fields;
        $data['title']                = $title;
        $this->load->view('crm/admin/custom_fields/customfield', $data);
    }
    /* Delete announcement from database */
    public function delete($id)
    {
        if (!$id) {
            redirect(perfex_admin_url('custom_fields'));
        }
        $response = $this->custom_fields_model->delete($id);
        if ($response == true) {
            set_alert('success', lang('deleted', lang('custom_field')));
        } else {
            set_alert('warning', lang('problem_deleting', lang('custom_field_lowercase')));
        }
        redirect(perfex_admin_url('custom_fields'));
    }
    /* Change survey status active or inactive*/
    public function change_custom_field_status($id, $status)
    {
        if ($this->input->is_ajax_request()) {
            $this->custom_fields_model->change_custom_field_status($id, $status);
        }
    }
}
