<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Authentication extends MX_Controller
{
    function __construct()
    {

        parent::__construct();
        //load_admin_language();

        $this->load->model('Authentication_model');
        $this->load->model('Users_model');  

        $this->load->library('form_validation');

        $this->form_validation->set_message('required', lang('form_validation_required'));
        $this->form_validation->set_message('valid_email', lang('form_validation_valid_email'));
        $this->form_validation->set_message('matches', lang('form_validation_matches'));

    }
    public function index()
    {

        $this->admin();
    }
    public function admin()
    {
        if (is_staff_logged_in()) {
           // redirect(ci_site_url('admin'));
        }

        $this->form_validation->set_rules('password', lang('admin_auth_login_password'), 'required');
        $this->form_validation->set_rules('username', lang('admin_auth_login_email'), 'required');
        if (perfex_get_option('recaptcha_secret_key') != '' && perfex_get_option('recaptcha_site_key') != '' && is_connected('google.com')) {
            $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'callback_recaptcha');
        }
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->login($this->input->post('username'), $this->input->post('password'), $this->input->post('remember'), true);
                if (is_array($success) && isset($success['memberinactive'])) {
                    set_alert('danger', lang('admin_auth_inactive_account'));
                    redirect(ci_site_url('admin/authentication'));
                } else if ($success == false) {
                    set_alert('danger', lang('Invalid username or password'));
                    redirect(ci_site_url('admin/authentication'));
                }

                perfex_do_action('after_staff_login');

                //logactivity of staff LOGIN
                $username = $this->input->post('username');
                $staffid= get_staff_user_id();
                $activitytype = 1;
                crmlogActivity('Staff logged in: '.$username,$activitytype,$staffid);

                $date = date('Y-m-d H:i:s');
                $ip = $this->input->ip_address();
                $this->db->where('staffid', $staffid);
                $this->db->update('main_shopfreemart.sfm_crm_staff', array('last_login' => $date, 'last_ip' => $ip));

                redirect(ci_site_url('admin'));
            }
        }
        $data['title'] = lang('admin_auth_login_heading');
        $this->load->view('authentication/login_admin', $data);
    }
    public function recaptcha($str = '')
    {
        return do_recaptcha_validation($str);
    }
    public function forgot_password()
    {
        if (is_staff_logged_in()) {
            redirect(ci_site_url('admin'));
        }
        $this->form_validation->set_rules('email', lang('admin_auth_login_email'), 'required|callback_email_exists');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->forgot_password($this->input->post('email'), true);
                if (is_array($success) && isset($success['memberinactive'])) {
                    set_alert('danger', lang('inactive_account'));
                    redirect(ci_site_url('authentication/forgot_password'));
                } else if ($success == true) {
                    set_alert('success', lang('check_email_for_reseting_password'));
                    redirect(ci_site_url('authentication/admin'));
                } else {
                    set_alert('danger', lang('error_setting_new_password_key'));
                    redirect(ci_site_url('authentication/forgot_password'));
                }
            }
        }
        $this->load->view('authentication/forgot_password');
    }
    public function reset_password($staff, $user_id, $new_pass_key)
    {
        if (!$this->Authentication_model->can_reset_password($staff, $user_id, $new_pass_key)) {
            set_alert('danger', lang('password_reset_key_expired'));
            redirect(ci_site_url('authentication/admin'));
        }
        $this->form_validation->set_rules('password', lang('admin_auth_reset_password'), 'required');
        $this->form_validation->set_rules('passwordr', lang('admin_auth_reset_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                perfex_do_action('before_user_reset_password', array(
                    'staff' => $staff,
                    'user_id' => $user_id
                ));
                $success = $this->Authentication_model->reset_password($staff, $user_id, $new_pass_key, $this->input->post('passwordr'));
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', lang('password_reset_key_expired'));
                } else if ($success == true) {
                    perfex_do_action('after_user_reset_password', array(
                        'staff' => $staff,
                        'user_id' => $user_id
                    ));
                    set_alert('success', lang('password_reset_message'));
                } else {
                    set_alert('danger', lang('password_reset_message_fail'));
                }
                redirect(ci_site_url('authentication/admin'));
            }
        }
        $this->load->view('authentication/reset_password');
    }
    public function set_password($staff, $user_id, $new_pass_key)
    {
        if (!$this->Authentication_model->can_set_password($staff, $user_id, $new_pass_key)) {
            set_alert('danger', lang('password_reset_key_expired'));
            redirect(ci_site_url('authentication/admin'));
            if ($staff == 1) {
                redirect(ci_site_url('authentication/admin'));
            } else {
                redirect(ci_site_url());
            }
        }
        $this->form_validation->set_rules('password', lang('admin_auth_set_password'), 'required');
        $this->form_validation->set_rules('passwordr', lang('admin_auth_set_password_repeat'), 'required|matches[password]');
        if ($this->input->post()) {
            if ($this->form_validation->run() !== false) {
                $success = $this->Authentication_model->set_password($staff, $user_id, $new_pass_key, $this->input->post('passwordr'));
                if (is_array($success) && $success['expired'] == true) {
                    set_alert('danger', lang('password_reset_key_expired'));
                } else if ($success == true) {
                    set_alert('success', lang('password_reset_message'));
                } else {
                    set_alert('danger', lang('password_reset_message_fail'));
                }
                if ($staff == 1) {
                    redirect(ci_site_url('authentication/admin'));
                } else {
                    redirect(ci_site_url());
                }
            }
        }
        $this->load->view('authentication/set_password');
    }
    public function logout()
    {

        $this->Authentication_model->logout();
        perfex_do_action('after_user_logout');
        redirect(ci_site_url('admin/authentication'));
    }

    public function stafflogout()
    {
         //logactivity of staff LOGIN
            $staffid = get_staff_user_id();
            $this->db->select('username');
            $this->db->where('staffid', $staffid);
            $staffdata = $this->db->get('main_shopfreemart.sfm_crm_staff')->row();
            $username = $staffdata->username;
            $activitytype = 2;
            crmlogActivity('Staff logged out: '.$username, $activitytype, $staffid);

        $this->Authentication_model->stafflogout();
        
        perfex_do_action('after_user_logout');
        redirect(ci_site_url('admin/authentication?logged_out=1'));
    }

    public function email_exists($email)
    {
        $total_rows = total_rows('sfm_uap_affiliates', array(
            'email' => $email
        ));
        if ($total_rows == 0) {
            $this->form_validation->set_message('email_exists', lang('auth_reset_pass_email_not_found'));
            return false;
        }
        return true;
    }
}
