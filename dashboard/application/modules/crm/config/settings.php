<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('perfex_APPPATH')){
        define('perfex_APPPATH',  realpath(__DIR__ . '/..').DIRECTORY_SEPARATOR);   
}
define('CRM_MODULE_PATH',perfex_APPPATH);
// Used for phpass_helper
define('PHPASS_HASH_STRENGTH', 8);
define('PHPASS_HASH_PORTABLE', FALSE);
// Admin url
define('perfex_admin_url', ci_site_url('admin'));
define('perfex_CLIENT_URL', ci_site_url());
// CRM server update url
define('UPDATE_URL','https://www.perfexcrm.com/perfex_updates/index.php');
// Get latest version info
define('UPDATE_INFO_URL','https://www.perfexcrm.com/perfex_updates/update_info.php');

// Defined folders
// CRM temporary path


define('TEMP_FOLDER',FCPATH .'temp' . '/');
// Database backups folder
define('BACKUPS_FOLDER',FCPATH.'backups'.'/');
// Customer attachments folder from profile
define('CLIENT_ATTACHMENTS_FOLDER',FCPATH.'uploads/clients'.'/');
// All tickets attachments
define('TICKET_ATTACHMENTS_FOLDER',FCPATH .'uploads/ticket_attachments' . '/');
// Company attachemnts, favicon,logo etc..
define('COMPANY_FILES_FOLDER',FCPATH .'uploads/company' . '/');
// Staff profile images
define('STAFF_PROFILE_IMAGES_FOLDER',FCPATH .'uploads/staff_profile_images' . '/');
// Contact profile images
define('CONTACT_PROFILE_IMAGES_FOLDER',FCPATH .'uploads/client_profile_images' . '/');
// Newsfeed attachments
define('NEWSFEED_FOLDER',FCPATH . 'uploads/newsfeed' . '/');
// Contracts attachments
define('CONTRACTS_UPLOADS_FOLDER',FCPATH . 'uploads/contracts' . '/');
// Tasks attachments
define('TASKS_ATTACHMENTS_FOLDER',FCPATH . 'uploads/tasks' . '/');
// Invoice attachments
define('INVOICE_ATTACHMENTS_FOLDER',FCPATH . 'uploads/invoices' . '/');
// Estimate attachments
define('ESTIMATE_ATTACHMENTS_FOLDER',FCPATH . 'uploads/estimates' . '/');
// Proposal attachments
define('PROPOSAL_ATTACHMENTS_FOLDER',FCPATH . 'uploads/proposals' . '/');
// Expenses receipts
define('EXPENSE_ATTACHMENTS_FOLDER',FCPATH . 'uploads/expenses' . '/');
// Lead attachments
define('LEAD_ATTACHMENTS_FOLDER',FCPATH . 'uploads/leads' . '/');
// Project files attachments
define('PROJECT_ATTACHMENTS_FOLDER',FCPATH . 'uploads/projects' . '/');
// Project discussions attachments
define('PROJECT_DISCUSSION_ATTACHMENT_FOLDER',FCPATH . 'uploads/discussions' . '/');

global $crm_modules;
 $crm_modules =  array(
        'contracts' => array(
            'view' => true,
            'view_own' => true,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'tasks' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true,
            'help' => lang('help_tasks_permissions')
        ),
        'reports' => array(
            'view' => true,
            'view_own' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ),
        'settings' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => false,
            'delete' => false
        ),
        'projects' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true,
            'help' => lang('help_project_permissions')
        ),
        'surveys' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'staff' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'customers' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'email_templates' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => false,
            'delete' => false
        ),
        'roles' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'expenses' => array(
            'view' => true,
            'view_own' => true,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'bulk_pdf_exporter' => array(
            'view' => true,
            'view_own' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ),
        'goals' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'knowledge_base' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'proposals' => array(
            'view' => true,
            'view_own' => true,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'estimates' => array(
            'view' => true,
            'view_own' => true,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'payments' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'invoices' => array(
            'view' => true,
            'view_own' => true,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'items' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'downlines' => array(
            'view' => true,
            'view_own' => false,
            'edit' => true,
            'create' => true,
            'delete' => true
        ),
        'changesponsor' => array(
            'view' => false,
            'view_own' => false,
            'edit' => true,
            'create' => false,
            'delete' => false
        ),
        'gotomemberdashboard' => array(
            'view' => true,
            'view_own' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ),
        'addfounding' => array(
            'view' => false,
            'view_own' => false,
            'edit' => false,
            'create' => true,
            'delete' => false
        ),
        'viewnetwork' => array(
            'view' => true,
            'view_own' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ),
        'adminadjustment' => array(
            'view' => false,
            'view_own' => false,
            'edit' => false,
            'create' => true,
            'delete' => false
        ),
        'viewtransactions' => array(
            'view' => true,
            'view_own' => false,
            'edit' => false,
            'create' => false,
            'delete' => false
        ),
        'regenerateordertotals' => array(
            'view' => false,
            'view_own' => false,
            'edit' => true,
            'create' => false,
            'delete' => false
        ),
        'regeneratecommissions' => array(
            'view' => false,
            'view_own' => false,
            'edit' => true,
            'create' => false,
            'delete' => false
        ),
        'membersurveyform' => array(
            'view' => false,
            'view_own' => false,
            'edit' => false,
            'create' => true,
            'delete' => false
        )
);




