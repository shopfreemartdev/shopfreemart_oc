<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('perfex_APPPATH')){
		define('perfex_APPPATH',  realpath(__DIR__ . '/..').DIRECTORY_SEPARATOR);   
		include_once perfex_APPPATH.'config/settings.php';
}

$CI = &get_instance();
$CI->load->helper('perfex_files');

$autoload['drivers'] = array();
array_push($autoload['drivers'],'session');

$autoload['libraries'] = array( 'user_agent', 'encryption','email', 'encoding_lib', 'action_hooks', 'perfex_base' );
array_unshift($autoload['libraries'],'database');
//$gateways = list_files(APPPATH.'/libraries/gateways');
//foreach($gateways as $gateway){
//    $pathinfo =  pathinfo($gateway);
//    // Check if file is .php and do not starts with .dot
//    // Offen happens Mac os user to have underscore prefixed files while unzipping the zip file.
//    if($pathinfo['extension'] == 'php' && 0 !== strpos($gateway, '.')){
//        array_push($autoload['libraries'],'gateways/'.strtolower($pathinfo['filename']));
//    }
//}

$autoload['helper'] = array(
        'url',
        'file',
        'form',
        'perfex_misc',
        'perfex_custom_fields',
        'perfex_merge_fields',
        'perfex_html',
        'perfex_db',
        'perfex_upload',
        'perfex_sales',
        'perfex_themes',
        'perfex_theme_style',
    );


$autoload['language'] = array('english');
$autoload['model'] = array( 'misc_model' , 'roles_model' , 'clients_model' , 'customers_model','tasks_model' );

