<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class _sfm_uap_affiliates extends MY_Model {

	private $primary_key 	= 'id';
	private $table_name 	= 'sfm_uap_affiliates';
	private $field_search 	= ['user_id', 'display_name', 'username', 'firstname', 'lastname', 'email', 'phone', 'rank', 'company', 'address1', 'address2', 'city', 'state', 'zipcode', 'country', 'shipping_address1', 'shipping_address2', 'shipping_city', 'shipping_state', 'shipping_zipcode', 'shipping_country', 'paid', 'replicated_url', 'sponsorkey', 'foundingpositions', 'upline_diamond', 'upline_doublediamond', 'upline_triplediamond', 'upline_ambassador', 'upline_crown', 'date_registered', 'downlines', 'referralsales', 'override_referralsales', 'earnings', 'commissions', 'commissions_overall', 'commissions_debits', 'coins', 'coins_overall', 'coins_debits', 'ewallet', 'withdrawns', 'visits', 'conversion', 'firstpurchasedate', 'orders', 'peinding_orders', 'unpaid_invoices', 'paid_invoices', 'fm_commissions', 'fm_commissions_overall', 'fm_commissions_debits', 'fm_coins', 'fm_coins_overall', 'fm_coins_debits', 'reserved_commissions', 'reserved_coins', 'pending_commissions', 'pending_coins', 'pending_withdrawals', 'lastdate', 'deleted', 'uid', 'rank_id', 'start_data', 'open_tickets', 'closed_tickets', 'status', 'user_type', 'perfex_is_admin'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= $field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . $field . " LIKE '%" . $q . "%' )";
        }

        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= $field . " LIKE '%" . $q . "%' ";
	            } else {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }

	        $where = '('.$where.')';
        } else {
        	$where .= "(" . $field . " LIKE '%" . $q . "%' )";
        }

        if (is_array($select_field) AND count($select_field)) {
        	$this->db->select($select_field);
        }

        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by($this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}

}

/* End of file Model_sfm_uap_affiliates.php */
/* Location: ./application/models/Model_sfm_uap_affiliates.php */