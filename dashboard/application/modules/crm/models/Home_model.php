<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home_model extends CRM_Model 
{
    private $perfex_is_admin;
    function __construct()
    {
        parent::__construct();
        $this->perfex_is_admin = perfex_is_admin();
    } 
    /**
     * @return array
     * Used in home dashboard page
     * Return all upcoming events this week
     */
    public function get_upcoming_events()
    {
 
        $this->db->where('(start BETWEEN "' . date('Y-m-d', strtotime('monday this week')) . '" AND "' . date('Y-m-d', strtotime('sunday this week')) . '")');
        $this->db->where('(user_id = ' . get_staff_user_id() . ' OR public = 1)');
        $this->db->order_by('start', 'desc');
        $this->db->limit(6);
        return $this->db->get('main_crm.crm_events')->result_array();
    }  
    /**
     * @param  integer (optional) Limit upcoming events
     * @return integer
     * Used in home dashboard page
     * Return total upcoming events next week
     */
    public function get_upcoming_events_next_week()
    {
        $monday_this_week = date('Y-m-d', strtotime('monday next week'));
        $sunday_this_week = date('Y-m-d', strtotime('sunday next week'));
        $this->db->where('(start BETWEEN "' . $monday_this_week . '" AND "' . $sunday_this_week . '")');
        $this->db->where('(user_id = ' . get_staff_user_id() . ' OR public = 1)');
        return $this->db->count_all_results('main_crm.crm_events');
    }
    /**
     * @param  mixed
     * @return array
     * Used in home dashboard page, currency passed from javascript (undefined or integer)
     * Displays weekly payment statistics (chart)
     */
    public function get_weekly_payments_statistics($currency)
    {

        $all_payments                 = array();
        $has_permission_payments_view = has_permission('payments', '', 'view');
        $this->db->select('amount,main_crm.crm_invoicepaymentrecords.date');
        $this->db->from('main_crm.crm_invoicepaymentrecords');
        $this->db->join('main_crm.crm_invoices', 'main_crm.crm_invoices.id = main_crm.crm_invoicepaymentrecords.invoiceid');
        $this->db->where('CAST(main_crm.crm_invoicepaymentrecords.date as DATE) >= "' . date('Y-m-d', strtotime('monday this week')) . '" AND CAST(main_crm.crm_invoicepaymentrecords.date as DATE) <= "' . date('Y-m-d', strtotime('sunday this week')) . '"');
        $this->db->where('main_crm.crm_invoices.status !=', 5);
        if ($currency != 'undefined') {
            $this->db->where('currency', $currency);
        }

        if (!$has_permission_payments_view) {
            $this->db->where('invoiceid IN (SELECT id FROM main_crm.crm_invoices WHERE addedfrom=' . get_staff_user_id() . ')');
        }

        // Current week
        $all_payments[] = $this->db->get()->result_array();
        $this->db->select('amount,main_crm.crm_invoicepaymentrecords.date');
        $this->db->from('main_crm.crm_invoicepaymentrecords');
        $this->db->join('main_crm.crm_invoices', 'main_crm.crm_invoices.id = main_crm.crm_invoicepaymentrecords.invoiceid');
        $this->db->where('CAST(main_crm.crm_invoicepaymentrecords.date as DATE) >= "' . date('Y-m-d', strtotime('monday last week', strtotime('last sunday'))) . '" AND CAST(main_crm.crm_invoicepaymentrecords.date as DATE) <= "' . date('Y-m-d', strtotime('sunday last week', strtotime('last sunday'))) . '"');

        $this->db->where('main_crm.crm_invoices.status !=', 5);
        if ($currency != 'undefined') {
            $this->db->where('currency', $currency);
        }
        // Last Week
        $all_payments[] = $this->db->get()->result_array();

        $chart = array(
            'labels' => get_weekdays(),
            'datasets' => array(
                array(
                    'label' => lang('this_week_payments'),
                    'backgroundColor' => 'rgba(37,155,35,0.2)',
                    'borderColor' => "#84c529",
                    'borderWidth' => 1,
                    'tension' => false,
                    'data' => array(
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                    )
                ),
                array(
                    'label' => lang('last_week_payments'),
                    'backgroundColor' => 'rgba(197, 61, 169, 0.5)',
                    'borderColor' => "#c53da9",
                    'borderWidth' => 1,
                    'tension' => false,
                    'data' => array(
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0
                    )
                )
            )
        );


        for ($i = 0; $i < count($all_payments); $i++) {
            foreach ($all_payments[$i] as $payment) {
                $payment_day = date('l', strtotime($payment['date']));
                $x           = 0;
                foreach (get_weekdays_original() as $day) {
                    if ($payment_day == $day) {
                        $chart['datasets'][$i]['data'][$x] += $payment['amount'];
                    }
                    $x++;
                }
            }
        }
        return $chart;
    }

    public function projects_status_stats()
    {
        $this->load->model('projects_model');
        $statuses = $this->projects_model->get_project_statuses();
        $colors   = get_system_favourite_colors();

        $chart = array(
            'labels' => array(),
            'datasets' => array()
        );

        $_data                         = array();
        $_data['data']                 = array();
        $_data['backgroundColor']      = array();
        $_data['hoverBackgroundColor'] = array();

        $i              = 0;
        $has_permission = has_permission('projects', '', 'view');
        foreach ($statuses as $status) {
            $this->db->where('status', $status);
            if (!$has_permission) {
                $this->db->where('id IN (SELECT project_id FROM main_crm.crm_projectmembers WHERE staff_id=' . get_staff_user_id() . ')');
            }

            array_push($chart['labels'], project_status_by_id($status));
            array_push($_data['backgroundColor'], $colors[$i]);
            array_push($_data['hoverBackgroundColor'], adjust_color_brightness($colors[$i], -20));
            array_push($_data['data'], $this->db->count_all_results('main_crm.crm_projects'));

            $i++;
        }
        $chart['datasets'][]           = $_data;
        $chart['datasets'][0]['label'] = lang('home_stats_by_project_status');
        return $chart;
    }
    public function leads_status_stats()
    {
        $this->load->model('leads_model');
        $statuses = $this->leads_model->get_status();
        $colors   = get_system_favourite_colors();
        $chart    = array(
            'labels' => array(),
            'datasets' => array()
        );

        $_data                         = array();
        $_data['data']                 = array();
        $_data['backgroundColor']      = array();
        $_data['hoverBackgroundColor'] = array();

        foreach ($statuses as $status) {
            $this->db->where('status', $status['id']);
            if (!$this->perfex_is_admin) {
                $this->db->where('(addedfrom = ' . get_staff_user_id() . ' OR is_public = 1 OR assigned = ' . get_staff_user_id() . ')');
            }
            if($status['color'] == ''){
                $status['color'] = '#737373';
            }
            array_push($chart['labels'], $status['name']);
            array_push($_data['backgroundColor'], $status['color']);
            array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['color'], -20));
            array_push($_data['data'], $this->db->count_all_results('main_crm.crm_leads'));
        }

        $chart['datasets'][] = $_data;
        return $chart;

    }
    /**
     * Display total tickets awaiting reply by department (chart)
     * @return array
     */
    public function tickets_awaiting_reply_by_department()
    {
        $this->load->model('departments_model');
        $departments = $this->departments_model->get();
        $colors      = get_system_favourite_colors();
        $chart       = array(
            'labels' => array(),
            'datasets' => array()
        );

        $_data                         = array();
        $_data['data']                 = array();
        $_data['backgroundColor']      = array();
        $_data['hoverBackgroundColor'] = array();

        $i = 0;
        foreach ($departments as $department) {
            if (!$this->perfex_is_admin) {
                if (perfex_get_option('staff_access_only_assigned_departments') == 1) {
                    $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                    $departments_ids      = array();
                    if (count($staff_deparments_ids) == 0) {
                        $departments = $this->departments_model->get();
                        foreach ($departments as $department) {
                            array_push($departments_ids, $department['departmentid']);
                        }
                    } else {
                        $departments_ids = $staff_deparments_ids;
                    }
                    if (count($departments_ids) > 0) {
                        $this->db->where('department IN (SELECT departmentid FROM main_shopfreemart.sfm_crm_staffdepartments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                    }
                }
            }
            $this->db->where_in('status', array(
                1,
                2,
                4
            ));

            $this->db->where('department', $department['departmentid']);
            $total = $this->db->count_all_results('main_shopfreemart.sfm_crm_tickets');

            if ($total > 0) {
                $color = '#333';
                if (isset($colors[$i])) {
                    $color = $colors[$i];
                }
                array_push($chart['labels'], $department['name']);
                array_push($_data['backgroundColor'], $color);
                array_push($_data['hoverBackgroundColor'], adjust_color_brightness($color, -20));
                array_push($_data['data'], $total);
            }
            $i++;
        }

        $chart['datasets'][] = $_data;
        return $chart;
    }
    /**
     * Display total tickets awaiting reply by status (chart)
     * @return array
     */
    public function tickets_awaiting_reply_by_status()
    {
        $this->load->model('tickets_model');
        $statuses             = $this->tickets_model->get_ticket_status();
        $_statuses_with_reply = array(
            1,
            2,
            4
        );

        $chart = array(
            'labels' => array(),
            'datasets' => array()
        );

        $_data                         = array();
        $_data['data']                 = array();
        $_data['backgroundColor']      = array();
        $_data['hoverBackgroundColor'] = array();

        foreach ($statuses as $status) {
            if (in_array($status['ticketstatusid'], $_statuses_with_reply)) {

                if (!$this->perfex_is_admin) {
                    if (perfex_get_option('staff_access_only_assigned_departments') == 1) {
                        $staff_deparments_ids = $this->departments_model->get_staff_departments(get_staff_user_id(), true);
                        $departments_ids      = array();
                        if (count($staff_deparments_ids) == 0) {
                            $departments = $this->departments_model->get();
                            foreach ($departments as $department) {
                                array_push($departments_ids, $department['departmentid']);
                            }
                        } else {
                            $departments_ids = $staff_deparments_ids;
                        }
                        if (count($departments_ids) > 0) {
                            $this->db->where('department IN (SELECT departmentid FROM main_shopfreemart.sfm_crm_staffdepartments WHERE departmentid IN (' . implode(',', $departments_ids) . ') AND staffid="' . get_staff_user_id() . '")');
                        }
                    }
                }

                $this->db->where('status', $status['ticketstatusid']);
                $total = $this->db->count_all_results('main_shopfreemart.sfm_crm_tickets');
                if ($total > 0) {
                    array_push($chart['labels'], ticket_status_translate($status['ticketstatusid']));
                    array_push($_data['backgroundColor'], $status['statuscolor']);
                    array_push($_data['hoverBackgroundColor'], adjust_color_brightness($status['statuscolor'], -20));
                    array_push($_data['data'], $total);
                }
            }
        }

        $chart['datasets'][] = $_data;
        return $chart;
    }


    public function getStatistics($stat_range = '') {
        $results = $range_query = array();

        if ($stat_range === '') return $results;

        if ($stat_range === 'today') {
            $range_query = array('DATE(date_added)' => date('Y-m-d'));
        } else if ($stat_range === 'week') {
            $range_query = array('WEEK(date_added)' => date('W'));
        } else if ($stat_range === 'month') {
            $range_query = array('MONTH(date_added)' => date('m'));
        } else if ($stat_range === 'year') {
            $range_query = array('YEAR(date_added)' => date('Y'));
        }

        $results['sales'] = $this->getTotalSales($range_query);
        $results['lost_sales'] = $this->getTotalLostSales($range_query);
        $results['customers'] = $this->getTotalCustomers($range_query);
        $results['sfm_orders'] = $this->getTotalOrders($range_query);
        $results['orders_completed'] = $this->getTotalOrdersCompleted($range_query);
        $results['delivery_orders'] = $this->getTotalDeliveryOrders($range_query);
        $results['collection_orders'] = $this->getTotalCollectionOrders($range_query);
        $results['tables_reserved'] = $this->getTotalTablesReserved($range_query);
        $results['cash_payments'] = $this->getTotalCashPayments($range_query);

        return $results;
    }

    public function getTotalMenus() {
        return $this->db->count_all('menus');
    }

    public function getTotalSales($range_query) {
        $total_sales = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->select_sum('order_total', 'total_sales');
            $this->db->where('status_id >', '0');
            $this->db->where($range_query);
            $query = $this->db->get('sfm_orders');

            if ($query->num_rows() > 0) {
                $row = $query->row_array();
                $total_sales = $row['total_sales'];
            }
        }

        return $total_sales;
    }

    public function getTotalLostSales($range_query) {
        $total_lost_sales = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->select_sum('order_total', 'total_lost_sales');
            $this->db->where($range_query);

            $this->db->group_start();
            $this->db->where('status_id <=', '0');
            $this->db->or_where('status_id', $this->config->item('canceled_order_status'));
            $this->db->group_end();

            $query = $this->db->get('sfm_orders');

            if ($query->num_rows() > 0) {
                $row = $query->row_array();
                $total_lost_sales = $row['total_lost_sales'];
            }
        }

        return $total_lost_sales;
    }

    public function getTotalCashPayments($range_query = '') {
        $cash_payments = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->select_sum('order_total', 'cash_payments');
            $this->db->where('status_id >', '0');
            $this->db->where('payment', 'cod');
            $this->db->where($range_query);
            $query = $this->db->get('sfm_orders');

            if ($query->num_rows() > 0) {
                $row = $query->row_array();
                $cash_payments = $row['cash_payments'];
            }
        }

        return $cash_payments;
    }

    public function getTotalCustomers($range_query) {
        $total_customers = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->where($range_query);
            $this->db->from('sfm_uap_affiliates');
            $total_customers = $this->db->count_all_results();
        }

        return $total_customers;
    }

    public function getTotalOrders($range_query) {
        $total_orders = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->where($range_query);
            $this->db->from('sfm_orders');
            $total_orders = $this->db->count_all_results();
        }

        return $total_orders;
    }

    public function getTotalOrdersCompleted($range_query = '') {
        $total_orders_completed = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->where($range_query);
            $this->db->where_in('status_id', (array) $this->config->item('completed_order_status'));
            $this->db->from('sfm_orders');
            $total_orders_completed = $this->db->count_all_results();
        }

        return $total_orders_completed;
    }

    public function getTotalDeliveryOrders($range_query = '') {
        $total_delivery_orders = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->where($range_query);
            $this->db->where('order_type', '1');
            $this->db->from('sfm_orders');

            $total_delivery_orders = $this->db->count_all_results();
        }

        return $total_delivery_orders;
    }

    public function getTotalCollectionOrders($range_query = '') {
        $total_collection_orders = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->where($range_query);
            $this->db->where('order_type', '2');
            $this->db->from('sfm_orders');

            $total_collection_orders = $this->db->count_all_results();
        }

        return $total_collection_orders;
    }

    public function getTotalTables() {
        return $this->db->count_all_results('tables');
    }

    public function getTotalTablesReserved($range_query = '') {
        $total_tables_reserved = 0;

        if (is_array($range_query) AND ! empty($range_query)) {
            $this->db->where($range_query);
            $this->db->where('status >', '0');
            $this->db->from('reservations');
            $total_tables_reserved = $this->db->count_all_results();
        }

        return $total_tables_reserved;
    }

    public function getTodayChart($hour = FALSE) {
        $result = array();

        $this->db->where('DATE(date_registered)', 'DATE(NOW())');
        $this->db->where('HOUR(date_registered)', $hour);
        $this->db->order_by('date_registered', 'ASC');
        if ($this->db->from('sfm_uap_affiliates')) {
            $result['sfm_uap_affiliates'] = $this->db->count_all_results();
        }

        $this->db->where('status_id >', '0');
        $this->db->where('DATE(date_added)', 'DATE(NOW())', FALSE);
        $this->db->where('HOUR(order_time)', $hour);
        $this->db->group_by('HOUR(order_time)');
        $this->db->order_by('date_added', 'ASC');
        if ($this->db->from('sfm_order')) {
            $result['sfm_orders'] = $this->db->count_all_results();
        }

        $this->db->where('status >', '0');
        $this->db->where('DATE(reserve_date)', 'DATE(NOW())', FALSE);
        $this->db->where('HOUR(reserve_time)', $hour);
        $this->db->group_by('HOUR(reserve_time)');
        $this->db->order_by('reserve_date', 'ASC');
        if ($this->db->from('reservations')) {
            $result['reservations'] = $this->db->count_all_results();
        }

        $this->db->where('DATE(date_added)', 'DATE(NOW())', FALSE);
        $this->db->where('HOUR(date_added)', $hour);
        $this->db->order_by('date_added', 'ASC');
        if ($this->db->from('reviews')) {
            $result['reviews'] = $this->db->count_all_results();
        }

        return $result;
    }

    public function getDateChart($date = FALSE) {
        $result = array();

        $this->db->where('DATE(date_registered)', $date);
        $this->db->group_by('DAY(date_registered)');
        if ($this->db->from('sfm_uap_affiliates')) {
            $result['sfm_uap_affiliates'] = $this->db->count_all_results();
        }

        $this->db->where('status_id >', '0');
        $this->db->where('DATE(date_added)', $date);
        $this->db->group_by('DAY(date_added)');
        if ($this->db->from('sfm_orders')) {
            $result['sfm_orders'] = $this->db->count_all_results();
        }

        $this->db->where('status >', '0');
        $this->db->where('DATE(reserve_date)', $date);
        $this->db->group_by('DAY(reserve_date)');
        if ($this->db->from('reservations')) {
            $result['reservations'] = $this->db->count_all_results();
        }

        $this->db->where('DATE(date_added)', $date);
        $this->db->group_by('DAY(date_added)');
        if ($this->db->from('reviews')) {
            $result['reviews'] = $this->db->count_all_results();
        }

        return $result;
    }

    public function getYearChart($year = FALSE, $month = FALSE) {
        $result = array();

        $this->db->where('YEAR(date_registered)', (int) $year);
        $this->db->where('MONTH(date_registered)', (int) $month);
        $this->db->group_by('MONTH(date_registered)');
        if ($this->db->from('sfm_uap_affiliates')) {
            $result['sfm_uap_affiliates'] = $this->db->count_all_results();
        }

        $this->db->where('status_id >', '0');
        $this->db->where('YEAR(date_added)', (int) $year);
        $this->db->where('MONTH(date_added)', (int) $month);
        $this->db->group_by('MONTH(date_added)');
        if ($this->db->from('sfm_orders')) {
            $result['sfm_orders'] = $this->db->count_all_results();
        }

        $this->db->where('status >', '0');
        $this->db->where('YEAR(reserve_date)', (int) $year);
        $this->db->where('MONTH(reserve_date)', (int) $month);
        $this->db->group_by('MONTH(reserve_date)');
        if ($this->db->from('reservations')) {
            $result['reservations'] = $this->db->count_all_results();
        }

        $this->db->where('YEAR(date_added)', (int) $year);
        $this->db->where('MONTH(date_added)', (int) $month);
        $this->db->group_by('MONTH(date_added)');
        if ($this->db->from('reviews')) {
            $result['reviews'] = $this->db->count_all_results();
        }

        return $result;
    }

    public function getReviewChart($rating_id, $menu_id) {
        $total_ratings = 0;
        $this->db->where('menu_id', $menu_id);
        $this->db->where('rating_id', $rating_id);
        $this->db->from('reviews');
        $total_ratings = $this->db->count_all_results();

        return $total_ratings;
    }

    public function getTopCustomers($filter = array()) {
        if ( ! empty($filter['page']) AND $filter['page'] !== 0) {
            $filter['page'] = ($filter['page'] - 1) * $filter['limit'];
        }

        if ($this->db->limit($filter['limit'], $filter['page'])) {
            $this->db->select('sfm_uap_affiliates.user_id, sfm_uap_affiliates.firstname, sfm_uap_affiliates.lastname, COUNT(sfm_orders.id) AS total_orders');
            $this->db->select_sum('order_total', 'total_sale');
            $this->db->from('sfm_uap_affiliates');
            $this->db->join('sfm_orders', 'sfm_orders.customer_id = sfm_uap_affiliates.user_id', 'left');
            $this->db->group_by('sfm_orders.customer_id');
            $this->db->order_by('total_orders', 'DESC');

            $query = $this->db->get();
            $result = array();

            if ($query->num_rows() > 0) {
                $result = $query->result_array();
            }

            return $result;
        }
    }

    public function getNewsFeed($number = 5, $expiry = 3) {
        $this->load->library('feed_parser');

        $this->feed_parser->set_feed_url('http://feeds.feedburner.com/Tastyigniter');
        $this->feed_parser->set_cache_life($expiry);

        return $this->feed_parser->getFeed($number);
    }



    public function getTotalOrdersByDay($date = '') {
        $implode = array();

        $implode= $this->config->item('config_complete_status');
        $order_data = array();

        for ($i = 0; $i < 24; $i++) {
            $order_data[$i] = array(
                'hour'  => $i,
                'total' => 0
            );
        }

        if ($date == '')
        {
            $date = "NOW()";
        }
        else
        {
            $date = "STR_TO_DATE('".$date."','%M %d,%Y')";
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, HOUR(date_added) AS hour FROM " . DB_PREFIX . "order WHERE order_status_id IN(" . implode(",", $implode) . ") AND DATE(date_added) = ".$date." GROUP BY HOUR(date_added) ORDER BY date_added ASC");

        foreach ($query->result_array() as $result) {
            $order_data[$result['hour']] = array(
                'hour'  => $result['hour'],
                'total' => $result['total']
            );
        }
        return $order_data;
    }



    public function getTotalOrdersByWeek() {
        $implode = array();

        $implode= $this->config->item('config_complete_status');

        $order_data = array();

        $date_start = strtotime('-' . date('w') . ' days');

        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', $date_start + ($i * 86400));

            $order_data[date('w', strtotime($date))] = array(
                'day'   => date('D', strtotime($date)),
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, date_added FROM " . DB_PREFIX . "order WHERE order_status_id IN(" . implode(",", $implode) . ") AND DATE(date_added) >= DATE('" . $this->db->escape(date('Y-m-d', $date_start)) . "') GROUP BY DAYNAME(date_added)");

        foreach ($query->result_array() as $result) {
            $order_data[date('w', strtotime($result['date_added']))] = array(
                'day'   => date('D', strtotime($result['date_added'])),
                'total' => $result['total']
            );
        }

        return $order_data;
    }

    public function getTotalOrdersByMonth() {
        $implode = array();

        $implode= $this->config->item('config_complete_status');

        $order_data = array();

        for ($i = 1; $i <= date('t'); $i++) {
            $date = date('Y') . '-' . date('m') . '-' . $i;

            $order_data[date('j', strtotime($date))] = array(
                'day'   => date('d', strtotime($date)),
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, date_added FROM " . DB_PREFIX . "order WHERE order_status_id IN(" . implode(",", $implode) . ") AND DATE(date_added) >= " . $this->db->escape(date('Y') . '-' . date('m') . '-1') . " GROUP BY DATE(date_added)");

        foreach ($query->result_array() as $result) {
            $order_data[date('j', strtotime($result['date_added']))] = array(
                'day'   => date('d', strtotime($result['date_added'])),
                'total' => $result['total']
            );
        }

        return $order_data;
    }

    public function getTotalOrdersByYear() {
        $implode = array();

        $implode= $this->config->item('config_complete_status');

        $order_data = array();

        for ($i = 1; $i <= 12; $i++) {
            $order_data[$i] = array(
                'month' => date('M', mktime(0, 0, 0, $i)),
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, date_added FROM " . DB_PREFIX . "order WHERE order_status_id IN(" . implode(",", $implode) . ") AND YEAR(date_added) = YEAR(NOW()) GROUP BY MONTH(date_added)");

        foreach ($query->result_array() as $result) {
            $order_data[date('n', strtotime($result['date_added']))] = array(
                'month' => date('M', strtotime($result['date_added'])),
                'total' => $result['total']
            );
        }

        return $order_data;
    }
    
    public function getTotalCustomersByDay() {
        $customer_data = array();

        for ($i = 0; $i < 24; $i++) {
            $customer_data[$i] = array(
                'hour'  => $i,
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, HOUR(date_registered) AS hour FROM " . DB_PREFIX . "uap_affiliates WHERE DATE(date_registered) = DATE(NOW()) GROUP BY HOUR(date_registered) ORDER BY date_registered ASC");

        foreach ($query->result_array() as $result) {
            $customer_data[$result['hour']] = array(
                'hour'  => $result['hour'],
                'total' => $result['total']
            );
        }

        return $customer_data;
    }



    public function getTotalCustomersByWeek() {
        $customer_data = array();

        $date_start = strtotime('-' . date('w') . ' days');

        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', $date_start + ($i * 86400));

            $customer_data[date('w', strtotime($date))] = array(
                'day'   => date('D', strtotime($date)),
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, date_registered FROM " . DB_PREFIX . "uap_affiliates WHERE DATE(date_registered) >= DATE('" . $this->db->escape(date('Y-m-d', $date_start)) . "') GROUP BY DAYNAME(date_registered)");

        foreach ($query->result_array() as $result) {
            $customer_data[date('w', strtotime($result['date_registered']))] = array(
                'day'   => date('D', strtotime($result['date_registered'])),
                'total' => $result['total']
            );
        }

        return $customer_data;
    }

    public function getTotalCustomersByMonth() {
        $customer_data = array();

        for ($i = 1; $i <= date('t'); $i++) {
            $date = date('Y') . '-' . date('m') . '-' . $i;

            $customer_data[date('j', strtotime($date))] = array(
                'day'   => date('d', strtotime($date)),
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, date_registered FROM sfm_uap_affiliates WHERE DATE(date_registered) >= " . $this->db->escape(date('Y') . '-' . date('m') . '-1') . " GROUP BY DATE(date_registered)");

        foreach ($query->result_array() as $result) {
            $customer_data[date('j', strtotime($result['date_registered']))] = array(
                'day'   => date('d', strtotime($result['date_registered'])),
                'total' => $result['total']
            );
        }

        return $customer_data;
    }

    public function getTotalCustomersByYear() {
        $customer_data = array();

        for ($i = 1; $i <= 12; $i++) {
            $customer_data[$i] = array(
                'month' => date('M', mktime(0, 0, 0, $i)),
                'total' => 0
            );
        }

        $query = $this->db->query("SELECT COUNT(*) AS total, date_registered FROM " . DB_PREFIX . "uap_affiliates WHERE YEAR(date_registered) = YEAR(NOW()) GROUP BY MONTH(date_registered)");

        foreach ($query->result_array() as $result) {
            $customer_data[date('n', strtotime($result['date_registered']))] = array(
                'month' => date('M', strtotime($result['date_registered'])),
                'total' => $result['total']
            );
        }

        return $customer_data;
    }   


    public function getTotalOrdersByCountry() {
        $implode = array();
        $implode= $this->config->item('config_complete_status');
        if ($implode) {
            $query = $this->db->query("SELECT COUNT(*) AS total, SUM(o.total) AS amount, c.iso_code_2 FROM " . DB_PREFIX . "order o LEFT JOIN `sfm_country` c ON (o.payment_country_id = c.country_id) WHERE o.order_status_id IN('" . (int)implode(',', $implode) . "') GROUP BY o.payment_country_id");
            return $query->result_array();
        } else {
            return array();
        }
    }    



    public function getTotalOrdersByDateRange($dateRanges) {
        $implode = array();

        $implode= $this->config->item('config_complete_status');

        $order_data = array();
         $startdate = $dateRanges[0];   
         $enddate = $dateRanges[count($dateRanges)-1]; 
         for ($i = 0; $i < count($dateRanges); $i++) {
             //$date = date('Y-m-d', $date_start + ($i * 86400));
             $date = date('m', strtotime($dateRanges[$i])).'/'.date('d', strtotime($dateRanges[$i]));
             $order_data[date('j', strtotime($dateRanges[$i]))] = array(
                'day'   => $date,
                'total' => 0
            );
        }

         $sql = "SELECT COUNT(*) AS total, date_added FROM " . DB_PREFIX . "order WHERE order_status_id IN(" . implode(",", $implode) . ") AND DATE(date_added) between '$startdate' AND '$enddate' GROUP BY DAYNAME(date_added)";
         $query = $this->db->query($sql);

         foreach ($query->result_array() as $result) {
             $order_data[date('j', strtotime($result['date_added']))]['total'] = (int)$result['total'];
         }

        return $order_data;
    }



    public function getTotalCustomersByDateRange($dateRanges) {
        $implode = array();

        $implode= $this->config->item('config_complete_status');

        $customer_data = array();

         $startdate = $dateRanges[0];   
         $enddate = $dateRanges[count($dateRanges)-1]; 

         for ($i = 0; $i < count($dateRanges); $i++) {
             //$date = date('Y-m-d', $date_start + ($i * 86400));
             $date = date('m',strtotime($dateRanges[$i])).'/'.date('d', strtotime($dateRanges[$i]));
             $customer_data[date('j', strtotime($dateRanges[$i]))] = array(
                'day'   => $date,
                'total' => 0
            );
        }

         $sql = "SELECT COUNT(*) AS total, date_registered FROM " . DB_PREFIX . "uap_affiliates WHERE DATE(date_registered)  between '$startdate' AND '$enddate' GROUP BY DAYNAME(date_registered)";
         $query = $this->db->query($sql);

         foreach ($query->result_array() as $result) {
             $customer_data[date('j', strtotime($result['date_registered']))]['total'] = (int)$result['total'];
         }
        // foreach ($query->result_array() as $result) {
        //     $customer_data[date('m', strtotime($result['date_registered']))] = array(
        //         'day'   => date('d', strtotime($result['date_registered'])),
        //         'total' => (int)$result['total']
        //     );
        // }

        return $customer_data;
    }

}
