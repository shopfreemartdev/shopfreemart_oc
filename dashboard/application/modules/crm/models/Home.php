<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CRM_Controller
{ 
    function __construct()
    {
        parent::__construct();
        $this->load->model('home_model');
    }
 
    /* This is admin home view */
    public function index()
    {
        $this->load->model('departments_model');
        $this->load->model('todo_model');
        $data['departments']               = $this->departments_model->get();
        $data['todos']                     = $this->todo_model->get_todo_items(0, 8);
        $data['todos_finished']            = $this->todo_model->get_todo_items(1, 4);
        $data['upcoming_events_next_week'] = $this->home_model->get_upcoming_events_next_week();
        $data['upcoming_events']           = $this->home_model->get_upcoming_events();
        $data['title']                     = lang('dashboard_string');
        $this->load->model('currencies_model');
        $data['currencies']                           = $this->currencies_model->get();
        $data['base_currency']                        = $this->currencies_model->get_base_currency();
        $data['activity_log']                         = $this->misc_model->get_activity_log();
        // Tickets charts
        $tickets_awaiting_reply_by_status = $this->home_model->tickets_awaiting_reply_by_status();
        $tickets_awaiting_reply_by_department = $this->home_model->tickets_awaiting_reply_by_department();

        $data['tickets_reply_by_status']              = json_encode($tickets_awaiting_reply_by_status);
        $data['tickets_awaiting_reply_by_department'] = json_encode($tickets_awaiting_reply_by_department);

        $data['tickets_reply_by_status_no_json']              = $tickets_awaiting_reply_by_status;
        $data['tickets_awaiting_reply_by_department_no_json'] = $tickets_awaiting_reply_by_department;

        $data['projects_status_stats']                = json_encode($this->home_model->projects_status_stats());
        $data['leads_status_stats']                   = json_encode($this->home_model->leads_status_stats());
        $data['google_ids_calendars']                 = $this->misc_model->get_google_calendar_ids();
        $data['bodyclass']                            = 'home invoices_total_manual';

        $data['recent_orders'] = $this->recent_orders();
        $this->load->model('Adminannouncements_model');
        $data['staff_announcements'] = $this->Adminannouncements_model->get();
        $data['is_home']             = true;
        $this->load->model('projects_model');
        $data['projects_activity'] = $this->projects_model->get_activity('', 20);
        // To load js files
        $data['calendar_assets']   = true;
        $data['chart_js_assets']   = true;
        $this->load->model('utilities_model');
        $data['calendar_data'] = $this->utilities_model->get_calendar_data();
        $this->load->model('estimates_model');
        $data['estimate_statuses'] = $this->estimates_model->get_statuses();

        $this->load->model('proposals_model');
        $data['proposal_statuses'] = $this->proposals_model->get_statuses();

        $wps_currency = 'undefined';
        if (is_using_multiple_currencies()) {
            $wps_currency = $data['base_currency']->id;
        }
        $data['weekly_payment_stats'] = json_encode($this->home_model->get_weekly_payments_statistics($wps_currency));
        $this->load->view('crm/admin/home', $data);

    }
    /* Chart weekly payments statistics on home page / ajax */
    public function weekly_payments_statistics($currency)
    {
        if ($this->input->is_ajax_request()) {
         //   echo json_encode($this->home_model->get_weekly_payments_statistics($currency));
            die();
        }
    }

    public function Statistics()
    {
        if ($this->input->is_ajax_request()) {
             if (isset($this->input->get['range'])) {
                $range = $this->input->get['range'];
            } else {
                $range = 'day';
            }
        }
    }



    public function chart() {

        $this->load->model('home_model');
        $json = array();

        $json['order'] = array();
        $json['customer'] = array();
        $json['xaxis'] = array();

        $json['order']['label'] = lang('Orders');
        $json['customer']['label'] = lang('Customers');
        $json['order']['data'] = array();
        $json['customer']['data'] = array();


        if (isset($_GET['range'])) {
            $range = $_GET['range'];
            $dateRanges = '1';
            $array_range = explode(" - " , $range);
            $dateRanges = $this->getDatesFromRange($array_range[0], $array_range[1]);
            $timestamp = strtotime($array_range[0]);
            if (count($dateRanges) <= 1) {
                $range = 'day';    
            } else {
                $range = 'daterange';    
                // for ($i = 0; $i < count($dateRanges); $i++) {
                //     $data = $this->Home_model->getDateChart($dateRanges[$i]);
                //     $data['time'] = mdate('%d %M', strtotime($dateRanges[$i]));
                //     $results[] = $data;
                // }
            }


        } else {
            $range = 'day';
        }

        switch ($range) {
            case 'day':

                if (isset($array_range[0]))
                    $results = $this->home_model->getTotalOrdersByDay($array_range[0]);
                else
                    $results = $this->home_model->getTotalOrdersByDay();

                foreach ($results as $key => $value) {
                    $json['order']['data'][] = array($key, $value['total']);
                }

                if (isset($array_range[0]))
                    $results = $this->home_model->getTotalCustomersByDay($array_range[0]);
                else
                    $results = $this->home_model->getTotalCustomersByDay();

                foreach ($results as $key => $value) {
                    $json['customer']['data'][] = array($key, $value['total']);
                }

                for ($i = 0; $i < 24; $i++) {
                    $json['xaxis'][] = array($i, $i);
                }
                break;
            case 'daterange':
                $results = $this->home_model->getTotalOrdersByDateRange($dateRanges);

                foreach ($results as $key => $value) {
                    $json['order']['data'][] = array($key, $value['total']);
                }
                $results = $this->home_model->getTotalCustomersByDateRange($dateRanges);

                foreach ($results as $key => $value) {
                    $json['customer']['data'][] = array($key, $value['total']);
                }
                for ($i = 0; $i < count($dateRanges); $i++) {
                     //$date = date('Y-m-d', $date_start + ($i * 86400));
                    $date = date('m', strtotime($dateRanges[$i])).'/'.date('d', strtotime($dateRanges[$i]));
                     $json['xaxis'][] = array($i, $date); 
                 }
                break;

            case 'week':
                $results = $this->home_model->getTotalOrdersByWeek();

                foreach ($results as $key => $value) {
                    $json['order']['data'][] = array($key, $value['total']);
                }

                $results = $this->home_model->getTotalCustomersByWeek();

                foreach ($results as $key => $value) {
                    $json['customer']['data'][] = array($key, $value['total']);
                }

                $date_start = strtotime('-' . date('w') . ' days');

                for ($i = 0; $i < 7; $i++) {
                    $date = date('Y-m-d', $date_start + ($i * 86400));

                    $json['xaxis'][] = array(date('w', strtotime($date)), date('D', strtotime($date)));
                }
                break;
            case 'month':
                $results = $this->home_model->getTotalOrdersByMonth();

                foreach ($results as $key => $value) {
                    $json['order']['data'][] = array($key, $value['total']);
                }

                $results = $this->home_model->getTotalCustomersByMonth();

                foreach ($results as $key => $value) {
                    $json['customer']['data'][] = array($key, $value['total']);
                }

                for ($i = 1; $i <= date('t'); $i++) {
                    $date = date('Y') . '-' . date('m') . '-' . $i;

                    $json['xaxis'][] = array(date('j', strtotime($date)), date('d', strtotime($date)));
                }
                break;
            case 'year':
                $results = $this->home_model->getTotalOrdersByYear();

                foreach ($results as $key => $value) {
                    $json['order']['data'][] = array($key, $value['total']);
                }

                $results = $this->home_model->getTotalCustomersByYear();

                foreach ($results as $key => $value) {
                    $json['customer']['data'][] = array($key, $value['total']);
                }

                for ($i = 1; $i <= 12; $i++) {
                    $json['xaxis'][] = array($i, date('M', mktime(0, 0, 0, $i)));
                }
                break;
            default:

        }

        header('content-type:application/json');
        echo json_encode($json);
        die;
    }  


    public function map() {
        $json = array();

        $results = $this->home_model->getTotalOrdersByCountry();

        foreach ($results as $result) {
            $json[strtolower($result['iso_code_2'])] = array(
                'total'  => $result['total'],
                'amount' => to_currency($result['amount'])
            );
        }

        header('content-type:application/json');
        echo json_encode($json);
    }

    public function recent_orders()
    {
                // Last 5 Orders
        $orders = array();

        $filter_data = array(
            'sort'  => 'o.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => 5
        );

        $this->load->model('manageorders/manageordersmodel');
        
        $results = $this->manageordersmodel->getOrders($filter_data);

        foreach ($results as $result) {
            $orders['orders'][] = array(
                'id'   => $result['id'],
                'customer'   => $result['customer'],
                'status'     => $result['order_status'],
                'date_added' => $result['date_added'],
                'total'      => to_currency($result['total'])
            );
        }

        return $orders; 
    }


    private function getDatesFromRange($start, $end) {
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(
            new DateTime($start),
            $interval,
            $realEnd
        );

        foreach($period as $date) {
            $array[] = $date->format('Y-m-d');
        }

        return $array;
    }    

}
