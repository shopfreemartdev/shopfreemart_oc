<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tickets_model extends CRM_Model
{
    private $piping = false;
    function __construct()
    {
        parent::__construct();
    }
    public function insert_piped_ticket($data)
    {

        $this->piping = true;
        $attachments  = $data['attachments'];
        $subject      = $data['subject'];
        // Prevent insert ticket to database if mail delivery error happen
        // This will stop createing a thousand tickets
        $system_blocked_subjects = array(
            'Mail delivery failed',
            'failure notice',
            'Returned mail: see transcript for details',
            'Undelivered Mail Returned to Sender',
            );

        $subject_blocked = false;

        foreach($system_blocked_subjects as $sb){
            if (strpos('x'.$subject, $sb) !== false) {
                $subject_blocked = true;
                break;
            }
        }

        if($subject_blocked == true){
            return;
        }

        $message      = $data['body'];
        $name         = $data['fromname'];
        $email        = $data['email'];
        $to           = $data['to'];
        $subject      = $subject;
        $message      = $message;
        $mailstatus   = false;
        $spam_filters = $this->db->get('main_shopfreemart.sfm_crm_ticketsspamcontrol')->result_array();
        foreach ($spam_filters as $filter) {
            $type  = $filter['type'];
            $value = $filter['value'];
            if ($type == "sender") {
                if (strtolower($value) == strtolower($email)) {
                    $mailstatus = "Blocked Sender";
                }
            }
            if ($type == "subject") {
                if (strpos("x" . strtolower($subject), strtolower($value))) {
                    $mailstatus = "Blocked Subject";
                }
            }
            if ($type == "phrase") {
                if (strpos("x" . strtolower($message), strtolower($value))) {
                    $mailstatus = "Blocked Phrase";
                }
            }
        }
        // No spam found
        if (!$mailstatus) {
            $pos = strpos($subject, "[Ticket ID: ");
            if ($pos === false) {
            } else {
                $tid = substr($subject, $pos + 12);
                $tid = substr($tid, 0, strpos($tid, "]"));
                $this->db->where('ticketid', $tid);
                $data = $this->db->get('main_shopfreemart.sfm_crm_tickets')->row();
                $tid  = $data->ticketid;
            }
            $to            = trim($to);
            $toemails      = explode(",", $to);
            $department_id = false;
            $user_id        = false;
            foreach ($toemails as $toemail) {
                if (!$department_id) {
                    $this->db->where('email', $toemail);
                    $data = $this->db->get('main_shopfreemart.sfm_crm_departments')->row();
                    if ($data) {
                        $department_id = $data->departmentid;
                        $to            = $data->email;
                    }
                }
            }
            if (!$department_id) {
                $mailstatus = "Department Not Found";
            } else {
                if ($to == $email) {
                    $mailstatus = "Blocked Potential Email Loop";
                } else {
                    $message = trim($message);
                    $this->db->where('active', 1);
                    $this->db->where('email', $email);
                    $result = $this->db->get('main_shopfreemart.sfm_crm_staff')->row();
                    if ($result) {
                        if ($tid) {
                            $data            = array();
                            $data['message'] = $message;
                            $data['status']  = 1;
                            if ($user_id == false) {
                                $data['name']  = $name;
                                $data['email'] = $email;
                            }
                            $reply_id = $this->add_reply($data, $tid, $result->staffid, $attachments);
                            if ($reply_id) {
                                $mailstatus = "Ticket Reply Imported Successfully";
                            }
                        } else {
                            $mailstatus = "Ticket ID Not Found";
                        }
                    } else {
                        $this->db->where('email', $email);
                        $result = $this->db->get('sfm_uap_affiliates')->row();
                        if ($result) {
                            $user_id    = $result->user_id;
                            $contactid = $result->id;
                        }
                        if ($user_id == false && perfex_get_option('email_piping_only_registered') == '1') {
                            $mailstatus = "Unregistered Email Address";
                        } else {
                            $filterdate = date("YmdHis", mktime(date("H"), date("i") - 15, date("s"), date("m"), date("d"), date("Y")));
                            $query      = 'SELECT count(*) as total FROM main_shopfreemart.sfm_crm_tickets WHERE date > "' . $filterdate . '" AND (email="' . $this->db->escape($email) . '"';
                            if ($user_id) {
                                $query .= " OR user_id=" . (int) $user_id;
                            }
                            $query .= ")";
                            $result = $this->db->query($query)->row();
                            if (10 < $result->total) {
                                $mailstatus = "Exceeded Limit of 10 Tickets within 15 Minutes";
                            } else {
                                if (isset($tid)) {
                                    $data            = array();
                                    $data['message'] = $message;
                                    $data['status']  = 1;
                                    if ($user_id == false) {
                                        $data['name']  = $name;
                                        $data['email'] = $email;
                                    } else {
                                        $data['user_id']    = $user_id;
                                        $data['contactid'] = $contactid;

                                        $this->db->where('user_id', $user_id);
                                        $this->db->where('ticketid', $tid);
                                        $t = $this->db->get('main_shopfreemart.sfm_crm_tickets')->row();
                                        if (!$t) {
                                            $abuse = true;
                                        }

                                    }
                                    if (!isset($abuse)) {
                                        $reply_id = $this->add_reply($data, $tid, null, $attachments);
                                        if ($reply_id) {
                                            // Dont change this line
                                            $mailstatus = "Ticket Reply Imported Successfully";
                                        }
                                    } else {
                                        $mailstatus = 'Ticket ID Not Found For User';
                                    }
                                } else {
                                    if (perfex_get_option('email_piping_only_registered') == 1 && !$user_id) {
                                        $mailstatus = "Blocked Ticket Opening from Unregistered User";
                                    } else {
                                        if (perfex_get_option('email_piping_only_replies') == '1') {
                                            $mailstatus = "Only Replies Allowed by Email";
                                        } else {
                                            $data               = array();
                                            $data['department'] = $department_id;
                                            $data['subject']    = $subject;
                                            $data['message']    = $message;
                                            $data['contactid']  = $contactid;
                                            $data['priority']   = perfex_get_option('email_piping_default_priority');
                                            if ($user_id == false) {
                                                $data['name']  = $name;
                                                $data['email'] = $email;
                                            } else {
                                                $data['user_id'] = $user_id;
                                            }
                                            $tid        = $this->add($data, null, $attachments);
                                            // Dont change this line
                                            $mailstatus = "Ticket Imported Successfully";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($mailstatus == "") {
            $mailstatus = "Ticket Import Failed";
        }
        $this->db->insert('main_crm.crm_ticketpipelog', array(
            'date' => date('Y-m-d H:i:s'),
            'email_to' => $to,
            'name' => $name,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
            'status' => $mailstatus
        ));

        return $mailstatus;
    }
    private function process_pipe_attachments($attachments, $ticket_id, $reply_id = '')
    {
        if (!empty($attachments)) {
            $ticket_attachments = array();
            $allowed_extensions = explode(',', perfex_get_option('ticket_attachments_file_extensions'));
            $path               = get_upload_path_by_type('ticket') . $ticket_id . '/';
            foreach ($attachments as $attachment) {
                $filename      = $attachment["filename"];
                $filenameparts = explode(".", $filename);
                $extension     = end($filenameparts);
                $extension     = strtolower($extension);
                if (in_array('.' . $extension, $allowed_extensions)) {
                    $filename = implode(array_slice($filenameparts, 0, 0 - 1));
                    $filename = trim(preg_replace("/[^a-zA-Z0-9-_ ]/", "", $filename));
                    if (!$filename) {
                        $filename = "attachment";
                    }
                    if (!file_exists($path)) {
                        mkdir($path);
                        fopen($path . 'index.html', 'w');
                    }
                    $filename = unique_filename($path, $filename . "." . $extension);
                    $fp       = fopen($path . $filename, "w");
                    fwrite($fp, $attachment["data"]);
                    fclose($fp);
                    array_push($ticket_attachments, array(
                        'file_name' => $filename,
                        'filetype' => get_mime_by_extension($filename)
                    ));
                }
            }
            $this->insert_ticket_attachments_to_database($ticket_attachments, $ticket_id, $reply_id);
        }
    }
    public function get($id = '', $where = array())
    {
        $this->db->select('*,main_shopfreemart.sfm_crm_tickets.user_id,main_shopfreemart.sfm_crm_tickets.name as from_name,main_shopfreemart.sfm_crm_tickets.email as ticket_email, main_shopfreemart.sfm_crm_departments.name as department_name, main_shopfreemart.sfm_crm_priorities.name as priority_name, statuscolor, main_shopfreemart.sfm_crm_tickets.admin, main_crm.crm_services.name as service_name, service, main_shopfreemart.sfm_crm_ticketstatus.name as status_name,main_shopfreemart.sfm_crm_tickets.ticketid,subject,sfm_uap_affiliates.firstname as user_firstname,sfm_uap_affiliates.lastname as user_lastname,main_shopfreemart.sfm_crm_staff.firstname as staff_firstname, main_shopfreemart.sfm_crm_staff.lastname as staff_lastname,lastreply,message,main_shopfreemart.sfm_crm_tickets.status,subject,department,priority,sfm_uap_affiliates.email,adminread,clientread,date,main_shopfreemart.sfm_crm_tickets.ip');
        $this->db->join('main_shopfreemart.sfm_crm_departments', 'main_shopfreemart.sfm_crm_departments.departmentid = main_shopfreemart.sfm_crm_tickets.department', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_ticketstatus', 'main_shopfreemart.sfm_crm_ticketstatus.ticketstatusid = main_shopfreemart.sfm_crm_tickets.status', 'left');
        $this->db->join('main_crm.crm_services', 'main_crm.crm_services.serviceid = main_shopfreemart.sfm_crm_tickets.service', 'left');
        $this->db->join('main_shopfreemart.sfm_uap_affiliates', 'sfm_uap_affiliates.user_id = main_shopfreemart.sfm_crm_tickets.user_id', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_staff', 'main_shopfreemart.sfm_crm_staff.staffid = main_shopfreemart.sfm_crm_tickets.admin', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_priorities', 'main_shopfreemart.sfm_crm_priorities.priorityid = main_shopfreemart.sfm_crm_tickets.priority', 'left');
        $this->db->where($where);
        if (is_numeric($id)) {
            $this->db->where('main_shopfreemart.sfm_crm_tickets.ticketid', $id);
            return $this->db->get('main_shopfreemart.sfm_crm_tickets')->row();
        }
        $this->db->order_by('lastreply', 'asc');
        return $this->db->get('main_shopfreemart.sfm_crm_tickets')->result_array();
    }
    /**
     * Get ticket by id and all data
     * @param  mixed  $id     ticket id
     * @param  mixed $user_id Optional - Tickets from USER ID
     * @return object
     */
    function get_ticket_by_id($id, $user_id = '')
    {
        $this->db->select('*,main_shopfreemart.sfm_crm_tickets.user_id,main_shopfreemart.sfm_crm_tickets.name as from_name,main_shopfreemart.sfm_crm_tickets.email as ticket_email, main_shopfreemart.sfm_crm_departments.name as department_name, main_shopfreemart.sfm_crm_priorities.name as priority_name, statuscolor, main_shopfreemart.sfm_crm_tickets.admin, main_crm.crm_services.name as service_name, service, main_shopfreemart.sfm_crm_ticketstatus.name as status_name,main_shopfreemart.sfm_crm_tickets.ticketid,subject,sfm_uap_affiliates.firstname as user_firstname,.sfm_uap_affiliates.lastname as user_lastname,main_shopfreemart.sfm_crm_staff.firstname as staff_firstname, main_shopfreemart.sfm_crm_staff.lastname as staff_lastname,lastreply,message,main_shopfreemart.sfm_crm_tickets.status,subject,department,priority,sfm_uap_affiliates.email,adminread,clientread,date,main_shopfreemart.sfm_crm_tickets.ip');
        $this->db->from('main_shopfreemart.sfm_crm_tickets');
        $this->db->join('main_shopfreemart.sfm_crm_departments', 'main_shopfreemart.sfm_crm_departments.departmentid = main_shopfreemart.sfm_crm_tickets.department', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_ticketstatus', 'main_shopfreemart.sfm_crm_ticketstatus.ticketstatusid = main_shopfreemart.sfm_crm_tickets.status', 'left');
        $this->db->join('main_crm.crm_services', 'main_crm.crm_services.serviceid = main_shopfreemart.sfm_crm_tickets.service', 'left');
        $this->db->join('main_shopfreemart.sfm_uap_affiliates', 'sfm_uap_affiliates.user_id = main_shopfreemart.sfm_crm_tickets.user_id', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_staff', 'main_shopfreemart.sfm_crm_staff.staffid = main_shopfreemart.sfm_crm_tickets.admin', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_priorities', 'main_shopfreemart.sfm_crm_priorities.priorityid = main_shopfreemart.sfm_crm_tickets.priority', 'left');
        $this->db->where('main_shopfreemart.sfm_crm_tickets.ticketid', $id);
        if (is_numeric($user_id)) {
            $this->db->where('main_shopfreemart.sfm_crm_tickets.user_id', $user_id);
        }
        $ticket = $this->db->get()->row();
        if ($ticket) {
            if ($ticket->admin == null || $ticket->admin == 0) {
                if ($ticket->contactid != 0) {
                    $ticket->submitter = $ticket->user_firstname . ' ' . $ticket->user_lastname;
                } else {
                    $ticket->submitter = $ticket->from_name;
                }
            } else {
                if ($ticket->contactid != 0) {
                    $ticket->submitter = $ticket->user_firstname . ' ' . $ticket->user_lastname;
                } else {
                    $ticket->submitter = $ticket->from_name;
                }
                $ticket->opened_by = $ticket->staff_firstname . ' ' . $ticket->staff_lastname;
            }
        }
        $ticket->attachments = $this->get_ticket_attachments($id);
        return $ticket;
    }
    /**
     * Insert ticket attachments to database
     * @param  array  $attachments array of attachment
     * @param  mixed  $ticketid
     * @param  boolean $replyid If is from reply
     */
    public function insert_ticket_attachments_to_database($attachments, $ticketid, $replyid = false)
    {
        foreach ($attachments as $attachment) {
            $attachment['ticketid']  = $ticketid;
            $attachment['dateadded'] = date('Y-m-d H:i:s');
            if ($replyid !== false && is_int($replyid)) {
                $attachment['replyid'] = $replyid;
            }
            $this->db->insert('main_shopfreemart.sfm_crm_ticketattachments', $attachment);
        }
    }
    /**
     * Get ticket attachments from database
     * @param  mixed $id      ticket id
     * @param  mixed $replyid Optional - reply id if is from from reply
     * @return array
     */
    public function get_ticket_attachments($id, $replyid = '')
    {
        $this->db->where('ticketid', $id);
        if (is_numeric($replyid)) {
            $this->db->where('replyid', $replyid);
        } else {
            $this->db->where('replyid', null);
        }
        $this->db->where('ticketid', $id);
        return $this->db->get('main_shopfreemart.sfm_crm_ticketattachments')->result_array();
    }
    /**
     * Add new reply to ticket
     * @param mixed $data  reply $_POST data
     * @param mixed $id    ticket id
     * @param boolean $admin staff id if is staff making reply
     */
    public function add_reply($data, $id, $admin = null, $pipe_attachments = false)
    {
        if (isset($data['assign_to_current_user'])) {
            $assigned = get_staff_user_id();
            unset($data['assign_to_current_user']);
        }
        $unsetters = array(
            'note_description',
            'department',
            'priority',
            'subject',
            'assigned',
            'project_id',
            'service',
            'status_top',
            'attachments',
            'DataTables_Table_0_length',
            'DataTables_Table_1_length',
            'custom_fields'
        );
        foreach ($unsetters as $unset) {
            if (isset($data[$unset])) {
                unset($data[$unset]);
            }
        }
        if ($admin !== null) {
            $data['admin'] = $admin;
            $status        = $data['status'];
        } else {
            $status = 1;
        }
        if (isset($data['status'])) {
            unset($data['status']);
        }

        $cc = '';
        if (isset($data['cc'])) {
            $cc = $data['cc'];
            unset($data['cc']);
        }

        $data['ticketid'] = $id;
        

        //$dateTime = date_create('now')->format('Y-m-d H:i:s');
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        //$dateTime =  gmdate("Y-m-d\TH:i:s\Z", $timestamp);
        $dateTime = gmdate('Y-m-d H:i:s', $timestamp);
        $data['date']     = $dateTime;
        $data['ip']       = $this->input->ip_address();
        $data['message']  = trim($data['message']);

        if ($this->piping == true) {
            $data['message'] = preg_replace('/\v+/u', '<br>', $data['message']);
        }

        // adminn can have html
        if ($admin == null) {
            $data['message'] = _strip_tags($data['message']);
            $data['message'] = nl2br_save_html($data['message']);
        }
        if (!isset($data['user_id'])) {
            $data['user_id'] = 0;
        }
        if (is_client_logged_in()) {
            $data['contactid'] = get_contact_user_id();
        }
        $_data = perfex_do_action('before_ticket_reply_add', array(
            'data' => $data,
            'id' => $id,
            'admin' => $admin
        ));
        $data  = $_data['data'];
        $this->db->insert('main_shopfreemart.sfm_crm_ticketreplies', $data);

        $insert_id = $this->db->insert_id();
        if ($insert_id){
            if (isset($assigned)) {
                $this->db->where('ticketid', $id);
                $this->db->update('main_shopfreemart.sfm_crm_tickets', array(
                    'assigned' => $assigned
                ));
            }
            if ($pipe_attachments == false) {
                $attachments = handle_ticket_attachments($id);
                if ($attachments) {
                    $this->tickets_model->insert_ticket_attachments_to_database($attachments, $id, $insert_id);
                }
            } else {
                $this->process_pipe_attachments($pipe_attachments, $id, $insert_id);
            }

            $_attachments = $this->get_ticket_attachments($id, $insert_id);

            //logActivity('New Ticket Reply [ReplyID: ' . $insert_id . ']');

            $this->db->select('status');
            $this->db->where('ticketid', $id);
            $old_ticket_status = $this->db->get('main_shopfreemart.sfm_crm_tickets')->row()->status;

            $this->db->where('ticketid', $id);
            $this->db->update('main_shopfreemart.sfm_crm_tickets', array(
                'lastreply' => date('Y-m-d H:i:s'),
                'status' => $status,
                'adminread' => 0,
                'clientread' => 0
            ));

            if ($old_ticket_status != $status) {
                perfex_do_action('after_ticket_status_changed', array(
                    'id' => $id,
                    'status' => $status
                ));
            }

            $this->load->model('emails_model');
            $ticket = $this->get_ticket_by_id($id);
            $user_id = $ticket->user_id;
            if ($ticket->user_id != 0 && $ticket->contactid != 0) {
                $email = $this->clients_model->get_contact($ticket->contactid)->email;
            } else {
                $email = $ticket->ticket_email;
            }
            if ($admin == null) {

                $this->load->model('departments_model');
                $this->load->model('staff_model');
                $staff = $this->staff_model->get('', 1);
                foreach ($staff as $member) {
                    if (perfex_get_option('access_tickets_to_none_staff_members') == 0 && !is_staff_member($member['staffid'])) {
                        continue;
                    }
                    $staff_departments = $this->departments_model->get_staff_departments($member['staffid'], true);
                    if (in_array($ticket->department, $staff_departments)) {
                        foreach ($_attachments as $at) {
                            $this->emails_model->add_attachment(array(
                                'attachment' => get_upload_path_by_type('ticket') . $id . '/' . $at['file_name'],
                                'filename' => $at['file_name'],
                                'type' => $at['filetype'],
                                'read' => true
                            ));
                        }

                        $merge_fields = array();
                        $merge_fields = array_merge($merge_fields, get_ticket_merge_fields('ticket-reply-to-admin', $id));
                        $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($ticket->user_id, $ticket->contactid));
                        $this->emails_model->send_email_template('ticket-reply-to-admin', $member['email'], $merge_fields, $id);
                   
                    }
                    // notify admin for client reply
                    $supportticket = $this->get_ticket_by_id($id);
                    add_notification(array(
                        'description' => 'client replied to ticket '.$id. "-".$supportticket->subject,
                        'touser_id' => $member['staffid'],
                        'event' => 10,
                        'fromcompany' => 1,
                        'fromuser_id' => null,
                        'module_key' => 0,
                        'module_id' => 0,
                        'link' => 'tickets/ticket/' . $data['ticketid'],
                        'additional_data' => serialize(array(
                            $supportticket->subject
                        ))
                    ));

                }
            } else {

                $merge_fields = array();
                $merge_fields = array_merge($merge_fields, get_ticket_merge_fields('ticket-reply', $id));
                $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($ticket->user_id, $ticket->contactid));

                foreach ($_attachments as $at) {
                    $this->emails_model->add_attachment(array(
                        'attachment' => get_upload_path_by_type('ticket') . $id . '/' . $at['file_name'],
                        'filename' => $at['file_name'],
                        'type' => $at['filetype'],
                        'read' => true
                    ));
                }

                //$this->emails_model->send_email_template('ticket-reply', $email, $merge_fields, $id, $cc);

                //notif client for admin reply
                $supportticket = $this->get_ticket_by_id($id);

                $notify_to = $supportticket->user_id;
                $event = 10;
                $module_id = 0;
                $module_key = 0;
                $link = 'crm/members/tickets/ticket/' . $data['ticketid'];

                $this->Notifications_model->notification_save($notify_to, $event, $module_id, $module_key, $link);

                //crmactivitylog
                $staffid = get_staff_user_id();
                $activitytype = 10;
                crmlogActivity('Staff Replied [ReplyID: ' . $insert_id . ']',$activitytype, $staffid);

            }
            perfex_do_action('after_ticket_reply_added', array(
                'data' => $data,
                'id' => $id,
                'admin' => $admin,
                'replyid' => $insert_id
            ));
            return $insert_id;
        }
        return false;
    }
    /**
     *  Delete ticket reply
     * @param   mixed $ticket_id    ticket id
     * @param   mixed $reply_id     reply id
     * @return  boolean
     */
    public function delete_ticket_reply($ticket_id, $reply_id)
    {
        $this->db->where('id', $reply_id);
        $this->db->delete('main_shopfreemart.sfm_crm_ticketreplies');
        if ($this->db->affected_rows() > 0) {
            // Get the reply attachments by passing the reply_id to get_ticket_attachments method
            $attachments = $this->get_ticket_attachments($ticket_id, $reply_id);
            if (count($attachments) > 0) {
                foreach ($attachments as $attachment) {
                    if (unlink(get_upload_path_by_type('ticket') . $ticket_id . '/' . $attachment['file_name'])) {
                        $this->db->where('id', $attachment['id']);
                        $this->db->delete('main_shopfreemart.sfm_crm_ticketattachments');
                    }
                }
                // Check if no attachments left, so we can delete the folder also
                $other_attachments = list_files(get_upload_path_by_type('ticket') . $ticket_id);
                if (count($other_attachments) == 0) {
                    delete_dir(get_upload_path_by_type('ticket') . $ticket_id);
                }
            }
             //crmactivitylog
                $staffid = get_staff_user_id();
                $activitytype = 13;
                crmlogActivity('Staff Deleted a Reply [ReplyID: '. $reply_id.'] from ticketID: '.$ticket_id,$activitytype, $staffid);
            return true;
        }
        return false;
    }
    /**
     * This functions is used when staff open client ticket
     * @param  mixed $user_id client id
     * @param  mixed $id     ticketid
     * @return array
     */
    public function get_user_other_tickets($user_id, $id)
    {
        $this->db->select('main_shopfreemart.sfm_crm_departments.name as department_name, main_crm.crm_services.name as service_name,main_shopfreemart.sfm_crm_ticketstatus.name as status_name,main_shopfreemart.sfm_crm_staff.firstname as staff_firstname, sfm_uap_affiliates.lastname as staff_lastname,ticketid,subject,firstname,lastname,lastreply');
        $this->db->from('main_shopfreemart.sfm_crm_tickets');
        $this->db->join('main_shopfreemart.sfm_crm_departments', 'main_shopfreemart.sfm_crm_departments.departmentid = main_shopfreemart.sfm_crm_tickets.department', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_ticketstatus', 'main_shopfreemart.sfm_crm_ticketstatus.ticketstatusid = main_shopfreemart.sfm_crm_tickets.status', 'left');
        $this->db->join('main_crm.crm_services', 'main_crm.crm_services.serviceid = main_shopfreemart.sfm_crm_tickets.service', 'left');
        $this->db->join('sfm_uap_affiliates', 'sfm_uap_affiliates.user_id = main_shopfreemart.sfm_crm_tickets.user_id', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_staff', 'main_shopfreemart.sfm_crm_staff.staffid = main_shopfreemart.sfm_crm_tickets.admin', 'left');
        $this->db->where('main_shopfreemart.sfm_crm_tickets.user_id', $user_id);
        $this->db->where('main_shopfreemart.sfm_crm_tickets.ticketid !=', $id);
        $tickets = $this->db->get()->result_array();
        $i       = 0;
        foreach ($tickets as $ticket) {
            $tickets[$i]['submitter'] = $ticket['firstname'] . ' ' . $ticket['lastname'];
            unset($ticket['firstname']);
            unset($ticket['lastname']);
            $i++;
        }
        return $tickets;
    }
    /**
     * Get all ticket replies
     * @param  mixed  $id     ticketid
     * @param  mixed $user_id specific client id
     * @return array
     */
    function get_ticket_replies($id)
    {
        $ticket_replies_comments_order = perfex_do_action('ticket_replies_comments_order', 'ASC');

        $this->db->select('main_shopfreemart.sfm_crm_ticketreplies.id,main_shopfreemart.sfm_crm_ticketreplies.ip,main_shopfreemart.sfm_crm_ticketreplies.name as from_name,main_shopfreemart.sfm_crm_ticketreplies.email as reply_email, main_shopfreemart.sfm_crm_ticketreplies.admin, main_shopfreemart.sfm_crm_ticketreplies.user_id,main_shopfreemart.sfm_crm_staff.firstname as staff_firstname,main_shopfreemart.sfm_crm_staff.lastname as staff_lastname,sfm_uap_affiliates.firstname as user_firstname,.sfm_uap_affiliates.lastname as user_lastname,message,date,contactid');
        $this->db->from('main_shopfreemart.sfm_crm_ticketreplies');
        $this->db->join('sfm_uap_affiliates', 'sfm_uap_affiliates.user_id = main_shopfreemart.sfm_crm_ticketreplies.user_id', 'left');
        $this->db->join('main_shopfreemart.sfm_crm_staff', 'main_shopfreemart.sfm_crm_staff.staffid = main_shopfreemart.sfm_crm_ticketreplies.admin', 'left');
        $this->db->where('ticketid', $id);
        $this->db->order_by('date', $ticket_replies_comments_order);
        $replies = $this->db->get()->result_array();
        $i       = 0;
        foreach ($replies as $reply) {
            if ($reply['admin'] !== null || $reply['admin'] != 0) {
                // staff reply
                $replies[$i]['submitter'] = $reply['staff_firstname'] . ' ' . $reply['staff_lastname'];
            } else {
                if ($reply['contactid'] != 0) {
                    $replies[$i]['submitter'] = $reply['user_firstname'] . ' ' . $reply['user_lastname'];
                } else {
                    $replies[$i]['submitter'] = $reply['from_name'];
                }
            }
            unset($replies[$i]['staff_firstname']);
            unset($replies[$i]['staff_lastname']);
            unset($replies[$i]['user_firstname']);
            unset($replies[$i]['user_lastname']);
            $replies[$i]['attachments'] = $this->get_ticket_attachments($id, $reply['id']);
            $i++;
        }
        return $replies;
    }
    /**
     * Add new ticket to database
     * @param mixed $data  ticket $_POST data
     * @param mixed $admin If admin adding the ticket passed staff id
     */
    public function add($data, $admin = null, $pipe_attachments = FALSE)
    {
        if ($admin !== null) {
            $data['admin'] = $admin;
            unset($data['ticket_client_search']);
        }
        if (isset($data['assigned']) && $data['assigned'] == '') {
            $data['assigned'] = 0;
        }
        if ($admin == null) {
            if (isset($data['email'])) {
                $data['user_id']    = 0;
                $data['contactid'] = 0;
            } else {
                // Opened from customer portal otherwise is passed from pipe or admin area
                if (!isset($data['user_id']) && !isset($data['contactid'])) {
                    $data['user_id']    = get_client_user_id();
                    $data['contactid'] = get_contact_user_id();
                }
            }
            $data['status'] = 1;
        }


        if (isset($data['custom_fields'])) {
            $custom_fields = $data['custom_fields'];
            unset($data['custom_fields']);
        }

        // CC is only from admin area
        $cc = '';
        if (isset($data['cc'])) {
            $cc = $data['cc'];
            unset($data['cc']);
        }

        //$dateTime = date_create('now')->format('Y-m-d H:i:s');
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        //$dateTime =  gmdate("Y-m-d\TH:i:s\Z", $timestamp);
        $dateTime = gmdate('Y-m-d H:i:s', $timestamp);
        $data['date']     = $dateTime;
        $data['ticketkey'] = md5(uniqid(time(), true));
        $data['status']    = 1;
        $data['message']   = trim($data['message']);
        $data['subject']   = trim($data['subject']);

        
        $data['assigned'] = 11;//assign ticket automatically to this staff


        if ($this->piping == true) {
            $data['message'] = preg_replace('/\v+/u', '<br>', $data['message']);
        }
        // Admin can have html
        if ($admin == null) {
            $data['message'] = _strip_tags($data['message']);
            $data['subject'] = _strip_tags($data['subject']);
            $data['message'] = nl2br_save_html($data['message']);
        }
        if (!isset($data['user_id'])) {
            $data['user_id'] = 0;
        }
        if (isset($data['priority']) && $data['priority'] == '' || !isset($data['priority'])) {
            $data['priority'] = 0;
        }


        $tags = '';
        if(isset($data['tags'])){
            $tags  = $data['tags'];
            unset($data['tags']);
        }

        $data['ip'] = $this->input->ip_address();
        $_data      = perfex_do_action('before_ticket_created', array(
            'data' => $data,
            'admin' => $admin
        ));
        $data       = $_data['data'];
        $this->db->insert('main_shopfreemart.sfm_crm_tickets', $data);


        $ticketid = $this->db->insert_id();
        if ($ticketid) {
            handle_tags_save($tags,$ticketid,'ticket');
            if (isset($custom_fields)) {
                handle_custom_fields_post($ticketid, $custom_fields);
            }
            if (isset($data['assigned']) && $data['assigned'] != 0) {
                if ($data['assigned'] != get_staff_user_id()) {
                    add_notification(array(
                        'description' => 'Ticket assigned to you',
                        'touser_id' => $data['assigned'],
                        'fromcompany' => 1,
                        'fromuser_id' => null,
                        'link' => 'tickets/ticket/' . $ticketid,
                        'additional_data' => serialize(array(
                            $data['subject']
                        ))
                    ));
                }
            }
            if ($pipe_attachments == false) {
                $attachments = handle_ticket_attachments($ticketid);
                if ($attachments) {
                    $this->insert_ticket_attachments_to_database($attachments, $ticketid);
                }
            } else {
                $this->process_pipe_attachments($pipe_attachments, $ticketid);
            }

            $_attachments = $this->get_ticket_attachments($ticketid);

            if($admin == null){
                $this->load->model('emails_model');
                if (isset($data['user_id']) && $data['user_id'] != false) {
                    $email = $this->clients_model->get_contact($data['contactid'])->email;
                }   else {
                    $email = $data['email'];
                }

            }
            
            $template = 'new-ticket-opened-admin';
            if ($admin == null) {
                $template = 'ticket-autoresponse';
                $this->load->model('departments_model');
                $this->load->model('staff_model');
                $staff = $this->staff_model->get('', 1);

                foreach ($staff as $member) {
                    if (perfex_get_option('access_tickets_to_none_staff_members') == 0 && !is_staff_member($member['staffid'])) {
                        continue;
                    }
                    $staff_departments = $this->departments_model->get_staff_departments($member['staffid'], true);
                    if (in_array($data['department'], $staff_departments)) {
                        $merge_fields = array();
                        $merge_fields = array_merge($merge_fields, get_ticket_merge_fields($template, $ticketid));
                        $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($data['user_id'], $data['contactid']));

                        foreach ($_attachments as $at) {
                            $this->emails_model->add_attachment(array(
                                'attachment' => get_upload_path_by_type('ticket') . $ticketid . '/' . $at['file_name'],
                                'filename' => $at['file_name'],
                                'type' => $at['filetype'],
                                'read' => true
                            ));
                        }

                        //$this->emails_model->send_email_template('new-ticket-created-staff', $member['email'], $merge_fields, $ticketid);

                        //notify admin for new ticket
                        if (perfex_get_option('receive_notification_on_new_ticket') == 1) {
                            add_notification(array(
                                'description' => 'new ticket created',
                                'event' => 9,
                                'touser_id' => $member['staffid'],
                                'fromcompany' => 1,
                                'fromuser_id' => null,
                                'module_key' => 0,
                                'module_id' => 0,
                                'link' => 'crm/member/tickets/ticket/' . $ticketid,
                                'additional_data' => serialize(array(
                                    $data['subject']
                                ))
                            ));
                        }
                    }
                }
            }
            $merge_fields = array();
            $merge_fields = array_merge($merge_fields, get_ticket_merge_fields($template, $ticketid));
            if($admin == null){
                $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($data['user_id'], $data['contactid']));

            }
            
            if ($admin != NULL) {
                // Admin opened ticket from admin area add the attachments to the email
                foreach ($_attachments as $at) {
                    $this->emails_model->add_attachment(array(
                        'attachment' => get_upload_path_by_type('ticket') . $ticketid . '/' . $at['file_name'],
                        'filename' => $at['file_name'],
                        'type' => $at['filetype'],
                        'read' => true
                    ));
                }
                $staffid = get_staff_user_id();
                $activitytype = 17;
                crmlogActivity('Staff Created New Ticket [ID: '.$ticketid.']', $activitytype,$staffid);
            }
            //$this->emails_model->send_email_template($template, $email, $merge_fields, $ticketid, $cc);
            perfex_do_action('after_ticket_added', $ticketid);
            return $ticketid;
        }
        return false;
    }
    
    /**
     * Get latest 5 client tickets
     * @param  integer $limit  Optional limit tickets
     * @param  mixed $user_id client id
     * @return array
     */
    public function get_client_latests_ticket($limit = 5, $user_id = '')
    {
        $this->db->select('main_shopfreemart.sfm_crm_tickets.user_id, ticketstatusid, statuscolor, main_shopfreemart.sfm_crm_ticketstatus.name as status_name,main_shopfreemart.sfm_crm_tickets.ticketid, subject, date');
        $this->db->from('main_shopfreemart.sfm_crm_tickets');
        $this->db->join('main_shopfreemart.sfm_crm_ticketstatus', 'main_shopfreemart.sfm_crm_ticketstatus.ticketstatusid = main_shopfreemart.sfm_crm_tickets.status', 'left');
        if (is_numeric($user_id)) {
            $this->db->where('main_shopfreemart.sfm_crm_tickets.user_id', $user_id);
        } else {
            $this->db->where('main_shopfreemart.sfm_crm_tickets.user_id', get_client_user_id());
        }
        $this->db->limit($limit);
        return $this->db->get()->result_array();
    }
    /**
     * Delete ticket from database and all connections
     * @param  mixed $ticketid ticketid
     * @return boolean
     */
    public function delete($ticketid)
    {
        $affectedRows = 0;
        perfex_do_action('before_ticket_deleted', $ticketid);
        // final delete ticket
        $this->db->where('ticketid', $ticketid);
        $this->db->delete('main_shopfreemart.sfm_crm_tickets');
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
            $this->db->where('ticketid', $ticketid);
            $attachments = $this->db->get('main_shopfreemart.sfm_crm_ticketattachments')->result_array();
            if (count($attachments) > 0) {
                if (is_dir(get_upload_path_by_type('ticket') . $ticketid)) {
                    if (delete_dir(get_upload_path_by_type('ticket') . $ticketid)) {
                        foreach ($attachments as $attachment) {
                            $this->db->where('id', $attachment['id']);
                            $this->db->delete('main_shopfreemart.sfm_crm_ticketattachments');
                            if ($this->db->affected_rows() > 0) {
                                $affectedRows++;
                            }
                        }
                    }
                }
            }

            $this->db->where('relid', $ticketid);
            $this->db->where('fieldto', 'tickets');
            $this->db->delete('main_crm.crm_customfieldsvalues');

            // Delete replies
            $this->db->where('ticketid', $ticketid);
            $this->db->delete('main_shopfreemart.sfm_crm_ticketreplies');

            $this->db->where('rel_id', $ticketid);
            $this->db->where('rel_type', 'ticket');
            $this->db->delete('main_shopfreemart.sfm_crm_notes');

            $this->db->where('rel_id',$ticketid);
            $this->db->where('rel_type','ticket');
            $this->db->delete('main_shopfreemart.sfm_crm_tags_in');

            // Get related tasks
            $this->db->where('rel_type', 'ticket');
            $this->db->where('rel_id', $ticketid);
            $tasks = $this->db->get('main_crm.crm_stafftasks')->result_array();
            foreach ($tasks as $task) {
                $this->tasks_model->delete_task($task['id']);
            }
        }
        if ($affectedRows > 0) {
            //crmactivitylog
            $staffid = get_staff_user_id();
            $activitytype = 11;
                crmlogActivity('Staff Deleted a Ticket [ticketID: '.$ticketid.']',$activitytype, $staffid);
            return true;
        }
        return false;
    }
    /**
     * Update ticket data / admin use
     * @param  mixed $data ticket $_POST data
     * @return boolean
     */
    public function update_single_ticket_settings($data)
    {
        $affectedRows = 0;
        $data         = perfex_do_action('before_ticket_settings_updated', $data);
        if (isset($data['custom_fields']) && count($data['custom_fields']) > 0) {
            $custom_fields[$data['custom_fields'][0][0]] = array();
            $custom_fields                               = array();
            foreach ($data['custom_fields'] as $field) {
                $custom_fields[$data['custom_fields'][0][0]][$field[1]] = $field[2];
            }
            if (handle_custom_fields_post($data['ticketid'], $custom_fields)) {
                $affectedRows++;
            }
            unset($data['custom_fields']);
        }
        $tags = '';
        if(isset($data['tags'])){
            $tags = $data['tags'];
            unset($data['tags']);
        }

        if(handle_tags_save($tags,$data['ticketid'],'ticket')){
            $affectedRows++;
        }

        if (isset($data['priority']) && $data['priority'] == '' || !isset($data['priority'])) {
            $data['priority'] = 0;
        }
        $current = $this->get_ticket_by_id($data['ticketid']);
        if ($data['assigned'] == '') {
            $data['assigned'] = 0;
        }
        $this->db->where('ticketid', $data['ticketid']);
        $this->db->update('main_shopfreemart.sfm_crm_tickets', $data);
        if ($this->db->affected_rows() > 0) {
            $affectedRows++;
        }
        $current_assigned = $current->assigned;
        if ($current_assigned != 0) {
            if ($current_assigned != $data['assigned']) {
                if ($data['assigned'] != 0 && $data['assigned'] != get_staff_user_id()) {
                    add_notification(array(
                        'description' => 'Ticket reassigned to you',
                        'touser_id' => $data['assigned'],
                        'fromcompany' => 1,
                        'fromuser_id' => get_staff_user_id(),
                        'link' => 'tickets/ticket/' . $data['ticketid'],
                        'additional_data' => serialize(array(
                            $data['subject']
                        ))
                    ));

                //crmactivitylog
                $staffid = get_staff_user_id();
                $activitytype = 16;
                crmlogActivity('Staff Reassigned Ticket [ticketID: '.$data['ticketid'].'] to [Staffid: '.$data['assigned'].']',$activitytype, $staffid);
                }
            }
        } else {
            if ($data['assigned'] != 0 && $data['assigned'] != get_staff_user_id()) {
                add_notification(array(
                    'description' => 'Ticket assigned to you',
                    'touser_id' => $data['assigned'],
                    'fromcompany' => 1,
                    'fromuser_id' => get_staff_user_id(),
                    'link' => 'tickets/ticket/' . $data['ticketid'],
                    'additional_data' => serialize(array(
                        $data['subject']
                    ))
                ));
               //crmactivitylog
                $staffid = get_staff_user_id();
                $activitytype = 9;
                crmlogActivity('Staff Assigned Ticket [ticketID: '.$data['ticketid'].'] to [Staffid: '.$data['assigned'].']',$activitytype, $staffid);
            }
        }
        if ($affectedRows > 0) {
            //logActivity('Ticket Updated [ID: ' . $data['ticketid'] . ']');
            //crmactivitylog
            $staffid = get_staff_user_id();
            $activitytype = 11;
                crmlogActivity('Staff Updated Ticket Details [ticketID: '.$data['ticketid'].']',$activitytype, $staffid);
            return true;
        }
        return false;
    }
    /**
     * C<ha></ha>nge ticket status
     * @param  mixed $id     ticketid
     * @param  mixed $status status id
     * @return array
     */
    public function change_ticket_status($id, $status)
    {
        $this->db->select('status');
        $this->db->where('ticketid', $id);
        $Status = $this->db->get('main_shopfreemart.sfm_crm_tickets')->row();

        $this->db->select('name');
        $this->db->where('ticketstatusid', $status);
        $prevStatus = $this->db->get('main_shopfreemart.sfm_crm_ticketstatus')->row();


        $this->db->where('ticketid', $id);
        $this->db->update('main_shopfreemart.sfm_crm_tickets', array(
            'status' => $status
        ));
        $alert   = 'warning';
        $message = lang('ticket_status_changed_fail');
        if ($this->db->affected_rows() > 0) {
            $alert   = 'success';
            $message = lang('ticket_status_changed_successfuly');
            perfex_do_action('after_ticket_status_changed', array(
                'id' => $id,
                'status' => $status
            ));
        }

        $this->db->select('name');
        $this->db->where('ticketstatusid', $status);
        $newStatus = $this->db->get('main_shopfreemart.sfm_crm_ticketstatus')->row();
        //crmactivitylog
        $staffid = get_staff_user_id();
        $activitytype = 14;
        crmlogActivity('Staff updated ticket [ticketID: '.$id.'] : [Status: '.$prevStatus->name.'] to [Status:'.$newStatus->name.']',$activitytype, $staffid);

        return array(
            'alert' => $alert,
            'message' => $message
        );
    }
    // Priorities
    /**
     * Get ticket priority by id
     * @param  mixed $id priority id
     * @return mixed     if id passed return object else array
     */
    public function get_priority($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('priorityid', $id);
            return $this->db->get('main_shopfreemart.sfm_crm_priorities')->row();
        }
        return $this->db->get('main_shopfreemart.sfm_crm_priorities')->result_array();
    }
    /**
     * Add new ticket priority
     * @param array $data ticket priority data
     */
    public function add_priority($data)
    {
        $this->db->insert('main_shopfreemart.sfm_crm_priorities', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $staffid = get_staff_user_id();
            $activitytype = 18;
            crmlogActivity('Staff Added New Ticket Priority [ID: ' . $insert_id . ', Name: ' . $data['name'] . ']', $activitytype, $staffid);
        }
        return $insert_id;
    }
    /**
     * Update ticket priority
     * @param  array $data ticket priority $_POST data
     * @param  mixed $id   ticket priority id
     * @return boolean
     */
    public function update_priority($data, $id)
    {
        $this->db->where('priorityid', $id);
        $this->db->update('main_shopfreemart.sfm_crm_priorities', $data);
        if ($this->db->affected_rows() > 0) {
            $staffid = get_staff_user_id();
            $activitytype = 19;
            crmlogActivity('Staff Updated Ticket Priority  [ID: ' . $id . ' Name: ' . $data['name'] . ']', $activitytype, $staffid);
            return true;
        }
        return false;
    }
    /**
     * Delete ticket priorit
     * @param  mixed $id ticket priority id
     * @return mixed
     */
    public function delete_priority($id)
    {
        $current = $this->get($id);
        // Check if the priority id is used in main_shopfreemart.sfm_crm_tickets table
        if (is_reference_in_table('priority', 'main_shopfreemart.sfm_crm_tickets', $id)) {
            return array(
                'referenced' => true
            );
        }
        $this->db->where('priorityid', $id);
        $this->db->delete('main_shopfreemart.sfm_crm_priorities');
        if ($this->db->affected_rows() > 0) {
            if (perfex_get_option('email_piping_default_priority') == $id) {
                perfex_update_option('email_piping_default_priority', '');
            }
            $staffid = get_staff_user_id();
            $activitytype = 20;
            crmlogActivity('Staff Deleted Ticket Priority Deleted [ID: ' . $id . ']', $activitytype, $staffid);
            return true;
        }
        return false;
    }
    // Predefined replies
    /**
     * Get predefined reply  by id
     * @param  mixed $id predefined reply id
     * @return mixed if id passed return object else array
     */
    public function get_predefined_reply($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('id', $id);
            return $this->db->get('main_shopfreemart.sfm_crm_predifinedreplies')->row();
        }
        return $this->db->get('main_shopfreemart.sfm_crm_predifinedreplies')->result_array();
    }
    /**
     * Add new predefined reply
     * @param array $data predefined reply $_POST data
     */
    public function add_predefined_reply($data)
    {

        $this->db->insert('main_shopfreemart.sfm_crm_predifinedreplies', $data);
        $insertid = $this->db->insert_id();
        $staffid = get_staff_user_id();
        $activitytype = 21;
        crmlogActivity('Staff Added New Predefined Reply [ID: ' . $insertid . ', ' . $data['name'] . ']', $activitytype, $staffid);
        return $insertid;
    }
    /**
     * Update predefined reply
     * @param  array $data predefined $_POST data
     * @param  mixed $id   predefined reply id
     * @return boolean
     */
    public function update_predefined_reply($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_shopfreemart.sfm_crm_predifinedreplies', $data);
        if ($this->db->affected_rows() > 0) {
            $staffid = get_staff_user_id();
            $activitytype = 22;
            crmlogActivity('Staff Updated Predefined Reply [ID: ' . $id . ', ' . $data['name'] . ']', $activitytype, $staffid);
            return true;
        }
        return false;
    }
    /**
     * Delete predifined reply
     * @param  mixed $id predefined reply id
     * @return boolean
     */
    public function delete_predefined_reply($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('main_shopfreemart.sfm_crm_predifinedreplies');
        if ($this->db->affected_rows() > 0) {
            $staffid = get_staff_user_id();
            $activitytype = 23;
            crmlogActivity('Staff Deleted Predefined Reply [' . $id . ']', $activitytype, $staffid);
            return true;
        }
        return false;
    }
    // Ticket statuses
    /**
     * Get ticket status by id
     * @param  mixed $id status id
     * @return mixed     if id passed return object else array
     */
    public function get_ticket_status($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('ticketstatusid', $id);
            return $this->db->get('main_shopfreemart.sfm_crm_ticketstatus')->row();
        }
        $this->db->order_by('statusorder', 'asc');
        return $this->db->get('main_shopfreemart.sfm_crm_ticketstatus')->result_array();
    }
    /**
     * Add new ticket status
     * @param array ticket status $_POST data
     * @return mixed
     */
    public function add_ticket_status($data)
    {
        $this->db->insert('main_shopfreemart.sfm_crm_ticketstatus', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            $staffid = get_staff_user_id();
            $activitytype = 24;
            crmlogActivity('Staff Added New Ticket Status [ID: ' . $insert_id . ', ' . $data['name'] . ']', $activitytype, $staffid);
            return $insert_id;
        }
        return false;
    }
    /**
     * Update ticket status
     * @param  array $data ticket status $_POST data
     * @param  mixed $id   ticket status id
     * @return boolean
     */
    public function update_ticket_status($data, $id)
    {
        $this->db->where('ticketstatusid', $id);
        $this->db->update('main_shopfreemart.sfm_crm_ticketstatus', $data);
        if ($this->db->affected_rows() > 0) {
            $staffid = get_staff_user_id();
            $activitytype = 25;
            crmlogActivity('Staff Updated Ticket Status [ID: ' . $id . ' Name: ' . $data['name'] . ']',$activitytype, $staffid);
            return true;
        }
        return false;
    }
    /**
     * Delete ticket status
     * @param  mixed $id ticket status id
     * @return mixed
     */
    public function delete_ticket_status($id)
    {
        $current = $this->get_ticket_status($id);
        // Default statuses cant be deleted
        if ($current->isdefault == 1) {
            return array(
                'default' => true
            );
            // Not default check if if used in table
        } else if (is_reference_in_table('status', 'main_shopfreemart.sfm_crm_tickets', $id)) {
            return array(
                'referenced' => true
            );
        }
        $this->db->where('ticketstatusid', $id);
        $this->db->delete('main_shopfreemart.sfm_crm_ticketstatus');
        if ($this->db->affected_rows() > 0) {
            $staffid = get_staff_user_id();
            $activitytype = 26;
            crmlogActivity('Staff Deleted Ticket Status [ID: ' . $id . ']', $activitytype, $staffid);
            return true;
        }
        return false;
    }
    // Ticket services
    function get_service($id = '')
    {
        if (is_numeric($id)) {
            $this->db->where('serviceid', $id);
            return $this->db->get('main_crm.crm_services')->row();
        }
        $this->db->order_by('serviceid', 'asc');
        return $this->db->get('main_crm.crm_services')->result_array();
    }
    public function add_service($data)
    {
        $this->db->insert('main_crm.crm_services', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            //logActivity('New Ticket Service Added [ID: ' . $insert_id . '.' . $data['name'] . ']');
        }
        return $insert_id;
    }
    public function update_service($data, $id)
    {
        $this->db->where('serviceid', $id);
        $this->db->update('main_crm.crm_services', $data);
        if ($this->db->affected_rows() > 0) {
            //logActivity('Ticket Service Updated [ID: ' . $id . ' Name: ' . $data['name'] . ']');
            return true;
        }
        return false;
    }
    public function delete_service($id)
    {
        if (is_reference_in_table('service', 'main_shopfreemart.sfm_crm_tickets', $id)) {
            return array(
                'referenced' => true
            );
        }
        $this->db->where('serviceid', $id);
        $this->db->delete('main_crm.crm_services');
        if ($this->db->affected_rows() > 0) {
            logActivity('Ticket Service Deleted [ID: ' . $id . ']');
            return true;
        }
        return false;
    }
    /**
     * @return array
     * Used in home dashboard page
     * Displays weekly ticket openings statistics (chart)
     */
    public function get_weekly_tickets_opening_statistics()
    {
        $this->db->where('CAST(date as DATE) >= "' . date('Y-m-d', strtotime('monday this week')) . '" AND CAST(date as DATE) <= "' . date('Y-m-d', strtotime('sunday this week')) . '"');
        $tickets = $this->db->get('main_shopfreemart.sfm_crm_tickets')->result_array();
        $chart   = array(
            'labels' => get_weekdays(),
            'datasets' => array(
                array(
                    'label' => lang('home_weekend_ticket_opening_statistics'),
                    'backgroundColor' => 'rgba(197, 61, 169, 0.5)',
                    'borderColor' => '#c53da9',
                    'borderWidth' => 1,
                    'tension' => false,
                    'data' => array(
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0
                    )
                )
            )
        );
        foreach ($tickets as $ticket) {
            $ticket_day = date('l', strtotime($ticket['date']));
            $i          = 0;
            foreach (get_weekdays_original() as $day) {
                if ($ticket_day == $day) {
                    $chart['datasets'][0]['data'][$i]++;
                }
                $i++;
            }
        }
        return $chart;
    }
    public function add_spam_filter($data)
    {
        $this->db->insert('main_shopfreemart.sfm_crm_ticketsspamcontrol', $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id) {
            return true;
        }
        return false;
    }
    public function edit_spam_filter($data)
    {
        $this->db->where('id', $data['id']);
        unset($data['id']);
        $this->db->update('main_shopfreemart.sfm_crm_ticketsspamcontrol', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
    public function delete_spam_filter($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('main_shopfreemart.sfm_crm_ticketsspamcontrol');
        if ($this->db->affected_rows() > 0) {
            logActivity('Tickets Spam Filter Deleted');
            return true;
        }
        return false;
    }
    public function get_tickets_assignes_disctinct()
    {
        return $this->db->query("SELECT DISTINCT(assigned) as assigned FROM main_shopfreemart.sfm_crm_tickets WHERE assigned != 0")->result_array();
    }
}
