<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Get current template assets url
 * @return string Assets url
 */
function template_assets_url()
{
    return base_url('assets/perfex/themes/' . perfex_get_option('clients_default_theme')) . '/';
}
function template_assets_path()
{
    return 'assets/perfex/themes/' . perfex_get_option('clients_default_theme');
}
/**
 * Current theme view part
 * @param  string $name file name
 * @param  array  $data variables passed to view
 */
function perfex_get_template_part($name, $data = array(), $return = false)
{
    $CI =& get_instance();
    if ($return == true) {
        return $CI->load->view('crm/template_parts/' . $name, $data, TRUE);
    }
    $CI->load->view('crm/template_parts/' . $name, $data);
}
/**
 * Get all client themes in themes folder
 * @return array
 */
function get_all_client_themes()
{
    return list_folders(CRM_MODULE_PATH . 'views/themes/');
}
/**
 * Get active client theme
 * @return mixed
 */
function active_clients_theme()
{
    $CI =& get_instance();

    $theme = 'shop';//perfex_get_option('clients_default_theme');
    //ld($theme);
    if ($theme == '') {
        show_error('Default theme is not set');
    }
    //ld(VIEWPATH . 'themes' .DIRECTORY_SEPARATOR. $theme);
    if (!is_dir(VIEWPATH . 'themes' .DIRECTORY_SEPARATOR. $theme)) {
        show_error('Theme does not exists');
    }
    return $theme;
}

perfex_add_action('app_customers_head','do_theme_required_head');
/**
 * Function used in the customers are in head and hook all the necessary data for full app usage
 * @param  array  $params pass params to use
 * @return void
 */
function do_theme_required_head($params = array()){
    ob_start();
    $isRTL = (perfex_is_rtl(true) ? 'true' : 'false');
    echo get_custom_fields_hyperlink_js_function();
    $locale = get_locale_key($params['language']);
    ?>
    <?php if(perfex_get_option('use_recaptcha_customers_area') == 1 && perfex_get_option('recaptcha_secret_key') != '' && perfex_get_option('recaptcha_site_key') != '' && is_connected('google.com')){ ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php } ?>
    <script>
        <?php if(is_staff_logged_in()){  ?>
          var perfex_admin_url = '<?php echo perfex_admin_url(); ?>';
          var perfex_admin_url = '<?php echo perfex_admin_url(); ?>';
          <?php } ?>
          var ci_site_url = '<?php echo ci_site_url(''); ?>';
        // Settings required for javascript
        var calendar_events_limit = "<?php echo perfex_get_option("calendar_events_limit"); ?>";
        var maximum_allowed_ticket_attachments = "<?php echo perfex_get_option("maximum_allowed_ticket_attachments"); ?>";
        var max_php_ini_upload_size = "<?php echo bytesToSize('', file_upload_max_size()); ?>";
        var file_exceds_maxfile_size_in_form = "<?php echo lang('file_exceds_maxfile_size_in_form'); ?>";
        var drop_files_here_to_upload = "<?php echo lang('drop_files_here_to_upload'); ?>";
        var browser_not_support_drag_and_drop = "<?php echo lang('browser_not_support_drag_and_drop'); ?>";
        var remove_file = "<?php echo lang('remove_file'); ?>";
        var tables_pagination_limit = "<?php echo perfex_get_option("tables_pagination_limit"); ?>";
        var date_format = "<?php echo perfex_get_option("dateformat"); ?>";
        date_format = date_format.split('|');
        date_format = date_format[0];
        var default_view_calendar = "<?php echo perfex_get_option('default_view_calendar'); ?>";
        var dt_lang = <?php echo json_encode(get_datatables_language_array()); ?>;
        var discussions_lang = <?php echo json_encode(get_project_discussions_language_array()); ?>;
        var confirm_action_prompt = "<?php echo lang('confirm_action_prompt'); ?>";
        var cf_translate_input_link_tip = "<?php echo lang('cf_translate_input_link_tip'); ?>";
        var cfh_popover_templates  = {};
        // Discussions
        var locale = '<?php echo $locale; ?>';
        var allowed_files = "<?php echo perfex_get_option('allowed_files'); ?>";
        var isRTL = '<?php echo $isRTL; ?>';
        var calendar_first_day = '<?php echo perfex_get_option('calendar_first_day'); ?>';
        var months_json = '<?php echo json_encode(array(lang('January'),lang('February'),lang('March'),lang('April'),lang('May'),lang('June'),lang('July'),lang('August'),lang('September'),lang('October'),lang('November'),lang('December'))); ?>';
        window.addEventListener('load',function(){
            custom_fields_hyperlink();
        });
    </script>
    <?php
    $contents = ob_get_contents();
    ob_end_clean();
    echo $contents;
}
