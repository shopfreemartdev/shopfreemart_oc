<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="row">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="announcementsView"></div>			
		<div id="announcementsGrid"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var displayannouncementsFilterPanel = Cookies.set('ti_displayannouncementsFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displayannouncementsFilterPanel = 'false';
		} else
		{
			displayannouncementsFilterPanel = 'true';
			$( '#announcementsFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displayannouncementsFilterPanel', displayannouncementsFilterPanel);

		reloadData( '#announcements',"<?php echo base_url();?>announcements/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displayannouncementsFilterPanel);
			if (displayannouncementsFilterPanel == null)
			{
				displayannouncementsFilterPanel = 'true';	
				Cookies.set('displayannouncementsFilterPanel', displayannouncementsFilterPanel);
			}
			console.log(displayannouncementsFilterPanel);				
			if (displayannouncementsFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#announcements','<?php echo base_url();?>announcements/data');	

		});
		
	
</script>
