	<?php if($setting['form-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('announcements/pagehead');?>
	</div>
		
			
	
	<div class="sbox">

	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo  $pageNote ;?></small> 

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#announcements')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		 <form action="<?php echo ci_site_url('announcements/save/'.$row['id']); ?>" class='form-horizontal'  id="announcementsFormAjax"
		 parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 
		 
<div class="col-md-12">
						<fieldset><legend> Announcements</legend>
									
								  <div class="form-group  " >
									<label for="Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['id'];?>' name='id'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Title" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <textarea name='title' rows='2' id='title' class='form-control '  
				           ><?php echo $row['title'] ;?></textarea>
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Description" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Description', (isset($fields['description']['language'])? $fields['description']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['description'];?>' name='description'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Start Date" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Start Date', (isset($fields['start_date']['language'])? $fields['start_date']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['start_date'];?>' name='start_date'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="End Date" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('End Date', (isset($fields['end_date']['language'])? $fields['end_date']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['end_date'];?>' name='end_date'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Created By" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Created By', (isset($fields['created_by']['language'])? $fields['created_by']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['created_by'];?>' name='created_by'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Share With" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Share With', (isset($fields['share_with']['language'])? $fields['share_with']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['share_with'];?>' name='share_with'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Created At" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Created At', (isset($fields['created_at']['language'])? $fields['created_at']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['created_at'];?>' name='created_at'
				style='width:150px !important;'	   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Read By" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Read By', (isset($fields['read_by']['language'])? $fields['read_by']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['read_by'];?>' name='read_by'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Deleted" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Deleted', (isset($fields['deleted']['language'])? $fields['deleted']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['deleted'];?>' name='deleted'   />
									  <i> <small></small></i>
									 </div> 
								  </div> </fieldset>
			</div>
			
			
		
			<div style="clear:both"></div>	
				
 		<div class="toolbar-line text-center">		
			<input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.sb_submit'); ?>" />
			<a href="javascript:void(0)" class="btn-sm btn btn-warning" onclick="ajaxViewClose('#announcements')"><?php echo $this->lang->line('core.sb_cancel'); ?></a>
 		</div>
			  		
		</form>

	</div>	
		<?php foreach($subgrid as $md) : ?>
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid"></div>
			</div>	
		</div>
		<?php endforeach;?>	
	
	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo ci_site_url($md['module'].'/detailview/form?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid' ).html( data );
	});		
<?php endforeach ?>
});
</script>				 
<script type="text/javascript">
$(document).ready(function() { 
	 
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('.markItUp').markItUp(mySettings );	

	var form = $('#announcementsFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});	
 	 
});

function showRequest()
{
	$('.formLoading').show();	
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		if(data.action =='submit')
		{
			ajaxViewClose('#announcements');	
		}	
		
		ajaxFilter('#announcements','announcements/data');
		notyMessage(data.message);		
	} else {	
		var n = noty({				
			text: data.message,
			type: 'error',
			layout: 'topRight',
		});	
		return false;
	}	
}	
</script>		 