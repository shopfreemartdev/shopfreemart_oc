	<?php if($setting['view-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('announcements/pagehead');?>
	</div>
		
	
	<div class="sbox">
	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo $pageNote ;?></small>

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#<?php echo  $pageModule ;?>')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		
			<table class="table table-striped table-bordered" >
				<tbody>	

					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['title'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Description', (isset($fields['description']['language'])? $fields['description']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['description'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Start Date', (isset($fields['start_date']['language'])? $fields['start_date']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['start_date'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('End Date', (isset($fields['end_date']['language'])? $fields['end_date']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['end_date'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Created By', (isset($fields['created_by']['language'])? $fields['created_by']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['created_by'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Share With', (isset($fields['share_with']['language'])? $fields['share_with']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['share_with'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Created At', (isset($fields['created_at']['language'])? $fields['created_at']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['created_at'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Read By', (isset($fields['read_by']['language'])? $fields['read_by']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['read_by'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Deleted', (isset($fields['deleted']['language'])? $fields['deleted']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['deleted'] ;?> </td>
						
					</tr>
										
				</tbody>	
			</table>    

		<?php foreach($subgrid as $md) : ?>
		<hr />
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid-<?php echo $id;?>"></div>
			</div>	
		</div>
		<hr />
		<?php endforeach;?>	

	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo ci_site_url($md['module'].'/detailview?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid-<?php echo $id;?>' ).html( data );
	});		
<?php endforeach ?>
});
</script>		  