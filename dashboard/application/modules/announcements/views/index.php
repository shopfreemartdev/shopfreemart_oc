<?php
    if(!isset($_GET['page'])){
      $page = 1;
    }
    else
    {
      $page = $_GET['page'];
    }
    $results_per_page = 3;
    $this_page_first_result = ($page-1)*3;

    $query2 = $this->db->get('announcements');
    $num_rows = $query2->num_rows();
    $total_pages = ceil($num_rows/$results_per_page);

    $this->db->select('*');
    $this->db->from('announcements');
    $this->db->order_by("created_at","desc");
    $this->db->limit($results_per_page,$this_page_first_result);

    $query1 = $this->db->get();
    $data1 =$query1->result_array();
  ?>

<div id="announcementpage" class="page-content row" >
  <!-- Page header -->
  <div class="page-header">
    <div class="page-title">
      <h3 style="margin-left: 20px;"> <?php echo $pageTitle; ?> <small><?php //echo $pageNote ?></small></h3>
    </div>
  </div>


  <div class="row">
    <div class="announcements">
      <?php
      $i = 0;
      if($query1->num_rows()>0){
      foreach ($data1 as $ann) { ?>

        <div class="anncontainer">
          <div class="anntitle">
            <h3>
	            <a href="../account/member/announcements/view/<?php echo $ann['id']?>">
            	<?php echo $ann['title']; ?>
	            </a>

	            <?php
                $date1 = date("Y-m-d",time());      //get current time/date
		            $date2 = $ann['created_at'];        //get announcement date created
		            $timenow = date_create($date1);     //set date format
		            $anntime = date_create($date2);     //set date format
		            $diff=date_diff($timenow,$anntime); //get time difference

	        		if ($diff->days < 3){
	        			echo '<span class="new">New</span>';
	        		}
	            ?>
            </h3>
          </div>

          <div class="anndate-auth">
            <label><?php echo date('F j, Y', strtotime( $ann['created_at'] ));?>, </label>
            <label><a href="#">
              <?php
                $this->db->from('sfm_uap_affiliates');
                $this->db->where('id',$ann['created_by']);
                $query2 = $this->db->get();
                $data2 = $query2->result_array();
                $authorname = $data2['0']['display_name'];
                echo $authorname;
              ?>
            </a></label>
          </div>

          <div class="anncontent">
            <?php echo word_limiter(strip_tags( $ann['description'] ),50)."<a href='../account/member/announcements/view/".$ann['id']."'>Read more</a>."; ?>
          </div>
        </div>

        <?php   }
          }
          else
          echo "<h4>No New Announcements</h4>";
      ?>

	  	<div class="pagination">
	      <ul class="pagination">
	        <?php
	          for($page=1; $page<=$total_pages; $page++){
	            echo '
	              <li><a href="announcements?page='.$page.'">'.$page.'</a></li>
	            ';
	          }
	        ?>
	      </ul>
		</div>
    </div>
  </div>
</div>
