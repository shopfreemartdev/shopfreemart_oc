<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menumodel extends SB_Model 
{

	public $table = 'sfm_cms_menu';
	public $primaryKey = 'menu_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT sfm_cms_menu.* FROM sfm_cms_menu  ";
	}
	public static function queryWhere(  ){
		
		return " WHERE sfm_cms_menu.menu_id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
}

?>
