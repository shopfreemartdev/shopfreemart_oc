<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pagesmodel extends SB_Model 
{

	public $table = 'sfm_cms_pages';
	public $primaryKey = 'pageID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT sfm_cms_pages.* FROM sfm_cms_pages  ";
	}
	public static function queryWhere(  ){
		
		return " WHERE sfm_cms_pages.pageID IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	
}

?>
