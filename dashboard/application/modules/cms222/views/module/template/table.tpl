<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');?>




<?php if($this->session->userdata('gid') ==1) : ?>
<div class="container">
<div class="row">
<div class="col-xs-12 push-right">
<a href="<?php echo ci_site_url('cms/module/rebuild/'.$pageModule.'?rurl='.$pageModule) ?>" class="btn"  title="Rebuild">
<i class="fa fa-cog"></i>&nbsp;<?php echo lang('Rebuild'); ?></a>
<a href="<?php echo ci_site_url('cms/module/config/'.$pageModule) ?>" class="btn"  title="Configuration">
<i class="fa fa-cog"></i>&nbsp;<?php echo $this->lang->line('core.btn_config'); ?></a>
</div>
</div>
</div>
<?php endif; ?>

<?php echo $this->session->flashdata('message');?>

<div id="ajax"><?=$show_message?></div>

<?php usort($tableGrid, "SiteHelpers::_sort"); ?>

<?php if($setting['gridsettings']['gs_toolbar']) : ?>
<?php $this->load->view('{class}/toolbar');?>
<?php endif;?>

<?php if($setting['gridsettings']['gs_portlet']) : ?>
<div class="sbox">
<div class="sbox-title">

<h4 class="pull-left">
<span>
<i class="fa fa-table" style="font-size: 18px !important"></i>
<?php echo $pageTitle ;?>&nbsp;
<small>
<?php echo $pageNote ;?>
</small>
</span>
</h4>
<div class="btn-group pull-right">

<a href="javascript:void(0)" class=""
onclick="reloadData('#{class}','{class}/data')"  title="Reload Data"><i class="fa fa-refresh" style="font-size: 17px; margin-top: 5px;" ></i></a>

</div>

</div>
<div class="sbox-content">
<?php endif;?>

<div class="panel panel-default panel-table">

<!-- gs_filterbar -->
<?php if($setting['gridsettings']['gs_filterbar'])
{
	echo $filterbar;
} ?>

<?php if(empty($rowData)) : ?>
<div class="alert alert-warning">
<center>
<h3><?=lang('no_records_found')?></h3>
</center>
</div>
<?php else: ?>

<!-- Grid Table Row -->
<div class="table-responsive">
<form
action='<?php echo ci_site_url('
{
	class
}/destroy') ?>' class='form-horizontal' id ='{class}Form' method="post" <?php echo $setting['gridsettings']['gs_css']; ?> >

<table class="display dataTable table table-striped table-bordered table-hover table-checkable table-responsive datatable dataTable table-clients no-footer dtr-inline table-hover " id="{class}Table" >
<thead>
<tr>
<?php if($setting['gridsettings']['gs_counter'])
{
	?>
	<th>
	No
	</th>
	<?php
} ?>

<?php if($setting['gridsettings']['gs_checkbox'])
{
	?>
	<th>
	<input type="checkbox" class="checkall" />
	</th>
	<?php
} ?>

<?php if($setting['view-method']=='expand')
{
	?>
	<th>
	</th> <?php
} ?>
<?php foreach ($tableGrid as $k => $t) : ?>
<?php if($t['view'] =='1'): ?>
<th>
<?php echo SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array()))  ?>
</th>
<?php endif; ?>
<?php endforeach; ?>

<?php if($setting['gridsettings']['gs_buttonpanel'])
{
	?>
	-
	<th width="70">
	Action
	</th>
	<?php
} ?>

</tr>
</thead>

<tbody>
<?php if($access['is_add'] =='1' && $setting['inline']=='true'): ?>
<tr id="form-0" >

<?php if($setting['gridsettings']['gs_counter'])
{
	?>
	<td>
	#
	</td>
	<?php
} ?>

<?php if($setting['gridsettings']['gs_checkbox'])
{
	?>
	<th>
	</th>
	<?php
} ?>

<?php if($setting['view-method']=='expand')
{
	?>
	<td>
	</td> <?php
} ?>
<?php foreach ($tableGrid as $t) :
if($t['view'] =='1') : ?>
<td data-form="<?php echo $t['field'];?>" data-form-type="<?php echo  AjaxHelpers::inlineFormType($t['field'],$tableForm);?>">
<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>
</td>
<?php endif;
endforeach ?>

<?php if($setting['gridsettings']['gs_buttonpanel'])
{
	?>
	<td style="width:50px;">

	<button type="button"  class=" btn btn-xs btn-info" rel="#<?php echo $pageModule ;?>Form"
	onclick="ajaxInlineSave('#<?php echo $pageModule ;?>','<?php echo ci_site_url('{class}/quicksave') ;?>','<?php echo ci_site_url('{class}/data?md=') ;?>')">
	<i class="fa fa-save">
	</i>
	</button>
	</td>


	<?php
} ?>

</tr>
<?php endif;?>

<?php foreach ( $rowData as $i => $row ) :
$id = $row->
{
	key
};
?>
<tr class="editable" id="form-<?php echo $row->{key} ;?>">

<?php if($setting['gridsettings']['gs_counter'])
{
	?>
	<td width="50">
	<?php echo ($i+1+$page) ?>
	</td>
	<?php
} ?>

<?php if($setting['gridsettings']['gs_checkbox'])
{
	?>
	<td width="50">
	<input type="checkbox" class="ids" name="id[]" value="<?php echo $row->{key} ?>" />
	</td>
	<?php
} ?>

<?php if($setting['view-method']=='expand'): ?>
<td>
<a href="javascript:void(0)" class="expandable" rel="#row-<?php echo $row->{key} ;?>" data-url="<?php echo ci_site_url('{class}/show/'.$id) ;?>">
<i class="fa fa-plus " >
</i>
</a>
</td>
<?php endif;?>
<?php foreach ( $tableGrid as $j => $field ) : ?>
<?php if($field['view'] =='1'):
$conn = (isset($field['conn']) ? $field['conn'] : array() );
$value = AjaxHelpers::gridFormater($row->$field['field'], $row , $field['attribute'],$conn);

?>
<td align="<?php echo $field['align'];?>" data-values="<?php echo $row->$field['field'] ;?>" data-field="<?php echo  $field['field'] ;?>" data-format="<?php echo htmlentities($value);?>">
<?php echo  $value;?>
</td>
<?php endif; ?>
<?php endforeach; ?>

<?php if($setting['gridsettings']['gs_buttonpanel'])
{
	?>
	<td data-values="action" data-key="<?php echo $row->{key} ;?> " >
	<?php if($setting['gridsettings']['gs_inlinebuttonpanel'])
	echo AjaxHelpers::buttonActionButtonInline('{class}',$access,$row->{key} ,$setting);
	else
	echo AjaxHelpers::buttonAction('{class}',$access,$row->{key} ,$setting);
	?>
	</td>
	<?php
} ?>

</tr>
<?php if($setting['view-method']=='expand'): ?>
<tr style="display:none" class="expanded" id="row-<?php echo $row->{key};?>">

<?php if($setting['gridsettings']['gs_counter'])
{
	?>
	<td class="number">
	</td>
	<?php
} ?>

<td>
</td>
<td>
</td>
<td colspan="<?php echo $colspan;?>" class="data">
</td>
<td>
</td>
</tr>
<?php endif;?>
<?php endforeach; ?>

</tbody>

</table>
</form>
</div>

<?php if($setting['gridsettings']['gs_footer']) : ?>
<?php $this->load->view('ajaxfooter'); ?>
<?php endif;?>
<?php endif; ?>

</div>


<?php if($setting['gridsettings']['gs_portlet']) : ?>
</div>
</div>

<?php endif;?>

<?php if($setting['inline'] =='true')  $this->load->view('cms/module/utility/inlinegrid') ;?>
<?php if($setting['view-method'] =='expand')  $this->load->view('cms/module/utility/extendgrid') ;?>




<script>
	var gridtable;
	$(document).ready(function()
	{
		$('.tips').tooltip();

		gridtable = $('#{class}Table').DataTable(
			{
				"processing": true,
				"serverSide": true,
				"autoWidth": false,
				"lengthMenu": [[10, 25, 50, 100, 0], [10, 25, 50, 100, "All"]],
				"ajax":
				{
					"url": "<?php echo base_url();?>{class}/listdata",
					"type": "POST",
					"data": function ( data )
					{
						data.filtertype = $('#filtertype').val();
					},
					"dataSrc": function ( json )
					{
						$('#total_all_pages').html(json.total_all_pages);
						$('#total_current_page').html(json.total_current_page);
						for ( var i=0, ien=json.data.length ; i<ien ; i++ )
						{
							json.data[i][0] = '<a href=#>'+json.data[i][0]+'</a>';
						}
						return json.data;
					}
				},
				"columnDefs": [
					{
						"targets": 0, "sort":true
					},
					{
						"targets": 1, "sort":true
					},
					{
						"width": "40%", "targets": 2, "sorting":false
					},
					{
						"targets": 3, "sorting":false
					},
					{
						"targets": 4, "sorting":false
					},
					{
						"targets": 5, "sorting":false
					}
				]
				// initComplete: function () {
				// this.api().columns().every( function () {
				//     var column = this;
				//     if (column[0] == 2)
				//     {
				//     var select = $('filtertype')
				//         .appendTo( $(column.footer()).empty() )
				//      // column.data().unique().sort().each( function ( d, j ) {
				//      //     typeselect.append( '<option value="'+d+'">'+d+'</option>' )
				//      // } );

				// 	}
				// } );
				//}
			});

		var typeselect = $('#filtertype').on( 'change', function ()
			{
				var val = $(this).val();
				gridtable.ajax.reload(null,false);
				//gridtable.columns(2).search(val ? '^'+val+'$' : '',true, false).draw();
			} 
			);


		$('input[type="checkbox"],input[type="radio"]').iCheck(
			{
				checkboxClass: 'icheckbox_square-green',
				radioClass: 'iradio_square-green',
			});
		$('#{class}Table .checkall').on('ifChecked',function()
			{
				$('#{class}Table input[type="checkbox"]').iCheck('check');
			});
		$('#{class}Table .checkall').on('ifUnchecked',function()
			{
				$('#{class}Table input[type="checkbox"]').iCheck('uncheck');
			});
		$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
		$('#{class}Paginate .pagination li a').click(function()
			{
				var url = $(this).attr('href');
				if(url != '')
				{
					reloadData('#{class}',url);
				}
				return false ;
			});
		loadControlvalues();

	});

	function filterList()
	{
		$('#filter-form').submit();
	}
	function dosearchbutton_click()
	{
		dofilterList();
	}
	function dofilterbutton_click()
	{
		$('#panel-filter').toggle('slow');

	}

	function dofilterList()
	{
		var attr = '';
		attr ='search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			display
			{
				class
			}FilterPanel = 'false';
		} else
		{
			display
			{
				class
			}FilterPanel = 'true';
			$( '#{class}Filter :input').each(function()
				{
					if(this.value !=='' && this.name !='_token'  && this.name !='search')
					{
						attr += this.name+':'+this.value+'|';
					}
				});
		}
		Cookies.set('display{class}FilterPanel', display{class}FilterPanel);
		reloadData( '#{class}',"{class}/data?search="+attr);
	}

	function dosearchbutton_click()
	{
		dofilterList();
	}


</script>
