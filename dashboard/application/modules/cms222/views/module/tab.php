	<ul class="list-inline">
		<li>
			<a href="<?php echo ci_site_url($row->module_name);?>"><i class="fa fa-cog"></i><?php echo $this->lang->line('core.fr_viewmodule'); ?> </a>
		</li>
		<li>
			<a href="<?php echo ci_site_url('cms/module/rebuild/'.$row->module_id.'?rd='.$row->module_name);?>"><i class="fa fa-reload"></i><?php echo $this->lang->line('core.fr_rebuildmodule'); ?> </a>
		</li>
	</ul>
	
	<div class="">
	<ul class="nav nav-tabs" style="margin-bottom:10px;">
	  <li <?php if($active == 'config') echo 'class="active"';?>><a href="<?php echo ci_site_url('cms/module/config/'.$module_name);?>"><?php echo $this->lang->line('core.modtab_info'); ?> </a></li>
	  <li <?php if($active == 'sql') echo 'class="active"';?> >
	  <a href="<?php echo ci_site_url('cms/module/sql/'.$module_name);?>"><?php echo $this->lang->line('core.modtab_sql'); ?> </a></li>
	  <li <?php if($active == 'table') echo 'class="active"';?>>
	  <a href="<?php echo ci_site_url('cms/module/table/'.$module_name);?>"><?php echo $this->lang->line('core.modtab_table'); ?> </a></li>
	  <li <?php if($active == 'form') echo 'class="active"';?>>
	  <a href="<?php echo ci_site_url('cms/module/form/'.$module_name);?>"><?php echo $this->lang->line('core.modtab_form'); ?> </a></li> 
	  <li <?php if($active == 'sub') echo 'class="active"';?>>
	  <a href="<?php echo ci_site_url('cms/module/sub/'.$module_name);?>">Master Detail</a></li>   
	  <li <?php if($active == 'permission') echo 'class="active"';?>>
	  <a href="<?php echo ci_site_url('cms/module/permission/'.$module_name);?>"><?php echo $this->lang->line('core.modtab_permission'); ?> </a></li>
	   <li <?php if($active == 'rebuild') echo 'class="active"';?> >
	   <a href="javascript://ajax" onclick="SximoModal('<?php echo ci_site_url('cms/module/build/'.$module_name);?>','<?php echo $this->lang->line('core.modtab_rebuildtitle'); ?> <?php echo $module_name;?>')"><?php echo $this->lang->line('core.modtab_rebuild'); ?> </a></li>
	</ul>
	</div>