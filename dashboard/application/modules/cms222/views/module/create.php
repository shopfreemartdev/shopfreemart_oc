
  <div class="container page-content row">

				<!-- Breadcrumbs line -->
				<div class="crumbs">
					<ul id="breadcrumbs" class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.html">Dashboard</a>
						</li>
						<li class="current">
							<a href="pages_calendar.html" title="">Calendar</a>
						</li>
					</ul>

					<ul class="crumb-buttons">
						<li class="first"><a href="charts.html" title=""><i class="icon-signal"></i><span>Statistics</span></a></li>
						<li class="dropdown"><a href="#" title="" data-toggle="dropdown"><i class="icon-tasks"></i><span>Users <strong>(+3)</strong></span><i class="icon-angle-down left-padding"></i></a>
							<ul class="dropdown-menu pull-right">
							<li><a href="form_components.html" title=""><i class="icon-plus"></i>Add new User</a></li>
							<li><a href="tables_dynamic.html" title=""><i class="icon-reorder"></i>Overview</a></li>
							</ul>
						</li>
						<li class="range"><a href="#">
							<i class="icon-calendar"></i>
							<span>May 30, 2017 - June 28, 2017</span>
							<i class="icon-angle-down"></i>
						</a></li>
					</ul>
				</div>
				<!-- /Breadcrumbs line -->

				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Form Layouts</h3>
						<span>Good morning, John!</span>
					</div>

					<!-- Page Stats -->
					<ul class="page-stats">
						<li>
							<div class="summary">
								<span>New orders</span>
								<h3>17,561</h3>
							</div>
							<div id="sparkline-bar" class="graph sparkline hidden-xs"><canvas width="96" height="35" style="display: inline-block; width: 96px; height: 35px; vertical-align: top;"></canvas></div>
							<!-- Use instead of sparkline e.g. this:
							<div class="graph circular-chart" data-percent="73">73%</div>
							-->
						</li>
						<li>
							<div class="summary">
								<span>My balance</span>
								<h3>$21,561.21</h3>
							</div>
							<div id="sparkline-bar2" class="graph sparkline hidden-xs"><canvas width="96" height="35" style="display: inline-block; width: 96px; height: 35px; vertical-align: top;"></canvas></div>
						</li>
					</ul>
					<!-- /Page Stats -->
				</div>
				<!-- /Page Header -->

    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
          <h3> Module <small> List Of All Modules</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="<?php echo ci_site_url('dashboard');?>">Dashboard</a></li>
		<li><a href="<?php echo ci_site_url('cms/module') ;?>"> Module </a></li>
        <li class="active"> Create New  </li>
      </ul>
	</div>  



	 <div class="page-content-wrapper m-t">  
	<?php echo $this->session->flashdata('message');?>	
	<form class="form-horizontal" action="<?php echo ci_site_url('cms/module/saveCreate/');?>" method="post" parsley-validate novalidate>


	<div class="form-group has-feedback">
		<label class="col-sm-3 text-right"> Module Name / Title </label>
		<div class="col-sm-9">	
			<input name="module_title" class="form-control input-sm" placeholder="Title Name" required />
		</div>
	</div>		
	
	<div class="form-group ">
		<label class="col-sm-3 text-right"> Module Note   </label>
		<div class="col-sm-9">	
		<input name="module_note" class="form-control input-sm" placeholder="Short description module" />
		
		</div>
	</div>
	<div class="form-group ">
		<label class="col-sm-3 text-right"> Template :  </label>
		<div class="col-sm-9">	
			<label class="radio">	
				<input type="radio" name="module_template" value="native" checked="checked" /> Native Crud   <br />
				<small style="font-style:italic"> Add,Edit,View in new page , Native php sorting and pagination </small> 
			</label>
			<label class="radio">	
				<input type="radio" name="module_template" value="ajax" />  Ajax Crud <br />				
				
				<small style="font-style:italic"> Edit Modal, Modal Add , Modal View , Copy Rows , lock master key values , Ajax sorting and pagination </small> 
				
			</label>
					
		</div>
	</div>

	<div class="form-group ">
		<label class="col-sm-3 text-right">Class Controller </label>
		<div class="col-sm-9">	
		<input name="module_name" class="form-control input-sm" placeholder="Class Controller / Module Name" required />
	  	
		</div>
	</div>	
		
	
	<div class="form-group">
		<label class="col-sm-3 text-right"> Module Table   </label>
		<div class="col-sm-9">	
			<select name="module_db" class="form-control input-sm">
				<option value="notable">No Table</option>
				<?php foreach($tables as $table){?>
					<option value="<?php echo $table;?>"><?php echo $table;?></option>
				<?php } ?>
			</select>	 	
		</div>
	</div>	
		
	<div class="form-group " style="display:none;">
		<label class="col-sm-3 text-right">Author </label>
		<div class="col-sm-9">	
	  
		
		</div>
	</div>	
		


	<div class="form-group switchSql">
		<label class="col-sm-3 text-right">  </label>
		<div class="col-sm-9">	
			<label class="radio radio-inline">
				<input type="radio" name="creation" value="auto"  checked="checked"  /> 
				Auto Mysql Statment 
			</label>		
			<label class="radio radio-inline">
				<input type="radio" name="creation" value="manual"  />
				Manual Mysql Statment 
			</label>		
		</div>
	</div>	
	
	<div class="form-group manualsql">
		<label class="col-sm-3 text-right">  </label>
		<div class="col-sm-9">
			<textarea class="form-control input-sm" name="sql_select" placeholder="SQL Select & Join Statement" rows="3" id="sql_select"></textarea>	  
		</div> 
	</div>	
	
	<div class="form-group manualsql">
		<label class="col-sm-3 text-right">  </label>
		<div class="col-sm-9">
			<textarea class="form-control input-sm" name="sql_where" placeholder="SQL Where Statement" rows="2" id="sql_where"></textarea>	 
		</div> 
	</div>		

	<div class="form-group manualsql">
		<label class="col-sm-3 text-right">  </label>
		<div class="col-sm-9">
			<textarea class="form-control input-sm" name="sql_group" placeholder="SQL Grouping Statement" rows="2" id="sql_group"></textarea>	
			
		</div> 
	</div>	
	
		
      <div class="form-group">
		<label class="col-sm-3 text-right">&nbsp;</label>
		<div class="col-sm-9">	
	  	<button type="submit" class="btn btn-primary ">  Create New Module </button>
		</div>	  

      </div>
    
 </form>
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('.manualsql').hide();
		$('.switchSql input:radio').on('ifClicked', function() {
		  val = $(this).val(); 
			if(val == 'manual')
			{
				$('.manualsql').show();
				$('#sql_select').attr("required","true");
				$('#sql_where').attr("required","true");
				
			} else {
				$('.manualsql').hide();
				$('#sql_select').removeAttr("required");
				$('#sql_where').removeAttr("required");
	
			}		  
		  
		});

	});
	
</script>
