<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Accountsummarymodel extends SB_Model 
{

	public $table = 'admin_groups';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT admin_groups.* FROM admin_groups   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE admin_groups.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
	public function getAdditionalCriteria($param){
		return "";
		//return " AND sfm_uap_transactions.user_id = '".$param."'" ;
	}

	public function set_defaults($data)
	{
		return $data;
	}	

    public static function getTypefilter($filtertype)
    {
        if (!empty($filtertype)) {
            return ""; // AND st.description LIKE '$filtertype%' ";
        } else {
            return "";
        }

    }	
	
}

?>
