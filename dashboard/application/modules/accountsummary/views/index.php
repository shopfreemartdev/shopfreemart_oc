<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="page-content-wrapper m-t">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="accountsummaryView"></div>			
		<div id="accountsummaryGrid"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var displayaccountsummaryFilterPanel = Cookies.set('ti_displayaccountsummaryFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displayaccountsummaryFilterPanel = 'false';
		} else
		{
			displayaccountsummaryFilterPanel = 'true';
			$( '#accountsummaryFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displayaccountsummaryFilterPanel', displayaccountsummaryFilterPanel);

		reloadData( '#accountsummary',"<?php echo base_url();?>accountsummary/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displayaccountsummaryFilterPanel);
			if (displayaccountsummaryFilterPanel == null)
			{
				displayaccountsummaryFilterPanel = 'true';	
				Cookies.set('displayaccountsummaryFilterPanel', displayaccountsummaryFilterPanel);
			}
			console.log(displayaccountsummaryFilterPanel);				
			if (displayaccountsummaryFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#accountsummary','<?php echo base_url();?>accountsummary/data');	

		});
		
	
</script>
