<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client_personal_refferal extends  Admin_Controller
{

	public $module 		= 'client_personal_refferal';
	public $per_page	= '10';
	public $default_filter_type = '31';

	function __construct() {
		parent::__construct();

		$this->load->model('client_personal_refferalmodel');
		$this->load->helper(ucfirst('client_personal_refferal'));
		$this->model = $this->client_personal_refferalmodel;

		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
		$this->data = array_merge( $this->data, array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'	=> 'client_personal_refferal',
			'pageUrl'			=>  base_url('client_personal_refferal'),
			'controller' => 'Admin_Controller',
		));

		//if(!$this->session->userdata('logged_in')) redirect('user/login',301);

	}

	public function index()
	{
		$filtertype = ((isset($_GET['linkfilter'])) ? $_GET['linkfilter'] : '');


		$this->data['linkfilter'] = (!empty($filtertype) ? $filtertype : $this->default_filter_type )  ;


		$countsql = "CALL update_filter_totals(".$this->info['id'].",".ci_get_current_user_id().")";
		$this->db->query($countsql);


		$this->render_view('client_personal_refferal/index');

	}

	private function load_data($parameter='')
	{
		if($this->access['is_view'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }
		// Filter sort and order for query
		$sort = (($this->input->get('sort', true) != null) ? $this->input->get('sort', true): $this->info['setting']['orderby'] );
		$order = (($this->input->get('order', true) != null) ?$this->input->get('order', true) : $this->info['setting']['ordertype'] );
		$rowperpage = (($this->input->get('rows', true) != null) ?  $this->input->get('rows', true) : $this->info['setting']['perpage']);
		if ($rowperpage == 'All') $rowperpage = 0;
		$filter = (($this->input->get('search', true) != null) ? $this->input->get('search', true) : '');

		//$sort = ($this->input->get('sort', true) !='' ? $this->input->get('sort', true) :  $this->info['setting']['orderby']);
		//$order = ($this->input->get('order', true) !='' ? $this->input->get('order', true) :  $this->info['setting']['ordertype']);

		// End Filter sort and order for query
		// Filter Search for query
		$filter = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
		// End Filter Search for query

		$page = max(1, (int) $this->input->get('page', 1));
		$params = array(
			'page'		=> $page ,
			'limit'		=> $rowperpage ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;

		$this->data['rowData']		= $results['rows'];
		// Build Pagination

		$pagination = $this->paginatorajax( array(
			'total_rows' => $results['total'] ,
			'per_page'	 => $params['limit']
		),$parameter);
		$this->data['pagination']	= $pagination;
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['param']		= $params;
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= SiteHelpers::viewColSpan($this->info['config']['grid']);
		$this->data['setting'] 		= $this->info['setting'];

		$this->data['total_rows'] = $results['total'];
		$this->data['page_from'] = (int)$page;
		$this->data['page_to'] =  (int)($page)  + (int)$params['limit'];

		// Group users permission
		$this->data['access']		= $this->access;
		// Render into template
	}

	public function tabindex($criteria = null)
	{
		$this->data['tabcriteria']		=  $criteria;
		$this->data['access']		= $this->access;
		$this->data['tab']		= "client_personal_refferal";
    	$this->render_tabview('client_personal_refferal/tabindex');
	}

	function tabdata()
	{
		$this->load_data('tabdata');
		$this->render_content('client_personal_refferal/tabtable');
	}

	function data()
	{
		$buffer = $this->ajaxdata('',true);
		$this->data['dataset'] = json_encode($buffer);
		$arraycolumndefs = array();
		$arraycolumns	= array();
        $tableGrid = $this->data['tableGrid'];
        $itemdef = array();
        $ctr=0;

        foreach ($tableGrid as $k => $t) {

            //if ($t['view'] == '1')
          	//{
             	$arraycolumndefs['targets']=$ctr;
    		    $arraycolumndefs['visible']= $t['view'];
    		    $arraycolumndefs['searchable']= $t['search'];
          		$arraycolumns['name']= $t['field'];
          		$arraycolumns['data']= $t['field'];
          		$this->data['columns'][] = $arraycolumns;

          		$arraycolumndefs['name']= $t['field'];
          		$arraycolumndefs['data']= $t['field'];
          		if (!empty($t['align']))
          		{
          			if ($t['align']=='right')
          		 		$arraycolumndefs['className'] = 'dt-'.$t['align'].' pad-left-right-20';
          		    else
          		    	$arraycolumndefs['className'] = 'dt-'.$t['align'];
          		}
          		if (!empty($t['width'])) {
          			$arraycolumndefs['width'] ="'".$t['width']."px'";
          			$arraycolumns['width']="'".$t['width']."px'";
             	}
          		$this->data['columndefs'][]=$arraycolumndefs;
          		$ctr++ ;
			//}
		}

		$this->data['fields'] =  AjaxHelpers::fieldLang($this->info['config']['grid']);
		//$this->data['columns'][] = array('name' => '_action','data'=>'_action','searchable' => false);
		//$this->data['columndefs'][]=array('name' => '_action','data'=>'_action','targets' => $ctr,'searchable' => false);

		$this->data['columns'] = json_encode($this->data['columns']);
		$this->data['columndefs'] = json_encode($this->data['columndefs']);

		$this->data['linkfilters'] = $this->model->get_linkfilters($this->info['id']);

	    $filtertype = ((isset($_GET['linkfilter'])) ? $_GET['linkfilter'] : '');
	    if (empty($filtertype))
        $filtertype = ((isset($_POST['filtertype'])) ? $_POST['filtertype'] : '');
	    $rankfilters = array();
	    if (empty($filtertype))
	    {
			$this->data['currentlinkfilter'] = 1;
	    }
	    else
	    {
			$this->data['currentlinkfilter'] = $filtertype;
			//$rankfilters = (!empty($this->model->get_Typefilter($filtertype)) ? $this->model->get_Typefilter($filtertype)['rankfilters'] : array());
			$rankfilters = array();

		}
		$this->data['rankfilters'] = json_encode($rankfilters);
		$this->data['row'] = $this->model->getColumnTable($this->info['table']);
		$this->render_content('client_personal_refferal/table');
	}

	function show( $id = null)
	{
		if($this->access['is_detail'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable($this->info['table']);
		}

		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		$this->data['fields'] =  AjaxHelpers::fieldLang($this->info['config']['grid']);
		$this->data['id'] = $id;
		$this->data['setting'] 		= $this->info['setting'];
		$this->render_content('client_personal_refferal/view');
	}

	function add( $id = null )
	{

		if($id =='')
			if($this->access['is_add'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }

		if($id !='')
			if($this->access['is_edit'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }

		$row = $this->model->getRow( $id );
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable($this->info['table']);
		}
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		$this->data['id'] = $id;
		$this->data['setting'] 		= $this->info['setting'];
		$this->render_content('client_personal_refferal/form');

	}

	function save() {

		$rules = $this->validateForm();

		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() )
		{
			$data = $this->validatePost();
			$ID = $this->model->insertRow($data , $this->input->get_post( 'id' , true ));
			// Input logs
			if( $this->input->get( 'id' , true ) =='')
			{
				$this->inputLogs("New Entry row with ID : $ID  , Has Been Save Successfull");
			} else {
				$this->inputLogs(" ID : $ID  , Has Been Changed Successfull");
			}
			// Redirect after save
			if($this->input->post('apply'))
			{
				$action = 'apply';
			} else {
				$action = 'submit';

			}
				header('content-type:application/json');
				echo json_encode(array(
					'status'	=>'success',
					'message'	=> ' Data has been saved succesfuly !',
					'action'	=>  $action
					));

		} else {
			header('content-type:application/json');
			echo json_encode(array(
					'message'	=> validation_errors('<li>', '</li>'),
					'status'	=> 'error'
					));

		}
	}

	function destroy()
	{
		if($this->access['is_remove'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }
		if(!is_null($this->input->post('id')))
		{
			$this->model->destroy($this->input->post( 'id' , true ));
			$this->inputLogs("ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
			header('content-type:application/json');
			echo json_encode(array(
				'status'=>'success',
				'message'=> SiteHelpers::alert('success','Data Has Been Removed Successfull')
			));
		} else {
			header('content-type:application/json');
			echo json_encode(array(
				'status'=>'error',
				'message'=> 'Ops , Something Went Wrong !'
			));

		}
	}


    function listdata($criteria_filter='all')
    {
        $output = $this->load_listdata($criteria_filter);
        echo json_encode($output);
    }


    function load_listdata($criteria_filter='all')
    {

        if ($this->access['is_view'] == 0) {echo SiteHelpers::alert('error', ' You are not allowed to view this page');die;}

        // Filter sort and order for query
        $start = ((isset($_POST['start'])) ? $_POST['start'] : 0);

        $sortcol = ((isset($_POST['order']['0']['column'])) ? $_POST['order']['0']['column'] : 0);
        $sortdir = ((isset($_POST['order']['0']['dir'])) ? $_POST['order']['0']['dir'] : 'asc');
        $length = ((isset($_POST['length'])) ? $_POST['length'] : 10);

        //$sort = (($sortcol != null) ? $sortcol : $this->info['setting']['orderby']);
        $order = (($sortdir != null) ? $sortdir : $this->info['setting']['ordertype']);
        $rowperpage = (($length != null) ? $length : $this->info['setting']['perpage']);

        if ($rowperpage == 'All') {
            $rowperpage = 0;
        }

        $filter = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
        $filtertype = ((isset($_POST['filtertype'])) ? $_POST['filtertype'] : '');
        $filter .= $this->model->getTypefilter($filtertype);

        $search = ((isset($_POST['search']['value'])) ? $_POST['search']['value'] : '');
        //$search = $this->model->getAdditionalCriteria($search);

        $ctr=0;
        $tableGrid = $this->info['config']['grid'];
        $criteria = "";
		foreach ($tableGrid as $k => $t) :
			if($t['view'] =='1'):
				if (!empty($criteria))  $criteria .= " OR ";
				$criteria .= $t['alias'].'.'.$t['field']." LIKE '$search%'";

				if ($ctr == $sortcol)
				{
					$sort =  $t['alias'].'.'.$t['field'];
				}
				$ctr++;
			endif;
		endforeach;
		if (!empty($criteria))
		{
			$search = " AND ( ".$criteria." )";
		}

        $page = max(1, (int) $start);
        $params = array(
            'page'   => $page,
            'limit'  => $rowperpage,
            'sort'   => $sort,
            'order'  => $order,
            'params' => $filter . $search,
            'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),
        );

        // Get Query
        $this->model->filter=$criteria_filter;
        $results = $this->model->getRows($params);

        // Build pagination setting
        $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
        $this->data['rowData'] = $results['rows'];

        // Row grid Number
        $this->data['i'] = ($page * $params['limit']) - $params['limit'];

        // Grid Configuration
        $this->data['param'] = $params;
        $this->data['tableGrid'] = $this->info['config']['grid'];
        $this->data['tableForm'] = $this->info['config']['forms'];
        $this->data['colspan'] = SiteHelpers::viewColSpan($this->info['config']['grid']);
        $this->data['setting'] = $this->info['setting'];
        $this->data['total_rows'] = $results['total'];
        $this->data['page_from'] = (int) $page;
        $this->data['page_to'] = (int) ($page) + (int) $params['limit'];

        // Group users permission
        $this->data['access'] = $this->access;

        $output = array(
            "draw"                     => ((isset($_POST['draw'])) ? $_POST['draw'] : 1),
            "recordsTotal"             => $results['total'],
            "recordsFiltered"          => $results['total'],
            "TotalRows"		           => $results['total'],
            "data"                     => array(),
        );


        foreach ($results['rows'] as $row_key => $row_val) {
            $col_array = array();
            $ctr = 0;
            foreach ($row_val as $col_key => $col_val) {
            	 $field  = $this->data['tableGrid'][$ctr];
	 			if($field['view'] =='1'):
					$conn = (isset($field['conn']) ? $field['conn'] : array() );
					$value = AjaxHelpers::gridFormater($col_val, $row_val , $field['attribute'],$conn);
					$col_array[] = $value;
				endif;
				$ctr++;
            }
           	$col_array[] = perfex_do_action('client_personal_refferal_render_table_row_action_buttons',array( 'row' =>$row_val,'access' => $this->data['access'],'setting' =>$this->data['setting']));
           	$output['data'][] = $col_array;
        }
        return $output;

    }


	function linkfilter($id)
	{
		$this->data['linkfilter'] = $id;
		$this->render_view('client_personal_refferal/index');
	}

    function ajaxdata($criteria_filter='',$raw=false)
    {
    	$filtersponsor =  ((isset($_POST['filtersponsor'])) ? $_POST['filtersponsor'] : '');
    	$filterranks =  ((isset($_POST['filterranks'])) ? $_POST['filterranks'] : '');

    	if(!empty($filtersponsor))
    	{
    		  if(!empty($criteria_filter)) $criteria_filter .= " AND ";
    		 $criteria_filter .= "((su.id LIKE '%$filtersponsor%') OR (su.user_login LIKE '%$filtersponsor%')  OR (su.user_email LIKE '%$filtersponsor%') OR (su.display_name LIKE '%$filtersponsor%'))";
    	}

    	if(!empty($filterranks))
    	{
    		if(!empty($criteria_filter)) $criteria_filter .= " AND ";
    		 $strfilterranks = implode(",", $filterranks);
    		 $criteria_filter .= " (rank_id in ($strfilterranks)) ";
    	}

    	$results = $this->load_ajaxdata($criteria_filter);
        $output = array(
            "draw"                     => ((isset($_POST['draw'])) ? $_POST['draw'] : 1),
            "recordsTotal"             => $results['total'],
            "recordsFiltered"          => $results['filtered'],
            "TotalRows"                  => $results['total'],
            "data"                     => array(),
        );

        $tableGrid = $this->data['tableGrid'];
        //usort($tableGrid, "SiteHelpers::_sort");

    	foreach ($this->data['rowData'] as $row_key => $row_val) {
            $col_array = array();
            foreach ($tableGrid as $k => $t) {

              //  if ($t['view'] == '1') {
                	$col_name = $t['field'];
					$col_val = $row_val->$col_name;
					$conn = (isset($t['conn']) ? $t['conn'] : array() );
					$value = AjaxHelpers::gridFormater($col_val, $row_val , $t['attribute'],$conn);
					$col_array[$col_name] = $value;
               // }
			}
          	//$colvalue =  AjaxHelpers::ButtonInline_client_personal_refferal2($this->data['access'],$row_val->id ,$this->data['setting']);

            $colvalue = _client_personal_refferal_render_table_row_action_buttons(array( 'row' => $row_val,'access' => $this->data['access'],'setting' => $this->data['setting']));

            $col_array['_action'] = $colvalue;
           	$output['data'][] = $col_array;
        }
        if ($raw)
        	return $output['data'];
        else
        	echo json_encode($output);
	}

	// START OF CUSTOM CODE// END OF CUSTOM CODE

}