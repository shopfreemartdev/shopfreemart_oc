	<?php if($setting['view-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('client_personal_refferal/pagehead');?>
	</div>
		
	
	<div class="sbox">
	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo $pageNote ;?></small>

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#<?php echo  $pageModule ;?>')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		
			<table class="table table-striped table-bordered table-hover table-checkable table-responsive datatable dataTable table-clients no-footer dtr-inline table-bordered" >
				<tbody>	

					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('User Id', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['user_id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Sponsorkey', (isset($fields['sponsorkey']['language'])? $fields['sponsorkey']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['sponsorkey'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Display Name', (isset($fields['display_name']['language'])? $fields['display_name']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['display_name'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Username', (isset($fields['username']['language'])? $fields['username']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['username'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Firstname', (isset($fields['firstname']['language'])? $fields['firstname']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['firstname'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Lastname', (isset($fields['lastname']['language'])? $fields['lastname']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['lastname'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Email', (isset($fields['email']['language'])? $fields['email']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['email'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Phone', (isset($fields['phone']['language'])? $fields['phone']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['phone'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Rank Id', (isset($fields['rank_id']['language'])? $fields['rank_id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['rank_id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Rank', (isset($fields['rank']['language'])? $fields['rank']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['rank'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Override Rank', (isset($fields['override_rank']['language'])? $fields['override_rank']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['override_rank'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Company', (isset($fields['company']['language'])? $fields['company']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['company'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Address1', (isset($fields['address1']['language'])? $fields['address1']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['address1'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Address2', (isset($fields['address2']['language'])? $fields['address2']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['address2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('City', (isset($fields['city']['language'])? $fields['city']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['city'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('State', (isset($fields['state']['language'])? $fields['state']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['state'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Zipcode', (isset($fields['zipcode']['language'])? $fields['zipcode']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['zipcode'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Country', (isset($fields['country']['language'])? $fields['country']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['country'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Shipping Address1', (isset($fields['shipping_address1']['language'])? $fields['shipping_address1']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['shipping_address1'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Shipping Address2', (isset($fields['shipping_address2']['language'])? $fields['shipping_address2']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['shipping_address2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Shipping City', (isset($fields['shipping_city']['language'])? $fields['shipping_city']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['shipping_city'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Shipping State', (isset($fields['shipping_state']['language'])? $fields['shipping_state']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['shipping_state'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Shipping Zipcode', (isset($fields['shipping_zipcode']['language'])? $fields['shipping_zipcode']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['shipping_zipcode'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Shipping Country', (isset($fields['shipping_country']['language'])? $fields['shipping_country']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['shipping_country'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Paid', (isset($fields['paid']['language'])? $fields['paid']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['paid'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Unilevel', (isset($fields['unilevel']['language'])? $fields['unilevel']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['unilevel'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Replicated Url', (isset($fields['replicated_url']['language'])? $fields['replicated_url']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['replicated_url'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Foundingpositions', (isset($fields['foundingpositions']['language'])? $fields['foundingpositions']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['foundingpositions'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Hasfoundingpositions', (isset($fields['hasfoundingpositions']['language'])? $fields['hasfoundingpositions']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['hasfoundingpositions'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Upline Diamond', (isset($fields['upline_diamond']['language'])? $fields['upline_diamond']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['upline_diamond'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Upline Doublediamond', (isset($fields['upline_doublediamond']['language'])? $fields['upline_doublediamond']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['upline_doublediamond'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Upline Triplediamond', (isset($fields['upline_triplediamond']['language'])? $fields['upline_triplediamond']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['upline_triplediamond'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Upline Ambassador', (isset($fields['upline_ambassador']['language'])? $fields['upline_ambassador']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['upline_ambassador'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Upline Crown', (isset($fields['upline_crown']['language'])? $fields['upline_crown']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['upline_crown'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Date Registered', (isset($fields['date_registered']['language'])? $fields['date_registered']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['date_registered'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Downlines', (isset($fields['downlines']['language'])? $fields['downlines']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['downlines'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Referralsales', (isset($fields['referralsales']['language'])? $fields['referralsales']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['referralsales'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Override Referralsales', (isset($fields['override_referralsales']['language'])? $fields['override_referralsales']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['override_referralsales'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Earnings', (isset($fields['earnings']['language'])? $fields['earnings']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['earnings'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Commissions2', (isset($fields['commissions2']['language'])? $fields['commissions2']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['commissions2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Commissions Overall', (isset($fields['commissions_overall']['language'])? $fields['commissions_overall']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['commissions_overall'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Commissions Debits', (isset($fields['commissions_debits']['language'])? $fields['commissions_debits']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['commissions_debits'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Coins2', (isset($fields['coins2']['language'])? $fields['coins2']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['coins2'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Coins Overall', (isset($fields['coins_overall']['language'])? $fields['coins_overall']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['coins_overall'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Coins Debits', (isset($fields['coins_debits']['language'])? $fields['coins_debits']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['coins_debits'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Ewallet', (isset($fields['ewallet']['language'])? $fields['ewallet']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['ewallet'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Withdrawns', (isset($fields['withdrawns']['language'])? $fields['withdrawns']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['withdrawns'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Visits', (isset($fields['visits']['language'])? $fields['visits']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['visits'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Conversion', (isset($fields['conversion']['language'])? $fields['conversion']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['conversion'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Firstpurchasedate', (isset($fields['firstpurchasedate']['language'])? $fields['firstpurchasedate']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['firstpurchasedate'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Orders', (isset($fields['orders']['language'])? $fields['orders']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['orders'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Pending Orders', (isset($fields['pending_orders']['language'])? $fields['pending_orders']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['pending_orders'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Unpaid Invoices', (isset($fields['unpaid_invoices']['language'])? $fields['unpaid_invoices']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['unpaid_invoices'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Paid Invoices', (isset($fields['paid_invoices']['language'])? $fields['paid_invoices']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['paid_invoices'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Commissions', (isset($fields['fm_commissions']['language'])? $fields['fm_commissions']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_commissions'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Commissions Overall', (isset($fields['fm_commissions_overall']['language'])? $fields['fm_commissions_overall']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_commissions_overall'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Commissions Overall Sfm', (isset($fields['fm_commissions_overall_sfm']['language'])? $fields['fm_commissions_overall_sfm']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_commissions_overall_sfm'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Commissions Reserves', (isset($fields['fm_commissions_reserves']['language'])? $fields['fm_commissions_reserves']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_commissions_reserves'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Withdrawals', (isset($fields['fm_withdrawals']['language'])? $fields['fm_withdrawals']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_withdrawals'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Commissions Purchases', (isset($fields['fm_commissions_purchases']['language'])? $fields['fm_commissions_purchases']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_commissions_purchases'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Commissions Debits', (isset($fields['fm_commissions_debits']['language'])? $fields['fm_commissions_debits']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_commissions_debits'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Coins', (isset($fields['fm_coins']['language'])? $fields['fm_coins']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_coins'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Coins Overall', (isset($fields['fm_coins_overall']['language'])? $fields['fm_coins_overall']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_coins_overall'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Coins Overall Sfm', (isset($fields['fm_coins_overall_sfm']['language'])? $fields['fm_coins_overall_sfm']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_coins_overall_sfm'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Coins Reserves', (isset($fields['fm_coins_reserves']['language'])? $fields['fm_coins_reserves']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_coins_reserves'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Coins Purchases', (isset($fields['fm_coins_purchases']['language'])? $fields['fm_coins_purchases']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_coins_purchases'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Fm Coins Debits', (isset($fields['fm_coins_debits']['language'])? $fields['fm_coins_debits']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['fm_coins_debits'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Pending Commissions', (isset($fields['pending_commissions']['language'])? $fields['pending_commissions']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['pending_commissions'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Pending Coins', (isset($fields['pending_coins']['language'])? $fields['pending_coins']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['pending_coins'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Pending Withdrawals', (isset($fields['pending_withdrawals']['language'])? $fields['pending_withdrawals']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['pending_withdrawals'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Pending Transfers', (isset($fields['pending_transfers']['language'])? $fields['pending_transfers']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['pending_transfers'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Lastdate', (isset($fields['lastdate']['language'])? $fields['lastdate']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['lastdate'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Deleted', (isset($fields['deleted']['language'])? $fields['deleted']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['deleted'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Uid', (isset($fields['uid']['language'])? $fields['uid']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['uid'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Start Data', (isset($fields['start_data']['language'])? $fields['start_data']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['start_data'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Open Tickets', (isset($fields['open_tickets']['language'])? $fields['open_tickets']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['open_tickets'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Closed Tickets', (isset($fields['closed_tickets']['language'])? $fields['closed_tickets']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['closed_tickets'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['status'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('User Type', (isset($fields['user_type']['language'])? $fields['user_type']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['user_type'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Is Admin', (isset($fields['is_admin']['language'])? $fields['is_admin']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['is_admin'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Avatar', (isset($fields['avatar']['language'])? $fields['avatar']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['avatar'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Role Id', (isset($fields['role_id']['language'])? $fields['role_id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['role_id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Message Checked At', (isset($fields['message_checked_at']['language'])? $fields['message_checked_at']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['message_checked_at'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Notification Checked At', (isset($fields['notification_checked_at']['language'])? $fields['notification_checked_at']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['notification_checked_at'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Active', (isset($fields['active']['language'])? $fields['active']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['active'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['title'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Datecreated', (isset($fields['datecreated']['language'])? $fields['datecreated']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['datecreated'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Password', (isset($fields['password']['language'])? $fields['password']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['password'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('New Pass Key', (isset($fields['new_pass_key']['language'])? $fields['new_pass_key']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['new_pass_key'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('New Pass Key Requested', (isset($fields['new_pass_key_requested']['language'])? $fields['new_pass_key_requested']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['new_pass_key_requested'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Last Ip', (isset($fields['last_ip']['language'])? $fields['last_ip']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['last_ip'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Last Login', (isset($fields['last_login']['language'])? $fields['last_login']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['last_login'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Last Password Change', (isset($fields['last_password_change']['language'])? $fields['last_password_change']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['last_password_change'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Profile Image', (isset($fields['profile_image']['language'])? $fields['profile_image']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['profile_image'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Direction', (isset($fields['direction']['language'])? $fields['direction']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['direction'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Default Language', (isset($fields['default_language']['language'])? $fields['default_language']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['default_language'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Commissions', (isset($fields['commissions']['language'])? $fields['commissions']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['commissions'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Coins', (isset($fields['coins']['language'])? $fields['coins']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['coins'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Real Firstname', (isset($fields['real_firstname']['language'])? $fields['real_firstname']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['real_firstname'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Real Lastname', (isset($fields['real_lastname']['language'])? $fields['real_lastname']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['real_lastname'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Soap Sale Count', (isset($fields['soap_sale_count']['language'])? $fields['soap_sale_count']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['soap_sale_count'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Soap Sale Start Date', (isset($fields['soap_sale_start_date']['language'])? $fields['soap_sale_start_date']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['soap_sale_start_date'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Has Soap Sale', (isset($fields['has_soap_sale']['language'])? $fields['has_soap_sale']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['has_soap_sale'] ;?> </td>
						
					</tr>
										
				</tbody>	
			</table>    

		<?php foreach($subgrid as $md) : ?>
		<hr />
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid-<?php echo $id;?>"></div>
			</div>	
		</div>
		<hr />
		<?php endforeach;?>	

	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo base_url($md['module'].'/detailview?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid-<?php echo $id;?>' ).html( data );
	});		
<?php endforeach ?>
});
</script>		  


  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->