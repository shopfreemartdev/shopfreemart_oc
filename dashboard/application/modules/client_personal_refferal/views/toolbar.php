    <div class="toolbar-line row">
    	<div class="col-md-12">
    		<div class="btn-group pull-right">	
			<?php  if($access['is_excel'] ==1) :?>
					<div class="btn-group">				
					<?php if($access['is_add'] ==1) :
						echo AjaxHelpers::buttonActionCreate($pageModule,$setting);
					   endif;
					  
					if($access['is_remove'] ==1) : ?>		
					<a href="javascript:void(0);"  onclick="ajaxRemove('#client_personal_refferal','client_personal_refferal');" class="tips btn btn-default" title="Remove">
					<i class="fa fa-trash-o"></i> </a>
					<?php endif; ?>
					<a href="client_personal_refferal/flysearch" onclick="SximoModal(this.href,'Advance Search'); return false;" class="tips btn btn-default"  title=" Search ">
					<i class="fa fa-search"></i></a>
					<a href="javascript:void(0)" class="tips btn btn-default" 
					onclick="reloadData('#client_personal_refferal','client_personal_refferal/data')"  title="Reload Data"><i class="fa fa-refresh"></i></a>					
				   <button type="button" class="btn btn-default dropdown-toggle tips"  title=" Download "
					  data-toggle="dropdown">
					  <i class="fa fa-download"></i>  <span class="caret"></span>
				   </button>
				   <ul class="dropdown-menu" role="menu">
					  <li><a href="<?php echo base_url( 'client_personal_refferal/export/excel') ;?>" title="Export to Excel" > Export Excel </a></li>
					  <li><a href="<?php echo base_url( 'client_personal_refferal/export/word');?>"  title="Export to Word"> Export Word </a></li>
					  <li><a href="<?php echo base_url( 'client_personal_refferal/export/csv');?>"   title="Export to CSV"> Export CSV</a></li>
				   </ul>

				</div> 			
			<?php endif;?>		
				<a href="<?php echo base_url( 'client_personal_refferal/export/print') ;?>" onclick="ajaxPopupStatic(this.href); return false;" class="tips btn btn-default"  title=" Print ">
				<i class="fa fa-print"></i> </a>	

				<?php
				 if(is_staff_logged_in() && get_staff_user_id()==1) : ?>	
				<a href="<?php echo base_url('cms/module/config/client_personal_refferal') ?>" class="tips btn btn-default"  title="Configuration">
				<i class="fa fa-cog"></i>&nbsp;<?php echo lang('config'); ?></a>
				<?php endif; ?>	
			</div>
		</div>
	</div>


  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->