	<?php if($setting['form-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('client_personal_refferal/pagehead');?>
	</div>
		
			
	
	<div class="sbox">

	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo  $pageNote ;?></small> 

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#client_personal_refferal')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		 <form action="<?php echo base_url('client_personal_refferal/save/'.$row['id']); ?>" class='form-horizontal'  id="client_personal_refferalFormAjax"
		 parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 
		 
<div class="col-md-12">
						<fieldset><legend> client_personal_refferal</legend>
									
								  <div class="form-group  " >
									<label for="Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['id'];?>' name='id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="User Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('User Id', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['user_id'];?>' name='user_id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Sponsorkey" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Sponsorkey', (isset($fields['sponsorkey']['language'])? $fields['sponsorkey']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['sponsorkey'];?>' name='sponsorkey'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Display Name" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Display Name', (isset($fields['display_name']['language'])? $fields['display_name']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['display_name'];?>' name='display_name'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Username" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Username', (isset($fields['username']['language'])? $fields['username']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['username'];?>' name='username'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Firstname" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Firstname', (isset($fields['firstname']['language'])? $fields['firstname']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['firstname'];?>' name='firstname'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Lastname" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Lastname', (isset($fields['lastname']['language'])? $fields['lastname']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['lastname'];?>' name='lastname'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Email" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Email', (isset($fields['email']['language'])? $fields['email']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['email'];?>' name='email'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Phone" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Phone', (isset($fields['phone']['language'])? $fields['phone']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['phone'];?>' name='phone'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Rank Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Rank Id', (isset($fields['rank_id']['language'])? $fields['rank_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['rank_id'];?>' name='rank_id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Rank" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Rank', (isset($fields['rank']['language'])? $fields['rank']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['rank'];?>' name='rank'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Override Rank" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Override Rank', (isset($fields['override_rank']['language'])? $fields['override_rank']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['override_rank'];?>' name='override_rank'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Company" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Company', (isset($fields['company']['language'])? $fields['company']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['company'];?>' name='company'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Address1" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Address1', (isset($fields['address1']['language'])? $fields['address1']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['address1'];?>' name='address1'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Address2" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Address2', (isset($fields['address2']['language'])? $fields['address2']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['address2'];?>' name='address2'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="City" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('City', (isset($fields['city']['language'])? $fields['city']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['city'];?>' name='city'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="State" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('State', (isset($fields['state']['language'])? $fields['state']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['state'];?>' name='state'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Zipcode" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Zipcode', (isset($fields['zipcode']['language'])? $fields['zipcode']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['zipcode'];?>' name='zipcode'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Country" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Country', (isset($fields['country']['language'])? $fields['country']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['country'];?>' name='country'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Shipping Address1" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Shipping Address1', (isset($fields['shipping_address1']['language'])? $fields['shipping_address1']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['shipping_address1'];?>' name='shipping_address1'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Shipping Address2" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Shipping Address2', (isset($fields['shipping_address2']['language'])? $fields['shipping_address2']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['shipping_address2'];?>' name='shipping_address2'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Shipping City" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Shipping City', (isset($fields['shipping_city']['language'])? $fields['shipping_city']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['shipping_city'];?>' name='shipping_city'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Shipping State" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Shipping State', (isset($fields['shipping_state']['language'])? $fields['shipping_state']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['shipping_state'];?>' name='shipping_state'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Shipping Zipcode" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Shipping Zipcode', (isset($fields['shipping_zipcode']['language'])? $fields['shipping_zipcode']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['shipping_zipcode'];?>' name='shipping_zipcode'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Shipping Country" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Shipping Country', (isset($fields['shipping_country']['language'])? $fields['shipping_country']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['shipping_country'];?>' name='shipping_country'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Paid" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Paid', (isset($fields['paid']['language'])? $fields['paid']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['paid'];?>' name='paid'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Unilevel" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Unilevel', (isset($fields['unilevel']['language'])? $fields['unilevel']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['unilevel'];?>' name='unilevel'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Replicated Url" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Replicated Url', (isset($fields['replicated_url']['language'])? $fields['replicated_url']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['replicated_url'];?>' name='replicated_url'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Foundingpositions" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Foundingpositions', (isset($fields['foundingpositions']['language'])? $fields['foundingpositions']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['foundingpositions'];?>' name='foundingpositions'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Hasfoundingpositions" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Hasfoundingpositions', (isset($fields['hasfoundingpositions']['language'])? $fields['hasfoundingpositions']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['hasfoundingpositions'];?>' name='hasfoundingpositions'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Upline Diamond" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Upline Diamond', (isset($fields['upline_diamond']['language'])? $fields['upline_diamond']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['upline_diamond'];?>' name='upline_diamond'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Upline Doublediamond" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Upline Doublediamond', (isset($fields['upline_doublediamond']['language'])? $fields['upline_doublediamond']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['upline_doublediamond'];?>' name='upline_doublediamond'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Upline Triplediamond" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Upline Triplediamond', (isset($fields['upline_triplediamond']['language'])? $fields['upline_triplediamond']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['upline_triplediamond'];?>' name='upline_triplediamond'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Upline Ambassador" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Upline Ambassador', (isset($fields['upline_ambassador']['language'])? $fields['upline_ambassador']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['upline_ambassador'];?>' name='upline_ambassador'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Upline Crown" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Upline Crown', (isset($fields['upline_crown']['language'])? $fields['upline_crown']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['upline_crown'];?>' name='upline_crown'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Date Registered" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Date Registered', (isset($fields['date_registered']['language'])? $fields['date_registered']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['date_registered'];?>' name='date_registered'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Downlines" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Downlines', (isset($fields['downlines']['language'])? $fields['downlines']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['downlines'];?>' name='downlines'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Referralsales" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Referralsales', (isset($fields['referralsales']['language'])? $fields['referralsales']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['referralsales'];?>' name='referralsales'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Override Referralsales" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Override Referralsales', (isset($fields['override_referralsales']['language'])? $fields['override_referralsales']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['override_referralsales'];?>' name='override_referralsales'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Earnings" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Earnings', (isset($fields['earnings']['language'])? $fields['earnings']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['earnings'];?>' name='earnings'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Commissions2" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Commissions2', (isset($fields['commissions2']['language'])? $fields['commissions2']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['commissions2'];?>' name='commissions2'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Commissions Overall" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Commissions Overall', (isset($fields['commissions_overall']['language'])? $fields['commissions_overall']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['commissions_overall'];?>' name='commissions_overall'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Commissions Debits" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Commissions Debits', (isset($fields['commissions_debits']['language'])? $fields['commissions_debits']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['commissions_debits'];?>' name='commissions_debits'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Coins2" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Coins2', (isset($fields['coins2']['language'])? $fields['coins2']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['coins2'];?>' name='coins2'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Coins Overall" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Coins Overall', (isset($fields['coins_overall']['language'])? $fields['coins_overall']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['coins_overall'];?>' name='coins_overall'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Coins Debits" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Coins Debits', (isset($fields['coins_debits']['language'])? $fields['coins_debits']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['coins_debits'];?>' name='coins_debits'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Ewallet" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Ewallet', (isset($fields['ewallet']['language'])? $fields['ewallet']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['ewallet'];?>' name='ewallet'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Withdrawns" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Withdrawns', (isset($fields['withdrawns']['language'])? $fields['withdrawns']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['withdrawns'];?>' name='withdrawns'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Visits" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Visits', (isset($fields['visits']['language'])? $fields['visits']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['visits'];?>' name='visits'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Conversion" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Conversion', (isset($fields['conversion']['language'])? $fields['conversion']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['conversion'];?>' name='conversion'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Firstpurchasedate" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Firstpurchasedate', (isset($fields['firstpurchasedate']['language'])? $fields['firstpurchasedate']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['firstpurchasedate'];?>' name='firstpurchasedate'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Orders" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Orders', (isset($fields['orders']['language'])? $fields['orders']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['orders'];?>' name='orders'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Pending Orders" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Pending Orders', (isset($fields['pending_orders']['language'])? $fields['pending_orders']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['pending_orders'];?>' name='pending_orders'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Unpaid Invoices" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Unpaid Invoices', (isset($fields['unpaid_invoices']['language'])? $fields['unpaid_invoices']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['unpaid_invoices'];?>' name='unpaid_invoices'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Paid Invoices" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Paid Invoices', (isset($fields['paid_invoices']['language'])? $fields['paid_invoices']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['paid_invoices'];?>' name='paid_invoices'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Commissions" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Commissions', (isset($fields['fm_commissions']['language'])? $fields['fm_commissions']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_commissions'];?>' name='fm_commissions'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Commissions Overall" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Commissions Overall', (isset($fields['fm_commissions_overall']['language'])? $fields['fm_commissions_overall']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_commissions_overall'];?>' name='fm_commissions_overall'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Commissions Overall Sfm" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Commissions Overall Sfm', (isset($fields['fm_commissions_overall_sfm']['language'])? $fields['fm_commissions_overall_sfm']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_commissions_overall_sfm'];?>' name='fm_commissions_overall_sfm'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Commissions Reserves" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Commissions Reserves', (isset($fields['fm_commissions_reserves']['language'])? $fields['fm_commissions_reserves']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_commissions_reserves'];?>' name='fm_commissions_reserves'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Withdrawals" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Withdrawals', (isset($fields['fm_withdrawals']['language'])? $fields['fm_withdrawals']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_withdrawals'];?>' name='fm_withdrawals'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Commissions Purchases" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Commissions Purchases', (isset($fields['fm_commissions_purchases']['language'])? $fields['fm_commissions_purchases']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_commissions_purchases'];?>' name='fm_commissions_purchases'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Commissions Debits" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Commissions Debits', (isset($fields['fm_commissions_debits']['language'])? $fields['fm_commissions_debits']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_commissions_debits'];?>' name='fm_commissions_debits'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Coins" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Coins', (isset($fields['fm_coins']['language'])? $fields['fm_coins']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_coins'];?>' name='fm_coins'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Coins Overall" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Coins Overall', (isset($fields['fm_coins_overall']['language'])? $fields['fm_coins_overall']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_coins_overall'];?>' name='fm_coins_overall'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Coins Overall Sfm" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Coins Overall Sfm', (isset($fields['fm_coins_overall_sfm']['language'])? $fields['fm_coins_overall_sfm']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_coins_overall_sfm'];?>' name='fm_coins_overall_sfm'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Coins Reserves" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Coins Reserves', (isset($fields['fm_coins_reserves']['language'])? $fields['fm_coins_reserves']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_coins_reserves'];?>' name='fm_coins_reserves'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Coins Purchases" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Coins Purchases', (isset($fields['fm_coins_purchases']['language'])? $fields['fm_coins_purchases']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_coins_purchases'];?>' name='fm_coins_purchases'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Fm Coins Debits" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Fm Coins Debits', (isset($fields['fm_coins_debits']['language'])? $fields['fm_coins_debits']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['fm_coins_debits'];?>' name='fm_coins_debits'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Pending Commissions" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Pending Commissions', (isset($fields['pending_commissions']['language'])? $fields['pending_commissions']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['pending_commissions'];?>' name='pending_commissions'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Pending Coins" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Pending Coins', (isset($fields['pending_coins']['language'])? $fields['pending_coins']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['pending_coins'];?>' name='pending_coins'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Pending Withdrawals" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Pending Withdrawals', (isset($fields['pending_withdrawals']['language'])? $fields['pending_withdrawals']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['pending_withdrawals'];?>' name='pending_withdrawals'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Pending Transfers" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Pending Transfers', (isset($fields['pending_transfers']['language'])? $fields['pending_transfers']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['pending_transfers'];?>' name='pending_transfers'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Lastdate" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Lastdate', (isset($fields['lastdate']['language'])? $fields['lastdate']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['lastdate'];?>' name='lastdate'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Deleted" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Deleted', (isset($fields['deleted']['language'])? $fields['deleted']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['deleted'];?>' name='deleted'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Uid" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Uid', (isset($fields['uid']['language'])? $fields['uid']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['uid'];?>' name='uid'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Start Data" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Start Data', (isset($fields['start_data']['language'])? $fields['start_data']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['start_data'];?>' name='start_data'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Open Tickets" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Open Tickets', (isset($fields['open_tickets']['language'])? $fields['open_tickets']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['open_tickets'];?>' name='open_tickets'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Closed Tickets" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Closed Tickets', (isset($fields['closed_tickets']['language'])? $fields['closed_tickets']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['closed_tickets'];?>' name='closed_tickets'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Status" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['status'];?>' name='status'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="User Type" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('User Type', (isset($fields['user_type']['language'])? $fields['user_type']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['user_type'];?>' name='user_type'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Is Admin" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Is Admin', (isset($fields['is_admin']['language'])? $fields['is_admin']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['is_admin'];?>' name='is_admin'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Avatar" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Avatar', (isset($fields['avatar']['language'])? $fields['avatar']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['avatar'];?>' name='avatar'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Role Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Role Id', (isset($fields['role_id']['language'])? $fields['role_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['role_id'];?>' name='role_id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Message Checked At" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Message Checked At', (isset($fields['message_checked_at']['language'])? $fields['message_checked_at']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['message_checked_at'];?>' name='message_checked_at'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Notification Checked At" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Notification Checked At', (isset($fields['notification_checked_at']['language'])? $fields['notification_checked_at']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['notification_checked_at'];?>' name='notification_checked_at'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Active" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Active', (isset($fields['active']['language'])? $fields['active']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['active'];?>' name='active'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Title" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['title'];?>' name='title'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Datecreated" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Datecreated', (isset($fields['datecreated']['language'])? $fields['datecreated']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['datecreated'];?>' name='datecreated'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Password" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Password', (isset($fields['password']['language'])? $fields['password']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['password'];?>' name='password'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="New Pass Key" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('New Pass Key', (isset($fields['new_pass_key']['language'])? $fields['new_pass_key']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['new_pass_key'];?>' name='new_pass_key'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="New Pass Key Requested" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('New Pass Key Requested', (isset($fields['new_pass_key_requested']['language'])? $fields['new_pass_key_requested']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['new_pass_key_requested'];?>' name='new_pass_key_requested'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Last Ip" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Last Ip', (isset($fields['last_ip']['language'])? $fields['last_ip']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['last_ip'];?>' name='last_ip'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Last Login" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Last Login', (isset($fields['last_login']['language'])? $fields['last_login']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['last_login'];?>' name='last_login'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Last Password Change" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Last Password Change', (isset($fields['last_password_change']['language'])? $fields['last_password_change']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['last_password_change'];?>' name='last_password_change'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Profile Image" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Profile Image', (isset($fields['profile_image']['language'])? $fields['profile_image']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['profile_image'];?>' name='profile_image'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Direction" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Direction', (isset($fields['direction']['language'])? $fields['direction']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['direction'];?>' name='direction'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Default Language" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Default Language', (isset($fields['default_language']['language'])? $fields['default_language']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['default_language'];?>' name='default_language'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Commissions" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Commissions', (isset($fields['commissions']['language'])? $fields['commissions']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['commissions'];?>' name='commissions'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Coins" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Coins', (isset($fields['coins']['language'])? $fields['coins']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['coins'];?>' name='coins'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Real Firstname" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Real Firstname', (isset($fields['real_firstname']['language'])? $fields['real_firstname']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['real_firstname'];?>' name='real_firstname'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Real Lastname" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Real Lastname', (isset($fields['real_lastname']['language'])? $fields['real_lastname']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['real_lastname'];?>' name='real_lastname'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Soap Sale Count" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Soap Sale Count', (isset($fields['soap_sale_count']['language'])? $fields['soap_sale_count']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['soap_sale_count'];?>' name='soap_sale_count'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Soap Sale Start Date" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Soap Sale Start Date', (isset($fields['soap_sale_start_date']['language'])? $fields['soap_sale_start_date']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['soap_sale_start_date'];?>' name='soap_sale_start_date'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Has Soap Sale" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Has Soap Sale', (isset($fields['has_soap_sale']['language'])? $fields['has_soap_sale']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['has_soap_sale'];?>' name='has_soap_sale'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> </fieldset>
			</div>
			
			
		
			<div style="clear:both"></div>	
				
 		<div class="toolbar-line text-center">		
			<input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo lang('Submit'); ?>" />
			<a href="javascript:void(0)" class="btn-sm btn btn-warning" data-dismiss="modal" aria-hidden="true" onclick="ajaxViewClose('#client_personal_refferal')"><?php echo lang('Cancel'); ?></a>
 		</div>
			  		
		</form>

	</div>	
		<?php foreach($subgrid as $md) : ?>
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid"></div>
			</div>	
		</div>
		<?php endforeach;?>	
	
	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo base_url($md['module'].'/detailview/form?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid' ).html( data );
	});		
<?php endforeach ?>
});
</script>				 
<script type="text/javascript">
$(document).ready(function() { 
	 
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('.markItUp').markItUp(mySettings );	

	var form = $('#client_personal_refferalFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});	
 	 
});

function showRequest()
{
	$('.formLoading').show();	
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		if(data.action =='submit')
		{
			ajaxViewClose('#client_personal_refferal');	
		}	
		
		ajaxFilter('#client_personal_refferal','client_personal_refferal/data');
		notyMessage(data.message);		
	} else {	
		var n = noty({				
			text: data.message,
			type: 'error',
			layout: 'topRight',
		});	
		return false;
	}	
}	
</script>		 


  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->