<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
		</div>
		<ul class="breadcrumb">
			<li><a href="<?php echo ci_site_url('dashboard') ?>">Dashboard</a></li>
			<li><a href="<?php echo ci_site_url('blogadmin') ?>"><?php echo $pageTitle ?></a></li>
			<li class="active"> Detail </li>
		</ul>
	</div>  
	
 	<div class="page-content-wrapper m-t">   
	
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover table-checkable table-responsive datatable dataTable table-bordered" >
				<tbody>	
			
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('CatID', (isset($fields['CatID']['language'])? $fields['CatID']['language'] : array())) ;?>
						</td>
						<td><?php echo SiteHelpers::gridDisplayView($row['CatID'],'CatID','1:tb_blogcategories:CatID:name') ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['title'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Slug', (isset($fields['slug']['language'])? $fields['slug']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['slug'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Content', (isset($fields['content']['language'])? $fields['content']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['content'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Created', (isset($fields['created']['language'])? $fields['created']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['created'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Tags', (isset($fields['tags']['language'])? $fields['tags']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['tags'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['status'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Entryby', (isset($fields['entryby']['language'])? $fields['entryby']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['entryby'] ;?> </td>
						
					</tr>
				
				</tbody>	
			</table>    
		</div>
	</div>
	
</div>
	  