<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bannersmodel extends SB_Model
{

	public $table = 'sfm_banner';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
	}

	function save_custom_banner(){
			$memberID=$this->input->post('memberID');
			$bannertemp = $_FILES['bannerIdName']['tmp_name'];
			$bannername = $_FILES['bannerIdName']['name'];
			$bannertext = pathinfo($bannername,PATHINFO_EXTENSION);
			$bannerinfo = getimagesize($bannertemp);
			$banner_width = $bannerinfo[0];
			$banner_height = $bannerinfo[1];
			$banner_prefix = round(microtime(true));
			//echo 'H'.$banner_width.'W'.$banner_height;
			$bannerpath = "../wp-content/uploads/Banners/CustomBanners/".$banner_prefix.$memberID.$bannername;
			move_uploaded_file($bannertemp, $bannerpath);
			$newBannerName = $banner_prefix.$memberID.$bannername;
			$new_banner_insert_data = array(
				'id'=>$memberID,
				'banner_url'=>$bannerpath,
				'banner_width'=>$banner_width,
				'banner_height'=>$banner_height,
				'banner_name'=>$newBannerName,
				'verified'=>'0'
				);
			$this->db->set($new_banner_insert_data);
			$insert = $this->db->insert('sfm_banner',$new_banner_insert_data);

			redirect('banners');

			//return $insert;
	}
	function fetch_custom_banners(){
		$query = $this->db->get('sfm_banner');
		return $query;
	}
	public static function querySelect(  ){


		return "   SELECT sfm_banner.* FROM sfm_banner   ";
	}
	public static function queryWhere(  ){
		$user_id = ci_get_current_user_id();
		$verified = "1";
		return "  WHERE sfm_banner.id='".$user_id."' AND sfm_banner.verified=".$verified."";
	}

	public static function queryGroup(){
		return "   ";
	}

	public function getAdditionalCriteria($param){
		return "";
		//return " AND sfm_uap_transactions.user_id = '".$param."'" ;
	}

	public function set_defaults($data)
	{
		return $data;
	}

}

?>
