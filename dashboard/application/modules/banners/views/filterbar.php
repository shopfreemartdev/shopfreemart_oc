 			<div id="bannersFilter" class="filterbar row">
			
				<!-- Filter Heading Row -->
				<div class="panel-heading">

					<div class="row">
						<div class="col-md-12">
							<form class="navbar-form" role="search">
								<div class="input-group add-on">
									<input type="text" id="search" name="search" class="form-control" value="<?php echo $filter_search; ?>" placeholder="<?php echo lang('text_filter_search'); ?>" />
									<div class="input-group-btn">
											<a style="margin-right: 10px;" class="btn btn-default" onclick="dosearchbutton_click();" title="<?php echo lang('text_search'); ?>"><i class="fa fa-search"></i></a>
											<a class="btn-filter btn btn-default" onclick="dofilterbutton_click();">
											<i class="fa fa-filter">
											</i>
											</a>
									</div>								
								</div>
							</form>
						</div>
					</div>
				</div>


				<!-- Filter Form Row -->
				<div id="panel-filter" class="panel-filter panel-body">
					<form role="form" id="filter-form" accept-charset="utf-8" method="GET" action="<?php echo base_url(); ?>">
						<div class="filter-bar">
							<div class="form-inline">
								<div class="row">
									<div class="col-md-12">
									
									<?php foreach ($tableForm as $t):
											if($t['search'] =='1') : ?>
												<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>
											<?php endif;
									endforeach; ?>		
																	
										<a class="btn btn-default" onclick="dofilterList();" title="<?php echo lang('text_filter'); ?>">
											<i class="fa fa-filter">
											</i>
										</a>&nbsp;
										<a class="btn btn-default" href="<?php echo base_url(); ?>banners" title="<?php echo lang('text_clear'); ?>">
											<i class="fa fa-times">
											</i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>


<script>

jQuery(function(){
	
		$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
		$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 	
		

});
</script>	



