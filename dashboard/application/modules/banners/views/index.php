<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="page-content-wrapper m-t">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="bannersView"></div>			
		<div id="bannersGrid"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var displaybannersFilterPanel = Cookies.set('ti_displaybannersFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displaybannersFilterPanel = 'false';
		} else
		{
			displaybannersFilterPanel = 'true';
			$( '#bannersFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displaybannersFilterPanel', displaybannersFilterPanel);

		reloadData( '#banners',"<?php echo base_url();?>banners/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displaybannersFilterPanel);
			if (displaybannersFilterPanel == null)
			{
				displaybannersFilterPanel = 'true';	
				Cookies.set('displaybannersFilterPanel', displaybannersFilterPanel);
			}
			console.log(displaybannersFilterPanel);				
			if (displaybannersFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#banners','<?php echo base_url();?>banners/data');	

		});
		
	
</script>
