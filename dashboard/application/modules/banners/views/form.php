	<?php if($setting['form-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('banners/pagehead');?>
	</div>
		
			
	
	<div class="sbox">

	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo  $pageNote ;?></small> 

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#banners')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		 <form action="<?php echo ci_site_url('banners/save/'.$row['id']); ?>" class='form-horizontal'  id="bannersFormAjax"
		 parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 
		 
<div class="col-md-12">
						<fieldset><legend> Banners</legend>
									
								  <div class="form-group  " >
									<label for="Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['id'];?>' name='id'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Banner Url" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Banner Url', (isset($fields['banner_url']['language'])? $fields['banner_url']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['banner_url'];?>' name='banner_url'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Banner Width" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Banner Width', (isset($fields['banner_width']['language'])? $fields['banner_width']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['banner_width'];?>' name='banner_width'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Banner Height" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Banner Height', (isset($fields['banner_height']['language'])? $fields['banner_height']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['banner_height'];?>' name='banner_height'   />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Banner Name" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Banner Name', (isset($fields['banner_name']['language'])? $fields['banner_name']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['banner_name'];?>' name='banner_name'   />
									  <i> <small></small></i>
									 </div> 
								  </div> </fieldset>
			</div>
			
			
		
			<div style="clear:both"></div>	
				
 		<div class="toolbar-line text-center">		
			<input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.sb_submit'); ?>" />
			<a href="javascript:void(0)" class="btn-sm btn btn-warning" onclick="ajaxViewClose('#banners')"><?php echo $this->lang->line('core.sb_cancel'); ?></a>
 		</div>
			  		
		</form>

	</div>	
		<?php foreach($subgrid as $md) : ?>
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid"></div>
			</div>	
		</div>
		<?php endforeach;?>	
	
	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo ci_site_url($md['module'].'/detailview/form?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid' ).html( data );
	});		
<?php endforeach ?>
});
</script>				 
<script type="text/javascript">
$(document).ready(function() { 
	 
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('.markItUp').markItUp(mySettings );	

	var form = $('#bannersFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});	
 	 
});

function showRequest()
{
	$('.formLoading').show();	
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		if(data.action =='submit')
		{
			ajaxViewClose('#banners');	
		}	
		
		ajaxFilter('#banners','banners/data');
		notyMessage(data.message);		
	} else {	
		var n = noty({				
			text: data.message,
			type: 'error',
			layout: 'topRight',
		});	
		return false;
	}	
}	
</script>		 