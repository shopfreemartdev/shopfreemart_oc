

<div class="page-content row" style="background-color: white;">

    <!-- Page header -->

    <div class="page-header">

      <div class="page-title">

        <h3 style="margin-left: 20px;"> <?php echo $pageTitle.'s'; ?> <small><?php //echo $pageNote ?></small></h3>

      </div>

    </div>





<div class="row" style="margin-bottom: 20px;">

      <div id="blog">

        <?php

            if(!isset($_GET['page'])){

              $page = 1;

            }

            else

            {

              $page = $_GET['page'];

            }

            $results_per_page = 3;

            $this_page_first_result = ($page-1)*3;

            

            $query = $this->db->get('tb_blogs');

            $num_rows = $query->num_rows();

           // $query = $this->db->get('tb_blogs',$results_per_page,$this_page_first_result);

            $total_pages = ceil($num_rows/$results_per_page);

            //$data = $query->result_array();



            $this->db->select('*');

            $this->db->from('tb_blogs');

            $this->db->order_by('created','desc');

            $this->db->limit($results_per_page,$this_page_first_result);

            $query=$this->db->get();

            $data = $query->result_array();

            if($query->num_rows()>0){

                foreach($data as $newRow){

                  ?>

                  <div class="post col-sm-12">

                    <div class="headline">

                      <h3>

                        <a style="font-weight: normal;" target="_blank" href="blog/read/<?php echo $newRow['slug'];?>">

                          <?php echo $newRow['title'];?>

                        </a>

                      </h3>

                    </div>



                    <!-- INFO -->

                    <div class="info">

                      <small>

                        <?php

                          $this->db->from('tb_users');

                          $this->db->where('id',$newRow['entryby']);

                          //var_dump($query);

                          $author = $this->db->get();

                          $dataauthor = $author->result_array();

                          $authorname = $dataauthor['0']['first_name'].' '.$dataauthor['0']['last_name'];

                          echo date('F j, Y', strtotime( $newRow['created'] )).' - by: '.$authorname;

                        ?>

                      </small>

                    </div>



                



                    <!-- CONTENT-->

                    <div class="content">

                      <?php echo word_limiter(strip_tags( $newRow['content'] ),40)?>

                    </div>



                    <!-- READ MORE-->

                    <div class="readmore">

                      <a class="pull-right btn btn-sm btn-success" href="<?php echo ci_site_url('blog/read/'.$newRow['slug'])?>" >Read more</a>

                    </div>

                  </div>

                  <?php

                }

            }

            else

            {

              ?><h3><?php echo "No Latest News"?></h3><?php

            }

        ?>

        <br/>



        <div class="pagination">

          <ul class="pagination">

            <?php 

              for($page=1; $page<=$total_pages; $page++){

                echo '

                  <li><a href="blog?page='.$page.'">'.$page.'</a></li>

                ';

              }

            ?>

          </ul>

        </div>

      </div>



      <!-- Custom Here-->

    <div class="col-md-3" >

      <?php $this->load->view('blog/sidebar'); ?>        

    </div>

  </div>

</div>



<script>

$(document).ready(function(){



  $('.do-quick-search').click(function(){

    $('#SximoTable').attr('action','<?php echo ci_site_url("blog/multisearch");?>');

    $('#SximoTable').submit();

  });

  

});  

</script>

