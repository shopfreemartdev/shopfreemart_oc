<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Callogsmodel extends SB_Model 
{

	public $table = 'sfm_crm_call_logs';
	public $primaryKey = 'logid';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT sfm_crm_call_logs.* FROM sfm_crm_call_logs   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE sfm_crm_call_logs.logid IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
	public function getAdditionalCriteria($param){
		if (empty($param)) {
			return "";
		}

		return "";
		//return " sfm_uap_transactions.user_id = '".$param."'" ;
	}

	public function set_defaults($data)
	{
		return $data;
	}	

    public static function getTypefilter($filtertype)
    {
        if (!empty($filtertype)) {
            return ""; // AND st.description LIKE '$filtertype%' ";
        } else {
            return "";
        }

    }	
	public static function queryCountSelect(  ){
		return "  {sql_countselect}  ";
	}

// START OF CUSTOM CODE// END OF CUSTOM CODE

}
?>
