
<div class="page-header">  <!-- Page header -->
                <div class="container-fluid" style="display:none">
                    <div class="row">
							<div id="breadcrumb">
								<?php echo $breadcrumbs; ?>
							</div>                        
					</div>                
				</div>   
				             
                <div class="row">
                        <div class="col-md-7">
                      	    
                            <h3 class="h3 page-title"><i class="icon-database text-danger"></i>  <?php echo $pageTitle; ?></h3>
                            <div class="text-muted page-desc"> <?php echo $pageNote; ?></div>
                        </div>
                        <div class="col-md-5">
                            <?php if($setting['gridsettings']['gs_toolbar']) : ?>
                            <?php perfex_do_action('callogs_render_table_toolbar'); ?>
                            <?php endif;?>
                        </div>                        
                </div>
</div>
            

  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->