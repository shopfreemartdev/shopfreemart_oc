	<?php if($setting['view-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('callogs/pagehead');?>
	</div>
		
	
	<div class="sbox">
	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo $pageNote ;?></small>

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#<?php echo  $pageModule ;?>')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		
			<table class="table table-striped table-bordered table-hover table-checkable table-responsive datatable dataTable table-clients no-footer dtr-inline table-bordered" >
				<tbody>	

					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Log ID', (isset($fields['logid']['language'])? $fields['logid']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['logid'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Userid', (isset($fields['userid']['language'])? $fields['userid']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['userid'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Email', (isset($fields['email']['language'])? $fields['email']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['email'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Phone', (isset($fields['phonenumber']['language'])? $fields['phonenumber']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['phonenumber'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Concern', (isset($fields['concern']['language'])? $fields['concern']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['concern'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Notes', (isset($fields['notes']['language'])? $fields['notes']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['notes'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Date Created', (isset($fields['datecreated']['language'])? $fields['datecreated']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['datecreated'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['status'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Staff', (isset($fields['staffid']['language'])? $fields['staffid']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['staffid'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Survey', (isset($fields['surveysend']['language'])? $fields['surveysend']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['surveysend'] ;?> </td>
						
					</tr>
										
				</tbody>	
			</table>    

		<?php foreach($subgrid as $md) : ?>
		<hr />
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid-<?php echo $id;?>"></div>
			</div>	
		</div>
		<hr />
		<?php endforeach;?>	

	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo base_url($md['module'].'/detailview?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid-<?php echo $id;?>' ).html( data );
	});		
<?php endforeach ?>
});
</script>		  


  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->