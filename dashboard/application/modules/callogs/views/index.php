<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="page-content-wrapper m-t">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="callogsView"></div>			
		<div id="callogsGrid" style="min-height: 500px;"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var displaycallogsFilterPanel = Cookies.set('ti_displaycallogsFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displaycallogsFilterPanel = 'false';
		} else
		{
			displaycallogsFilterPanel = 'true';
			$( '#callogsFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displaycallogsFilterPanel', displaycallogsFilterPanel);

		reloadData( '#callogs',"<?php echo base_url();?>callogs/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displaycallogsFilterPanel);
			if (displaycallogsFilterPanel == null)
			{
				displaycallogsFilterPanel = 'true';	
				Cookies.set('displaycallogsFilterPanel', displaycallogsFilterPanel);
			}
			console.log(displaycallogsFilterPanel);				
			if (displaycallogsFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#callogs','<?php echo base_url();?>callogs/data');	

		});
		
	
</script>

  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->