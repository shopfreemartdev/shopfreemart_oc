<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');?>

<?php
if($setting['gridsettings']['gs_portlet']) : ?>
<div class="sbox">

	<div class="sbox-title">
		<h4>
			<i class="fa fa-table" style="font-size: 18px">
			</i><?php echo $pageTitle ;?>
			<small>
				<?php echo  $pageNote ;?>
			</small>

			<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#{class}s')">
				<i class="fa fa fa-times">
				</i>
			</a>
		</h4>
	</div>

	<div class="sbox-content">
		<?php endif;?>
		<div class="page-content-wrapper m-t">
			<div id="members-account-page" class="span12">
				<div class="row">
					<div class="span9">

						<div id="ajax">
							<?=$show_message?>
						</div>
						<h3>
							<?=$this->lang->line('title_goes_here')?>
						</h3>
						<div class="{class}-form">
							FORM GOES HERE

								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#BillingAddress" data-toggle="tab">
											Billing Address
										</a>
									</li>
									<li class="">
										<a href="#ShippingAddress" data-toggle="tab">
											Shipping Address
										</a>
									</li>
									<li class="">
										<a href="#Profile" data-toggle="tab">
											Profile
										</a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane m-t active" id="BillingAddress">

										<div class="form-group  " >
											<label for="Billing Address 1" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Billing Address 1', (isset($fields['billing_address_1']['language'])? $fields['billing_address_1']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['billing_address_1'];?>' name='billing_address_1'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Billing Address 2" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Billing Address 2', (isset($fields['billing_address_2']['language'])? $fields['billing_address_2']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['billing_address_2'];?>' name='billing_address_2'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Billing City" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Billing City', (isset($fields['billing_city']['language'])? $fields['billing_city']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['billing_city'];?>' name='billing_city'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Billing State" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Billing State', (isset($fields['billing_state']['language'])? $fields['billing_state']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['billing_state'];?>' name='billing_state'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Billing Country" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Billing Country', (isset($fields['billing_country']['language'])? $fields['billing_country']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['billing_country'];?>' name='billing_country'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Billing Postal Code" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Billing Postal Code', (isset($fields['billing_postal_code']['language'])? $fields['billing_postal_code']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['billing_postal_code'];?>' name='billing_postal_code'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Fax" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Fax', (isset($fields['fax']['language'])? $fields['fax']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['fax'];?>' name='fax'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment Preference Amount" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment Preference Amount', (isset($fields['payment_preference_amount']['language'])? $fields['payment_preference_amount']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_preference_amount'];?>' name='payment_preference_amount'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Primary Email" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Primary Email', (isset($fields['primary_email']['language'])? $fields['primary_email']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['primary_email'];?>' name='primary_email'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Paypal Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Paypal Id', (isset($fields['paypal_id']['language'])? $fields['paypal_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['paypal_id'];?>' name='paypal_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Safepay Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Safepay Id', (isset($fields['safepay_id']['language'])? $fields['safepay_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['safepay_id'];?>' name='safepay_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Moneybookers Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Moneybookers Id', (isset($fields['moneybookers_id']['language'])? $fields['moneybookers_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['moneybookers_id'];?>' name='moneybookers_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payza Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payza Id', (isset($fields['payza_id']['language'])? $fields['payza_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payza_id'];?>' name='payza_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Custom Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Custom Id', (isset($fields['custom_id']['language'])? $fields['custom_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['custom_id'];?>' name='custom_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Bank Transfer" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Bank Transfer', (isset($fields['bank_transfer']['language'])? $fields['bank_transfer']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='bank_transfer' rows='2' id='bank_transfer' class='form-control '
				           >
													<?php echo $row['bank_transfer'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Enable Affiliate Marketing" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Enable Affiliate Marketing', (isset($fields['enable_affiliate_marketing']['language'])? $fields['enable_affiliate_marketing']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['enable_affiliate_marketing'];?>' name='enable_affiliate_marketing'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Enable Custom Url" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Enable Custom Url', (isset($fields['enable_custom_url']['language'])? $fields['enable_custom_url']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['enable_custom_url'];?>' name='enable_custom_url'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Custom Url Link" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Custom Url Link', (isset($fields['custom_url_link']['language'])? $fields['custom_url_link']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['custom_url_link'];?>' name='custom_url_link'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Website" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Website', (isset($fields['website']['language'])? $fields['website']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['website'];?>' name='website'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 1" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 1', (isset($fields['jrox_custom_field_1']['language'])? $fields['jrox_custom_field_1']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_1' rows='2' id='jrox_custom_field_1' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_1'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 2" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 2', (isset($fields['jrox_custom_field_2']['language'])? $fields['jrox_custom_field_2']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_2' rows='2' id='jrox_custom_field_2' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_2'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 3" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 3', (isset($fields['jrox_custom_field_3']['language'])? $fields['jrox_custom_field_3']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_3' rows='2' id='jrox_custom_field_3' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_3'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 4" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 4', (isset($fields['jrox_custom_field_4']['language'])? $fields['jrox_custom_field_4']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_4' rows='2' id='jrox_custom_field_4' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_4'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 5" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 5', (isset($fields['jrox_custom_field_5']['language'])? $fields['jrox_custom_field_5']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_5' rows='2' id='jrox_custom_field_5' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_5'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 6" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 6', (isset($fields['jrox_custom_field_6']['language'])? $fields['jrox_custom_field_6']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_6' rows='2' id='jrox_custom_field_6' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_6'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 7" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 7', (isset($fields['jrox_custom_field_7']['language'])? $fields['jrox_custom_field_7']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_7' rows='2' id='jrox_custom_field_7' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_7'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 8" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 8', (isset($fields['jrox_custom_field_8']['language'])? $fields['jrox_custom_field_8']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_8' rows='2' id='jrox_custom_field_8' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_8'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 9" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 9', (isset($fields['jrox_custom_field_9']['language'])? $fields['jrox_custom_field_9']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_9' rows='2' id='jrox_custom_field_9' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_9'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Jrox Custom Field 10" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Jrox Custom Field 10', (isset($fields['jrox_custom_field_10']['language'])? $fields['jrox_custom_field_10']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='jrox_custom_field_10' rows='2' id='jrox_custom_field_10' class='form-control '
				           >
													<?php echo $row['jrox_custom_field_10'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Profile Description" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Profile Description', (isset($fields['profile_description']['language'])? $fields['profile_description']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<textarea name='profile_description' rows='2' id='profile_description' class='form-control '
				           >
													<?php echo $row['profile_description'] ;?>
												</textarea>
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Alert Downline Signup" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Alert Downline Signup', (isset($fields['alert_downline_signup']['language'])? $fields['alert_downline_signup']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['alert_downline_signup'];?>' name='alert_downline_signup'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Alert New Commission" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Alert New Commission', (isset($fields['alert_new_commission']['language'])? $fields['alert_new_commission']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['alert_new_commission'];?>' name='alert_new_commission'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Alert Payment Sent" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Alert Payment Sent', (isset($fields['alert_payment_sent']['language'])? $fields['alert_payment_sent']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['alert_payment_sent'];?>' name='alert_payment_sent'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Allow Downline View" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Allow Downline View', (isset($fields['allow_downline_view']['language'])? $fields['allow_downline_view']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['allow_downline_view'];?>' name='allow_downline_view'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Allow Downline Email" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Allow Downline Email', (isset($fields['allow_downline_email']['language'])? $fields['allow_downline_email']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['allow_downline_email'];?>' name='allow_downline_email'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Last Login Date" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Last Login Date', (isset($fields['last_login_date']['language'])? $fields['last_login_date']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['last_login_date'];?>' name='last_login_date'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Last Login Ip" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Last Login Ip', (isset($fields['last_login_ip']['language'])? $fields['last_login_ip']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['last_login_ip'];?>' name='last_login_ip'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Signup Date" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Signup Date', (isset($fields['signup_date']['language'])? $fields['signup_date']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['signup_date'];?>' name='signup_date'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Updated On" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Updated On', (isset($fields['updated_on']['language'])? $fields['updated_on']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['updated_on'];?>' name='updated_on'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Updated By" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Updated By', (isset($fields['updated_by']['language'])? $fields['updated_by']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['updated_by'];?>' name='updated_by'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Login Status" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Login Status', (isset($fields['login_status']['language'])? $fields['login_status']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['login_status'];?>' name='login_status'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Confirm Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Confirm Id', (isset($fields['confirm_id']['language'])? $fields['confirm_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['confirm_id'];?>' name='confirm_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Initial Fee" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Initial Fee', (isset($fields['initial_fee']['language'])? $fields['initial_fee']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['initial_fee'];?>' name='initial_fee'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Facebook Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Facebook Id', (isset($fields['facebook_id']['language'])? $fields['facebook_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['facebook_id'];?>' name='facebook_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Twitter Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Twitter Id', (isset($fields['twitter_id']['language'])? $fields['twitter_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['twitter_id'];?>' name='twitter_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Facebook Link" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Facebook Link', (isset($fields['facebook_link']['language'])? $fields['facebook_link']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['facebook_link'];?>' name='facebook_link'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Google Plus" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Google Plus', (isset($fields['google_plus']['language'])? $fields['google_plus']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['google_plus'];?>' name='google_plus'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Linkedin Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Linkedin Id', (isset($fields['linkedin_id']['language'])? $fields['linkedin_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['linkedin_id'];?>' name='linkedin_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Activation" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Activation', (isset($fields['activation']['language'])? $fields['activation']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['activation'];?>' name='activation'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
									</div>

									<div class="tab-pane m-t " id="ShippingAddress">

										<div class="form-group  " >
											<label for="Payment Address 1" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment Address 1', (isset($fields['payment_address_1']['language'])? $fields['payment_address_1']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_address_1'];?>' name='payment_address_1'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment Address 2" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment Address 2', (isset($fields['payment_address_2']['language'])? $fields['payment_address_2']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_address_2'];?>' name='payment_address_2'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment City" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment City', (isset($fields['payment_city']['language'])? $fields['payment_city']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_city'];?>' name='payment_city'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment State" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment State', (isset($fields['payment_state']['language'])? $fields['payment_state']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_state'];?>' name='payment_state'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment Country" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment Country', (isset($fields['payment_country']['language'])? $fields['payment_country']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_country'];?>' name='payment_country'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment Postal Code" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment Postal Code', (isset($fields['payment_postal_code']['language'])? $fields['payment_postal_code']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_postal_code'];?>' name='payment_postal_code'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Work Phone" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Work Phone', (isset($fields['work_phone']['language'])? $fields['work_phone']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['work_phone'];?>' name='work_phone'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Mobile Phone" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Mobile Phone', (isset($fields['mobile_phone']['language'])? $fields['mobile_phone']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['mobile_phone'];?>' name='mobile_phone'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Home Phone" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Home Phone', (isset($fields['home_phone']['language'])? $fields['home_phone']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['home_phone'];?>' name='home_phone'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Payment Name" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Payment Name', (isset($fields['payment_name']['language'])? $fields['payment_name']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['payment_name'];?>' name='payment_name'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
									</div>

									<div class="tab-pane m-t " id="Profile">

										<div class="form-group  " >
											<label for="User Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('User Id', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['user_id'];?>' name='user_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Company" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Company', (isset($fields['company']['language'])? $fields['company']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['company'];?>' name='company'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Display Name" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Display Name', (isset($fields['display_name']['language'])? $fields['display_name']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['display_name'];?>' name='display_name'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Password" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Password', (isset($fields['password']['language'])? $fields['password']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['password'];?>' name='password'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Username" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Username', (isset($fields['username']['language'])? $fields['username']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['username'];?>' name='username'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Email" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Email', (isset($fields['email']['language'])? $fields['email']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['email'];?>' name='email'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Lname" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Lname', (isset($fields['lname']['language'])? $fields['lname']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['lname'];?>' name='lname'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Status" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['status'];?>' name='status'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Fname" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Fname', (isset($fields['fname']['language'])? $fields['fname']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['fname'];?>' name='fname'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Rank" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Rank', (isset($fields['rank']['language'])? $fields['rank']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['rank'];?>' name='rank'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Sponsor Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Sponsor Id', (isset($fields['sponsor_id']['language'])? $fields['sponsor_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['sponsor_id'];?>' name='sponsor_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
										<div class="form-group  " >
											<label for="Member Id" class=" control-label col-md-4 text-left">
												<?php echo SiteHelpers::activeLang('Member Id', (isset($fields['member_id']['language'])? $fields['member_id']['language'] : array()))  ;?>
											</label>
											<div class="col-md-8">
												<input type='text' class='form-control' placeholder='' value='<?php echo $row['member_id'];?>' name='member_id'   />
												<i>
													<small>
													</small>
												</i>
											</div>
										</div>
									</div>
								</div>


								<div style="clear:both">
								</div>

								<div class="toolbar-line text-center">
									<input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo $this->lang->line('core.sb_submit'); ?>" />
									<a href="javascript:void(0)" class="btn-sm btn btn-warning" onclick="ajaxViewClose('#accounts')">
										<?php echo $this->lang->line('core.sb_cancel'); ?>
									</a>
								</div>

							</form>
						</div>

					</div>
					<div class="span3 capitalize">
						<?php
						if($show_upload == 1): ?>
						<form action="<?=base_url('members')?>account/update_photo" method="post" enctype="multipart/form-data">
							<div class="member-photo">
								<div class="well text-center">
									<h3 class="text-left">
										<?=$this->lang->line('member_photo')?>
									</h3>
									<p>
										<img src="<?=base_url('members', true)?><?=$member_photo?>" alt=""/>
									</p>
									<p>
										<input type="file" name="userfile" size="15"/>
									</p>
									<p>
										<?php
										if(!empty($show_delete_photo)): ?>
										<a href="<?=base_url('members')?>account/delete_photo/<?=$member_photo_raw?>/<?=$this->session->userdata('userid')?>" class="btn btn-block btn-warning">
											<?=$this->lang->line('delete_photo')?>
										</a>
										<?php endif; ?>
									</p>
									<p>
										<button type="submit" class="btn btn-primary btn-block">
											<?=$this->lang->line('update_photo')?>
										</button>
									</p>
								</div>
							</div>
						</form>
						<?php endif; ?>

						<div class="quick-stats hidden-phone">
							<div class="well">
								<h3>
									<?=$this->lang->line('dashboard_quickstats')?>
								</h3>
								<div class="pull-right">
									<?=$dashboard_unpaid_invoices?>
								</div>
								<div>
									<a href="<?=base_url('members')?>/invoices/view/unpaid">
										<?=$this->lang->line('dashboard_unpaid_invoices')?>
									</a>
								</div>
								<div class="pull-right">
									<?=$dashboard_paid_invoices?>
								</div>
								<div>
									<a href="<?=base_url('members')?>/invoices/view/paid">
										<?=$this->lang->line('dashboard_paid_invoices')?>
									</a>
								</div>
								<hr />
								<p>
									<a href="<?=base_url('members')?>invoices/view" class="btn btn-primary btn-block">
										<?=$this->lang->line('view_invoices')?>
									</a>
								</p>
								</p>
								<?php
								if($enable_affiliate_marketing == 1): ?>
								<hr />
								<div class="pull-right">
									<?=$dashboard_unpaid_commissions?>
								</div>
								<div>
									<a href="<?=base_url('members')?>/commissions/view/unpaid">
										<?=$this->lang->line('dashboard_unpaid_commissions')?>
									</a>
								</div>
								<div class="pull-right">
									<?=$dashboard_paid_commissions?>
								</div>
								<div>
									<a href="<?=base_url('members')?>/commissions/view/paid">
										<?=$this->lang->line('dashboard_paid_commissions')?>
									</a>
								</div>
								<hr />
								<p>
									<a href="<?=base_url('members')?>commissions/view" class="btn btn-primary btn-block">
										<?=$this->lang->line('view_commissions')?>
									</a>
								</p>
								</p>
								<?php endif; ?>
							</div>
						</div>

						<?php
						if($sts_support_enable == 1): ?>
						<div class="support-tickets hidden-phone">
							<div class="well">
								<h3>
									<?=$this->lang->line('dashboard_support_tickets')?>
								</h3>
								<div class="pull-right">
									<?=$dashboard_open_tickets?>
								</div>
								<div>
									<a href="<?=base_url('members')?>/support/view/open">
										<?=$this->lang->line('dashboard_open_tickets')?>
									</a>
								</div>
								<div class="pull-right">
									<?=$dashboard_closed_tickets?>
								</div>
								<div>
									<a href="<?=base_url('members')?>/support/view/closed">
										<?=$this->lang->line('dashboard_closed_tickets')?>
									</a>
								</div>
								<hr />
								<p>
									<a href="<?=base_url('members')?>support/add" class="btn btn-primary btn-block">
										<?=$this->lang->line('create_support_ticket')?>
									</a>
								</p>
								</p>
							</div>
						</div>
						<?php endif; ?>
						<?php
						if($member_hide_list_box == 0): ?>
						<div class="mailing-lists hidden-phone">
							<div class="well">
								<h3>
									<?=$this->lang->line('dashboard_mailing_lists')?>
								</h3>
								<form action="<?=base_url('members')?>mailing_lists/update/account" method="post">
									<?php
									foreach($mailing_lists as $m): ?>
									<label class="checkbox">
										<?=$m['mailing_list_check_box']?> <?=$m['mailing_list_name']?>
									</label>
									<?php endforeach; ?>
									<p>
										<hr />
										<button type="submit" class="btn btn-primary btn-block">
											<?=$this->lang->line('update_subscriptions')?>
										</button>
									</p>
								</form>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php
	foreach($subgrid as $md) : ?>
	<div  id="<?php echo  $md['module'] ;?>">
		<h4>
			<i class="fa fa-table">
			</i><?php echo  $md['title'] ;?>
		</h4>
		<div id="<?php echo  $md['module'] ;?>View">
		</div>
		<div class="table-responsive">
			<div id="<?php echo  $md['module'] ;?>Grid">
			</div>
		</div>
	</div>
	<?php endforeach;?>

	<?php
	if($setting['gridsettings']['gs_portlet']) : ?>
</div>
</div>
<?php endif;?>



<script language="javascript" src="<?=base_url('', true)?>js/jquery.validate.js">
</script>

<script>
	$("#account-form").validate();

	function onSelectChange(id, select)
	{
		var index;
		for(index=0; index<select.options.length; index++)
		if(select.options[index].selected)
		{
			if(select.options[index].value!="")
			cid = select.options[index].value;
			$("."+id+"-field-"+id+"_state").load("<?=base_url('members')?>account/load_regions/" + cid + "/"+id+"_state");
		}
	}
</script>

<script>
	$(document).ready(function()
		{
			<?php foreach($subgrid as $md) : ?>
			$.post( '<?php echo ci_site_url($md['module'].'/detailview/form?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data )
				{
					$( '#<?php echo $md['module'] ;?>Grid' ).html( data );
				});
			<?php endforeach ?>
		});
</script>
<script type="text/javascript">
	$(document).ready(function()
		{

			$('.previewImage').fancybox();
			$('.tips').tooltip();
			$(".select2").select2({ width:"98%"});
			$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
			$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'});
			$('.markItUp').markItUp(mySettings );

			var form = $('#accountsFormAjax');
			form.parsley();
			form.submit(function()
				{

					if(form.parsley('isValid') == true)
					{
						var options =
						{
							dataType:      'json',
							beforeSubmit :  showRequest,
							success:       showResponse
						}
						$(this).ajaxSubmit(options);
						return false;

					} else
					{
						return false;
					}

				});

		});

	function showRequest()
	{
		$('.formLoading').show();
	}
	function showResponse(data)
	{

		if(data.status == 'success')
		{
			if(data.action =='submit')
			{
				ajaxViewClose('#accounts');
			}

			ajaxFilter('#accounts','accounts/data');
			notyMessage(data.message);
		} else
		{
			var n = noty(
				{
					text: data.message,
					type: 'error',
					layout: 'topRight',
				});
			return false;
		}
	}
</script>
