<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class {controller}model extends SB_Model 
{

	public $table = '{table}';
	public $primaryKey = '{key}';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  {sql_select}  ";
	}
	public static function queryWhere(  ){
		
		return " {sql_where}   ";
	}
	
	public static function queryGroup(){
		return " {sql_group}  ";
	}
	
	public static function getAdditionalCriteria($param){
		return "";
		//return " AND sfm_uap_transactions.user_id = '".$param."'" ;
	}

	public function set_defaults($data)
	{
		return $data;
	}	
	
}

?>
