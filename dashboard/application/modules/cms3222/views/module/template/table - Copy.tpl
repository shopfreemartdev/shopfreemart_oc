<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');?>

        <div id="ajax"><?=$show_message?></div>

<?php usort($tableGrid, "SiteHelpers::_sort"); ?>

<?php if($setting['gridsettings']['gs_toolbar']) : ?>
<?php $this->load->view('{class}/toolbar');?>
<?php endif;?>

<?php if($setting['gridsettings']['gs_portlet']) : ?>
<div class="sbox">
	<div class="sbox-title">
		<h4 class="pull-left">
		<?php echo AjaxHelpers::buttonBacktoDashboard($pageModule); ?>
			<?php echo $pageTitle ;?>
			<small>
				<?php echo $pageNote ;?>
			</small>
		</h4>
		
		<div class="pull-right">
			<div class="btn-group">
				<?php if($this->access['is_add'] ==1) :
				echo AjaxHelpers::buttonActionCreate($pageModule,$setting);
				endif;

				if($this->access['is_remove'] ==1) : ?>
				<a href="javascript:void(0);"  onclick="ajaxRemove('#{class}','{class}');" class="tips btn btn-sm btn-danger" title="Remove">
					<i class="fa fa-trash-o">
					</i>
				</a>
				<?php endif; ?>
				<!-- <a href="{class}/flysearch" onclick="SximoModal(this.href,'Advance Search'); return false;" class="tips btn  btn-info"  title=" Search ">
				<i class="fa fa-search"></i> Search </a> -->
			</div>

			<div class="btn-group">
				<?php  if($access['is_excel'] ==1) :?>
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-sm dropdown-toggle tips"  title=" Download "
					  data-toggle="dropdown">
						<i class="fa fa-download">
						</i>
						<span class="caret">
						</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="<?php echo ci_site_url( '{class}/export/excel') ;?>" title="Export to Excel" >
								Export Excel
							</a>
						</li>
						<li>
							<a href="<?php echo ci_site_url( '{class}/export/word');?>"  title="Export to Word">
								Export Word
							</a>
						</li>
						<li>
							<a href="<?php echo ci_site_url( '{class}/export/csv');?>'"   title="Export to CSV">
								Export CSV
							</a>
						</li>
					</ul>

				</div>
				<?php endif;?>
				<a href="<?php echo ci_site_url( '{class}/export/print') ;?>" onclick="ajaxPopupStatic(this.href); return false;" class="tips btn btn-sm btn-info"  title=" Print ">
					<i class="fa fa-print">
					</i>
				</a>

				<?php
				if($this->session->userdata('gid') ==1) : ?>
				<a href="<?php echo ci_site_url('cms/module/config/{class}') ?>" class="tips btn btn-sm btn-default"  title="Configuration">
					<i class="fa fa-cog">
					</i>
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="sbox-content">
		<?php endif;?>

		<?php if(empty($rowData)) : ?>
	        <div class="alert alert-warning">
	            <h3><?=$this->lang->line('no_records_found')?></h3>
	            <p>
					<div class="btn-group">
						<?php 
						if($this->access['is_add'] ==1) :
							echo AjaxHelpers::buttonActionCreate($pageModule,$setting);
						endif; 
						echo AjaxHelpers::buttonBacktoDashboard($pageModule); 
						?>
					</div>
	            </p>
	        </div>
		<?php else: ?>        
			<div class="panel panel-default panel-table">
		
			<?php if($setting['gridsettings']['gs_filterbar']) : ?>
			<!-- Filter Heading Row -->
			<div class="panel-heading">
				<div class="row">
					<div class="col-md-9">
						<form class="navbar-form" role="search">
							<div class="input-group add-on">
								<input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
								<div class="input-group-btn">
									<button class="btn btn-default" type="submit">
										<i class="glyphicon glyphicon-search">
										</i>
									</button>
								</div>
							</div>
						</form>
					</div>

					<div class="col-md-3 align-right">
						<button class="btn btn-filter btn-default">
							<i class="fa fa-filter">
							</i>
						</button>

						<a href="javascript:void(0)" class="btn btn-filter btn-default"
						onclick="reloadData('#{class}','{class}/data')"  title="Reload Data">
							<i class="fa fa-refresh">
							</i>
						</a>
					</div>
				</div>
			</div>
			<!-- Filter Form Row -->
			<div class="panel-body panel-filter">
				<form role="form" id="filter-form" accept-charset="utf-8" method="GET" action="<?php echo base_url(); ?>">
					<div class="filter-bar">
						<div class="form-inline">
							<div class="row">
								<div class="col-md-12">
									<?php
									if(!$user_strict_location){
									?>
									<div class="form-group">
										<select name="filter_location" class="form-control input-sm" class="form-control input-sm">
											<option value="">
												<?php echo lang('text_filter_location'); ?>
											</option>
											<?php
											foreach($filter_locations as $location){
											?>
											<?php
											if($location['location_id'] === $filter_location){
											?>
											<option
												value="<?php echo $location['location_id']; ?>" <?php echo set_select('filter_location', $location['location_id'], TRUE); ?> >
												<?php echo $location['location_name']; ?>
											</option>
											<?php
											}
											else
											{
											?>
											<option
												value="<?php echo $location['location_id']; ?>" <?php echo set_select('filter_location', $location['location_id']); ?> >
												<?php echo $location['location_name']; ?>
											</option>
											<?php
											}?>
											<?php
											} ?>
										</select>&nbsp;
									</div>
									<?php
									} ?>
									<div class="form-group">
										<select name="filter_status" class="form-control input-sm">
											<option value="">
												<?php echo lang('text_filter_status'); ?>
											</option>
											<?php
											foreach($filter_statuses as $status){
											?>
											<?php
											if($status['status_id'] === $filter_status){
											?>
											<option
												value="<?php echo $status['status_id']; ?>" <?php echo set_select('filter_status', $status['status_id'], TRUE); ?> >
												<?php echo $status['status_name']; ?>
											</option>
											<?php
											}
											else
											{
											?>
											<option
												value="<?php echo $status['status_id']; ?>" <?php echo set_select('filter_status', $status['status_id']); ?> >
												<?php echo $status['status_name']; ?>
											</option>
											<?php
											}?>
											<?php
											} ?>
											<option
												value="0" <?php echo ($filter_status === '0') ? 'selected' : ''; ?>>
												<?php echo lang('text_lost_orders'); ?>
											</option>
										</select>&nbsp;
									</div>
									<div class="form-group">
										<select name="filter_type" class="form-control input-sm">
											<option value="">
												<?php echo lang('text_filter_order_type'); ?>
											</option>
											<?php
											if($filter_type === '1'){
											?>
											<option
												value="1" <?php echo set_select('filter_type', '1', TRUE); ?> >
												<?php echo lang('text_delivery'); ?>
											</option>
											<option
												value="2" <?php echo set_select('filter_type', '2'); ?> >
												<?php echo lang('text_collection'); ?>
											</option>
											<?php
											}
											else
											if($filter_type === '2'){
											?>
											<option
												value="1" <?php echo set_select('filter_type', '1'); ?> >
												<?php echo lang('text_delivery'); ?>
											</option>
											<option
												value="2" <?php echo set_select('filter_type', '2', TRUE); ?> >
												<?php echo lang('text_collection'); ?>
											</option>
											<?php
											}
											else
											{
											?>
											<option
												value="1" <?php echo set_select('filter_type', '1'); ?> >
												<?php echo lang('text_delivery'); ?>
											</option>
											<option
												value="2" <?php echo set_select('filter_type', '2'); ?> >
												<?php echo lang('text_collection'); ?>
											</option>
											<?php
											} ?>
										</select>&nbsp;
									</div>
									<div class="form-group">
										<select name="filter_payment" class="form-control input-sm">
											<option value="">
												<?php echo lang('text_filter_payment'); ?>
											</option>
											<?php
											foreach($filter_paymentmethods as $payment){
											?>
											<?php
											if($payment['name'] === $filter_paymentmethod){
											?>
											<option
												value="<?php echo $payment['name']; ?>" <?php echo set_select('filter_payment', $payment['name'], TRUE); ?> >
												<?php echo $payment['title']; ?>
											</option>
											<?php
											}
											else
											{
											?>
											<option
												value="<?php echo $payment['name']; ?>" <?php echo set_select('filter_payment', $payment['name']); ?> >
												<?php echo $payment['title']; ?>
											</option>
											<?php
											}?>
											<?php
											} ?>
										</select>&nbsp;
									</div>
									<div class="form-group">
										<select name="filter_date" class="form-control input-sm">
											<option value="">
												<?php echo lang('text_filter_date'); ?>
											</option>
											<?php
											foreach($filter_order_dates as $key => $value){
											?>
											<?php
											if($key === $filter_date){
											?>
											<option
												value="<?php echo $key; ?>" <?php echo set_select('filter_date', $key, TRUE); ?> >
												<?php echo $value; ?>
											</option>
											<?php
											}
											else
											{
											?>
											<option
												value="<?php echo $key; ?>" <?php echo set_select('filter_date', $key); ?> >
												<?php echo $value; ?>
											</option>
											<?php
											}?>
											<?php
											} ?>
										</select>
									</div>
									<a class="btn btn-grey" onclick="filterList();" title="<?php echo lang('text_filter'); ?>">
										<i class="fa fa-filter">
										</i>
									</a>&nbsp;
									<a class="btn btn-grey" href="<?php echo page_url(); ?>" title="<?php echo lang('text_clear'); ?>">
										<i class="fa fa-times">
										</i>
									</a>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<?php endif;?> <!-- gs_filterbar -->
			
			<!-- Grid Table Row -->
			<div class="table-responsive">
				<form
					action='<?php echo ci_site_url('{class}/destroy') ?>' class='form-horizontal' id ='{class}Form' method="post" <?php echo $setting['gridsettings']['gs_css']; ?> >

					<table class="table table-striped " id="{class}Table" >
						<thead>
							<tr>
								<?php if($setting['gridsettings']['gs_counter']) { ?>
								<th>
									No
								</th>
								<?php } ?>

								<?php if($setting['gridsettings']['gs_checkbox']) { ?>
								<th>
									<input type="checkbox" class="checkall" />
								</th>
								<?php } ?>

								<?php if($setting['view-method']=='expand') { ?>
								<th>
								</th> <?php } ?>
								<?php foreach ($tableGrid as $k => $t) : ?>
								<?php if($t['view'] =='1'): ?>
								<th>
									<?php echo SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array()))  ?>
								</th>
								<?php endif; ?>
								<?php endforeach; ?>

								<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
								-
								<th width="70">
									Action
								</th>
								<?php } ?>

							</tr>
						</thead>

						<tbody>
							<?php if($access['is_add'] =='1' && $setting['inline']=='true'): ?>
							<tr id="form-0" >

								<?php if($setting['gridsettings']['gs_counter']) { ?>
								<td>
									#
								</td>
								<?php } ?>

								<?php if($setting['gridsettings']['gs_checkbox']) { ?>
								<th>
								</th>
								<?php } ?>

								<?php if($setting['view-method']=='expand') { ?>
								<td>
								</td> <?php } ?>
								<?php foreach ($tableGrid as $t) :
								if($t['view'] =='1') : ?>
								<td data-form="<?php echo $t['field'];?>" data-form-type="<?php echo  AjaxHelpers::inlineFormType($t['field'],$tableForm);?>">
									<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>
								</td>
								<?php endif;
								endforeach ?>

								<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
								<td style="width:50px;">

									<button type="button"  class=" btn btn-xs btn-info" rel="#<?php echo $pageModule ;?>Form"
					onclick="ajaxInlineSave('#<?php echo $pageModule ;?>','<?php echo ci_site_url('{class}/quicksave') ;?>','<?php echo ci_site_url('{class}/data?md=') ;?>')">
										<i class="fa fa-save">
										</i>
									</button>
								</td>
								<?php } ?>

							</tr>
							<?php endif;?>

							<?php foreach ( $rowData as $i => $row ) :
							$id = $row->{key};
							?>
							<tr class="editable" id="form-<?php echo $row->{key} ;?>">

								<?php if($setting['gridsettings']['gs_counter']) { ?>
								<td width="50">
									<?php echo ($i+1+$page) ?>
								</td>
								<?php } ?>

								<?php if($setting['gridsettings']['gs_checkbox']) { ?>
								<td width="50">
									<input type="checkbox" class="ids" name="id[]" value="<?php echo $row->{key} ?>" />
								</td>
								<?php } ?>

								<?php if($setting['view-method']=='expand'): ?>
								<td>
									<a href="javascript:void(0)" class="expandable" rel="#row-<?php echo $row->{key} ;?>" data-url="<?php echo ci_site_url('{class}/show/'.$id) ;?>">
										<i class="fa fa-plus " >
										</i>
									</a>
								</td>
								<?php endif;?>
								<?php foreach ( $tableGrid as $j => $field ) : ?>
								<?php if($field['view'] =='1'):
								$conn = (isset($field['conn']) ? $field['conn'] : array() );
								$value = AjaxHelpers::gridFormater($row->$field['field'], $row , $field['attribute'],$conn);

								?>
								<td align="<?php echo $field['align'];?>" data-values="<?php echo $row->$field['field'] ;?>" data-field="<?php echo  $field['field'] ;?>" data-format="<?php echo htmlentities($value);?>">
									<?php echo  $value;?>
								</td>
								<?php endif; ?>
								<?php endforeach; ?>

								<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
								<td data-values="action" data-key="<?php echo $row->{key} ;?> " >
									<?php if($setting['gridsettings']['gs_inlinebuttonpanel'])
									echo AjaxHelpers::buttonActionButtonInline('{class}',$access,$row->{key} ,$setting);
									else
									echo AjaxHelpers::buttonAction('{class}',$access,$row->{key} ,$setting);
									?>
								</td>
								<?php } ?>

							</tr>
							<?php if($setting['view-method']=='expand'): ?>
							<tr style="display:none" class="expanded" id="row-<?php echo $row->{key};?>">

								<?php if($setting['gridsettings']['gs_counter']) { ?>
								<td class="number">
								</td>
								<?php } ?>

								<td>
								</td>
								<td>
								</td>
								<td colspan="<?php echo $colspan;?>" class="data">
								</td>
								<td>
								</td>
							</tr>
							<?php endif;?>
							<?php endforeach; ?>

						</tbody>

					</table>
				</form>
			</div>


			<?php if($setting['gridsettings']['gs_footer']) : ?>
			<?php $this->load->view('ajaxfooter'); ?>
			<?php endif;?>
		</div>
		<?php endif; ?>
		
		<?php if($setting['gridsettings']['gs_portlet']) : ?>
	</div>
</div>

<?php endif;?>

<?php if($setting['inline'] =='true')  $this->load->view('cms/module/utility/inlinegrid') ;?>
<?php if($setting['view-method'] =='expand')  $this->load->view('cms/module/utility/extendgrid') ;?>
<script>


	<!--
	function filterList()
	{
		$('#filter-form').submit();
	}
	//--></script>

	<script>


	var displayFilterPanel = Cookies.set('ti_displayFilterPanel');

	jQuery('.btn-filter').click(function(event)
		{
			var $this = $(this),
			$panel = $this.parents('.panel'),
			$panelFilter = $panel.find('.panel-filter');

			$panel.find('.panel-filter').slideToggle(function()
				{
					if ($panelFilter.is(':visible'))
					{
						$('.panel-table .btn-filter').addClass('active');
						displayFilterPanel = 'true';
					} else
					{
						displayFilterPanel = 'false';
						$('.panel-table .btn-filter').removeClass('active');
					}

					Cookies.set('ti_displayFilterPanel', displayFilterPanel);
				});
		});





	$(document).ready(function()
		{
			$('.tips').tooltip();
			$('input[type="checkbox"],input[type="radio"]').iCheck(
				{
					checkboxClass: 'icheckbox_square-green',
					radioClass: 'iradio_square-green',
				});
			$('#{class}Table .checkall').on('ifChecked',function()
				{
					$('#{class}Table input[type="checkbox"]').iCheck('check');
				});
			$('#{class}Table .checkall').on('ifUnchecked',function()
				{
					$('#{class}Table input[type="checkbox"]').iCheck('uncheck');
				});
			$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
			$('#{class}Paginate .pagination li a').click(function()
				{
					var url = $(this).attr('href');
					if(url != '')
					{
						reloadData('#{class}',url);
					}
					return false ;
				});

		});
</script>
