
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> <?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3>
      </div>


    </div>

<div class="page-content-wrapper m-t">
		  
		 <form class="form-vertical row " action="<?php echo ci_site_url('cms/pages/save/'.$row['pageID']);?>" method="post" novalidate parsley-validate>

			<div class="col-sm-8 ">
				<div class="sbox">
					<div class="sbox-title">Page Content </div>	
					<div class="sbox-content">				
						
					  <div class="form-group  " >
						
						<div class="" style="background:#fff;">
						  <textarea name='content' rows='535' id='content'    class='form-control markItUp'  style = "height:600px"
							 ><?php echo htmlentities($content) ?></textarea> 
						 </div> 
					  </div> 	
					 </div>
				</div>	
		 	</div>		 
		 
		 <div class="col-sm-4 ">
			<div class="sbox">
				<div class="sbox-title">Page Info </div>	
				<div class="sbox-content">						
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class=""> PageID </label>
					
					  <?php echo form_input(array('name'=>'pageID','value'=>$row['pageID'],'class'=>'form-control'));?>
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Title </label>
					 <?php echo form_input(array('name'=>'title','value'=>$row['title'],'class'=>'form-control'));?>
					 
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Alias </label>
						 <?php echo form_input(array('name'=>'alias','value'=>$row['alias'],'class'=>'form-control'));?>
					 
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Filename </label>
					
					  <input name="filename" type="text" class="form-control input-sm" value="<?php echo $row['filename']?>" 
					  <?php if($row['pageID'] !='') echo 'readonly="1"';?> required
					  />
					
				  </div> 
				  <div class="form-group  " style="display:none" >
				  <label for="ipt"> Who can view this page ? </label>
					<?php foreach($groups as $group):?> 
					<label class="checkbox">					
					  <input  type='checkbox' name='roleid[<?php echo $group['id'] ?>]'    value="<?php echo $group['id'] ?>"
					  <?php if($group['access'] ==1 or $group['id'] ==1) echo 'checked'?>
					   /> 
					  <?php echo $group['name'] ?>
					</label>  
					<?php endforeach;?>	
						  
				  </div> 
				  <div class="form-group  "  style="display:none" >
					<label> Show for Guest ? unlogged  </label>
					<label class="checkbox"><input  type='checkbox' name='allow_guest' 
 						<?php if($row['allow_guest'] ==1 ) echo 'checked';?> value="1"	/> Allow Guest ?  </lable>
				  </div>

	
				  <div class="form-group  " >
					<label> Status </label>
					<label class="radio">					
					  <input  type='radio' name='status'  value="enable" required
					  <?php if( $row['status'] =='enable')  	echo 'checked';?>				  
					   /> 
					  Enable
					</label> 
					<label class="radio">					
					  <input  type='radio' name='status'  value="disabled" required
					    <?php if( $row['status'] =='disabled')  	echo 'checked';?>				  
					   /> 
					  Disabled
					</label> 					 
				  </div> 

				  <div class="form-group  " >
					<label> Template </label>
					<label class="radio">					
					  <input  type='radio' name='template'  value="frontend" required
					  <?php if( $row['template'] =='frontend')  	echo 'checked';?>	
					  				  
					   /> 
					  Frontend
					</label> 
					<label class="radio">					
					  <input  type='radio' name='template'  value="member" required
					  <?php if( $row['template'] =='member')  	echo 'checked';?>	
					  				  
					   /> 
					  Member
					</label> 					
					<label class="radio">					
					  <input  type='radio' name='template'  value="backend" required	
					    <?php if( $row['template'] =='backend')  	echo 'checked';?>			  
					   /> 
					  Backend
					</label> 					 
				  </div> 				  
				  
			  <div class="form-group">
					<input type="submit" name="apply" class="btn btn-info btn-sm" value="Apply" />
					<input type="submit" name="submit" class="btn btn-primary btn-sm" value="Submit " />
					<a href="<?php echo ci_site_url('cms/pages');?>" class="btn btn-sm btn-warning">Back To List </a>		 
		
			  </div> 
			  </div>
			  </div>				  				  
				  		
			</div>

		</form>
	</div>
</div>	
</div>
<style type="text/css">
.note-editor .note-editable { height:500px;}
</style>

<script src="<?php echo assets_url();?>js/tinymce/jquery.tinymce.min"></script>
<script type="text/javascript">
	tinymce.init({
  selector: 'textarea',
  theme: 'modern',
  plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern code',
  toolbar1: 'code | formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
</script>
