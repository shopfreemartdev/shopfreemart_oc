<script type="text/javascript" src="<?php echo base_url().'assets/sximo/js/plugins/jquery.nestable.js';?>"></script>

  <div class="page-content row">


	<div class="page-content-wrapper m-t">


	<ul class="nav nav-tabs" style="margin:10px 0;">

		<li <?php if($active == 'sidebar') echo 'class="active"'?>><a href=""><i class="icon-paragraph-justify2"></i><?php echo $this->lang->line('core.tab_sidemenu'); ?> </a></li>
	</ul>


		<div class="col-sm-12">

		<div class="box ">

			<form class="form-horizontal" action="<?php echo ci_site_url('cms/menu/saveOrder')?>" method="post">
			<input type="hidden" name="reorder" id="reorder" value="" />
			<button type="submit" class="btn btn-primary "><?php echo $this->lang->line('core.sb_reorder'); ?> </button>
			</form>
            <div id="list2" class="dd" style="min-height:350px;">
	              <ol class="dd-list">
				<?php foreach ($menus as $menu) : ?>
					  <li data-id="<?php echo $menu['menu_id'];?>" class="dd-item dd3-item">
						<div class="dd-handle dd3-handle"></div><div class="dd3-content"><?php echo  $menu['menu_name'];?>
							<span class="pull-right">
							<a href="<?php echo ci_site_url('cms/menu/index/'.$menu['menu_id'].'?pos='.$active);?>"><i class="fa fa-cogs"></i></a></span>
						</div>
						<?php if(count($menu['childs']) > 0) : ?>
							<ol class="dd-list" style="">
								<?php foreach ($menu['childs'] as $menu2) : ?>
								 <li data-id="<?php echo $menu2['menu_id'];?>" class="dd-item dd3-item">
									<div class="dd-handle dd3-handle"></div><div class="dd3-content"><?php echo $menu2['menu_name'];?>
										<span class="pull-right">
										<a href="<?php echo  ci_site_url('cms/menu/index/'.$menu2['menu_id'].'?pos='.$active);?>"><i class="fa fa-cogs"></i></a></span>
									</div>
									<?php if(count($menu2['childs']) > 0) : ?>
									<ol class="dd-list" style="">
										<?php foreach($menu2['childs'] as $menu3) : ?>
										 	<li data-id="<?php echo $menu3['menu_id'];?>" class="dd-item dd3-item">
												<div class="dd-handle dd3-handle"></div><div class="dd3-content"><?php echo  $menu3['menu_name'] ;?>
													<span class="pull-right">
													<a href="<?php echo  ci_site_url('cms/menu/index/'.$menu3['menu_id'].'?pos='.$active);?>"><i class="fa fa-cogs"></i></a>
													</span>
												</div>
											</li>
										<?php endforeach ;?>
									</ol>
									<?php endif;?>
								</li>
								<?php endforeach;?>
							</ol>
						<?php endif;?>
					</li>
				<?php endforeach; ?>
	              </ol>
            </div>
			<form class="form-horizontal" action="<?php echo ci_site_url('cms/menu/saveOrder')?>" method="post">
			<input type="hidden" name="reorder" id="reorder" value="" />


			<button type="submit" class="btn btn-primary "><?php echo $this->lang->line('core.sb_reorder'); ?> </button>
			</form>
		 </div>
		</div>

		</div>
		<div style="clear:both;"></div>

	</div>




<script>
$(document).ready(function(){
	$('.dd').nestable();
    update_out('#list2',"#reorder");

    $('#list2').on('change', function() {
		var out = $('#list2').nestable('serialize');
		$('#reorder').val(JSON.stringify(out));

    });
		$('.ext-link').hide();

	$('.menutype input:radio').on('ifClicked', function() {
	 	 val = $(this).val();
  			mType(val);

	});

	mType('<?php echo $row['menu_type'];?>');


});

function mType( val )
{
		if(val == 'external') {
			$('.ext-link').show();
			$('.int-link').hide();
		} else {
			$('.ext-link').hide();
			$('.int-link').show();
		}
}


function update_out(selector, sel2){

	var out = $(selector).nestable('serialize');
	$(sel2).val(JSON.stringify(out));

}
</script>
