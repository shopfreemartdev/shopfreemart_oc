
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><?php echo $this->lang->line('core.m_blastemail'); ?> <small> Send Bulk Email </small></h3>
      </div>
   
  
	  
	  
    </div>

 <div class="page-content-wrapper m-t">  
		  	
<div class="block-content">
	<ul class="nav nav-tabs" >
		<li><a href="<?php echo ci_site_url('cms/config');?>"><?php echo $this->lang->line('core.tab_siteinfo'); ?> </a></li>
		<li  class="active"><a href="<?php echo ci_site_url('cms/config/email');?>" ><?php echo $this->lang->line('core.t_emailtemplate'); ?> </a></li>
		<li><a href="<?php echo ci_site_url('cms/config/security');?>"><?php echo $this->lang->line('core.tab_loginsecurity'); ?> </a></li>
		<li ><a href="<?php echo ci_site_url('cms/config/getTranslation');?>" >Translation <sup>New </sup></a></li>
	</ul>	
<div class="tab-content">
	  <div class="tab-pane active use-padding" id="info">	
	 <form class="form-vertical row" action="<?php echo ci_site_url('cms/config/postEmail');?>" method="post">
	
	<div class="col-sm-6">
	
		<fieldset > <legend><?php echo $this->lang->line('core.fr_newaccountinfo'); ?>  </legend>
		  <div class="form-group">
			<label for="ipt" class=" control-label"><?php echo $this->lang->line('core.t_emailtemplate'); ?> </label>		
			<textarea rows="20" name="regEmail" class="form-control input-sm  markItUp"><?php echo $regEmail ;?></textarea>		
		  </div>  
		

		<div class="form-group">  
		<label for="ipt" class=" control-label col-md-4"> </label>
			<div class="col-md-8">  
				<button class="btn btn-primary" type="submit"><?php echo $this->lang->line('core.sb_savechanges'); ?> </button>	 
			</div>
		</div>
	
  	</fieldset>


</div> 


	<div class="col-sm-6">
	
	 <fieldset> <legend><?php echo $this->lang->line('core.forgotpassword'); ?> </legend>
  
		
		  <div class="form-group">
			<label for="ipt" class=" control-label "><?php echo $this->lang->line('core.t_emailtemplate'); ?> </label>
			
			<textarea rows="20" name="resetEmail" class="form-control input-sm markItUp"><?php echo $resetEmail ;?></textarea>
			 
		  </div> 
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4"> </label>
		<div class="col-md-8">
			<button class="btn btn-primary" type="submit"><?php echo $this->lang->line('core.sb_savechanges'); ?> </button>
		 </div> 
	 
	  </div>	  
	 </fieldset>    
 	
 </div>
 </form>
</div>
</div>
</div>
</div>





