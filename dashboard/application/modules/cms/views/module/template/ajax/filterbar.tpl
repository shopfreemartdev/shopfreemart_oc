<div class="search-conditions-box pad-15">
	<form class='form-horizontal dataTables_advancefilter'>
		<legend> Advance Filter <input type="reset" name="reset" class="btn btn-danger btn-sm pull-right" style="margin-top: -8px;
    padding: 4px;" value=" <?php echo lang('Reset'); ?> " /></legend>

		<div class="col-md-4">
			<div class="form-group  " >
				<label for="User Id" class=" control-label col-md-4 text-left">
					<?php echo SiteHelpers::activeLang('Field 1', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array()))  ;?>
				</label>
				<div class="col-md-8">
					<input type='search' class='form-control' placeholder="Enter any user id"  data-column-index='0'    />
					<i>
						<small>
						</small>
					</i>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="form-group  " >
				<label for="Date Registered" class=" control-label col-md-4 text-left">
					<?php echo SiteHelpers::activeLang('Feild 2', (isset($fields['date_registered']['language'])? $fields['date_registered']['language'] : array()))  ;?>
				</label>
				<div class="col-md-8">

					<input type='search' class='form-control date' placeholder='Select a date' value='<?php echo $row['date_registered'];?>' data-column-name='date_registered'
					style='width:150px !important;'	    />
					<i>
						<small>
						</small>
					</i>
				</div>
			</div>
		</div>

		<div class="col-md-4">

			<div class="form-group  " >
				<label for="Rank Id" class=" control-label col-md-4 text-left">
					<?php echo SiteHelpers::activeLang('Field 3', (isset($fields['rank_id']['language'])? $fields['rank_id']['language'] : array()))  ;?>
				</label>
				<div class="col-md-8">
					<select data-column-name='rank_id' rows='5' id='rank_id' code='{$rank_id}'
							class='form-control select2 '     >
					</select>
					<i>
						<small>
						</small>
					</i>
				</div>
			</div>

		</div>
		<div style="clear:both">
		</div>
	</form>
</div>

