<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="page-content-wrapper m-t">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="{class}View"></div>			
		<div id="{class}Grid" style="min-height: 500px;"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var display{class}FilterPanel = Cookies.set('ti_display{class}FilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			display{class}FilterPanel = 'false';
		} else
		{
			display{class}FilterPanel = 'true';
			$( '#{class}Filter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_display{class}FilterPanel', display{class}FilterPanel);

		reloadData( '#{class}',"<?php echo base_url();?>{class}/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(display{class}FilterPanel);
			if (display{class}FilterPanel == null)
			{
				display{class}FilterPanel = 'true';	
				Cookies.set('display{class}FilterPanel', display{class}FilterPanel);
			}
			console.log(display{class}FilterPanel);				
			if (display{class}FilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#{class}','<?php echo base_url();?>{class}/data');	

		});
		
	
</script>

  <!--START OF CUSTOM CODE-->{custom_code}<!--END OF CUSTOM CODE-->