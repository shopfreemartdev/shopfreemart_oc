
	<div class="toolbar-line row">
    	<div class="col-md-6">
    		<div class="btn-group">		
	
				<?php echo AjaxHelpers::buttonBacktoDashboard($pageModule,'Back'); ?>
			
				<?php if($this->access['is_add'] ==1) :
					echo AjaxHelpers::buttonActionCreate($pageModule,$setting,$newbuttontext);
				   endif;
				  
				if($this->access['is_remove'] ==1) : ?>		
				<a href="javascript:void(0);"  onclick="ajaxRemove('#{class}','{class}');" class="tips btn  btn-danger" title="Remove">
				<i class="fa fa-trash-o"></i>&nbsp;<?php echo $this->lang->line('core.btn_remove'); ?> </a>
				<?php endif; ?>

			<?php  if($access['is_excel'] ==1) :?>
				<div class="btn-group">				
				   <button type="button" class="tips btn btn-primary dropdown-toggle "  title=" Download "
					  data-toggle="dropdown">
					  <i class="fa fa-download"></i> <?php echo $this->lang->line('core.btn_download'); ?>&nbsp; <span class="caret"></span>
				   </button>
				   <ul class="dropdown-menu" role="menu">
					  <li><a href="<?php echo ci_site_url( '{class}/export/excel') ;?>" title="Export to Excel" > Export Excel </a></li>
					  <li><a href="<?php echo ci_site_url( '{class}/export/word');?>"  title="Export to Word"> Export Word </a></li>
					  <li><a href="<?php echo ci_site_url( '{class}/export/csv');?>'"   title="Export to CSV"> Export CSV</a></li>
				   </ul>

				</div> 			
			<?php endif;?>		
			
				<a href="<?php echo ci_site_url( '{class}/export/print') ;?>" onclick="ajaxPopupStatic(this.href); return false;" class="tips btn  btn-info"  title=" Print ">
				<i class="fa fa-print"></i> Print </a>	


				
			</div>
		</div>	
    	<div class="col-md-6">
    		<div class="btn-group pull-right">				
				 <a href="{class}/flysearch" onclick="SximoModal(this.href,'Advance Search'); return false;" class="tips btn  btn-default"  title="Advance Search ">
				<i class="fa fa-search"></i> Advance Search </a> 	
			</div>
		</div>
	</div>