<style>
table.dataTable tbody tr.selected {
    background-color: #B0BED9;
}
</style>
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header ">
      <div class="page-title">
        <h3> Module <small>  List Of All Modules </small></h3>
      </div>
    </div>
	<div class="page-content-wrapper">
	<?php //$this->load->view('cms/module/tab',array('active'=>'config')); ?>

	<div class="tabbable tabbable-custom ribon-sximo">
<section class="  fadeInDown">

		<div class="row m-l-none m-r-none m-t  white-bg shortcut " >
			<div class="col-sm-6 col-md-3 b-r  p-sm ">
				<a href="<?php echo ci_site_url('cms/module/create');?>" class="btn btn-primary">
					Create Module
				</a>
			</div>
		</div> </section>		

		
	</div>
	<?php echo $this->session->flashdata('message');?>
	  <div class="white-bg p-sm m-b unziped" style=" border:solid 1px #ddd; display:none;">
	    <form action="<?php echo ci_site_url('cms/module/install') ?>" class="breadcrum-search" name="moduleinstall" id="moduleinstall" method="post" enctype="multipart/form-data" parsley-validate novalidate >
	      <h3>Select File ( Module zip installer ) </h3>
	      <p>  <input type="file" name="installer" required style="float:left;">  <button type="submit" class="btn btn-primary btn-xs" style="float:left;"  ><i class="fa fa-upload"></i> Install</button></p>
	    </form>
	    <div class="clr"></div>
	  </div>

	<form name="SximoTable" id="SximoTable" action="#" method="post" class="form-horizontal" >
	<div class="table-responsive ibox-content   fadeInUp" style="min-height:400px;">
	<?php if(count($rowData) >=1) :?> 
		<table id="gridtable" class="table table-striped table-bordered table-hover table-responsive">
			<thead>

	 		<tr>
				<th><?php echo $this->lang->line('core.btn_action'); ?> </th>					
				<th><input type="checkbox" class="checkall" /></th>
				<th class="searchable"><?php echo $this->lang->line('core.t_module'); ?> </th>
				<th class="searchable">Controller</th>
				<th class="searchable">Database</th>
				<th class="searchable">PRI</th>
				<th class="searchable">Created</th>
		
			</tr>
			</thead>
        <tbody>
		<?php foreach ($rowData as $row) : ?>
			<tr>		
				<td>
				<div class="btn-group">
				<button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
				<i class="fa fa-cog"></i> <span class="caret"></span>
				</button>
					<ul style="display: none;" class="dropdown-menu icons-right">
						<li><a href="<?php echo ci_site_url($row->module_name);?>" target="_blank"><i class="fa fa-cog"></i><?php echo $this->lang->line('core.fr_viewmodule'); ?> </a></li>
						<li><a href="<?php echo ci_site_url('cms/module/config/'.$row->module_name);?>"><i class="fa fa-edit"></i><?php echo $this->lang->line('core.fr_editmodule'); ?> </a></li>
						<li><a href="javascript://ajax" class="clone" onclick="ajaxClone(<?php echo $row->module_id?>,'<?php echo ci_site_url('cms/module')?>')" ><i class="fa fa-clone"></i>Clone</a></li>
						<?php if($type !='core') : ?>
						<li><a href="javascript://ajax" onclick="SximoConfirmDelete('<?php echo ci_site_url('cms/module/destroy/'.$row->module_id);?>')"><i class="fa fa-trash-o"></i><?php echo $this->lang->line('core.fr_removemodule'); ?> </a></li>
						<li class="divider"></li>
						<li><a href="<?php echo ci_site_url('cms/module/rebuild/'.$row->module_id);?>"><i class="fa fa-reload"></i><?php echo $this->lang->line('core.fr_rebuildmodule'); ?> </a></li>
						<?php endif;?>
					</ul>
				</div>					
				</td>
				<td>
				 <?php if($type !='core'):?>
				<input type="checkbox" class="ids" name="id[]" value="<?php echo $row->module_id ;?>" /> <?php endif;?></td>
				<td><?php echo $row->module_title ;?> </td>
				<td><?php echo $row->module_name ;?> </td>
				<td><?php echo $row->module_db ;?> </td>
				<td><?php echo $row->module_db_key ;?> </td>
				<td><?php echo $row->module_created ;?> </td>
			</tr>
		<?php endforeach;?>	
	</tbody>

	</table>
	</form>
	
	<?php else:?>
		
		<p class="text-center" style="padding:50px 0;"><?php echo $this->lang->line('core.norecord'); ?> ! 
		<br /><br />
		<a href="<?php echo ci_site_url('cms/module/create');?>" class="btn btn-default "><i class="icon-plus-circle2"></i><?php echo $this->lang->line('core.fr_newmodule'); ?> </a>
		 </p>	
	<?php endif;?>
	</div>	
	
	</div>	

</div>	  
	  
  <script language='javascript' >
  var editor; 
  var firstload=true;
  jQuery(document).ready(function($){


    // Setup - add a text input to each footer cell
    $('.searchable').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#gridtable').DataTable({
    		dom: '<fi<t><lp>>',
            stateSave: true,
    	    select: true	});
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );




	// $('#gridtable tbody tr').every(function () {
	// 	console.log($(this).columns);
 //    });

    var m_module = '';
    <?php if(isset($_GET['m'])) { ?>
    	m_module = '<?php echo $_GET['m']?>';
    <?php } ?>
    if (firstload && m_module != '')
    {
		$('#gridtable tbody td').each(function() {
		        var cellText = $(this).html();    
		        if($.trim(cellText) == m_module){
		         	$(this).closest('tr').addClass("selected");
		        }
		});    	
	}



    $('#gridtable tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('#button').click( function () {
        table.row('.selected').remove().draw( false );
    } );

    $('.post_url').click(function(e){
      e.preventDefault();
      if( ( $('.ids',$('#SximoTable')).is(':checked') )==false ){
        alert( $(this).attr('data-title') + " not selected");
        return false;
      }
      $('#SximoTable').attr({'action' : $(this).attr('href') }).submit();
    })



  });


function ajaxClone(  id , url )
{
	
	
	var n = noty({				
		text: 'Enter new module name: <br/><input id="txtmodule_name" type="text" placeholder="">',
		type: 'Confirmation',
		timeout : 50,
		layout: 'topCenter',
	    theme: 'relax',
		buttons: [
			{addClass: 'btn btn-primary btn-sm', text: 'Submit', onClick: function($noty) {	
					var datas = id; //.serialize();
					var module_name = $noty.$bar.find('input#txtmodule_name').val();

					$.post( url+'/duplicate/'+id+'?modulename='+module_name,module_name,function( data ) {

						data = JSON.parse(decodeURIComponent(data));
						console.log(data);
							//notyMessage(data);
							//ajaxFilter( id ,url );
						if(data.status =='success')
						{
							notyMessage(data.message);
						} else {
							notyMessage(data.message );
						}				
					});	
					$noty.close();
				}
			},
			{addClass: 'btn btn-danger btn-sm', text: 'Cancel', onClick: function($noty) {
					$noty.close();
				}
			}
		]
	});	

}


  </script>	  
