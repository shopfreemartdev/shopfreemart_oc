<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-title">
            <h3>
                <?php echo $this->
                lang->line('core.mod_mysqltitle'); ?> :
                <?php echo ucwords( $row->
                module_name );?>
                <small>
                    <?php echo $this->
                    lang->line('core.mod_mysqltitlesub'); ?>
                </small>
            </h3>
        </div>
    </div>
    <div class="page-content-wrapper m-t">
        <?php $this->
        load->view('cms/module/tab',array('active'=>'sql')); ?>
        <?php echo $this->
        session->flashdata('message');?>
        <div class="sbox fadeInUp">
            <div class="sbox-title">
                <h5>
                    <?php echo $this->
                    lang->line('core.mod_mysqlinfo'); ?>
                    <small>
                        <?php echo $this->
                        lang->line('core.mod_mysqlinfosub'); ?>
                    </small>
                </h5>
            </div>
            <div class="sbox-content">
                <form action="<?php echo ci_site_url('cms/module/savesql/'.$module_name);?>" class="form-vertical" method="post">
                    <div class="infobox infobox-info fade in">
                        <button class="close" data-dismiss="alert" type="button">
                            x
                        </button>
                        <p>
                            <?php echo $this->
                            lang->line('core.mod_sqltips_a'); ?>
                        </p>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="ipt">
                            <?php echo $this->
                            lang->line('core.mod_sqlselect'); ?>
                        </label>
                        <textarea class="tab_behave form-control" id="sql_select" name="sql_select" placeholder="<?php echo $this->lang->line('core.mod_sqlselect'); ?>" rows="5" style="height:250px">
                            <?php echo trim($sql_select) ;?>
                        </textarea>
                    </div>

                    <div class="form-group">
                        <label class=" control-label" for="ipt">
                        </label>
                        <button class="btn btn-primary" type="submit">
                            <?php echo $this->
                            lang->line('core.btn_savesql'); ?>
                        </button>
                        <button class="btn btn-primary rebuild-model"  data-style="zoom-in">Rebuild Model</button>

                    </div>

                    <div class="form-group">
                        <label class=" control-label" for="ipt">
                            <?php echo $this->
                            lang->line('core.mod_sqlwhere'); ?>
                        </label>
                        <textarea class="form-control input-sm" id="sql_where" name="sql_where" placeholder="<?php echo $this->lang->line('core.mod_sqlwhere'); ?>" rows="2" style="height:350px">
                            <?php echo $sql_where ;?>
                        </textarea>
                    </div>
                    <div class="infobox infobox-danger fade in">
                        <button class="close" data-dismiss="alert" type="button">
                            x
                        </button>
                        <p>
                            <?php echo $this->
                            lang->line('core.mod_sqltips_b'); ?>
                        </p>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="ipt">
                            <?php echo $this->
                            lang->line('core.mod_sqlgroup'); ?>
                        </label>
                        <textarea class="form-control input-sm" id="sql_group" name="sql_group" placeholder="SQL Grouping Statement" rows="2" style="height:250px">
                            <?php echo $sql_group ;?>
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="ipt">
                            <?php echo lang('Count Select (Optional)'); ?>
                        </label>
                        <input class="form-control input-sm" id="sql_countselect" name="sql_countselect" placeholder="SELECT count(0)" type="text" value ="<?php echo $sql_countselect;?>"></input>
                    </div>
                    <div class="form-group">
                        <label class=" control-label" for="ipt">
                        </label>
                        <button class="btn btn-primary" type="submit">
                            <?php echo $this->
                            lang->line('core.btn_savesql'); ?>
                        </button>
                    </div>
                    <input name="module_id" type="hidden" value="<?php echo $row->module_id ;?>"/>
                    <input name="module_name" type="hidden" value="<?php echo $row->module_name ;?>"/>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var btnrebuild = $( '.rebuild-model' ).ladda();
    $('.rebuild-model').on('click', function () {
          bootbox.confirm({
            title: "Are you sure you want to rebuild model file?",
            message: "All changes will be overwritten except your code inside the safe zone.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancel'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirm'
                }
            },
            callback: function (result) {
                    if (result)
                    {
                        btnrebuild.ladda( 'start' );
                            $.ajax({
                                url: '<?=base_url()?>cms/module/dobuild/<?php echo $row->module_id ;?>',
                                type: 'POST',
                                dataType: 'json',
                                data: {model:'y'},
                                success: function (result) {
                                    if (result.success) {
                                        var alertId = appAlert.success(result.message, {duration: 20000});
                                        //bind undo selector
                                        if (undo !== "0") {
                                            undoHandler({
                                                alertSelector: alertId,
                                                url: url,
                                                id: id
                                            });
                                        }
                                    } else {
                                        appAlert.error(result.message);
                                    }
                                    btnrebuild.ladda('stop');
                                }
                            });
                    }                
                }
                
            });
  });

</script>
