<?php

class Menu extends Member_Controller  {

	protected $layout = "layouts.main";
	public $module 	= 'menu';
	public $per_page	= '100';
		
	public function __construct() {		
		parent::__construct();	
		load_sximo_language();
		$this->load->model('cms/menumodel');
		$this->model = $this->menumodel;	
		$this->info = $this->model->makeInfo( $this->module);	
		$this->access = $this->model->validAccess($this->info['id']);	

		//if(!$this->session->userdata('logged_in'))redirect('user/login',301);
		//if($this->session->userdata('gid') !=1) redirect('dashboard',301);		
		
	} 
	
	public function index( $id = null )
	{
	
		$pos = ($this->input->get('pos')!='' ? $this->input->get('pos') : 'top' );
		$rows = $this->db->get_where('sfm_cms_menu',array('menu_id'=>$id ));
		if(count($rows->result())>=1)
		{
			$row = $rows->result_array();
			$this->data['row'] =  $row[0];
		} else {
			$this->data['row'] = $this->model->getColumnTable('sfm_cms_menu'); 
		}
		//echo '<pre>';print_r($this->data['row']);echo '</pre>';exit;

		$this->data['menus']		= SiteHelpers::menus( $pos ,'all');
		$this->data['modules'] 		= $this->db->query("SELECT * FROM sfm_cms_module WHERE module_type !='core'")->result();
		$this->data['groups'] 		= $this->db->get('main_shopfreemart.sfm_crm_roles');
		$this->data['pages'] 		= $this->db->get('sfm_cms_pages');
		$this->data['active'] = $pos;	
		$this->data['pageTitle'] = $this->lang->line('core.m_menu'); 
		$this->render_view('cms/menu/index',$this->data, true );		
    		
	}
	
	public function add( $id = null)
	{
	
		$pos = ($this->input->get('pos')!='' ? $this->input->get('pos') : 'top' );
		$rows = $this->db->get_where('sfm_cms_menu',array('menu_id'=>$id ));
		if(count($rows->result())>=1)
		{
			$row = $rows->result_array();
			$this->data['row'] =  $row[0];
		} else {
			$this->data['row'] = $this->model->getColumnTable('sfm_cms_menu'); 
		}
		//echo '<pre>';print_r($this->data['row']);echo '</pre>';exit;

		$this->data['menus']		= SiteHelpers::menus( $pos ,'all');
		$this->data['modules'] 		= $this->db->query("SELECT * FROM sfm_cms_module WHERE module_type !='core'")->result();
		$this->data['groups'] 		= $this->db->get('main_shopfreemart.sfm_crm_roles');
		$this->data['pages'] 		= $this->db->get('sfm_cms_pages');
		$this->data['active'] = $pos;	
		$this->data['pageTitle'] = $this->lang->line('core.m_menu'); 
		$this->render_view('cms/menu/add',$this->data, true );		
	}

	public function reorder( $id = null)
	{
	
		$pos = ($this->input->get('pos')!='' ? $this->input->get('pos') : 'sidebar' );
		$rows = $this->db->get_where('sfm_cms_menu',array('menu_id'=>$id, 'active' => '1' ));
		if(count($rows->result())>=1)
		{
			$row = $rows->result_array();
			$this->data['row'] =  $row[0];
		} else {
			$this->data['row'] = $this->model->getColumnTable('sfm_cms_menu'); 
		}
		//echo '<pre>';print_r($this->data['row']);echo '</pre>';exit;

		$this->data['menus']		= SiteHelpers::menus( $pos ,'all');
		$this->data['modules'] 		= $this->db->query("SELECT * FROM sfm_cms_module WHERE module_type !='core'")->result();
		$this->data['groups'] 		= $this->db->get('main_shopfreemart.sfm_crm_roles');
		$this->data['pages'] 		= $this->db->get('sfm_cms_pages');
		$this->data['active'] = $pos;	
		$this->data['pageTitle'] = $this->lang->line('core.m_menu'); 
		$this->render_content('cms/menu/reorder',$this->data, true );		
	}


	function saveOrder( $id =0)
	{
		$rules = array(
			array('field'   => 'reorder','label'   => 'Reorder Array','rules'   => 'required'),
		);
		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() ){
			$menus = json_decode($this->input->post('reorder'),true);
			$child = array();
			$a=0;
			foreach($menus as $m)
			{			
				if(isset($m['children']))
				{
					$b=0;
					foreach($m['children'] as $l)					
					{
						if(isset($l['children']))
						{
							$c=0;
							foreach($l['children'] as $l2)
							{
								$level3[] = $l2['id'];
								$this->db->where(array('menu_id'=>$l2['id']));
								$this->db->update('sfm_cms_menu',array('parent_id'=> $l['id'],'ordering'=>$c));
								$c++;	
							}		
						}
						$this->db->where(array('menu_id'=>$l['id']));
						$this->db->update('sfm_cms_menu',array('parent_id'=> $m['id'],'ordering'=>$b));
						$b++;
					}							
				}
				$this->db->where(array('menu_id'=>$m['id']));
				$this->db->update('sfm_cms_menu',array('parent_id'=> 0,'ordering'=>$a));
				$a++;		
			}

			redirect('cms/menu',301);
		} else {
			redirect('cms/menu',301);
		}	
	}
		
	
	function save( $id =0)
	{

		$rules = array(
			array('field'   => 'menu_name','label'   => 'Menu Name','rules'   => 'required'),
			array('field'   => 'active','label'   	 => 'Active','rules'   => 'required'),
			array('field'   => 'menu_type','label'   => 'Menu Type','rules'   => 'required'),
			array('field'   => 'position','label'    => 'Position','rules'   => 'required')
		);
		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() ){
			$pos = $this->input->post('position',true);	
			$data = $this->validatePost('sfm_cms_menu');			
			$arr = array();
			$groups = $this->db->get('main_shopfreemart.sfm_crm_roles')->result();
			foreach($groups as $g)
			{
				$arr[$g->roleid] = (isset($_POST['groups'][$g->roleid]) ? "1" : "0" );	
			}
			$data['access_data'] = json_encode($arr);		
			$data['allow_guest'] 	= (isset($_POST['allow_guest']) ? '1' : '0');
			$data['parent_id'] 	= (isset($_POST['parent_id']) ? '0' : '0');
			//echo '<pre>';print_r($data);echo '</pre>';exit;	
			$this->model->insertRow($data , $this->input->post('menu_id'));			
			redirect('cms/menu?pos='.$pos);
			
		} else {
			redirect('cms/menu?pos='.$pos);
		}	
	
	}
	
	public function destroy($id)
	{
		$menus = $this->db->get_where('sfm_cms_menu',array('parent_id'=>$id))->result();
		
		foreach($menus as $row)
		{
			$this->model->destroy($row->menu_id);
		}
		
		$this->model->destroy($id);
		// redirect
		SiteHelpers::alert('success','Successfully deleted row!');
		redirect('cms/menu',301);
	}		
		
	
}	