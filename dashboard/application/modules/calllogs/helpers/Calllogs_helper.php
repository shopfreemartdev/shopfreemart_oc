<?php

perfex_add_action('calllogs_render_table_header','_calllogs_render_table_header');
function _calllogs_render_table_header(){
?>
	<div class="member-page-header">
		<?php get_instance()->load->view('calllogs/pagehead');?>
	</div>
<?php 
}

perfex_add_action('calllogs_render_table_toolbar','_calllogs_render_table_toolbar');
function _calllogs_render_table_toolbar(){
   get_instance()->load->view('calllogs/toolbar');
}


perfex_add_action('calllogs_render_table_row_action_buttons','_calllogs_render_table_row_action_buttons');
function _calllogs_render_table_row_action_buttons($param = array()){
	$row = $param['row'];
	$access = $param['access'];
	$setting = $param['setting'];

	return AjaxHelpers::buttonAction('calllogs',$access,$row->logid ,$setting);
}

perfex_add_action('calllogs_after_render_table_thead','_calllogs_after_render_table_thead');
function _calllogs_after_render_table_thead(){

}
perfex_add_action('calllogs_before_render_table_tfoot','_calllogs_before_render_table_tfoot');
function _calllogs_before_render_table_tfoot(){

}
perfex_add_action('calllogs_render_table_tfoot','_calllogs_render_table_tfoot');
function _calllogs_render_table_tfoot(){

}
perfex_add_action('calllogs_after_render_table_tfoot','_calllogs_after_render_table_tfoot');
function _calllogs_after_render_table_tfoot(){

}
perfex_add_action('calllogs_render_table_footer','_calllogs_render_table_footer');
function _calllogs_render_table_footer($param){
	$setting = $param['setting'];
	$rowData = $param['rowData'];
	$total_rows = $param['total_rows'];

	if($setting['gridtype'] == 'JQGrid') {	?>

		<!-- JQGRID INIT START -->
		<script type="text/javascript" src="<?=assets_url()?>perfex/plugins/jqwidgets/jqx-all.js" charset="UTF-8"></script>
		<script>
		var gridID = "calllogsjqxgrid";
		/*
		 * ***************************************************************
		 * Definitions
		 * ***************************************************************
		 */
		/*
		 * function for get Grid data adapter
		 */
		function getAdapter() {
		    var source ={
		        datatype: "json",
		        datafields: [
		            { name: 'user_id'},
		            { name: 'display_name'},
		            { name: 'commissions'},
		            { name: 'coins'},
		            { name: 'Active'}
		        ],
		        sortcolumn: 'user_id',
		        sortdirection: 'DESC',
		        cache: false,
		        url: '<?php echo base_url();?>calllogs/listdata',
		        filter: function(){
		            $("#"+gridID).jqxGrid('updatebounddata', 'filter');
		        },
		        sort: function(){
		         $("#"+gridID).jqxGrid('updatebounddata', 'sort');
		        },
		        root: 'Rows',
		        beforeprocessing: function(data){		
		            if (data != null){
		                source.totalrecords =  data[0].TotalRows;					
		            }
		        }
		    };
		    var dataAdapter = new $.jqx.dataAdapter(source, {
		        loadError: function(xhr, status, error){
		            alert(error);
		        }
		    });
		    return dataAdapter;
		}//End:getAdapter()
			/*
			 * ***************************************************************
			 * Implementaions
			 * ***************************************************************
			 */
			$(document).ready(function () {
			    var editRenderer = function (row, datafield, value, defaultHtml, columnSettings, rowData) {return '<a href="#" class="tooltipT"  title="Click to Edit." style="margin-left:5px;position:relative; top:5px">Edit</a>';}
			    var deleteRenderer = function (row, datafield, value, defaultHtml, columnSettings, rowData) {return '<a href="#"  style="margin-left: 5px;" title="Click to Delete.">Delete</a>';}
			    var statusRenderer = function (row, datafield, value, defaultHtml, columnSettings, rowData) {
			        var str_status = ""; 
			        if(rowData.Active ===  "1"){
			            str_status = 'Deactive';
			            return '<a href="javascript:void(0);" onclick="#" style="margin-left:5px;" class="status" title="Click to Deactivate.">'+str_status+'</a>';
			        }else{
			            str_status = 'Active';
			            return '<a href="javascript:void(0);"  onclick="#" style="margin-left:5px;" class="status" title="Click to Activate.">'+str_status+'</a>';
			        }
			    }
			    $("#"+gridID).jqxGrid({
			        width: '100%',
			        source: getAdapter(),
			        theme: 'default',
			        pageable: true, 
			        
			        pagesize:20,
			        pagesizeoptions: ['20', '40', '60','100'],
			        autoheight: true,
			        sortable: true,
			        altrows: true,
			        enabletooltips: true,
			        filterable: true,
			        showfilterrow: true,
			        virtualmode: true,
			        enablehover: true,
			        selectionmode: 'checkbox',
			        showstatusbar: true,
			        rendergridrows: function(obj){
			            return obj.data;    
			        },
			        renderstatusbar: function (statusbar) {
			            // appends buttons to the status bar.
			            var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>");
			            var reloadButton = $("<div style='float: left; margin-left: 5px;'><button type='button' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-refresh'></span> Reload</button></div>");
			            var clearButton = $("<div style='float: left; margin-left: 5px;'><button type='button' class='btn btn-default btn-xs'><span class='glyphicon glyphicon-filter'></span>Clear</button></div>");
			            container.append(reloadButton);
			            container.append(clearButton);
			            statusbar.append(container);
			            //reloadButton.jqxButton({ theme: theme, width: 65 });
			            // reload grid data.
			            reloadButton.click(function (event) {
			                //alert("stoper here");
			                $("#"+gridID).jqxGrid({ source: getAdapter() });
			                //$("#"+gridID).jqxGrid('sortby', 'OrderNumber', 'desc');
			            });
			            clearButton.click(function (event) {
			                $("#"+gridID).jqxGrid('clearfilters');
			            });
			        },
			        columns: [
			            { text: 'User ID', datafield: 'user_id', columngroup: 'CustomersDetail',width:'100',filtercondition: 'starts_with'},
			            { text: 'Name', datafield: 'display_name',columngroup: 'CustomersDetail', filtercondition: 'starts_with'},
			            { text: 'Commissions', datafield: 'commissions',columngroup: 'CustomersDetail', filtercondition: 'starts_with'},
			            { text: 'Coins', datafield: 'coins',columngroup: 'CustomersDetail', filtercondition: 'starts_with'},
			            { text: 'Edit',cellsalign: 'center',filterable: false,sortable: false,columngroup: 'Action',cellsrenderer: editRenderer, width:'60',cellsalign: 'center'},
			            { text: 'Delete',cellsalign: 'center',filterable: false,sortable: false,align: 'center',columngroup: 'Action',cellsrenderer: deleteRenderer, width:'60',cellsalign: 'center'},
			            { text: 'Status',cellsalign: 'center',filterable: false,sortable: false ,align: 'center',columngroup: 'Action',cellsrenderer: statusRenderer, width:'100',cellsalign: 'center'},
			        ],
			        columngroups: [
			            { text: 'Customer Detail', align: 'center', name: 'CustomersDetail'},
			            { text: 'Actions', align: 'center', name: 'Action' }
			        ]
			    });
			});
		</script>
			<!--End:JQGRID INIT() -->

  <?php }
	}  ?>