<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');?>

<?php perfex_do_action('calllogs_render_table_header'); ?>
<?php echo $this->session->flashdata('message');?>
<div id="ajax">
  <?=$show_message?>
</div>

<?php
if($setting['gridsettings']['gs_portlet']) : ?>
<div class="sbox">

  <!-- BOX TITLE -->
  <div class="sbox-title">
    <!-- Header -->
    <h2 class="pull-left">
      <span>
        <i class="fa fa-phone-square" style="font-size: 18px !important">
        </i>
        <?php echo $pageTitle ;?>&nbsp;
        <small>
          <?php echo $pageNote ;?>
        </small>
      </span>
    </h2>

    <div class="btn-group pull-right">

      <a href="javascript:void(0)" class="" onclick="reloadData('#calllogs','calllogs/data')"  title="Reload Data" class="btn btn-default">
        <i class="fa fa-refresh" style="font-size: 17px; margin-top: 5px;" >
        </i> Reload
      </a>
      <?php if(is_staff_logged_in() && get_staff_user_id() == '1') : ?>
      <a href="<?php echo base_url('cms/module/rebuild/'.$pageModule.'?rurl='.$pageModule) ?>"  title="Rebuild" class="btn btn-default">
        <i class="fa fa-cog" style="font-size: 17px; margin-top: 5px;" >
        </i>&nbsp;
      </a>
      <a href="<?php echo base_url('cms/module/config/'.$pageModule) ?>" title="Configuration" class="btn btn-default">
        <i class="fa fa-wrench" style="font-size: 17px; margin-top: 5px;">
        </i>&nbsp;
      </a>
      <?php endif; ?>
    </div>
  </div>

    <!-- BOX CONTENT -->
  <div class="sbox-content ">
    <?php endif;?>

    <!-- Filter Link Row -->
    <div class="filterlinks-line ">
      <ul class="subsubsub">
          <?php
          if (!$currentlinkfilter) $currentlinkfilter = 1;
          if (!isset($controller)) $controller = '';
          foreach ($linkfilters as $link) {

          if($controller == 'Member_Controller'){
            $count = db_get_var(FormatHelper::validatesql($link['sql_count']));
          }
          else {
            $count = $link['count'];
          }


          if ($link['id'] == $currentlinkfilter)
          {
              echo "<li><span class='current'  data-toggle='tooltip' data-title='".$link["description"]."' data-original-title='' title=''>".$link['title']."</span><span class='count'> (".number_format($count).")</span></li>";
          }
          else
          {
              $currentclass = '';
          ?>
              <li >
                  <a href="<?=base_url()?>calllogs/linkfilter/<?=$link['id']?>" data-module-id='calllogs'  data-toggle="tooltip" data-title="<?=$link['description']?>" data-original-title="" title="" data-act='sximo-ajax'><?=$link['title']?><span class="count"> (<?=number_format($count)?>) </span></a></li>
            <?php }
          } ?>
      </ul>
    </div>


    <!-- Grid Filter Row -->
    <div class="wells panel_s mg-btm-5 datatable-filter" id="filter-panel1">
      <style>
        li.active {
          color: white;
        }
        .checkbox input[type="checkbox"], .checkbox input[type="radio"] {
          opacity: 1;
          z-index: 1;
        }
      </style>
        <div class="row" style="display:inline;">
            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-search">
                        </i>
                    </span>
                    <input class="form-control multiselect-search" id="filter-member"  placeholder="Search for anything..." type="search">
                    </input>
                </div>
            </div>
            <div class="col-md-5">
                <div class="input-group">

                    </input>
                </div>
            </div>


            <div class="col-md-4">
                <div class="btn-group pull-right mleft4 btn-with-tooltip-group _filter_data" uib-dropdown auto-close="always" data-toggle="tooltip" data-title="Filter Options" data-original-title="" title="">
                    <button type="button" class="btn btn-default dropdown-toggle" uib-dropdown auto-close="always"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                     <i class="fa fa-filter"></i>  <span class="caret"></span>
                    </button>
                    <ul id="filter-dropdownmenu"  class="dropdown-menu dropdown-menu-right width300 ">
                          <li class="">
                            <a href="#" data-cview="clearfilter" name="clearfilter" class="doReset clearfilter tips dropdown-filter-item" title="Clear Filters">Clear Filters</a>
                          </li>
                          <li class="">
                            <a href="#" data-cview="advancefilter" class="dropdown-filter-item" name="advancefilter"  onclick="do_filter_active('advancefilter');return true;" type="button" data-toggle="collapse" data-target="#advancefilter-panel" title="Advance Filter">Advance Filter</a></li>
      <!--                     <li class=""><a href="#" data-cview="filter_row" class="dropdown-filter-item" onclick="do_filter_active('filter_row');return true;" type="button" data-toggle="collapse" data-target="#filter_row" title="Filter Row">Filter Row</a></li>
 -->                      <li class="">
                            <a href="#" data-cview="resetlayout" name="resetlayout" class="doResetLayout tips dropdown-filter-item" title="Reset Layout">Reset Layout</a>
                          </li>

                     </ul>

                </div>
            </div>

        </div>
    </div>


    <!-- Advance Filter Row -->
    <div class="row wells filter collapse" id="advancefilter-panel" >
        <div class="col-md-12">
            <?php echo $this->load->view('calllogs/filterbar');?>
         </div>
    </div>



    <!-- Grid Table -->
    <div class="table-responsive">
    <table class="table table-striped table-bordered dataTable table-responsive " id="calllogsTable" >
      <!-- gs_filterbar -->

      <?php perfex_do_action('calllogs_before_render_table_thead'); ?>
      <thead>
        <tr class="filters">
          <?php
          if($setting['gridsettings']['gs_counter'])
          {
            ?>
            <th>
              <!-- <input type="text" class="form-control" placeholder="#" disabled> -->
              No
            </th>
            <?php
          }?>

          <?php
          if($setting['gridsettings']['gs_checkbox'])
          {
            ?>
            <th>
              <input type="checkbox" class="checkall" />
            </th>
            <?php
          }?>

          <?php
          if($setting['view-method'] == 'expand')
          {
            ?>
            <th>
            </th><?php
          }?>
          <?php
          foreach($tableGrid as $k => $t): ?>
          <?php
         // if($t['view'] == '1'): ?>

          <th class="sorting text-<?=$t['align']?>" <?php echo (isset($t['width']) ? 'width=' . $t['width'] : '') ?>  >

            <?php echo SiteHelpers::activeLang($t['label'], (isset($t['language']) ? $t['language'] : array())) ?>

          </th>
          <?php //endif;?>
          <?php endforeach;?>

          <?php
          if($setting['gridsettings']['gs_buttonpanel'])
          {
            ?>
            <th width=350>
              Action
            </th>
            <?php
          }?>
        </tr>
      </thead>

      <tbody>
      <div id="tableloading" class="searching jqx-rc-all jqx-rc-all-bootstrap" style="    text-align: center;height: 100%;position: absolute;width: 100%;display: none">
        <span style="position: absolute;left: 38px;right: 0;top: 27%;z-index: 9999;bottom: 0;font-size: 20px;font-weight: bold;" class="ajax-grid-load">
          Loading...
        </span>
      </div>
      </tbody>

      <?php perfex_do_action('calllogs_before_render_table_tfoot'); ?>
      <?php perfex_do_action('calllogs_after_render_table_tfoot'); ?>
    </table>
    </div>
    <?php
    if($setting['gridsettings']['gs_footer']) : ?>
    <?php $this->load->view('ajaxfooter'); ?>

    <?php endif; ?>

  </div>

  <?php
  if($setting['gridsettings']['gs_portlet']) : ?>
</div>
</div>
<?php endif;?>

<?php
if($setting['inline'] == 'true')  $this->load->view('cms/module/utility/inlinegrid') ;?>
<?php
if($setting['view-method'] == 'expand')  $this->load->view('cms/module/utility/extendgrid') ;?>

<?php perfex_do_action('calllogs_render_table_footer',array('setting'   =>$setting,'rowData'   => $rowData,'total_rows'=>$total_rows)); ?>



<!-- Delete Modal -->
<div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Member</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center;">
          <h3 style="color:#00699b;">Are you sure you want to delete this member?</h3>
          <h4 style="color:#ff0000;"><strong>Note:</strong> This process is irreversible</h4>
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?php echo base_url().'calllogs/deletemember/'; ?>" id="confirm_delete" class="btn btn-info pull-left" onclick="ajaxCall(this.href); return false;" data-dismiss="modal">Confirm Delete</a>
        <button id="cancel_delete" type="button" class="btn btn-danger" data-dismiss="modal">Cancel Delete</button>
      </div>
    </div>
  </div>
</div>

<?php //ld($columns); ?>

<script type="text/javascript">
  $(document).ready(function(){
    $(document).on('click', '.delete_member', function() {
      var id = $(this).attr("id");
      var url = "<?php echo base_url();?>calllogs/deletemember/"+id;
      $('#confirm_delete').attr("href",url);
    });
  });
</script>


<script>
  //# sourceURL=calllogs.js
  var gridtable;
  var gridcolumndefs = <?=$columndefs?>;
  var gridcolumns = <?=$columns?>;
  var initialdata = <?=$dataset?>;
  var initial_rankfilters = [<?=$rankfilters?>];
  var search_thread = null;

  $(document).ready(function()
  {
  //  Initialize Datatable();

    var _dom = '<?php if($setting['gridsettings']['gs_filterbar']) echo "RZrtip"; else echo "RZfrtip"; ?>'; //'Bfrtip',
    _dom = "JBRZC<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'il><'col-sm-7'p>>";

    var ajaxurl = "<?php echo base_url(); ?>calllogs/ajaxdata";
    var linkfilter = '<?php echo (isset($currentlinkfilter) ? $currentlinkfilter : ''); ?>';
    if (!empty(linkfilter))
    {
      ajaxurl = ajaxurl+'?linkfilter='+linkfilter;
    }

    gridtable = $('#calllogsTable').DataTable(
    {
        "data": initialdata,
        "language":
        {
          "processing": ""
        },
        "dom":  _dom, //'RirClt<"clear">p'
        "processing": true,
        "serverSide": true,
        "responsive": true,
        //"autoWidth": true,
        "deferRender": true,
        "lengthMenu": [[10, 25, 50, 100, 0], [10, 25, 50, 100, "All"]],
        "deferLoading":<?=$total_rows?>,
        "stateSave": false,
        "colReorder": true,
        "ColResize": true,
        "stateSaveParams": function (settings, data) {
           data.search.search = "";
        },
        "stateLoadParams": function (settings, data) {
          data.search.search = "";
        },
        "searchDelay": 1000,
        "select": true,
        buttons: [
          'colvis',
          'excel',
          'print'
        ]   ,
        "columnDefs": gridcolumndefs,
        "columns": gridcolumns,
        "ajax":
        {
          "dataType": 'json',
          "url": ajaxurl,
          "type": "POST",
          "data": function ( data )
          {

            data.filtermember = $('#filter-member').val();
            data.filtersponsor= $('#filter-sponsor').val();
            data.filterranks= $('#filter-rank').val();
            showLoading('#calllogsTable',true);
            <?php  if($setting['gridsettings']['gs_filterbar'])
            {
              ?>
              // var elementid = '#filterdropdown';
              // var items = $(elementid).jqxComboBox('getSelectedItems');
              // var selectedItems = "";
              // $.each(items, function (index)
              //   {
              //     selectedItems += this.value;
              //     if (items.length - 1 != index)
              //     {
              //       selectedItems += ",";
              //     }
              //   });
              // data.filtertype = selectedItems;
              <?php
            } ?>
          },
          "dataSrc": function ( json )
          {
            $('#total_all_pages').html(json.total_all_pages);
            $('#total_current_page').html(json.total_current_page);
            for ( var i=0, ien=json.data.length ; i<ien ; i++ )
            {
              //console.log(json.data[i]);
              // json.data[i][0] = '<a href=#>'+json.data[i][0]+'</a>';
              //  json.data[i][0] = PhotonDataFormatter.actionButtonDropdown(actionsbuttons);
            }
            showLoading('#calllogsTable',false);
            return json.data;
          }
        }
    });

    $('.doResetLayout').click(function()
        {
          gridtable.state.clear();
          window.location.reload();
      });

    $('#calllogsTable').init_datatable({
        tableId         : '#calllogs',
        table           : gridtable,
        headrowfilter   : true,
        action          :  "<?php echo base_url(); ?>calllogs"
    });
  // end Init_datatable();

    $(this).init_form();

 });

$(".dropdown-filter-item").on("click", function () {
    var t = $(this).attr("data-target");
    var dropdowns = $('#filter-dropdownmenu');
    if( dropdowns.is(':visible') ) {
      $('._filter_data').removeClass('open');
      //dropdowns.hide();
    } else {
      //dropdowns.show();
      $('._filter_data').addClass('open');
    }
    if (t != null)
    $(t).toggle();

});

  $('#filter-rank').multiselect({
      //buttonContainer: '<div class="open" />',
      includeSelectAllOption: true,
          buttonClass: 'form-control',
          nonSelectedText: 'Select Ranks ',
          numberDisplayed: 3,
          buttonWidth: '100%',
          //templates: {
            //  button: '<span class="multiselect dropdown-toggle" data-toggle="dropdown">Click me!</span>'
          //}
      onChange: function(option, checked, select) {
        gridtable.ajax.reload();
        return;
      },
        onSelectAll: function() {
        gridtable.ajax.reload();
        return;
      },
        onDeselectAll: function() {
        gridtable.ajax.reload();
        return;
      },

  });

  //FOR filter
  <?php  if($setting['gridsettings']['gs_filterbar'])
  {
    ?>
    //Init_filterdropdown("filterdropdown","Choose filter options");
    <?php
  } ?>
  <?php  if($setting['gridsettings']['gs_filterbar'])
  {
    ?>
    // function Init_filterdropdown(element,placeholder)
    // {
    //   var data = '<?=json_encode($module_filters)?>';
    //   var source =
    //   {
    //     datatype: "json",
    //     datafields: [
    //       {
    //         name: 'id'
    //       },
    //       {
    //         name: 'title'
    //       },
    //       {
    //         name: 'description'
    //       }
    //     ],
    //     localdata: data
    //   };
    //   var dataAdapter = new $.jqx.dataAdapter(source);
    //   element = '#'+element;
    //   $(element).jqxComboBox(
    //     {
    //       displayMember: "title", valueMember: "id",
    //       multiSelect: true,
    //       theme: 'bootstrap',
    //       width: "100%",
    //       height:34,
    //       source: dataAdapter
    //     });
    // }

    <?php
  } ?>

    // Head filter bar
    <?php //if($setting['gridsettings']['gs_filterbar']) { ?>
      var filterrow = $("<tr id='filter_row' class='collapse'>");
      var col_name;
      var col_index;
      <?php
      $col_index =0;
      foreach ($tableGrid as $k => $t)
      {
          if ($t['view'] == '1') {
             $control = SiteHelpers::transForm($t['field'] , $tableForm);
            ?>
            col_name = '<?=$t['field'];?>';
            col_index = <?=$col_index;?>;
            //var input = $("<input type='search' class='form-control' data-column-index='"+col_index+"'></input>")
            var input = $("<?=$control;?>")
            .data('column-index', col_index)
            .on( 'keyup change', function () {
              var index = $(this).data("column-index");

              if ( gridtable.columns(col_name).search() !== this.value ) {
                  gridtable.columns(index)
                      .search( this.value )
                      .draw();
              }
           });
            filterrow.append($('<td>').append(input));
          <?php $col_index ++;
        }
      } ?>
      // echo $this->load->view('calllogs/filterbar');?>
      filterrow.insertBefore($('#calllogsTable tbody'));
      //$(filter_row).toggle();

    <?php //} ?>


</script>

