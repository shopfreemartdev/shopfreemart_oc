<div class="page-wrap" style="background-color: #fff;">
<div class="page-content row">
	<!-- Page header -->
	<div class="sbox-header">
		<div class="sbox-title">
			<span><h3> <i class="fa  fa-list" style="font-size: 18px !important">
				</i><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3></span>
				<a href="<?php echo ci_site_url(); ?>activitylog" class="collapse-close btn btn-info pull-right" style="margin-top: -6px;" title="Return to Activity Log"><i class="fa fa fa-undo" alt></i>Go Back</a>
		</div>
	</div>  
	
 	<div class="page-content-wrapper m-t">   
	
		<div class="table-responsive">
			<table class="table table-striped table-bordered" >
				<tbody>	
			
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('User Id', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['user_id'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Activity', (isset($fields['activity']['language'])? $fields['activity']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['activity'] ;?> </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Date', (isset($fields['date']['language'])? $fields['date']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['date'] ;?> </td>
						
					</tr>		

					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Order ID', (isset($fields['order_id']['language'])? $fields['order_id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['order_id'] ;?> </td>
						
					</tr>

					<tr>
						<td width='30%' class='label-view text-right'>
						<?php echo SiteHelpers::activeLang('Transaction ID', (isset($fields['transaction_id']['language'])? $fields['transaction_id']['language'] : array())) ;?>
						</td>
						<td><?php echo $row['transaction_id'] ;?> </td>
						
					</tr>				
				</tbody>	
			</table>    
		</div>
	</div>
	
</div>
</div>
	  