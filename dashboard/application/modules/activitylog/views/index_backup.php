  <?php usort($tableGrid, "SiteHelpers::_sort"); ?>
  <div class="page-wrap" style="background-color: #fff;">
  <div class="page-content row">
    <!-- Page header -->
    <div class="sbox-header">
      <div class="sbox-title">
        <span><h3> <i class="fa  fa-list" style="font-size: 18px !important">
		</i><?php echo $pageTitle ?> <small><?php echo $pageNote ?></small></h3></span>
      </div>
    </div>


	<div class="page-content-wrapper m-t">
	<?php echo $this->session->flashdata('message');?>
	 <form action='<?php echo base_url('activitylog/destroy') ?>' class='form-horizontal' id ='SximoTable' method="post" >
	 <div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th> No </th>
				<?php foreach ($tableGrid as $k => $t) : ?>
					<?php if($t['view'] =='1'): ?>
						<th><?php echo SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array()))  ?></th>
					<?php endif; ?>
				<?php endforeach; ?>
				<th>Action</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="sximo-quick-search" >
				<td> # </td>
				<td> </td>
				<?php foreach ($tableGrid as $t) :?>
					<?php if($t['view'] =='1') :?>
					<td>						
						<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>								
					</td>
					<?php endif;?>
				<?php endforeach;?>
				<td style="width:130px;">
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> GO</button></td>
			 </tr>
			<?php foreach ( $rowData as $i => $row ) : ?>
            <tr>
					<td width="50"> <?php echo ($i+1) ?> </td>
				 <?php foreach ( $tableGrid as $j => $field ) : ?>
					 <?php if($field['view'] =='1'): ?>
					 <td>
					 <?php 
							$conn = (isset($field['conn']) ? $field['conn'] : array() );
							echo AjaxHelpers::gridFormater($row->$field['field'], $row , $field['attribute'],$conn);
					?>	
					 </td>
					 <?php endif; ?>
				 <?php endforeach; ?>
				 <td>
					 <div class="btn-group">
						
						<?php //<ul  class="dropdown-menu  icons-left pull-right"> ?>
<a href="<?php echo base_url('activitylog/show/'.$row->id)?>"  class="tips "  title="view"><i class="fa  fa-search"></i> <?php echo lang('view'); ?> </a>
					 	<?php /*if($access['is_detail'] ==1) : ?>
						
						<?php endif; */?>				
						<?php //</ul> ?>
					</div>					 
					
					</td>
            </tr>

            <?php endforeach; ?>

        </tbody>

    </table>
	</div>
	</form>
	
	<?php echo $this->load->view('footer');?>
	</div>
</div>
</div>

<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#SximoTable').attr('action','<?php echo base_url("activitylog/multisearch");?>');
		$('#SximoTable').submit();
	});
	
});	
</script>
