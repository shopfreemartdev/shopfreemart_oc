<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="page-content-wrapper m-t">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="activitylogView"></div>			
		<div id="activitylogGrid" style="min-height: 500px;"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var displayactivitylogFilterPanel = Cookies.set('ti_displayactivitylogFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displayactivitylogFilterPanel = 'false';
		} else
		{
			displayactivitylogFilterPanel = 'true';
			$( '#activitylogFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displayactivitylogFilterPanel', displayactivitylogFilterPanel);

		reloadData( '#activitylog',"<?php echo base_url();?>activitylog/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displayactivitylogFilterPanel);
			if (displayactivitylogFilterPanel == null)
			{
				displayactivitylogFilterPanel = 'true';	
				Cookies.set('displayactivitylogFilterPanel', displayactivitylogFilterPanel);
			}
			console.log(displayactivitylogFilterPanel);				
			if (displayactivitylogFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#activitylog','<?php echo base_url();?>activitylog/data');	

		});
		
	
</script>

  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->