	<?php if($setting['form-method'] =='native') : ?>
	
	<div class="member-page-header">
	<?php $this->load->view('activitylog/pagehead');?>
	</div>
		
			
	
	<div class="sbox">

	<div class="sbox-title">  <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small><?php echo  $pageNote ;?></small> 

	<a href="javascript:void(0)" class="collapse-close pull-right" onclick="ajaxViewClose('#activitylog')"><i class="fa fa fa-times"></i></a>
	</h4>
	 </div>

	<div class="sbox-content"> 
	<?php endif;?>	


		 <form action="<?php echo base_url('activitylog/save/'.$row['id']); ?>" class='form-horizontal'  id="activitylogFormAjax"
		 parsley-validate='true' novalidate='true' method="post" enctype="multipart/form-data" > 
		 
<div class="col-md-12">
						<fieldset><legend> Activitylog</legend>
									
								  <div class="form-group  " >
									<label for="Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['id'];?>' name='id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="User Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('User Id', (isset($fields['user_id']['language'])? $fields['user_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['user_id'];?>' name='user_id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Activity" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Activity', (isset($fields['activity']['language'])? $fields['activity']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['activity'];?>' name='activity'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Date" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Date', (isset($fields['date']['language'])? $fields['date']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  
				<input type='text' class='form-control datetime' placeholder='' value='<?php echo $row['date'];?>' name='date'
				style='width:150px !important;'	   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Order Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Order Id', (isset($fields['order_id']['language'])? $fields['order_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['order_id'];?>' name='order_id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> 					
								  <div class="form-group  " >
									<label for="Transaction Id" class=" control-label col-md-4 text-left">
									<?php echo SiteHelpers::activeLang('Transaction Id', (isset($fields['transaction_id']['language'])? $fields['transaction_id']['language'] : array()))  ;?>				
									</label>
									<div class="col-md-8">
									  <input type='text' class='form-control' placeholder='' value='<?php echo $row['transaction_id'];?>' name='transaction_id'   Disabled />
									  <i> <small></small></i>
									 </div> 
								  </div> </fieldset>
			</div>
			
			
		
			<div style="clear:both"></div>	
				
 		<div class="toolbar-line text-center">		
			<input type="submit" name="submit" class="btn btn-primary btn-sm" value="<?php echo lang('Submit'); ?>" />
			<a href="javascript:void(0)" class="btn-sm btn btn-warning" data-dismiss="modal" aria-hidden="true" onclick="ajaxViewClose('#activitylog')"><?php echo lang('Cancel'); ?></a>
 		</div>
			  		
		</form>

	</div>	
		<?php foreach($subgrid as $md) : ?>
		<div  id="<?php echo  $md['module'] ;?>">
			<h4><i class="fa fa-table"></i> <?php echo  $md['title'] ;?></h4>
			<div id="<?php echo  $md['module'] ;?>View"></div>
			<div class="table-responsive">
				<div id="<?php echo  $md['module'] ;?>Grid"></div>
			</div>	
		</div>
		<?php endforeach;?>	
	
	<?php if($setting['form-method'] =='native'): ?>
		</div>	
	</div>	
	<?php endif;?>	
<script>
$(document).ready(function(){
<?php foreach($subgrid as $md) : ?>
	$.post( '<?php echo base_url($md['module'].'/detailview/form?md='.$md['master'].'+'.$md['master_key'].'+'.$md['module'].'+'.$md['key'].'+'.$id) ;?>' ,function( data ) {
		$( '#<?php echo $md['module'] ;?>Grid' ).html( data );
	});		
<?php endforeach ?>
});
</script>				 
<script type="text/javascript">
$(document).ready(function() { 
	 
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	//$('.markItUp').markItUp(mySettings );	

	var form = $('#activitylogFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});	
 	 
});

function showRequest()
{
	$('.formLoading').show();	
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		if(data.action =='submit')
		{
			ajaxViewClose('#activitylog');	
		}	
		
		ajaxFilter('#activitylog','activitylog/data');
		notyMessage(data.message);		
	} else {	
		var n = noty({				
			text: data.message,
			type: 'error',
			layout: 'topRight',
		});	
		return false;
	}	
}	
</script>		 


  <!--START OF CUSTOM CODE--><!--END OF CUSTOM CODE-->