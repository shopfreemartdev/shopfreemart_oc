<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activitylogmodel extends SB_Model 
{

	public $table = 'sfm_user_logs';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT sfm_user_logs.* FROM sfm_user_logs                          ";
	}
	public static function queryWhere(  ){
		
		return " WHERE sfm_user_logs.user_id = {currentuserid}                           ";
	}
	
	public static function queryGroup(){
		return "                                                       ";
	}
	
	public function getAdditionalCriteria($param){
		if (empty($param)) {
			return "";
		}

		return "";
		//return " sfm_uap_transactions.user_id = '".$param."'" ;
	}

	public function set_defaults($data)
	{
		return $data;
	}	

    public static function getTypefilter($filtertype)
    {
        if (!empty($filtertype)) {
            return ""; // AND st.description LIKE '$filtertype%' ";
        } else {
            return "";
        }

    }	
	public static function queryCountSelect(  ){
		return "  {sql_countselect}  ";
	}

// START OF CUSTOM CODE// END OF CUSTOM CODE

}
?>
