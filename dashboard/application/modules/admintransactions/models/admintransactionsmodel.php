<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admintransactionsmodel extends SB_Model 
{

	public $table = 'sfm_uap_transactions';
	public $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "   SELECT sfm_uap_transactions.* FROM sfm_uap_transactions   ";
	}
	public static function queryWhere(  ){
		
		return "  WHERE sfm_uap_transactions.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "   ";
	}
	
	public function getAdditionalCriteria($param){
		return "";
		//return " AND sfm_uap_transactions.user_id = '".$param."'" ;
	}

	public function set_defaults($data)
	{
		return $data;
	}	

    public static function getTypefilter($filtertype)
    {
        if (!empty($filtertype)) {
            return ""; // AND st.description LIKE '$filtertype%' ";
        } else {
            return "";
        }

    }	
	
}

?>
