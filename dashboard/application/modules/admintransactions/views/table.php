<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');?>
	
	<div class="member-page-header">
	<?php $this->load->view('admintransactions/pagehead');?>
	</div>
		
<?php echo $this->session->flashdata('message');?>
	
<div id="ajax"><?=$show_message?></div>

<?php usort($tableGrid, "SiteHelpers::_sort"); ?>

<?php if($setting['gridsettings']['gs_toolbar']) : ?>
<?php $this->load->view('admintransactions/toolbar');?>
<?php endif;?>

<?php if($setting['gridsettings']['gs_portlet']) : ?>
<div class="sbox">
	<div class="sbox-title">

		<h4 class="pull-left">
			<span>
			<i class="fa fa-table" style="font-size: 18px !important"></i>
			<?php echo $pageTitle ;?>&nbsp;
			<small>
				<?php echo $pageNote ;?>
			</small>
			</span>
		</h4>
		<div class="btn-group pull-right">				

			<a href="javascript:void(0)" class="" 
			onclick="reloadData('#admintransactions','admintransactions/data')"  title="Reload Data"><i class="fa fa-refresh" style="font-size: 17px; margin-top: 5px;" ></i></a>	
			<?php if($this->session->userdata('gid') ==1) : ?>	
					<a href="<?php echo ci_site_url('cms/module/rebuild/'.$pageModule.'?rurl='.$pageModule) ?>"  title="Rebuild">
					<i class="fa fa-cog" style="font-size: 17px; margin-top: 5px;" ></i>&nbsp;</a>				
					<a href="<?php echo ci_site_url('cms/module/config/'.$pageModule) ?>" title="Configuration">
					<i class="fa fa-wrench" style="font-size: 17px; margin-top: 5px;"></i>&nbsp;</a>
			<?php endif; ?>			
		</div>

	</div>
	<div class="sbox-content">
		<?php endif;?>
		<form
			action='<?php echo ci_site_url('admintransactions/destroy') ?>' class='form-horizontal' id ='admintransactionsForm' method="post" <?php echo $setting['gridsettings']['gs_css']; ?> >

			<div class="panel panel-default panel-table">
		
			<!-- gs_filterbar -->
			<?php if($setting['gridsettings']['gs_filterbar']) {
				echo $filterbar;
			} ?>

				<!-- Grid Table Row -->
				<div class="table-responsive">

						<table class="display dataTable table table-striped table-bordered table-hover table-checkable table-responsive datatable dataTable table-clients no-footer dtr-inline table-hover " id="admintransactionsTable" >
								<?php if(empty($rowData)) : ?>
									<tr>
										<div class="alert alert-warning">
											<center>
											<h4><?=lang('no_records_found')?></h4>
											</center>
										</div>
									</tr>

								<?php else: ?>	
						
						
							<thead>
								<tr>
									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<th>
										No
									</th>
									<?php } ?>

									<?php if($setting['gridsettings']['gs_checkbox']) { ?>
									<th>
										<input type="checkbox" class="checkall" />
									</th>
									<?php } ?>

									<?php if($setting['view-method']=='expand') { ?>
									<th>
									</th> <?php } ?>
									<?php foreach ($tableGrid as $k => $t) : ?>
									<?php if($t['view'] =='1'): ?>
									<th>
										<?php echo SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array()))  ?>
									</th>
									<?php endif; ?>
									<?php endforeach; ?>

									<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
									<th width="70">
										Action
									</th>
									<?php } ?>

								</tr>
							</thead>

							<tbody>
							
							<?php if($access['is_add'] =='1' && $setting['inline']=='true'): ?>
																																								<tr id="form-0" >

									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<td>
										#
									</td>
									<?php } ?>

									<?php if($setting['gridsettings']['gs_checkbox']) { ?>
									<th>
									</th>
									<?php } ?>

									<?php if($setting['view-method']=='expand') { ?>
									<td>
									</td> <?php } ?>
									<?php foreach ($tableGrid as $t) :
									if($t['view'] =='1') : ?>
									<td data-form="<?php echo $t['field'];?>" data-form-type="<?php echo  AjaxHelpers::inlineFormType($t['field'],$tableForm);?>">
										<?php echo SiteHelpers::transForm($t['field'] , $tableForm) ;?>
									</td>
									<?php endif;
									endforeach ?>

									<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
									<td style="width:50px;">

										<button type="button"  class=" btn btn-xs btn-info" rel="#<?php echo $pageModule ;?>Form"
						onclick="ajaxInlineSave('#<?php echo $pageModule ;?>','<?php echo ci_site_url('admintransactions/quicksave') ;?>','<?php echo ci_site_url('admintransactions/data?md=') ;?>')">
											<i class="fa fa-save">
											</i>
										</button>
									</td>
									
								
									<?php } ?>

								</tr>
									<?php endif;?>

									<?php foreach ( $rowData as $i => $row ) :
									$id = $row->id;
									?>
																																												<>

									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<td width="50">
										<?php echo ($i+1+$page) ?>
									</td>
									<?php } ?>

									<?php if($setting['gridsettings']['gs_checkbox']) { ?>
									<td width="50">
										<input type="checkbox" class="ids" name="id[]" value="<?php echo $row->id ?>" />
									</td>
									<?php } ?>

									<?php if($setting['view-method']=='expand'): ?>
									<td>
										<a href="javascript:void(0)" class="expandable" rel="#row-<?php echo $row->id ;?>" data-url="<?php echo ci_site_url('admintransactions/show/'.$id) ;?>">
											<i class="fa fa-plus " >
											</i>
										</a>
									</td>
									<?php endif;?>
									<?php foreach ( $tableGrid as $j => $field ) : ?>
									<?php if($field['view'] =='1'):
									$conn = (isset($field['conn']) ? $field['conn'] : array() );
									$value = AjaxHelpers::gridFormater($row->$field['field'], $row , $field['attribute'],$conn);

									?>
									<td align="<?php echo $field['align'];?>" data-values="<?php echo $row->$field['field'] ;?>" data-field="<?php echo  $field['field'] ;?>" data-format="<?php echo htmlentities($value);?>">
										<?php echo  $value;?>
									</td>
									<?php endif; ?>
									<?php endforeach; ?>

									<?php if($setting['gridsettings']['gs_buttonpanel']) { ?>
									<td data-values="action" data-key="<?php echo $row->id ;?> " >
										<?php if($setting['gridsettings']['gs_inlinebuttonpanel'])
										echo AjaxHelpers::buttonActionButtonInline('admintransactions',$access,$row->id ,$setting);
										else
										echo AjaxHelpers::buttonAction('admintransactions',$access,$row->id ,$setting);
										?>
									</td>
									<?php } ?>

								</tr>
									<?php if($setting['view-method']=='expand'): ?>
																						<tr style="display:none" class="expanded" id="row-<?php echo $row->id;?>">

									<?php if($setting['gridsettings']['gs_counter']) { ?>
									<td class="number">
									</td>
									<?php } ?>

									<td>
									</td>
									<td>
									</td>
									<td colspan="<?php echo $colspan;?>" class="data">
									</td>
									<td>
									</td>
								</tr>
									<?php endif;?>
									<?php endforeach; ?>

							</tbody>
								<?php endif;?>							
							
						</table>

				</div>

				<?php if($setting['gridsettings']['gs_footer']) : ?>
				<?php $this->load->view('ajaxfooter'); ?>
			<?php endif; ?>				
			
			</div>
		</form>
		
		<?php if($setting['gridsettings']['gs_portlet']) : ?>
	</div>
</div>

<?php endif;?>

<?php if($setting['inline'] =='true')  $this->load->view('cms/module/utility/inlinegrid') ;?>
<?php if($setting['view-method'] =='expand')  $this->load->view('cms/module/utility/extendgrid') ;?>





<script>
	var gridtable;
	$(document).ready(function()
	{
		$('.tips').tooltip();

		gridtable = $('#admintransactionsTable').DataTable(
			{
				"processing": true,
				"serverSide": true,
				"autoWidth": false,
				"lengthMenu": [[10, 25, 50, 100, 0], [10, 25, 50, 100, "All"]],
				"ajax":
				{
					"url": "<?php echo base_url();?>admintransactions/listdata",
					"type": "POST",
					"data": function ( data )
					{
						data.filtertype = $('#filtertype').val();
					},
					"dataSrc": function ( json )
					{
						$('#total_all_pages').html(json.total_all_pages);
						$('#total_current_page').html(json.total_current_page);
						for ( var i=0, ien=json.data.length ; i<ien ; i++ )
						{
							json.data[i][0] = '<a href=#>'+json.data[i][0]+'</a>';
						}
						return json.data;
					}
				}
				// initComplete: function () {
				// this.api().columns().every( function () {
				//     var column = this;
				//     if (column[0] == 2)
				//     {
				//     var select = $('filtertype')
				//         .appendTo( $(column.footer()).empty() )
				//      // column.data().unique().sort().each( function ( d, j ) {
				//      //     typeselect.append( '<option value="'+d+'">'+d+'</option>' )
				//      // } );

				// 	}
				// } );
				//}
			});

		var typeselect = $('#filtertype').on( 'change', function ()
			{
				var val = $(this).val();
				gridtable.ajax.reload(null,false);
				//gridtable.columns(2).search(val ? '^'+val+'$' : '',true, false).draw();
			} 
			);


		$('input[type="checkbox"],input[type="radio"]').iCheck(
			{
				checkboxClass: 'icheckbox_square-green',
				radioClass: 'iradio_square-green',
			});
		$('#admintransactionsTable .checkall').on('ifChecked',function()
			{
				$('#admintransactionsTable input[type="checkbox"]').iCheck('check');
			});
		$('#admintransactionsTable .checkall').on('ifUnchecked',function()
			{
				$('#admintransactionsTable input[type="checkbox"]').iCheck('uncheck');
			});
		$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
		$('#admintransactionsPaginate .pagination li a').click(function()
			{
				var url = $(this).attr('href');
				if(url != '')
				{
					reloadData('#admintransactions',url);
				}
				return false ;
			});
		loadControlvalues();

	});

	function filterList()
	{
		$('#filter-form').submit();
	}
	function dosearchbutton_click()
	{
		dofilterList();
	}
	function dofilterbutton_click()
	{
		$('#panel-filter').toggle('slow');
	}


</script>

