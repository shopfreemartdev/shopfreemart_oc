<div class="page-content row">
		
	<!-- Begin Content -->
	<div class="page-content-wrapper m-t">

		<div class="resultData"></div>
		<div class="ajaxLoading"></div>
		<div id="admintransactionsView"></div>			
		<div id="admintransactionsGrid"></div>
	</div>	
	<!-- End Content -->  
</div>	
<script>


	var displayadmintransactionsFilterPanel = Cookies.set('ti_displayadmintransactionsFilterPanel');


	function filterList()
	{
		$('#filter-form').submit();
	}


	function dofilterList()
	{
		var attr = '';
		
		attr += 'rows='+$('#rows').val()+'&';
		attr += 'sort='+$('#sort').val()+'&';
		attr += 'order='+$('#order').val()+'&';
		
		attr += 'search=search:'+$('#search').val()+'|';
		if ($('#panel-filter').css("display") == 'none')
		{
			displayadmintransactionsFilterPanel = 'false';
		} else
		{
			displayadmintransactionsFilterPanel = 'true';
			$( '#admintransactionsFilter :input').each(function() {
				if(this.value !=='' && this.name !='_token'  && this.name !='search') { attr += this.name+':'+this.value+'|'; }
			});
		}		
		Cookies.set('ti_displayadmintransactionsFilterPanel', displayadmintransactionsFilterPanel);

		reloadData( '#admintransactions',"<?php echo base_url();?>admintransactions/data?"+attr);	
	}
	

	function dosearchbutton_click()
	{
		dofilterList();

	}
	
	function loadControlvalues()
	{
			console.log(displayadmintransactionsFilterPanel);
			if (displayadmintransactionsFilterPanel == null)
			{
				displayadmintransactionsFilterPanel = 'true';	
				Cookies.set('displayadmintransactionsFilterPanel', displayadmintransactionsFilterPanel);
			}
			console.log(displayadmintransactionsFilterPanel);				
			if (displayadmintransactionsFilterPanel == 'true')
			{
				$('#panel-filter').attr("style", "display:block");
			} 
			else
			{
				$('#panel-filter').attr("style", "display:none");
			}

	}


	$(document).ready(function()
		{
				reloadData('#admintransactions','<?php echo base_url();?>admintransactions/data');	

		});
		
	
</script>
