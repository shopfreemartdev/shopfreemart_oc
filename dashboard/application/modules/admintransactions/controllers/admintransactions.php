<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admintransactions extends CRM_Controller 
{

	public $module 		= 'admintransactions';
	public $per_page	= '10';

	function __construct() {
		parent::__construct();
		
		$this->load->model('admintransactionsmodel');
		$this->model = $this->admintransactionsmodel;
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);	
		$this->data = array_merge( $this->data, array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'	=> 'admintransactions',
			'pageUrl'			=>  ci_site_url('admintransactions'),
		));
		
		//if(!$this->session->userdata('logged_in')) redirect('user/login',301);
		
	}

	public function index()
	{

		$this->render_view('admintransactions/index');		
    	
	}		
	
	private function load_data($parameter='')
	{
		if($this->access['is_view'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }		
		// Filter sort and order for query 
		$sort = (($this->input->get('sort', true) != null) ? $this->input->get('sort', true): $this->info['setting']['orderby'] );
		$order = (($this->input->get('order', true) != null) ?$this->input->get('order', true) : $this->info['setting']['ordertype'] );
		$rowperpage = (($this->input->get('rows', true) != null) ?  $this->input->get('rows', true) : $this->info['setting']['perpage']);
		if ($rowperpage == 'All') $rowperpage = 0;
		$filter = (($this->input->get('search', true) != null) ? $this->input->get('search', true) : '');

		//$sort = ($this->input->get('sort', true) !='' ? $this->input->get('sort', true) :  $this->info['setting']['orderby']); 
		//$order = ($this->input->get('order', true) !='' ? $this->input->get('order', true) :  $this->info['setting']['ordertype']);
		
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
		// End Filter Search for query 
		
		$page = max(1, (int) $this->input->get('page', 1));
		$params = array(
			'page'		=> $page ,
			'limit'		=> $rowperpage ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		
		$pagination = $this->paginatorajax( array(
			'total_rows' => $results['total'] ,
			'per_page'	 => $params['limit']
		),$parameter);
		$this->data['pagination']	= $pagination;
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration
		$this->data['param']		= $params; 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= SiteHelpers::viewColSpan($this->info['config']['grid']);	
		$this->data['setting'] 		= $this->info['setting'];	
				
		$this->data['total_rows'] = $results['total'];
		$this->data['page_from'] = (int)$page;
		$this->data['page_to'] =  (int)($page)  + (int)$params['limit'];
		
		// Group users permission
		$this->data['access']		= $this->access;
		// Render into template
	}
	
	public function tabindex($criteria = null)
	{
		$this->data['tabcriteria']		=  $criteria;
		$this->data['access']		= $this->access;
		$this->data['tab']		= "admintransactions";			
    	$this->render_tabview('admintransactions/tabindex'); 
	}

	function tabdata() 
	{
		$this->load_data('tabdata');
		$this->render_content('admintransactions/tabtable');
	}
	
	function data() 
	{
		$this->load_data();		
		$this->render_content('admintransactions/table');
	}
	
	function show( $id = null) 
	{
		if($this->access['is_detail'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable($this->info['table']); 
		}

		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		$this->data['fields'] =  AjaxHelpers::fieldLang($this->info['config']['grid']);
		$this->data['id'] = $id;
		$this->data['setting'] 		= $this->info['setting'];
		$this->render_content('admintransactions/view');
	}
  
	function add( $id = null ) 
	{

		if($id =='')
			if($this->access['is_add'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }

		if($id !='')
			if($this->access['is_edit'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }

		$row = $this->model->getRow( $id );
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable($this->info['table']); 
		}
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		$this->data['id'] = $id;
		$this->data['setting'] 		= $this->info['setting'];
		$this->render_content('admintransactions/form');
	
	}
	
	function save() {
		
		$rules = $this->validateForm();

		$this->form_validation->set_rules( $rules );
		if( $this->form_validation->run() )
		{
			$data = $this->validatePost();
			$ID = $this->model->insertRow($data , $this->input->get_post( 'id' , true ));
			// Input logs
			if( $this->input->get( 'id' , true ) =='')
			{
				$this->inputLogs("New Entry row with ID : $ID  , Has Been Save Successfull");
			} else {
				$this->inputLogs(" ID : $ID  , Has Been Changed Successfull");
			}
			// Redirect after save	
			if($this->input->post('apply'))
			{ 
				$action = 'apply';
			} else {
				$action = 'submit';	

			}
				header('content-type:application/json');	
				echo json_encode(array(
					'status'	=>'success',
					'message'	=> ' Data has been saved succesfuly !',
					'action'	=>  $action
					));	
			
		} else {
			header('content-type:application/json');
			echo json_encode(array(
					'message'	=> validation_errors('<li>', '</li>'),
					'status'	=> 'error'
					));			
			
		}
	}

	function destroy()
	{
		if($this->access['is_remove'] ==0) { echo SiteHelpers::alert('error',' You are not allowed to view this page'); die; }
		if(!is_null($this->input->post('id')))
		{
			$this->model->destroy($this->input->post( 'id' , true ));
			$this->inputLogs("ID : ".implode(",",$this->input->post( 'id' , true ))."  , Has Been Removed Successfull");
			header('content-type:application/json');
			echo json_encode(array(
				'status'=>'success',
				'message'=> SiteHelpers::alert('success','Data Has Been Removed Successfull')
			));
		} else {
			header('content-type:application/json');
			echo json_encode(array(
				'status'=>'error',
				'message'=> 'Ops , Something Went Wrong !'
			));

		} 	
	}


    function listdata($criteria_filter='all')
    {
        $output = $this->load_listdata($criteria_filter);
        echo json_encode($output);
    }

    function load_listdata($criteria_filter='all')
    {

        if ($this->access['is_view'] == 0) {echo SiteHelpers::alert('error', ' You are not allowed to view this page');die;}

        // Filter sort and order for query
        $start = ((isset($_POST['start'])) ? $_POST['start'] : 0);
        $sortcol = ((isset($_POST['order']['0']['column'])) ? $_POST['order']['0']['column'] : 0);
        $sortdir = ((isset($_POST['order']['0']['dir'])) ? $_POST['order']['0']['dir'] : 'asc');
        $length = ((isset($_POST['length'])) ? $_POST['length'] : 10);
        $sort = (($sortcol != null) ? $sortcol : $this->info['setting']['orderby']);
        $order = (($sortdir != null) ? $sortdir : $this->info['setting']['ordertype']);
        $rowperpage = (($length != null) ? $length : $this->info['setting']['perpage']);

        if ($rowperpage == 'All') {
            $rowperpage = 0;
        }

        $filter = (!is_null($this->input->get('search', true)) ? $this->buildSearch() : '');
        $filtertype = ((isset($_POST['filtertype'])) ? $_POST['filtertype'] : '');
        $filter .= $this->model->getTypefilter($filtertype);
        $search = ((isset($_POST['search']['value'])) ? $_POST['search']['value'] : '');
        $search = $this->model->getAdditionalCriteria($search);
       	$sort = 'date_created '.$order.',abs(id) '.$order;
       	$order = '';
        $page = max(1, (int) $start);

        $params = array(
            'page'   => $page,
            'limit'  => $rowperpage,
            'sort'   => $sort,
            'order'  => $order,
            'params' => $filter . $search,
            'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0),
        );

        // Get Query
        $this->model->filter=$criteria_filter;
        $results = $this->model->getRows($params);

        // Build pagination setting
        $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
        $this->data['rowData'] = $results['rows'];

        // Row grid Number
        $this->data['i'] = ($page * $params['limit']) - $params['limit'];

        // Grid Configuration
        $this->data['param'] = $params;
        $this->data['tableGrid'] = $this->info['config']['grid'];
        $this->data['tableForm'] = $this->info['config']['forms'];
        $this->data['colspan'] = SiteHelpers::viewColSpan($this->info['config']['grid']);
        $this->data['setting'] = $this->info['setting'];
        $this->data['total_rows'] = $results['total'];
        $this->data['page_from'] = (int) $page;
        $this->data['page_to'] = (int) ($page) + (int) $params['limit'];

        // Group users permission
        $this->data['access'] = $this->access;

        $output = array(
            "draw"                     => ((isset($_POST['draw'])) ? $_POST['draw'] : 1),
            "recordsTotal"             => $results['total'],
            "recordsFiltered"          => $results['total'],
            "data"                     => array(),
        );

        foreach ($results['rows'] as $row_key => $row_val) {
            $col_array = array();

            foreach ($row_val as $col_key => $col_val) {
                $col_array[] = $col_val;
            }
            $output['data'][] = $col_array;
        }
        return $output;
    }

}
