<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgget_uriniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter CAPTCHA Helper
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/xml_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Create CAPTCHA
 *
 * @access	public
 * @param	array	array of data for the CAPTCHA
 * @param	string	path to create the image in
 * @param	string	URL to the CAPTCHA image folder
 * @param	string	server path to font
 * @return	string
 */
if ( ! function_exists('create_captcha'))
{
	function create_captcha($data = '', $img_path = '', $img_url = '', $font_path = '')
	{
		$defaults = array('word' => '', 'img_path' => '', 'img_url' => '', 'img_width' => '100', 'img_height' => '25', 'font_path' => '', 'expiration' => 7200, 'word_length'=>5, 'type'=>'number' );

		foreach ($defaults as $key => $val)
		{
			if ( ! is_array($data))
			{
				if ( ! isset($$key) OR $$key == '')
				{
					$$key = $val;
				}
			}
			else
			{
				$$key = ( ! isset($data[$key])) ? $val : $data[$key];
			}
		}

		if ($img_path == '' OR $img_url == '')
		{
			return FALSE;
		}

		if ( ! @is_dir($img_path))
		{
			return FALSE;
		}

		if ( ! is_writable($img_path))
		{
			return FALSE;
		}

		if ( ! extension_loaded('gd'))
		{
			return FALSE;
		}

		// -----------------------------------
		// Remove old images
		// -----------------------------------

		list($usec, $sec) = explode(" ", microtime());
		$now = ((float)$usec + (float)$sec);

		$current_dir = @opendir($img_path);

		while ($filename = @readdir($current_dir))
		{
			if ($filename != "." and $filename != ".." and $filename != "index.html")
			{
				$name = str_replace(".jpg", "", $filename);

				if (($name + $expiration) < $now)
				{
					@unlink($img_path.$filename);
				}
			}
		}

		@closedir($current_dir);

		// -----------------------------------
		// Do we have a "word" yet?
		// -----------------------------------

	   if ($word == '')
	   {
      switch ($type) {
        case 'alphanum':
            $pool = '13456789abcdehkmnopqrstuvwxyzABCDEFGHJKLMNPRSUVWXYZ';
          break;

        case 'number' :
        default:
            $pool = '1234567890';
          break;

        case 'alpha' :
            $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
          break;

        case 'capital' :
            $pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
          break;
      }

			$str = '';
			for ($i = 0; $i < $word_length; $i++)
			{
				$str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
			}

			$word = $str;
	   }

		// -----------------------------------
		// Determine angle and position
		// -----------------------------------

		$length	= strlen($word);
		$angle	= ($length >= 6) ? rand(-($length-6), ($length-6)) : 0;
		$x_axis	= rand(6, (360/$length)-16);
		$y_axis = ($angle >= 0 ) ? rand($img_height, $img_width) : rand(6, $img_height);

		// -----------------------------------
		// Create image
		// -----------------------------------

		// PHP.net recommends imagecreatetruecolor(), but it isn't always available
		if (function_exists('imagecreatetruecolor'))
		{
			$im = imagecreatetruecolor($img_width, $img_height);
		}
		else
		{
			$im = imagecreate($img_width, $img_height);
		}

		// -----------------------------------
		//  Assign colors
		// -----------------------------------

		$bg_color		= imagecolorallocate ($im, 255, 255, 255);
		$border_color	= imagecolorallocate ($im, 102, 102, 102);
		$text_color		= imagecolorallocate ($im, 255, 32, 32);
		$grid_color		= imagecolorallocate($im, 182, 182, 182);
		$shadow_color	= imagecolorallocate($im, 240, 240, 240);

		// -----------------------------------
		//  Create the rectangle
		// -----------------------------------

		ImageFilledRectangle($im, 0, 0, $img_width, $img_height, $bg_color);

		// -----------------------------------
		//  Create the spiral pattern
		// -----------------------------------

		$theta		= 1;
		$thetac		= 7;
		$radius		= 16;
		$circles	= 20;
		$points		= 32;

		for ($i = 0; $i < ($circles * $points) - 1; $i++)
		{
			$theta = $theta + $thetac;
			$rad = $radius * ($i / $points );
			$x = ($rad * cos($theta)) + $x_axis;
			$y = ($rad * sin($theta)) + $y_axis;
			$theta = $theta + $thetac;
			$rad1 = $radius * (($i + 1) / $points);
			$x1 = ($rad1 * cos($theta)) + $x_axis;
			$y1 = ($rad1 * sin($theta )) + $y_axis;
			imageline($im, $x, $y, $x1, $y1, $grid_color);
			$theta = $theta - $thetac;
		}

		// -----------------------------------
		//  Write the text
		// -----------------------------------

		$use_font = ($font_path != '' AND file_exists($font_path) AND function_exists('imagettftext')) ? TRUE : FALSE;

		if ($use_font == FALSE)
		{
			$font_size = 5;
			$x = rand(0, $img_width/($length/2));
			$y = 0;
		}
		else
		{
			$font_size	= 14;
			$x = rand(10, $img_width/($length/1.5));
			$y = $font_size+2;
		}

		for ($i = 0; $i < strlen($word); $i++)
		{
			if ($use_font == FALSE)
			{
				$y = rand($img_height/10 , $img_height/3);
				imagestring($im, $font_size, $x, $y, substr($word, $i, 1), $text_color);
				$x += ($font_size*2);
			}
			else
			{
				$y = rand($img_height/10, $img_height/3);
				imagettftext($im, $font_size, $angle, $x, $y, $text_color, $font_path, substr($word, $i, 1));
				$x += $font_size;
			}
		}


		// -----------------------------------
		//  Create the border
		// -----------------------------------

		imagerectangle($im, 0, 0, $img_width-1, $img_height-1, $border_color);

		// -----------------------------------
		//  Generate the image
		// -----------------------------------

		$img_name = $now.'.jpg';

		ImageJPEG($im, $img_path.$img_name);

		$img = "<img src=\"$img_url$img_name\" width=\"$img_width\" height=\"$img_height\" style=\"border:0;\" alt=\" \" />";

		ImageDestroy($im);

		return array('word' => $word, 'time' => $now, 'image' => $img);
	}
}



/**
 * prepare uri
 *
 * @param string $uri
 * @return full url
 */
if (!function_exists('get_uri')) {

    function get_uri($uri = "") {
        $ci = get_instance();
        $index_page = $ci->config->item('index_page');
        $uri = get_uri_route() . $uri;
        return trim_slashes(base_url($uri));
    }

}



function assets_url($uri = '', $protocol = NULL)
{
	return base_url($uri,$protocol )."assets/";
}


if (!function_exists('get_layout_mainfile')) {

	function get_layout_mainfile()
	{
		if (isset($_GET['th']))
		{
			$theme = $_GET['th'];
			if ($theme == 'member')
			{
				 return 'themes/frontend/';
			}
		}
		return 'layout/';
	}
}
if (!function_exists('get_uri_route')) {

    function get_uri_route() {
   return;
        if (isset($_GET['t']))
        {
            $uri = $_GET['t'];
        }
		$uri = MEMBERS_ROUTE;
        return $uri;

    	if (get_current_login_user()->is_admin)
    	{
		  $uri = ADMIN_ROUTE;
		} else if(get_current_login_user()->is_staff)
		{
    		$uri = STAFF_ROUTE;
		} else if(get_current_login_user()->is_member)
		{
			$uri = MEMBERS_ROUTE;
		} else
		{
			$uri = "";
		}
        if (isset($_GET['t']))
        {
            $uri = $_GET['t'];
        }


        return $uri;
    }

}
/**
 * use this to print file path
 *
 * @param string $uri
 * @return full url of the given file path
 */
if (!function_exists('get_file_uri')) {

    function get_file_uri($uri = "") {
        return  trim_slashes(base_url($uri));
    }

}

/**
 * get the url of user avatar
 *
 * @param string $image_name
 * @return url of the avatar of given image reference
 */
if (!function_exists('get_avatar')) {

    function get_avatar($image_name = "") {
        if ($image_name==="system_bot") {
            return base_url()."assets/images/avatar-bot.jpg";
        } else if ($image_name) {
            return base_url().get_setting("profile_image_path").'/'.$image_name;
        } else {
            return base_url()."assets/images/avatar.jpg";
        }
    }

}

/**
 * link the css files
 *
 * @param array $array
 * @return print css links
 */
if (!function_exists('load_css')) {

    function load_css(array $array) {
        foreach ($array as $uri) {
            echo "<link rel='stylesheet' type='text/css' href='" . base_url(). $uri. "' />";
        }
    }

}


/**
 * link the javascript files
 *
 * @param array $array
 * @return print js links
 */
if (!function_exists('load_js')) {

    function load_js(array $array) {
        foreach ($array as $uri) {
            echo "<script type='text/javascript'  src='" . base_url() . $uri. "' async></script>";
        }
    }

}

/**
 * check the array key and return the value
 *
 * @param array $array
 * @return extract array value safely
 */
if (!function_exists('get_array_value')) {

    function get_array_value(array $array, $key) {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
    }

}

/**
 * prepare a anchor tag for any js request
 *
 * @param string $title
 * @param array $attributes
 * @return html link of anchor tag
 */
if (!function_exists('js_anchor')) {

    function js_anchor($title = '', $attributes = '') {
        $title = (string) $title;
        $html_attributes = "";

        if (is_array($attributes)) {
            foreach ($attributes as $key => $value) {
                $html_attributes .= ' ' . $key . '="' . $value . '"';
            }
        }

        return '<a href="#"' . $html_attributes . '>' . $title . '</a>';
    }

}


/**
 * prepare a anchor tag for modal
 *
 * @param string $url
 * @param string $title
 * @param array $attributes
 * @return html link of anchor tag
 */
if (!function_exists('modal_anchor')) {

    function modal_anchor($url, $title = '', $attributes = '') {
        $attributes["data-act"] = "ajax-modal";
        if (get_array_value($attributes, "data-modal-title")) {
            $attributes["data-title"] = get_array_value($attributes, "data-modal-title");
        } else {
            $attributes["data-title"] = get_array_value($attributes, "title");
        }
        $attributes["data-action-url"] = $url;

        return js_anchor($title, $attributes);
    }

}

/**
 * prepare a anchor tag for ajax request
 *
 * @param string $url
 * @param string $title
 * @param array $attributes
 * @return html link of anchor tag
 */
if (!function_exists('ajax_anchor')) {

    function ajax_anchor($url, $title = '', $attributes = '') {
        $attributes["data-act"] = "ajax-request";
        $attributes["data-action-url"] = $url;
        return js_anchor($title, $attributes);
    }

}

/**
 * get the selected menu
 *
 * @param string $url
 * @param array $submenu
 * @return string "active" indecating the active page
 */
if (!function_exists('active_menu')) {

    function active_menu($menu = "", $submenu = array()) {
        $ci = & get_instance();
        $controller_name = strtolower(get_class($ci));

        //compare with controller name. if not found, check in submenu values
        if ($menu === $controller_name) {
            return "active";
        } else if (count($submenu)) {
            foreach ($submenu as $sub_menu) {
                if ($sub_menu['url'] === $controller_name) {
                    return "active";
                }
            }
        }
    }

}

/**
 * get the selected submenu
 *
 * @param string $submenu
 * @param boolean $is_controller
 * @return string "active" indecating the active sub page
 */
if (!function_exists('active_submenu')) {

    function active_submenu($submenu = "", $is_controller = false) {
        $ci = & get_instance();
        //if submenu is a controller then compare with controller name, otherwise compare with method name
        if ($is_controller && $submenu === strtolower(get_class($ci))) {
            return "active";
        } else if ($submenu === strtolower($ci->router->method)) {
            return "active";
        }
    }

}

/**
 * get the defined config value by a key
 * @param string $key
 * @return config value
 */
if (!function_exists('get_setting')) {

    function get_setting($key = "") {
        $ci = get_instance();
        $u = $ci->config->item($key);
        return $u;
    }

}

function save_setting($setting_name, $setting_value) {
	$ci = & get_instance();
	$ci->config->set_item($setting_name,$setting_value);
}

/**
 * check if a string starts with a specified sting
 *
 * @param string $string
 * @param string $needle
 * @return true/false
 */
if (!function_exists('starts_with')) {

    function starts_with($string, $needle) {
        $string = $string;
        return $needle === "" || strrpos($string, $needle, -strlen($string)) !== false;
    }

}

/**
 * check if a string ends with a specified sting
 *
 * @param string $string
 * @param string $needle
 * @return true/false
 */
if (!function_exists('ends_with')) {

    function ends_with($string, $needle) {
        return $needle === "" || (($temp = strlen($string) - strlen($string)) >= 0 && strpos($string, $needle, $temp) !== false);
    }

}

/**
 * create a encoded id for sequrity pupose
 *
 * @param string $id
 * @param string $salt
 * @return endoded value
 */
if (!function_exists('encode_id')) {

    function encode_id($id, $salt) {
        $ci = get_instance();
        $id = $ci->encrypt->encode($id . $salt);
        $id = str_replace("=", "~", $id);
        $id = str_replace("+", "_", $id);
        $id = str_replace("/", "-", $id);
        return $id;
    }

}


/**
 * decode the id which made by encode_id()
 *
 * @param string $id
 * @param string $salt
 * @return decoded value
 */
if (!function_exists('decode_id')) {

    function decode_id($id, $salt) {
        $ci = get_instance();
        $id = str_replace("_", "+", $id);
        $id = str_replace("~", "=", $id);
        $id = str_replace("-", "/", $id);
        $id = $ci->encrypt->decode($id);
        if ($id && strpos($id, $salt) !== false) {
            return str_replace($salt, "", $id);
        }
    }

}

if (!function_exists('echo_uri')) {

    function echo_uri($uri = "") {
    	$uri = get_uri_route() . $uri;
		echo trim_slashes(base_url().$uri);
    }

}
/**
 * decode html data which submited using a encode method of encodeAjaxPostData() function
 *
 * @param string $html
 * @return htmle
 */
if (!function_exists('decode_ajax_post_data')) {

    function decode_ajax_post_data($html) {
        $html = str_replace("~", "=", $html);
        $html = str_replace("^", "&", $html);
        return $html;
    }

}

/**
 * check if fields has any value or not. and generate a error message for null value
 *
 * @param array $fields
 * @return throw error for bad value
 */
if (!function_exists('check_required_hidden_fields')) {

    function check_required_hidden_fields($fields = array()) {
        $has_error = false;
        foreach ($fields as $field) {
            if (!$field) {
                $has_error = true;
            }
        }
        if ($has_error) {
            echo json_encode(array("success" => false, 'message' => lang('something_went_wrong')));
            exit();
        }
    }

}

/**
 * convert simple link text to clickable link
 * @param string $text
 * @return html link
 */
if (!function_exists('link_it')) {

    function link_it($text) {
        return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a href="$1" target="_blank">$1</a>', $text);
    }

}



/**
 * send mail
 *
 * @param string $to
 * @param string $subject
 * @param string $message
 * @param array $optoins
 * @return true/false
 */
if (!function_exists('send_app_mail')) {

    function send_app_mail($to, $subject, $message, $optoins = array()) {
        $email_config = Array(
            'charset' => 'utf-8',
            'mailtype' => 'html'
        );

        //check mail sending method from settings
        if (get_setting("email_protocol") === "smtp") {
            $email_config["protocol"] = "smtp";
            $email_config["smtp_host"] = get_setting("email_smtp_host");
            $email_config["smtp_port"] = get_setting("email_smtp_port");
            $email_config["smtp_user"] = get_setting("email_smtp_user");
            $email_config["smtp_pass"] = get_setting("email_smtp_pass");
            $email_config["smtp_crypto"] = get_setting("email_smtp_security_type");
            if (!$email_config["smtp_crypto"]) {
                $email_config["smtp_crypto"] = "tls";
            }
        }

        $ci = get_instance();
        $ci->load->library('email', $email_config);
        $ci->email->clear();
        $ci->email->set_newline("\r\n");
        $ci->email->from(get_setting("email_sent_from_address"), get_setting("email_sent_from_name"));
        $ci->email->to($to);
        $ci->email->subject($subject);
        $ci->email->message($message);

        //add attachment
        $attachments = get_array_value($optoins, "attachments");
        if (is_array($attachments)) {
            foreach ($attachments as $value) {
                $ci->email->attach(trim($value));
            }
        }

        //check cc
        $cc = get_array_value($optoins, "cc");
        if ($cc) {
            $ci->email->cc($cc);
        }

        //check bcc
        $bcc = get_array_value($optoins, "bcc");
        if ($bcc) {
            $ci->email->bcc($bcc);
        }

        //send email
        if ($ci->email->send()) {
            return true;
        } else {
            //show error message in none production version
            if (ENVIRONMENT !== 'production') {
                show_error($ci->email->print_debugger());
            }
            return false;
        }
    }

}


/**
 * get users ip address
 *
 * @return ip
 */
if (!function_exists('get_real_ip')) {

    function get_real_ip() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}


/**
 * convert string to url
 *
 * @param string $address
 * @return url
 */
if (!function_exists('to_url')) {

    function to_url($address = "") {
        if (strpos($address, 'http://') === false && strpos($address, 'https://') === false) {
            $address = "http://" . $address;
        }
        return $address;
    }

}

/**
 * validate post data using the codeigniter's form validation method
 *
 * @param string $address
 * @return throw error if foind any inconsistancy
 */
if (!function_exists('validate_submitted_data')) {

    function validate_submitted_data($fields = array()) {
        $ci = get_instance();
        foreach ($fields as $field_name => $requirement) {
            $ci->form_validation->set_rules($field_name, $field_name, $requirement);
        }

        if ($ci->form_validation->run() == FALSE) {
            if (ENVIRONMENT === 'production') {
                $message = lang('something_went_wrong');
            } else {
                $message = validation_errors();
            }
            echo json_encode(array("success" => false, 'message' => $message));
            exit();
        }
    }

}

/**
 * team members profile anchor. only clickable to team members
 * client's will see a none clickable link
 *
 * @param string $id
 * @param string $name
 * @param array $attributes
 * @return html link
 */
if (!function_exists('get_team_member_profile_link')) {

    function get_team_member_profile_link($id = 0, $name = "", $attributes = array()) {
        $ci = get_instance();
        if (get_current_login_user()->user_type === "staff") {
            return anchor("team_members/view/" . $id, $name, $attributes);
        } else {
            return js_anchor($name, $attributes);
        }
    }

}


/**
 * team members profile anchor. only clickable to team members
 * client's will see a none clickable link
 *
 * @param string $id
 * @param string $name
 * @param array $attributes
 * @return html link
 */
if (!function_exists('get_client_contact_profile_link')) {

    function get_client_contact_profile_link($id = 0, $name = "", $attributes = array()) {
        return anchor("clients/contact_profile/" . $id, $name, $attributes);
    }

}


/**
 * return a colorful label accroding to invoice status
 *
 * @param Object $invoice_info
 * @return html
 */
if (!function_exists('get_invoice_status_label')) {

    function get_invoice_status_label($invoice_info, $return_html = true) {
        $invoice_status_class = "label-default";
        $status = "not_paid";
        $now = get_my_local_time("Y-m-d");
        if ($invoice_info->status != "draft" && $invoice_info->due_date < $now && $invoice_info->payment_received < $invoice_info->invoice_value) {
            $invoice_status_class = "label-danger";
            $status = "overdue";
        } else if ($invoice_info->status !== "draft" && $invoice_info->payment_received <= 0) {
            $invoice_status_class = "label-warning";
            $status = "not_paid";
        } else if ($invoice_info->payment_received * 1 && $invoice_info->payment_received >= $invoice_info->invoice_value) {
            $invoice_status_class = "label-success";
            $status = "fully_paid";
        } else if ($invoice_info->payment_received > 0 && $invoice_info->payment_received < $invoice_info->invoice_value) {
            $invoice_status_class = "label-primary";
            $status = "partially_paid";
        } else if ($invoice_info->status === "draft") {
            $invoice_status_class = "label-default";
            $status = "draft";
        }

        $invoice_status = "<span class='label $invoice_status_class large'>" . lang($status) . "</span>";
        if ($return_html) {
            return $invoice_status;
        } else {
            return $status;
        }
    }

}



/**
 * get all data to make an invoice
 *
 * @param Int $invoice_id
 * @return array
 */
if (!function_exists('get_invoice_making_data')) {

    function get_invoice_making_data($invoice_id) {
        $ci = get_instance();
        $invoice_info = $ci->Invoices_model->get_details(array("id" => $invoice_id))->row();
        if ($invoice_info) {
            $data['invoice_info'] = $invoice_info;
            $data['client_info'] = $ci->Clients_model->get_one($data['invoice_info']->client_id);
            $data['invoice_items'] = $ci->Invoice_items_model->get_details(array("invoice_id" => $invoice_id))->result();
            $data['invoice_status_label'] = get_invoice_status_label($invoice_info);
            $data["invoice_total_summary"] = $ci->Invoices_model->get_invoice_total_summary($invoice_id);
            return $data;
        }
    }

}

/**
 *
 * get invoice number
 * @param Int $invoice_id
 * @return string
 */
if (!function_exists('get_invoice_id')) {

    function get_invoice_id($invoice_id) {
        $prefix = get_setting("invoice_prefix");
        $prefix = $prefix ? $prefix : strtoupper(lang("invoice")) . " #";
        return $prefix . $invoice_id;
    }

}

if (!function_exists('get_transaction_id')) {

    function get_transaction_id($transaction_id) {
        $prefix = get_setting("transactionid_prefix");
        $prefix = $prefix ? $prefix : strtoupper(lang("$transaction_id")) . " #";
        return $prefix ;
    }

}

/**
 *
 * get estimate number
 * @param Int $estimate_id
 * @return string
 */
if (!function_exists('get_estimate_id')) {

    function get_estimate_id($estimate_id) {
        $prefix = get_setting("estimate_prefix");
        $prefix = $prefix ? $prefix : strtoupper(lang("estimate")) . " #";
        return $prefix . $estimate_id;
    }

}


/**
 * ger all data to make an estimate
 *
 * @param Int $estimate_id
 * @return array
 */
if (!function_exists('get_estimate_making_data')) {

    function get_estimate_making_data($estimate_id) {
        $ci = get_instance();
        $estimate_info = $ci->Estimates_model->get_details(array("id" => $estimate_id))->row();
        if ($estimate_info) {
            $data['estimate_info'] = $estimate_info;
            $data['client_info'] = $ci->Clients_model->get_one($data['estimate_info']->client_id);
            $data['estimate_items'] = $ci->Estimate_items_model->get_details(array("estimate_id" => $estimate_id))->result();
            $data["estimate_total_summary"] = $ci->Estimates_model->get_estimate_total_summary($estimate_id);
            return $data;
        }
    }

}


/**
 * get team members and teams select2 dropdown data list
 *
 * @return array
 */
if (!function_exists('get_team_members_and_teams_select2_data_list')) {

    function get_team_members_and_teams_select2_data_list() {
        $ci = get_instance();

        $team_members = $ci->Users_model->get_all_where(array("deleted" => 0, "user_type" => "staff"))->result();
        $members_and_teams_dropdown = array();

        foreach ($team_members as $team_member) {
            $members_and_teams_dropdown[] = array("type" => "member", "id" => "member:" . $team_member->id, "text" => $team_member->first_name . " " . $team_member->last_name);
        }

        $team = $ci->Team_model->get_all_where(array("deleted" => 0))->result();
        foreach ($team as $team) {
            $members_and_teams_dropdown[] = array("type" => "team", "id" => "team:" . $team->id, "text" => $team->title);
        }

        return $members_and_teams_dropdown;
    }

}



/**
 * submit data for notification
 *
 * @return array
 */
if (!function_exists('log_notification')) {

    function log_notification($event, $options = array(), $user_id = 0) {

        $ci = get_instance();

        $url = get_uri("notification_processor/create_notification");

        $req = "event=" . encode_id($event, "notification");

        if ($user_id) {
            $req .= "&user_id=" . $user_id;
        } else if ($user_id === "0") {
            $req .= "&user_id=" . $user_id; //if user id is 0 (string) we'll assume that it's system bot
        } else {
            $req .= "&user_id=" . get_current_login_user()->id;
        }


        foreach ($options as $key => $value) {
            $value = urlencode($value);
            $req .= "&$key=$value";
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        curl_exec($ch);
        curl_close($ch);
    }

}


/**
 * save custom fields for any context
 *
 * @param Int $estimate_id
 * @return array
 */
if (!function_exists('save_custom_fields')) {

    function save_custom_fields($related_to_type, $related_to_id) {
        $ci = get_instance();
        $custom_fields = $ci->Custom_fields_model->get_combined_details($related_to_type, $related_to_id)->result();

        //save custom fields
        foreach ($custom_fields as $field) {
            $field_name = "custom_field_" . $field->id;
            //save only submitted fields
            if (array_key_exists($field_name, $_POST)) {
                $value = $ci->input->post($field_name);

                $field_value_data = array(
                    "related_to_type" => $related_to_type,
                    "related_to_id" => $related_to_id,
                    "custom_field_id" => $field->id,
                    "value" => $value
                );
                $ci->Custom_field_values_model->upsert($field_value_data);
            }
        }
    }

}


function _encodeURIComponent($str) {
    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
    return strtr(rawurlencode($str), $revert);
}

function db_get_results($sql='')
{
	$_this = & get_Instance();
    return $_this->db->query($sql)->result_array();
}
function db_execute($sql='')
{
	$_this = & get_Instance();
    return $_this->db->query($sql);
}

function db_get_where($table,$where)
{
	$_this = & get_Instance();
	$_this->db->_reset_select();
    $query = $_this->db->get_where($table, $where)->result();

    if ($query->num_rows() > 0)
    {
        return $query->result();
    }
    return FALSE;
}

function db_get_row($sql='')
{
	$_this = & get_Instance();
  	$query = $_this->db->query($sql)->row();
	return $query;
}

function db_get_rowarray($sql='')
{
	$_this = & get_Instance();
	$query = $_this->db->query($sql)->row_array();	
	return $query;
}


function db_get_var($sql='')
{
	$_this = & get_Instance();
    $query = $_this->db->query($sql);
    $field_array = $query->list_fields();
    $val = $query->row_array()[$field_array[0]];
	return $val;
}

if ( !function_exists('l'))
{
function l($message) {

	echo "<pre>Writelog: ";

	var_dump($message);

	echo "</pre>";

	//$log = new Affiliate_WP_Logging;

	//$log->log('Log from Miscfunctions: '.$message );

}
}


if ( !function_exists('ld'))
{
function ld($message) {

	echo "<pre>Writelogdie: ";

	var_dump($message);

	echo "</pre>";

	// $log = new Affiliate_WP_Logging;

	// $log->log('Log from Miscfunctions: '.$message );

	die;

}
}

function get_current_login_user() {
	if (is_client_logged_in())
	{
		$login_user =get_client_data();
		return $login_user;
	}
}


function theme_url() {
	return ci_site_url()."/assets";
}

function Sync_Member($user_id=0)
{
	if (!$user_id)
	{
		load_wp();
        if (is_user_logged_in()) {
            $user_id = get_current_user_id();
        }
        else
        {
        	return '';
        }
	}

    //$sync_url = "https://www.free-mart.com/syncmember/".$user_id;
    $sync_url= "http://www.free-mart.com/syncer.php?id=".$get_current_user_id;

    $needsynching = true;
    $htmldata = "";
     if ($needsynching) {
     	echo "
        $.ajax({
            url: '".$sync_url."',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(response) {
            console.log('Member data sync!');
            console.log(response);
        })
        .fail(function(response) {
            console.log('error while syncing member data:'+response);
        }); ";
    }
}




function sync_all_members()
{
	$_this = & get_Instance();
	//$_this->load->library('mcurl');
	$rows = $_this->db->query("Select user_id from sfm_uap_affiliates where display_name is null limit 1000")->result_array();

	foreach ($rows as $r)
	{

		$uid = $r["user_id"];
		echo "<pre>Synching userid: $uid</pre>";
		sync_data($uid);
		//$url = "http://free-mart.com/syncer.php?id=$uid";

		//$_this->mcurl->add_call("sync-$uid","get",$url);
	}

		//$data = $_this->mcurl->execute();
		//$_this->mcurl->debug();

		return;





   // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, array(
        	CURLOPT_RETURNTRANSFER => 1,
        	CURLOPT_URL => $url,
        	CURLOPT_USERAGENT => 'Codular Sample cURL Request'
    ));

    // Send the request & save response to $resp
    $resp = curl_exec($curl);

    // Close request to clear up some resources
    curl_close($curl);

    echo $resp;
    die;



}



function add_encryption_key_old()
{
    $CI =& get_instance();
    $key         = generate_encryption_key();
    $config_path = CRM_MODULE_PATH . 'config/config.php';
    $CI->load->helper('file');
    @chmod($config_path, FILE_WRITE_MODE);
    $config_file = read_file($config_path);
    $config_file = trim($config_file);
    $config_file = str_replace("\$config['encryption_key'] = '';", "\$config['encryption_key'] = '" . $key . "';", $config_file);
    if (!$fp = fopen($config_path, FOPEN_WRITE_CREATE_DESTRUCTIVE)) {
        return FALSE;
    }
    flock($fp, LOCK_EX);
    fwrite($fp, $config_file, strlen($config_file));
    flock($fp, LOCK_UN);
    fclose($fp);
    @chmod($config_path, FILE_READ_MODE);
    return $key;
}

/**
 * Check if the document should be RTL or LTR
 * The checking are performed in multiple ways eq Contact/Staff Direction from profile or from general settings *
 * @param  boolean $client_area
 * @return boolean
 */
function perfex_is_rtl($client_area = false)
{
    $CI =& get_instance();
    // if (is_client_logged_in()) {
    //     $CI->db->select('direction')->from('sfm_uap_affiliates')->where('id', get_client_user_id());
    //     $direction = $CI->db->get()->row()->direction;
    //     if ($direction == 'rtl') {
    //         return true;
    //     } else if ($direction == 'ltr') {
    //         return false;
    //     } else if (empty($direction)) {
    //         if (perfex_get_option('rtl_support_client') == 1) {
    //             return true;
    //         }
    //     }
    //     return false;
    // } else if ($client_area == true) {
    //     // Client not logged in and checked from clients area
    //     if (perfex_get_option('rtl_support_client') == 1) {
    //         return true;
    //     }
    // } else 

    if (is_staff_logged_in()) {
        $CI->db->select('direction')->from('main_crm.crm_staff')->where('staffid', get_staff_user_id());
        $direction = $CI->db->get()->row()->direction;
        if ($direction == 'rtl') {
            return true;
        } else if ($direction == 'ltr') {
            return false;
        } else if (empty($direction)) {
            if (perfex_get_option('rtl_support_admin') == 1) {
                return true;
            }
        }
        return false;
    } else if($client_area == false){
         if (perfex_get_option('rtl_support_admin') == 1) {
                return true;
            }
    }
    return false;
}
/**
 * Generate encryption key for app-config.php
 * @return stirng
 */
function generate_encryption_key()
{
    $CI =& get_instance();
    // In case accessed from my_functions_helper.php
    $CI->load->library('encryption');
    $key = bin2hex($CI->encryption->create_key(16));
    return $key;
}
/**
 * Function used to validate all recaptcha from google reCAPTCHA feature
 * @param  string $str
 * @return boolean
 */
function do_recaptcha_validation($str = '')
{
    $CI =& get_instance();
    $CI->load->library('form_validation');
    $google_url = "https://www.google.com/recaptcha/api/siteverify";
    $secret     = perfex_get_option('recaptcha_secret_key');
    $ip         = $CI->input->ip_address();
    $url        = $google_url . "?secret=" . $secret . "&response=" . $str . "&remoteip=" . $ip;
    $curl       = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    $res = curl_exec($curl);
    curl_close($curl);
    $res = json_decode($res, true);
    //reCaptcha success check
    if ($res['success']) {
        return TRUE;
    } else {
        $CI->form_validation->set_message('recaptcha', _l('recaptcha_error'));
        return FALSE;
    }
}
/**
 * Get current date format from options
 * @return string
 */
function get_current_date_format($php = false)
{
    $format = perfex_get_option('dateformat');
    $format = explode('|', $format);

    $hook_data = perfex_do_action('get_current_date_format', array(
        'format' => $format,
        'php' => $php
    ));

    $format = $hook_data['format'];
    $php    = $php;

    if ($php == false) {
        return $format[1];
    } else {
        return $format[0];
    }
}
/**
 * Check if current user is admin
 * @param  mixed $staffid
 * @return boolean if user is not admin
 */
function perfex_is_admin($staffid = '')
{
    $_staffid = get_staff_user_id();

    if (is_numeric($staffid)) {
        $_staffid = $staffid;
    }
    $CI =& get_instance();
    $admin= $CI->db->query("Select staffid from main_crm.crm_staff where admin = 1 and staffid = '$_staffid'")->row();
 
    if ($admin) {
        return true;
    }
    return false;
}
/**
 * Is user logged in
 * @return boolean
 */
function is_logged_in()
{
    $CI =& get_instance();
    if (!$CI->session->has_userdata('client_logged_in') && !$CI->session->has_userdata('staff_logged_in')) {
        return false;
    }
    return true;
}


function is_logged_in_as_client()
{
    $CI =& get_instance();

    if (($CI->session->has_userdata('logged_in_as_client') && $CI->session->userdata('logged_in_as_client'))) {
        return true;
    }
    return false;
}
/**
 * Is client logged in
 * @return boolean
 */
function is_client_logged_in()
{

    if (get_client_user_id())
    	return true;
    else
    	return false;

    if ($CI->session->has_userdata('client_logged_in') && $CI->session->userdata('client_logged_in')) {
        return true;
    }
    return false;
}
/**
 * Is staff logged in
 * @return boolean
 */
function is_staff_logged_in()
{
    $CI =& get_instance();
    if ($CI->session->has_userdata('staff_logged_in') && $CI->session->userdata('staff_logged_in') ) {
        return true;
    }
    return false;
}
/**
 * Return logged staff User ID from session
 * @return mixed
 */
function get_staff_user_id()
{
    $CI =& get_instance();

    if (!is_staff_logged_in())
    	return false; 

    return $CI->session->userdata('staff_user_id');
}
function get_staff_data()
{

    if (!is_staff_logged_in())
    	return false; 

    $CI =& get_instance();
    if (!$CI->session->has_userdata('staff_data')) {
        return false;
    }
    return  $CI->session->userdata('staff_data');
}

function get_staff_username()
{
    $CI =& get_instance();

    if (!is_staff_logged_in())
    	return false; 
	return $CI->session->userdata('staff_username');
}


/**
 * Return logged client User ID from session
 * @return mixed
 */
function get_client_user_id()
{
    $CI =& get_instance();
    if (!$CI->session->has_userdata('client_user_id')) {
        return false;
    }
    return $CI->session->userdata('client_user_id');
}

function get_client_username()
{
    $CI =& get_instance();
    if ($CI->session->has_userdata('client_logged_in')) {
	    if ($CI->session->has_userdata('client_data'))
	    {
	    	return $CI->session->userdata('client_data')->username;
	    }
	}
    return "";
}

function get_client_display_name()
{
    $CI =& get_instance();
    if ($CI->session->has_userdata('client_logged_in')) {
	    if ($CI->session->has_userdata('client_data'))
	    {
	    	return $CI->session->userdata('client_data')->display_name;
	    }
	}
    return "";
}


function get_client_data()
{
    $CI =& get_instance();
    if (is_client_logged_in()) {
	    if ($CI->session->has_userdata('client_data'))
	    {

	    	return $CI->session->userdata('client_data');
	    }
	    else
	    {
	    //	redirect('signin');
	    }
	}

    return null;
}

function get_contact_user_id()
{
    $CI =& get_instance();
    if (!$CI->session->has_userdata('client_logged_in')) {
        return false;
    }
    return $CI->session->userdata('contact_user_id');
}
/**
 * Get admin url
 * @param string url to append (Optional)
 * @return string admin url
 */
function perfex_admin_url($url = '')
{
    if ($url == '' || $url == '/') {
        if ($url == '/') {
            $url = '';
        }
        return perfex_admin_url.'/' ;
    } else {
        return perfex_admin_url. '/'.$url;
    }
}
function perfex_client_url($url = '')
{
    if ($url == '' || $url == '/') {
        if ($url == '/') {
            $url = '';
        }
        return perfex_CLIENT_URL.'/'  ;
    } else {
        return perfex_CLIENT_URL . $url ;
    }
}

/**
 * Outputs language string based on passed line
 * @since  Version 1.0.1
 * @param  string $line  language line string
 * @param  string $label sprint_f label
 * @return string        formated language
 */
function _l($line, $label = '',$log_errors = TRUE)
{
    $CI =& get_instance();

    $hook_data = perfex_do_action('before_get_language_text',array('line'=>$line,'label'=>$label));
    $line = $hook_data['line'];
    $label = $hook_data['label'];

    if (is_array($label) && count($label) > 0) {
        $_line = vsprintf($CI->lang->line(trim($line),$log_errors), $label);
    } else {
        $_line = @sprintf($CI->lang->line(trim($line),$log_errors), $label);
    }

    $hook_data = perfex_do_action('after_get_language_text',array('line'=>$line,'label'=>$label,'formated_line'=>$_line));
    $_line = $hook_data['formated_line'];
    $line = $hook_data['line'];

    if ($_line != '') {
        if (preg_match('/"/', $_line) && !is_html($_line)) {
            $_line = htmlspecialchars($_line, ENT_COMPAT);
        }
        return $CI->encoding_lib->toUTF8($_line);
    }

    if (mb_strpos($line, '_db_') !== false) {
        return 'db_translate_not_found';
    }

    return $CI->encoding_lib->toUTF8($line);
}
/**
 * Format date to selected dateformat
 * @param  date $date Valid date
 * @return date/string
 */
function _d($date)
{
    if ($date == '' || is_null($date) || $date == '0000-00-00') {
        return '';
    }
    $format = get_current_date_format();
    $date   = strftime($format, strtotime($date));
    return perfex_do_action('after_format_date', $date);
}
/**
 * Format datetime to selected datetime format
 * @param  datetime $date datetime date
 * @return datetime/string
 */
function _dt($date)
{
    if ($date == '' || is_null($date) || $date == '0000-00-00 00:00:00') {
        return '';
    }
    $format = get_current_date_format();
    $date   = strftime($format . ' %H:%M:%S', strtotime($date));
    return perfex_do_action('after_format_datetime', $date);
}
/**
 * Convert string to sql date based on current date format from options
 * @param  string $date date string
 * @return mixed
 */
function to_sql_date($date, $datetime = false)
{
    if ($date == '') {
        return NULL;
    }

    $to_date     = 'Y-m-d';
    $from_format = get_current_date_format(true);

    $hook_data['date']        = $date;
    $hook_data['from_format'] = $from_format;
    $hook_data['datetime']    = $datetime;

    $hook_data = perfex_do_action('before_sql_date_format', $hook_data);

    $date        = $hook_data['date'];
    $from_format = $hook_data['from_format'];

    if ($datetime == false) {
        return date_format(date_create_from_format($from_format, $date), $to_date);
    } else {
        if (strpos($date, ' ') === false) {
            $date .= ' 00:00:00';
        } else {
            $_temp = explode(' ', $date);
            $time  = explode(':', $_temp[1]);
            if (count($time) == 2) {
                $date .= ':00';
            }
        }

        if ($from_format == 'd/m/Y') {
            $date = preg_replace('#(\d{2})/(\d{2})/(\d{4})\s(.*)#', '$3-$2-$1 $4', $date);
        } else if ($from_format == 'm/d/Y') {
            $date = preg_replace('#(\d{2})/(\d{2})/(\d{4})\s(.*)#', '$3-$1-$2 $4', $date);
        } else if ($from_format == 'm.d.Y') {
            $date = preg_replace('#(\d{2}).(\d{2}).(\d{4})\s(.*)#', '$3-$1-$2 $4', $date);
        } else if ($from_format == 'm-d-Y') {
            $date = preg_replace('#(\d{2})-(\d{2})-(\d{4})\s(.*)#', '$3-$1-$2 $4', $date);
        }

        $d = strftime('%Y-%m-%d %H:%M:%S', strtotime($date));
        return perfex_do_action('to_sql_date_formatted', $d);
    }
}
/**
 * Check if passed string is valid date
 * @param  string  $date
 * @return boolean
 */
function perfex_is_date($date)
{
    if (strlen($date) < 10) {
        return false;
    }
    return (bool) strtotime($date);
}
/**
 * Get locale key by system language
 * @param  string $language language name from (application/languages) folder name
 * @return string
 */
function get_locale_key($language = 'english')
{
    $locale = 'en';
    if ($language == '') {
        return $locale;
    }

    $locales = get_locales();

    if (isset($locales[$language])) {
        $locale = $locales[$language];
    } else if (isset($locales[ucfirst($language)])) {
        $locale = $locales[ucfirst($language)];
    } else {
        foreach ($locales as $key => $val) {
            $key      = strtolower($key);
            $language = strtolower($language);
            if (strpos($key, $language) !== false) {
                $locale = $val;
                // In case $language is bigger string then $key
            } else if (strpos($language, $key) !== false) {
                $locale = $val;
            }
        }
    }

    $locale = perfex_do_action('before_get_locale', $locale);

    return $locale;
}
/**
 * Check if staff user has permission
 * @param  string  $permission permission shortname
 * @param  mixed  $staffid if you want to check for particular staff
 * @return boolean
 */
function has_permission($permission, $staffid = '', $can = '')
{
	return true;
    $_permission = $permission;
    $CI =& get_instance();
    // check for passed perfex_is_admin function
    if (function_exists($permission) && is_callable($permission)) {
        return call_user_func($permission, $staffid);
    }
    if (perfex_is_admin($staffid)) {
        return true;
    }

    $_user_id = get_staff_user_id();
    if ($staffid != '') {
        $_user_id = $staffid;
    }

    if ($can == '') {
        return false;
    }

        $sql = "Select * from (
Select main_crm.crm_permissions.permissionid, name, shortname from main_crm.crm_permissions where active =1
and shortname = '$permission'
union
Select module_id,module_name, module_title from main_shopfreemart.sfm_cms_module where active =1
and module_title = '$permission'
) main";

    $permission = $CI->db->query($sql)->row();
    if (!$permission) {
        return false;
    }
    $CI->db->select('1');
    $CI->db->from('main_crm.crm_staffpermissions');
    $CI->db->where('permissionid', $permission->permissionid);
    $CI->db->where('staffid', $_user_id);
    $CI->db->where('can_' . $can, 1);
    $perm = $CI->db->get()->row();
    if ($perm) {
        return true;
    }
    return false;
}
/**
 * Function is customer admin
 * @param  mixed  $id       customer id
 * @param  staff_id  $staff_id staff id to check
 * @return boolean
 */
function is_customer_admin($id, $staff_id = '')
{

    $_staff_id = get_staff_user_id();
    if (is_numeric($staff_id)) {
        $_staff_id = $staff_id;
    }
    $customer_admin_found = total_rows('main_crm.crm_customeradmins', array(
        'customer_id' => $id,
        'staff_id' => $_staff_id
    ));
    if ($customer_admin_found > 0) {
        return true;
    }
    return false;
}
/**
 * Check if staff member have assigned customers
 * @param  mixed $staff_id staff id
 * @return boolean
 */
function have_assigned_customers($staff_id = '')
{
    $_staff_id = get_staff_user_id();
    if (is_numeric($staff_id)) {
        $_staff_id = $staff_id;
    }
    $customers_found = total_rows('main_crm.crm_customeradmins', array(
        'staff_id' => $_staff_id
    ));
    if ($customers_found > 0) {
        return true;
    }
    return false;
}
/**
 * Check if contact has permission
 * @param  string  $permission permission name
 * @param  string  $contact_id     contact id
 * @return boolean
 */
function has_contact_permission($permission, $contact_id = '')
{
	return true;
    $CI =& get_instance();
    if (!class_exists('perfex_base')) {
        $CI->load->library('perfex_base');
    }
    $permissions = $CI->perfex_base->get_contact_permissions();
    // Contact id passed form function
    if ($contact_id != '') {
        $_contact_id = $contact_id;
    } else {
        // Current logged in contact
        $_contact_id     = get_contact_user_id();
    }
    foreach ($permissions as $_permission) {
        if ($_permission['short_name'] == $permission) {
            if (total_rows('main_crm.crm_contactpermissions', array(
                'permission_id' => $_permission['id'],
                'user_id' => $_contact_id
            )) > 0) {
                return true;
            }
        }
    }
    return false;
}
/**
 * Check if user is staff member
 * In the staff profile there is option to check IS NOT STAFF MEMBER eq like contractor
 * Some features are disabled when user is not staff member
 * @param  string  $id staff id
 * @return boolean
 */
function is_staff_member($id = '')
{
    $CI =& get_instance();
    $staffid = $id;
    if ($staffid == '') {
        $staffid = get_staff_user_id();
    }
    $CI->db->select('1')->from('main_crm.crm_staff')->where('staffid', $staffid)->where('is_not_staff', 0);
    $row = $CI->db->get()->row();
    if ($row) {
        return true;
    }
    return false;
}
/**
 * Load language in admin area
 * @param  string $staff_id
 * @return string return loaded language
 */

function load_sximo_language()
{
    $CI =& get_instance();
    $CI->session->set_userdata('lang','en');
    $CI->load->language('core','en');
    $CI->load->language('core',$CI->session->userdata('lang'));
}

function load_perfex_language($staff_id = '')
{

    $CI =& get_instance();

    $CI->lang->is_loaded = array();
    $CI->lang->language  = array();

     $language = perfex_get_option('active_language');
    if (is_staff_logged_in() || $staff_id != '') {
        $staff_language = get_staff_default_language($staff_id);
        if (!empty($staff_language)) {
            if (file_exists(CRM_MODULE_PATH . 'language/' . $staff_language)) {
                $language = $staff_language;

            }
        }
    }
    if ($language == 'language')
    	$language = 'english';

    $CI->load->language('crm/'.$language,$language);
    if (file_exists(CRM_MODULE_PATH . 'language/' . $language . '/custom_lang.php')) {
        $CI->load->language('crm/custom_lang', $language);
    }
    $language = perfex_do_action('after_load_admin_language', $language);

    return $language;
}

/**
 * Load customers area language
 * @param  string $customer_id
 * @return string return loaded language
 */
function load_client_language($customer_id = '')
{
    $CI =& get_instance();
    $language = perfex_get_option('active_language');
    if (is_client_logged_in() || $customer_id != '') {

        $client_language = get_client_default_language($customer_id);

        if (!empty($client_language)) {
            if (file_exists(CRM_MODULE_PATH . 'language/' . $client_language)) {
                $language = $client_language;
            }
        }
    }
    	//ld('crm/'.$language . '_lang', $language);
    $CI->lang->load($language . '_lang', $language);


    if (file_exists(CRM_MODULE_PATH . 'language/' . $language . '/custom_lang.php')) {
        $CI->lang->load('custom_lang', $language);
    }

    $language = perfex_do_action('after_load_client_language', $language);

    return $language;
}
/**
 * Get current url with query vars
 * @return string
 */
function current_full_url()
{
    $CI =& get_instance();
    $url = $CI->config->ci_site_url($CI->uri->uri_string());
    return $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
}


function sync_data_fromfreemart($id)
{
	$_this = & get_Instance();
    $url = "https://free-mart.com/syncer.php?id=$id";
   // Get cURL resource
    $curl = curl_init();
    // Set some options - we are passing in a useragent too here
    curl_setopt_array($curl, array(
        	CURLOPT_RETURNTRANSFER => 1,
        	CURLOPT_URL => $url,
        	CURLOPT_USERAGENT => 'Codular Sample cURL Request'
    ));

    // Send the request & save response to $resp
    $resp = curl_exec($curl);

    // Close request to clear up some resources
    curl_close($curl);
    //echo $resp;
    return;


	$_this->load->library('mcurl');
	$_this->mcurl->add_call("sync-$id","get",$url);
	$data = $_this->mcurl->execute();
	$_this->mcurl->debug();
	return;

}

function sync_data($id)
{
	$_this = & get_Instance();
	$rows = $_this->db->query("CALL main_shopfreemart.update_user_info($id);");
	$_this->Authentication_model->logged_client($id);
	return;

	$_this->load->library('Main');
	$_this->main->Sync_Member_Data($id);
	return;
}



function load_sso_cookie()
{
	if (!USE_WPLOGIN_ON_BACKEND) return;
 $wpurl = WP_URL;

    if (!defined('COOKIEHASH')){
    define( 'COOKIEHASH', md5($wpurl) );
    }
    $cookiename = "wordpress_logged_in_" . COOKIEHASH;
    get_wp_logged_user();

    $c = md5($cookiename);
    $v = md5('user').get_wp_logged_user();
   echo " <img src='https://www.freemartfriends.com/setcookie.php?c=$c&v=$v' style='display:none;' /> ";

}


/**
 * String starts with
 * @param  string $haystack
 * @param  string $needle
 * @return boolean
 */
if (!function_exists('_startsWith')) {
    function _startsWith($haystack, $needle)
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }
}
/**
 * String ends with
 * @param  string $haystack
 * @param  string $needle
 * @return boolean
 */
if (!function_exists('endsWith')) {
    function endsWith($haystack, $needle)
    {
        // search forward starting from end minus needle length characters
        return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
    }
}
/**
 * Check if there is html in string
 */
if(!function_exists('is_html')){
  function is_html($string)
  {
    return preg_match("/<[^<]+>/",$string,$m) != 0;
  }
}
/**
 * Get string after specific charcter/word
 * @param  string $string    string from where to get
 * @param  substring $substring search for
 * @return string
 */
function strafter($string, $substring)
{
    $pos = strpos($string, $substring);
    if ($pos === false)
        return $string;
    else
        return (substr($string, $pos + strlen($substring)));
}
/**
 * Get string before specific charcter/word
 * @param  string $string    string from where to get
 * @param  substring $substring search for
 * @return string
 */
function strbefore($string, $substring)
{
    $pos = strpos($string, $substring);
    if ($pos === false)
        return $string;
    else
        return (substr($string, 0, $pos));
}
/**
 * Is internet connection open
 * @param  string  $domain
 * @return boolean
 */
function is_connected($domain = 'www.perfexcrm.com')
{
    $connected = @fsockopen($domain, 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}
/**
 * Replace Last Occurence of a String in a String
 * @since  Version 1.0.1
 * @param  string $search  string to be replaced
 * @param  string $replace replace with
 * @param  string $subject [the string to search
 * @return string
 */
function str_lreplace($search, $replace, $subject)
{
    $pos = strrpos($subject, $search);
    if ($pos !== false) {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }
    return $subject;
}
/**
 * Get string bettween words
 * @param  string $string the string to get from
 * @param  string $start  where to start
 * @param  string $end    where tstrrposo end
 * @return string formated string
 */
function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini    = strpos($string, $start);
    if ($ini == 0)
        return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}
/**
 * Format datetime to time ago with specific hours mins and seconds
 * @param  datetime $lastreply
 * @param  string $from      Optional
 * @return mixed
 */
function time_ago_specific($date, $from = "now")
{
    $datetime   = strtotime($from);
    $date2      = strtotime("" . $date);
    $holdtotsec = $datetime - $date2;
    $holdtotmin = ($datetime - $date2) / 60;
    $holdtothr  = ($datetime - $date2) / 3600;
    $holdtotday = intval(($datetime - $date2) / 86400);
    $str        = '';
    if (0 < $holdtotday) {
        $str .= $holdtotday . "d ";
    }
    $holdhr = intval($holdtothr - $holdtotday * 24);
    $str .= $holdhr . "h ";
    $holdmr = intval($holdtotmin - ($holdhr * 60 + $holdtotday * 1440));
    $str .= $holdmr . "m";
    return $str;
}
/**
 * Format seconds to quantity
 * @param  mixed  $sec      total seconds
 * @return [integer]
 */
function sec2qty($sec)
{
    $seconds = $sec / 3600;
    return round($seconds, 2);
}
/**
 * @deprecated
 * Format seconds to hours/minutes or seconds
 * @param  mixed $seconds
 * @return mixed
 */
function format_seconds($seconds)
{
    $minutes = $seconds / 60;
    $hours   = $minutes / 60;
    if ($minutes >= 60) {
        return round($hours, 2) . ' ' . _l('hours');
    } elseif ($seconds > 60) {
        return round($minutes, 2) . ' ' . _l('minutes');
    } else {
        return $seconds . ' ' . _l('seconds');
    }
}
/**
 * Format seconds to H:I:S
 * @param  integer $seconds         mixed
 * @param  boolean $include_seconds
 * @return string
 */
function seconds_to_time_format($seconds = 0, $include_seconds = false) {
  $hours = floor($seconds / 3600);
  $mins = floor(($seconds - ($hours * 3600)) / 60);
  $secs = floor($seconds % 60);

  $hours = ($hours < 10) ? "0" . $hours : $hours;
  $mins = ($mins < 10) ? "0" . $mins : $mins;
  $secs = ($secs < 10) ? "0" . $secs : $secs;
  $sprintF = $include_seconds == true ? '%02d:%02d:%02d' : '%02d:%02d';
  return sprintf($sprintF, $hours, $mins, $secs);
}

/*
 * ip_in_range.php - Function to determine if an IP is located in a
 *                   specific range as specified via several alternative
 *                   formats.
 *
 * Network ranges can be specified as:
 * 1. Wildcard format:     1.2.3.*
 * 2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
 * 3. Start-End IP format: 1.2.3.0-1.2.3.255
 *
 * Return value BOOLEAN : ip_in_range($ip, $range);
 *
 * Copyright 2008: Paul Gregg <pgregg@pgregg.com>
 * 10 January 2008
 * Version: 1.2
 *
 * Source website: http://www.pgregg.com/projects/php/ip_in_range/
 * Version 1.2
 *
 * This software is Donationware - if you feel you have benefited from
 * the use of this tool then please consider a donation. The value of
 * which is entirely left up to your discretion.
 * http://www.pgregg.com/donate/
 *
 * Please do not remove this header, or source attibution from this file.
 */

// ip_in_range
// This function takes 2 arguments, an IP address and a "range" in several
// different formats.
// Network ranges can be specified as:
// 1. Wildcard format:     1.2.3.*
// 2. CIDR format:         1.2.3/24  OR  1.2.3.4/255.255.255.0
// 3. Start-End IP format: 1.2.3.0-1.2.3.255
// The function will return true if the supplied IP is within the range.
// Note little validation is done on the range inputs - it expects you to
// use one of the above 3 formats.
Function ip_in_range($ip, $range) {
  if (strpos($range, '/') !== false) {
    // $range is in IP/NETMASK format
    list($range, $netmask) = explode('/', $range, 2);
    if (strpos($netmask, '.') !== false) {
      // $netmask is a 255.255.0.0 format
      $netmask = str_replace('*', '0', $netmask);
      $netmask_dec = ip2long($netmask);
      return ( (ip2long($ip) & $netmask_dec) == (ip2long($range) & $netmask_dec) );
    } else {
      // $netmask is a CIDR size block
      // fix the range argument
      $x = explode('.', $range);
      while(count($x)<4) $x[] = '0';
      list($a,$b,$c,$d) = $x;
      $range = sprintf("%u.%u.%u.%u", empty($a)?'0':$a, empty($b)?'0':$b,empty($c)?'0':$c,empty($d)?'0':$d);
      $range_dec = ip2long($range);
      $ip_dec = ip2long($ip);

      # Strategy 1 - Create the netmask with 'netmask' 1s and then fill it to 32 with 0s
      #$netmask_dec = bindec(str_pad('', $netmask, '1') . str_pad('', 32-$netmask, '0'));

      # Strategy 2 - Use math to create it
      $wildcard_dec = pow(2, (32-$netmask)) - 1;
      $netmask_dec = ~ $wildcard_dec;

      return (($ip_dec & $netmask_dec) == ($range_dec & $netmask_dec));
    }
  } else {
    // range might be 255.255.*.* or 1.2.3.0-1.2.3.255
    if (strpos($range, '*') !==false) { // a.b.*.* format
      // Just convert to A-B format by setting * to 0 for A and 255 for B
      $lower = str_replace('*', '0', $range);
      $upper = str_replace('*', '255', $range);
      $range = "$lower-$upper";
    }

    if (strpos($range, '-')!==false) { // A-B format
      list($lower, $upper) = explode('-', $range, 2);
      $lower_dec = (float)sprintf("%u",ip2long($lower));
      $upper_dec = (float)sprintf("%u",ip2long($upper));
      $ip_dec = (float)sprintf("%u",ip2long($ip));
      return ( ($ip_dec>=$lower_dec) && ($ip_dec<=$upper_dec) );
    }

    echo 'Range argument is not in 1.2.3.4/24 or 1.2.3.4/255.255.255.0 format';
    return false;
  }

}

if (!function_exists('to_currency')) {

    function to_currency($number = 0, $currency = "", $no_of_decimals = 2, $format = 1) {

    	if ($number == null)
    	{
			$number =0;
		}


    	if ($number == '')
    	{
			$number =0;
		}

    	if (!is_numeric($number))
    	{
			$number =0;
		}

        $decimal_separator = get_setting("decimal_separator");
        $thousand_separator = get_setting("thousand_separator");

        $negative_sign = "";
        if ($number < 0) {
            $number = $number * -1;
            $negative_sign = "-";

        }
        if (!$currency) {
            $currency = get_setting("currency_symbol");
        }

        $currency_position = get_setting("currency_position");
        if (!$currency_position) {
            $currency_position = "left";
        }

        if ($decimal_separator === ",") {
            if ($thousand_separator !== " ") {
                $thousand_separator = ".";
            }

            if ($currency_position === "right") {
                $out = $negative_sign . number_format($number, $no_of_decimals, ",", $thousand_separator) . $currency;
            } else {
                $out = $negative_sign . $currency . number_format($number, $no_of_decimals, ",", $thousand_separator);
            }
        } else {
            if ($thousand_separator !== " ") {
                $thousand_separator = ",";
            }

            if ($currency_position === "right") {
                $out = $negative_sign . number_format($number, $no_of_decimals, ".", $thousand_separator) . $currency;
            } else {

                $out = $negative_sign . $currency . number_format($number, $no_of_decimals, ".", $thousand_separator);
            }
        }

        if ($negative_sign == "-")
            return "<span style='color:red'>$out</span>";
        else
            return "<span>$out</span>";
    }

}

/**
 * convert a number to quantity format
 *
 * @param number $number
 * @return number
 */
if (!function_exists('to_decimal_format')) {

    function to_decimal_format($number = 0) {
        $decimal_separator = get_setting("decimal_separator");
        $decimal = 0;
        if (is_numeric($number) && floor($number) != $number) {
            $decimal = 2;
        }
        if ($decimal_separator === ",") {
            return number_format($number, $decimal, ",", ".");
        } else {
            return number_format($number, $decimal, ".", ",");
        }
    }

}

/**
 * convert a currency value to data format
 *
 * @param number $currency
 * @return number
 */
if (!function_exists('unformat_currency')) {

    function unformat_currency($currency = "") {
// remove everything except a digit "0-9", a comma ",", and a dot "."
        $new_money = preg_replace('/[^\d,-\.]/', '', $currency);
        $decimal_separator = get_setting("decimal_separator");
        if ($decimal_separator === ",") {
            $new_money = str_replace(".", "", $new_money);
            $new_money = str_replace(",", ".", $new_money);
        } else {
            $new_money = str_replace(",", "", $new_money);
        }
        return $new_money;
    }

}

/**
 * get array of international currency codes
 *
 * @return array
 */
if (!function_exists('get_international_currency_code_list')) {

    function get_international_currency_code_list() {
        return array(
            "AED",
            "AFN",
            "ALL",
            "AMD",
            "ANG",
            "AOA",
            "ARS",
            "AUD",
            "AWG",
            "AZN",
            "BAM",
            "BBD",
            "BDT",
            "BGN",
            "BHD",
            "BIF",
            "BMD",
            "BND",
            "BOB",
            "BOV",
            "BRL",
            "BSD",
            "BTN",
            "BWP",
            "BYR",
            "BZD",
            "CAD",
            "CDF",
            "CHE",
            "CHF",
            "CHW",
            "CLF",
            "CLP",
            "CNY",
            "COP",
            "COU",
            "CRC",
            "CUC",
            "CUP",
            "CVE",
            "CZK",
            "DJF",
            "DKK",
            "DOP",
            "DZD",
            "EGP",
            "ERN",
            "ETB",
            "EUR",
            "FJD",
            "FKP",
            "GBP",
            "GEL",
            "GHS",
            "GIP",
            "GMD",
            "GNF",
            "GTQ",
            "GYD",
            "HKD",
            "HNL",
            "HRK",
            "HTG",
            "HUF",
            "IDR",
            "ILS",
            "INR",
            "IQD",
            "IRR",
            "ISK",
            "JMD",
            "JOD",
            "JPY",
            "KES",
            "KGS",
            "KHR",
            "KMF",
            "KPW",
            "KRW",
            "KWD",
            "KYD",
            "KZT",
            "LAK",
            "LBP",
            "LKR",
            "LRD",
            "LSL",
            "LYD",
            "MAD",
            "MDL",
            "MGA",
            "MKD",
            "MMK",
            "MNT",
            "MOP",
            "MRO",
            "MUR",
            "MVR",
            "MWK",
            "MXN",
            "MXV",
            "MYR",
            "MZN",
            "NAD",
            "NGN",
            "NIO",
            "NOK",
            "NPR",
            "NZD",
            "OMR",
            "PAB",
            "PEN",
            "PGK",
            "PHP",
            "PKR",
            "PLN",
            "PYG",
            "QAR",
            "RON",
            "RSD",
            "RUB",
            "RWF",
            "SAR",
            "SBD",
            "SCR",
            "SDG",
            "SEK",
            "SGD",
            "SHP",
            "SLL",
            "SOS",
            "SRD",
            "SSP",
            "STD",
            "SYP",
            "SZL",
            "THB",
            "TJS",
            "TMT",
            "TND",
            "TOP",
            "TRY",
            "TTD",
            "TWD",
            "TZS",
            "UAH",
            "UGX",
            "USD",
            "USN",
            "USS",
            "UYI",
            "UYU",
            "UZS",
            "VEF",
            "VND",
            "VUV",
            "WST",
            "XAF",
            "XAG",
            "XAU",
            "XBA",
            "XBB",
            "XBC",
            "XBD",
            "XCD",
            "XDR",
            "XFU",
            "XOF",
            "XPD",
            "XPF",
            "XPT",
            "XSU",
            "XTS",
            "XUA",
            "YER",
            "ZAR",
            "ZMW"
        );
    }

};


/**
 * get dropdown list fro international currency code
 *
 * @return array
 */
if (!function_exists('get_international_currency_code_dropdown')) {

    function get_international_currency_code_dropdown() {
        $result = array();
        foreach (get_international_currency_code_list() as $value) {
            $result[$value] = $value;
        }
        return $result;
    }

};
function ci_get_current_user_id() {
	return get_client_user_id();

	$_this = & get_Instance();
	//ld($_this->session->userdata);
	$uid = $_this->session->userdata('uid');
	return $uid;
}

function mkdir_if_not_exist($path, $mode = 0777, $recursive = TRUE)
{
	if (!is_dir($path))
	{
		// Reference: http://stackoverflow.com/questions/3997641/why-cant-php-create-a-directory-with-777-permissions
		$oldmask = umask(0);
		mkdir($path, $mode, $recursive);
		umask($oldmask);
	}
}


// check whether a string starts with the target substring
function starts_with($haystack, $needle)
{
	return substr($haystack, 0, strlen($needle))===$needle;
}

// check whether a string ends with the target substring
function ends_with($haystack, $needle)
{
	return substr($haystack, -strlen($needle))===$needle;
}


// location of public asset folder
function asset_url($path)
{
	return base_url('assets/'.$path);
}

// location of uploaded files
function upload_url($path)
{
	return base_url('assets/uploads/'.$path);
}

// location of post-processed assets (e.g. combined CSS / JS files)
function dist_url($path)
{
	return base_url('assets/dist/'.$path);
}

// location of post-processed images (i.e. optimized filesize)
function image_url($path)
{
	return base_url('assets/dist/images/'.$path);
}

// location to pages in different language
// Sample Usage:
// 	- lang_url('en'): to English version of current page
// 	- lang_url('en', 'about'): to English version of About page
function lang_url($lang, $url = NULL)
{
	$CI =& get_instance();
	$config = $CI->config->item('ci_bootstrap');

	if ( empty($config['languages']) )
	{
		$url = ($url===NULL) ? current_full_url() : $url;
		return base_url($url);
	}
	else
	{
		$lang_config = $config['languages'];
		$available_lang = $lang_config['available'];

		if ($url===NULL)
		{
			$segment_1 = $CI->uri->segment(1, $lang_config['default']);

			// current page in target language
			if (array_key_exists($segment_1, $available_lang))
			{
				// URL already contains language abbr
				if ($CI->uri->total_segments()==1)
					$target_url = str_replace("/$segment_1", "/$lang", current_full_url());
				else
					$target_url = str_replace("/$segment_1/", "/$lang/", current_full_url());
			}
			else
			{
				// URL does not contain language abbr
				$target_url = base_url($lang.'/'.$CI->uri->uri_string());
			}
		}
		else
		{
			// target page in target language
			$target_url = base_url($lang.'/'.$url);
		}

		return $target_url;
	}
}


// refresh current page (interrupt other actions)
function refresh()
{
	redirect(current_full_url(), 'refresh');
}

// referrer page
function referrer()
{
	$CI =& get_instance();
	$CI->load->library('user_agent');
	return $CI->agent->referrer();
}

// redirect back to referrer page
function redirect_referrer()
{
	redirect(referrer());
}


if(!function_exists('db_get_all_data')) {
	function db_get_all_data($table_name = null, $where = false) {
		$ci =& get_instance();
		if ($where) {
			$ci->db->where($where);
		}
	  	$query = $ci->db->get($table_name);

	    return $query->result();
	}
}

if(!function_exists('_ent')) {
	function _ent($string = null) {
		return htmlentities($string);
	}
}

if(!function_exists('is_allowed')) {
	function is_allowed($permission, Closure $func) {
		return true;

		$ci =& get_instance();
		$reflection = new ReflectionFunction($func);
		$arguments  = $reflection->getParameters();


		if ($ci->aauth->is_allowed($permission)) {
			call_user_func($func, $arguments);
		} else {
			ob_start();
			call_user_func($func, $arguments);
			$buffer = ob_get_contents();
			ob_end_clean();

		}
	}
}
if(!function_exists('display_menu_module')) {
	function display_menu_module($parent, $level, $menu_type) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_menu');
		$menu_type_id = $ci->model_menu->get_id_menu_type_by_flag($menu_type);
	    $result = $ci->db->query("SELECT a.id, a.label, a.link, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `menu` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.menu_type_id = ".$menu_type_id." AND a.parent=" . $parent." order by `sort` ASC")->result();

		$ret = '';
	    if ($result) {
		    $ret .= '<ol class="dd-list">';
		   	foreach ($result as $row) {
		        if ($row->Count > 0) {
		        	$ret .= '<li class="dd-item dd3-item" data-id="'.$row->id.'">
		                                          <div class="dd-handle dd3-handle"></div><div class="dd3-content">'._ent($row->label);

		            if ($ci->aauth->is_allowed('menu_delete')) {
				            $ret .= '<span class="pull-right"><a class="remove-data" href="javascript:void()" data-href="'.site_url('administrator/menu/delete/'.$row->id).'"><i class="fa fa-trash btn-action"></i></a>
				                </span';
		            }

		            if ($ci->aauth->is_allowed('menu_update')) {
				            $ret .= '<span class="pull-right"><a href="'.site_url('administrator/menu/edit/'.$row->id).'"><i class="fa fa-pencil btn-action"></i></a>
		                        </span>';
		            }

		            $ret .= '</div>';
					$ret .= display_menu_module($row->id, $level + 1, $menu_type);
					$ret .= "</li>";
		        } elseif ($row->Count==0) {
		            $ret .= '<li class="dd-item dd3-item" data-id="'.$row->id.'">
		                                          <div class="dd-handle dd3-handle"></div><div class="dd3-content">'._ent($row->label);

		            if ($ci->aauth->is_allowed('menu_delete')) {
				            $ret .= '<span class="pull-right"><a class="remove-data" href="javascript:void()" data-href="'.site_url('administrator/menu/delete/'.$row->id).'"><i class="fa fa-trash btn-action"></i></a>
				                </span';
		            }

		            if ($ci->aauth->is_allowed('menu_update')) {
				            $ret .= '<span class="pull-right"><a href="'.site_url('administrator/menu/edit/'.$row->id).'"><i class="fa fa-pencil btn-action"></i></a>
		                        </span>';
		            }

					$ret .= '</div></li>';
		        }
		    }
		    $ret .= "</ol>";
	    }

	    return $ret;
	}
}

if(!function_exists('display_menu_admin')) {
	function display_menu_admin($parent, $level) {
		$ci =& get_instance();
		$ci->load->database();
		$ci->load->model('model_menu');
	    $result = $ci->db->query("SELECT a.id, a.label,a.icon_color, a.type, a.link,a.icon, Deriv1.Count FROM `menu` a  LEFT OUTER JOIN (SELECT parent, COUNT(*) AS Count FROM `menu` GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.menu_type_id = 1 AND a.parent=" . $parent." order by `sort` ASC")->result();

		$ret = '';
	    if ($result) {
	    	if (($level > 1) AND ($parent > 0) ) {
		    	$ret .= '<ul class="treeview-menu">';
	    	} else {
	    		$ret = '';
	    	}
		   	foreach ($result as $row) {
		   		$perms = 'menu_'.strtolower(str_replace(' ', '_', $row->label));

		   		$links = explode('/', $row->link);

				$segments = array_slice($ci->uri->segment_array(), 0, count($links));

		   		if (implode('/', $segments) == implode('/', $links)) {
		   			$active = 'active';
		   		} else {
		   			$active = '';
		   		}
		   		if ($row->type == 'label') {
		        	$ret .= '<li class="header">'._ent($row->label).'</li>';
		   		} else {
			        if ($row->Count > 0) {
			        	if ($ci->aauth->is_allowed($perms)) {
				        	$ret .= '<li class="'.$active.'">
										        	<a href="'.site_url($row->link).'">';

							if ($parent) {
								$ret .= '<i class="fa fa-circle-o '._ent($row->icon_color).'"></i> <span>'._ent($row->label).'</span>
									            <span class="pull-right-container">
									              <i class="fa fa-angle-left pull-right"></i>
									            </span>
									          </a>';
							} else {
								$ret .= '<i class="fa '._ent($row->icon).' '._ent($row->icon_color).'"></i> <span>'._ent($row->label).'</span>
									            <span class="pull-right-container">
									              <i class="fa fa-angle-left pull-right"></i>
									            </span>
									          </a>';
							}

							$ret .= display_menu_admin($row->id, $level + 1);
							$ret .= "</li>";
						}
			        } elseif ($row->Count==0) {
			           if ($ci->aauth->is_allowed($perms)) {
							$ret .= '<li class="'.$active.'">
										        	<a href="'.site_url($row->link).'">';

							if ($parent) {
								$ret .= '<i class="fa fa-circle-o '._ent($row->icon_color).'"></i> <span>'._ent($row->label).'</span>
									            <span class="pull-right-container"></i>
									            </span>
									          </a>';
							} else {
								$ret .= '<i class="fa '._ent($row->icon).' '._ent($row->icon_color).'"></i> <span>'._ent($row->label).'</span>
									            <span class="pull-right-container"></i>
									            </span>
									          </a>';
							}

							$ret .= "</li>";
						}
			        }
		   		}
		    }
		    if ($level != 1) {
		    	$ret .= '</ul>';
	    	}
	    }

	    return $ret;
	}
	function hasPermission($key, $value) {
			$ci =& get_instance();
		if (isset($ci->permission[$key])) {
			return in_array($value, $ci->permission[$key]);
		} else {
			return false;
		}
	}


	function get_sponsor_name($user_id){
		$row= db_get_row('Select u2.user_id, u2.display_name from sfm_uap_affiliates u1 left join sfm_uap_affiliates u2 on u1.sponsorkey = u2.user_id WHERE u1.user_id='.$user_id);
	    if ($row)
	    	return $row->display_name;
	    else
	    	return '';
	}

	function get_module_filters( $moduleid )
	{
		return db_get_results("SELECT id,title,description from sfm_filters");
	}



    /**
    * Workaround for json_encode's UTF-8 encoding if a different charset needs to be used
    *
    * @param mixed $result
    * @return string
    */
    function jsonify($result = FALSE)
    {
      if(is_null($result))
        return 'null';

      if($result === FALSE)
        return 'false';

      if($result === TRUE)
        return 'true';

      if(is_scalar($result))
      {
        if(is_float($result))
          return floatval(str_replace(',', '.', strval($result)));

        if(is_string($result))
        {
          static $jsonReplaces = array(array('\\', '/', '\n', '\t', '\r', '\b', '\f', '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
          return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $result) . '"';
        }
        else
          return $result;
      }

      $isList = TRUE;

      for($i = 0, reset($result); $i < count($result); $i++, next($result))
      {
        if(key($result) !== $i)
        {
          $isList = FALSE;
          break;
        }
      }

      $json = array();

      if($isList)
      {
        foreach($result as $value)
          $json[] = jsonify($value);

        return '[' . join(',', $json) . ']';
      }
      else
      {
        foreach($result as $key => $value)
          $json[] = jsonify($key) . ':' . jsonify($value);

        return '{' . join(',', $json) . '}';
      }
    }
	

}



// ------------------------------------------------------------------------

/* End of file captcha_helper.php */
/* Location: ./system/heleprs/captcha_helper.php */


function activitylog_save($userid, $acivity, $orderid,$transactionid){
		$_this = & get_Instance();
        $data = array(
          'user_id' => $userid,
          'activity' => $acivity,
          'date' => date("Y-m-d H:i:s"),
          'order_id' => $orderid,
          'transaction_id' => $transactionid
        );
        $_this->db->insert('sfm_user_logs', $data);

        //return $event_module; 
}

function get_status_label($value){
	return($value);
}