<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| COPYRIGHT NOTICE                                                     
| Copyright 2013 JROX Technologies, Inc.  All Rights Reserved.    
| -------------------------------------------------------------------------                                                                        
| This script may be only used and modified in accordance to the license      
| agreement attached (license.txt) except where expressly noted within      
| commented areas of the code body. This copyright notice and the  
| comments above and below must remain intact at all times.  By using this 
| code you agree to indemnify JROX Technologies, Inc, its corporate agents   
| and affiliates from any liability that might arise from its use.                                                        
|                                                                           
| Selling the code for this program without prior written consent is       
| expressly forbidden and in violation of Domestic and International 
| copyright laws.  
|	
| -------------------------------------------------------------------------
| FILENAME: template_helper.php
| -------------------------------------------------------------------------     
| 
| This file loads the template for JEM
|
*/

function _generate_report_graph($graph = '')
{
	$CI =& get_instance();
	
	$show_graph = '<script type="text/javascript">
	var flashvars = {};
	var params = {
					menu: "false",
					wmode: "transparent"
				 };
	var attributes = {};

	swfobject.embedSWF(	"' . $CI->config->item('base_url') . 'js/flashcharts/open-flash-chart.swf", 
						"stats-graph", "' . JROX_CHARTS_GRAPH_WIDTH . '", "' . JROX_CHARTS_GRAPH_HEIGHT . '", "9.0.0",
						"expressInstall.swf", flashvars, params, attributes);
	</script>

';

	if (!empty($graph))
	{
		$show_graph .= '<script type="text/javascript">
	function ofc_ready(){}
	
	function open_flash_chart_data()
	{
		return JSON.stringify(data);
	}
	
	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
		return window[movieName];
	  } else {
		return document[movieName];
	  }
	}
		
	var data = ' . $graph . ';
	</script>
	'; 
	
	}	

	return $show_graph;
}

// ------------------------------------------------------------------------

function _generate_sub_headline()
{
	$CI =& get_instance();
	//echo '<a class="btn btn-info iframe" href="http://www.youtube.com/v/F1xRW1CYpik?rel=0&wmode=transparent">Need Help with this page? Watch this tutorial.</a>';
	//echo $CI->uri->uri_string();
}

// ------------------------------------------------------------------------

function _generate_price_button($s = array(), $num_options = '')
{
	$CI =& get_instance();
	
	if ($s['login_for_price'] == 1)
	{
		if (!$CI->session->userdata('userid'))
		{
			return '<a href="' . base_url('login') . '" class="btn btn-info login_for_price">' . lang('login_for_price') . '</a>';	
		}
		else
		{
			if ($s['add_cart_for_price'] == 1)
			{
				return '<a href="' . base_url() . 'cart/add/' . $s['s_pid'] . '" class="add_to_cart">' . lang('add_cart_for_price') . '</a>';	
			}
			else
			{
				return format_amounts($s['product_price'], $num_options);
				
			}
		}
	}
	elseif ($s['add_cart_for_price'] == 1)
	{
		return '<a href="' . base_url() . 'cart/add/' . $s['s_pid'] . '" class="add_to_cart">' . lang('add_cart_for_price') . '</a>';	
	}
	else
	{
		return format_amounts($s['product_price'], $num_options);
	}
}


// ------------------------------------------------------------------------

function _generate_order_status()
{
	$CI =& get_instance();
	$array = array();
	
	foreach ($CI->config->item('dbi_order_status') as $v)
	{
		$array[$v] = lang($v);
	}

	return $array;
}

// ------------------------------------------------------------------------

function _check_ssl($url = '')
{
	if ($_SERVER['SERVER_PORT'] == '443')
	{
		return 'https://' . $url;	
	}
	
	return 'http://' . $url;
}

// ------------------------------------------------------------------------

function _content_filter($body = '')
{
	$CI =& get_instance();
	
	if (is_array($CI->config->item('dbi_content_filter')))
	{
		foreach ($CI->config->item('dbi_content_filter') as $k => $v)
		{
			$body = str_replace($k, $v, $body);	
		}
	}
	
	return html_entity_decode($body, ENT_QUOTES, $CI->config->item('charset'));
}

// ------------------------------------------------------------------------

function _generate_year_dropdown($select = '', $class = 'select2 text-left', $onchange = 'ChangeReportStatus(this)')
{
	$CI =& get_instance();

	$options = array();
	
	$total_years = $CI->config->item('dbr_total_report_years');
	
	$month = '1';
	
	$current_year = date('Y');
	
	$last_year = $current_year - $total_years;
	
	for ($i = $current_year; $i > $last_year; $i = $i - '1')
	{	
		$options[$month .'/'.$i] = lang('year'). ' ' . date('Y', mktime(0, 0, 0, '1'  , '1', $i));
	}
		  			   
	return form_dropdown('change_status', $options, $select, 'class="' . $class . '" style="width: 150px" onchange="' . $onchange . '"'); 
}

// ------------------------------------------------------------------------

function change_content_type($value = '')
{
	$CI =& get_instance();
	
	switch ($value)
	{
		case '1':
			
			return '<span class="label label-primary">' . lang('blog') . '</span>';
			
		break;
		
		case '2':
		
			return '<span class="label label-info">' . lang('standard') . '</span>';
		
		break;
		
		case '3':
		
			return '<span class="label label-warning">' . lang('advanced') . '</span>';
		
		break;
	}
}

// ------------------------------------------------------------------------

function _generate_month_dropdown($select = '', $class = 'select2 text-left', $onchange = 'ChangeReportStatus(this)')
{
	$CI =& get_instance();
	
	$options = array();
	
	$total_years = $CI->config->item('dbr_total_report_years');
	
	$current_year = date('Y');
	
	$last_year = $current_year - $total_years;
	
	for ($i = $current_year; $i > $last_year; $i = $i - '1')
	{	
		for ($j = 12; $j >= 1; $j = $j - '1')
		{
			$options[date('m/Y', mktime(0, 0, 0, $j  , '1', $i))] = date('F Y', mktime(0, 0, 0, $j  , '1', $i));
		}
		
		$j = 1;
	}
		  			   
	return form_dropdown('change_status', $options, $select, 'class="' . $class . '" style="width: 160px" onchange="' . $onchange . '"'); 
}

// ------------------------------------------------------------------------

function load_form($type = '', $file ='', $data = '', $stream = false)
{
	$CI =& get_instance();
	
	if (file_exists(APPPATH . '/views/' . $type . '/' . '/custom_templates/' . $file . '.php' ))
	{
		$body = '/custom_templates/' . $file;
			
	}
	else 
	{
		$body = $file;
	}
	
	if ($stream == true)
	{
		return $CI->load->view($type . '/' . $body, $data, true);
	}
	else
	{
		echo header('Content-type: text/html; charset=' . $CI->config->item('charset'));
		$CI->load->view($type . '/' . $body, $data);
	}
}

// ------------------------------------------------------------------------

function _show_ratings($num = '', $avg = false)
{
	
	// Get fractional part of the rank
	$num = round($num,1);
    $partial = ($num - floor($num)) * 10;
	
    $path = base_url() . 'images/misc/stars';

    $stars = '<span title="' . $num . '">';
    
	for($i=1; $i<=($num);$i++)
    {
        $stars .= '<img src="' . $path . '/star.png" alt=""/>';
    }

    if ($partial > 0)
    {
        $stars .= '<img src="' . $path . '/star' . $partial . '.png" alt=""/>';
    }

    $stars .= '</span>';
	
    return $stars;

}

// ------------------------------------------------------------------------

function load_admin_tpl($type = '', $file = '', $data = '', $top = true, $bottom = true)
{
	$CI =& get_instance();
	
	$template = '';
	
	echo header('Content-type: text/html; charset=' . $CI->config->item('charset'));
	
	if ($top == true)
	{
		//load header file
		$CI->load->view('admin/tpl_adm_header', $data);
	}
	
	//load content
	$CI->load->view($type . '/' . $file, $data);
	
	if ($bottom == true)
	{
		//load footer
		$CI->load->view('admin/tpl_adm_footer', $data);
	}
	
	//echo $template;
}

// ------------------------------------------------------------------------


function get_bread_crumbs($type = '')
{
	$CI = & get_instance();
	$a = '<ul class="breadcrumb"><li><a href="' . base_url() . '"><i class="fa fa-home"></i></li><li> ' . lang('Dashboard') . ' </a></li>';
	
	//$a .= '<li class="pull-right date_blue" >' . lang('Logged_in_as') .' ' . $CI->session->userdata('username') . ' / ' . lang('Last_login') . ': ' . $CI->session->userdata('ll_date') . ' / ' . $CI->session->userdata('ll_ip') . '</li>';
	if($CI->uri->segment(1) == 'member' && $CI->uri->segment(2) == 'dashboard'){
		// Do nothing
	}
	else if($CI->uri->segment(1) == 'member' && $CI->uri->segment(2) == !NULL){
		$a .= 	'<li>'.'<a href="'.$CI->uri->segment(2).'">'.str_replace('_', ' ', humanize($CI->uri->segment(2))).'</a>'.'</li>';
	}
	else if($CI->uri->segment(1) != 'member'){
		$a .= 	'<li>'.'<a href="'.$CI->uri->segment(2).'">'.str_replace('_', ' ', humanize($CI->uri->segment(1))).'</a>'.'</li>';
	}
	$a .= '</ul>';

	return $a;
}

// ------------------------------------------------------------------------

function _previous_next($type = 'next', $table = '', $id = '', $tid = false, $sql = '')
{
	$CI = & get_instance();

	if ($tid == true)
	{
		if ($id >= '0')
		{
			if ($type == 'previous')
			{
				return '<a href="' . base_url('js') . $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/' . $CI->uri->segment(3) . '/' . $id . '/' . $CI->uri->segment(5,0). '/' . $CI->uri->segment(6,0) . '/' . $CI->uri->segment(7,0). '/' . $CI->uri->segment(8,0). '/' . $CI->uri->segment(9,0) . '/' . $CI->uri->segment(10,0) . '" class="btn btn-primary tip" data-toggle="tooltip" title="' . lang('previous') . '"><i class="icon-chevron-sign-left"></i></a>';
			}
		
			return '<a href="' . base_url('js') . $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/' . $CI->uri->segment(3) . '/' . $id . '/' . $CI->uri->segment(5,0). '/' . $CI->uri->segment(6,0) . '/' . $CI->uri->segment(7,0). '/' . $CI->uri->segment(8,0). '/' . $CI->uri->segment(9,0) . '/' . $CI->uri->segment(10,0) . '" class="btn btn-primary tip" data-toggle="tooltip" title="' . lang('next') . '"><i class="icon-chevron-sign-right"></i></a>';
		}
		
		return;
	}
	
	$sort = $type == 'next' ? 'ASC' : 'DESC';
	$less = $type == 'next' ? ' > ' : ' < ';
	
	switch ($table)
	{
		case 'discount_groups':
		case 'affiliate_groups':
			$key = 'group_id';
		break;
		
		case 'admin_users':
			$key = 'admin_id';
		break;
		
		case 'coupons':
			$key = 'coupon_id';
		break;
		
		case 'languages':
			$key = 'language_id';
		break;
		
		case 'currencies':
			$key = 'currency_id';
		break;
		
		case 'tax_zones':
			$key = 'tax_zone_id';
		break;
		
		case 'regions':
			$key = 'region_id';
		break;
		
		case 'countries':
			$key = 'country_id';
		break;
				
		case 'vendors':
			$key = 'vendor_id';
		break;
		
		case 'modules':
			$key = 'module_id';
		break;
		
		case 'members':
			$key = 'member_id';
		break;
		
		case 'email_mailing_lists':
			$key = 'mailing_list_id';
		break;
		
		case 'invoices':
			$key = 'invoice_id';
		break;
		
		case 'products':
			$key = 'product_id';
		break;
		
		case 'products_categories':
		case 'content_categories':
		case 'faq_categories':
		case 'attribute_categories':
			$key = 'category_id';
		break;
		
		case 'products_attributes':
			$key = 'attribute_id';
		break;
		
		case 'content_articles':
		case 'faq_articles':
			$key = 'article_id';
		break;
		
		case 'manufacturers':
			$key = 'manufacturer_id';
		break;
		
		case 'affiliate_commissions':
			$key = 'comm_id';
		break;
		
		case 'support_tickets':
			$key = 'ticket_id';
		break;
		
		case 'support_categories':
			$key = 'support_category_id';
		break;
		
		default:
			$key = 'id';
		break;
			
	}
	
	$sql = 'SELECT ' . $key . ' FROM ' . $CI->db->dbprefix($table) . ' WHERE ' . $key . $less . $id . ' ' . $sql . ' ORDER BY ' . $key . ' ' . $sort . ' LIMIT 1';

	$query = $CI->db->query($sql);
	
	if ($query->num_rows() > 0)
	{
		$row = $query->row_array();
		
		if ($type == 'previous')
		{
			return '<a href="' . base_url('js') . $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/' . $CI->uri->segment(3) . '/' . $row[$key] . '" class="btn btn-primary tip" data-toggle="tooltip" title="' . lang('previous') . '"><i class="icon-chevron-sign-left"></i></a>';
		}
		
		return '<a href="' . base_url('js') . $CI->uri->segment(1) . '/' . $CI->uri->segment(2) . '/' . $CI->uri->segment(3) . '/' . $row[$key] . '" class="btn btn-primary tip" data-toggle="tooltip" title="' . lang('next') . '"><i class="icon-chevron-sign-right"></i></a>';
	}
	 
	return  false;
}

// ------------------------------------------------------------------------

function _show_msg($type = '', $msg = '' )
{
	$CI = & get_instance();
	
	if ($type == 'error')
	{
		return '<div class="col-lg-12"><div class="alert alert-danger animated shake capitalize"><button type="button" class="close" data-dismiss="alert">×</button><h4><i class="icon-exclamation-sign"></i> '. lang('errors_in_submission') . '</h4>' . $msg . '</div></div>';	
	}
	
	return '<div class="col-lg-12 alert-msg"><div class="alert alert-success animated fadeIn capitalize"><button type="button" class="close" data-dismiss="alert">×</button><i class="icon-thumbs-up"></i> ' . $msg . '</div></div>';
}

// ------------------------------------------------------------------------

function _load_custom_form_fields($type = '') //load the custom fields for use on forms
{
	$CI = & get_instance();
	
	$CI->db->order_by($CI->config->item('dbs_cff_column'), $CI->config->item('dbs_cff_order')); 	
	$CI->db->where('form_type', $type);
	$query = $CI->db->get('form_fields');
	
	if ($query->num_rows() > 0)
	{
		return $query->result_array();
	}
	 
	return  false;
}

// ------------------------------------------------------------------------

function _initialize_js_header($type = '', $exclude_array = array())
{
	$CI = & get_instance();
	
	$a = array('jquery.js', 'tabs/ui.tabs.pack.js', 'forms/jquery.form.js', 'dimensions/jquery.dimensions.js', 
			   'accordion/jquery.accordion.js', 'autocomplete/jquery.autocomplete.js', 'thickbox/thickbox.js', 
			   'popupwindow/jquery.popupwindow.js', 'scrollto/jquery.scrollto.js', 'smoothmenu/ddsmoothmenu.js', 'common.js'
			   );
	
	$js = '';
	
	foreach ($a as $v)
	{
		if (in_array($v, $exclude_array))
		{
			continue;
		}
		else
		{			
			$js .= '<script language="JavaScript" type="text/javascript" src="' . base_url() . 'js/' . $v . '"></script>';
			$js .= "\n";
		}
	}
	
	return $js;
}

// ------------------------------------------------------------------------

function _load_page($file = '')
{
	ob_start();
	
	include($file);
	
	$buffer = ob_get_contents();
	
	@ob_end_clean();
	
	return $buffer;
}

// ------------------------------------------------------------------------

function load_field_type($type = '', $options = '', $selected = '', $class = '', $mod = 'public')
{
	switch ($type)
	{
		case "text":
			
			$data = '<input name="' . $options['form_field_name'] . '" type="text" id="' . $options['form_field_name'] . '" value="' . $selected . '" class="' . $class .'" placeholder="' . $options['form_field_description'] . '"/>';
			
		break;
		
		case "hidden":
			
			if ($mod == 'admin')
			{
				$data = '<input name="' . $options['form_field_name'] . '" type="text" id="' . $options['form_field_name'] . '" value="' . $selected . '" class="' . $class .'"/>';
			}
			else
			{
			
				$data = '<div>' . $selected . '<input name="' . $options['form_field_name'] . '" type="hidden" id="' . $options['form_field_name'] . '" value="' . $selected . '" class="' . $class .'"/></div>';
			}
			
		break;
		
		case "textarea":
		
			$data = '<textarea name="' . $options['form_field_name'] . '" id="' . $options['form_field_name'] . '" cols="45" rows="5" class="' . $class .'">' . $selected . '</textarea>';
		
		break;
		
		case "dropdown":
		
			$data = '<select name="' . $options['form_field_name'] . '" class="' . $class .'">';
			
			//explode the field values
			$values = explode(",", $options['form_field_values']);
			
			foreach ($values as $v)
			{
				//explode the name value pairs
				$value = explode("|", $v);
				
				if (!empty($value[0]))
				{
					if ($value[0] == $selected)
					{
						$data .= '<option value="' . trim($value[0]) . '" selected="selected">' . trim($value[1]) . '</option>';
					}
					else
					{
						$data .= '<option value="' . $value[0] . '">' . $value[1] . '</option>';
					}
				}
			}

			$data .= '</select>';
		
		break;
	}
	
	return $data; 
}

// ------------------------------------------------------------------------

function _generate_form_dropdown($form_type = '', $form_name = '', $form_values = '', $form_selected = '', $form_options = '', $value = '', $name = '', $selected = '')
	{
		/*
		|------------------------------------------------------
		| this generates the form input such as text / textarea
		|
		| -----------------------------------------------------
		| 
		| $form_type = type of form such as text or dropdown
		| $form_name = the name / id for the form field
		| $form_values = options for the form
		| $form_selected = the selected values if Any.
		| $form_options = extra form field code
		| $value = form dropdown value
		| $name = form dropdown name
		| $selected = the selected id in the dropdown
		|
		|------------------------------------------------------
		*/
		
		$CI = & get_instance();
		
		//first format the form_values array...
		$options = format_array($form_values, $value, $name);
		
		//and the form_selected array
		$selected_options = format_array($form_selected, $selected);
		
		switch ($form_type)
		{
			case "dropdown":
				$m = form_dropdown($form_name, $options, $selected_options, $form_options);
			break;
			
			case "multiple-dropdown":
				$m = form_dropdown($form_name.'[]', $options, $selected_options, 'multiple="multiple" id="' . $form_name . '"' . $form_options . '"');
			break;

		}
		
		return $m;
		
	}
	
// ------------------------------------------------------------------------

function _format_invoice_balance($amount = '', $options = '')
{
	$CI = & get_instance();
	
	if ($amount >= 0)
	{
		$a = lang('print_invoice_balance_due') . ': &nbsp;&nbsp;';
	}
	else
	{
		
		$a = lang('print_invoice_credit_invoice') . ': &nbsp;&nbsp;';
	}
	
	$a .= format_amounts($amount, $options);
	$a = str_replace('-', '+', $a);
	if ($amount > 0)
	{
		$a = '<span class="amount-due">' . $a . '</span>';
	}
	else
	{
		$a = '<span class="invoice-paid">' . $a . '</span>';
	}
	


	return $a;
}

// ------------------------------------------------------------------------

function _generate_settings_field($v = '', $value = '', $attributes = '')
{
	$CI = & get_instance();
	
	switch ($v['settings_type'])
	{
		case 'textarea':
			
		switch ($v['settings_function'])
		{
			case 'base64_decode';
			
				$value = base64_decode($value);
				
			break;
		}
		
		$data = array(
						  'name'	=> $v['settings_key'],
						  'id'	=> $v['settings_key'],
						  'value'	=> $value,
						  'class'	=> 'form-control ' . $attributes
						 );

			return form_textarea($data);
				
		
		break;
		
		case 'text':
		
			switch ($v['settings_function'])
			{
				case 'license_id':
				
					if (defined('JEM_ENABLE_RESELLER_LINKS'))
					{
						return '<a href="' . $this->config->item('customizer_reseller_direct_url') .'">' . $this->config->item('customizer_reseller_company_name') . '</a>';
					}
				
				break;
			}
			
			$data = array(
						  'name'	=> $v['settings_key'],
						  'id'	=> $v['settings_key'],
						  'value'	=> $value,
						  'class'	=> 'form-control  ' . $attributes
						 );

			return form_input($data);
			
		break;
		
		case 'readonly':
		
			return $value;
		
		break;
		
		case 'dropdown':
			
			$attributes = array('id' => $v['settings_key'], 'class' => 'select');
			
			switch ($v['settings_function'])
			{
				case 'cc_auth_type':
				
					$options = array('AUTH_CAPTURE' => 'Authorization and Capture',
									 'AUTH_ONLY' => 'Authorization Only');
				
				break;
				
				case 'timezone_menu':
				
					$CI->load->helper('date');
					
					return timezone_menu($v['settings_value'], 'form-control', 'sts_store_default_timezone');
					
				break;
				
				case 'forced_matrix_spillover':
					
					$options = array(	'none' => lang('no_sponsor'),
									 	'random' => lang('random_affiliate'),
										'specific'	=> lang('specific_affiliate'),
									);
					
				break;
				
				case 'paper_size':
				
					$options = array(	'letter' => 'letter',
										'legal'	=> 'legal',
										'11x17' => '11x17');
										
				break;
				
				case 'paper_orientation':
					
					$options = array(	'landscape' => 'landscape',
										'portrait'	=> 'portrait');
										
				break;
				
				case 'order_status':
				
					$options = array(	'order_pending' => lang('order_pending'),
									 	'order_processing' => lang('order_processing'),
										'order_completed' => lang('order_completed')
									 
									 );
					
				break;
				
				case 'editor':
					
					$options = array(	'innovaeditor' => 'innovaeditor');
										
				break;
				
				case 'affiliate_performance_bonus_required':
					
					$options = array(	'commission_amount' => lang('commission_amount'),
										'sales_amount' => lang('sales_amount'));
				
				break;
				
				case 'homepage_redirect':
				
					$options = array(	'homepage' => 'homepage',
										'content'	=> 'content',
										'store'	=> 'store',
										);
					
				break;
					
				break;
				
				case 'cart_commissions':
					$options = array(	'total_sale' => lang('total_sale'),
										'per_product'	=> lang('per_product'));
				break;
				
				case 'affiliate_group':
					
					$array = $CI->db_validation_model->_get_details('affiliate_groups');
					$options = format_array($array, 'group_id', 'aff_group_name');
					
				break;
				
				case 'commission_refund_option':
				
					$options = array(	'none'	=>	lang('do_nothing'),
										'delete'	=>	lang('delete'),
										'pending'	=>	lang('pending')
									);
				
				break;
				
				case 'affiliate_link_type':
				
					$options = array('regular' => lang('regular'), 'subdomain' => lang('subdomain'), 'replicated_site' => lang('replicated_site'));
				
				break;
				
				case 'affiliate_link_username_id':
					
					$options = array('username' => lang('username'), 'id' => lang('id'));
				
				break;
				
				case 'video_control_bar':
					
					$options = array('bottom' => 'bottom', 'over' => 'over', 'none' => 'none');
					
				break;
				
				case 'affiliate_performance_bonus':
				
					$options = array('group_upgrade' => lang('group_upgrade'), 'payment_amount' => lang('payment_amount'));
				
				break;
				
				case 'affiliate_program_type':
				
					$options = array(	'pay-per-sale' => lang('pay-per-sale'), 
										//'pay-per-lead' => lang('pay-per-lead'), 
										//'pay-per-click' => lang('pay-per-click')
										);
					
				break;
				
				case 'delimiter':
				
					$options = array('comma' => 'comma', 'tab' => 'tab', 'semicolon' => 'semicolon');
				
				break;
				
				case 'product_listing_style':
					
					$options = array('grid' => 'grid', 'list' => 'list');
				
				break;
				
				case 'backup_option':
				
					$options = array('daily' => lang('daily'), 'weekly' => lang('weekly'), 'monthly' => lang('monthly'));
				
				break;
				
				case 'boolean':
					
            		$options = array('1' => lang('enable'), '0' => lang('disable'));
            		
				break;
				
				case 'commission_type':
					$options = array('flat' => lang('flat'), 'percent' => lang('percent'));
				break;
				
				case 'countries':
					
					$CI->load->helper('country');		
		
					$options = _load_countries_dropdown($countries = _load_countries_array());

				break;
				
				case 'currencies':	
				
					$currency_array = $CI->db_validation_model->_get_details('currencies');
					$options = format_array($currency_array, 'code', 'title');
				
				break;
				
				case 'date_format':
				
					$options = array(
										'mm/dd/yyyy:m/d/Y:M d Y' => 'mm/dd/yyyy',
										'dd/mm/yyyy:d/m/Y:d M Y' => 'dd/mm/yyyy',
										//'yyyy/mm/dd' => 'yyyy/mm/dd',
									);
				
				break;
				
				
				case 'image_library':
				
					$options = array('GD' => 'GD', 'GD2' => ' GD2', 'ImageMagick' => 'ImageMagick', 'NetPBM' => 'NetPBM');
					
				break;
				
				case 'language':
					
					$lang_array = $CI->db_validation_model->_get_details('languages');

					$lang_admin_array = array();
					
					foreach ($lang_array as $lang_value)
					{
						$lang_value_path = APPPATH . 'language/' . $lang_value['name'] . '/adm_main_lang.php';
		
						if (file_exists($lang_value_path))
						{
							array_push($lang_admin_array, $lang_value);
						}
					}
					
					$options = format_array($lang_admin_array, 'name', 'name');
					
				break;
				
				case 'pending_commission':
					$options = array('no_pending' => lang('pending_no_email'), 
									 'alert_pending' => lang('pending_send_email'), 
									 'no_unpaid' => lang('unpaid_no_email'), 
									 'alert_unpaid' => lang('unpaid_send_email'));
				break;
				
				case 'mailer_type':
					
					$options = array('php' => 'php', 'smtp' => 'smtp', 'sendmail' => 'sendmail', 'qmail' => 'qmail');
				
				break;
				
				case 'player_link':
					
					$options = array('play'	=> 'play', 'link'	=> 'link');
				
				break;
				
				case 'ssl_tls':
					
					$options = array('none' => 'none', 'ssl' => 'ssl', 'tls' => 'tls');
					
				break;
				
				case 'mailing_list':
				
					$list_array = $CI->db_validation_model->_get_details('email_mailing_lists');
					$options = format_array($list_array, 'mailing_list_id', 'mailing_list_name', true, 'none');	
				
				break;
				
				case 'numbers_5':
					
					$options = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5);
				
				break;
				
				case 'numbers_10':
					
					$options = array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10);
				
				break;
				
				case 'matrix_width':
				
					for ($i=2; $i <=5; $i++)
					{
						$options[$i] = $i;
					}
				
				break;
				
				case 'numbers_30':
				
					$i = 1;
					while ($i <=30)
					{
						$options[$i] = $i;
						$i++;
					}
				break;
				
				case 'numbers_50':

					$options = array(50 => 50, 100 => 100, 250 => 250, 500 => 500, 1000 => 1000);
					
				break;
				
				case 'enable':
				
					$options = array(0 => lang('disable'), 1 => lang('enable'));
				
				break;
				
				case 'theme':
					
					$template_array = $CI->db_validation_model->_get_details('layout_themes');
					$options = format_array($template_array, 'template_id', 'template_name');
					
				break;
				
				case 'weight':
					
					$options = array('lbs' => lang('lbs'), 'kg' => lang('kg'));
				
				break;
				
				case 'tax_zone':
				
					$array = $CI->db_validation_model->_get_details('tax_zones');
					$options = format_array($array, 'tax_zone_id', 'tax_zone_name');	
					
				break;
				
				case 'zone_calc':
				
					$options = array('item' => lang('item'), 'price' => lang('price'), 'weight' => lang('weight'));
					
				break;
				
				case 'address_type':
				
					$options = array('billing' => lang('billing'), 'shipping' => lang('shipping'));
				
				break;
				
				case 'members_dashboard_template':
					$options = array('0' => 'tpl_members_dashboard', '2' => 'tpl_members_dashboard2', '3' => 'tpl_members_dashboard3',
									'4' => 'tpl_members_dashboard4', '5' => 'tpl_members_dashboard5', '6' => 'tpl_members_dashboard6');
				
				break;
				
				case 'product_details_template':
					$options = array('0' => 'tpl_product_details', '2' => 'tpl_product_details2', '3' => 'tpl_product_details3',
									'4' => 'tpl_product_details4');
				
				break;
			}
			
			return form_dropdown($v['settings_key'], $options, $value, 'class="form-control select" id="' . $v['settings_key'] . '"');
	
		break;
	}
	
}

// ------------------------------------------------------------------------

function _check_close_buttons($type = '')
{
	$CI = & get_instance();
	
	if ($type == 'click')
	{
		return 'window.opener.location.reload();self.close();return false;';
	}
	else
	{
		return 'href="javascript:void(0);" onClick="window.opener.location.reload();self.close();return false;"';
	}
}

// ------------------------------------------------------------------------

function _check_admin_popups($url = '', $name = '', $height = '300', $width = '500', $class = '', $style = '')
{
	
	$atts = array(
              'width'      => $width,
              'height'     => $height,
              'scrollbars' => 'yes',
              'status'     => 'no',
              'resizable'  => 'yes',
              'screenx'    => '50',
              'screeny'    => '50'
            );

		return anchor_popup( $url , $name, $atts, $class, $style);
		
}

// ------------------------------------------------------------------------

function _check_thickbox()
{
	$CI = & get_instance();
	
	if ($CI->config->item('sts_image_enable_thickbox') == 1)
	{
		if ($CI->config->item('images_thickbox_class'))
		{
			return 'class="' . $CI->config->item('images_thickbox_class') . '"';
		}
		else
		{
			return 'class="thickbox"';
		}
		
		
		//return 'class="MagicZoom MagicThumb"';
		//return 'class="MagicZoom MagicThumb" rel="zoom-width: 400px; zoom-position: bottom';
	}
	else
	{
		return 'class="popupwindow" rel="windowCenter" target="name"';
	}
}
// ------------------------------------------------------------------------

function _generate_thumb($type = 'products', $v = array(), $resize = 1)
{
	$CI = & get_instance();
	
	$a = base_url() . 'images/' .  $type . '/' . $v['photo_file_name'];
	
	if ($resize == 1)
	{
		if (file_exists(PUBPATH . '/images/' .  $type . '/' . $v['raw_name'].'_jrox'.$v['file_ext']))
		{
			$a = base_url() . 'images/' .  $type . '/' . $v['raw_name'].'_jrox'.$v['file_ext'];
		}
	}
	
	return $a;	
}

// ------------------------------------------------------------------------

function _check_image_thumbnail($type = '', $v = '', $resize = 1, $height = '', $width = '', $thickbox = true, $rel = true, $class = 'thumb-images')
{
	$CI = & get_instance();
	
	$a = base_url() . 'images/' .  $type . '/' . $v['photo_file_name'];
	
	if ($resize == 1)
	{
		if (file_exists(PUBPATH . '/images/' .  $type . '/' . $v['raw_name'].'_jrox'.$v['file_ext']))
		{
			$a = base_url() . 'images/' .  $type . '/' . $v['raw_name'].'_jrox'.$v['file_ext'];
		}
	}
	
	if ($thickbox == true)
	{
		$rel_code = $rel == true ? 'rel="products"' : '';
		
		$b = base_url() . 'images/' .  $type . '/' . $v['photo_file_name'];
		$thickbox_code = _check_thickbox();
		$url = '<a href="' . $b . '" ' . $thickbox_code . ' ' . $rel_code . '>';
		$url .=  '<img src="' . $a . '" class="' . $class . '" alt="' . $v['photo_file_name'] . '" />';
	    $url .= '</a>';
		
	}
	else
	{
		$url = '<img src="' . $a . '" class="' . $class . '" alt="' . $v['photo_file_name'] . '"/>';
	}
	
	

	return $url;  
}

// ------------------------------------------------------------------------

function _generate_dynamic_tags($textarea = '', $url = false, $dynamic_tags = true)
{
	$CI =& get_instance();
	
	if ($dynamic_tags == true)
	{
		//setup dynamic tags
		$data = '<div>';
		$data .= '<select id="allowed_tags" class="form-control">';
		$data .= '<option value=\'\'>' . lang('allowed_tags') . '</option>';	
			
		if ($url == true) //use for menu maker
		{
			$options = array(	'home', 'store', 'members_login', 'view_cart', 'product_categories', 'manufacturers', 'new_products', 'content',
								'terms_of_service', 'privacy_policy', 'about_us', 'contact_us'
							);
			
			sort($options);
			foreach ($options as $v)
			{
				$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
			}	
		}
		else
		{
		
				
			$data .= '<option value="{store_name}">' . lang('store_name') . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {store_name}&nbsp;&nbsp;&nbsp; </option>';
			$data .= '<option value="{login_url}">' . lang('login_url') . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {login_url}&nbsp;&nbsp;&nbsp; </option>';
			
			switch ($textarea)
			{
				case 'member_payment_invoice_template':
				
					$options = array('customers_name' , 'customers_company', 'invoice_date', 'due_date',
									 'customers_address_1', 'customers_address_2', 'customers_city',
									 'customers_state', 'customers_country', 'customers_postal_code',
									 'shipping_name', 'shipping_address_1', 'shipping_address_2', 'shipping_company',
									 'shipping_city', 'shipping_state', 'shipping_country', 'order_status',
									 'shipping_postal_code', 'customers_telephone', 'payment_link',
									 'tax_amount', 'shipping_amount', 'customers_primary_email',
									 'products_html', 'products_text', 'payments_html', 'payments_text',
									 'total_amount', 'subtotal_amount', 'total_payments', 'balance_due', 'invoice_id',
									 'store_name', 'store_phone', 'store_shipping_address', 'store_shipping_city',
									 'store_shipping_state', 'store_shipping_postal_code','store_shipping_country',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
									 
									 
									 
									
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}	
					
				break;
				
				case 'admin_alert_comment_moderation_template':
				
					$options = array('article_id', 'article_url' ,'article_comment');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_affiliate_commission_generated_template':
				
					$options = array('member_username', 'commission_amount' ,'fname', 'current_date', 'admin_login_url');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
				
				break;
				
				case 'admin_alert_contact_us_template':
					
					$options = array('current_time', 'contact_email' ,'contact_message_html', 'contact_message_text');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_alert_new_content_comment_template':
				
					$options = array('article_id', 'article_url' ,'article_comment');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
				
				break;
				
				case 'admin_alert_new_order_template':
					
					$options = array('fname', 'customer_name' ,'invoice_id', 'invoice_amount', 'transaction_id', 'payment_method');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_alert_new_signup_template':
					
					$options = array('member_name', 'member_username' ,'signup_ip', 'current_time');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_alert_product_review_template':
					
					$options = array('product_id', 'product_url' ,'review_comment');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_alert_support_ticket_response':
					
					$options = array('current_time', 'ticket_id' ,'ticket_priority', 'ticket_subject', 'ticket_message');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
								
				case 'admin_failed_login_template';
				
					$options = array('admin_login_url', 'username' ,'password', 'date', 'ip_address');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
				
				break;
				
				case 'admin_product_inventory_alert_template';
				
					$options = array('product_name', 'inventory_alert_level' ,'product_id', 'product_sku', 'current_inventory');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
				
				break;
				
				case 'admin_reset_password_template':
					
					$options = array('username', 'new_password');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_ticket_deletion_template':
					
					$options = array('fname', 'ticket_id');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_vendor_alert_template':
					
					$options = array('invoice_id', 'product_name', 'product_sku', 'quantity', 'shipping_fname', 'shipping_lname', 'shipping_address_1', 'shipping_address_2', 'shipping_city', 'shipping_state', 'shipping_postal_code', 'shipping_country');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'assign_support_ticket_alert_template':
					
					$options = array('fname', 'ticket_id');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_affiliate_downline_signup':
					
					$options = array('downline_name', 'downline_email', 'downline_username', 'fname');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_affiliate_commission_generated_template':
				
					$options = array('commission_amount', 'current_date',
									'store_link', 'affiliate_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'payment_name', 'payment_address_1', 'payment_address_2',
									 'payment_city', 'payment_state', 'payment_country', 
									 'payment_postal_code', 'home_phone', 'work_phone',
									 'mobile_phone', 'fax', 'website',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_affiliate_payment_sent_template':
				
					$options = array('payment_amount', 'affiliate_note');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_affiliate_performance_bonus_amount_template':
				
					$options = array('bonus_amount','store_link', 'affiliate_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'payment_name', 'payment_address_1', 'payment_address_2',
									 'payment_city', 'payment_state', 'payment_country', 
									 'payment_postal_code', 'home_phone', 'work_phone',
									 'mobile_phone', 'fax', 'website',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_affiliate_performance_group_upgrade_template':
				
					$options = array('upgraded_affiliate_group','store_link', 'affiliate_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'payment_name', 'payment_address_1', 'payment_address_2',
									 'payment_city', 'payment_state', 'payment_country', 
									 'payment_postal_code', 'home_phone', 'work_phone',
									 'mobile_phone', 'fax', 'website',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_affiliate_send_downline_email':
					
					$options = array('downline_sponsor_name', 'downline_sponsor_email', 'downline_sponsor_affiliate_link',
									 'downline_member_name', 'downline_member_username', 'downline_member_email',
									 'downline_member_id', 'downline_member_affiliate_link', 'downline_message_html',
									 'downline_message_text');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_email_confirmation_template':
					
					$options = array('fname', 'confirm_link');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_login_details_template':
					
					$options = array('store_link','affiliate_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'payment_name', 'payment_address_1', 'payment_address_2',
									 'payment_city', 'payment_state', 'payment_country', 
									 'payment_postal_code', 'home_phone', 'work_phone',
									 'mobile_phone', 'fax', 'website',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'admin_offline_credit_card_email_template':
					
					$options = array('store_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'home_phone', 'work_phone', 'cc_digits', 
									 'mobile_phone', 'fax', 'website', 'invoice_id',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_order_status_change_template':
					
					$options = array('fname', 'invoice_id', 'new_order_status', 
									 'product_type', 'member_id', 'customers_name',
									 'customers_company', 'customers_address_1',
									 'customers_address_2', 'customers_city',
									 'customers_postal_code', 'customers_state',
									 'customers_country', 'customers_telephone',
									 'customers_primary_email', 'shipping_name',
									 'shipping_company', 'shipping_address_1',
									 'shipping_address_2', 'shipping_city',
									 'shipping_state', 'shipping_country',
									 'shipping_method', 'shipping_tracking_id');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'member_reset_password_template':
					
					$options = array('fname', 'new_password' );
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
								
				case 'support_ticket_response_template':
					
					$options = array('fname', 'ticket_id', 'admin_fname', );
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
					
				case 'email_send-member':
				case 'email_send-mailing_list':
				
					$options = array('affiliate_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'payment_name', 'payment_address_1', 'payment_address_2',
									 'payment_city', 'payment_state', 'payment_country', 
									 'payment_postal_code', 'home_phone', 'work_phone',
									 'mobile_phone', 'fax', 'website',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}	
					
				break;
				
				case 'followups':
					
					$options = array('affiliate_link', 
									 'fname' , 'lname', 'username', 'company',
									 'billing_address_1', 'billing_address_2', 'billing_city',
									 'billing_state', 'billing_country', 'billing_postal_code',
									 'payment_name', 'payment_address_1', 'payment_address_2',
									 'payment_city', 'payment_state', 'payment_country', 
									 'payment_postal_code', 'home_phone', 'work_phone',
									 'mobile_phone', 'fax', 'website',
									 'store_address', 'store_city', 'store_state', 
									 'store_country', 'store_postal_code', 'store_phone',
									 'current_date', 'current_time', 'signup_ip',
									 'jrox_custom_field_1', 'jrox_custom_field_2',
									 'jrox_custom_field_3', 'jrox_custom_field_4',
									 'jrox_custom_field_5', 'jrox_custom_field_6',
									 'jrox_custom_field_7', 'jrox_custom_field_8',
									 'jrox_custom_field_9', 'jrox_custom_field_10',
									 'member_id', 'mailing_list_id', 'unsubscribe_link');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}	
					
				break;
				
				case 'textads':
				case 'articleads':
				case 'htmlads':
				case 'emailads':
					
					$options = array('store_link', 'affiliate_link', 'fname' , 'lname', 'username', 'primary_email',
									 'custom_field_1','custom_field_2','custom_field_3', 'custom_field_4','custom_field_5',
									 'custom_field_6','custom_field_7','custom_field_8', 'custom_field_9', 'custom_field_10',
									);
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}		
					
				break;
				
				case 'email_template_body_html':
				case 'send_member_email':			
				
					$options = array('username', 'fname' , 'lname');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}				
				break;
				
				case 'product_overview':
				case 'product_description_1':
				case 'product_description_2':
					
					$options = array('product_name', 'product_price' , 'product_type', 'product_sku', 
									 'product_weight', 'product_inventory', 'min_quantity_ordered', 
									 'max_quantity_ordered');
					
					foreach ($options as $v)
					{
						$data .= '<option value="{' . $v . '}">' . lang($v) . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {' . $v . '}&nbsp;&nbsp;&nbsp; </option>';
					}	
					
					for ($i = 1; $i<=$CI->config->item('sts_affiliate_commission_levels'); $i ++)
					{
					$data .= '<option value="{custom_commission_value_'.$i.'}">' . lang('custom_commission_value_'.$i.'') . ' &nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp; {custom_commission_value_'.$i.'}&nbsp;&nbsp;&nbsp; </option>';
					}
								
				break;
			}
		}
		
		$data .= '</select>';
					
		$data .= '</div>';
		
		return $data;
	}
		
	return false;
}

// ------------------------------------------------------------------------

function _check_language_translation($name = '')
{
	$CI =& get_instance();
	
	if ($CI->config->item('sts_content_translate_menus') == 1)
	{
		if (!$CI->config->item('cfg_disable_capitalize_title_headings'))
		{
			//return ucfirst(lang($name));
		}
		
		//set spaces to underscros for language translation
		
		$name = str_replace(' ', '_', $name);
		
		if (lang(strtolower($name)))
		{							   
			return lang(strtolower($name));
		}
	}
	
	//if no translation just replace underscores
	$name = str_replace('_', ' ', $name);
	
	if (!$CI->config->item('cfg_disable_capitalize_title_headings'))
	{
		return ucfirst($name);
	}
	return $name;
}

// ------------------------------------------------------------------------

function _generate_menu_id($id = '', $type = 'id')
{
	if(!empty($id))
	{
		$id = $type .'="' . $id . '"';	
	}
	
	return $id;
}

// ------------------------------------------------------------------------

function _htmlentities($text = '')
{
	$CI =& get_instance();
	
	if (!$CI->config->item('jem_set_disable_htmlentities'))
	{
		$text = htmlentities($text);
	}
	
	return $text;
}

// ------------------------------------------------------------------------

function _generate_nav_menu($data = '', $class = 'nav', $id = 'nav-one', $type = 'horizontal', $aff_mark = '')
{

	//generates the menus for the site
	
	if (empty($data)) { return false; }
	
	$a = unserialize($data);
	
	$aff_array = array('{members_marketing}', '{members_email_downline}',
						'{members_payments}', '{members_commissions}',
						'{members_downline}', '{members_coupons}'
						);
	$html = '<ul class="' . $class . '">';
	
	$i = 1;
	$j = 1;
	
	//check for correct class
	switch ($id)
	{
		case 'footer':
			$link_class = 'Bottom'; 
		break;
		
		case 'left-menu':
			$link_class = 'Left';
		break;
		
		case 'jroxRightMenu':
			$link_class = 'Right';
		break;
		
		default:
			$link_class = 'Top';
		break;
	}

	foreach ($a as $k => $v)
	{
		$uri_class ='';
		if ($link_class == 'Top') { $uri_class = _check_menu_class($v['menu_url']); }
		
		if (is_array($v['subs']) && count($v['subs']) > 0)
		{
			if ($aff_mark == 0 && in_array($v['menu_url'], $aff_array)) continue;
			
			$html .= ' <li class="dropdown">' . _check_nav_url($v, true);
			$html .= '<ul class="dropdown-menu">';
				
			//generate the submenus 
			foreach ($v['subs'] as $c)
			{
				if ($aff_mark == 0 && in_array($c['menu_url'], $aff_array)) continue;
				
				$html .= '    <li><a href="' . _generate_site_urls($c['menu_url']) . '" ' . html_entity_decode($c['menu_options']) . '><span>' . _check_language_translation($c['menu_name']) . '</span></a></li>';
				$j++;
				
			}
			
			$html .= '  </ul>';
		}
		else
		{
			if ($aff_mark == 0 && in_array($v['menu_url'], $aff_array)) continue;
			
			$html .= ' <li class="jroxMenu' . $link_class . 'Links">' . _check_nav_url($v);
		}
		
		$html .= '</li><li class="divider-vertical"></li>';
		
		$i++;
	}
	
	$html .= '</ul>';
	
	return $html;

}

// ------------------------------------------------------------------------

function _check_nav_url($v = '', $caret = false)
{
	$n = _check_language_translation($v['menu_name']);
	
	if (empty($v['menu_url']))
	{
		return $n;	
	}
	
	$a = '<a href="' . _generate_site_urls($v['menu_url']) . '" ' . html_entity_decode($v['menu_options']). ' class="dropdown-toggle">' . $n;
	if ($caret == true)
	{
		$a .= ' <b class="caret"></b>';	
	}
	
	$a .= '</a>';
	
	return $a;
}

// ------------------------------------------------------------------------

function _generate_footer_menu($data = '', $class = 'footer', $id = '', $type = 'horizontal', $aff_mark = '')
{
	if (empty($data)) { return false; }
	
	$a = unserialize($data);
	
	$i = 1;
	$j = 1;
	
	//setup some formatting for links
	switch (count($a))
	{
		case '6':
		case '5';
			$span = 2;
		break;
		
		case '4':
		case '3':
			$span = 3;
		break;
		
		case '2':
			$span = 3;
			$sub_class = 'inline_footer';
		break;
		
		
	}
	//$span = 12 / count($a);
	
	$aff_array = array('{members_marketing}', '{members_email_downline}',
						'{members_payments}', '{members_commissions}',
						'{members_downline}', '{members_coupons}'
						);
						
	$html = '';
	$sub_class = '';
	$link_class = 'Bottom'; 
	foreach ($a as $k => $v)
	{
		$html .= '<div class="span' . $span . '"> <ul class="' . $class . '">';
		
		$uri_class ='';
		
		if (is_array($v['subs']) && count($v['subs']) > 0)
		{
			if ($aff_mark == 0 && in_array($v['menu_url'], $aff_array)) continue;
			
			$html .= ' <li id="' . $id . '-nav-' . $i . '" class="footer-list">' . _check_menu_url($v, $link_class, $uri_class);
			
			if ($type == 'vertical') $html .= '<span class="sf-sub-indicator"> &#187;</span>';
			
			$html .= '  <ul class="' . $sub_class . '">';
				
			//generate the submenus 
			foreach ($v['subs'] as $c)
			{
				if ($aff_mark == 0 && in_array($c['menu_url'], $aff_array)) continue;
				
				$html .= '    <li id="' . $id . '-subnav-' . $j . '" class="footer-sublist"><a href="' . _generate_site_urls($c['menu_url']) . '" ' . html_entity_decode($c['menu_options']) . '><span>' . _check_language_translation($c['menu_name']) . '</span></a></li>';
				$j++;
				
			}
			
			$html .= '  </ul>';
		}
		else
		{
			if ($aff_mark == 0 && in_array($v['menu_url'], $aff_array)) continue;
			
			$html .= ' <li id="' . $id . '-nav-one-' . $i . '" class="footer-list">' . _check_menu_url($v, $link_class, $uri_class);
		}
		
		$html .= '</li></ul>';
		
		$html .= '</div>';
		$i++;
	}
	
	return $html;
}

// ------------------------------------------------------------------------

function _generate_html_menu($data = '', $class = 'sf-menu', $id = 'nav-one', $type = 'horizontal', $aff_mark = '')
{

	//generates the menus for the site
	
	if (empty($data)) { return false; }
	
	$a = unserialize($data);
	
	$aff_array = array('{members_marketing}', '{members_email_downline}',
						'{members_payments}', '{members_commissions}',
						'{members_downline}', '{members_coupons}'
						);
	$html = '<ul id="' . $id . '" class="' . $class . '">';
	
	$i = 1;
	$j = 1;
	
	//check for correct class
	switch ($id)
	{
		case 'footer':
			$link_class = 'Bottom'; 
		break;
		
		case 'left-menu':
			$link_class = 'Left';
		break;
		
		case 'jroxRightMenu':
			$link_class = 'Right';
		break;
		
		default:
			$link_class = 'Top';
		break;
	}

	foreach ($a as $k => $v)
	{
		$uri_class ='';
		if ($link_class == 'Top') { $uri_class = _check_menu_class($v['menu_url']); }
		
		if (is_array($v['subs']) && count($v['subs']) > 0)
		{
			if ($aff_mark == 0 && in_array($v['menu_url'], $aff_array)) continue;
			
			$html .= ' <li id="' . $id . '-nav-' . $i . '" class="jroxMenu' . $link_class . 'Links">' . _check_menu_url($v, $link_class, $uri_class);
			
			if ($type == 'vertical') $html .= '<span class="sf-sub-indicator"> &#187;</span>';
			
			$html .= '  <ul>';
				
			//generate the submenus 
			foreach ($v['subs'] as $c)
			{
				if ($aff_mark == 0 && in_array($c['menu_url'], $aff_array)) continue;
				
				$html .= '    <li id="' . $id . '-subnav-' . $j . '" class="jroxMenuSubLinks"><a href="' . _generate_site_urls($c['menu_url']) . '" ' . html_entity_decode($c['menu_options']) . '><span>' . _check_language_translation($c['menu_name']) . '</span></a></li>';
				$j++;
				
			}
			
			$html .= '  </ul>';
		}
		else
		{
			if ($aff_mark == 0 && in_array($v['menu_url'], $aff_array)) continue;
			
			$html .= ' <li id="' . $id . '-nav-one-' . $i . '" class="jroxMenu' . $link_class . 'Links">' . _check_menu_url($v, $link_class, $uri_class);
		}
		
		$html .= '</li>';
		
		$i++;
	}
	
	$html .= '</ul>';
	
	return $html;

}

// ------------------------------------------------------------------------

function _check_menu_url($v = '', $link_class = '', $uri_class = '')
{
	$n = '<span class=" footer-link jroxMenuLink' . $link_class . '" ' . _generate_menu_id($uri_class) . '>' . _check_language_translation($v['menu_name']) . '</span>';
	
	if (empty($v['menu_url']))
	{
		return $n;	
	}
	
	$a = '<a href="' . _generate_site_urls($v['menu_url']) . '" ' . html_entity_decode($v['menu_options']). ' ' . _generate_menu_id($uri_class, 'class') . '>' . $n . '</a>';
	
	return $a;
}

// ------------------------------------------------------------------------

function _check_menu_class($url = '')
{
	$CI =& get_instance();
	
	if (!$CI->uri->segment(1) && $url == '{home}') { return 'jroxMenuLink_home'; }
	
	if ($url == '{' . $CI->uri->segment(1) . '}' || $url == '{' . $CI->uri->segment(2) . '}')
	{
		return 'jroxMenuLink_' . $CI->uri->segment(1);
	}
	
	if (function_exists('parse_url'))
	{
		//parse the url
		$seg = parse_url($url);
		
		$path = '/';
		$path .= !empty($seg['path']) ? $seg['path'] : '';
		
		$seg2 = $CI->uri->uri_string();
		
		if ($path == $seg2)
		{
			return 'jroxMenuLink_' . $CI->uri->segment(1);	
		}
	}
	
	//try to match to other segments
	$str = $CI->uri->segment(1) . '/' . $CI->uri->segment(2);
	
	$url = str_replace('{', '', $url);
	$url = str_replace('}', '', $url);
	switch ($url)
	{
		case 'store':
			
			if ($str == 'products/details') return 'jroxMenuLink_' . $url;
			if ($str == 'products/category') return 'jroxMenuLink_' . $url;
		
		break;
		
		case 'members_home':
			
			if ($str == 'members/') return 'jroxMenuLink_members_home';
		
		break;
		
		case 'members_support':
			if ($str == 'members/support') return 'jroxMenuLink_members_support';
		break;
		
		case 'members_invoices': 
			if ($str == 'members/invoices') return 'jroxMenuLink_members_invoices';
		break;
		
		case 'members_marketing':
			if ($str == 'members/marketing') return 'jroxMenuLink_members_marketing';
		break;
		
		case 'members_details':
			if ($str == 'members/account') return 'jroxMenuLink_members_details';
		break;
		
		case 'members_content':
			if ($str == 'members/content') return 'jroxMenuLink_members_content';
		break;

	}
	
	return false;
}

// ------------------------------------------------------------------------

function _generate_site_urls($url = '')
{
	//change the dynamic tags for URLs {link}
	$CI =& get_instance();
	
	switch ($url)
	{
		case '{about_us}':
			
			$url = str_replace("{about_us}", base_url() . CONTENT_ROUTE . '/view/about_us' , $url);
			return $url;
		
		break;
		
		case '{contact_us}':
			
			$url = str_replace("{contact_us}", base_url() . 'forms/contact_us', $url);
			return $url;
		
		break;
		
		case '{checkout}':
			
			$url = str_replace("{checkout}", base_url('checkout') . 'checkout', $url);
			return $url;
		
		break;
		
		case '{content}':
			
			$url = str_replace("{content}", base_url() . CONTENT_ROUTE , $url);
			return $url;
		
		break;
		
		case '{faq}':
			
			$url = str_replace("{faq}", base_url() . FAQ_ROUTE , $url);
			return $url;
		
		break;
		
		case '{home}':

			$url = str_replace("{home}", base_url(), $url);
			return $url;
		
		break;
		
		case '{rss_feeds}':
			
			$url = str_replace("{rss_feeds}", base_url() . 'rss', $url);
			return $url;
		
		break;

		case '{new_products}':
			
			$url = str_replace("{new_products}", base_url() . PRODUCTS_ROUTE . '/new_products', $url);
			return $url;
		
		break;
		
		case '{store}':
			
			$url = str_replace("{store}", base_url() . 'store' , $url);
			return $url;
		
		break;
		
		case '{members_login}':
			
			$url = str_replace("{members_login}", base_url('members', true) . 'login' , $url);
			return $url;
		
		break;
		
		case '{view_cart}':
			
			$url = str_replace("{view_cart}", base_url() . 'cart' , $url);
			return $url;
		
		break;
		
		case '{product_categories}':		
			
			$url = str_replace("{product_categories}", base_url() . CATEGORIES_ROUTE , $url);
			return $url;
		
		break;
		
		case '{manufacturers}':
			
			$url = str_replace("{manufacturers}", base_url() . 'manufacturers' , $url);
			return $url;
		
		break;
		
		case '{vendors}':
			
			$url = str_replace("{vendors}", base_url() . 'vendors' , $url);
			return $url;
		
		break;
		
		case '{sitemap}':
			
			$url = str_replace("{sitemap}", base_url() . 'sitemap' , $url);
			return $url;
		
		break;
		
		case '{terms_of_service}':
			
			$url = str_replace("{terms_of_service}", base_url() . CONTENT_ROUTE . '/view/tos' , $url);
			return $url;
		
		break;
		
		case '{privacy_policy}':
			
			$url = str_replace("{privacy_policy}", base_url() . CONTENT_ROUTE . '/view/privacy_policy' , $url);
			return $url;
		
		break;
		
		case '{members_support}':
		
			$url = str_replace("{members_support}", base_url('members')  . 'support/view' , $url);
			return $url;
		
		break;
		
		case '{members_invoices}':
		
			$url = str_replace("{members_invoices}", base_url('members')  . 'invoices/view' , $url);
			return $url;
		
		break;
		
		case '{members_payments}':
		
			$url = str_replace("{members_payments}", base_url('members')  . 'payments/view' , $url);
			return $url;
		
		break;
		
		case '{members_commissions}':
		
			$url = str_replace("{members_commissions}", base_url('members') . 'commissions/view' , $url);
			return $url;
		
		break;
		
		case '{members_home}':
		
			$url = str_replace("{members_home}", base_url('members')  , $url);
			return $url;
		
		break;
		
		case '{members_marketing}':
		
			$url = str_replace("{members_marketing}", base_url('members')  . 'marketing/view' , $url);
			return $url;
		
		break;
		
		case '{members_details}':
		
			$url = str_replace("{members_details}", base_url('members')  . 'account' , $url);
			return $url;
		
		break;
		
		case '{members_content}':
		
			$url = str_replace("{members_content}", base_url('members')  . 'content/view' , $url);
			return $url;
		
		break;
		
		case '{members_downline}':
		
			$url = str_replace("{members_downline}",  'javascript:void(window.open(\''. base_url('members')  . 'downline/view\', \'popup\', \'width=700,height=500, location=no, menubar=no, status=no,toolbar=no, scrollbars=yes, resizable=yes\'))', $url);
			return $url;
		
		break;
		
		case '{members_email_downline}':
			$url = str_replace("{members_email_downline}", base_url('members')  . 'downline/email' , $url);
			return $url;
		break;
		
		case '{members_coupons}':
		
			$url = str_replace("{members_coupons}", base_url('members')  . 'coupons/view' , $url);
			return $url;
		
		break;
		
		case '{members_downloads}':
		
			$url = str_replace("{members_downloads}", base_url('members')  . 'downloads/view' , $url);
			return $url;
		
		break;
		
		case '{members_memberships}':
		
			$url = str_replace("{members_memberships}", base_url('members')  . 'memberships/view' , $url);
			return $url;
		
		break; 
		
		//set sponsor data
		case '{sponsor_contact_email}':
		
			$get = get_cookie($CI->config->item('aff_cookie_name'));
			
			//if there is sponsor info
			if (!empty($get))
			{
				$user = explode('-', $CI->encrypt->decode($get, JROX_COOKIE_ENCRYPT));
					
				$sponsor = $CI->aff->_validate_user($user[1]);
				
				if (is_array($sponsor))
				{
					if (!empty($sponsor['primary_email']))
					{
						return 'mailto:' . $sponsor['primary_email'];	
					}
				}
			}
			
			return 'mailto:' . $CI->config->item('sts_store_email');
			
		break;
		
		
		default:
			return $url;
		break;
	}

}

// ------------------------------------------------------------------------

function _check_wysiwyg_disable($redirect = '')
{
	$CI = & get_instance();
	
	if ($CI->config->item('sts_admin_enable_wysiwyg_content') == '1')
	{
		if ($CI->session->userdata('admin_disable_wysiwyg') == true)
		{
			return '<a class="btn btn-primary" href="' . base_url() . 'content_articles/update_wysiwyg/enable/' . $redirect . '">
 			<i class="icon-check"></i> ' . lang('enable_wysiwyg') .'</a>';
		}
		else
		{
			return '<a class="btn btn-primary" href="' . base_url() . 'content_articles/update_wysiwyg/disable/' . $redirect . '">
 					<i class="icon-remove"></i> ' . lang('disable_wysiwyg') .'</a>';
		}
	}
	else
	{
		return false;
	}	
}
	
// ------------------------------------------------------------------------

function _initialize_html_editor($options = array(	'type' => 'content',
										'content' => '',
										'height'	=> '400',
										'width'	=> '100%',
										'editor_type'	=> 'description',
										'textarea'	=>	'txtContent',
										'instance'	=>	'oEdit1',
										'tags'	=> true,
									))
{
	$CI = & get_instance();
	
	$enable_html = $CI->config->item('sts_admin_enable_wysiwyg_content');
	
	if ($CI->session->userdata('admin_disable_wysiwyg') == true)
	{
		$enable_html = 0;	
	}
							   
							   
	
	//load plugins
	$CI->load->helper($CI->config->item('sts_content_html_editor'));


	$data['editor_path'] =  '<script language="Javascript" src="' . base_url('js') . 'js/scripts/language/en-US/editor_lang.js"></script>
								 <script language="Javascript" type="text/javascript" src="' . base_url('js') . 'js/scripts/innovaeditor.js"></script>';
	
	


	$html_type = $enable_html;
	
	$data['editor'] = HTML_Editor(	$options['instance'], 
									$html_type,  
									$options['editor_type'], 
									$options['content'], 
									$options['textarea'], 
									$options['height'], 
									$options['width'], 
									$options['tags']);
	
	return $data;
}

function _show_template_block($type = '', $class = '', $limit = 1, $show_on_columns = false)
{
	$CI = & get_instance();
	
	$data = '';
	
	switch ($type)
	{
		case 'referral_box':
		
			$CI->load->model('boxes/boxes_referrer_details_model', 'ref');
			
			$aff_data = $CI->ref->_generate_box();
			
			if (!empty($aff_data))
			{
				$data .= '<div ' . $class . '>';
				
				if (!empty($aff_data['sponsor_image']))
				{
					$data .=  '<p><img src="' . $aff_data['sponsor_image'] . '" class="referral-image img-polaroid" /></p>';
				}
		
				$data .= '<p><span class="referral-text">' . $aff_data['sponsor_text'] . '</span> ';
				$data .= '<span class="referral-name">' . $aff_data['name'] . '</span></br />';
				$data .= '<span class="referral-username">' . $aff_data['username'] . '</span>';
				$data .= '</p></div>';	
			}
		break;
		
		case 'facebook_like_box':
			
			if ($CI->config->item('sts_site_facebook_page_code'))
			{
				$data .= '<div ' . $class . '>' . base64_decode($CI->config->item('sts_site_facebook_page_code')) . '</div>';
			}
			
		break;
		
		case 'social_media':
			
			$data .= '<div ' .$class . '><h3>' . lang('connect_with_us') . '</h3>';
			
		 	if ($CI->config->item('sts_site_facebook_url'))
			{
            	$data .= '<a href="' . $CI->config->item('sts_site_facebook_url') .'"><img src="' . base_url('js') . 'themes/main/' . $CI->config->item('layout_design_site_theme') . '/img/social/facebook.png" alt="facebook" /></a>';
			}
            
            if ($CI->config->item('sts_site_youtube_url'))
			{
            	$data .= '<a href="' . $CI->config->item('sts_site_youtube_url') . '"><img src="' . base_url('js') . 'themes/main/' . $CI->config->item('layout_design_site_theme') . '/img/social/youtube.png" alt="youtube"/></a>';
			}
            
            if ($CI->config->item('sts_site_twitter_url'))
			{
            	$data .= '<a href="' . $CI->config->item('sts_site_twitter_url') . '"><img src="' . base_url('js') . 'themes/main/' . $CI->config->item('layout_design_site_theme') . '/img/social/twitter.png" alt="twitter" /></a>';
			}
			
			if ($CI->config->item('sts_site_linkedin_url'))
			{
            	$data .= '<a href="' . $CI->config->item('sts_site_linkedin_url') . '"><img src="' . base_url('js') . 'themes/main/' . $CI->config->item('layout_design_site_theme') . '/img/social/linkedin.png" alt="linkedin" /></a>';
			}
			
			if ($CI->config->item('sts_site_pinterest_url'))
			{
            	$data .= '<a href="' . $CI->config->item('sts_site_pinterest_url') . '"><img src="' . base_url('js') . 'themes/main/' . $CI->config->item('layout_design_site_theme') . '/img/social/pinterest.png" alt="pinterest" /></a>';
			}
			
			if ($CI->config->item('sts_site_google_plus_url'))
			{
            	$data .= '<a href="' . $CI->config->item('sts_site_google_plus_url') . '"><img src="' . base_url('js') . 'themes/main/' . $CI->config->item('layout_design_site_theme') . '/img/social/google-plus.png" alt="google plus" /></a>';
			}
			
			$data .= '</div>';
			
		break;
		
		case 'manufacturers':
		
			$CI->load->model('manufacturers_model', 'man');
					
			$brands = $CI->man->_get_top_manufacturers('checkout');
			
			if (!empty($brands))
			{
				$data .= '<div ' . $class . '>';
				foreach ($brands as $b)
				{
					
					if (file_exists(PUBPATH . '/images/manufacturers/' . url_title(strtolower($b['manufacturer_name'])) . '-logo.jpg'))
					{
						
						$data .= '<a href="' . base_url() . 'products/manufacturer/' . $b['manufacturer_id'] . '/' . url_title(strtolower($b['manufacturer_name'])) . '"><img src="' . base_url('js')  . 'images/manufacturers/' . url_title(strtolower($b['manufacturer_name'])) . '-logo.jpg" class="manufacturer-image" alt="' . $b['manufacturer_name'] . '"/></a> ';
					}
				}	
				
				$data .= '</div>';
			}
			
			
		break;
		
		case 'payment-options':
			
			//now get the payment options
			$CI->load->model('gateways_model', 'gateways');
					
			$types = $CI->gateways->_get_all_gateways('checkout');
		
			if (!empty($types) && count($types) > 1)
			{
				$data .= '<div ' .$class . '><ul class="unstyled">';
				$i = 0;
				foreach ($types as $v)
				{	
					if (file_exists(PUBPATH . '/images/misc/gateways/' . $v['module_file_name'] . '.png'))
					{	
						$i++;
						$http = $_SERVER['SERVER_PORT'] == '443' ? 'base_SSL_url' : 'base_url';
						$data .= '<li><img src="' . $CI->config->slash_item($http) . 'images/misc/gateways/' . $v['module_file_name'] . '.png" /></li>';
					}
				}
				
				$data .= '</ul></div>';
				
				if ($i > 1) return $data;
			}
		
		break;
		
		case 'address';
		
		$CI->load->helper('country_helper');
		
		$data .= '<div ' . $class .'>
					<div class="footer_address"> 
                    <div>
                        <h3><a href="' . base_url() . 'contact"><i class="icon-phone"></i> ' . lang('contact_us') . '</a></h3> 
						<p><strong class="store_name">' . $CI->config->item('sts_store_name') . '</strong><br />
						<span class="address_1">' . $CI->config->item('sts_store_shipping_address') . '</span><br />
						<span class="city">' . $CI->config->item('sts_store_shipping_city') . ' ' . $CI->config->item('sts_store_shipping_state')  . ' ' . $CI->config->item('sts_store_shipping_zip') . '</span><br />
						<span class="country">' . _get_country_name($CI->config->item('sts_store_shipping_country'), 'country_name') . '</span><br />
						</p>
						<p>
						<span class="email">' . safe_mailto($CI->config->item('sts_store_email')) . '</span><br />
						<span class="phone">' . $CI->config->item('sts_store_phone_number') . '</span>
						</p>
                    </div>
                </div>
		</div>';
		
		break;
		
		
		case 'about-us':
			
			if ($show_on_columns == true || (!$CI->config->item('layout_design_enable_left_column_home_page') && !$CI->config->item('layout_design_enable_right_column_home_page')))
			{
				//get latest blogs
				$blog = $CI->content_model->_get_homepage_articles($limit, 4);
				if (!empty($blog) && count($blog) > 3)
				{	
					$icons = $CI->config->item('dbi_about_us_icons');
						
					$data .= '<div class="about-us-row homepage-row clearfix">'; 
					foreach ($blog as $b) 
					{
						$data .= '<div ' . $class . '><div class="about-us-block" ><div class="about-us-image">';
						
						if (!empty($b['preview_image']))
						{
							$data .= '<img src="' . base_url('js') . 'images/content/' . $b['preview_image'] . '" alt="' . $b['content_title'] . '" />';
						}
						else
						{
							$data .= '<i class="' . random_element($icons) . ' icon-3x"></i>';
						}
						 
							$data .= '</div><div><h3><a href="' . base_url() . $b['title_url'] . '">' . limit_chars($b['content_title'],40) . '</a></h3>
							
							<div class="about-us-body"><p>' . word_limiter(strip_tags($b['preview_body']), 25) . ' 
							<a href="' . base_url() . $b['title_url'] . '"><strong>'. lang('read_more') . '</strong></a>
							</p>
							</div>
							</div>
						</div>
						</div>';
					}
					
					$data .= '</div>';
				}
			}
		break;	
		
		case 'articles':
		
			//get latest blogs
			$blog = $CI->content_model->_get_homepage_articles($limit);
			if (!empty($blog))
			{
				foreach ($blog as $b)
				{
					$data .= '<div ' . $class . '><div class="preview-image-div"><a href="' . base_url() . $b['title_url'] . '">';
					
					if (!empty($b['preview_image']))
					{
						if(preg_match("/http:\/\//",$b['preview_image']) || preg_match("/https:\/\//",$b['preview_image']))
						{ 
							$image = $b['preview_image'];
						} 
						else
						{
							$image =  base_url('js') . 'images/content/' . $b['preview_image'];
						}
						$data .= '<img src="' . $image . '" alt="' . $b['content_title'] . '" class="preview-image" />';
					}
					else
					{
						$data .= '<img src="' . base_url('js') . 'images/misc/blog-icon-home.png" alt="' . $b['content_title'] . '" class="preview-image" />';
					}
						$data .= '</a></div><div class="latest-news-body"><h4><a href="' . base_url() . $b['title_url'] . '">' . $b['content_title'] . '</a></h4>
						
						<p class="blog-body">
							<i class="icon-edit-sign icon-4x pull-left"></i>' . word_limiter(strip_tags($b['preview_body']), 25) . '
						</p>
						<p>
							<a href="' . base_url() . $b['title_url'] . '" class="btn btn-primary btn-small">'. lang('read_more') . '</a>
						</p>
						</div>
					</div>';
				}
			}
           
		break;	
	}
	
	return $data;
}



if ( ! function_exists('getheader')) {
	/**
	 * Get Header
	 *
	 * @return    string
	 */
	function getheader($file='header') {
		$themepath=get_instance()->config->item('theme_path');
		$theme=get_instance()->config->site_theme.'/';
		get_instance()->load->view('themes/default/header',get_instance()->data);
		//return get_instance()->template->getPartialView('header');
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('getfooter')) {
	/**
	 * Get Footer
	 *
	 * @return    string
	 */
	function getfooter($file='footer') {
		$themepath=get_instance()->config->item('theme_path');
		$theme=get_instance()->config->site_theme.'/';		
		echo get_instance()->load->view($themepath.$theme.$file);		
		//return get_instance()->template->getPartialView('footer');
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('getsidemenu')) {
	/**
	 * Get Footer
	 *
	 * @return    string
	 */
	function getsidemenu($file='sidemenu') {
		$themepath=get_instance()->config->item('theme_path');
		$theme=get_instance()->config->site_theme.'/';		
		echo get_instance()->load->view($themepath.$theme.$file);			
		//return get_instance()->template->loadView('sidemenu');
	}
}


// ------------------------------------------------------------------------

if ( ! function_exists('get_partial')) {
	/**
	 * Get Partial
	 *
	 * @param string $partial
	 * @param string $class
	 *
	 * @return string
	 */
	function get_partial($partial = '', $class = '') {
		return get_instance()->template->getPartialView($partial, array('class' => $class));
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_partial')) {
	/**
	 * Load Partial
	 *
	 * @param string $partial
	 * @param array  $data
	 *
	 * @return string
	 */
	function load_partial($partial = '', $data = array()) {
		echo get_instance()->template->loadView($partial, $data);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('partial_exists')) {
	/**
	 * Check if Partial Exist in layout
	 *
	 * @param string $partial
	 *
	 * @return string
	 */
	function partial_exists($partial = '') {
		return (get_instance()->template->getPartialView($partial)) ? TRUE : FALSE;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_doctype')) {
	/**
	 * Get Doctype
	 *
	 * @return    string
	 */
	function get_doctype() {
		return get_instance()->template->getDocType();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_doctype')) {
	/**
	 * Set Doctype
	 *
	 * @param string $doctype
	 */
	function set_doctype($doctype = '') {
		get_instance()->template->setHeadTag('doctype', $doctype);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_metas')) {
	/**
	 * Get metas html tags
	 *
	 * @return    string
	 */
	function get_metas() {
		return get_instance()->template->getMetas();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_meta')) {
	/**
	 * Set metas html tags
	 */
	function set_meta($meta = array()) {
		get_instance()->template->setHeadTag('meta', $meta);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_favicon')) {
	/**
	 * Get favicon html tag
	 *
	 * @return    string
	 */
	function get_favicon() {
		return get_instance()->template->getFavIcon();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_favicon')) {
	/**
	 * Set favicon html tag
	 *
	 * @param string $href
	 */
	function set_favicon($href = '') {
		get_instance()->template->setHeadTag('favicon', $href);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_title')) {
	/**
	 * Get page title html tag
	 *
	 * @return    string
	 */
	function get_title() {
		return get_instance()->template->getTitle();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_title')) {
	/**
	 * Set page title html tag
	 *
	 * @param string $title
	 *
	 * @return string
	 */
	function set_title($title = '') {
		get_instance()->template->setHeadTag('title', $title);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_heading')) {
	/**
	 * Get page heading
	 *
	 * @return    string
	 */
	function get_heading() {
		return get_instance()->template->getHeading();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_heading')) {
	/**
	 * Set page heading
	 *
	 * @param string $heading
	 *
	 * @return string
	 */
	function set_heading($heading = '') {
		get_instance()->template->setHeadTag('heading', $heading);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_style_tags')) {
	/**
	 * Get multiple stylesheet html tags
	 *
	 * @return    string
	 */
	function get_style_tags() {
		return get_instance()->template->getStyleTags();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_style_tag')) {
	/**
	 * Set single stylesheet html tag
	 *
	 * @param string $href
	 * @param string $name
	 * @param null   $priority
	 *
	 * @return string
	 */
	function set_style_tag($href = '', $name = '', $priority = NULL) {
		get_instance()->template->setStyleTag($href, $name, $priority);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_style_tags')) {
	/**
	 * Set multiple stylesheet html tags
	 *
	 * @param array $tags
	 *
	 * @return string
	 */
	function set_style_tags($tags = array()) {
		get_instance()->template->setStyleTag($tags);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_script_tags')) {
	/**
	 * Get multiple scripts html tags
	 *
	 * @return    string
	 */
	function get_script_tags() {
		return get_instance()->template->getScriptTags();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_script_tag')) {
	/**
	 * Set single scripts html tags
	 *
	 * @param string $href
	 * @param string $name
	 * @param null   $priority
	 *
	 * @return string
	 */
	function set_script_tag($href = '', $name = '', $priority = NULL) {
		get_instance()->template->setScriptTag($href, $name, $priority);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('set_script_tags')) {
	/**
	 * Set multiple scripts html tags
	 *
	 * @param array $tags
	 *
	 * @return string
	 */
	function set_script_tags($tags = array()) {
		get_instance()->template->setScriptTag($tags);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_active_styles')) {
	/**
	 * Get the active theme custom stylesheet html tag,
	 * generated by customizer
	 *
	 * @return    string
	 */
	function get_active_styles() {
		return get_instance()->template->getActiveStyle();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_theme_options')) {
	/**
	 * Get the active theme options set in theme customizer
	 *
	 * @param string $item
	 *
	 * @return string
	 */
	function get_theme_options($item = '') {
		return get_instance()->template->getActiveThemeOptions($item);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_breadcrumbs')) {
	/**
	 * Get page breadcrumbs
	 *
	 * @return    string
	 */
	function get_breadcrumbs() {
		return get_instance()->template->getBreadcrumb();
	}
	

function get_bread_crumbs2($type = '')
{
	$CI = & get_instance();
	$a = '<ul class="breadcrumb"><li><a href="' . base_url() . '"><i class="fa fa-home"></i> ' . lang('Dashboard') . ' </a></li>';
	
	if ($type == '')
	{

		
		if (!$CI->uri->segment(2))
		{
			$a .= '<li class="pull-right date_blue" >' . lang('Logged_in_as') .' ' . $CI->session->userdata('username') . ' / ' . lang('Last_login') . ': ' . $CI->session->userdata('ll_date') . ' / ' . $CI->session->userdata('ll_ip') . '</li>';
		}
		else 
		{
			if ($CI->uri->segment(1) == 'modules')
			{
				$a .= '<li><a href="' . base_url('js') . 'modules/' . $CI->uri->segment(2) . '">';
			}
			elseif ($CI->uri->segment(2) == 'follow_ups')
			{
				$a .= '<li><a href="' . base_url() . 'mailing_lists/">';	
			}
			else
			{
				$a .= '<li><a href="' . base_url() . $CI->uri->segment(1) . '">';
			}
			
			$name = $CI->uri->segment(1);
			if ($CI->config->item('sts_content_translate_menus') == 1)
			{
				if ($name == 'follow_ups') $name = 'mailing_lists';
				if (lang(strtolower($name)))
				{							   
					if ($name == 'members') $name = 'contacts'; 
					$a .= lang(strtolower($name));
				}
				else
				{
					$a .= str_replace('_', ' ', str_replace('module_', '', $name));
				}
			}
			else
			{
				$a .= str_replace('_', ' ', humanize($name));
			}
			$a .= '</a></li>';
		}
		
		if ($CI->uri->segment(3))
		{
			$a .= '<li>';
			if ($CI->config->item('sts_content_translate_menus') == 1)
			{
				
				if (lang(strtolower($CI->uri->segment(3))))
				{							   
					$name = strtolower($CI->uri->segment(3));
					if ($name == 'view_members') $name = 'view_contacts'; 
					if ($name == 'add_member') $name = 'add_contact';
					if ($name == 'update_member') $name = 'update_contact';  
					$a .= str_replace('_', ' ', lang(strtolower($name)));
				}
				else
				{
					$a .= str_replace('_', ' ', $CI->uri->segment(3));
				}
			}
			else
			{
				$a .= str_replace('_', ' ', $CI->uri->segment(3));
			}
			$a .= '</li>';
		}
	}
	else
	{
		$a .= '<li><a href="' . base_url() . $type . '">';
		$a .= str_replace('_', ' ', humanize($type));
		$a .= '</a></li>';
	}
	$a .= '<li class="pull-right date_blue" >' . lang('Logged_in_as') .' ' . $CI->session->userdata('username') . ' / ' . lang('Last_login') . ': ' . $CI->session->userdata('ll_date') . ' / ' . $CI->session->userdata('ll_ip') . '</li>';	
	$a .= '</ul>';

	return $a;
}
	
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_button_list')) {
	/**
	 * Get admin page heading action buttons
	 *
	 * @return    string
	 */
	function get_button_list() {
		return get_instance()->template->getButtonList();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_icon_list')) {
	/**
	 * Get admin page heading icons
	 *
	 * @return    string
	 */
	function get_icon_list() {
		return get_instance()->template->getIconList();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_nav_menu')) {
	/**
	 * Build admin theme navigation menu
	 *
	 * @param array $prefs
	 *
	 * @return string
	 */
	function get_nav_menu($prefs = array()) {
		return get_instance()->template->navMenu($prefs);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('get_theme_partials')) {
	/**
	 * Get the theme partial areas/regions
	 *
	 * @param null   $theme
	 * @param string $domain
	 *
	 * @return string
	 */
	function get_theme_partials($theme = NULL, $domain = 'main') {

		$theme_config = load_theme_config(trim($theme, '/'), $domain);

		return isset($theme_config['partial_area']) ? $theme_config['partial_area'] : array();
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('find_theme_files')) {
	/**
	 * Search a theme folder for files.
	 *
	 * Searches an individual folder for any theme files and returns an array
	 * appropriate for display in the theme tree view.
	 *
	 * @param string $filename The theme folder to search
	 *
	 * @return array $theme_files
	 */
	function find_theme_files($filename = NULL) {
		if (empty($filename)) {
			return NULL;
		}

		$CI =& get_instance();
		$CI->config->load('template');

		$theme_files = array();
		foreach (glob(ROOTPATH . MAINDIR . "/views/themes/{$filename}/*") as $file) {
			$file_name = basename($file);
			$file_ext = strtolower(substr(strrchr($file, '.'), 1));

			$type = '';
			if (is_dir($file) AND ! in_array($file_name, config_item('theme_hidden_folders'))) {
				$type = 'dir';
			} else if ( ! in_array($file_name, config_item('theme_hidden_files'))) {
				if (in_array($file_ext, config_item('allowed_image_ext'))) {
					$type = 'img';
				} else if (in_array($file_ext, config_item('allowed_file_ext'))) {
					$type = 'file';
				}
			}

			if ($type !== '') {
				$theme_files[] = array('type' => $type, 'name' => $file_name, 'path' => $file, 'ext' => $file_ext);
			}
		}

		$type = array();
		foreach ($theme_files as $key => $value) {
			$type[$key] = $value['type'];
		}
		array_multisort($type, SORT_ASC, $theme_files);

		return $theme_files;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('list_themes')) {
	/**
	 * List existing themes in the system
	 *
	 * Lists the existing themes in the system by examining the
	 * theme folders in both admin and main domain, and also gets the theme
	 * config.
	 *
	 * @return array The names,path,config of the theme directories.
	 */
	function list_themes() {
		$themes = array();

		foreach (array(MAINDIR, ADMINDIR) as $domain) {
			foreach (glob(ROOTPATH . "{$domain}/views/themes/*", GLOB_ONLYDIR) as $filepath) {
				$filename = basename($filepath);

				$themes[] = array(
					'location' => $domain,
					'basename' => $filename,
					'path'     => "{$domain}/views/themes/{$filename}",
					'config'   => load_theme_config($filename, $domain),
				);
			}
		}

		return $themes;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_theme_config')) {
	/**
	 * Load a single theme config file into an array.
	 *
	 * @param string $filename The name of the theme to locate. The config file
	 *                         will be found and loaded by looking in the admin and main theme folders.
	 * @param string $domain   The domain where the theme is located.
	 *
	 * @return mixed The $theme array from the file or false if not found. Returns
	 * null if $filename is empty.
	 */
	function load_theme_config($filename = NULL, $domain = MAINDIR) {
		if (empty($filename)) {
			return NULL;
		}

		if ( ! file_exists(ROOTPATH . "{$domain}/views/themes/{$filename}/theme_config.php")) {
			log_message('debug', 'Theme [' . $filename . '] does not have a config file.');

			return NULL;
		}

		include(ROOTPATH . "{$domain}/views/themes/{$filename}/theme_config.php");

		if ( ! isset($theme) OR ! is_array($theme)) {
			log_message('debug', 'Theme [' . $filename . '] config file does not appear to contain a valid array.');

			return NULL;
		}

		log_message('debug', 'Theme [' . $filename . '] config file loaded.');

		return $theme;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('load_theme_file')) {
	/**
	 * Load a single theme generic file into an array.
	 *
	 * @param string $filename The name of the file to locate. The file will be
	 *                         found by looking in the admin and main themes folders.
	 * @param string $theme    The theme to check.
	 *
	 * @return mixed The $theme_file array from the file or false if not found. Returns
	 * null if $filename is empty.
	 */
	function load_theme_file($filename = NULL, $theme = NULL) {
		if (empty($filename) OR empty($theme)) {
			return NULL;
		}

		$theme_file_path = ROOTPATH . MAINDIR . "/views/themes/{$theme}/{$filename}";

		if ( ! file_exists($theme_file_path)) {
			return NULL;
		}

		$CI =& get_instance();
		$CI->config->load('template');

		$file_name = basename($theme_file_path);
		$file_ext = strtolower(substr(strrchr($theme_file_path, '.'), 1));

		if (in_array($file_ext, config_item('allowed_image_ext'))) {
			$file_type = 'img';
			$content = root_url(MAINDIR . "/views/themes/{$theme}/{$filename}");
		} else if (in_array($file_ext, config_item('allowed_file_ext'))) {
			$file_type = 'file';
			$content = htmlspecialchars(file_get_contents($theme_file_path));
		} else {
			return NULL;
		}

		$theme_file = array(
			'name'        => $file_name,
			'ext'         => $file_ext,
			'type'        => $file_type,
			'path'        => $theme_file_path,
			'content'     => $content,
			'is_writable' => is_really_writable($theme_file_path),
		);

		return $theme_file;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('ci_delete_theme')) {
	/**
	 * Delete existing theme folder.
	 *
	 * @param null $theme
	 * @param      $domain
	 *
	 * @return bool
	 */
	function ci_delete_theme($theme = NULL, $domain = MAINDIR) {
		if (empty($theme)) {
			return FALSE;
		}

		if ( ! function_exists('delete_files')) {
			get_instance()->load->helper('file');
		}

		// Delete the specified admin and main language folder.
		if (!empty($domain)) {
			$path = ROOTPATH . "{$domain}/views/themes/{$theme}";
			if (is_dir($path)) {
				delete_files($path, TRUE);
				rmdir($path);

				return TRUE;
			}
		}

		return FALSE;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('save_theme_file')) {
	/**
	 * Save a theme file.
	 *
	 * @param string  $filename The name of the file to locate. The file will be
	 *                          found by looking in the admin and main themes folders.
	 * @param string  $theme    The theme to check.
	 * @param array   $new_data A string of the theme file content replace.
	 * @param boolean $return   True to return the contents or false to return TRUE.
	 *
	 * @return bool|string False if there was a problem loading the file. Otherwise,
	 * returns true when $return is false or a string containing the file's contents
	 * when $return is true.
	 */
	function save_theme_file($filename = NULL, $theme = NULL, $new_data = NULL, $return = FALSE) {
		if (empty($filename) OR empty($theme) OR empty($new_data)) {
			return FALSE;
		}

		$theme_file_path = ROOTPATH . MAINDIR . "/views/themes/{$theme}/{$filename}";

		if ( ! file_exists($theme_file_path)) {
			return FALSE;
		}

		$file_ext = strtolower(substr(strrchr($theme_file_path, '.'), 1));

		$CI =& get_instance();
		$CI->config->load('template');

		if ( ! in_array($file_ext, config_item('allowed_file_ext')) OR ! is_really_writable($theme_file_path)) {
			return FALSE;
		}

		if ($fp = @fopen($theme_file_path, FOPEN_READ_WRITE_CREATE_DESTRUCTIVE)) {
			flock($fp, LOCK_EX);
			fwrite($fp, $new_data);
			flock($fp, LOCK_UN);
			fclose($fp);

			@chmod($theme_file_path, FILE_WRITE_MODE);

			return ($return === TRUE) ? $new_data : TRUE;
		}

		return FALSE;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('create_child_theme_files')) {
	/**
	 * Create child theme file(s).
	 *
	 * @param array  $files The name of the files to locate. The file will be
	 *                          found by looking in the main themes folders.
	 * @param string $source_theme      The theme folder to copy the file from.
	 * @param string $child_theme_data 	The child theme data.
	 *
	 * @return bool Returns false if file is not found in $source_theme
	 * or $child_theme already exist.
	 */
	function create_child_theme_files($files = array(), $source_theme = NULL, $child_theme_data = NULL) {
		if (empty($files) OR empty($source_theme) OR empty($child_theme_data)) {
			return FALSE;
		}

		// preparing the paths
		$source_theme = rtrim($source_theme, '/');
		$child_theme = rtrim($child_theme_data['name'], '/');

		$source_theme_path = ROOTPATH . MAINDIR . "/views/themes/" ."{$source_theme}";
		$child_theme_path = ROOTPATH . MAINDIR . "/views/themes/" ."{$child_theme}";

		if ( ! function_exists('write_file')) {
			get_instance()->load->helper('file');
		}

		// creating the destination directory
		if ( ! is_dir($child_theme_path)) {
			mkdir($child_theme_path, DIR_WRITE_MODE, TRUE);
		}

		$failed = FALSE;
		foreach ($files as $file) {
			if (file_exists("{$child_theme_path}/{$file['name']}") OR ! file_exists($file['path'])) {
				continue;
			}

			if ($file['name'] === 'theme_config.php') {
				if ($start = strpos($file['content'], '$theme[\'child\']')) {
					$end = strpos($file['content'], ';', $start);
					$search = substr($file['content'], $start, $end - $start + 1);
					$replace = str_replace('child', 'parent', str_replace('TRUE', '\'' . $source_theme . '\'', $search));

					$file['content'] = str_replace($search, $replace, $file['content']);

					if ($start = strpos($file['content'], '$theme[\'title\']')) {
						$end = strpos($file['content'], ';', $start);
						$search = substr($file['content'], $start, $end - $start + 1);
						$replace = str_replace($child_theme_data['old_title'], $child_theme_data['title'], $search);

						$file['content'] = str_replace($search, $replace, $file['content']);
					}
				}

				if ( ! write_file("{$child_theme_path}/{$file['name']}", html_entity_decode($file['content']))) {
					$failed = TRUE;
				}
			} else {
				copy("{$source_theme_path}/{$file['name']}", "{$child_theme_path}/{$file['name']}");
			}
		}

		return $failed === TRUE ? FALSE : TRUE;
	}
}

if ( ! function_exists('get_currentuserinfo')) {
	function get_currentuser()
	{
		return get_instance()->user;
	}
}
// ------------------------------------------------------------------------

/* End of file template_helper.php */
/* Location: ./system/shopigniter/helpers/template_helper.php */
?>