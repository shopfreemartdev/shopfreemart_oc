<?php
class AjaxHelpers
{
	public static function gridFormater($val , $row, $attribute = array() , $arr = array())
	{
		$_this = & get_Instance();
		$formatvalue = '';
		// Handling predefined function
		if(isset($attribute['predefined']['value']) and $attribute['predefined']['value'] != 'none'){
			if (isset($attribute['predefined']['formatvalue']))
			{
				$formatvalue = $attribute['predefined']['formatvalue'] ;
			}
			$val = AjaxHelpers::formatRows($attribute['predefined']['value'],$val,$formatvalue,$row) ;
		}


		// Handling Image & file Field
		if($attribute['image']['active'] == '1' && $attribute['image']['active'] != '')
		{
			$val = SiteHelpers::showUploadedFile($val,$attribute['image']['path']) ;
		}

		// Handling Quick Display As
		if(isset($arr['valid']) && $arr['valid'] == 1){
			$fields = str_replace("|",",",$arr['display']);
			$Q      = $_this->db->query(" SELECT ".$fields." FROM ".$arr['db']." WHERE ".$arr['key']." = '".$val."' ")->row();
			if(count($Q) >= 1 ){
				$rowObj = $Q;
				$fields = explode("|",$arr['display']);
				$v      = '';
				$v .= (isset($fields[0]) && $fields[0] != '' ?  $rowObj->$fields[0].' ' : '');
				$v .= (isset($fields[1]) && $fields[1] != ''  ? $rowObj-> $fields[1].' ' : '');
				$v .= (isset($fields[2]) && $fields[2] != ''  ? $rowObj->$fields[2].' ' : '');


				$val = $v;
			}
		}

		// Handling format function
		// if(isset($attribute['formater']['active']) && $attribute['formater']['active'] == 1){
		// 	$val =$formatvalue ;

		// 	foreach($row as $k=>$i){
		// 		$k = "{".$k."}";
		// 		if(preg_match("/$k/",$val))
		// 		$val = str_replace($k,$i,$val);
		// 	}
		// 	$c = explode("|",$val);


		// 	if(isset($c[0]) && class_exists($c[0])){
		// 		$val = call_user_func( array($c[0],$c[1]), str_replace(":",",",$c[2]));
		// 		//$val = $c[2];
		// 	}

		// }

		// Handling Link  function
		if(isset($attribute['hyperlink']['active']) && $attribute['hyperlink']['active'] == 1 && $attribute['hyperlink']['link'] != ''){

			$attr   = '';
			$linked = $attribute['hyperlink']['link'];
			foreach($row as $k=>$i){

				if(preg_match("/$k/",$attribute['hyperlink']['link']))
				$linked = str_replace($k,$i, $linked);
			}
			if($attribute['hyperlink']['target'] == 'modal'){
				$attr = 'onclick="SximoModal(this.href,\''.addslashes ($val).'\'); return false"';
			}
			//$val = " < a href = '".ci_site_url($linked)."' $attr style = 'display:block' > ".$val." < span class = 'fa fa - arrow - circle - right pull - right'></span></a > ";
			$val = "<a href='".ci_site_url($linked)."' $attr style='display:block' >".$val." </a>";

		}



		return $val;

	}

	public static function formatRows( $format_as, $value, $format_value = '',$row = 0 )
	{


		if($format_as == 'image'){
			// FORMAT AS IMAGE
			$vals   = '';
			$values = explode(',',$value);

			foreach($values as $val){
				if($val != ''){
					if(file_exists('.'.$format_value . $val))
					$vals .= '<a href="'.url( $format_value . $val).'" target="_blank" class="previewImage"><img src="'.asset( $format_value . $val ).'" border="0" width="30" class="img-circle" style="margin-right:2px;" /></a>';
				}
			}
			$value = $vals;

		}
		elseif($format_as == 'email')
		{
			// FORMAT AS EMAIL
			$value = FormatHelper::mailTo($value);

		}

		elseif($format_as == 'userlinkwithtooltip')
		{
			// avatarwithtooltip
			$value = FormatHelper::userlinkwithtooltip($value);

		}
		elseif($format_as == 'link')
		{
			// FORMAT AS LINK
			$value = '<a href="'.$value.'">'.$value.'</a>';

		}
		elseif($format_as == 'phonelink')
		{
			// FORMAT AS LINK
			$value = '<a href="tel:'.$value.'">'.$value.'</a>';

		}
		elseif($format_as == 'date')
		{

			// FORMAT AS DATE
			if($format_value == ''){
				if(defined('CNF_DATE') && CNF_DATE != '' ){
					$value = date("".CNF_DATE."",strtotime($value));
				}
			}
			else
			{

				$value = date("$format_value",strtotime($value));
			}

			$value = strtotime($value);
			$value = FormatHelper::FormatTime($value);

		}
		elseif($format_as == 'humanize_date')
		{

			// FORMAT AS DATE
			if($format_value == ''){
				if(defined('CNF_DATE') && CNF_DATE != '' ){
					$value = date("".CNF_DATE."",strtotime($value));
				}
			}
			else
			{
				$value = date("$format_value",strtotime($value));
			}

			$value = strtotime($value);
			$value = FormatHelper::FormatTime($value);

		}
		elseif($format_as == 'ipaddress')
		{
			$value = FormatHelper::IPAddress($value);
		}
		else
		if($format_as == 'file')
		{
			// FORMAT AS FILES DOWNLOAD
			$format_value = $vals;
			$vals         = '';
			$values       = explode(',',$value);
			foreach($values as $val){

				if(file_exists('.'.$format_value . $val))
				$vals .= '<a href="'.asset($format_value. $val ).'"> '.$val.' </a><br />';
			}
			$value = $vals ;

		}
		else
		if( $format_as == 'database')
		{
			// Database Lookup

			$fields = explode("|",$format_value);
			if(count($fields) >= 2){

				$field_table = str_replace(':',',',$fields[2]);
				$field_toShow= explode(":",$fields[2]);
				//echo " SELECT ".$field_table." FROM ".$fields[0]." WHERE ".$fields[1]." IN(".$value.") ";
				$Q           = DB::select(" SELECT ".$field_table." FROM ".$fields[0]." WHERE ".$fields[1]." IN(".$value.") ");
				if(count($Q) >= 1 ){
					$value = '';
					foreach($Q as $qv){
						$sub_val = '';
						foreach($field_toShow as $fld){
							$sub_val .= $qv->
							{
								$fld
							}.' ';
						}
						$value .= $sub_val.', ';

					}
					$value = substr($value,0,($value - 2));
				}

			}


		}
		else
		if($format_as == 'checkbox' or $format_as == 'radio')
		{
			// FORMAT AS RADIO / CHECKBOX VALUES

			if (empty($format_value))
			{
				$value = FormatHelper::yesnocircle($value);
			}
			else
			{
				$values = explode(',',$format_value);
				if(count($values) >= 1){
					for($i = 0; $i < count($values); $i++){
						$val = explode(':',$values[$i]);
						if(trim($val[0]) == $value)
						{
							$value = $val[1];
							//$value = FormatHelper::yesnocircle($value);
						}
					}
				}
				else
				{

					$value = '';
				}
			}
		}

		else
		if($format_as == 'function')
		{
			$val = $format_value;
			foreach($row as $k=>$i){
				$k = "{".$k."}";
				if(preg_match("/$k/",$val))
				$val = str_replace($k,$i,$val);
			}
			$c = explode("|",$val);
			if(isset($c[0]) && class_exists($c[0])){
				$value = call_user_func( array($c[0],$c[1]), str_replace(":",",",$c[2]));
				//$val = $c[2];
			}
		}
		else
		if($format_as == 'currency')
		{
			$value = to_currency($value);
		}
		else
		if($format_as == 'status')
		{
			$value = FormatHelper::formatstatus($value);
		}
		else
		if($format_as == 'rank')
		{
			$value = FormatHelper::formatrank($value);
		}
		else
		if($format_as == 'sponsor')
		{
		if (empty($format_value))
			{
				$email = 'no email';
				if (isset($row->email))
				{
					$email = $row->email;
				}
				$displayname = 'no name';

				if (isset($row->display_name))
				{
					$displayname = $row->display_name;
				}
				$value = FormatHelper::sponsor_avatar($value,$email,$displayname);
			}
			else
			{
				$val = explode(':',$format_value);
				$row = get_object_vars($row);
				$username = '';
				$email = '';
				$displayname = '';

				if (is_array($val) && count($val) > 1)
				{
					$uid = $value;
					$username =   $row[$val[0]];

					if (isset($row[$val[1]]))
						$displayname = $row[$val[1]];

					if (isset($val[2]))
					{
						if (isset($row[$val[2]]))
							$email = $row[$val[2]];
					}
					$value=  FormatHelper::sponsor_avatar($value, $username, $displayname,$email);

				} else
				{
					$uid = $value;
					$displayname = $row[$val[0]];
					$pic= SiteHelpers::sponsor_avatar(32,$uid,$uid);
					$value = '<a href="members/show/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$display.'</a>';

				}
			}
		}
		else
		if($format_as == 'orderstatus')
		{
			$value = FormatHelper::formatwostatus($value);
		}
		else

		if($format_as == 'avatar')
		{
			if (empty($format_value))
			{
				$email = 'no email';
				if (isset($row->email))
				{
					$email = $row->email;
				}
				$displayname = 'no name';

				if (isset($row->display_name))
				{
					$displayname = $row->display_name;
				}
				$value = FormatHelper::user_avatar($value,$email,$displayname);
			}
			else
			{
				$val = explode(':',$format_value);
				$row = get_object_vars($row);
				$username = '';
				$email = '';
				$displayname = '';

				if (is_array($val) && count($val) > 1)
				{
					$uid = $value;
					$username =   $row[$val[0]];

					if (isset($row[$val[1]]))
						$displayname = $row[$val[1]];

					if (isset($val[2]))
					{
						if (isset($row[$val[2]]))
							$email = $row[$val[2]];
					}
					$value=  FormatHelper::user_avatar($value, $username, $displayname,$email);

				} else
				{
					$uid = $value;
					$displayname = $row[$val[0]];
					$pic= SiteHelpers::avatar(32,$uid,$uid);
					$value = '<a href="members/show/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$displayname.'</a>';

				}
			}
		}
		else
		if($format_as == 'custom')
		{

			if (!empty($format_value))
			{
				$val = $format_value;
				foreach($row as $k=>$i){
					$k = "{".$k."}";
					if(preg_match("/$k/",$val))
					$val = str_replace($k,$i,$val);
				}
				$c = explode("|",$val);
				foreach($c as $i){
					$value = $value.$i;
				}
			}
		}
		return $value;

	}
	public static function formatRows2( $format_as ,$attr , $row = null )
	{

		$conn = (isset($attr['conn']) ? $attr['conn'] :array('valid'  =>0,'db'     =>'','key'    =>'','display'=>'') );
		$field     = $attr['field'];
		$format_as = (isset($attr['format_as']) ?  $attr['format_as'] : '');
		$format_value = (isset($attr['format_value']) ?  $attr['format_value'] : '');


		if($conn['valid'] == '1')
		{
			$value = self::formatLookUp($value,$attr['field'],implode(':',$conn));
		}


		// preg_match('~{([^{]*)}~i',$format_value, $match);
		// if(isset($match[1])){
		// 	$real_value = $row->
		// 	{
		// 		$match[1]
		// 	};
		// 	$format_value = str_replace($match[0],$real_value,$format_value);
		// }

		if($format_as == 'image'){
			// FORMAT AS IMAGE
			$vals   = '';
			$values = explode(',',$value);

			foreach($values as $val){
				if($val != ''){
					if(file_exists('.'.$format_value . $val))
					$vals .= '<a href="'.url( $format_value . $val).'" target="_blank" class="previewImage"><img src="'.asset( $format_value . $val ).'" border="0" width="30" class="img-circle" style="margin-right:2px;" /></a>';
				}
			}
			$value = $vals;

		}
		elseif($format_as == 'link')
		{
			// FORMAT AS LINK
			$value = '<a href="'.$format_value.'">'.$value.'</a>';

		}
		else
		if($format_as == 'date')
		{

			// FORMAT AS DATE
			if($format_value == ''){
				if(defined('CNF_DATE') && CNF_DATE != '' ){
					$value = date("".CNF_DATE."",strtotime($value));
				}
			}
			else
			{
				$value = date("$format_value",strtotime($value));
			}

		}
		else
		if($format_as == 'file')
		{
			// FORMAT AS FILES DOWNLOAD
			$vals   = '';
			$values = explode(',',$value);
			foreach($values as $val){

				if(file_exists('.'.$format_value . $val))
				$vals .= '<a href="'.asset($format_value. $val ).'"> '.$val.' </a><br />';
			}
			$value = $vals ;

		}
		else
		if( $format_as == 'database')
		{
			// Database Lookup
			$fields = explode("|",$format_value);
			if(count($fields) >= 2){

				$field_table = str_replace(':',',',$fields[2]);
				$field_toShow= explode(":",$fields[2]);
				//echo " SELECT ".$field_table." FROM ".$fields[0]." WHERE ".$fields[1]." IN(".$value.") ";
				$Q           = DB::select(" SELECT ".$field_table." FROM ".$fields[0]." WHERE ".$fields[1]." IN(".$value.") ");
				if(count($Q) >= 1 ){
					$value = '';
					foreach($Q as $qv){
						$sub_val = '';
						foreach($field_toShow as $fld){
							$sub_val .= $qv->
							{
								$fld
							}.' ';
						}
						$value .= $sub_val.', ';

					}
					$value = substr($value,0,($value - 2));
				}

			}


		}
		else
		if($format_as == 'checkbox' or $format_as == 'radio')
		{
			// FORMAT AS RADIO / CHECKBOX VALUES

			$values = explode(',',$format_value);
			if(count($values) >= 1){
				for($i = 0; $i < count($values); $i++){
					$val = explode(':',$values[$i]);
					if(trim($val[0]) == $value) $value = $val[1];
				}

			}
			else
			{

				$value = '';
			}


		}
		else
		{

		}

		return $value;


	}
	static public function fieldLang( $fields )
	{
		if(!$fields){
			return;
		}
		$l = array();

		foreach($fields as $fs){
			foreach($fs as $f)
			$l[$fs['field']] = $fs;
		}
		return $l;
	}

	static public function instanceGrid(  $class)
	{
		$_this = & get_Instance();
		$data  = array(
			'class'=> $class ,
		);
		$_this->load->view('sximo/module/utility/instance',$data);

	}



	static function inlineFormType( $field  ,$forms )
	{
		$type = '';
		foreach($forms as $f){
			if($f['field'] == $field ){
				$type = ($f['type'] != 'file' ? $f['type'] : '');
			}
		}
		if($type == 'select' || $type = "radio" || $type == 'checkbox'){
			$type = 'select';
		}
		else
		if($type == 'file')
		{
			$type = '';
		}
		else
		{
			$type = 'text';
		}
		return $type;
	}

	static public function buttonActionFounding( $module , $access , $id , $setting, $status, $i)
	{
		$_this = & get_Instance();

		$html  = '
		<div class="btn-group action" >
		<button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown"  aria-expanded="false">
		<i class="fa fa-cog"></i>
		</button>
		<ul  class="dropdown-menu  icons-left pull-right">';

		if($module == 'foundingmembership' && $status != 'Completed'){
			$html .= '<li><a data-toggle="modal" data-target="#payBalance_'.$i.'"><i class="fa fa-check-square-o"></i> Pay Balance</a></li>';
		}


		if($access['is_detail'] == 1)
		{
			if($setting['view-method'] != 'expand'){
				$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
				if($setting['view-method'] == 'modal')
				$onclick = " onclick=\"SximoModal(this.href,'View Detail'); return false; \"" ;
				$html .= '<li><a href="'.ci_site_url($module.'/show/'.$id).'" '.$onclick.'><i class="fa fa-search"></i> '. lang('View').'</a></li>';
			}
		}
		if($access['is_edit'] == 1)
		{
			$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
			if($setting['form-method'] == 'modal')
			$onclick = " onclick=\"SximoModal(this.href,'Edit Form'); return false; \"" ;

			$html .= '<li><a href="'.ci_site_url($module.'/add/'.$id).'" '.$onclick.'><i class="fa  fa-edit"></i> '.lang('Edit').'</a></li>';
		}
		$html .= '</ul></div>';
		return $html;
	}



	static public function buttonActionInline2( $id ,$key )
	{
		$divid = 'form-'.$id;
		$html  = '<div class="actionopen" style="display:none">
		<button onclick="saved(\''.$divid.'\')" class="btn btn-primary" type="button"><i class="fa  fa-save"></i></button>
		<button onclick="canceled(\''.$divid.'\')" class="btn btn-danger " type="button"><i class="fa  fa-repeat"></i></button>
		<input type="hidden" value="'.$id.'" name="'.$key.'">
		</div>
		';
		return $html;
	}
	static public function buttonActionViewDownline( $module  ,$setting , $text = '')
	{
		$_this   = & get_Instance();
		$onclick = " onclick=\"ajaxViewDownlines('#".$module."',this.href); return false; \"" ;
		$html .= '<a href="'.ci_site_url($module.'/add/'.$id).'" '.$onclick.' class="btn btn-default" style="margin-right: 2px;"><i class="fa  fa-sitemap"></i>View Downline</a>';
		return $html;
	}

	static public function buttonActionCreate( $module  ,$setting,$text = '')
	{
		$_this   = & get_Instance();
		$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
		if($setting['form-method'] == 'modal')
		$onclick = " onclick=\"SximoModal(this.href,'Create Detail'); return false; \"" ;

		if($text == ''){
			$text = $_this->lang->line('core.btn_create');
			if($module == "tickets")
			$text = $this->lang->line('add_new_ticket');
		}

		$html = '
		<a href="'.ci_site_url($module.'/add').'" class="tips btn btn-default"  title="'.$text.'" '.$onclick.' >
		<i class="fa fa-plus-circle"></i> '.$text.'</a>';
		return $html;
	}


	static public function CreateDashboardMenuButton( $module,$setting,$layout_design_site_theme, $imagefile = '', $text = '',$desc = '')
	{


		if(!$imagefile)
		$imagefile = $module.'.png';
		if(!$text)
		$text = lang('dashboard_'.$module);
		if(!$desc)
		$desc    = lang('dashboard_'.$module.'_desc');

		$_this   = & get_Instance();
		$onclick = " onclick=\"ajaxShowGrid('#".$module."',this.href); return false; \"" ;
		if($setting['form-method'] == 'modal')
		$onclick = " onclick=\"SximoModalLarge(this.href,'Create Detail'); return false; \"" ;

		$onclick = "";
		$html    = '
		<a href="'.base_url('member').$module.'"  title="'.$text.'" '.$onclick.'>
		<img src="'. base_url('themes').'/img/'.$imagefile.'"  /></a>
		<br>'.$text;
		return $html;
	}



	static public function buttonBacktoDashboard($module,$text = '')
	{
		$dashboard = '0';
		if(isset($_GET['dashboard'])){
			$dashboard = $_GET['dashboard'];
		}

		if($dashboard == '1'){
			$html = '
			<a href="javascript:void(0);"  onclick="ajaxBackClose(\'#'.$module.'\')" class="btn btn-success"  >
			<i class="fa fa-arrow-left"></i> '.$text.'</a>';
		}
		else
		{
			$html = '
			<a href="'.base_url().'dashboard" class="btn btn-success" >
			<i class="fa fa-arrow-left"></i> '.$text.'</a>';
		}
		return $html;
	}


	static public function buttonActionButtonInline( $module , $access , $id , $setting)
	{
		$_this = & get_Instance();
		$html  = "";

		if($module == 'marketingtools'){
			$html .= '<a href="'.ci_site_url($module.'/show/'.$id).'" class="btn btn-default" style="margin-right: 2px;"><i class="fa fa-search"></i> '. $_this->lang->line('core.btn_view').'</a>';
			return $html;
		}
		if(strtolower($module) == 'managemembers'){

			return AjaxHelpers::ButtonInline_managemembers($access , $id , $setting);
		}


		if($access['is_detail'] == 1)
		{
			if($setting['view-method'] != 'expand'){

				$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
				if($setting['view-method'] == 'modal')
				$onclick = " onclick=\"SximoModal(this.href,'View Detail'); return false; \"" ;
				$html .= '<a href="'.ci_site_url($module.'/show/'.$id).'" '.$onclick.' class="btn btn-default" style="margin-right: 2px;"><i class="fa fa-search"></i> '. $_this->lang->line('core.btn_view').'</a>';
			}
		}
		if($access['is_edit'] == 1)
		{
			$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
			if($setting['form-method'] == 'modal')
			$onclick = " onclick=\"SximoModal(this.href,'Edit Form'); return false; \"" ;

			$html .= '<a href="'.ci_site_url($module.'/add/'.$id).'" '.$onclick.' class="btn btn-default" style="margin-right: 2px;"><i class="fa  fa-edit"></i> '.$_this->lang->line('core.btn_edit').'</a>';
		}


		return $html;
	}

	static public function ButtonInline_managemembers2($access , $id , $setting)
	{
		$html = "";
		$module = 'managemembers';
		$onclick = "" ;
		$html .= '<a href="'.base_url().'admin/clients/client/'.$id.'" '.$onclick.'
		data-toggle="tooltip" data-placement="top" title="'.lang('View Details').'"
		class="btn btn-default" style="margin-right: 2px;">
		<i class="fa fa-search"></i></a>';

		// View Network
		$onclick = "" ;
		$html .= '<a href="'.base_url().'genealogy/tree/'.$id.'?full=1" target="_blank"
		data-toggle="tooltip" data-placement="top" title="'.lang('View Network').'"
		 class="btn btn-default" style="margin-right: 2px;">
		 <i class="fa fa-sitemap"></i></a>';

		// GO to Member Dashboard
		$onclick = "" ;
		$html .= '<a href="'.base_url().'admin/clients/login_as_client/'.$id.'" target="_blank"
		data-toggle="tooltip" data-placement="top" title="'.lang('Go to Member Dashboard').'"
		class="btn btn-default" style="margin-right: 2px;">
		<i class="fa fa-sign-in"></i></a>';

		// Update Data
		$onclick = "" ;
		$html .= '<a href="'.base_url().'ajax.php?u='.$id.'"
		data-toggle="tooltip" data-placement="top" title="'.lang('Update User Stats').'"
		class="btn btn-default" style="margin-right: 2px;">
		<i class="fa fa-refresh"></i></a>';

		return $html;
	}

	static public function ButtonInline_soapsales($access , $id , $setting)
	{
		$html = "";
		$module = 'soapsales';
		$onclick = "" ;
		$html .= '<a href="'.base_url().'admin/clients/client/'.$id.'" '.$onclick.'
		data-toggle="tooltip" data-placement="top" title="'.lang('View Details').'"
		class="btn btn-default" style="margin-right: 2px;">
		<i class="fa fa-search"></i></a>';

		// // View Network
		// $onclick = "" ;
		// $html .= '<a href="'.base_url().'genealogy/tree/'.$id.'?full=1" target="_blank"
		// data-toggle="tooltip" data-placement="top" title="'.lang('View Network').'"
		//  class="btn btn-default" style="margin-right: 2px;">
		//  <i class="fa fa-sitemap"></i></a>';

		// // GO to Member Dashboard
		// $onclick = "" ;
		// $html .= '<a href="'.base_url().'admin/clients/login_as_client/'.$id.'" target="_blank"
		// data-toggle="tooltip" data-placement="top" title="'.lang('Go to Member Dashboard').'"
		// class="btn btn-default" style="margin-right: 2px;">
		// <i class="fa fa-sign-in"></i></a>';

		// // Update Data
		// $onclick = "" ;
		// $html .= '<a href="'.base_url().'ajax.php?u='.$id.'"
		// data-toggle="tooltip" data-placement="top" title="'.lang('Update User Stats').'"
		// class="btn btn-default" style="margin-right: 2px;">
		// <i class="fa fa-refresh"></i></a>';

		return $html;
	}



	static public function buttonAction( $module , $access , $id , $setting)
	{
		$_this = & get_Instance();
		$inline = $setting['gridsettings']['gs_inlinebuttonpanel'];

		if ($inline)
		{
			return AjaxHelpers::buttonActionButtonInline( $module , $access , $id , $setting);
		}
// <td class="text-right">


		$html  = '<div class="btn-group"  style="min-width: 65px;">';

		if($access['is_edit'] == 1)
		{
			$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
			if($setting['form-method'] == 'modal')
			$onclick = " onclick=\"SximoModal(this.href,'Edit Form'); return false; \"" ;

			$html .= '<a href="'.ci_site_url($module.'/add/'.$id).'" '.$onclick.' data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>';
		} else if($access['is_detail'] == 1)
		{
			if($setting['view-method'] != 'expand'){
				$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
				if($setting['view-method'] == 'modal')
				$onclick = " onclick=\"SximoModal(this.href,'View Detail'); return false; \"" ;
				$html .= '<a href="'.ci_site_url($module.'/show/'.$id).'" '.$onclick.' data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="View"><i class="fa fa-search"></i></a>';
			}
		}

 	 	$html .= '<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false"><span class="caret"></span></button>
		 			<ul class="dropdown-menu dropdown-menu-right">';


		if($access['is_detail'] == 1)
		{
			if($setting['view-method'] != 'expand'){
				$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
				if($setting['view-method'] == 'modal')
				$onclick = " onclick=\"SximoModal(this.href,'View Detail'); return false; \"" ;
				$html .= '<li><a href="'.ci_site_url($module.'/show/'.$id).'" '.$onclick.'><i class="fa fa-search"></i> '. lang('View').'</a></li>';
			}
		}
		if($access['is_edit'] == 1)
		{
			$onclick = " onclick=\"ajaxViewDetail('#".$module."',this.href); return false; \"" ;
			if($setting['form-method'] == 'modal')
			$onclick = " onclick=\"SximoModal(this.href,'Edit Form'); return false; \"" ;

			$html .= '<li><a href="'.ci_site_url($module.'/add/'.$id).'" '.$onclick.'><i class="fa  fa-edit"></i> '.lang('Edit').'</a></li>';
		}

		if($module == 'foundingmembership' && $setting['status'] != 'Completed'){
			$user_to_check = array(6062,27,1901);
			if ( in_array($setting['client_logged_in'], $user_to_check) ) {
				$html .= '<li><a href="'.ci_site_url().'../product/joint-venture-partner-founding-membership/?fref_id='.$id.'"><i class="fa fa-check-square-o"></i> Pay Balance</a></li>';
			}
			else {
				//$html .= '<li><a data-toggle="modal" data-target="#payBalance"><i class="fa fa-check-square-o"></i> Pay Balance</a></li>';
			}
		}

		if($module == 'managemembers'){

			//Goto Member Dashboard
			$html .= '<li><a href="'.base_url().'admin/clients/login_as_client/'.$id.'" target="_blank" ><i class="fa fa-dashboard"></i> '.lang('Member Dashboard').'</a></li>';

			//View Network
			$html .= '<li><a href="'.base_url().'genealogy/tree/'.$id.'?full=1" target="_blank" >
		 	<i class="fa fa-sitemap"></i>'.lang('View Network').'</a></li>';

			//Update User Data
			$onclick = " onclick=\"RefreshUser(this.href); return false; \"" ;
			$html .= '<li><a href="'.ci_site_url('sync/member/'.$id).'" '.$onclick.'><i class="fa  fa-refresh"></i> '.lang('Refresh Data').'</a></li>';

			//Admin Adjustment
			$html .= '<li><a href="'.ci_site_url('crm/clients/update_user_info/'.$id).'" '.$onclick.'><i class="fa  fa-exchange"></i> '.lang('Admin Adjustment').'</a></li>';

			$html .= '<li><a href="#" data-act="ajax-modal" data-title="" data-action-url="'.ci_site_url('member/messages/modal_form/'.$id).'" ><i class="fa fa-envelope-o"></i> '.lang('Send Message').'</a></li>';
		}

		$html .= '</ul></div></ul> </div></td>';

		return $html;
	}

}
