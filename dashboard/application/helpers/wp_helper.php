<?php


  function load_wp_fm($full_wp = 0)
  {

    $s = WPFMPATH;
    ld(WPFMPATH);
    if (!defined('WP_LOADED')) {
      define('WP_LOADED',TRUE);
      //define('SHORTINIT', TRUE);

      if (!defined('CI')) {
        define('CI',true);

        if ($full_wp == 1) {
          define('WP_USE_THEMES', TRUE);
          define('SHORTINIT', FALSE);
          require_once(WPFMPATH.'wp-load.php' );
        }

        else {
          define('WP_USE_THEMES', FALSE);
          define('SHORTINIT', TRUE);
          require_once(WPFMPATH.'wp-load.php' );
          require_once(FCPATH.'wp-settings.php' );
        }
        //ld(FCPATH);
      }
    }
  }

  function load_wp($full_wp = 0)
  {

    $s = WPPATH;
    if (!defined('WP_LOADED')) {
      define('WP_LOADED',TRUE);
      //define('SHORTINIT', TRUE);

      if (!defined('CI')) {
        define('CI',true);

        if ($full_wp == 1) {
          define('WP_USE_THEMES', TRUE);
          define('SHORTINIT', FALSE);
          require_once(WPPATH.'wp-load.php' );
        }

        else {
          define('WP_USE_THEMES', FALSE);
          define('SHORTINIT', TRUE);
          require_once(WPPATH.'wp-load.php' );
          require_once(FCPATH.'wp-settings.php' );
        }
        //ld(FCPATH);
      }
    }
  }

  function load_wc($full_wp = 0)
  {

    load_wp($full_wp);
    WC()->init();
    WC()->frontend_includes();
    WC()->include_template_functions();
  }

//i figured out a solution
 function wp_custom_login($userlogin,$userpass){
	$credentials = array( 'user_login' =>  $userlogin, 'user_password' => $userpass, 'remember' => true );

	$secure_cookie = is_ssl();

	$secure_cookie = apply_filters('secure_signon_cookie', $secure_cookie, $credentials);
	add_filter('authenticate', 'wp_authenticate_cookie', 30, 3);

	$user = wp_authenticate($credentials['user_login'], $credentials['user_password']);
  	if ( is_wp_error($user) )
    {
        echo $user->get_error_message();
        return false;
	}
	else
	{
		wp_set_auth_cookie($user->ID, $credentials["remember"], $secure_cookie);
		do_action('wp_login', $user->user_login, $user);
		return true;
	}
 }


    function WP_autologin( $username, $password ) {
      load_wp();
      return wp_custom_login( $username, $password );



          if ( is_user_logged_in() ) {
              wp_logout();
          }

      add_filter( 'authenticate', 'allow_programmatic_login', 10, 3 );    // hook in earlier than other callbacks to short-circuit them
      $user = wp_signon( array( 'user_login' => $username , 'user_pass' => $password ) );
      remove_filter( 'authenticate', 'allow_programmatic_login', 10, 3 );

      if ( is_a( $user, 'WP_User' ) ) {
          wp_set_current_user( $user->ID, $user->user_login );

          if ( is_user_logged_in() ) {
              return true;
          }
      }

      return false;
   }

    function WP_signout()
    {
    		 load_wp();

            $user_id = get_current_user_id();
            $session = wp_get_session_token();
            $sessions = WP_Session_Tokens::get_instance($user_id);
            $sessions->destroy_others($session);
                         wp_logout();
          //  wp_logout();
            //header("Location: http://fubar.com/login/?logout=0");


}
    function WP_manuallogin($username, $password) {
      load_wp();
      $creds = array();
      //$user=wp_authenticate($username, $password);
      $creds['user_login'] = $username;
      $creds['user_password'] = $password;
      $creds['remember'] = true;
      $user=wp_signon( $creds, false );
      if ( is_wp_error($user) )
      {
        //echo $user->get_error_message();
        return false;
    }
      return true;


  }

   function wp_autologout()
  {


      global $wo, $sqlConnect;
      if ($wo['loggedin'] == false) {
          return false;
      }
      $logged_user_id = Wo_Secure($wo['user']['user_id']);
      $user_id        = Wo_Secure($user_id);

    load_wp();

     // get all sessions for user with ID $user_id
    $sessions = WP_Session_Tokens::get_instance( $user_id );

   // we have got the sessions, destroy them all!
    $sessions->destroy_all();
      //if ( is_user_logged_in() ) {
      wp_logout();
      //}
  }

  function get_wp_logged_user()
  {
  	$wpurl = WP_URL;

	if (!defined('COOKIEHASH')){
    define( 'COOKIEHASH', md5($wpurl) );
	}

    $cookiename = "wordpress_logged_in_" . COOKIEHASH;
    //var_dump($_COOKIE);
    //die();
    if (isset($_COOKIE[$cookiename])) {
      $items = explode('|',$_COOKIE[$cookiename]);
      if ($items[0])
      {
        return $items[0];
      }
    }

  	//load_wp();
  	//return is_user_logged_in();
    return false;
  }

  function generate_hashpassword( $plaintext ) {
      require_once( WPPATH.'wp-includes/class-phpass.php' );
      $hasher = new PasswordHash(8, TRUE);
      $value =  $hasher->HashPassword( $plaintext );
      return $value;
  }

  function check_hashpassword( $plaintext, $hash ) {
      require_once( WPPATH.'wp-includes/class-phpass.php' );
      $hasher = new PasswordHash(8, TRUE);
      $value =  $hasher->CheckPassword( $plaintext, $hash );
      return $value;
  }


?>