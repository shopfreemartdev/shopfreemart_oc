<?php
class FormatHelper {


	static public function status($status)
	{
		if($status ==1)
		{

			return '<span class="label label-primary"> Active </span>';
		} else {

			return '<span class="label label-danger"> Inactive </span>';
		}

	}


	static public function yesnocircle($value)
	{

		$value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
		if($value)
		{
			return '<i class="fa fa-check-circle" style="margin: 10px; font-size: 20px; color: #01bd00;">';
			//return '<span class="text-center"><input type="radio" checked="checked" /></span>';
			//return '<a href="#" class="btn btn-success btn-xs round" title="No"><i class="fa fa-ok"></i></a>';
		} else {
			return '<i class="fa fa-close" style="margin: 10px; font-size: 20px; color: #ff4242;">';
		}

	}



	static public function rewardstatus($status)
	{

        $ticket_status_class = "label-danger";
        if ($status == "open") {
            $ticket_status_class = "label-warning";
        } else if ($status == "Completed") {
            $ticket_status_class = "label-success";
        } else if ($status == "inprogress") {
            $status = "open"; //don't show client_replied status to client
        }
		return "<span class='label $ticket_status_class large'>" . lang($status) . "</span> ";

	}

	static public function rewarddescription($type,$orderno=0,$fromuser='',$fromuserid=0)
	{

		$out = lang($type);
		if ($orderno != 0) {
			$orderlink = base_url().'shoporders2/view_order/'.$orderno;
			$out = "$out<BR/>From Order <a href='$orderlink'>#$orderno</a>";
		}
		if ($fromuser != '') {
			$userlink = base_url().'profile/'.$fromuserid;
			$out = "$out by <a href='$userlink'>$fromuser</a>";
		}
		return $out;

	}


	static public function currency($amount)
	{
			return  to_currency($amount);
	}

	static public function mailTo($mail)
	{
		return '<div class="media">  <a href="mailto:'.$mail.'" class="btn btn-default btn-xs round" title="Send message" ><i class="fa fa-envelope-o"></i></a> </div>';

	}



	static public function formattolink($value)
	{
		$value = str_replace("http,//","http://",$value);
		$value = str_replace("https,//","https://",$value);
		return '<a href="'.$value.'" target="_blank">.'.$value.'</a>';
	}


	static public function formatwoid($value)
	{
		$value = str_replace("http,//","http://",$value);
		$value = str_replace("https,//","https://",$value);
		return '<a href="'.base_url().'/shoporders2/view_order/'.$value.'" target="_blank">#'.$value.'</a>';
	}

	static public function validatesql($value)
	{

		$_this = & get_Instance();

		if (!isset($_this->session))
			return $value;

		if (!isset($_this->session->userdata))
			return $value;

		$uid = get_client_user_id();

		$value = str_replace("{currentuserid}",$uid,$value);

		$value = str_replace("currentuserid",$uid,$value);

		$value = str_replace("{get_client_user_id}",get_client_user_id(),$value);

		$value = str_replace("{get_staff_user_id}",get_staff_user_id(),$value);

		if (isset($_SESSION['admin_client']))
			$value = str_replace("{selecteduserid}",$_SESSION['admin_client'],$value);

		return $value;
	}

	static public function user_avatar($uid,$username='',$displayname='',$email='')
	{
		$pic='';
		$pic= SiteHelpers::avatar(32,$uid,$email);
		//$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		$firstline = $username;
		if (!empty($displayname))
			$firstline = $displayname;

		$secondline = $uid;
		if (!empty($username))
			$secondline .= ' | '.$username;

		$res = '<div class="media">
                        <div class="media-left">
                            <span class="avatar avatar-xs">
                                '.$pic.'
                            </span>
                        </div>
                        <div class="media-body clearfix w100p">
                            <div class="media-heading m0">
                                <div ><a href="'.base_url().'admin/clients/client/'.$uid.'"  onclick="ajaxViewDetail(\'#members\',this.href); return false; class="dark"><strong>'.$firstline.'</strong></a>&nbsp;&nbsp;<small class="text-off">'.$secondline.'</small></div>

                                <small class="text-off"><a href="mailto:'.$email.'" class="round" title="Send message" ><i class="fa fa-envelope-o"></i>&nbsp'.$email.'</a></small>

                            </div>
                        </div>
                    </div>';


		return $res;
	}

	static public function member_avatar($uid,$username='',$displayname='',$email='')
	{
		$pic='';
		$pic= SiteHelpers::avatar(32,$uid,$email);
		//$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		$firstline = $username;
		if (!empty($displayname))
			$firstline = $displayname;

		$secondline = $uid;
		if (!empty($username))
			$secondline .= ' | '.$username;

		$res = '<div class="media">
                        <div class="media-left">
                            <span class="avatar avatar-xs">
                                '.$pic.'
                            </span>
                        </div>

                        <div class="media-body clearfix w100p">
                            <div class="media-heading m0">
                                <div >
                                <a style="display: block;" href="'.base_url().'profile/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; class="dark"><strong>'.$firstline.'</strong></a>
                                <small class="text-off">'.$secondline.'</small></div>
                            </div>
                        </div>
                    </div>';


		return $res;
	}

	static public function unilevel_member_info($uid,$displayname='',$email='',$rank)
	{

		$rank = FormatHelper::formatrank($rank);
        $res = '
			<div>
				<div><a href="'.ci_site_url('profile/'.$uid).'">'.$displayname.'</a></div>
				<div style="margin-bottom: 5px;"><a href="mailto:'.$email.'" style="font-size: 12px; color: #000000;">'.$email.'</a></div>
				<div>'.$rank.'</div>
			</div>
		';
		
		return $res;
	}

	static public function order_avatar($uid,$firstname='',$lastname='',$email='')
	{
		$pic='';
		$pic= SiteHelpers::avatar(32,$uid,$email);
		//$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		$firstline = $firstname;
		if (!empty($firstname))
			$firstline = $firstname;

		if (!empty($lastname)) {
			$firstline .= ' '.$lastname;
		}

		$secondline = '';
		if (!empty($firstname))
			$secondline .= ' | ID: '.$uid;

		$res = '<div class="media">
                        <div class="media-left">
                            <span class="avatar avatar-xs">
                                '.$pic.'
                            </span>
                        </div>
                        <div class="media-body clearfix w100p">
                            <div class="media-heading m0">
                                <div ><a href="'.base_url().'admin/clients/client/'.$uid.'"  onclick="ajaxViewDetail(\'#members\',this.href); return false; class="dark"><strong>'.$firstline.'</strong></a>&nbsp;&nbsp;<small class="text-off">'.$secondline.'</small></div>

                                <small class="text-off"><a href="mailto:'.$email.'" class="round" title="Send message" ><i class="fa fa-envelope-o"></i>&nbsp'.$email.'</a></small>

                            </div>
                        </div>
                    </div>';


		return $res;
	}

	static public function sponsor_avatar($uid,$username='',$displayname='',$email='')
	{
		$pic='';
		//$pic= SiteHelpers::avatar(32,$uid,$email);
		//$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		$firstline = $username;
		if (!empty($displayname))
			$firstline = $displayname;

		$secondline = $uid;
		if (!empty($username))
			$secondline .= ' | '.$username;

		$res = '<div class="media">

                        <div class="media-body clearfix w100p">
                            <div class="media-heading m0">
                                <div ><a href="'.base_url().'admin/clients/client/'.$uid.'"  onclick="ajaxViewDetail(\'#members\',this.href); return false; class="dark"><strong>'.$firstline.'</strong></a><br /><small class="text-off">'.$secondline.'</small></div>
                            </div>
                        </div>
                    </div>';


		return $res;
	}
	static public function avatar($value='')
	{
		$uid = 0;
		$firstname ='';
		$lastname  = '';
		$email = '';
		$arrvalue = explode(',',$value);

		$uid = $arrvalue[0];

		if (count($arrvalue) == 2 )
		{
			if ($arrvalue[1])
			{
				$firstname = $arrvalue[1];
			}
		}
		if (count($arrvalue) == 3 )
		{
			if ($arrvalue[1])
			{
				$firstname = $arrvalue[1];
			}
			if ($arrvalue[2])
			{
				$lastname = $arrvalue[2];
			}
		}

		if (count($arrvalue) == 4 )
		{
			if ($arrvalue[1])
			{
				$firstname = $arrvalue[1];
				$firstname=str_replace("{","",$firstname);
			}
			if ($arrvalue[2])
			{
				$lastname = $arrvalue[2];
				$lastname=str_replace("{","",$lastname);
			}
			if ($arrvalue[3])
			{
				$email = $arrvalue[3];
				$email=str_replace("{","",$email);
			}

		}



		$pic='';
		$pic= SiteHelpers::avatar(32,$uid,$email);
		$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		return $res;
	}

	static public function coinsandcomms($coins = '', $commissions = '')
	{
		$html = '';

		if (isset($coins)) {
				$html .= "Coins: ".$coins;
		}

		if (isset($commissions)) {
				$html .= "</br>";
				$html .= "Commissions: ".$commissions;
		}

		return $html;
	}

	static public function lastlogininfo($lastlogin = '', $ip = '')
	{
		$html = '';

		if (isset($lastlogin)) {
			if (!empty($lastlogin) AND $lastlogin != '') {
				$html .= "<i class='fa fa-calendar'></i> ".time_ago_specific($lastlogin);
			}
			else {
				//$html .= "Diamond: n/a";
			}
		}

		if (isset($ip)) {
			if (!empty($ip)) {
				$html .= "</br>";
				$html .= "<div class='media'>  <a href='http://geomaplookup.net?ip=$ip' title='GO to IP Address location' ><i class='fa fa-globe'></i></a> ".$ip."</div>";
			}
			else {
				//$html .= "</br>";
				//$html .= "Double Diamond: n/a";
			}
		}

		return $html;
	}

	static public function ranksdate($diamond = '', $doublediamond = '', $triplediamond = '', $ambassador = '',	$globalambassador = '',	$crownambassador = '')
	{
		$html = '';

		if (isset($diamond)) {
			if (!empty($diamond) AND $diamond != '') {
				$val1=explode(' ',$diamond);
				$val2= $val1[0];
				$html .= "Diamond: <span data-toggle='tooltip' data-placement='top' title='".$diamond."'>".$val2."</span>";
			}
			else {
				//$html .= "Diamond: n/a";
			}
		}

		if (isset($doublediamond)) {
			if (!empty($doublediamond)) {
				$val1=explode(' ',$doublediamond);
				$val2= $val1[0];
				$html .= "</br>";
				$html .= "Double Diamond: <span data-toggle='tooltip' data-placement='top' title='".$doublediamond."'>".$val2."</span>";
			}
			else {
				//$html .= "</br>";
				//$html .= "Double Diamond: n/a";
			}
		}

		if (isset($triplediamond)) {
			if (!empty($triplediamond)) {
				$val1=explode(' ',$triplediamond);
				$val2= $val1[0];
				$html .= "</br>";
				$html .= "Triple Diamond: <span data-toggle='tooltip' data-placement='top' title='".$triplediamond."'>".$val2."</span>";
			}
			else {
				//$html .= "</br>";
				//$html .= "Triple Diamond: n/a";
			}
		}
		if (isset($ambassador)) {
			if (!empty($ambassador)) {
				$val1=explode(' ',$ambassador);
				$val2= $val1[0];
				$html .= "</br>";
				$html .= "Ambassador: <span data-toggle='tooltip' data-placement='top' title='".$ambassador."'>".$val2."</span>";
			}
			else {
				//$html .= "</br>";
				//$html .= "Ambassador: n/a";
			}
		}
		if (isset($globalambassador)) {
			if (!empty($globalambassador)) {
				$val1= explode(' ',$globalambassador);
				$val2= $val1[0];
				$html .= "</br>";
				$html .= "Global Ambassador: <span data-toggle='tooltip' data-placement='top' title='".$globalambassador."'>".$val2."</span>";
			}
			else {
				//$html .= "</br>";
				//$html .= "Global Ambassador: n/a";
			}
		}
		if (isset($crownambassador)) {
			if (!empty($crownambassador)) {
				$val1=explode(' ',$crownambassador);
				$val2= $val1[0];
				$html .= "</br>";
				$html .= "Crown Ambassador: <span data-toggle='tooltip' data-placement='top' title='".$crownambassador."'>".$val2."</span>";
			}
			else {
				//$html .= "</br>";
				//$html .= "Crown Ambassador: n/a";
			}
		}

		return $html;
	}


	static public function order_payments($cash='',$coins='',$cc='')
	{
		$html = '';

		if (isset($cc)) {
			if (!empty($cc) AND $cc != 0) {
				$html .= "CC: ".to_currency($cc);
			}
			else {
				$html .= "CC: n/a";
			}
		}

		if (isset($cash)) {
			if (!empty($cash)) {
				$html .= "</br>";
				$html .= "Cash: ".to_currency($cash*-1);
			}
			else {
				$html .= "</br>";
				$html .= "Cash: n/a";
			}
		}

		if (isset($coins)) {
			if (!empty($coins)) {
				$html .= "</br>";
				$html .= "Coins: ".to_currency($coins*-1);
			}
			else {
				$html .= "</br>";
				$html .= "Coins: n/a";
			}
		}

		return $html;
	}

	static public function order_counts($id='',$transactions='',$cash='',$coins='',$bucket='',$escrow='',$loss='')
	{
		$module = 'manageorders';
		$html = '';

		if (isset($transactions)) {
			$title = "Total Transactions";
			$html .= "Transactions: ".modal_anchor(get_uri($module.'/view_transactions/'.$id), $transactions, array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));

		}

		if (isset($cash)) {
			$title = "Total Transaction Cash";
			$html .="</br>";
			$html .= "Cash: ".modal_anchor(get_uri($module.'/view_transactions_cash/'.$id), $cash, array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
		}

		if (isset($coins)) {
			$title = "Total Transaction Coins";
			$html .="</br>";
			$html .= "Coins: ".modal_anchor(get_uri($module.'/view_transactions_coin/'.$id), $coins, array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
		}

		if (isset($bucket)) {
			$title = "Total Soap Bucket";
			$html .="</br>";
			$html .= "Bucket: ".modal_anchor(get_uri($module.'/view_transactions_bucket/'.$id), $bucket, array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
		}

		if ( (isset($escrow)) && ($escrow!='0') ) {
			$title = "Total Escrow";
			$html .="</br>";
			$html .= "Escrow: ".modal_anchor(get_uri($module.'/view_transactions_escrow/'.$id), $escrow, array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
		}

		if ( (isset($loss)) && ($loss!='0') ) {
			$title = "Total Commission Loss";
			$html .="</br>";
			$html .= "Loss: ".modal_anchor(get_uri($module.'/view_transactions_loss/'.$id), $loss, array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
		}

		return $html;
	}

	static public function order_totals($id='',$cost='',$cash='',$coin='',$ucash='',$ucoin='',$scash='',$scoin='',$fcash='',$fcoin='')
	{
		$module = 'manageorders';
		$html = '';

		if (isset($cost)) {

			if (!empty($cost)) {
				$title = "Total Cost";
				$html .= "Total Cost: ".modal_anchor(get_uri($module.'/total_cost/'.$id), to_currency($cost) , array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($cash)) {
			if (!empty($cash)) {
				$title = "Total Cash";
				$html .="</br>";
				$html .= "Cash: ".modal_anchor(get_uri($module.'/total_cash/'.$id), to_currency($cash), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($coin)) {
			if (!empty($coin)) {
				$title = "Total Coins";
				$html .="</br>";
				$html .= "Coins: ".modal_anchor(get_uri($module.'/total_coin/'.$id), to_currency($coin), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($ucash)) {
			if (!empty($ucash)) {
				$title = "Total Unilevel Cash";
				$html .="</br>";
				$html .= "Unilevel Cash: ".modal_anchor(get_uri($module.'/total_unilevel_cash/'.$id), to_currency($ucash), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($ucoin)) {
			if (!empty($ucoin)) {
				$title = "Total Unilevel Coins";
				$html .="</br>";
				$html .= "Unilevel Coins: ".modal_anchor(get_uri($module.'/total_unilevel_coin/'.$id), to_currency($ucoin), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($scash)) {
			if (!empty($scash)) {
				$title = "Total Soap Cash";
				$html .="</br>";
				$html .= "Soap Cash: ".modal_anchor(get_uri($module.'/total_soap_cash/'.$id), to_currency($scash), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($scoin)) {
			if (!empty($scoin)) {
				$title = "Total Soap Coins";
				$html .="</br>";
				$html .= "Soap Coins: ".modal_anchor(get_uri($module.'/total_soap_coin/'.$id), to_currency($scoin), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($fcash)) {
			if (!empty($fcash)) {
				$title = "Total Founding Cash";
				$html .="</br>";
				$html .= "Founding Cash: ".modal_anchor(get_uri($module.'/total_founding_cash/'.$id), to_currency($fcash), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		if (isset($fcoin)) {
			if (!empty($fcoin)) {
				$title = "Total Founding Coins";
				$html .="</br>";
				$html .= "Founding Coins: ".modal_anchor(get_uri($module.'/total_founding_coin/'.$id), to_currency($fcoin), array("class" => "", "title" => $title, "style" => "margin-right: 10px; font-weight: 600;", "data-toggle" => "tooltip", "data-placement" => "top"));
			}
		}

		return $html;
	}

	static public function formatorderno($value)
	{
		return '<a href="#"> #'.$value.'</a>';
	}

	static public function formatcheckbox($value)
	{

		$arrvalue = explode(',',$value);
		$uid = $arrvalue[0];
		$val=0;
		if (count($arrvalue) == 2 )
		{
			if ($arrvalue[1])
			{
				$val = $arrvalue[1];
			}
		}
		return "<span><input type='checkbox' name='_checkfield1' value ='checked' class='form-control' style='opacity=1 !important' /></span>";
		return "<input type='checkbox' name='checkfield".$uid."' value ='".ltrim(rtrim($val))."' class='form-control' />";
	}


	static public function formatwostatus($status)
	{
		$statuses = array(
			'wc-pending'    =>  'Pending Payment',
			'wc-processing' =>  'Processing',
			'wc-on-hold'    =>  'On Hold',
			'wc-completed'  =>  'Completed',
			'wc-cancelled'  =>  'Cancelled',
			'wc-refunded'   =>  'Refunded',
			'wc-failed'     =>  'Failed',
		);
		$status   = 'wc-' === substr( $status, 0, 3 ) ? substr( $status, 3 ) : $status;
		$status   = isset( $statuses[ 'wc-' . $status ] ) ? $statuses[ 'wc-' . $status ] : $status;

        $status_class = "label-danger";
        if ($status == "Pending Payment") {
            $status_class = "label-warning";
        } else if ($status == "Completed") {
            $status_class = "label-success";
        }else if ($status == "Processing") {
            $status_class = "label-info";
        }else if ($status == "Received") {
            $status_class = "label-sucess";
        }
		return "<span class='label $status_class large'>" . lang($status) . "</span> ";
	}

	static public function formatstatus($status)
	{
        $status_class = "label-danger";
        if ($status == "Pending") {
            $status_class = "label-warning";
        } else if ($status == "Completed") {
            $status_class = "label-success";
        }else if ($status == "Processing") {
            $status_class = "label-info";
        }else if ($status == "Received") {
            $status_class = "label-sucess";
        }
		return "<span class='label $status_class large'>" . lang($status) . "</span> ";
	}

	static public function formatstatusbyid($id)
	{
		$status = "";
        $status_class = "";
        if ($id == 1) {
        	$status = "Pending";
            $status_class = "label-warning";
        } else if ($id == 2) {
        	$status = "Completed";
            $status_class = "label-success";
        } else if ($id == 3) {
        	$status == "Processing";
            $status_class = "label-info";
        } else if ($id == 4) {
        	$status = "Cancelled";
            $status_class = "label-danger";
        } else if ($id == 5) {
        	$status = "On-hold";
            $status_class = "label-danger";
        } else if ($id == 6) {
        	$status = "Partially-Completed";
            $status_class = "label-primary";
        }

		return "<span class='label $status_class large'>" . lang($status) . "</span> ";
	}

	static public function formatwototal($id)
	{

		$statuses = array(
			'wc-pending'    =>  'Pending Payment',
			'wc-processing' =>  'Processing',
			'wc-on-hold'    =>  'On Hold',
			'wc-completed'  =>  'Completed',
			'wc-cancelled'  =>  'Cancelled',
			'wc-refunded'   =>  'Refunded',
			'wc-failed'     =>  'Failed',
		);
		$status   = 'wc-' === substr( $status, 0, 3 ) ? substr( $status, 3 ) : $status;
		$status   = isset( $statuses[ 'wc-' . $status ] ) ? $statuses[ 'wc-' . $status ] : $status;

		return $status;
	}

	static public function userlinkwithtooltip($id)
	{
		ob_start();
		$user = $this->db->query("Select user_id,username,sponsorkey,rank,downlines,email,display_name from sfm_uap_affiliates where user_id = $id")->row();
		$pic='';
		$pic= SiteHelpers::avatar(32,$id,$user->display_name);
		$res = '<a id="avatar-$id" href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$user->display_name.'</a>';
		?>
		<div class="profile_pop_up_content" style="display: none;">
			<div class="profile_tooltip">
				<div class="image_tooltip">
					<?php $md5 = md5( strtolower( trim( $user->email ) ) ); ?>
					<a href="<?php echo ci_site_url()."profile/".$user->user_id; ?>"><img class="profile-rounded-image-small" src="https://www.gravatar.com/avatar/<?php echo $md5; ?>.jpg?d=mm&s=70" width="70" height="70" alt="s1" title="s1"></a>
				</div>
				<h3 class="name"><a href="<?php echo ci_site_url()."profile/".$user->user_id; ?>" name="title"><?php echo $user->display_name; ?></a></h3>
			</div>
			<div class="detail_tooltip">
				<p>
					Username: <strong><?php echo $user->username; ?></strong><br>
					Rank: <strong><?php echo $user->rank; ?></strong><br>
					Sponsor Key: <strong><?php echo $user->sponsorkey; ?></strong><br>
					Downlines: <strong><?php echo $user->downlines; ?></strong><br>
					Email: <strong><?php echo $user->email; ?></strong><br>
				</p>
			</div>
			<div class="action_tooltip">
				<div class="view_genealogy">
					<a href="<?php echo ci_site_url('genealogy/show/' . $user->user_id) ?>" title = "View Genealogy" onclick="ajaxViewDetail('#genealogy',this.href); return false;" >
						<i class="fa fa-sitemap"></i>
					</a>
				</div>
				<div class="send_message">
					<?php echo modal_anchor(get_uri("member/messages/modal_form/" . $user->user_id), "<i class='fa fa-envelope'></i> ", array("class"=> "","title"=> lang('Send_message'))); ?>
				</div>
				<div class="view_social">
					<a href="https://www.freemartfriends.com/<?php echo $user->username; ?>" target="_blank" title="View Social"><i class="fa fa-user"></i></a>
				</div>
			</div>
		</div>
		<script>
			$(document).on('mouseover', 'avatar-'+$id, function() {
			  $(this).children('.profile_pop_up_content').show();
			 });

			 $(document).on('mouseout', 'avatar-'+$id, function() {
			  $(this).children('.profile_pop_up_content').hide();
			 });
        </script>

		<?php
		$html = ob_get_clean();
		return $status;
	}

	static public function IPAddress($value)
	{
			if (empty($value))
				return;

			return '<div class="media">  <a href="http://geomaplookup.net?ip='.$value.'" title="GO to IP Address location" ><i class="fa fa-globe"></i></a>'.$value.' </div>';
	}

	static public function FormatTime($timestamp)
    {
    	if (empty($timestamp))
    		return ;

    // Get time difference and setup arrays
    $difference = time() - $timestamp;
    $periods = array("second", "minute", "hour", "day", "week", "month", "years");
    $lengths = array("60","60","24","7","4.35","12");

    // Past or present
    if ($difference >= 0)
    {
        $ending = "ago";
    }
    else
    {
        $difference = -$difference;
        $ending = "to go";
    }

    // Figure out difference by looping while less than array length
    // and difference is larger than lengths.
    $arr_len = count($lengths);
    for($j = 0; $j < $arr_len && $difference >= $lengths[$j]; $j++)
    {
        $difference /= $lengths[$j];
    }

    // Round up
    $difference = round($difference);

    // Make plural if needed
    if($difference != 1)
    {
        $periods[$j].= "s";
    }

    // Default format
    $text = "$difference $periods[$j] $ending";

    // over 24 hours
    if($j > 2)
    {
        // future date over a day formate with year
        if($ending == "to go")
        {
            if($j == 3 && $difference == 1)
            {
                $text = "Tomorrow at ". date("g:i a", $timestamp);
            }
            else
            {
                $text = date("F j, Y \a\\t g:i a", $timestamp);
            }
            return $text;
        }

        if($j == 3 && $difference == 1) // Yesterday
        {
            $text = "Yesterday at ". date("g:i a", $timestamp);
        }
        else if($j == 3) // Less than a week display -- Monday at 5:28pm
        {
            $text = date("l \a\\t g:i a", $timestamp);
        }
        else if($j < 6 && !($j == 5 && $difference == 12)) // Less than a year display -- June 25 at 5:23am
        {
            $text = date("F j \a\\t g:i a", $timestamp);
        }
        else // if over a year or the same month one year ago -- June 30, 2010 at 5:34pm
        {
            $text = date("M d, Y", $timestamp);
        }
    }

    return $text;
}


	static public function formatrank($rank)
	{

		if ($rank == 3 || $rank == 'diamond')
		{
        	$rank_color = "rank-diamond";
        	$rank_text = "Diamond";
        }
		else if ($rank == 4 || $rank == 'doublediamond')
		{
        	$rank_color = "rank-doublediamond";
        	$rank_text = "Double Diamond";}
		else if ($rank == 5 || $rank == 'triplediamond')
		{
			$rank_color = "rank-triplediamond";
			$rank_text = "Triple Diamond";}
		else if ($rank == 6 || $rank == 'ambassador')
		{
			$rank_color = "rank-ambassador";
			$rank_text = "Ambassador";
		}
		else if ($rank == 7 || $rank == 'global')
		{
			$rank_color = "rank-ambassador";
			$rank_text = "Global";
		}
		else if ($rank == 8 || $rank == 'crown')
		{
			$rank_color = "rank-ambassador";
			$rank_text = "Crown";
		}
		else
		{
		    $rank_color = "rank-member";
			$rank_text = 'Member';
		}

		return "<span class='label round $rank_color large'>" . lang($rank_text) . "</span> ";
	}

	static public function calllogstatus($id)
	{
		$status = "";
        $status_class = "";
        if ($id == 1) {
        	$status = "Open";
            $status_class = "label-info";
        } else if ($id == 2) {
        	$status = "In-Progress";
            $status_class = "label-primary";
        } else if ($id == 3) {
        	$status = "On-Hold";
            $status_class = "label-warning";
        } else if ($id == 4) {
        	$status = "Completed";
            $status_class = "label-success";
        }

		return "<span class='label $status_class large'>" . $status . "</span> ";
	}

	static public function staffname($firstname = '', $lastname = '')
	{
		$html = '';

		if (isset($firstname) && isset($lastname)) {
				$val1=explode(' ',$firstname);
				$val2= $val1[0];
				$html .= $firstname . " " .$lastname;
		}
		return $html;
	}
	static public function calllogsurveysendstatus($id)
	{
		$status = "";
        $status_class = "";
        if ($id == 1) {
        	$status = "Sent";
            $status_class = "label-success";
        } else if ($id == 0) {
        	$status = "Not Sent";
            $status_class = "label-warning";
        }

		return "<span class='label $status_class large'>" . $status . "</span> ";
	}

	static public function managesurveymembername($firstname = '', $lastname = '')
	{
		$html = '';

		if (isset($firstname) && isset($lastname)) {
				$val1=explode(' ',$firstname);
				$val2= $val1[0];
				$html .= $firstname . " " .$lastname;
		}
		else{
			$html.= "<span class='label label-danger large'>Not a Member</span>";
		}
		return $html;
	}

	static public function voicemailisreadstatus($value)
	{
		$status = "";
        $status_class = "";
        if ($value == 1) {
        	$status = "Read";
            $status_class = "label-success";
        } else if ($value == 0) {
        	$status = "Unread";
            $status_class = "label-warning";
        }

		return "<span class='label $status_class large'>" . $status . "</span> ";
	}
}