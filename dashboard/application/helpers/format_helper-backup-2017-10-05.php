<?php
class FormatHelper {


	static public function status($status)
	{
		if($status ==1)
		{

			return '<span class="label label-primary"> Active </span>';
		} else {

			return '<span class="label label-danger"> Inactive </span>';
		}

	}


	static public function yesnocircle($value)
	{

		$value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
		if($value)
		{
			return '<div class="iradio_square-green checked hover text-center" style="position: relative;"><input type="radio" checked="checked" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>';
			//return '<span class="text-center"><input type="radio" checked="checked" /></span>';
			//return '<a href="#" class="btn btn-success btn-xs round" title="No"><i class="fa fa-ok"></i></a>';
		} else {
			return '<span class="text-center"><input type="checkbox" /></span>';
		}

	}



	static public function rewardstatus($status)
	{

        $ticket_status_class = "label-danger";
        if ($status == "open") {
            $ticket_status_class = "label-warning";
        } else if ($status == "Completed") {
            $ticket_status_class = "label-success";
        } else if ($status == "inprogress") {
            $status = "open"; //don't show client_replied status to client
        }
		return "<span class='label $ticket_status_class large'>" . lang($status) . "</span> ";

	}

	static public function rewarddescription($type,$orderno=0,$fromuser='',$fromuserid=0)
	{

		$out = lang($type);
		if ($orderno != 0) {
			$orderlink = base_url().'shoporders2/view_order/'.$orderno;
			$out = "$out<BR/>From Order <a href='$orderlink'>#$orderno</a>";
		}
		if ($fromuser != '') {
			$userlink = base_url().'profile/'.$fromuserid;
			$out = "$out by <a href='$userlink'>$fromuser</a>";
		}
		return $out;

	}


	static public function currency($amount)
	{
			return  to_currency($amount);
	}

	static public function mailTo($mail)
	{
		return '<div class="media">  <a href="mailto:'.$mail.'" class="btn btn-default btn-xs round" title="Send message" ><i class="fa fa-envelope-o"></i></a> </div>';

	}



	static public function formattolink($value)
	{
		$value = str_replace("http,//","http://",$value);
		$value = str_replace("https,//","https://",$value);
		return '<a href="'.$value.'" target="_blank">.'.$value.'</a>';
	}


	static public function formatwoid($value)
	{
		$value = str_replace("http,//","http://",$value);
		$value = str_replace("https,//","https://",$value);
		return '<a href="'.base_url().'/shoporders2/view_order/'.$value.'" target="_blank">#'.$value.'</a>';
	}

	static public function validatesql($value)
	{

		$_this = & get_Instance();

		if (!isset($_this->session))
			return $value;

		if (!isset($_this->session->userdata))
			return $value;

		$uid = get_client_user_id();

		$value = str_replace("{currentuserid}",$uid,$value);

		$value = str_replace("{get_client_user_id}",get_client_user_id(),$value);

		$value = str_replace("{get_staff_user_id}",get_staff_user_id(),$value);

		return $value;
	}

	static public function user_avatar($uid,$username='',$displayname='',$email='')
	{
		$pic='';
		$pic= SiteHelpers::avatar(32,$uid,$email);
		//$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		$firstline = $username;
		if (!empty($displayname))
			$firstline = $displayname;

		$secondline = $uid;
		if (!empty($username))
			$secondline .= ' | '.$username;

		$res = '<div class="media">
                        <div class="media-left">
                            <span class="avatar avatar-xs">
                                '.$pic.'
                            </span>
                        </div>
                        <div class="media-body clearfix w100p">
                            <div class="media-heading m0">
                                <div ><a href="'.base_url().'admin/clients/client/'.$uid.'"  onclick="ajaxViewDetail(\'#members\',this.href); return false; class="dark"><strong>'.$firstline.'</strong></a>&nbsp;&nbsp;<small class="text-off">'.$secondline.'</small></div>

                                <small class="text-off"><a href="mailto:'.$email.'" class="round" title="Send message" ><i class="fa fa-envelope-o"></i>&nbsp'.$email.'</a></small>



                            </div>
                        </div>
                    </div>';


		return $res;
	}
	static public function sponsor_avatar($uid,$username='',$displayname='',$email='')
	{
		$pic='';
		//$pic= SiteHelpers::avatar(32,$uid,$email);
		//$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		$firstline = $username;
		if (!empty($displayname))
			$firstline = $displayname;

		$secondline = $uid;
		if (!empty($username))
			$secondline .= ' | '.$username;

		$res = '<div class="media">

                        <div class="media-body clearfix w100p">
                            <div class="media-heading m0">
                                <div ><a href="'.base_url().'admin/clients/client/'.$uid.'"  onclick="ajaxViewDetail(\'#members\',this.href); return false; class="dark"><strong>'.$firstline.'</strong></a><br /><small class="text-off">'.$secondline.'</small></div>
                            </div>
                        </div>
                    </div>';


		return $res;
	}
	static public function avatar($value='')
	{
		$uid = 0;
		$firstname ='';
		$lastname  = '';
		$email = '';
		$arrvalue = explode(',',$value);

		$uid = $arrvalue[0];

		if (count($arrvalue) == 2 )
		{
			if ($arrvalue[1])
			{
				$firstname = $arrvalue[1];
			}
		}
		if (count($arrvalue) == 3 )
		{
			if ($arrvalue[1])
			{
				$firstname = $arrvalue[1];
			}
			if ($arrvalue[2])
			{
				$lastname = $arrvalue[2];
			}
		}

		if (count($arrvalue) == 4 )
		{
			if ($arrvalue[1])
			{
				$firstname = $arrvalue[1];
				$firstname=str_replace("{","",$firstname);
			}
			if ($arrvalue[2])
			{
				$lastname = $arrvalue[2];
				$lastname=str_replace("{","",$lastname);
			}
			if ($arrvalue[3])
			{
				$email = $arrvalue[3];
				$email=str_replace("{","",$email);
			}

		}



		$pic='';
		$pic= SiteHelpers::avatar(32,$uid,$email);
		$res = '<a href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$firstname.' '.$lastname.'</a>';

		return $res;
	}

	static public function formatorderno($value)
	{
		return '<a href="#"> #'.$value.'</a>';
	}

	static public function formatcheckbox($value)
	{

		$arrvalue = explode(',',$value);
		$uid = $arrvalue[0];
		$val=0;
		if (count($arrvalue) == 2 )
		{
			if ($arrvalue[1])
			{
				$val = $arrvalue[1];
			}
		}
		return "<span><input type='checkbox' name='_checkfield1' value ='checked' class='form-control' style='opacity=1 !important' /></span>";
		return "<input type='checkbox' name='checkfield".$uid."' value ='".ltrim(rtrim($val))."' class='form-control' />";
	}


	static public function formatwostatus($status)
	{
		$statuses = array(
			'wc-pending'    =>  'Pending Payment',
			'wc-processing' =>  'Processing',
			'wc-on-hold'    =>  'On Hold',
			'wc-completed'  =>  'Completed',
			'wc-cancelled'  =>  'Cancelled',
			'wc-refunded'   =>  'Refunded',
			'wc-failed'     =>  'Failed',
		);
		$status   = 'wc-' === substr( $status, 0, 3 ) ? substr( $status, 3 ) : $status;
		$status   = isset( $statuses[ 'wc-' . $status ] ) ? $statuses[ 'wc-' . $status ] : $status;

        $status_class = "label-danger";
        if ($status == "Pending Payment") {
            $status_class = "label-warning";
        } else if ($status == "Completed") {
            $status_class = "label-success";
        }else if ($status == "Processing") {
            $status_class = "label-info";
        }else if ($status == "Received") {
            $status_class = "label-sucess";
        }
		return "<span class='label $status_class large'>" . lang($status) . "</span> ";
	}

	static public function formatstatus($status)
	{
        $status_class = "label-danger";
        if ($status == "Pending") {
            $status_class = "label-warning";
        } else if ($status == "Completed") {
            $status_class = "label-success";
        }else if ($status == "Processing") {
            $status_class = "label-info";
        }else if ($status == "Received") {
            $status_class = "label-sucess";
        }
		return "<span class='label $status_class large'>" . lang($status) . "</span> ";
	}

	static public function formatwototal($id)
	{

		$statuses = array(
			'wc-pending'    =>  'Pending Payment',
			'wc-processing' =>  'Processing',
			'wc-on-hold'    =>  'On Hold',
			'wc-completed'  =>  'Completed',
			'wc-cancelled'  =>  'Cancelled',
			'wc-refunded'   =>  'Refunded',
			'wc-failed'     =>  'Failed',
		);
		$status   = 'wc-' === substr( $status, 0, 3 ) ? substr( $status, 3 ) : $status;
		$status   = isset( $statuses[ 'wc-' . $status ] ) ? $statuses[ 'wc-' . $status ] : $status;

		return $status;
	}

	static public function userlinkwithtooltip($id)
	{
		ob_start();
		$user = $this->db->query("Select user_id,username,sponsorkey,rank,downlines,email,display_name from sfm_uap_affiliates where user_id = $id")->row();
		$pic='';
		$pic= SiteHelpers::avatar(32,$id,$user->display_name);
		$res = '<a id="avatar-$id" href="'.base_url().'admin/clients/client/'.$uid.'" onclick="ajaxViewDetail(\'#members\',this.href); return false; " > '.$pic.' '.$user->display_name.'</a>';
		?>
		<div class="profile_pop_up_content" style="display: none;">
			<div class="profile_tooltip">
				<div class="image_tooltip">
					<?php $md5 = md5( strtolower( trim( $user->email ) ) ); ?>
					<a href="<?php echo ci_site_url()."profile/".$user->user_id; ?>"><img class="profile-rounded-image-small" src="https://www.gravatar.com/avatar/<?php echo $md5; ?>.jpg?d=mm&s=70" width="70" height="70" alt="s1" title="s1"></a>
				</div>
				<h3 class="name"><a href="<?php echo ci_site_url()."profile/".$user->user_id; ?>" name="title"><?php echo $user->display_name; ?></a></h3>
			</div>
			<div class="detail_tooltip">
				<p>
					Username: <strong><?php echo $user->username; ?></strong><br>
					Rank: <strong><?php echo $user->rank; ?></strong><br>
					Sponsor Key: <strong><?php echo $user->sponsorkey; ?></strong><br>
					Downlines: <strong><?php echo $user->downlines; ?></strong><br>
					Email: <strong><?php echo $user->email; ?></strong><br>
				</p>
			</div>
			<div class="action_tooltip">
				<div class="view_genealogy">
					<a href="<?php echo ci_site_url('genealogy/show/' . $user->user_id) ?>" title = "View Genealogy" onclick="ajaxViewDetail('#genealogy',this.href); return false;" >
						<i class="fa fa-sitemap"></i>
					</a>
				</div>
				<div class="send_message">
					<?php echo modal_anchor(get_uri("member/messages/modal_form/" . $user->user_id), "<i class='fa fa-envelope'></i> ", array("class"=> "","title"=> lang('Send_message'))); ?>
				</div>
				<div class="view_social">
					<a href="https://www.freemartfriends.com/<?php echo $user->username; ?>" target="_blank" title="View Social"><i class="fa fa-user"></i></a>
				</div>
			</div>
		</div>
		<script>
			$(document).on('mouseover', 'avatar-'+$id, function() {
			  $(this).children('.profile_pop_up_content').show();
			 });

			 $(document).on('mouseout', 'avatar-'+$id, function() {
			  $(this).children('.profile_pop_up_content').hide();
			 });
        </script>

		<?php
		$html = ob_get_clean();
		return $status;
	}

	static public function IPAddress($value)
	{
			if (empty($value))
				return;

			return '<div class="media">  <a href="http://geomaplookup.net?ip='.$value.'" title="GO to IP Address location" ><i class="fa fa-globe"></i></a>'.$value.' </div>';
	}

	static public function FormatTime($timestamp)
    {
    	if (empty($timestamp))
    		return ;

    // Get time difference and setup arrays
    $difference = time() - $timestamp;
    $periods = array("second", "minute", "hour", "day", "week", "month", "years");
    $lengths = array("60","60","24","7","4.35","12");

    // Past or present
    if ($difference >= 0)
    {
        $ending = "ago";
    }
    else
    {
        $difference = -$difference;
        $ending = "to go";
    }

    // Figure out difference by looping while less than array length
    // and difference is larger than lengths.
    $arr_len = count($lengths);
    for($j = 0; $j < $arr_len && $difference >= $lengths[$j]; $j++)
    {
        $difference /= $lengths[$j];
    }

    // Round up
    $difference = round($difference);

    // Make plural if needed
    if($difference != 1)
    {
        $periods[$j].= "s";
    }

    // Default format
    $text = "$difference $periods[$j] $ending";

    // over 24 hours
    if($j > 2)
    {
        // future date over a day formate with year
        if($ending == "to go")
        {
            if($j == 3 && $difference == 1)
            {
                $text = "Tomorrow at ". date("g:i a", $timestamp);
            }
            else
            {
                $text = date("F j, Y \a\\t g:i a", $timestamp);
            }
            return $text;
        }

        if($j == 3 && $difference == 1) // Yesterday
        {
            $text = "Yesterday at ". date("g:i a", $timestamp);
        }
        else if($j == 3) // Less than a week display -- Monday at 5:28pm
        {
            $text = date("l \a\\t g:i a", $timestamp);
        }
        else if($j < 6 && !($j == 5 && $difference == 12)) // Less than a year display -- June 25 at 5:23am
        {
            $text = date("F j \a\\t g:i a", $timestamp);
        }
        else // if over a year or the same month one year ago -- June 30, 2010 at 5:34pm
        {
            $text = date("M d, Y", $timestamp);
        }
    }

    return $text;
}


	static public function formatrank($rank)
	{

		if ($rank == 3)
		{
        	$rank_color = "rank-diamond";
        	$rank_text = "Diamond";
        }
		else if ($rank == 4)
		{
        	$rank_color = "rank-doublediamond";
        	$rank_text = "Double Diamond";}
		else if ($rank == 5)
		{
			$rank_color = "rank-triplediamond";
			$rank_text = "Triple Diamond";}
		else if ($rank == 6)
		{
			$rank_color = "rank-ambassador";
			$rank_text = "Ambassador";
		}
		else
		{
		    $rank_color = "rank-member";
			$rank_text = 'Member';
		}

		return "<span class='label round $rank_color large'>" . lang($rank_text) . "</span> ";
	}

}