<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Drivers
| 4. Helper files
| 5. Custom config files
| 6. Language files
| 7. Models
|
*/

$autoload['packages'] = array(
    APPPATH.'third_party/grocery_crud',
    //APPPATH.'third_party/image_crud',
    //APPPATH.'third_party/rest_server'
);

$autoload['libraries'] = array(
    'database',
    'session',
    'form_validation',
    'pagination',
    //'encrypt',
    'user_agent',
    'template',
    'finediff',
    'parser',
    'datatables',
    'mcurl');

$autoload['drivers'] = array('session','cache');

$autoload['helper'] = array('form','cookie','string','template','html','format','site','url','action_hooks','general','language','widget','inflector', 'date_time','wp','text');

// array(,, ,,,'form',,'general','text', 'array', 'string', 'number',  'security', 'country', 'email',,'ajax',','file',  , ,,'activity_logs', 'currency','app_files',    'url',  , 'security', 'download', 'captcha');

$autoload['config'] = array('app');
$autoload['config'] = array('pagination');

$autoload['language'] = array('default', 'custom');

//$autoload['model'] = array('db_validation_model', 'license_model', 'security_model', 'emailing_model', 'cont//ent_model', 'facebook_model', 'login_model');

//$autoload['model'] = array( );

// /*
 //$autoload['model'] = array(
 //   'Crud_model',
 //   'Announcements_model',
 //   'Settings_model',
 //   'Messages_model',
  //  'Notifications_model');
// 'Crud_model',
    // 'Settings_model',
    // 'Users_model',
    // 'Team_model',
    // 'Attendance_model',
    // 'Leave_types_model',
    // 'Leave_applications_model',
    // 'Events_model',
    // 'Announcements_model',
    // 'Messages_model',
    // 'Clients_model',
    // 'Projects_model',
    // 'Milestones_model',
    // 'Tasks_model',
    // 'Project_comments_model',
    // 'Activity_logs_model',
    // 'Project_files_model',
    // 'Notes_model',
    // 'Project_members_model',
    // 'Ticket_types_model',
    // 'Tickets_model',
    // 'Ticket_comments_model',
    // 'Items_model',
    // 'Invoices_model',
    // 'Invoice_items_model',
    // 'Invoice_payments_model',
    // 'Payment_methods_model',
    // 'Email_templates_model',
    // 'Roles_model',
    // 'Posts_model',
    // 'Timesheets_model',
    // 'Expenses_model',
    // 'Expense_categories_model',
    // 'Taxes_model',
    // 'Social_links_model',
    // 'Notification_settings_model',
    // 'Notifications_model',
    // 'Custom_fields_model',
    // 'Estimate_forms_model',
    // 'Estimate_requests_model',
    // 'Custom_field_values_model',
    // 'Estimates_model',
    // 'Estimate_items_model',
// );
