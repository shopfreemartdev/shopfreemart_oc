<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//$route['default_controller'] = 'crm/clients';
//$route['404_override'] = '';
//$route['translate_uri_dashes'] = FALSE;
//
//$route['admin'] = "crm/dashboard";


// $route['dashboard'] = "memberdashboard";
// $route['home'] = "memberdashboard";
// $route['member/dashboard'] = "memberdashboard";
// $route['member'] = "memberdashboard";
 $route['user/login'] = 'signin';

 // $route['admin'] = "crm/dashboard";
 // $route['admin/([a-z0-9A-Z]+)'] = "crm/$1";
 // $route['admin/(:any)'] = "crm/$1";


////MEMBERS ROUTING
define('MEMBERS_ROUTE', 'member');
//
//
////ADMIN ROUTING
define('ADMIN_ROUTE', 'admin');
//
//// STAFF ROUTING
define('STAFF_ROUTE', 'staff');

$route['unilevel/(:any)'] = "genealogy/$1";
$route['unilevel/(:any)/(:any)'] = "genealogy/$1/$2";
$route['unilevel'] = "genealogy";

// $route['profile/(:any)'] = "profile/index/$1";
$route['profile/([a-z0-9A-Z]+)'] = "profile/index/$1";

$route['myaccount/(:any)'] = "member/team_members/view/$1";
$route['team_members/view/(:any)'] = "member/team_members/view/$1";
$route['team_members/(:any)'] = "member/team_members/$1";
$route['members/viewdetails/(:any)'] = "member/team_members/view/$1";
$route['members/viewdetails/(:any)/(:any)'] = "member/team_members/$1/$2";
$route['tickets/(:any)'] = "member/tickets/view/$1";
$route['tickets/(:any)/(:any)'] = "member/tickets/$1/$2";
$route['tickets'] = "member/tickets";

$route['knowledge_base/(:any)'] = "member/knowledge_base/$1";
$route['knowledge_base/(:any)/(:any)'] = "member/knowledge_base/$1/$2";
$route['knowledge_base'] = "member/knowledge_base";

//$route['user/profile'] = "member/team_members/view/$1";

$route['signout'] = "signin/sign_out";
$route['logout'] = "signin/sign_out";
$route['logoff'] = "signin/sign_out";
$route['login'] = "signin";
$route['sign_out'] = "signin/sign_out";



$route[MEMBERS_ROUTE.'/coupons'] = "coupons";
$route[MEMBERS_ROUTE.'/coupons/(:any)'] = "coupons/$1";

$route[MEMBERS_ROUTE.'/trafficlogs'] = "trafficlogs";
$route[MEMBERS_ROUTE.'/trafficlogs/(:any)'] = "trafficlogs/$1";

$route[MEMBERS_ROUTE.'/clienthome'] = "clienthome";
$route[MEMBERS_ROUTE.'/downlines'] = "downlines";
$route[MEMBERS_ROUTE.'/earnings'] = "earnings";
$route[MEMBERS_ROUTE.'/foundingmembership'] = "foundingmembership";
$route[MEMBERS_ROUTE.'/knowledge_base_articles'] = "knowledge_base_articles";
$route[MEMBERS_ROUTE.'/myorders'] = "myorders";
$route[MEMBERS_ROUTE.'/payments'] = "payments";
$route[MEMBERS_ROUTE.'/reports'] = "reports";
$route[MEMBERS_ROUTE.'/reports/expenses'] = "reports/expenses";
$route[MEMBERS_ROUTE.'/reports/expenses_income'] = "reports/expenses_income";
$route[MEMBERS_ROUTE.'/reports/expenses_vs_income'] = "reports/expenses_vs_income";
$route[MEMBERS_ROUTE.'/reports/sales'] = "reports/sales";
$route[MEMBERS_ROUTE.'/rewards'] = "rewards";
$route[MEMBERS_ROUTE.'/shoporders'] = "shoporders";
$route[MEMBERS_ROUTE.'/support'] = "support";
// $route[MEMBERS_ROUTE.'/surveys'] = "surveys";
$route[MEMBERS_ROUTE.'/tickets'] = "tickets";
$route[MEMBERS_ROUTE.'/transactions'] = "transactions";
$route[MEMBERS_ROUTE.'/transfers'] = "transfers";
$route[MEMBERS_ROUTE.'/transfers/(:any)'] = "transfers/$1";
$route[MEMBERS_ROUTE.'/unilevel'] = "unilevel";
$route[MEMBERS_ROUTE.'/utilities/bulk_pdf_exporter'] = "utilities/bulk_pdf_exporter";
$route[MEMBERS_ROUTE.'/utilities/media'] = "utilities/media";
$route[MEMBERS_ROUTE.'/withdrawals'] = "withdrawals";
$route[MEMBERS_ROUTE.'/withdrawals/(:any)'] = "withdrawals/$1";



define('PROFILE_ROUTE', 'profiles');
$route[PROFILE_ROUTE.'/([a-z0-9A-Z]+)'] = "profiles/users/$1";

//CONTENT ROUTING
define('CONTENT_ROUTE', 'content');


//CONTENT CATEGORIES ROUTING
define('CONTENT_CATEGORIES_ROUTE', 'content_categories');
$route[CONTENT_CATEGORIES_ROUTE.'/:any'] = "content_categories/$1";

//FAQ ROUTING
define('FAQ_ROUTE', 'faq');

//KB ROUTING
define('KB_ROUTE', 'kb');

//FAQ CATEGORIES
define('FAQ_CATEGORIES_ROUTE', 'faq_categories');
$route[FAQ_CATEGORIES_ROUTE.'/:any'] = "faq_categories/$1";

//AFFILIATE ROUTING
define('AFFILIATE_ROUTE', 'affiliates');
$route[AFFILIATE_ROUTE.'/:any'] = "refer/id/$1";

//AFFILIATE TOOLS ROUTING
define('AFF_TOOLS_ROUTE', 'aff_tools');
$route[AFF_TOOLS_ROUTE.'/:any'] = "refer/id/$1";

//URI ROUTING OPTION
define('AFF_ROUTE_OPTION', 'replication'); //change to regular or replication



//ADMIN LOGIN ROUTE
define('ADMIN_LOGIN_ROUTE', 'admin_login');
$route[ADMIN_LOGIN_ROUTE] = 'admin_login';

//REPLICATION ROUTING
define('REPLICATION_ROUTE', 'reps');
$route[REPLICATION_ROUTE.'/:any'] = "reps/id/$1";

//ROTATOR ROUTING
$route['rotator/:any'] = "rotator/rotate/$1/$2";


//404 ERROR ROUTING
define('ERROR_404_ROUTE', 'error');

//REGISTRATION ROUTE
$route['registration'] = "checkout/registration";
$route['registration/:any']= "checkout/registration/$1";

//EMAIL PAYMENT LINK
$route['payments/make_payment/:any'] = 'login/general';

//REPORT ROUTE
//$route[MEMBERS_ROUTE . '/([a-z0-9A-Z_]+)/report/([0-9]+)/([0-9]+)'] = "modules/$1/generate/$2/$3";

//PRODUCTS
define('PRODUCTS_ROUTE', 'products');

//MANUFACTURERS
define('MANUFACTURERS_ROUTE', 'manufacturers');
$route[MANUFACTURERS_ROUTE.'/:any'] = "manufacturers/$1";
$route['brands'] = "manufacturers";
$route['brands/:any'] = "manufacturers/$1";

//PRODUCT CATEGORIES ROUTING
define('CATEGORIES_ROUTE', 'product_categories');
$route[CATEGORIES_ROUTE.'/:any'] = "product_categories/index/$1";

//$route['brands'] = "manufacturers/index/$1";

//STORE HOME
define('STORE_ROUTE', 'store');
$route[STORE_ROUTE] = "home/index/store";

//TRACKING
$route['track/:any'] = "track/index/$1";

$route['maintenance'] = "home/maintenance";
$route['switch_language/:any'] = "home/switch_language/$1";
$route['switch_currency/:any'] = "home/switch_currency/$1";

$route['maintenance'] = "home/maintenance";
$route['switch_language/:any'] = "home/switch_language/$1";
$route['switch_currency/:any'] = "home/switch_currency/$1";


//DEFAULT ROUTES

$route['scaffolding_trigger'] = "dixlaerox2882jd92u8d43qadf82dd9aj2jd9a2j23jd8s";


$route['contact'] = "forms/contact_us/";
///ute['blog'] = "content";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
define('SSL_MEMBERS_AREA','member');
define('SSL_PUBLIC_AREA','');

$route["blank/(:any)"] = "page/blank/%1";


// FOR CRM
$route['admin']  = "crm/home";
$route['admin/(:any)']  = "crm/$1";
$route['admin/(:any)/(:any)']  = "crm/$1/$2";
$route['admin/(:any)/(:any)/(:any)']  = "crm/$1/$2/$3";
$route['admin/(:any)/(:any)/(:any)/(:any)']  = "crm/$1/$2/$3/$4";
$route['admin/(:any)/(:any)/(:any)/(:any)/(:any)']  = "crm/$1/$2/$3/$4/$5";


// Misc controller rewrites
$route['crm/admin/access_denied']  = "crm/misc/access_denied";
$route['crm/admin/not_found']  = "crm/misc/not_found";

// Staff rewrites
$route['crm/admin/profile']  = "crm/staff/profile";
$route['crm/admin/profile/(:num)']  = "crm/staff/profile/$1";
$route['crm/admin/profile/(:num)/(:any)']  = "crm/staff/profile/$1/$2";

/* Clients links and routes */
// // In case if client access directly to url without the arguments redirect to clients url
$route['crm/']  = "crm/clients";
$route['crm/viewinvoice']  = "crm/clients/viewinvoice";
$route['crm/viewinvoice/(:num)/(:any)']  = "crm/clients/viewinvoice/$1/$2";

$route['crm/viewestimate/(:num)/(:any)']  = "crm/clients/viewestimate/$1/$2";
$route['crm/viewestimate']  = "crm/clients/viewestimate";

$route['crm/viewproposal/(:num)/(:any)']  = "crm/clients/viewproposal/$1/$2";

// $route['crm/survey/(:num)/(:any)']  = "crm/clients/survey/$1/$2";
$route['crm/knowledge_base']  = "crm/clients/knowledge_base";
$route['crm/knowledge_base/(:any)']  = "crm/clients/knowledge_base/$1";

$route['crm/knowledge-base']  = "crm/clients/knowledge_base";
$route['crm/knowledge-base/(:any)']  = "crm/clients/knowledge_base/$1";

$route['default_controller'] = 'memberdashboard';

include_once "pageroutes.php";


?>