<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Authentication_model extends SB_model
{
    function __construct()
    {
        parent::__construct();
       // $this->load->model('crm/user_autologin');
    }

   /**
     * @return boolean
     * Check if autologin found
     */
    public function autologin()
    {

        if (strpos(current_full_url(), 'clients/login_as_client') !== false) {
            return;
        }

          //set_cookie('winID__'.get_client_user_id(),get_client_user_id());
          if (is_staff_logged_in())
          {
  
              $client_id = (int)get_cookie('client_user_id');
              //  l($this->session);
              // l(get_client_user_id());
              //  ld($_COOKIE);
              if ($client_id > 0)
              {

                   // l($client_id);
                   // l(get_client_user_id());
                   // l($this->session);
                    if ($client_id != get_client_user_id())
                    {

                        $this->logged_client($client_id,true);
                        $this->session->set_userdata(array(
                            'logged_in_as_client' => true
                         ));  

                        return true;
                    }
                    else
                    {

                        $this->session->set_userdata(array(
                            'logged_in_as_client' => true
                         ));                        
                        return true; 
                    }
              }
          }

        // LOGOUT CLIENT IF WPLOGIN CANT FIND ANY LOGGED USER
        if (USE_WPLOGIN_ON_BACKEND )
        {
        	return false;
            $wp_currentuser = get_wp_logged_user();

            //If no logged user found from wp
            if(!$wp_currentuser)
            {
                if (get_client_user_id())
                {
                    $this->logout();
                }
            }
            else
            {
                // check if wpuser and current user is different
                if (get_client_username() !=  $wp_currentuser)
                {
                    $current_user = $this->db->query("Select user_id from sfm_uap_affiliates where username = '$wp_currentuser'")->row();
                    if($current_user)
                    {
                        $this->logged_client($current_user->user_id);
                        return true;
                    }
                    return false; 
                }
            }

        }

        $this->load->helper('cookie');
        if (!is_logged_in()) {
            if ($cookie = get_cookie('autologin', true)) {
                $data = unserialize($cookie);
                if (isset($data['key']) AND isset($data['user_id'])) {
                    if (!is_null($user = $this->Users_model->user_get($data['user_id'], md5($data['key'])))) {
                        // Login user
                        if ($user->staff == 1) {
                            $user_data = array(
                                'staff_user_id' => $user->id,
                                'staff_logged_in' => true
                            );
                        } else {
                            $user_data = array(
                                'client_user_id' => $user->id,
                                'client_logged_in' => true
                            );
                        }
                        $this->session->set_userdata($user_data);
                        // Renew users cookie to prevent it from expiring
                        set_cookie(array(
                            'name' => 'autologin',
                            'value' => $cookie,
                            'expire' => 60 * 60 * 24 * 31 * 2 // 2 months
                        ));
                        $this->update_login_info($user->id, $user->staff);
                        return true;
                    }
                }
            }
        }
        else
        {

          //set_cookie('winID__'.get_client_user_id(),get_client_user_id());
          if (is_staff_logged_in())
          {

              $client_id = (int)get_cookie('client_user_id');
               // l('cookie: '.$client_id.' server:'. get_client_user_id());
              if ($client_id > 0)
              {

                    if ($client_id != get_client_user_id())
                    {
                        $this->logged_client($client_id,true);
                        return true;
                    }
              }
              return true;
          }
        }

        // IF USING WORDPRESS LOGIN
        // AUTOLOGIN CLIENT FROM WP
      // ld($this->session->userdata);
      
        if (USE_WPLOGIN_ON_BACKEND )
        {
        	return false;
            $wp_currentuser = get_wp_logged_user();

            //If no logged user found from wp
            if(!$wp_currentuser)
            {
                if (get_client_user_id())
                {
                    $this->logout();
                }
                return false;
            }
            else
            {
                //If logged user found from wp or username is different
                //If user session not found , then manually create
                //$this->add_user($wp_currentuser,'',true); AUTO ADD USER IF NOT FOUND

                if (!get_client_user_id())
                {

                    // NOW CHECK FROM THE APP USER TABLE
                    $current_user = $this->db->query("Select user_id from sfm_uap_affiliates where username = '$wp_currentuser'")->row();
                    if(!$current_user)
                    {
                        return false;
                    }

                    $this->logged_client($current_user->user_id);
                    return true;
                }
            }
        }


        return false;
    }



    function login($username, $password, $remember=false, $staff=false)
    {


        if ((!empty($username)) AND (!empty($password))) {

            // IF STAFF
            if ($staff == true) {

                $table = 'main_shopfreemart.sfm_crm_staff';
                $_id   = 'staffid';
                $this->db->where('username', $username);

                $user = $this->db->get($table)->row();
                if ($user) {
                    $email = $user->email;

                    // Email is okey lets check the password now
                    $this->load->helper('phpass');
                    $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                    if (!$hasher->CheckPassword($password, $user->password)) {
                        // Password failed, return
                        return false;
                    }
                } else {
                    //log staff activity 
                    $activitytype = 44; // failed login attempt
                    crmlogActivity('Failed Login Attempt. Username: '.$username.', Is Staff Member: ' . ($staff == true ? 'Yes' : 'No') . ', IP: ' . $this->input->ip_address() . ']', $activitytype, '');
                
                    return false;
                }
                if ($user->active == 0) {
                    //log staff activity 
                    $activitytype = 45; // inactive user login attempt
                    crmlogActivity('Inactive User Tried to Login. Username: '.$username.', Is Staff Member:' . ($staff == true ? 'Yes' : 'No') . ', IP:' . $this->input->ip_address() . ']', $activitytype, $user->staffid);
                    return array(
                        'memberinactive' => true
                    );
                }
                perfex_do_action('before_staff_login', array(
                    'email' => $email,
                    'user_id' => $user->$_id
                ));

                $arraydata = array(
                'logged_in'  => true,
                'email' => $email,
                'user_id' => $user->$_id,
                'staff_user_id' => $user->$_id,
                'staff_username'=> $username,
                'staff_logged_in' => true,
                'uid' => $user->$_id
                );
                get_instance()->session->set_userdata($arraydata);
                return true;

            }
            else
            {

            // IF CLIENT

                $table = $this->Users_model->table;;
                $_id   =$this->Users_model->table_column_pri;
                $_user = $this->Users_model->table_column_user;

                $this->db->where($_user, $username);

                $user_info = $this->db->get($table)->row();

                if ($user_info)
                {

                    // IF USING WORDPRESS LOGIN
                    if (USE_WPLOGIN_ON_BACKEND)
                    {

                        $result = WP_autologin($username,$password);
                        if (!$result)
                        {

                            return false;
                        }
                    }
                    $this->logged_client($user_info->ID);
                    return true;
                }
            }
        }
        return false;
    }

    function stafflogout()
    {

        if (is_staff_logged_in()) {
           perfex_do_action('before_staff_logout', get_staff_user_id());

        }
        

        // LOGOUT STAFF
        $this->session->set_userdata(array(
            'logged_in_as_client' => false,
            'staff_user_id' => null,
            'staff_logged_in' => false
         ));

        $this->session->unset_userdata('logged_in_as_client');
        $this->session->unset_userdata('staff_user_id');
        $this->session->unset_userdata('staff_logged_in');

        if (!get_staff_user_id() && !get_client_user_id())
        {
            $this->session->unset_userdata('logged_in');
        }
        else
        {
             $this->session->set_userdata('logged_in',true);
        }

    }

    /**
     * @param  boolean If Client or Staff
     * @return none
     */
    function logout($staff = false)
    {

        if (is_client_logged_in()) {
            //CLIENT
            perfex_do_action('before_client_logout', get_client_user_id());
        }

        // LOGOUT CLIENT
        $this->session->set_userdata(array(
             'uid'    => '',
             'username'    => '',
             'aid'    => '',
             'gid'    => '',
             'eid'    => '',
             'll'    => '',
             'fid'    => '',
             'memberdata'    => '',
             'm_aff_marketing' => '',
             'permissions' => array(),
            'login_user' => null,
            'email' => null,
            'user_id' => null,
            'contact_user_id' => null,
            'client_user_id' => null,
            'client_data' => null,
            'contact_user_id' => null,
            'client_logged_in' => false,
         ));
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('contact_user_id');
        $this->session->unset_userdata('client_user_id');
        $this->session->unset_userdata('client_data');
        // END

        if (!is_client_logged_in() && !is_staff_logged_in())
        {
            $this->session->unset_userdata('logged_in');
        }
        else
        {
             $this->session->set_userdata('logged_in',true);
        }

        //$this->session->sess_destroy();


        //     if (USE_WPLOGIN_ON_BACKEND)
        //     {
        //         WP_signout();
        //         get_instance()->session->sess_destroy();
        //         get_instance()->load->helper('cookie');
        //         if (!defined('COOKIEHASH')){
        //             define( 'COOKIEHASH', md5( WP_URL ) );
        //         }
        //         $cookiename = "wordpress_logged_in_" . COOKIEHASH;
        //         delete_cookie($cookiename);
        //         $this->session->sess_destroy();
        //         redirect(WP_URL);
        //     }

        // } else {

        //     //STAFF
        //     $this->delete_autologin($staff);
        //     perfex_do_action('before_staff_logout', get_client_user_id());
        //     $this->session->unset_userdata('email');
        //     $this->session->unset_userdata('user_id');
        //     $this->session->unset_userdata('uid');
        //     $this->session->unset_userdata('staff_user_id');
        //     $this->session->unset_userdata('staff_logged_in');
        //     $this->session->sess_destroy();
        // }

    }

    function logged_client($user_id, $logged_as_client = false)
    {
        $user = $this->Users_model->get_user_data($user_id);

        $login_user = $this->Users_model->get_access_info($user_id);
        $arraydata = array(
        'logged_in'  => true,
        'uid'    => $user_id,
        'username'    => $user->username,
        'aid'    =>  $user_id,
        //'gid'    => 1,
        'eid'    =>  $user->email,
        'fid'    => $user->display_name,
        //'memberdata' => $user,
        //'m_aff_marketing' => 1,
        'permissions' => array(),
        'login_user' => $login_user,
        'email' => $user->email,
        'user_id' => $user_id,
        'contact_user_id' => $user_id,
        'client_user_id' => $user_id,
        'client_data' => $user,
        'contact_user_id' => $user_id,
        'client_logged_in' => true);
        $this->session->set_userdata($arraydata);

    }
    /**
     * @param  integer ID to create autologin
     * @param  boolean Is Client or Staff
     * @return boolean
     */
    private function create_autologin($user_id, $staff)
    {
        // $this->load->helper('cookie');
        // $key = substr(md5(uniqid(rand() . get_cookie($this->config->item('sess_cookie_name')))), 0, 16);
        // $this->user_autologin->delete($user_id, $key, $staff);
        // if ($this->user_autologin->set($user_id, md5($key), $staff)) {
        //     set_cookie(array(
        //         'name' => 'autologin',
        //         'value' => serialize(array(
        //             'user_id' => $user_id,
        //             'key' => $key
        //         )),
        //         'expire' => 60 * 60 * 24 * 31 * 2 // 2 months
        //     ));
        //     return true;
        // }
        // return false;
    }
    /**
     * @param  boolean Is Client or Staff
     * @return none
     */
    private function delete_autologin($staff)
    {
        // $this->load->helper('cookie');
        // if ($cookie = get_cookie('autologin', true)) {
        //     $data = unserialize($cookie);
        //     $this->user_autologin->delete($data['user_id'], md5($data['key']), $staff);
        //     delete_cookie('autologin', 'aal');
        // }
    }


    /**
     * @param  integer ID
     * @param  boolean Is Client or Staff
     * @return none
     * Update login info on autologin
     */
    private function update_login_info($user_id, $staff)
    {
        // $table = 'main_crm.crm_contacts';
        // $_id   = 'id';
        // if ($staff == true) {
        //     $table = 'main_crm.crm_staff';
        //     $_id   = 'staffid';
        // }
        // $this->db->set('last_ip', $this->input->ip_address());
        // $this->db->set('last_login', date('Y-m-d H:i:s'));
        // $this->db->where($_id, $user_id);
        // $this->db->update($table);
    }
    /**
     * Send set password email
     * @param string $email
     * @param boolean $staff is staff of contact
     */
    public function set_password_email($email, $staff)
    {
        $table = 'main_crm.crm_contacts';
        $_id   = 'id';
        if ($staff == true) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $_id   = 'staffid';
        }
        $this->db->where('email', $email);
        $user = $this->db->get($table)->row();
        if ($user) {
            if ($user->active == 0) {
                return array(
                    'memberinactive' => true
                );
            }
            $new_pass_key = md5(rand() . microtime());
            $this->db->where($_id, $user->$_id);
            $this->db->update($table, array(
                'new_pass_key' => $new_pass_key,
                'new_pass_key_requested' => date('Y-m-d H:i:s')
            ));
            if ($this->db->affected_rows() > 0) {
                $this->load->model('emails_model');
                $data['new_pass_key'] = $new_pass_key;
                $data['staff']        = $staff;
                $data['user_id']       = $user->$_id;
                $data['email']        = $email;

                if (is_dir(CRM_MODULE_PATH . 'views/my_email') || is_dir(CRM_MODULE_PATH . 'views/email')) {
                    $send = $this->emails_model->send_email($user->email, _l('password_set_email_subject', perfex_get_option('companyname')), 'set-password', $data);
                } else {
                    $merge_fields = array();
                    if ($staff == false) {
                        $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($user->user_id, $user->$_id));
                    } else {
                        $merge_fields = array_merge($merge_fields, get_staff_merge_fields($user->$_id));
                    }
                    $merge_fields = array_merge($merge_fields, get_password_merge_field($data, $staff, 'set'));
                    $send         = $this->emails_model->send_email_template('contact-set-password', $user->email, $merge_fields);
                }

                if ($send) {
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
    /**
     * @param  string Email from the user
     * @param  Is Client or Staff
     * @return boolean
     * Generate new password key for the user to reset the password.
     */
    public function forgot_password($email, $staff = false)
    {
        $table = 'main_crm.crm_contacts';
        $_id   = 'id';
        if ($staff == true) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $_id   = 'staffid';
        }
        $this->db->where('email', $email);
        $user = $this->db->get($table)->row();

        if ($user) {
            if ($user->active == 0) {
                return array(
                    'memberinactive' => true
                );
            }

            $new_pass_key = md5(rand() . microtime());
            $this->db->where($_id, $user->$_id);
            $this->db->update($table, array(
                'new_pass_key' => $new_pass_key,
                'new_pass_key_requested' => date('Y-m-d H:i:s')
            ));

            if ($this->db->affected_rows() > 0) {
                $this->load->model('emails_model');
                $data['new_pass_key'] = $new_pass_key;
                $data['staff']        = $staff;
                $data['user_id']       = $user->$_id;
                if (is_dir(CRM_MODULE_PATH . 'views/my_email') || is_dir(CRM_MODULE_PATH . 'views/email')) {
                    $send = $this->emails_model->send_email($user->email, _l('password_reset_email_subject', perfex_get_option('companyname')), 'forgot-password', $data);
                } else {
                    $merge_fields = array();
                    if ($staff == false) {
                        $template     = 'contact-forgot-password';
                        $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($user->user_id, $user->$_id));
                    } else {
                        $template     = 'staff-forgot-password';
                        $merge_fields = array_merge($merge_fields, get_staff_merge_fields($user->$_id));
                    }
                    $merge_fields = array_merge($merge_fields, get_password_merge_field($data, $staff, 'forgot'));
                    $send         = $this->emails_model->send_email_template($template, $user->email, $merge_fields);
                }
                if ($send) {
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }
    /**
     * Update user password from forgot password feature or set password
     * @param boolean $staff        is staff or contact
     * @param mixed $user_id
     * @param string $new_pass_key the password generate key
     * @param string $password     new password
     */
    public function set_password($staff, $user_id, $new_pass_key, $password)
    {
        if (!$this->can_set_password($staff, $user_id, $new_pass_key)) {
            return array(
                'expired' => true
            );
        }
        $this->load->helper('phpass');
        $hasher   = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        $password = $hasher->HashPassword($password);
        $table    = 'main_crm.crm_contacts';
        $_id      = 'id';
        if ($staff == true) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $_id   = 'staffid';
        }
        $this->db->where($_id, $user_id);
        $this->db->where('new_pass_key', $new_pass_key);
        $this->db->update($table, array(
            'password' => $password
        ));
        if ($this->db->affected_rows() > 0) {
            logActivity('User Set Password [User ID:' . $user_id . ', Is Staff Member:' . ($staff == true ? 'Yes' : 'No') . ', IP:' . $this->input->ip_address() . ']');
            $this->db->set('new_pass_key', NULL);
            $this->db->set('new_pass_key_requested', NULL);
            $this->db->set('last_password_change', date('Y-m-d H:i:s'));
            $this->db->where($_id, $user_id);
            $this->db->where('new_pass_key', $new_pass_key);
            $this->db->update($table);
            return true;
        }
        return null;
    }
    /**
     * @param  boolean Is Client or Staff
     * @param  integer ID
     * @param  string
     * @param  string
     * @return boolean
     * User reset password after successful validation of the key
     */
    public function reset_password($staff, $user_id, $new_pass_key, $password)
    {
        if (!$this->can_reset_password($staff, $user_id, $new_pass_key)) {
            return array(
                'expired' => true
            );
        }
        $this->load->helper('phpass');
        $hasher   = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        $password = $hasher->HashPassword($password);
        $table    = 'main_crm.crm_contacts';
        $_id      = 'id';
        if ($staff == true) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $_id   = 'staffid';
        }

        $this->db->where($_id, $user_id);
        $this->db->where('new_pass_key', $new_pass_key);
        $this->db->update($table, array(
            'password' => $password
        ));
        if ($this->db->affected_rows() > 0) {
            logActivity('User Reseted Password [User ID:' . $user_id . ', Is Staff Member:' . ($staff == true ? 'Yes' : 'No') . ', IP:' . $this->input->ip_address() . ']');
            $this->db->set('new_pass_key', NULL);
            $this->db->set('new_pass_key_requested', NULL);
            $this->db->set('last_password_change', date('Y-m-d H:i:s'));
            $this->db->where($_id, $user_id);
            $this->db->where('new_pass_key', $new_pass_key);
            $this->db->update($table);
            $this->load->model('emails_model');
            $this->db->where($_id, $user_id);
            $user          = $this->db->get($table)->row();
            $data['email'] = $user->email;

            if (is_dir(CRM_MODULE_PATH . 'views/my_email') || is_dir(CRM_MODULE_PATH . 'views/email')) {
                $this->emails_model->send_email($user->email, _l('password_changed_email_subject'), 'reset-password', $data);
            } else {
                $merge_fields = array();
                if ($staff == false) {
                    $template     = 'contact-password-reseted';
                    $merge_fields = array_merge($merge_fields, get_client_contact_merge_fields($user->user_id, $user->$_id));
                } else {
                    $template     = 'staff-password-reseted';
                    $merge_fields = array_merge($merge_fields, get_staff_merge_fields($user->$_id));
                }
                $this->emails_model->send_email_template($template, $user->email, $merge_fields);
            }

            return true;
        }
        return null;
    }
    /**
     * @param  integer Is Client or Staff
     * @param  integer ID
     * @param  string Password reset key
     * @return boolean
     * Check if the key is not expired or not exists in database
     */
    public function can_reset_password($staff, $user_id, $new_pass_key)
    {
        $table = 'main_crm.crm_contacts';
        $_id   = 'id';
        if ($staff == true) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $_id   = 'staffid';
        }

        $this->db->where($_id, $user_id);
        $this->db->where('new_pass_key', $new_pass_key);
        $user = $this->db->get($table)->row();
        if ($user) {
            $timestamp_now_minus_1_hour = time() - (60 * 60);
            $new_pass_key_requested     = strtotime($user->new_pass_key_requested);
            if ($timestamp_now_minus_1_hour > $new_pass_key_requested) {
                return false;
            }
            return true;
        }
        return false;
    }
    /**
     * @param  integer Is Client or Staff
     * @param  integer ID
     * @param  string Password reset key
     * @return boolean
     * Check if the key is not expired or not exists in database
     */
    public function can_set_password($staff, $user_id, $new_pass_key)
    {
        $table = 'main_crm.crm_contacts';
        $_id   = 'id';
        if ($staff == true) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $_id   = 'staffid';
        }
        $this->db->where($_id, $user_id);
        $this->db->where('new_pass_key', $new_pass_key);
        $user = $this->db->get($table)->row();
        if ($user) {
            $timestamp_now_minus_48_hour = time() - (3600 * 48);
            $new_pass_key_requested      = strtotime($user->new_pass_key_requested);
            if ($timestamp_now_minus_48_hour > $new_pass_key_requested) {
                return false;
            }
            return true;
        }
        return false;
    }
}
