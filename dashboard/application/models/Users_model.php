<?php

class Users_model extends SB_model {

    public $table = 'sfm_users'; // 'tb_users';
    public $table_column_pri = 'id'; //'id';
    public $table_column_select =  'id,user_login,display_name,user_email'; //0=id, 1=username, 2=display_name;
    public $table_column_user =  'user_login'; //'username';
    public $table_column_pass = 'user_pass'; //'password';
    public $default_user_theme = 'shop';

    public $username = false;
    public $display_name = false;
    public $is_user_logged = false;
    public $is_admin = false;
    public $user_data = null;
    public $USE_WPLOGIN = 1;

    function __construct() {

        parent::__construct($this->table);
        //check_logged_user();
        $this->USE_WPLOGIN = USE_WPLOGIN_ON_BACKEND;
        if ($this->USE_WPLOGIN)
        {
            get_instance()->load->helper('wp');
        }

    }

    function sync($id)
    {

        if ($id)
        {
            //sync_data($id);
            $result = $this->db->query("CALL main_shopfreemart.update_user_info($id);");
            return;
        }
    }

    // function check_logged_user($redirectiflogged=false)
    // {
    //     $this->is_user_logged = false;
    //     $this->login_user_id() = false;
    //     $this->username = false;
    //     $this->display_name = false;

    //     // IF USING WORDPRESS LOGIN
    //     if ($this->USE_WPLOGIN)
    //     {

    //         $wp_currentuser = get_wp_logged_user();

    //         //If no logged user found from wp
    //         if(!$wp_currentuser)
    //         {

    //             redirect($this->login_redirect,301);

    //             if (isset(get_instance()->session))
    //             {
    //              get_instance()->session->unset_userdata(array(
    //                  'logged_in'  => false,
    //                  'uid'    => '',
    //                  'username'    => '',
    //                  'aid'    => '',
    //                  'gid'    => '',
    //                  'eid'    => '',
    //                  'll'    => '',
    //                  'fid'    => '',
    //                  'memberdata'    => '',
    //                  'm_aff_marketing' => '',
    //                  'permissions' => array(),
    //                  'site_theme' => '',
    //              ));
    //              }
    //              if ($redirectiflogged)
    //              {

    //                 redirect($this->login_redirect,301);
    //              }
    //              return false;
    //         }


    //         //If logged user found from wp or username is different
    //         if (!get_instance()->session->userdata('logged_in') or get_instance()->session->userdata('username') != $wp_currentuser)
    //         {

    //             //If user session not found , then manually create
    //             //$this->add_user($wp_currentuser,'',true); AUTO ADD USER IF NOT FOUND

    //             // NOW CHECK FROM THE APP USER TABLE
    //             $this->db->select($this->table_column_select);
    //             $current_user = $this->db->get_where($this->table,array($this->table_column_user => $wp_currentuser))->row();
    //             if(!$current_user)
    //             {
    //                 return false;
    //             }
    //             $current_member = $this->get_user_data($wp_currentuser);
    //             get_instance()->session->set_userdata(array(
    //             'logged_in'  => true,
    //             'uid'    =>  $current_user->id,
    //             'username'    =>$wp_currentuser,
    //             'aid'    => $current_user->id,
    //             'gid'    => 1,
    //             'eid'    => $current_user->user_email,
    //             'fid'    => $current_user->display_name,
    //             'memberdata' => $current_member,
    //              'm_aff_marketing' => 1,
    //              'permissions' => array(),
    //              'site_theme' => $this->default_user_theme,
    //             ));
    //         }

    //     }
    //     else
    //     // IF USING CI LOGIN
    //     {
    //         //If logged user found
    //         if (!get_instance()->session->userdata('logged_in'))
    //         {
    //             return false;
    //         }
    //     }

    //     // USER IS LOGGED FROM THIS LINE
    //     $this->is_user_logged = true;
    //     $this->login_user_id() = get_instance()->session->userdata('uid');
    //     $this->username = get_instance()->session->userdata('username');
    //     $this->display_name = get_instance()->session->userdata('fid');
    //     $this->affiliate_id = get_instance()->session->userdata('aid');
    //     $this->login_user = get_instance()->session->userdata('login_user');

    //     if ($redirectiflogged)
    //     {
    //          if (!empty($this->logout_redirect))
    //          {
    //             redirect($this->login_redirect,301);
    //          }
    //     }

    // }

    function get_accountdetails( $user_id = 0,$row_array = 0) {
        // if (!is_numeric($user_id)) return;
        //   $user = $this->db->get_where('sfm_uap_affiliates',array('user_id' => $user_id))->row();
        // if(!$user)
        // {
        // $result = $this->db->query("
        //     INSERT INTO sfm_uap_affiliates (id, uid, user_id,username, rank_id, date_registered, status, sponsorkey)

        //         Select distinct
        //         main_freemart.wp_users.id as id,
        //         wp_users.id as uid,
        //         wp_users.id as user_id,
        //         wp_users.user_login as username,
        //         1 as rank_id,
        //         user_registered as start_date ,
        //         1 as status ,
        //         wu1.meta_value as referral_id
        //         from
        //         main_freemart.wp_users
        //         LEFT JOIN main_freemart.wp_usermeta wu1 ON ( main_freemart.wp_users.ID = wu1.user_id and wu1.meta_key = 'referral_id')
        //         WHERE (main_freemart.wp_users.id = $user_id);");
        // }
        $sql = "
        Select
            sfm_uap_affiliates.* ,
            sfm_uap_affiliates.firstname as first_name ,
            sfm_uap_affiliates.lastname as last_name ,
            sfm_uap_affiliates.company as company_name ,
            nr.label as next_rank,
            af1.display_name as upline_diamond,
            af2.display_name as upline_doublediamond,
            af3.display_name as upline_triplediamond,
            af4.display_name as upline_ambassador,
            af1.user_id as upline_diamond_id,
            af2.user_id as upline_doublediamond_id,
            af3.user_id as upline_triplediamond_id,
            af4.user_id as upline_ambassador_id,
            af6.display_name as sponsor_name,
            sfm_uap_affiliates.user_id as client_id,
            af6.avatar,
            '0' as permissions

            from
            sfm_uap_affiliates
            left join sfm_uap_ranks nr on sfm_uap_affiliates.rank_id + 1 = nr.id
            left join sfm_uap_affiliates af1 on sfm_uap_affiliates.upline_diamond = af1.user_id
            left join sfm_uap_affiliates af2 on sfm_uap_affiliates.upline_doublediamond = af2.user_id
            left join sfm_uap_affiliates af3 on sfm_uap_affiliates.upline_triplediamond = af3.user_id
            left join sfm_uap_affiliates af4 on sfm_uap_affiliates.upline_ambassador = af4.user_id
            left join sfm_uap_affiliates af6 on sfm_uap_affiliates.sponsorkey = af6.user_id
            WHERE sfm_uap_affiliates.deleted <> 1 AND sfm_uap_affiliates.user_id=$user_id ";

        $result = $this->db->query('SET SQL_BIG_SELECTS=1;');
         $result = $this->db->query($sql);

        if ($result->num_rows()) {
            if ($row_array)
            {
             $row =  $result->row_array();
             $row['permissions'] = array();
            }
            else
            {
             $row =  $result->row();
             $row->permissions = array();
            }
            return $row;
        }
        return null;
    }

    function add_user($username,$password='1234',$getdatafromwp=1)
    {

        //Verify user first if exist
        $this->db->select($this->table_column_select);
        $current_member = $this->db->get_where($this->table,array($this->$table_column_user => $username))->row();

        if(!$current_member)
        {
            if ($getdatafromwp)
            {
                $wp_user = $this->db->get_where('wp_user', array('user_login'=> $username))->row();
                $code = rand(10000,10000000);
                $authen = array(
                    'member_id' => $wp_user->ID,
                    'user_id' => $wp_user->ID,
                    'display_name'  => $wp_user->display_name,
                    'email'      => $wp_user->user_email,
                    'activation'  => $code,
                    'username'  =>  $wp_user->user_login,
                    'group_id'    =>  1,
                    'password'    =>  $wp_user->user_pass ,
                    'active'    =>  1 ,
                    'permissions' => array(),
                    'site_theme' => $default_user_theme,
                  );
                $this->db->insert($this->table,$authen);
            }
            else
            {
                $authen = array(
                    'user_id' =>$username,
                    'password'    =>  $password ,
                    'active'    =>  1 ,
                    'permissions' => array(),
                    'site_theme' =>  $default_user_theme ,
                  );
                $this->db->insert($this->table,$authen);
            }
        }
    }

    function login_user_id() {
        return ci_get_current_user_id();
    }


    function login_user() {
        $login_user = get_instance()->session->userdata('login_user');
        return $login_user;
       // return $login_user_id ? $login_user_id : false;
    }

    function get_details($options = array()) {
        //die('sample');
        $users_table = $this->db->dbprefix('sfm_uap_affiliates');
        $team_member_job_info_table = $this->db->dbprefix('team_member_job_info');

        $where = "";
        $id = get_array_value($options, "id");
        /*$user_type = get_array_value($options, "user_type");*/
        $client_id = get_array_value($options, "client_id");
        $exclude_user_id = get_array_value($options, "exclude_user_id");

        if ($id) {
            $where .= " AND $users_table.user_id=$id";
        }
/*
        if ($user_type) {
            $where .= " AND $users_table.user_type='$user_type'";
        }*/

        if ($client_id) {
            //$where .= " AND $users_table.client_id=$client_id";
        }

        if ($exclude_user_id) {
            $where .= " AND $users_table.id!=$exclude_user_id";
        }


        // $custom_field_type = "team_members";
        // /*if ($user_type === "client") {
        //     $custom_field_type = "contacts";
        // }*/

        // //prepare custom fild binding query
        // $custom_fields = get_array_value($options, "custom_fields");
        // $custom_field_query_info = $this->prepare_custom_field_query_string($custom_field_type, $custom_fields, $users_table);
        // $select_custom_fieds = get_array_value($custom_field_query_info, "select_string");
        // $join_custom_fieds = get_array_value($custom_field_query_info, "join_string");


        //prepare full query string
        $sql = "SELECT $users_table.*,
            $team_member_job_info_table.date_of_hire, $team_member_job_info_table.salary, $team_member_job_info_table.salary_term
        FROM $users_table
        LEFT JOIN $team_member_job_info_table ON $team_member_job_info_table.user_id=$users_table.user_id
        WHERE 1=1 $where
        ORDER BY $users_table.display_name";

        return $this->db->query($sql);
    }

    function is_email_exists($email, $id = 0) {
        $result = $this->get_all_where(array("email" => $email, "deleted" => 0));
        if ($result->num_rows() && $result->row()->id != $id) {
            return $result->row();
        } else {
            return false;
        }
    }

    function get_job_info($user_id) {
        parent::use_table("team_member_job_info");
        return parent::get_one_where(array("user_id" => $user_id));
    }

    function save_job_info($data) {
        parent::use_table("team_member_job_info");

        //check if job info already exists
        $where = array("user_id" => get_array_value($data, "user_id"));
        $exists = parent::get_one_where($where);
        if ($exists->user_id) {
            //job info found. update the record
            return parent::update_where($data, $where);
        } else {
            //insert new one
            return parent::save($data);
        }
    }

    function savenotification($data)
    {
        parent::use_table("sfm_uap_affiliates");

        //check if job info already exists
        $where = array("user_id" => get_array_value($data, "user_id"));
        $exists = parent::get_one_where($where);
        if ($exists->user_id) {
            //job info found. update the record
            return parent::update_where($data, $where);
        } else {
            //insert new one
            return parent::save($data);
        }

    }

    function get_team_members($member_ids = "") {
        $users_table = $this->db->dbprefix($this->table);
        $sql = "SELECT $users_table.*
        FROM $users_table
        WHERE $users_table.deleted=0 AND FIND_IN_SET($users_table.id, '$member_ids')
        ORDER BY $users_table.first_name";
        return $this->db->query($sql);
    }

    function get_access_info($user_id = 0) {

        if (!$user_id)
            $user_id = ci_get_current_user_id();

        $sql = "SELECT sfm_uap_affiliates.*,'all' as permissions, sfm_uap_affiliates.user_id  as client_id from sfm_uap_affiliates where user_id=".$user_id;
        $row =  $this->db->query($sql)->row();
        return $row;


        $roles_table = $this->db->dbprefix('roles');
        $team_table = $this->db->dbprefix('team');

        $sql = "SELECT $users_table.id, $users_table.user_type, $users_table.is_admin, $users_table.role_id, $users_table.email,
            $users_table.first_name, $users_table.last_name, $users_table.image, $users_table.message_checked_at, $users_table.notification_checked_at, $users_table.client_id,
            $users_table.is_primary_contact, $users_table.sticky_note,
            $roles_table.title as role_title, $roles_table.permissions,
            (SELECT GROUP_CONCAT(id) team_ids FROM $team_table WHERE FIND_IN_SET('$user_id', `members`)) as team_ids
        FROM $users_table
        LEFT JOIN $roles_table ON $roles_table.id = $users_table.role_id AND $roles_table.deleted = 0
        WHERE $users_table.deleted=0 AND $users_table.id=$user_id";
        return $this->db->query($sql)->row();
    }

    function get_team_members_and_clients($user_type = "", $user_ids, $exlclude_user = 0) {
        $users_table = $this->db->dbprefix($this->table);
        $clients_table = $this->db->dbprefix('clients');


        $where = "";
        if ($user_type) {
            $where.= " AND sfm_uap_affiliates.user_type='$user_type'";
        }


        if ($user_ids) {
            $ids= '';
            foreach ($user_ids as $user_id)
            {
                foreach ($user_id as $userid)
                {
                    if (!empty($ids))
                        $ids .= ',' ;
                    $ids .= $userid;
                }
            }

            $where.= "  AND FIND_IN_SET(sfm_uap_affiliates.user_id, '$ids')";
        }

        if ($exlclude_user) {
            $where.= " AND sfm_uap_affiliates.user_id !=$exlclude_user";
        }

        $sql = "SELECT
    sfm_uap_affiliates.id,
    sfm_uap_affiliates.user_id,
    sfm_uap_affiliates.rank,
    sfm_uap_affiliates.firstname,
    sfm_uap_affiliates.firstname as first_name,
    sfm_uap_affiliates.lastname,
    sfm_uap_affiliates.lastname as last_name,
    sfm_uap_affiliates.company ,
    sfm_uap_affiliates.company as company_name ,
    sfm_uap_affiliates.user_type
    FROM
    sfm_uap_affiliates
    WHERE 1=1  $where
    ORDER BY
    sfm_uap_affiliates.user_id ASC";


    return $this->db->query($sql);


    }

    /* return comma separated list of user names */

    function user_group_names($user_ids = "") {
        $users_table = $this->db->dbprefix($this->table);

        $sql = "SELECT GROUP_CONCAT(' ', $users_table.first_name, ' ', $users_table.last_name) AS user_group_name
        FROM $users_table
        WHERE FIND_IN_SET($users_table.id, '$user_ids')";
        return $this->db->query($sql)->row();
    }

    function get_user_data($user_id = "") {

        if (empty($user_id))
            return false;

        $sql = "SELECT *
        FROM sfm_uap_affiliates
        WHERE user_id = '$user_id';";
        return $this->db->query($sql)->row();
    }




    function get_downlines($options = array()) {
        //die('sample');
        $where = "";

        $id = get_array_value($options, "id");
        $status = get_array_value($options, "status");

        if (!$id) {
            $id =  $this->login_user_id();
        }


        $where .= " AND sfm_uap_affiliates.sponsorkey=$id ";


        if ($status == 'paid') {
            $where .= " AND sfm_uap_affiliates.paid=1 ";
        }
        elseif ($status == 'unpaid') {
            $where .= " AND (sfm_uap_affiliates.paid is null or sfm_uap_affiliates.paid = 0)  ";
        }

        //prepare full query string
        $sql = "SELECT
        sfm_uap_affiliates.user_id,
        sfm_uap_affiliates.username,
        sfm_uap_affiliates.display_name,
        sfm_uap_affiliates.rank,
        sfm_uap_affiliates.email,
        sfm_uap_affiliates.`status`,
        sfm_uap_affiliates.date_registered,
        sfm_uap_affiliates.paid,
        sfm_uap_affiliates.sponsorkey,
        sfm_uap_affiliates.avatar ,
        sfm_uap_affiliates.avatar as image,
        sfm_uap_affiliates.user_type
        FROM
        sfm_uap_affiliates
         WHERE 1=1 $where
        ORDER BY sfm_uap_affiliates.date_registered";

        return $this->db->query($sql);
    }

    function get_downlines_user_ids($options = array()) {
        //die('sample');

        if (empty($options))
        {
            $options = array(
                'id' =>  $this->login_user_id()
                );
        }

        $where = "";
        $id = get_array_value($options, "id");
        $status = get_array_value($options, "status");

        if (!$id) {
            $id = $this->login_user_id();
        }


        $where .= " AND sfm_uap_affiliates.sponsorkey=$id ";


        if ($status == 'paid') {
            $where .= " AND sfm_uap_affiliates.paid=1 ";
        }
        elseif ($status == 'unpaid') {
            $where .= " AND (sfm_uap_affiliates.paid is null or sfm_uap_affiliates.paid = 0)  ";
        }

        //prepare full query string
        $sql = "SELECT
        sfm_uap_affiliates.user_id
        FROM
        sfm_uap_affiliates
         WHERE 1=1 $where
        ORDER BY sfm_uap_affiliates.date_registered";

        return $this->db->query($sql)->row_array();
    }

        function get_downlines_ids($options = array()) {
        //die('sample');

        if (empty($options))
        {
            $options = array(
                'id' =>  $this->login_user_id()
                );
        }

        $where = "";
        $id = get_array_value($options, "id");
        $status = get_array_value($options, "status");

        if (!$id) {
            $id = $this->login_user_id();
        }

        $where .= " AND sfm_uap_affiliates.sponsorkey=$id ";


        if ($status == 'paid') {
            $where .= " AND sfm_uap_affiliates.paid=1 ";
        }
        elseif ($status == 'unpaid') {
            $where .= " AND (sfm_uap_affiliates.paid is null or sfm_uap_affiliates.paid = 0)  ";
        }

        //prepare full query string
        $sql = "SELECT
        user_id
        FROM
        sfm_uap_affiliates
         WHERE 1=1 $where
        ORDER BY sfm_uap_affiliates.date_registered";

        return $this->db->query($sql)->result_array();
    }

    public function get_next_rank_sales()
    {
        $ret =0;
        return $ret;
    }

    function generate_downline_user_data($options = array()){
         //   return;
        echo ('Downline json generated!');
        if (empty($options))
        {
            $options = array('id' =>  'user_data');
        }

        $id = get_array_value($options, "id");

        if ($id != 'user_data') {
            $where = " AND sponsorkey=$id ";
            $md5 = md5($id);
        }
        else {
            $where = "";
            $md5 = md5('user_data');
        }

        $sql = "SELECT
        sfm_uap_affiliates.id,
        sfm_uap_affiliates.firstname,
        sfm_uap_affiliates.lastname,
        sfm_uap_affiliates.sponsorkey,
        sfm_uap_affiliates.email,
        sfm_uap_affiliates.rank,
        sfm_uap_affiliates.username,
        sfm_uap_affiliates.downlines
        FROM sfm_uap_affiliates
        WHERE 1=1 $where
        ORDER BY sfm_uap_affiliates.date_registered";

        $user_data = $this->db->query($sql)->result_array();

        //$user_data = $this->Users_model->generate_downline_user_data();
        $response = array();
        $posts = array();
        $counter = 0;
        foreach ($user_data as $user)
        {
            $user_id = $user['id'];
            $name = $user['firstname']." ".$user['lastname'];
            $email = $user['email'];
            $md5email = md5(strtolower(trim($email)));
            $sponsorkey = $user['sponsorkey'];
            $rank = $user['rank'];
            $username = $user['username'];
            $downlines = $user['downlines'];

            array_push($posts,array('name' => $name,'email' => $email, 'md5email' => $md5email, 'sponsorkey' => $sponsorkey, 'id' =>$user_id, 'rank' => $rank, 'username' => $username, 'downlines' => $downlines));
        }

        $json = json_encode($posts,JSON_PRETTY_PRINT);
        $fp = fopen('./json/'.$md5 .'.json', 'w');
        fwrite($fp, $json);

        $message = 'Data generated successfully';
        $message = $md5;

        return $message;
    }

    function user_get($user_id, $key)
    {
        // check if user is staff
        $this->db->where('user_id', $user_id);
        $this->db->where('key_id', $key);
        $user = $this->db->get('main_crm.crm_userautologin')->row();
        if (!$user) {
            return NULL;
        }
        if ($user->staff == 1) {
            $table = 'main_shopfreemart.sfm_crm_staff';
            $this->db->select($table . '.staffid as id');
            $_id   = 'staffid';
            $staff = true;
        } else {
            $table = 'sfm_uap_affiliates';
            $this->db->select($table . '.id as id');
            $_id   = 'id';
            $staff = false;
        }
        $this->db->select($table . '.' . $_id);
        $this->db->from($table);
        $this->db->join('main_crm.crm_userautologin', 'main_crm.crm_userautologin.user_id = ' . $table . '.' . $_id);
        $this->db->where('main_crm.crm_userautologin.user_id', $user_id);
        $this->db->where('main_crm.crm_userautologin.key_id', $key);
        $query = $this->db->get();
        if ($query) {
            if ($query->num_rows() == 1) {
                $user        = $query->row();
                $user->staff = $staff;
                return $user;
            }
        }
        return NULL;
    }
    /**
     * Set new autologin if user have clicked remember me
     * @param mixed $user_id clientid/user_id
     * @param string $key     cookie key
     * @param integer $staff   is staff or client
     */
    function user_set($user_id, $key, $staff)
    {
        return $this->db->insert('main_crm.crm_userautologin', array(
            'user_id' => $user_id,
            'key_id' => $key,
            'user_agent' => substr($this->input->user_agent(), 0, 149),
            'last_ip' => $this->input->ip_address(),
            'staff' => $staff
        ));
    }
    /**
     * Delete user autologin
     * @param  mixed $user_id clientid/user_id
     * @param  string $key     cookie key
     * @param integer $staff   is staff or client
     */
    function user_delete($user_id, $key, $staff)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('key_id', $key);
        $this->db->where('staff', $staff);
        $this->db->delete('main_crm.crm_userautologin');
    }



    function user_gift($id){
        $sql = "SELECT COUNT(*) as count FROM `sfm_gift_certificates` WHERE user_id = $id AND status = 'Available'";
        $user_data = $this->db->query($sql)->result();
        return $user_data[0];
    }

    function user_gift_completed($id){
        $sql = "SELECT * FROM `sfm_gift_certificates` WHERE user_id = $id AND status = 'Completed' ";
        $result = $this->db->query($sql)->result();
        return $result;
    }

}
