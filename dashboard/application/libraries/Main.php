<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

    //put this on wp-db.php line 1986
        //$table_parts = explode( '.', $table );
        //$table = '`' . implode( '`.`', $table_parts ) . '`';
        //$sql = "$type INTO $table ($fields) VALUES ($formats)";
class Main
{

    public $wp_users = 'main_freemart.wp_users';
    public $wp_usermeta = 'main_freemart.wp_usermeta';

    public $wp_postmeta = 'main_freemart.wp_postmeta';
    public $wp_posts = 'main_freemart.wp_posts';


    public $wp_trans_postmeta = 'main_freemart.wp_trans_postmeta';
    public $wp_trans_posts = 'main_freemart.wp_trans_posts';

    function __construct() {
    }

    function process_payments($order_id)
    {
//ld($order_id);
        global $wpdb;
        //$order_id = 700583;
        $commissions_used = $wpdb->get_var("SELECT IFNULL(SUM(sm.meta_value),0) as total FROM sfm_woocommerce_order_items s
                INNER JOIN sfm_woocommerce_order_itemmeta sm on (s.order_item_id = sm.order_item_id AND sm.meta_key = '_line_total')
                WHERE s.order_id = $order_id AND s.order_item_name = 'Commissions'");


        if ($commissions_used != 0) {
            $hascom = $wpdb->get_var("Select count(0) from sfm_uap_transactions where order_id = $order_id and sfm_uap_transactions.status_id = 2 and deleted = 0 and sfm_uap_transactions.type_id = 301;");

            $user_id = get_post_meta( $order_id, '_customer_user', true );
            if($hascom == 0)
            {
                $this->refresh_user($user_id);
                $this->create_commissionpayment_post($user_id, $commissions_used, $order_id );
                $this->refresh_user($user_id);
            }
        }


        $coins_used = $wpdb->get_var("SELECT IFNULL(SUM(sm.meta_value),0) as total FROM sfm_woocommerce_order_items s
                INNER JOIN sfm_woocommerce_order_itemmeta sm on (s.order_item_id = sm.order_item_id AND sm.meta_key = '_line_total')
                WHERE s.order_id = $order_id AND s.order_item_name = 'Coins'");

        if ($coins_used != 0) {
            $hascoin = $wpdb->get_var("Select count(0) from sfm_uap_transactions where order_id = $order_id and sfm_uap_transactions.status_id = 2 and deleted = 0 and sfm_uap_transactions.type_id = 302;");

            $user_id = get_post_meta( $order_id, '_customer_user', true );
            if($hascoin == 0)
            {
                $this->refresh_user($user_id);
                $this->create_coinpayment_post($user_id, $coins_used, $order_id );
                $this->refresh_user($user_id);
            }
        }
    }

    function refresh_user($user_id = 0)
    {
        if($user_id == 0)
        {
            return;
        }

        if(empty($user_id))
        {
            return;
        }

        global $wpdb;
        $wpdb->get_results("CALL update_user_info($user_id)");
    }

    function process_newcommissions($order)
    {
//ld($order);
        global $wpdb;
        global $woocommerce;

        $order_id = $order->id;

        $hascom   = $wpdb->get_var("Select count(0) from sfm_uap_transactions where order_id = $order_id and sfm_uap_transactions.status_id in (1, 2,3) and deleted = 0 ;");

        if($hascom != 0)
        {
             update_post_meta($order_id, 'commissionscreated', '1', true );
             return;
        }

        $commissionscreated = get_post_meta( $order_id, 'commissionscreated', true );

        if($commissionscreated == 1)
        return;

//ld($hascom);
        global $wpdb;
        $wpdb->query('SET OPTION SQL_BIG_SELECTS = 1');

        $meta          = get_post_meta( $order_id );
        //$total_bonus_value = get_post_meta( $order_id, 'total_bonus_value', true );
        $customer_user = get_post_meta( $order_id, '_customer_user', true );


        $customer_user_info = new WP_User( $customer_user );
        $customer_name      = $customer_user_info->first_name . " " . $customer_user_info->last_name;
        $customer_role      = $customer_user_info->roles[0];
        $first_purchase     = $customer_user_info->first_purchase;
        $first_purchase_date= $customer_user_info->first_purchase_date;

        $total_sales        = $customer_user_info->total_sales;

        if($total_sales == 0 || $total_sales == "")
        $total_sales   = $this->get_totalsales($customer_user);

        $above_sponsor = $wpdb->get_var('Select sponsorkey from sfm_uap_affiliates where user_id='.$customer_user);

        if(!$above_sponsor) {
            return;
        }

        if($above_sponsor_sale == 0 || $above_sponsor_sale == "")
        $above_sponsor_sale         = $this->get_totalsales($above_sponsor);


        $coded_bonus                = ( 0.10 * $total_bonus_value );
        $bonus_given                = 0;
        $unilevel_bonus             = 0;

        $qualified                  = 0;
        $diamond                    = 0;
        $double_diamond             = 0;
        $triple_diamond             = 0;
        $ambassador                 = 0;

        $unilevel_info              = new WP_User(1); //Get admin info for saving extra bonus value to unilevel_total
        $unilevel_total             = $unilevel_info->unilevel_total;

        $sponsor_counter            = 0;

        global $WC;

        //Check if Founding Member has been purchased
        $order                      = new WC_Order($order_id);
        $items                      = $order->get_items();
//ld($items);
        $order_date                 = $order->get_date_created();
        $product_counter            = 0;
        $founding_member            = 0;
        $founding_price             = 0;
        $founding_quantity          = 0;
        $personal_assistant         = 0;
        $personal_price             = 0;
        $personal_quantity          = 0;
        $personal_assistant_counter = 0;
        $special_counter            = 0;
        $regular_counter            = 0;
        $donationpurchase           = 0;
        $donationpurchase_total     = 0;
        $total_bonus_value          = 0;


        foreach($items as $item => $values)
        {
            $_product    = $values['data']->post;
         //   echo " < b > ".$_product->post_title.'</b > < br > Quantity: '.$values['qty'].' < br > ';

            $bonus_value = get_post_meta($values['product_id'] , '_bonus_value', true);
            //$bonus_value = get_post_meta($values['product_id'] , '_bonus_value', true);
           // echo "  Bonus Value: ".$bonus_value." < br > ";
            $item_quantity          = $values['qty'];
            $total_item_bonus_value = ( $bonus_value * $item_quantity );
           // echo " Total Item Bonus Value: ".$total_item_bonus_value." < br > ";
            $total_bonus_value      = ( $total_bonus_value + $total_item_bonus_value );
        }
        $coded_bonus = ( 0.10 * $total_bonus_value );


         //echo "  Total Bonus Value: ".$total_bonus_value." < br > ";

         //echo "  Coded  Bonus Value (10 % ): ".$coded_bonus." < br > ";
         //die;
        update_post_meta( $order_id, 'total_bonus_value', $total_bonus_value  );
				update_post_meta( $order_id, 'coded_bonus_value', $coded_bonus );

//ld($items);
        //Check each product
        foreach( $items as $item )
        {

            $product_name         = $item['name'];
            $product_id           = $item['echo'];
            $product_id           = $item['product_id'];
            $product_variation_id = $item['variation_id'];
            $product_total        = $item['line_total'];
            $product_quantity     = $item['qty'];

            if($product_variation_id == 0){
                $product_price = get_post_meta($product_id , '_regular_price', true);
            }
            else
            {
                $product_price = get_post_meta($product_variation_id , '_regular_price', true);
            }

            if($product_id == 700071){
                $founding_member   = 1;
                $founding_price    = $founding_price + $product_price;
                $founding_total    = $founding_total + $product_total;
                $founding_quantity = $founding_quantity + $product_quantity;
                $special_counter++;
            }
            elseif($product_id == 626){
                $founding_member   = 1;
                $founding_price    = $founding_price + $product_price;
                $founding_total    = $founding_total + $product_total;
                $founding_quantity = $founding_quantity + $product_quantity;
                $special_counter++;
            }
            elseif($product_id == 643){
                $personal_assistant = 1;
                $personal_price     = $personal_price + $product_price;
                $personal_total     = $personal_total + $product_total;
                $personal_quantity  = $personal_quantity + $product_quantity;
                $special_counter++;
            }
            elseif($product_id == 898 ){
                $special_counter++;
            }
            elseif($product_id == 362924 ){
                $special_counter++;
            }
            elseif($product_id == 29141 ){
                $donationpurchase       = 1;
                $donationpurchase_total = $donationpurchase_total + $product_total;

                $special_counter++;
            }
            else
            {
                $regular_counter++;
            }
        }
        if( $order->payment_method == "wcCpg2" ){
            $new_customer_commission = $customer_user_info->commission_pay - $order->get_total();
            update_user_meta( $customer_user, 'commission_pay', $new_customer_commission );
        }
//ld($hascom);
        //If Founding Member is purchased update sponsor info
        if($founding_member == 1)
        {

            $fs_info                       = new WP_User( $customer_user ); //Get Customer Info
            $fs_current_balance            = $fs_info->founder_balance; // Get Current Founding Balance
            $fs_sponsor                    = $fs_info->referral_id; //Get Sponsor ID
            $fs_name                       = $fs_info->first_name . " " .$fs_info->last_name;

            $fs_price                      = ( $founding_total / $founding_quantity );
            $fs_total_balance              = $fs_current_balance + $founding_total; //Total Founding Balance
            $fs_rank                       = floor( $fs_total_balance / 1000 ); //Founding Rank or Level


            $fs_sponsor_info               = new WP_User( $fs_sponsor ); //Get Sponsor Info
            $fs_sponsor_current_commission = $fs_sponsor_info->commission_pay; // Get Current Commission
            $fs_commission                 = ($founding_total * 0.10); //Get 10 % Value on Founding Membership
            $fs_bonus_pay                  = $fs_sponsor_current_commission + $fs_commission; //Total Commission
            //$fs_bonus_coins = $fs_current_coins + $fs_commission_coins; //Total Coins


            // //Update Customer's Info
            // update_user_meta( $customer_user, 'founder', 1 );
            // update_user_meta( $customer_user, 'founder_rank', $fs_rank );
            // update_user_meta( $customer_user, 'founder_balance', $fs_total_balance );
            // update_post_meta( $customer_user, 'foundingmembership_balance', $fs_total_balance );

            // //Create founding position
            // for ($i = 1; $i <= $founding_quantity; $i++) {
            //  $fs_position = "founding_" . $i ;
            //  $fs_position_payment = $fs_info->$fs_position + $fs_price;
            //  update_user_meta( $customer_user, $fs_position, $fs_position_payment );
            // }

            //Update Sponsor's Commission
            update_user_meta( $fs_sponsor, 'commission_pay', $fs_bonus_pay );
            //update_user_meta( $fs_sponsor, 'commission_coins', $fs_bonus_coins );

            //Update FoundingBalanceToDone

            $this->pay_commission($customer_user,$fs_sponsor, $founding_total,$order_id,"Founder Purchase");

            if($first_purchase == null)
            {
                update_user_meta( $customer_user, 'first_purchase_date', $order_date);  //    date('Y - m - d') );
                update_user_meta( $customer_user, 'first_purchase', 1 );
            }

        }

        if($personal_assistant == 1)
        {
            $pa_info                       = new WP_User( $customer_user ); //Get Customer Info
            $pa_sponsor                    = $pa_info->referral_id; //Get Sponsor ID
            $pa_current_sales_assistant    = $pa_info->sales_assistant;
            $pa_current_sales_calls        = $pa_info->sales_calls;
            $pa_name                       = $pa_info->first_name . " " .$pa_info->last_name;

            $pa_total_calls                = 1000 * $personal_quantity;
            $pa_total_sales_assistant      = $pa_current_sales_assistant + $personal_quantity;
            $pa_total_sales_calls          = $pa_current_sales_calls + $pa_total_calls;

            $pa_sponsor_info               = new WP_User( $pa_sponsor ); //Get Sponsor Info
            $pa_sponsor_current_commission = $pa_sponsor_info->commission_pay; // Get Current Commission
            $pa_commission                 = $personal_total * 0.10; //Get 10 % Value on Personal Sales Assistant Total Price
            $pa_bonus_pay                  = $pa_sponsor_current_commission + $pa_commission; //Total Commission


            //Update Customer's Info
            update_user_meta( $customer_user, 'sales_assistant_status', 1 );
            update_user_meta( $customer_user, 'sales_assistant', $pa_total_sales_assistant );
            update_user_meta( $customer_user, 'sales_calls', $pa_total_sales_calls );

            //Update Sponsor's Commission
            update_user_meta( $pa_sponsor, 'commission_pay', $pa_bonus_pay );

            //$this->create_commission_post( $pa_name, $customer_user, $pa_sponsor, $total_bonus_value, $pa_commission, $order_id,"PSA Purchase");

            $this->pay_commission($customer_user,$pa_sponsor, $personal_total,$order_id,"PSA Purchase");

            if($first_purchase == null)
            {
                update_user_meta( $customer_user, 'first_purchase_date',  $order_date);  //    date('Y - m - d') );
                update_user_meta( $customer_user, 'first_purchase', 1 );
            }

        }


        if($donationpurchase == 1)
        {

            $customer_user_info = new WP_User( $customer_user );
            $customer_name      = $customer_user_info->first_name . " " . $customer_user_info->last_name;

            $this->create_coins_post( $customer_name, $customer_user_info, $customer_user, $donationpurchase_total, $donationpurchase_total, $order_id,"Coin Purchase");

            if($first_purchase == null)
            {
                update_user_meta( $customer_user, 'first_purchase_date',  $order_date);  //    date('Y - m - d') );
                update_user_meta( $customer_user, 'first_purchase', 1 );
            }

        }

//ld($regular_counter);
        //If More Than Founding Member or Personal Sales Product is Purchased else Do Nothing.
        if( $regular_counter >= 1 )
        {
//ld($hascom);
            //Check Unilevel Total
            if($unilevel_total == ""){
                $unilevel_total = 0;
            }

            //Check Sponsor
            if($above_sponsor == ""){
                $above_sponsor == 0;
                update_user_meta( $customer_user, 'no_referrer', 'Main Sponsor' );
            }

            //Check Sponsor Sales
            if($above_sponsor_sale == ""){
                $above_sponsor_sale = 1;
                //update_user_meta( $above_sponsor, 'total_sales', $above_sponsor_sale );
            }
            else
            {
                $above_sponsor_sale = $this->get_totalsales($above_sponsor) + 1;
                //update_user_meta( $above_sponsor, 'total_sales', $above_sponsor_sale );
            }

            //Upgrading Sponsor Rank for main sponsor.
            $main_sponsor_id = get_user_meta( $customer_user, 'referral_id', true );
            $main_sponsor_info = new WP_User( $main_sponsor_id );
            $main_sponsor_type = $main_sponsor_info->roles[0];


            if($main_sponsor_type == "free"){
                $new_user_role = "qualified";
                $user_id       = wp_update_user( array('ID'  => $main_sponsor_id,'role'=> $new_user_role ) );

                if( is_wp_error( $user_id ) ){
                    // There was an error, probably that user doesn't exist.
                }
                else
                {
                    // Success!
                }
            }

            //Customer First Time Purchased

            $IsComplan = 0;
            if($first_purchase == null)
            {
                $IsComplan      = 1;
                $unilevel_bonus = $total_bonus_value - $bonus_given;
                $unilevel_total = $unilevel_total + $unilevel_bonus;
                update_user_meta(1, 'unilevel_total', $unilevel_total);
                //update_user_meta( $customer_user, 'first_purchase_date',      date('Y - m - d') );
                update_user_meta( $customer_user, 'first_purchase_date',   $order_date);  //    date('Y - m - d') );
                update_user_meta( $customer_user, 'first_purchase', 1 );

            }
            elseif($first_purchase_date != "" && $first_purchase == 1)
            {
                $current = date('Y-m-d');
                $diff    = date_diff(date_create($first_purchase_date),date_create($current));
                $diff    = $diff->format('%a');
                if($diff <= 45)
                $IsComplan = 1;
            }

            //REMOVE
            $IsComplan = 1;
            // echo " < pre > first_purchase = ".$first_purchase;
            // echo ",first_purchase_date = ".$first_purchase_date;
            // echo ",current_date = ".$current;
            // echo ",Diff = ".$diff;
            // echo ",IsComplan = ".$IsComplan."</pre > ";
            //die();

            if($IsComplan == 1)
            {


                if( $above_sponsor != 0)
                {


                    if($above_sponsor != 0)
                    {
                        //$first_purchase_main_sponsor_rank = get_user_meta($customer_user,'first_purchase_main_sponsor_rank',true);
                        //if($first_purchase_main_sponsor_rank == null)
                        //{
                            //$first_purchase_main_sponsor_rank = $this->get_rolename($above_sponsor);
                       //     update_user_meta( $customer_user, 'first_purchase_main_sponsor_rank', $first_purchase_main_sponsor_rank );
                       // }
                        $this->pay_commission($customer_user,$above_sponsor, $total_bonus_value,$order_id,"Sponsor Bonus");
                        $sql = "CALL main_shopfreemart.update_user_info($customer_user);";
                        $wpdb->get_var($sql);

                    }


                    //Start Jeff Code
                    // for the main sponsor

                    //for diamond
                                        $diamond_sponsor = $wpdb->get_var('Select upline_diamond from sfm_uap_affiliates where user_id = '.$customer_user);
                    //$diamond_sponsor = get_user_meta($customer_user,'first_purchase_diamond_sponsor',true);
                    //if($diamond_sponsor == null)
                    //{
                    //    $diamond_sponsor = $this->get_diamond($customer_user);
                    //    update_user_meta( $customer_user, 'first_purchase_diamond_sponsor', $diamond_sponsor );
                    //}

                    //for doublediamond
                                        $doublediamond_sponsor = $wpdb->get_var('Select upline_doublediamond from sfm_uap_affiliates where user_id = '.$customer_user);
                    //$doublediamond_sponsor = get_user_meta($customer_user,'first_purchase_doublediamond_sponsor',true);
                    //if($doublediamond_sponsor == null)
                    //{
                    //    $doublediamond_sponsor = $this->get_doublediamond($customer_user);
                    //    update_user_meta( $customer_user, 'first_purchase_doublediamond_sponsor', $doublediamond_sponsor);
                    //}

                    //for triplediamond
                                        $triplediamond_sponsor = $wpdb->get_var('Select upline_triplediamond from sfm_uap_affiliates where user_id = '.$customer_user);
                    //$triplediamond_sponsor = get_user_meta($customer_user,'first_purchase_triplediamond_sponsor',true);
                    //if($triplediamond_sponsor == null)
                    //{
                    //    $triplediamond_sponsor = $this->get_triplediamond($customer_user);
                    //    update_user_meta( $customer_user, 'first_purchase_triplediamond_sponsor', $triplediamond_sponsor);
                    //}

                    //for ambassador
                                        $ambassador_sponsor = $wpdb->get_var('Select upline_ambassador from sfm_uap_affiliates where user_id = '.$customer_user);
                    //$ambassador_sponsor = get_user_meta($customer_user,'first_purchase_ambassador_sponsor',true);
                    //if($ambassador_sponsor == null)
                    //{
                    //    $ambassador_sponsor = $this->get_ambassador($customer_user);
                    //    update_user_meta( $customer_user, 'first_purchase_ambassador_sponsor', $ambassador_sponsor);
                    //}


                    if($above_sponsor != 0)
                    {
                        //$first_purchase_main_sponsor_rank = get_user_meta($customer_user,'first_purchase_main_sponsor_rank',true);
                        //if($first_purchase_main_sponsor_rank == null)
                        //{
                            //$first_purchase_main_sponsor_rank = $this->get_rolename($above_sponsor);
                       //     update_user_meta( $customer_user, 'first_purchase_main_sponsor_rank', $first_purchase_main_sponsor_rank );
                       // }
                        $this->pay_commission($customer_user,$above_sponsor, $total_bonus_value,$order_id,"Sponsor Bonus");
                    }

                    // DO NOT GIVE COMMISSION ON NEWLY CHANGED ROLE
                    //if ($diamond_sponsor == $a_sponsor_id && $role_updated == 1)
                    //  $diamond_sponsor = $doublediamond_sponsor;

                    // if ($doublediamond_sponsor == $a_sponsor_id && $role_updated == 1)
                    //  $doublediamond_sponsor = $doublediamond_sponsor;

                    // if ($triplediamond_sponsor == $a_sponsor_id && $role_updated == 1)
                    //  $triplediamond_sponsor = "";

                    // if ($ambassador_sponsor == $a_sponsor_id && $role_updated == 1)
                    // {
                    //  $ambassador_sponsor = $this->get_ambassador($ambassador_sponsor);
                    // }

                    // if ($triplediamond_sponsor == "")
                    //  $triplediamond_sponsor = $ambassador_sponsor;

                    // if ($doublediamond_sponsor == "")
                    //  $doublediamond_sponsor = $triplediamond_sponsor;

                    // if ($diamond_sponsor == "")
                    //  $diamond_sponsor = $doublediamond_sponsor;
                    // END of condition
                    // Do not process commissions for founding additional payment

//ld($hascom);
                    $isfoundingadditional = get_post_meta($order_id,"foundingpostid",true);
                    if(!$isfoundingadditional)
                    {
                        if($diamond_sponsor != "")
                        $this->pay_commission($customer_user,$diamond_sponsor, $total_bonus_value,$order_id,"Diamond Bonus","diamond");

                        if($doublediamond_sponsor != "")
                        $this->pay_commission($customer_user,$doublediamond_sponsor, $total_bonus_value,$order_id,"Double Diamond Bonus","double_diamond");

                        if($triplediamond_sponsor != "")
                        $this->pay_commission($customer_user,$triplediamond_sponsor, $total_bonus_value,$order_id,"Triple Diamond Bonus","triple_diamond");

                        if($ambassador_sponsor != "")
                        $this->pay_commission($customer_user,$ambassador_sponsor, $total_bonus_value,$order_id,"Ambassador Bonus","ambassador");
                    }
                }
            }
            else
            {
                if( $above_sponsor != 0 )
                {
                    $total_unilevel_given = 0;
                    $i                    = 1;

                    while( ($i <= 9) && ($above_sponsor != 0) )
                    {
                        $unilevel_sponsor            =new WP_User( $above_sponsor );
                        $unilevel_sponsor_type       = $unilevel_sponsor->roles[0];
                        $unilevel_sponsor_commission = $unilevel_sponsor->commission_pay;
                        $unilevel_sponsor_coins      = $unilevel_sponsor->commission_coins;

                        if($i == 1)
                        {
                            $unilevel_level       = $total_bonus_value * 0.01; // Compute Lvl 1 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 2)
                        {
                            $unilevel_level       = $total_bonus_value * 0.01; // Compute Lvl 2 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 3){
                            $unilevel_level       = $total_bonus_value * 0.02; // Compute Lvl 3 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 4){
                            $unilevel_level       = $total_bonus_value * 0.03; // Compute Lvl 4 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 5){
                            $unilevel_level       = $total_bonus_value * 0.05; // Compute Lvl 5 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 6){
                            $unilevel_level       = $total_bonus_value * 0.08; // Compute Lvl 6 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 7){
                            $unilevel_level       = $total_bonus_value * 0.13; // Compute Lvl 7 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 8)
                        {
                            $unilevel_level       = $total_bonus_value * 0.21; // Compute Lvl 8 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        elseif($i == 9)
                        {
                            $unilevel_level       = $total_bonus_value * 0.34; // Compute Lvl 9 unilevel Commission
                            $bonus_unilevel_value = $unilevel_sponsor_commission + $unilevel_level; // Adds unilevel commission to current total commission
                            update_user_meta($above_sponsor, 'commission_pay', $bonus_unilevel_value); // Update total commission of sponsor based on the which level they are

                            $total_unilevel_given = $total_unilevel_given + $unilevel_level; // Add the previous unilevel commission to the total unilevel commission given until all levels are added
                        }
                        $this->create_commission_post( $customer_name, $customer_user, $above_sponsor, $total_bonus_value, $unilevel_level, $order_id, "Unilevel ".$i );
                        $above_sponsor = $unilevel_sponsor->referral_id;
                        if($above_sponsor == "" || $above_sponsor == 0 )
                        {
                            $above_sponsor = 0;
                        }

                        $i++;
                    }

                    $global_pool       = $total_bonus_value * 0.01; // Get 1 % of BV as Global Pool
                    $crown_pool        = $total_bonus_value * 0.01; // Get 1 % of BV as Crown Pool

                    $total_global_pool = $unilevel_info->global_pool + $global_pool;
                    $total_crown_pool  = $unilevel_info->crown_pool + $crown_pool;

                    $unilevel_bonus    = $total_bonus_value - $total_unilevel_given - $total_global_pool + $total_crown_pool;
                    $unilevel_total    = $unilevel_total + $unilevel_bonus;

                    update_user_meta(1, 'global_pool', $total_global_pool);
                    update_user_meta(1, 'crown_pool', $total_crown_pool);
                    update_user_meta(1, 'unilevel_total', $unilevel_total);

                }
            }
        }
        update_post_meta($order_id, 'commissionscreated', '1', true );
    }

    function Inithooks()
    {
        add_action( 'woocommerce_thankyou', 'processAfterOrderCreated', 10, 1 );
        add_action('woocommerce_order_status_awaiting', array('order_completed'));
        add_action('woocommerce_order_status_received', array('order_completed'));
        add_action('woocommerce_order_status_packing', array('order_completed'));
        add_action('woocommerce_order_status_completed', array('order_completed'));
        add_action('woocommerce_order_status_cancelled', array('order_cancelled'));
        add_action('woocommerce_order_status_processing', array('order_processing'));
        add_action('woocommerce_order_status_refunded',array('order_refunded'));
        add_action('woocommerce_order_status_pending',array('order_pending'));
        add_action('woocommerce_order_status_onhold', array('order_onhold'));
        add_action( 'wp_ajax_RemoveCommissions',array('RemoveCommissions'));
        add_action( 'wp_ajax_GenerateCommissions', array('GenerateCommissions'));
    }

    function test()
    {
        die('test');
    }

    function FormatPrice($value)
    {

        return $value;
    }

    function change_posts_to_tranpost()
    {
        //global $wpdb;
        //$wpdb->posts = $this->wp_trans_posts;
        //$wpdb->postmeta = $this->wp_trans_postmeta;
    }


    function change_posts_to_default()
    {
        //global $wpdb;
        //$wpdb->posts = $this->wp_posts;
        //$wpdb->postmeta =  $this->wp_postmeta ;
    }

    function Sync_Member_Data($user_id = 0) {
        global $wpdb;
        $sql = "CALL main_shopfreemart.update_user_info($user_id);";
        $wpdb->get_results($sql);
        return $sql;
    }


    function Sync_Member_Data_From_Freemart($user_id = 0)
    {

        if ($user_id  == 0)
            //die('No userid specified');
            $user_id = get_current_user_id();

        global $wpdb;
        load_wp();
        $referral_id = $wpdb->get_var("select meta_value from sfm_usermeta
            where meta_key='referral_id' and user_id = $user_id");

        $sponsorkey = $wpdb->get_var("select sponsorkey from sfm_uap_affiliates where user_id =$user_id");

        if (empty($sponsorkey))
        {
            $wpdb->get_var("Update sfm_uap_affiliates set sponsorkey = $referral_id where user_id =$user_id");
            $sponsorkey = $referral_id ;
        }

        $referral_ = $wpdb->get_var("select meta_value from $this->wp_usermeta
            where meta_key='referral_id' and user_id = $user_id");

        $wpdb->get_var("select meta_value from $this->wp_usermeta
            where meta_key='billing_phone' and user_id = $user_id");


        $member_data = Array();

        $billing_phone = $wpdb->get_var("select meta_value from $this->wp_usermeta
            where meta_key='billing_phone' and user_id = $user_id");


        $wpdb->get_results("CALL main_shopfreemart.Update_user_rank($user_id);");
        $wpdb->get_results("CALL main_shopfreemart.update_user_totals($user_id);");

        $user = $wpdb->get_row("Select user_login, user_email, user_registered, display_name from sfm_users where id = $user_id");

        // $value = $totalcomm - $this->get_total_reserved_comms($userid) ;
        // $value = $value - $this->get_withdrawals_completepending($userid);
        // $value = $value - $this->get_total_comm_purchase_excluded($userid);


        $member_data['register_date'] = $user->user_registered;

        $totalwo = $wpdb->get_var("select count(0) from $this->wp_posts
            left join  $this->wp_postmeta  on $this->wp_postmeta.post_id = $this->wp_posts.id  and $this->wp_postmeta.meta_key = '_customer_user'
            where post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing')
            and $this->wp_postmeta.meta_value = $user_id");
        if($totalwo == "")
        {
            $totalwo = "0";
        }

        $totalwo2 = $wpdb->get_var("select count(0) from sfm_posts
            left join  sfm_postmeta  on sfm_postmeta.post_id = sfm_posts.id  and sfm_postmeta.meta_key = '_customer_user'
            where post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing')
            and sfm_postmeta.meta_value = $user_id");
        if($totalwo2 == "")
        {
            $totalwo2 = "0";
        }
$totalwo = (int)$totalwo + (int)$totalwo2;

        $billing_phone = $wpdb->get_var("select meta_value from $this->wp_usermeta
            where meta_key='billing_phone' and user_id = $user_id");
        if($billing_phone == "")
        {
            $billing_phone = "";
        }

        $member_data['total_orders'] = $totalwo;


        $total_users = $wpdb->get_var("select count(0) from $this->wp_usermeta
            where meta_key='referral_id' and meta_value = $user_id");
        $member_data['total_downlines'] = $total_users;
        $member_data['paid_downlines'] = $this->get_totalsales($user_id);
        $member_data['unpaid_referrals'] = (int)$member_data['total_downlines'] - (int)$member_data['paid_downlines'];

        $wpdb->get_results("CALL main_shopfreemart.Load_rank_advancement_history($user_id);");

        $wpdb->get_results("CALL main_shopfreemart.update_user_totals($user_id);");

        // $sponsor_diamond       = $this->get_diamond($user_id);
        // $sponsor_doublediamond = $this->get_doublediamond($user_id);
        // $sponsor_triplediamond = $this->get_triplediamond($user_id);
        // $sponsor_ambassador    = $this->get_ambassador($user_id);
        // $member_data['upline_diamond'] = $sponsor_diamond;
        // $member_data['upline_doublediamond'] = $sponsor_doublediamond ;
        // $member_data['upline_triplediamond'] = $sponsor_triplediamond;
        // $member_data['upline_ambassador'] = $sponsor_ambassador;

        $member_data['referral_link'] = $user->user_login;
        $member_data['email'] = $user->user_email;
        $member_data['phone'] = $billing_phone;


        $ordercount = $wpdb->get_var("Select order_id from sfm_uap_transactions where from_user_id = $user_id and type_id = 101 and status_id = 2
            and order_id > 0  limit 1");

        if($ordercount > 0)
        $member_data['paid'] = 1;
        else
        $member_data['paid'] = 0;


        $sql = "UPDATE main_shopfreemart.sfm_uap_affiliates Set
        display_name='".$user->display_name."'
        ,date_registered='".$member_data['register_date']."'
        ,orders='".$member_data['total_orders']."'
        ,downlines='".$member_data['total_downlines']."'
        ,referralsales='".$member_data['paid_downlines']."'
        ,replicated_url='".$member_data['referral_link']."'
        ,email ='".$member_data['email']."'
        ,phone ='".$member_data['phone']."'
        ,paid ='".$member_data['paid']."'
        WHERE user_id =".$user_id;
        //echo $sql;
        $rez = $wpdb->query($sql);
        return $member_data;
    }


    //************************FOR COMMISSIONS************************************
    function get_reserved_comms($userid = '')
    {

        global $wpdb;

        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'reserved_comms', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commission_sponsor_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish' and $this->wp_trans_posts.post_name  NOT LIKE 'manual%') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commission_order_id' )
            ) as tbl2 where post_date < '2016-06-01' and ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commission_value' )
            ");

        if($value == "")
        {
            $value = 0;
        }

        return $this->FormatPrice($value);
    }

    function get_total_comm_purchase($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_comm_purchase', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_posts.post_status  from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commissionpayment_payer_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commissionpayment_order_id' )
            ) as tbl2 where post_date < '2016-08-12' and (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')) )
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commissionpayment_value' )
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }

    function get_total_comm_purchase_excluded($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_comm_purchase', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_posts.post_status  from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commissionpayment_payer_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commissionpayment_order_id' )
            ) as tbl2 where post_date >= '2016-08-12' and (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')) )
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commissionpayment_value' )
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }

    function get_total_coin_purchase_excluded($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_coin_purchase', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_posts.post_status  from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coinpayment_payer_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coinpayment_order_id' )
            ) as tbl2 where post_date >= '2016-08-12' and (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')) )
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coinpayment_value' )
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }


    function get_total_reserved_comms($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_reserved_comms', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = (float)$this->get_reserved_comms($userid) - (float)$this->get_total_comm_purchase($userid);


        if($value == "")
        {
            $value = 0;
        }

        if($value == "-0")
        {
            $value = 0;
        }


        return $this->FormatPrice($value);
    }

    //************************FOR COINS************************************

    function get_reserved_coins($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'reserved_coins', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coins_sponsor_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish' and $this->wp_trans_posts.post_name  NOT LIKE 'manual%') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coins_order_id' )
            ) as tbl2 where post_date < '2016-06-01' and ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coins_value' )
            ");


        if($value == "")
        $value = 0;
        return $this->FormatPrice($value);
    }

    function get_total_coin_purchase($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_coin_purchase', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value)  from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_posts.post_status from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coinpayment_payer_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coinpayment_order_id' )
            ) as tbl2 where  post_date < '2016-08-12' and (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')) )
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coinpayment_value' )
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }

    function get_total_reserved_coins($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_reserved_coins', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $this->get_reserved_coins($userid) - $this->get_total_coin_purchase($userid);

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }


    //***********************OTHERS************************************/

    function get_total_reserves($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_reserves', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $this->get_total_reserved_comms($userid) + get_total_reserved_coins($userid) - $this->get_total_reserved_manuals($userid);

        if($value == "")
        {
            $value = "0";
        }
        if($value == "-0")
        {
            $value = 0;
        }
        $value = $this->FormatPrice($value);


        return $value ;
    }



    function get_withdrawals_completepending($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        $value = $wpdb->get_var("
            Select sum(withdrawal_amount) from
            (
            Select wpmaa.post_id, wpmaa.meta_value as withdrawal_amount, wpmab.meta_value as withdrawal_status, wpmau.meta_value as withdrawal_userid from $this->wp_posts wpaa
            inner join $this->wp_postmeta wpmaa on (wpaa.ID = wpmaa.post_id and wpmaa.meta_key = 'withdrawal_amount' )
            inner join $this->wp_postmeta wpmau on (wpaa.ID = wpmau.post_id and wpmau.meta_key = 'withdrawal_userid' )
            left join $this->wp_postmeta wpmab on (wpaa.ID = wpmab.post_id and wpmab.meta_key = 'withdrawal_status' )

            where wpaa.post_status = 'publish' and wpmau.meta_value = $userid and (wpmab.meta_value = 'Completed' or wpmab.meta_value = 'Pending')
            ) tt1
            group by withdrawal_userid
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }


    function get_total_reserved_manuals($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'total_reserved_comms', true );
        //if ($savedvalue != null) return $savedvalue;


        $value = $wpdb->get_var("
            Select sum(meta_value) as total from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_posts.post_status  from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commissionpayment_payer_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commissionpayment_order_id' )
            ) as tbl2 where post_date < '2016-08-12' and (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')) )
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commissionpayment_value_manual' ) limit 1 ;
            ");

        if($value == "")
        {
            $value = 0;
        }

        if($value == "-0")
        {
            $value = 0;
        }

        $value2 = $wpdb->get_var("
            Select sum(meta_value) as total from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_posts.post_status,$this->wp_posts.post_date from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coinpayment_payer_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coinpayment_order_id' )
            ) as tbl2 where  post_date < '2016-08-12' and (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')) )
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coinpayment_value_manual' )
            ");

        if($value2 == "")
        {
            $value2 = 0;
        }

        if($value2 == "-0")
        {
            $value2 = 0;
        }

        return (float)$value + (float)$value2;
    }


    function get_payout_crooks($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //  $savedvalue = get_user_meta( $userid, 'total_payout_crooks', true );
        //  if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(amount) from (
            Select wpmbb.meta_value as amount, wpmab.meta_value as user_id from $this->wp_trans_postmeta wpmab
            INNER JOIN
            (
            Select wpmaa.post_id, wpmaa.meta_value from $this->wp_trans_posts wpaa
            inner join $this->wp_trans_postmeta wpmaa on (wpaa.ID = wpmaa.post_id and wpmaa.meta_key = 'davidcrookpayment_amount' )
            where wpaa.post_status = 'publish'
            ) wpmbb
            on (wpmbb.post_id = wpmab.post_id and wpmab.meta_key = 'davidcrookpayment_userid' and wpmab.meta_value=$userid)
            ) tt1
            group by user_id
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }

    function get_payout_balance_crooks($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //$savedvalue = get_user_meta( $userid, 'payout_balance_crooks', true );
        //if ($savedvalue != null) return $savedvalue;

        $value = $this->get_total_reserves($userid) - $this->get_payout_crooks($userid);

        if($value == "")
        {
            $value = "0";
        }

        if($value == "-0")
        {
            $value = 0;
        }
        $value = wc_price($value);
        if($value == "-$0.00"){
            $value = "$0.00";
        }
        $value = str_replace("-", "", $value);
        return ''.$value;
    }

    //************CURRENT COMMS AND COINS*************************

    function get_current_comms($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        $totalcomm = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commission_sponsor_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commission_order_id' )
            ) as tbl2 where ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commission_value' )
            ");
        if($totalcomm == "")
        {
            $totalcomm = "0";
        }


        $value = $totalcomm - $this->get_total_reserved_comms($userid) ;

        $value = $value - $this->get_withdrawals_completepending($userid);

        $value = $value - $this->get_total_comm_purchase_excluded($userid);

        if($value == "")
        {
            $value = 0;
        }

        if($value == - 0)
        {
            $value = 0;
        }

        return $this->FormatPrice($value);
    }

    function get_current_coins_2($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        $totalcoin = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coins_sponsor_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coins_order_id' )
            ) as tbl2 where ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coins_value' )");
        if($totalcoin == "")
        {
            $totalcoin = "0";
        }

        $value = $totalcoin - $this->get_total_reserved_coins($userid);

        $value = $value - $this->get_total_coin_purchase_excluded($userid);

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }

    function get_overall_commissions($userid = '')
    {
        global $wpdb;

        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //  $savedvalue = get_user_meta( $userid, 'total_payout_crooks', true );
        //  if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commission_sponsor_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commission_order_id' )
            ) as tbl2 where ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commission_value' )
            ");


        if($value == "")
        {
            $value = "0";
        }
        return $value ;
        //return $this->FormatPrice($value);
    }
    function get_overall_coins($userid = '')
    {
        global $wpdb;
        if($userid == "" ){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        //  $savedvalue = get_user_meta( $userid, 'total_payout_crooks', true );
        //  if ($savedvalue != null) return $savedvalue;

        $value = $wpdb->get_var("
            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coins_sponsor_id' and meta_value=$userid and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coins_order_id' )
            ) as tbl2 where ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coins_value' )
            ");

        if($value == "")
        {
            $value = "0";
        }
        return $this->FormatPrice($value);
    }


    // END OF COMMISSION TRANSACTIONS SUMMARY //


    function RemoveCommissions2($order_id = 0)
    {
        if(!$order_id){
            $order_id = $_POST['id'];
        }
        // echo json_encode('test');
        // die();
        update_post_meta($order_id, 'commissionscreated', '0', true );
        delete_existing_commissions($order_id);
        echo json_encode(true);
    }

    function GenerateCommissions2()
    {
        $order_id = $_POST['id'];
        $this->process_newcommissions($order_id);
        echo json_encode(true);
    }


    function Runmanualprocess()
    {
        //echo
        echo get_totalsales(438);
        echo "<br />";
        echo order_completed(3701);
        echo "<br />";
        echo get_totalsales(438);
        die;

        $user = 3402;

        $above_sponsor = get_user_meta( $user, 'referral_id', true );

        echo $above_sponsor;
        die;
        // $above_sponsor_sale = get_user_meta( $above_sponsor, 'total_sales', true );

        if($above_sponsor == "")
        $above_sponsor = 27;  // force to John  return ;
        // if ($above_sponsor_sale == 0 || $above_sponsor_sale == "")
        //  $above_sponsor_sale = $this->get_totalsales($above_sponsor);
        global $wpdb;
        $first_purchase_date = get_user_meta($user, 'first_purchase_date',true);
        //echo  $first_purchase_date;
        $above_sponsor_sale = $this->get_ambassador($above_sponsor,$first_purchase_date);
        echo $value;
        die;

        //  $auser = new WP_User($above_sponsor);
        printf($above_sponsor_sale);
        die;
        //override
        $overridesale = get_user_meta( $user, 'override_totalsales', true ) ;
        if($overridesale != null)
        $above_sponsor_sale = $overridesale;
        // end


        if($above_sponsor_sale >= 9){
            return $above_sponsor;
        }
        else
        {
            return get_ambassador($above_sponsor);
        }
    }


    function ManualProcessFounding($userid,$amount)
    {
        $product_total    = $amount;
        $product_quantity = 1;
        $price            = 1000;

        $founder_level    = count_founding_levels($userid);

        $user_info        = new WP_User($userid); //Get member info
        $name             = $user_info->first_name . ' ' . $user_info->last_name;
        $foundingstatus   = 'Completed';

        $display_name     = $user_info->display_name;

        $post_title       = "Founding Membership for ".$name;
        $postdate         = date('Y-m-d H:i:s');


        $new_founding_post= array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $userid,
            'post_type'  => 'foundingmembership'
        );

        // if($product_total > 1000){

        $prod_total = $amount / 1000;
        // echo $prod_total." < br/>";

        $ans        = $prod_total;///get the whole number


        //echo $ans." < br/>";
        $excess     = 0;
        $total      = explode(".", $prod_total,2) ;
        foreach($total as $key )
        {
            ///get excess
        }
        ///excess
        if($product_total % 1000 == 0)
        {
            ///whole number
        }
        else
        {
            $ansExcess = $product_total % 1000;
            $excess    = 1;
            //   echo " < br/>ansExcess = ".$ansExcess." < br/>";
        }


        if($product_quantity > 1 && $price <= 1000)
        {
            $condition = $product_quantity;
        }
        else
        {
            $condition = $ans + $excess;
        }



        //   echo " < br/>condition = ".$condition." < br/>";
        $loopCtr = 0;
        $lvl     = 0;
        for($ctr = 1; $ctr <= $condition; $ctr++){

            $lvl = $founder_level + $loopCtr;

            if($product_quantity > 1 && $price <= 1000){
                $pay     = $price;
                $balance = 1000 - $pay;
            }
            else
            {
                if($ctr > $ans){
                    $pay     = $ansExcess;
                    $balance = 1000 - $pay;
                }
                else
                {
                    $pay     = 1000;
                    $balance = 0;
                }
            }

            //echo "pay = ".$pay." < br/>";

            $founding_post_id = wp_insert_post($new_founding_post);
            //  // if ($value != 0 && $value2)
            //  // $value = wc_price($payment,2);
            //  // $value2 = wc_price($balance,2);


            add_post_meta($founding_post_id, 'foundingmembership_userid', $userid);
            add_post_meta($founding_post_id, 'foundingmembership_notes', 'Reserved Founding Membership');
            add_post_meta($founding_post_id, 'foundingmembership_user', $display_name);
            add_post_meta($founding_post_id, 'foundingmembership_level', $lvl);
            add_post_meta($founding_post_id, 'foundingmembership_payment', $pay);
            add_post_meta($founding_post_id, 'foundingmembership_balance', $balance);
            add_post_meta($founding_post_id, 'foundingmembership_status', $foundingstatus );
            //   $count = $wpdb->get_var( "SELECT COUNT( * ) FROM $wpdb->usermeta WHERE user_id = '$userid' AND meta_key = 'foundingmembership_done' AND meta_value = 1" );

            //     if($count < 1){
            //       add_user_meta( $userid, 'foundingmembership_done', 1);
            //     }

            $loopCtr++;
        }
        ////end of for loop
    }


    function ManualProcessFoundingPSA($userid,$amount)
    {
        $product_total    = $amount;
        $product_quantity = 1;
        $price            = 1000;

        $founder_level    = count_founding_levels($userid);

        $user_info        = new WP_User($userid); //Get member info
        $name             = $user_info->first_name . ' ' . $user_info->last_name;
        $foundingstatus   = 'Completed';

        $display_name     = $user_info->display_name;

        $post_title       = "Founding Membership for ".$name;
        $postdate         = date('Y-m-d H:i:s');


        $new_founding_post= array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $userid,
            'post_type'  => 'foundingmembership'
        );

        // if($product_total > 1000){

        $prod_total = $amount / 1000;
        // echo $prod_total." < br/>";

        $ans        = $prod_total;///get the whole number


        //echo $ans." < br/>";
        $excess     = 0;
        $total      = explode(".", $prod_total,2) ;
        foreach($total as $key )
        {
            ///get excess
        }
        ///excess
        if($product_total % 1000 == 0)
        {
            ///whole number
        }
        else
        {
            $ansExcess = $product_total % 1000;
            $excess    = 1;
            //   echo " < br/>ansExcess = ".$ansExcess." < br/>";
        }


        if($product_quantity > 1 && $price <= 1000)
        {
            $condition = $product_quantity;
        }
        else
        {
            $condition = $ans + $excess;
        }



        //   echo " < br/>condition = ".$condition." < br/>";
        $loopCtr = 0;
        $lvl     = 0;
        for($ctr = 1; $ctr <= $condition; $ctr++){

            $lvl = $founder_level + $loopCtr;

            if($product_quantity > 1 && $price <= 1000){
                $pay     = $price;
                $balance = 1000 - $pay;
            }
            else
            {
                if($ctr > $ans){
                    $pay     = $ansExcess;
                    $balance = 1000 - $pay;
                }
                else
                {
                    $pay     = 1000;
                    $balance = 0;
                }
            }

            //echo "pay = ".$pay." < br/>";

            $founding_post_id = wp_insert_post($new_founding_post);
            //  // if ($value != 0 && $value2)
            //  // $value = wc_price($payment,2);
            //  // $value2 = wc_price($balance,2);


            add_post_meta($founding_post_id, 'foundingmembership_userid', $userid);
            add_post_meta($founding_post_id, 'foundingmembership_notes', 'Reserved PSA Founding Membership');
            add_post_meta($founding_post_id, 'foundingmembership_user', $display_name);
            add_post_meta($founding_post_id, 'foundingmembership_level', $lvl);
            add_post_meta($founding_post_id, 'foundingmembership_payment', $pay);
            add_post_meta($founding_post_id, 'foundingmembership_balance', $balance);
            add_post_meta($founding_post_id, 'foundingmembership_status', $foundingstatus );
            //   $count = $wpdb->get_var( "SELECT COUNT( * ) FROM $wpdb->usermeta WHERE user_id = '$userid' AND meta_key = 'foundingmembership_done' AND meta_value = 1" );

            //     if($count < 1){
            //       add_user_meta( $userid, 'foundingmembership_done', 1);
            //     }

            $loopCtr++;
        }
        ////end of for loop
    }



    /*********************************************************************
    /*********************************************************************
    ***************** MAIN FUNCTIONS ROUTINES  **************************
    *********************************************************************/
    /*********************************************************************/

    function IsOrderHasActiveCommissions($order_id)
    {
        global $wpdb;
        $result = $wpdb->get_var("CALL IsOrderHasActiveCommissions(".$order_id.")");
			 echo $result ;
			 die;
        if($result == 0)
        return 0;
        else
        return 1;
    }


    function get_rank($userid, $first_purchasedate = '')
    {
        global $wpdb;
        if($userid == 6062)
        return "administrator";

        $highest_sale = $this->get_totalsales($userid,$first_purchasedate);

        // if ($highest_sale >= 54) {
        //     $account_role = "crown_ambassador";
        //     $user_id = wp_update_user( array( 'ID' => $userid, 'role' => $account_role ) );
        // }
        // elseif ($highest_sale >= 27) {
        //     $account_role = "global_ambassador";
        //     $user_id = wp_update_user( array( 'ID' => $userid, 'role' => $account_role ) );
        // }
        // else

        if($highest_sale >= 9){
            $account_role = "ambassador";
            $user_id      = wp_update_user( array('ID'  => $userid,'role'=> $account_role ) );
        }
        elseif($highest_sale >= 6){
            $account_role = "triple_diamond";
            $user_id      = wp_update_user( array('ID'  => $userid,'role'=> $account_role ) );
        }
        elseif($highest_sale >= 3){
            $account_role = "double_diamond";
            $user_id      = wp_update_user( array('ID'  => $userid,'role'=> $account_role ) );
        }
        elseif($highest_sale >= 1){
            $account_role = "diamond";
            $user_id      = wp_update_user( array('ID'  => $userid,'role'=> $account_role ) );
        }
        else
        {
            $user     = new WP_User($userid);
            $ownsales = $this->get_owntotalsales($userid);

            if(!empty($user->roles))
            {
                 if( $user->roles[0] == "shop_manager" ){
                    $account_role = "shop_manager";
                    return $account_role;

                }
                elseif( $user->roles[0] == "administrator" ){
                    $account_role = "administrator";
                    return $account_role;
                }
            }

           if($ownsales > 0){
                $account_role = "qualified";
                $user_id      = wp_update_user( array('ID'  => $userid,'role'=> $account_role ) );
            }
            else
            {
                $account_role = "free";

            }
        }


        return $account_role;
    }


    function get_sponsorid($uid)
    {
        global $wpdb;
        return  $wpdb->get_var("select sponsorkey from sfm_uap_affiliates where user_id = $uid");
    }


    function get_diamond( $user )
    {
        global $wpdb;

        // //ADDED to retain original upline
        // if(PERSISTENT_FIRSTDATE)
        // {
        //     $original_diamond_upline = $wpdb->get_var("select meta_value from $this->wp_usermeta
        //         where meta_key='original_diamond_upline' and user_id = $user");

        //     if(!empty($original_diamond_upline))
        //     {
        //         return $original_diamond_upline;
        //     }
        // }
        // //END


        $above_sponsor = $this->get_sponsorid($user);
        if($above_sponsor == "")
        $above_sponsor      = 27;  // force to John  return ;

        //$first_purchase_date = get_user_meta( $user, 'first_purchase_date',true);
        //$above_sponsor_sale = $this->get_totalsales($above_sponsor,$first_purchase_date);
        $above_sponsor_sale = $this->get_totalsales($above_sponsor);
        $overridesale       = $this->get_override_totalsales( $user);

        if($overridesale != null)
        $above_sponsor_sale = $overridesale;

        if($above_sponsor_sale >= 1)
        {
            $wpdb->get_results("INSERT INTO $this->wp_usermeta (user_id, meta_key, meta_value ) values ($user,'original_diamond_upline',$above_sponsor)");
            return $above_sponsor;
        }
        else
        {
            return get_diamond($above_sponsor);
        }
    }

    function get_doublediamond( $user )
    {
        global $wpdb;

        // //ADDED to retain original upline
        // if(PERSISTENT_FIRSTDATE)
        // {
        //     $original_doublediamond_upline = $wpdb->get_var("select meta_value from $this->wp_usermeta
        //         where meta_key='original_doublediamond_upline' and user_id = $user");

        //     if(!empty($original_doublediamond_upline))
        //     {
        //         return $original_doublediamond_upline;
        //     }
        // }
        // //END

        $above_sponsor = $this->get_sponsorid($user);

        if($above_sponsor == "")
        $above_sponsor      = 27;  // force to John  return ;

        //$first_purchase_date = get_user_meta( $user, 'first_purchase_date',true);
        //$above_sponsor_sale = $this->get_totalsales($above_sponsor,$first_purchase_date);
        $above_sponsor_sale = $this->get_totalsales($above_sponsor);
        $overridesale       = $this->get_override_totalsales( $user);

        if($overridesale != null)
        $above_sponsor_sale = $overridesale;
        // end


        if($above_sponsor_sale >= 3)
        {
            $wpdb->get_results("INSERT INTO $this->wp_usermeta (user_id, meta_key, meta_value ) values ($user,'original_doublediamond_upline',$above_sponsor)");
            return $above_sponsor;
        }
        else
        {
            return get_doublediamond($above_sponsor);
        }
    }


    function get_triplediamond( $user )
    {
        global $wpdb;

        // //ADDED to retain original upline
        // if(PERSISTENT_FIRSTDATE)
        // {
        //     $original_triplediamond_upline = $wpdb->get_var("select meta_value from $this->wp_usermeta
        //         where meta_key='original_triplediamond_upline' and user_id = $user");

        //     if(!empty($original_triplediamond_upline))
        //     {
        //         return $original_triplediamond_upline;
        //     }
        // }
        // //END

        $above_sponsor = $this->get_sponsorid($user);

        if($above_sponsor == "")
        $above_sponsor      = 27;  // force to John  return ;

        //$first_purchase_date = get_user_meta( $user, 'first_purchase_date',true);
        //$above_sponsor_sale = $this->get_totalsales($above_sponsor,$first_purchase_date);
        $above_sponsor_sale = $this->get_totalsales($above_sponsor);
        $overridesale       = $this->get_override_totalsales( $user);

        if($overridesale != null)
        $above_sponsor_sale = $overridesale;
        // end


        if($above_sponsor_sale >= 6)
        {
            $wpdb->get_results("INSERT INTO $this->wp_usermeta (user_id, meta_key, meta_value ) values ($user,'original_triplediamond_upline',$above_sponsor)");
            return $above_sponsor;
        }
        else
        {
            return get_triplediamond($above_sponsor);
        }
    }


    function get_ambassador( $user )
    {

         global $wpdb;
        // //ADDED to retain original upline
        // if(PERSISTENT_FIRSTDATE)
        // {
        //     $original_ambassador_upline = $wpdb->get_var("select meta_value from $this->wp_usermeta
        //         where meta_key='original_ambassador_upline' and user_id = $user");

        //     if(!empty($original_ambassador_upline))
        //     {
        //         return $original_ambassador_upline;
        //     }
        // }
        // //END

        $above_sponsor = $this->get_sponsorid($user);

        if($above_sponsor == "")
        $above_sponsor      = 27;  // force to John  return ;

        //$first_purchase_date = get_user_meta( $user, 'first_purchase_date',true);
        //$above_sponsor_sale = $this->get_totalsales($above_sponsor,$first_purchase_date);
        $above_sponsor_sale = $this->get_totalsales($above_sponsor);
        $overridesale       = $this->get_override_totalsales( $user);

        if($overridesale != null)
        $above_sponsor_sale = $overridesale;
        // end


        if($above_sponsor_sale >= 6)
        {
            $wpdb->get_results("INSERT INTO $this->wp_usermeta (user_id, meta_key, meta_value ) values ($user,'original_ambassador_upline',$above_sponsor)");
            return $above_sponsor;
        }
        else
        {
            return get_ambassador($above_sponsor);
        }
    }


    function get_override_totalsales($uid)
    {
        global $wpdb;
        return  $wpdb->get_var("select meta_value from $this->wp_usermeta
            where meta_key='override_totalsales' and user_id = $uid");
    }

    function get_rolebadge($type)
    {
        if($type == 'crown_ambassador'){
            $role_badge = "badge-ambassador";
        }
        elseif($type == 'global_ambassador'){
            $role_badge = "badge-ambassador";
        }
        elseif($type == 'ambassador'){
            $role_badge = "badge-ambassador";
        }
        elseif($type == 'triple_diamond'){
            $role_badge = "badge-triplediamond";
        }
        elseif($type == 'double_diamond'){
            $role_badge = "badge-doublediamond";
        }
        elseif($type == 'diamond'){
            $role_badge = "badge-diamond";
        }
        elseif($type == 'administrator'){
            $role_badge = "badge-ambassador";
        }
        elseif($type == 'shop_manager'){
            $role_badge = "badge-ambassador";
        }
        elseif($type == 'qualified'){
            $role_badge = "badge-qualified";
        }
        elseif($type == 'free'){
            $role_badge = "badge-free";
        }
        return $role_badge;
    }

    function get_rolename($type)
    {
        if($type == 'crown_ambassador'){
            $account_name = "FreeMart Crown Ambassador";
        }
        elseif($type == 'global_ambassador'){
            $account_name = "FreeMart Global Ambassador";
        }
        elseif($type == 'ambassador'){
            $account_name = "FreeMart Ambassador";
        }
        elseif($type == 'triple_diamond'){
            $account_name = "FreeMart Triple Diamond";
        }
        elseif($type == 'double_diamond'){
            $account_name = "FreeMart Double Diamond";
        }
        elseif($type == 'diamond'){
            $account_name = "FreeMart Diamond";
        }
        elseif($type == 'administrator'){
            $account_name = "FreeMart Admin";
        }
        elseif($type == 'shop_manager'){
            $account_name = "FreeMart Manager";
        }
        elseif($type == 'qualified'){
            $account_name = "Member";
        }
        elseif($type == 'free'){
            $account_name = "Free Member";
        }

        return $account_name;
    }

    function get_rankname($userid)
    {
        $highest_sale = $this->get_totalsales($userid);
        // if ($highest_sale >= 54) {
        //     //echo "Crown Ambassador";
        //     $account_type = "Ambassador";
        // }
        // elseif ($highest_sale >= 27) {
        //     //echo "Global Ambassador";
        //     $account_type = "Ambassador";
        // }
        // else
        if($highest_sale >= 9){
            //echo "Ambassador";
            $account_type = "FreeMart Ambassador";
        }
        elseif($highest_sale >= 6){
            //echo "Triple Diamond";
            $account_type = "FreeMart Triple Diamond";
        }
        elseif($highest_sale >= 3){
            //echo "Double Diamond";
            $account_type = "FreeMart Double Diamond";
        }
        elseif($highest_sale >= 1){
            //echo "Diamond";
            $account_type = "FreeMart Diamond";
        }
        return $account_type;
    }

    function rank_upline( $atts )
    {



        if(isset($_SESSION['backid'])){
            $current_user = new WP_User( $_SESSION['backinfo']->ID );
        }
        else
        {
            $current_user = wp_get_current_user();
        }

        $userid                = $current_user->ID;

        $sponsorinfo           = new WP_User(get_diamond($userid));

        $sponsor_diamond       = $sponsorinfo->display_name; ;

        $sponsorinfo           = new WP_User(get_doublediamond($userid));
        $sponsor_doublediamond = $sponsorinfo->display_name;

        $sponsorinfo           = new WP_User(get_triplediamond($userid));
        $sponsor_triplediamond = $sponsorinfo->display_name;

        $sponsorinfo           = new WP_User(get_ambassador($userid));
        $sponsor_ambassador    = $sponsorinfo->display_name;

        $output                = "<div class='upline-sponsors'>";
        $output .= "<h4>Upline Leaders</h4>";
        $output .= "<div class='upline-item col-xs-12'>";
        $output .= "<div class='col-xs-12 col-sm-3'><span class='badge badge-diamond'></span><p>" . $sponsor_diamond . "</p><p class='para2'>(Diamond)</p></div>";
        $output .= "<div class='col-xs-12 col-sm-3'><span class='badge badge-doublediamond'></span><p>" . $sponsor_doublediamond . "</p><p class='para2'>(Double Diamond)</p></div>";
        $output .= "<div class='col-xs-12 col-sm-3'><span class='badge badge-triplediamond'></span><p>" . $sponsor_triplediamond . "</p><p class='para2'>(Triple Diamond)</p></div>";
        $output .= "<div class='col-xs-12 col-sm-3'><span class='badge badge-ambassador'></span><p>" . $sponsor_ambassador . "</p><p class='para2'>(Ambassador)</p></div>";
        $output .= "</div>";
        $output .= "</div>";

        return $output;
    }


    function main_sponsor( $atts )
    {

        if(isset($_SESSION['backid'])){
            $current_user = new WP_User( $_SESSION['backinfo']->ID );
        }
        else
        {
            $current_user = wp_get_current_user();
        }
        global $wpdb;


        if($current_user == null)
        return "";


        $sponsor_id = get_user_meta( $current_user->ID, 'referral_id', true );

        if($sponsor_id == null)
        return "";

        $spon_name = $wpdb->get_var("SELECT display_name from $this->wp_users WHERE `ID` = $sponsor_id");

        $spon_email= $wpdb->get_var("SELECT user_email from $this->wp_users WHERE `ID` = $sponsor_id");

        $spon_phone= $wpdb->get_var("SELECT meta_value from $this->wp_usermeta WHERE `user_id` = $sponsor_id AND `meta_key` = 'billing_phone' ORDER BY `user_id`");


        $output    = "<div class='upline-sponsors'>";
        $output .= "<h4><span style='font-size: 18px; font-weight: bold;'>Main Sponsor</span></h4>";
        $output .= "<div class='upline-item col-xs-12'>";
        $output .= "<div class='col-xs-12 col-sm-4'><p><span style='font-size: 20px !important'>".$spon_name."</span></p><p class='para2'><span style='font-size: 12px !important'>(Name)</span></p></div>";
        $output .= "<div class='col-xs-12 col-sm-4'><p><span style='font-size: 20px !important'>".$spon_email."</span></p><p class='para2'><span style='font-size: 12px !important'>(Email)</span></p></div>";
        $output .= "<div class='col-xs-12 col-sm-4'><p><span style='font-size: 20px !important'>".$spon_phone."</span></p><p class='para2'><span style='font-size: 12px !important'>(Phone)</span></p></div>";
        $output .= "</div>";
        $output .= "</div>";

        return $output;
    }


    function get_owntotalsales($userid)
    {

        if($userid == "")
        {
            if(isset($_SESSION['backid'])){
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid']))
            {
                $userid = $_GET['uid'];
            }
        }
        global $wpdb;
        $totalsales = $wpdb->get_var(
            "Select Count(0) as totalsale from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commission_payer_id' and meta_value=".$userid." and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commission_remark' ) and (meta_value like '%Sponsor Bonus%')"
        );
        //return $totalsales;

        if($totalsales == ""){
            $totalsales = "0";
        }

        return $totalsales;
    }

    function get_withdrawalpayments($userid)
    {
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        global $wpdb;
        $cur_withdrawalpayment_pay = $wpdb->get_var(
            "Select sum(meta_value) from $this->wp_trans_postmeta,
            (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'withdrawal_userid' and meta_value= ".$userid." and ($this->wp_trans_posts.post_status='pending' or $this->wp_trans_posts.post_status='publish')
            ) as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'withdrawal_amount')"
        );


        if($cur_withdrawalpayment_pay == "")
        {
            $cur_withdrawalpayment_pay = "0.00";
        }
        else
        {
            $cur_withdrawalpayment_pay = round($cur_withdrawalpayment_pay,2);
        }

        return $cur_withdrawalpayment_pay;
    }


    function get_commissionpayments($userid)
    {
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        global $wpdb;
        $cur_commissionpayment_pay = $wpdb->get_var("

            Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_trans_posts.post_status  from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commissionpayment_payer_id' and meta_value=".$userid." and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commissionpayment_order_id' )
            ) as tbl2 where  (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commissionpayment_value' )
            ");


        if($cur_commissionpayment_pay == "")
        {
            $cur_commissionpayment_pay = "0.00";
        }
        else
        {
            $cur_commissionpayment_pay = round($cur_commissionpayment_pay,2);
        }
        // $cur_commissionpayment_pay = $userid;
        return $cur_commissionpayment_pay;
    }

    function get_coinpayments($userid)
    {
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        global $wpdb;
        $cur_coinpayment_pay = $wpdb->get_var(

            "Select sum(meta_value)  from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id, $this->wp_trans_posts.post_status from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coinpayment_payer_id' and meta_value=".$userid." and $this->wp_trans_posts.post_status='publish') as tbl
            where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coinpayment_order_id' )
            ) as tbl2 where (ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish')))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coinpayment_value' )"
        );


        if($cur_coinpayment_pay == "")
        {
            $cur_coinpayment_pay = "0.00";
        }
        else
        {
            $cur_coinpayment_pay = round($cur_coinpayment_pay,2);
        }

        return $cur_coinpayment_pay;
    }

    function get_totalcommissions($userid)
    {
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        global $wpdb;
        $cur_commission_pay = $wpdb->get_var(
            "Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commission_sponsor_id' and meta_value=".$userid." and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commission_order_id' )
            ) as tbl2 where ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing','publish'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'commission_value' )"
        );
        //return $commission;


        if($cur_commission_pay == "")
        {
            $cur_commission_pay = "0.00";
        }
        else
        {
            $cur_commission_pay = round($cur_commission_pay,2);
        }


        return $cur_commission_pay;
    }


    function get_totalcoins($userid)
    {
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        global $wpdb;
        $cur_coin_pay = $wpdb->get_var(
            "Select sum(meta_value) from $this->wp_trans_postmeta,
            (
            Select tbl2.post_id from $this->wp_posts, (
            Select meta_value,$this->wp_trans_postmeta.post_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'coins_sponsor_id' and meta_value=".$userid." and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'coins_order_id' )
            ) as tbl2 where ID = tbl2.meta_value and (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing'))
            ) as maintbl where $this->wp_trans_postmeta.post_id = maintbl.post_id and (meta_key = 'coins_value' )"
        );


        if($cur_coin_pay == "")
        {
            $cur_coin_pay = "0.00";
        }
        else
        {
            $cur_coin_pay = round($cur_coin_pay,2);
        }


        return $cur_coin_pay;
    }

    function get_totalsales($userid, $first_purchasedate = '')
    {
        global $wpdb;
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        if($first_purchasedate != '')
        $first_purchasedate_criteria = " and order_date <= '".$first_purchasedate."' ";
        else
        $first_purchasedate_criteria = "";


        $ssql                        = "Select count(0) from sfm_uap_ranks_history where affiliate_id = ".$userid.$first_purchasedate_criteria;
        $totalsales                  = $wpdb->get_var( $ssql);
        // echo $totalsales;
        // echo ' < br />';
        //  echo $ssql;

        //die;
        //return $totalsales;

        //override
        //$overridesale = get_user_meta( $userid, 'override_totalsales', true ) ;
        //if ($overridesale != null)
        //      $totalsales = $overridesale;
        // end

        if($totalsales == "")
        {
            $totalsales = "0";
        }

        return $totalsales;
    }

    function get_alldownlinetotalsales($userid)
    {
        global $wpdb;
        if($userid == ""){
            if(isset($_SESSION['backid']))
            {
                $current_user = new WP_User( $_SESSION['backinfo']->ID );
            }
            else
            {
                $current_user = wp_get_current_user();
            }
            $userid = $current_user->ID;
            if(isset($_GET['uid'])){
                $userid = $_GET['uid'];
            }
        }

        $downlinetotalsales = $wpdb->get_var("

            Select count(0) from $this->wp_trans_postmeta wpmab
            INNER JOIN
            (

            Select wpmaa.post_id, wpmaa.meta_value from $this->wp_trans_posts wpaa
            inner join $this->wp_trans_postmeta wpmaa on (wpaa.ID = wpmaa.post_id and wpmaa.meta_key = 'commission_remark' and (wpmaa.meta_value = 'Sponsor Bonus' or wpmaa.meta_value = 'Founder Purchase' or wpmaa.meta_value = 'PSA Purchase'))
            where wpaa.post_status = 'publish'
            ) wpmbb
            on (wpmbb.post_id = wpmab.post_id and wpmab.meta_key = 'commission_sponsor_id' and wpmab.meta_value =".$userid.")
            ");
        //return $totalsales;

        //override
        //    $overridesale = get_user_meta( $userid, 'override_totalsales', true ) ;
        //   if ($overridesale != null)
        //      $totalsales = $overridesale;
        // end

        if($downlinetotalsales == "")
        {
            $downlinetotalsales = "0";
        }

        return $downlinetotalsales;
    }


    function create_founding_transaction( $id, $quantity )
    {

        $payer_info = new WP_User($id); //Get member info
        $subtotal   = $_POST['founder_balance'];
        $total      = $quantity * $_POST['founder_balance'];

        $address    = array(
            'first_name'=> $payer_info->first_name,
            'last_name' => $payer_info->last_name,
            'company'   => $payer_info->billing_company,
            'email'     => $payer_info->user_email,
            'phone'     => $payer_info->billing_phone,
            'address_1' => $payer_info->billing_address_1,
            'address_2' => $payer_info->billing_address_2,
            'city'      => $payer_info->billing_city,
            'state'     => $payer_info->billing_state,
            'postcode'  => $payer_info->billing_postcode,
            'country'   => $payer_info->billing_country
        );

        $args['totals']['subtotal'] = $total;
        $args['totals']['total'] = $total;

        $args['totals']['subtotal_tax'] = 0;
        $args['totals']['tax'] = 0;

        $order = wc_create_order(array('customer_id'=> $id));
        $order->add_product( get_product( '1110' ), $quantity, $args ); //(get_product with id and next is for quantity)
        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );
        $order->calculate_totals();
        $order->update_status('completed');

        update_post_meta($order->id, '_order_total', $total);


        if($_SESSION['founding_position'] != '' && $_SESSION['founding_price'] != ''){
            $fap = $_SESSION['founding_position'];
            $fp  = $_SESSION['founding_price'];
        }

        update_post_meta($order->id, '_founder_additional_payment', $fap);


        //update_user_meta($id,$fap,$newPrice);
    }






    function order_pending($order_id)
    {
        change_status('pending',$order_id);
    }

    function order_processing($order_id)
    {
        change_status('processing',$order_id);
    }

    function order_refunded($order_id)
    {
        change_status('cancelled',$order_id);
    }

    function order_cancelled($order_id)
    {
        update_post_meta($order_id, 'commissionscreated', '0', true );
        delete_existing_commissions($order_id);
        change_status('cancelled',$order_id);
    }

    function order_failed($order_id)
    {
        change_status('cancelled',$order_id);
    }

    function order_onhold($order_id)
    {
        die(
            'test');
        change_status('onhold',$order_id);
    }
    function order_completed($order_id)
    {

        try
        {
            $order = new WC_Order($order_id);
            $this->process_newcommissions($order);
            change_status('completed',$order_id);
            return "1";
        }
        catch(Exception $e){
            return "0";
        }
    }


    function getorderdetails($order_id)
    {
        //Check if Founding Member has been purchased
        $order = new WC_Order($order_id);
        $items = $order->get_items();
        foreach($items as $item => $values){

            // if Item is Founding Membership
            if($product_id == 626){
                $product_name         = $item['name'];
                $product_id           = $item['product_id'];
                $product_variation_id = $item['variation_id'];
                $product_total        = $item['line_total'];
                $product_quantity     = $item['qty'];

            }

        }
    }



    function change_status($orderstatus, $order_id = 0)
    {

        // Update founding Memmbership if exist
        global $wpdb;

        if($order_id != 0)
        {
            $update = $wpdb->query("

                UPDATE $this->wp_trans_postmeta wp INNER JOIN
                (
                Select wp2.meta_id as meta_id from $this->wp_trans_postmeta wp1
                INNER JOIN
                $this->wp_trans_postmeta wp2 on (wp1.post_id = wp2.post_id and wp2.meta_key = 'foundingmembership_status' )
                where wp1.meta_key = 'foundingmembership_order_id' and wp1.meta_value = ".$order_id."
                ) as wp1
                ON wp.meta_id = wp1.meta_id
                Set wp.meta_value = '".$this->get_commission_status($orderstatus)."'
                ");

            if($orderstatus == 'completed')
            {
                $saveDate = get_post_meta($order_id, 'completed_date', true);
                if($saveDate == null)
                update_post_meta( $order_id, 'completed_date', date('Y-m-d') );
            }

            $status = $this->get_commission_status($orderstatus,$order_id);

            $update = $wpdb->query("UPDATE $this->wp_trans_postmeta wp INNER JOIN
                (
                Select wp2.meta_id as meta_id from $this->wp_trans_postmeta wp1
                INNER JOIN
                $this->wp_trans_postmeta wp2 on (wp1.post_id = wp2.post_id and wp2.meta_key = 'commission_status' )
                where wp1.meta_key = 'commission_order_id' and wp1.meta_value = ".$order_id."
                ) as wp1
                ON wp.meta_id = wp1.meta_id
                Set wp.meta_value = '".$status."'");

            $update = $wpdb->query("UPDATE $this->wp_trans_postmeta wp INNER JOIN
                (
                Select wp2.meta_id as meta_id from $this->wp_trans_postmeta wp1
                INNER JOIN
                $this->wp_trans_postmeta wp2 on (wp1.post_id = wp2.post_id and wp2.meta_key = 'coins_status' )
                where wp1.meta_key = 'coins_order_id' and wp1.meta_value = ".$order_id."
                ) as wp1
                ON wp.meta_id = wp1.meta_id
                Set wp.meta_value = '".$status."'");

        }

    }


    function delete_existing_commissions($order_id, $note)
    {
        //ld("CALL remove_commissions($order_id, $note)");
        global $wpdb;
        $posts = $wpdb->get_results("CALL remove_commissions($order_id, '$note')");
    }

    function delete_existing_user_commissions($order_id, $user_id)
    {
        global $wpdb;
        $posts = $wpdb->get_results("Select post_id from $this->wp_trans_postmeta where meta_id in (
            Select * from (
            Select meta_id from $this->wp_trans_postmeta, (Select post_id from $this->wp_trans_posts inner join $this->wp_trans_postmeta on $this->wp_trans_posts.ID = $this->wp_trans_postmeta.post_id
            and meta_key = 'commission_sponsor_id' and meta_value='".$user_id."' and $this->wp_trans_posts.post_status='publish') as tbl where $this->wp_trans_postmeta.post_id = tbl.post_id and (meta_key = 'commission_order_id' ) and meta_value = '".$order_id."') as p)");

        if($wpdb->num_rows > 0)
        {
            foreach($posts as $post)
            {
                $this->change_posts_to_tranpost();
                $post_id = $post->post_id;
                // echo $post_id;
                wp_trash_post( $post_id);
                $this->change_posts_to_default();
            }
        }
    }



    function get_commission_status($orderstatus,$order_id = 0)
    {
        if($orderstatus == 'awaiting' || $orderstatus == 'received' || $orderstatus == 'packing' || $orderstatus == 'completed'  )
        {
            // if ($order_id == 0)
            //  $status = 'Completed';
            // else
            // {
            //     $saveDate = get_post_meta($order_id, 'completed_date', true);
            //     if ($saveDate == null)
            //     {
            //      $status = "On - hold";
            //  }
            //  else
            //  {
            //      $currentDate = time();
            //      $nextWednesday = strtotime('next wednesday', $saveDate);
            //      $nextWednesday = date('d - m - Y', $nextWednesday);
            //      if (strtotime($nextWednesday) < $currentDate) {
            //          $status = "On - hold";
            //          #$date occurs in the future
            //      } else {
            //          $status = "Completed";
            //      }
            //  }
            // }
            $status = "Completed";
        }
        elseif($orderstatus == 'processing')
        $status = 'Processing';
        elseif($orderstatus == 'cancelled' || $orderstatus == 'refunded' )
        $status = 'Cancelled';
        elseif($orderstatus == 'pending' )
        $status = 'Pending';
        elseif($orderstatus == 'on-hold' )
        $status = 'On-hold';
        else
        $status = $orderstatus;
        return $status;
    }


    function display()
    {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM wp_coins_and_commissions INNER JOIN $this->wp_users ON wp_coins_and_commissions.full_name = new WP_Users.display_name");
        echo '<table cellspacing="0" cellpadding="8" border="1">';
        echo '<tr>
        <td>id</td>
        <td>name</td>
        <td>total coins</td>
        <td>total commissions</td>
        <td>total</td>
        </tr>';
        foreach($result as $value){
            $id    = $value->ID;
            $val   = $value->total_coins;
            $total = $val + $value->total_commission;
            // echo ' < tr >
            // < td > '.$id.'</td >
            // < td > '.$value->full_name.'</td >
            // < td > '.$val.'</td >
            // < td > '.$value->total_commission.'</td >
            // < td > '.$total.'</td >
            // </tr > ';
            // if(transfer_coins_to_commission( $id, $val,$total )){
            //  echo "ok < br/>";
            // }
            runscript($id);
        }
        echo '</table>';
    }

    function runscript($id)
    {
        global $wpdb;
        $wpdb->query("Update $this->wp_trans_posts set post_status = 'Trash' where id =
            (Select post_id from $this->wp_trans_postmeta where post_id in
            (Select post_id from $this->wp_trans_postmeta where meta_key = 'withdrawal_remark' and meta_value = 'Pending payment to be paid by David Crookston')
            and meta_key = 'withdrawal_userid' and meta_value = '".$id."'
            )");

        $wpdb->query("Update $this->wp_trans_posts set post_status = 'Trash' where id =
            (Select post_id from $this->wp_trans_postmeta where post_id in
            (Select post_id from $this->wp_trans_postmeta where meta_key = 'commission_remark' and meta_value = 'Coin Transfer')
            and meta_key = 'commission_sponsor_id' and meta_value = '".$id."'
            )");

        $wpdb->query("Update $this->wp_trans_posts set post_status = 'Trash' where id =
            (Select post_id from $this->wp_trans_postmeta where post_id in
            (Select post_id from $this->wp_trans_postmeta where meta_key = 'coinpayment_remark' and meta_value = 'Coin Payment')
            and meta_key = 'coinpayment_payer_id' and meta_value = '".$id."'
            )");
    }

    function pending_payment($atts)
    {
        global $wpdb;
        $userid          = $this->get_current_user_id();

        $result          = $wpdb->get_row("SELECT * FROM $this->wp_trans_postmeta WHERE meta_key='withdrawal_userid' AND meta_value='$userid'");
        $pending_payment = get_post_meta($result->post_id,'withdrawal_amount',true);

        $display = $wpdb->get_var("
            Select sum(meta3.meta_value) as TotalPendingPayment
            from $this->wp_trans_posts
            INNER JOIN $this->wp_trans_postmeta meta1 ON ( meta1.post_id = $this->wp_trans_posts.id and meta1.meta_key = 'withdrawal_remark' and meta1.meta_value = 'Pending payment to be paid by David Crookston')
            INNER JOIN $this->wp_trans_postmeta meta2 ON ( meta2.post_id = $this->wp_trans_posts.id and meta2.meta_key = 'withdrawal_userid' and meta2.meta_value = '".$userid."')
            INNER JOIN $this->wp_trans_postmeta meta3 ON ( meta3.post_id = $this->wp_trans_posts.id and meta3.meta_key = 'withdrawal_amount')
            where $this->wp_trans_posts.post_status = 'publish'");

        if($userid != '102' && $userid != '105' && $userid != '110' && $userid != '1336' && $userid != '134' && $userid != '136' && $userid != '1372' && $userid != '143' && $userid != '152' && $userid != '1594' && $userid != '168' && $userid != '183' && $userid != '1927' && $userid != '239' && $userid != '2481' && $userid != '2563' && $userid != '1515' && $userid != '2777' && $userid != '302' && $userid != '3216' && $userid != '347' && $userid != '3483' && $userid != '38' && $userid != '397' && $userid != '4366' && $userid != '485' && $userid != '566' && $userid != '58' && $userid != '61' && $userid != '643' && $userid != '665' && $userid != '667' && $userid != '686' && $userid != '731' && $userid != '734' && $userid != '768' && $userid != '734'){
            // return wc_price($display);
        }
        return wc_price($display);

    }



    function membership_list($atts)
    {
        global $wpdb;
        $id          = $this->get_current_user_id();
        $get_post_id = $wpdb->get_results("SELECT * FROM $this->wp_trans_postmeta WHERE meta_value = '$id' AND meta_key = 'foundingmembership_userid'");
        foreach($get_post_id as $value){
            //echo $value->post_id." < br/>";
            $meta = get_post_meta( $value->post_id, 'foundingmembership_payment',true);
            // echo $value->post_id." - ".$meta." < br/>";
        }

        //echo $postId;
        //$meta = get_post_meta( $postId, 'foundingmembership_payment',true);
        ///echo $meta;
    }

    function manual_add_founding_payment($atts)
    {
        return get_template_part( 'add_founding_payment', get_post_format() );
    }


    function count_records()
    {
        global $wpdb;
        $userid      = $this->get_current_user_id();

        $count_query = "SELECT $this->wp_trans_postmeta.post_id FROM $this->wp_trans_postmeta INNER JOIN $this->wp_trans_posts ON $this->wp_trans_postmeta.post_id = $this->wp_trans_posts.ID WHERE $this->wp_trans_postmeta.meta_key = 'foundingmembership_userid' AND $this->wp_trans_postmeta.meta_value = '$userid' AND $this->wp_trans_posts.post_status = 'publish'";
        $num         = $wpdb->get_var($count_query);

        $num         = $wpdb->num_rows + 1;
        return $num;
    }

    function input_founding_payment( $userId, $founder_lvl, $payment, $balance, $status, $remarks)
    {
        global $wpdb;
        if($userId == ''){
            echo "<script>alert('User doesn't exist');</script>";
            $url = esc_url( home_url() )."/add-founding-payment/?issue=user";
            wp_redirect($url);
            exit();
            return false;
        }
        else
        {
            $user_info    = new WP_User($userId); //Get member info
            $name         = $user_info->first_name . ' ' . $user_info->last_name;

            $display_name = $user_info->display_name;
            $post_title   = "Founding Membership for ".$name;
            $postdate     = date('Y-m-d H:i:s');

            $post_details = $wpdb->get_results("SELECT ID,post_author FROM $this->wp_trans_posts WHERE post_author = '$userId' AND post_title = '$post_title' AND post_type = 'foundingmembership'");
            foreach($post_details as $key ){
                $meta = get_post_meta($key->ID,'foundingmembership_payment',true);
                echo "post id = ".$key->ID." author id = ".$key->post_author." payment = ".$meta."<br/>";
            }

            $new_founding_post = array(
                'post_title' => $post_title,
                'post_status'=> 'publish',
                'post_date'  => $postdate,
                'post_author'=> $userId,
                'post_type'  => 'foundingmembership'
            );

            $founding_post_id = wp_insert_post($new_founding_post);

            if($value != 0 && $value2)
            $value = wc_price($payment,2);
            $value2= wc_price($balance,2);

            if($payment == 1000){
                $status = "Completed";
            }

            add_post_meta($founding_post_id, 'foundingmembership_userid', $userId);
            add_post_meta($founding_post_id, 'foundingmembership_user', $display_name);
            add_post_meta($founding_post_id, 'foundingmembership_level', $founder_lvl);
            add_post_meta($founding_post_id, 'foundingmembership_payment', $payment);
            add_post_meta($founding_post_id, 'foundingmembership_balance', $balance);
            add_post_meta($founding_post_id, 'foundingmembership_status', $status);
            add_post_meta($founding_post_id, 'foundingmembership_remarks', $remarks);

            $count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE user_id='$userId' AND meta_key='foundingmembership_done' AND meta_value = 1" );

            if($count < 1){
                add_user_meta( $userId, 'foundingmembership_done', 1);
            }
            // add_user_meta( $userId, 'foundingmembership_done', 1);
            return true;
        }

    }
    function check_founding_transaction($userid)
    {
        global $wpdb;
        $post_details = $wpdb->get_results("SELECT ID,post_author FROM $this->wp_trans_posts WHERE post_author = '$userid' AND post_type = 'foundingmembership' AND post_status = 'publish'");
        foreach($post_details as $val){
            $balance = get_post_meta($val->ID,'foundingmembership_balance',true);
            ?>
            <script type="text/javascript">
                $(document).ready(function()
                    {
                        $('#founding_transaction').click(function()
                            {
                                $('#founding').modal('show');
                            });
                    });
            </script>
            <div class="modal fade" id="founding" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 150px;">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-body">


                        </div>
                        <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div> -->
                    </div>
                </div>
            </div>

            <?php
            break;
        }
        return $userid;
    }

    function updateDisplayFoundingMembership()
    {

    }

    function isPostback()
    {
        $isPostBack = false;

        $referer = "";
        $thisPage= "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

        if(isset($_SERVER['HTTP_REFERER'])){
            $referer = $_SERVER['HTTP_REFERER'];
        }

        if($referer == $thisPage){
            $isPostBack = true;
        }
        return $isPostBack;
    }

    function display_founding_membership($atts)
    {

        global $wpdb;
        global $post;

        //Check if not post back)  $ret = fix those founding record.
        //if (!isPostback())
        //{
        // FixFoundingShits();
        //}

        //raprap
        $newfounding = update_post_meta($post->ID,'newfounding','1');
        $userid      = $this->get_current_user_id();
        $link        = add_query_arg( 'founding', 'new', 'http://free-mart.com/product/founding-membership/' );
        //$wpdb->posts = "$this->wp_trans_posts";
        //$wpdb->postmeta = "$this->wp_trans_postmeta";
        //var_dump(change_posts_to_tranpost());

        ///return $userid;
        // $link = esc_url( home_url() )." / product / founding - membership / ";
        $result      = '<a href="'.$link.'" class="btn btn-primary">Avail Founding Membership</a><br/><br/>';
        if(is_admin())
        {
            $result = 'Enter id to be deleted: <input type="text" id ="foundingid"></input> <a href="'.$link.'" class="btn btn-primary">Delete Now (walang bawian)</a><br/><br/>';
            $link   = add_query_arg( 'founding', 'New', 'http://free-mart.com/product/founding-membership/' );
        }
        $result .= '<table class="table"><thead>';
        $result .= '<tr style="background:#6495ED;color:white;"><th>ID</th><th>Level</th><th>Paid</th><th>Balance</th><th>Status</th><th>Notes</th><th>Action</th><th></th></tr></thead><tbody>';


        // $post_details = $wpdb->get_results("SELECT ID,post_author FROM $this->wp_trans_posts WHERE post_author = '$userid' AND post_type = 'foundingmembership' AND post_status = 'publish'");
        $post_details = $wpdb->get_results("
            Select
            $this->wp_posts.id as post_id,
            t1.meta_value as  foundingmembership_level,
            t2.meta_value as  foundingmembership_status
            from $this->wp_posts
            INNER JOIN $this->wp_postmeta on $this->wp_posts.id = $this->wp_postmeta.post_id
            LEFT JOIN $this->wp_postmeta t1 on ($this->wp_posts.id = t1.post_id and t1.meta_key =  'foundingmembership_level')
            LEFT JOIN $this->wp_postmeta t2 on ($this->wp_posts.id = t2.post_id and t2.meta_key =  'foundingmembership_status')
            where $this->wp_postmeta.meta_key =  'foundingmembership_userid' and $this->wp_postmeta.meta_value = '$userid'
            order by ABS(t1.meta_value)
            ");
        $ctr          = 1;


        foreach($post_details as $key ){

            $payment = get_post_meta($key->post_id,'foundingmembership_payment',true);
            $balance = get_post_meta($key->post_id,'foundingmembership_balance',true);
            $status = get_post_meta($key->post_id,'foundingmembership_status',true);
            $level = get_post_meta($key->post_id,'foundingmembership_level',true);
            $notes = get_post_meta($key->post_id,'foundingmembership_notes',true);


            if($payment < 1000){
                $bg = "#eafdff";
            }
            else
            {
                $bg = "";
            }
            $postsid = $key->post_id;

            //get_related_foundingpayment($userid,$postsid);

            $result .= '<tr style="background:'.$bg.'">';
            $result .= '<td>'.$key->post_id.'</td>';
            $result .= '<td>'.$level.'</td>';
            $result .= '<td>'.wc_price($payment).'</td>';
            $result .= '<td>'.wc_price($balance).'</td>';
            $result .= '<td>'.$status.'</td>';
            $result .= '<td>'.$notes.'</td>';
            $result .= '<input type="hidden" name="founding_val" id="founding_balance" value="'.$key->post_id.'">';


            $user_info = new WP_User($userid);
            //raprap
            //$role = $user_info->roles[0];
            // if( !isset($_SESSION['backid']) ){
            $url       = esc_url( home_url() )."/product/founding-membership/";
            // }
            // else{
            //  $url = esc_url( home_url() )." / pay - founding - balance / ";
            // }
            $url       = add_query_arg(array('postid'  => $key->post_id,'bal'     => $balance,'founding'=> 'old'),$url);

            if($payment < 1000 && $this->gettotalfounding($postsid) < 1000 ){
                $result .= '<td><a href="'.$url.'" class="btn btn-primary"  id="pay_btn" target="_blank">Pay Balance</a> &nbsp; <a href="#payments'.$ctr.'" class="btn btn-info" data-toggle="collapse"><span class="glyphicon glyphicon-search"></span></a></td>';
            }
            else
            {
                $result .= '<td><button class="btn btn-primary" disabled>Pay Balance</button> &nbsp; <a href="#payments'.$ctr.'" class="btn btn-info" data-toggle="collapse"><span class="glyphicon glyphicon-search"></span></a></td>';
            }
            $result .= '</tr>';
            $result .= '<tr id="payments'.$ctr.'" class="collapse"><td colspan="7"> '.$this->get_related_founding_payment($postsid).'</td></tr>';
            $ctr++;
        }

        $result .= '</tbody></table>';
        //var_dump($postsid);
        if(count_records() == 1){
            //$result .= ' < h3 > You do not have founding membership</h3 > ';
        }

        //////

        return $result;
    }



    function gettotalfounding($postid)
    {
        global $wpdb;

        $gettotal = $wpdb->get_results("select wp1.post_id,wp1.meta_key, wp1.meta_value, wp2.post_status as Status from $this->wp_trans_postmeta wp1 LEFT JOIN $this->wp_trans_posts wp2 ON wp1.post_id = wp2.ID where wp1.post_id='".$postid."' AND
            (wp1.meta_key='foundingmembership_balance_order_id' OR wp1.meta_key='foundingmembership_order_id') AND (wp2.post_status != 'Trash' OR wp2.post_status != 'trash')");
        $sum      = 0;
        foreach($gettotal as $key){
            $order = new WC_Order( $key->meta_value );
            $items = $order->get_items();
            foreach($items as $item){

                $product_name = $item['name'];
                $product_id   = $item['product_id'];
                // $product_variation_id = $item['variation_id'];
                if($product_id == 626){
                    $product_total    = $item['line_total'];
                    $product_quantity = $item['qty'];

                    $price            = $product_total / $product_quantity;
                    // $price = wc_price($price);
                }
            }
            $sum += $price;
        }
        return $sum;
    }


    function gettotal_completedfounding($postid)
    {
        global $wpdb;

        $gettotal = $wpdb->get_results("select wp1.meta_value as order_id from $this->wp_trans_postmeta wp1 LEFT JOIN $this->wp_trans_posts wp2 ON wp1.post_id = wp2.ID where wp1.post_id='".$postid."' AND
            (wp1.meta_key='foundingmembership_balance_order_id' OR wp1.meta_key='foundingmembership_order_id') AND (wp2.post_status != 'Trash' OR wp2.post_status != 'trash') ");


        $sum      = 0;
        foreach($gettotal as $key){
            $get_total_completed = $wpdb->get_row("select ID, post_status from $this->wp_posts WHERE ID = '".$key->order_id."' AND (post_status in ('wc-completed','wc-awaiting','wc-received','wc-packing')) ");
            $order               = new WC_Order( $get_total_completed->ID );
            $items               = $order->get_items();
            foreach($items as $item){

                $product_name = $item['name'];
                $product_id   = $item['product_id'];
                // $product_variation_id = $item['variation_id'];
                if($product_id == 626){
                    $product_total    = $item['line_total'];
                    $product_quantity = $item['qty'];

                    $price            = $product_total / $product_quantity;
                    // $price = wc_price($price);
                }

            }
            if($get_total_completed != ''){
                $sum += $price;
            }




        }
        return $sum;
    }

    function foundingPayment($order_id)
    {
        $order = new WC_Order( $order_id );
        $items = $order->get_items();
        foreach($items as $item){

            $product_name = $item['name'];
            $product_id   = $item['product_id'];
            // $product_variation_id = $item['variation_id'];
            if($product_id == 626){
                $product_total    = $item['line_total'];
                $product_quantity = $item['qty'];

                $price            = $product_total / $product_quantity;
                $price            = wc_price($price);
                return $price;
            }

        }

    }

    function RunOrderCron()
    {

        global $wpdb;
        $SQL     = "
        Select distinct * from (
        Select wp.post_status as orderstatus, wpm1.meta_value as order_id,wpm2.meta_value as commstatus from $this->wp_trans_posts wp1
        inner join $this->wp_trans_postmeta wpm1 on (wp1.ID = wpm1.post_id and wpm1.meta_key = 'commission_order_id'  and wp1.post_status='publish')
        inner join $this->wp_trans_postmeta wpm2 on (wp1.ID = wpm2.post_id and wpm2.meta_key = 'commission_status'  and wp1.post_status='publish')
        inner join $this->wp_trans_posts wp on (wp.ID =  wpm1.meta_value)
        ) ass where ass.commstatus = 'On-hold'
        ";
        $results = $wpdb->get_results($SQL);
        if($wpdb->num_rows > 0)
        {
            $data = "";
            foreach($results as $item)
            {
                $order_id    = $item->order_id;
                $orderstatus = str_replace("wc-","",$item->orderstatus);
                if($orderstatus == 'awaiting' || $orderstatus == 'received' || $orderstatus == 'packing' || $orderstatus == 'completed'  )
                change_status('completed',$order_id);
                else
                change_status($orderstatus,$order_id);
            }
        }
        echo "RunOrderCron Done!";
        exit();

    }

    function orderDate($order_id)
    {
        global $wpdb;

        $order      = new WC_Order($order_id);
        $order_date = $order->get_date_created();
        return $order_date;
    }

    function get_related_founding_payment($postid)
    {
        global $wpdb;

        $payments = '<div class="panel panel-default"><table class="table">';

        $query    = $wpdb->get_results("
            select distinct meta_value as order_id from $this->wp_postmeta
            where meta_key='foundingmembership_order_id' and post_id = '$postid'
            ");
        //      select * from $this->wp_trans_postmeta where meta_key = 'foundingmembership_order_id' AND post_id = '".$postid."'");
        //$query1 = $wpdb->get_row("select * from $this->wp_trans_postmeta where meta_key = 'foundingmembership_order_id' AND post_id = '".$postid."'");


        $payments .= '<thead><tr>';
        $payments .= '<th>Order #</th><th>Payment</th><th>Status</th><th>Date</th>';
        $payments .= '</tr></thead>';

        $payments .= '<tbody>';
        //$payments .= ' < tr><td><a href = "'.esc_url( home_url() ).' / transactions / view - order / '.$query1->meta_value.' / " > #'.$query1->meta_value.'</a></td><td > '.foundingPayment($query1->meta_value).'</td><td > '.founding_statuses($query1->meta_value).'</td><td > '.orderDate($query1->meta_value).'</td><tr > ';
        foreach($query as $key ){
            $order        = new WC_Order($key->order_id);
            $order_date   = $order->get_date_created();
            $order_status = $order->get_status();
            $total        = $order->get_total();

            $payments .= '<tr>';
            $payments .= '<td><a href="'.esc_url( home_url() ).'/transactions/view-order/'.$key->order_id.'/">#'.$key->order_id.'</a></td></td><td>'.$total.'</td><td>'.$order_status.'</td><td>'.$order_date.'</td>';
            $payments .= '</tr></tbody>';
        }
        $payments .= '<tr><td colspan="2" style="font-weight:bold;">Total Payment If Completed = '.wc_price($this->gettotalfounding($postid)).'</td><td colspan="2" style="font-weight:bold;">Total Balance If Completed = '.wc_price(1000 - $this->gettotalfounding($postid)).'</td><td><div>'.gettotal_completedfounding($postid).'</div></td></tr></table></div>';

        $displayOption = $wpdb->get_var("select wp1.post_id,wp1.meta_key, wp1.meta_value, wp2.post_status as Status from $this->wp_trans_postmeta wp1 LEFT JOIN $this->wp_trans_posts wp2 ON wp1.post_id = wp2.ID where wp1.post_id='".$postid."' AND
            (wp1.meta_key='foundingmembership_balance_order_id' OR wp1.meta_key='foundingmembership_order_id') AND (wp2.post_status != 'Trash' OR wp2.post_status != 'trash')");

        $displayOption = $wpdb->num_rows;
        //var_dump($displayOption);
        //if($displayOption > 0){
        return $payments;
        //}

    }

    function display_Option($postid)
    {
        $displayOptions = $wpdb->get_var("select wp1.post_id,wp1.meta_key, wp1.meta_value, wp2.post_status as Status from $this->wp_trans_postmeta wp1 LEFT JOIN $this->wp_trans_posts wp2 ON wp1.post_id = wp2.ID where wp1.post_id='".$postid."' AND
            (wp1.meta_key='foundingmembership_balance_order_id' OR wp1.meta_key='foundingmembership_order_id') AND (wp2.post_status != 'Trash' OR wp2.post_status != 'trash')");
        $displayOptions = $wpdb->num_rows;
        return $displayOptions;
    }

    function pay_founding_balance($atts)
    {
        return get_template_part( 'pay_founding_balance', get_post_format() );
    }



    function FixFoundingShits($user_id = 0 )
    {
        global $wpdb;

        if(!$user_id)
        $user_id = $this->get_current_user_id();

        $rows    = $wpdb->get_results("Select post_id from $this->wp_postmeta where meta_key = '_customer_user' and  meta_value =$user_id");
        foreach($rows as $row)
        {
            $order_id = $row->post_id;
            $this->processAfterOrderCreated($order_id);
        }

        //fix founder level number
        $rows = $wpdb->get_results("Select
            $this->wp_posts.id as post_id,
            t1.meta_value as  foundingmembership_level,
            t2.meta_value as  foundingmembership_status
            from $this->wp_posts
            INNER JOIN $this->wp_postmeta on $this->wp_posts.id = $this->wp_postmeta.post_id
            LEFT JOIN $this->wp_postmeta t1 on ($this->wp_posts.id = t1.post_id and t1.meta_key =  'foundingmembership_level')
            LEFT JOIN $this->wp_postmeta t2 on ($this->wp_posts.id = t2.post_id and t2.meta_key =  'foundingmembership_status')
            where $this->wp_postmeta.meta_key =  'foundingmembership_userid' and $this->wp_postmeta.meta_value = '$user_id'
            order by post_id");
        $ctr  = 0;
        foreach($rows as $row)
        {
            $ctr++;
            $post_id = $row->post_id;
            update_post_meta($post_id, 'foundingmembership_level', $ctr );
        }
    }

function pay_commission($customer_user, $sponsoruser, $total_bonus_value, $order_id, $remark,$sponsor_rank='')
{
        //l("Generating commissions for Order #: ".$order_id." With Total bonus Value of: ".$total_bonus_value);
        if ($total_bonus_value ==0)
        return;

        $customer_user_info = new WP_User( $customer_user );
        $customer_name = $customer_user_info->first_name . " " . $customer_user_info->last_name;

        $user_info = new WP_User($sponsoruser);
        $user_current_commission = $this->get_current_comms();
        $user_current_coins = $this->get_current_coins_2();
        $coded_bonus = ( 0.10 * $total_bonus_value );

        // for coins
        $user_coin = $coded_bonus * 0.20;
        $bonus_coins = $user_current_coins + $user_coin;
        update_user_meta( $sponsoruser, 'commission_coins', $bonus_coins );

        $this->create_coins_post( $customer_name, $customer_user, $sponsoruser, $total_bonus_value, $user_coin, $order_id, $remark,$sponsor_rank );

        // for commissions
        $user_commission = $coded_bonus * 0.80;
        $bonus_commission = $user_current_commission + $user_commission;
        update_user_meta($customer_user, 'commission_pay', $bonus_commission);

        $this->create_commission_post( $customer_name, $customer_user, $sponsoruser, $total_bonus_value, $user_commission, $order_id, $remark,$sponsor_rank);

        //update user info
        //ld($customer_user." ".$sponsoruser);
        $this->refresh_user($customer_user);
        $this->refresh_user($sponsoruser);

        //for creating new founding record
        if ($remark == "Founding Purchase"){
           $this->processAfterOrderCreated($order_id);
        }
}

    function get_commission_status_id($orderstatus,$order_id = 0)
    {
        if($orderstatus == 'awaiting' || $orderstatus == 'received' || $orderstatus == 'packing' || $orderstatus == 'completed'  || $orderstatus == 'processing' )
        {
            // if ($order_id == 0)
            //  $status = 'Completed';
            // else
            // {
            //     $saveDate = get_post_meta($order_id, 'completed_date', true);
            //     if ($saveDate == null)
            //     {
            //      $status = "On - hold";
            //  }
            //  else
            //  {
            //      $currentDate = time();
            //      $nextWednesday = strtotime('next wednesday', $saveDate);
            //      $nextWednesday = date('d - m - Y', $nextWednesday);
            //      if (strtotime($nextWednesday) < $currentDate) {
            //          $status = "On - hold";
            //          #$date occurs in the future
            //      } else {
            //          $status = "Completed";
            //      }
            //  }
            // }
            $status = "2";
        }
        elseif($orderstatus == 'cancelled' || $orderstatus == 'refunded' )
        $status = '4';
        elseif($orderstatus == 'pending' || $orderstatus == 'failed' )
        $status = '2';
        elseif($orderstatus == 'on-hold' )
        $status = '5';
        else
        $status = $orderstatus;
        return $status;
    }



function get_transaction_type($value,$type='comm')
{
    $ret = '';
    if ( $value == 'Founder Purchase') ($type=='comm') ?   $ret = 106 : $ret = 206  ;
    if ( $value ==  'PSA Purchase')  ($type=='comm') ?   $ret = 107 : $ret = 207  ;
    if ( $value ==  'Sponsor Bonus')  ($type=='comm') ?   $ret = 101 : $ret = 201  ;
    if ( $value ==  'Diamond Bonus')  ($type=='comm') ?   $ret = 102 : $ret = 202  ;
    if ( $value ==  'Double Diamond Bonus') ($type=='comm') ?   $ret = 103 : $ret = 203  ;
    if ( $value ==  'Triple Diamond Bonus')  ($type=='comm') ?   $ret = 104 : $ret = 204  ;
    if ( $value ==  'Ambassador Bonus') ($type=='comm') ?   $ret = 105 : $ret = 205  ;
    if ( $value ==  'Manual Commission') ($type=='comm') ?   $ret = 109 : $ret = 209  ;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 1)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 2)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 3)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 4)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 5)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 6)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 7)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 8)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 9)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 10)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 11)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 12)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 13)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 14)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 15)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 16)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 17)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 18)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 19)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 20)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 21)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 22)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 23)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 24)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 25)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 26)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 27)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 28)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 29)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 57)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 31)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 32)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 33)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 34)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 35)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 36)')  $ret = 108;
    if ( $value ==  'Monthly Bonus (Vested Founding Members 37)')  $ret = 108;
    return $ret;
}



    function create_commission_post( $name, $id, $sponsor, $total, $value, $order_id, $remark, $status = '', $sponsor_rank = '' )
    {

        if($value == 0)
        return;

        global $wpdb;
        $post_title   = "Commission for ".$remark." - ".$name;
        $payer_name   = $name;
        $payer_id     = $id;
        $price_total  = $total;
        $value        = $value;
        $sponsor_id   = $sponsor;
        $sponsor_info = new WP_User($sponsor_id); //Get member info
        $sponsor_name = $sponsor_info->first_name . ' ' . $sponsor_info->last_name;

        $order        = new WC_Order($order_id);
        $postdate     = $order->get_date_created();

        if($status == '')
        {
        $status   = $this->get_commission_status($order->get_status(), $order_id);

        }
        $new_commission_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $payer_id,
            'post_type'  => 'commissions',

        );


        //delete_existing_user_commissions($order_id,$sponsor_id);

        if($order_id == '0' && $remark == 'Coin Transfer'){
            $order_id = $commission_post_id;
        }
        if($value != 0)
        $value = round($value,2);

        if($price_total != 0)
        $price_total = round($price_total,2);

        if($sponsor_rank == '')
        $sponsor_rank = $this->get_rank($sponsor_id);


          $wpdb->insert(
             'sfm_uap_transactions',
                array(
                    'type_id' => $this->get_transaction_type($remark, 'comm'),
                    'status_id' => $this->get_commission_status_id($order->get_status(), $order_id),
                    'date_created' => $postdate,
                    'user_id' =>$sponsor_id,
                    'order_id' => $order_id,
                    'from_user_id' => $payer_id,
                    'amount' => $value,
                    'order_total'=> $total,
                    'notes' => $remark
                 )
         );


        return $commission_post_id;
    }


    function create_coins_post( $name, $id, $sponsor, $total, $value, $order_id, $remark, $status = '',$sponsor_rank = '')
    {
        if($value == 0)
        return;

        global $wpdb;
        $post_title   = "Bonus Freedom Coins for ".$remark." - ".$name;
        $payer_name   = $name;
        $payer_id     = $id;
        $price_total  = $total;
        $value        = $value;
        $sponsor_id   = $sponsor;
        $sponsor_info = new WP_User($sponsor_id); //Get member info
        $sponsor_name = $sponsor_info->first_name . ' ' . $sponsor_info->last_name;

        $order        = new WC_Order($order_id);
        $order_date   = $order->get_date_created();
        if($status == '')
        $status         = $this->get_commission_status($order->get_status(),$order_id);

        $new_coins_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  =>  $order_date ,
            'post_author'=> $payer_id,
            'post_type'  => 'coins',

        );


        if($sponsor_rank == '')
        $sponsor_rank = $this->get_rank($sponsor_id);


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type($remark,'coin'),
                   'status_id' => $this->get_commission_status_id($order->get_status(),$order_id) ,
                   'date_created' => $order_date,
                   'user_id' =>$sponsor_id,
                   'order_id' => $order_id,
                   'from_user_id' => $payer_id,
                   'amount' => $value,
                   'order_total'=> $total,
                   'notes' => $remark
                 )
         );

        //$this->change_posts_to_default();
        //Sync_Member_Data($sponsor_id);
    }




    function create_commissionpayment_post( $id, $value, $order_id )
    {
        global $wpdb;
        global $current_user;

        $user_info                  = new WP_User($id); //Get member info
        $name                       = $user_info->first_name . ' ' . $user_info->last_name;


        $post_title                 = "Commission Payment for Order No.".$order_id;
        $postdate                   = gmdate('Y-m-d H:i:s');

        $new_commissionpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'commissions_payment'
        );

        //delete_existing_user_commissions($order_id,$sponsor_id);
        refresh_user($id);
        $commissions = $wpdb->get_var("SELECT commissions FROM sfm_uap_affiliates WHERE user_id = $id");
        if($commissions < $value*-1){
        	return;
        }

        if($value != 0)
        $value = round($value,2);

        $order  = new WC_Order($order_id);

        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
             	   'date_created' => $order->order_date,
                   'type_id' => 301,
                   'status_id' => $this->get_commission_status_id($order->get_status(), $order_id),
                   'user_id' =>$id,
                   'order_id' => $order_id,
                   'from_user_id' => $id,
                   'amount' => $value,
                   'order_total'=> $total,
                   'notes' => $remark
                 )
         );

        refresh_user($id);
        //Sync_Member_Data($id);
    }

    function create_commission_post_manual2($userid, $value, $remark = 'Manual Commission', $overridedate = '2016-03-01 00:00:00')
    {
        $user_info = new WP_User($userid); //Get member info
        $user_name = $user_info->display_name; // $sponsor_info->first_name . ' ' . $sponsor_info->last_name;
        $post_title= "Manual Commission for ".$user_name;
        if($overridedate == '')
        $overridedate        = date('Y-m-d H:i:s');

        $new_commission_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $overridedate,
            'post_author'=> $userid,
            'post_type'  => 'commissions',

        );

        $status             = $this->get_commission_status("completed");

        if($remark == 'Sponsor Bonus')
        $custom = 'commission-referral';
        elseif($remark == 'Diamond Bonus')
        $custom = 'commission-diamond';
        elseif($remark == 'Double Diamond Bonus')
        $custom = 'commission-doublediamond';
        elseif($remark == 'Triple Diamond Bonus')
        $custom = 'commission-triplediamond';
        elseif($remark == 'Ambassador Bonus')
        $custom = 'commission-ambassador';


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type($remark),
                   'status_id' => 2,
                   'date_created' => $overridedate,
                   'user_id' =>$user_info->ID,
                   'amount' => $value,
                   'notes' => $remark
                 )
         );


    }



    function create_coins_post_manual2($userid, $value, $remark = 'Manual Coin', $overridedate = '2016-03-01 00:00:00')
    {
        if($overridedate == '')
        $overridedate   = date('Y-m-d H:i:s');
        $user_info      = new WP_User($userid); //Get member info
        $user_name      = $user_info->display_name; // $sponsor_info->first_name . ' ' . $sponsor_info->last_name;
        $post_title     = "Manual Coin for ".$user_name;
        $new_coins_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $overridedate ,
            'post_author'=> $userid,
            'post_type'  => 'coins',

        );

        $status        = $this->get_commission_status("completed");




        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type($remark,'coin'),
                   'status_id' => 2,
                   'date_created' => $overridedate,
                   'user_id' =>$user_info->ID,
                   'amount' => $value,
                   'notes' => $remark
                 )
         );


    }


    function create_commissionpayment_post_manual( $id, $value )
    {
        global $wpdb;
        $user_info                  = new WP_User($id); //Get member info
        $name                       = $user_info->first_name . ' ' . $user_info->last_name;
        $order_id                   = '400000';
        $post_title                 = "Commission Payment for New FoundingMembership";
        $postdate                   = '2016-12-01 00:00:00'; // date('Y - m - d H:i:s');

        $new_commissionpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'commissions_payment'
        );

        //delete_existing_user_commissions($order_id,$sponsor_id);

        if($value != 0)
        $value = round($value,2);



        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type("Commission Payment"),
                   'status_id' => 2,
                   'date_created' => $postdate,
                   'order_id'=>$order_id,
                   'user_id' =>$id,
                   'amount' => $value,
                   'notes' =>"Commission Payment"
                 )
         );

    }

    function create_commissionpayment_post_manual_prev( $id, $value )
    {
        global $wpdb;
        $user_info                  = new WP_User($id); //Get member info
        $name                       = $user_info->first_name . ' ' . $user_info->last_name;
        $order_id                   = '28423';
        $post_title                 = "Commission Payment for New FoundingMembership";
        $postdate                   = '2016-03-01 00:00:00'; // date('Y - m - d H:i:s');

        $new_commissionpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'commissions_payment'
        );


        //$commissionpayment_post_id = wp_insert_post($new_commissionpayment_post);

        if($value != 0)
        $value = round($value,2);

        add_post_meta($commissionpayment_post_id, 'commissionpayment_payer_name', $name);
        add_post_meta($commissionpayment_post_id, 'commissionpayment_payer_id', $id);
        add_post_meta($commissionpayment_post_id, 'commissionpayment_value', $value);
        add_post_meta($commissionpayment_post_id, 'commissionpayment_order_id', $order_id);
        add_post_meta($commissionpayment_post_id, 'commissionpayment_remark', "Commission Payment");
        add_post_meta($commissionpayment_post_id, 'commissionpayment_date', $postdate);


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => 301,
                   'status_id' => 2,
                   'date_created' => $postdate,
                   'order_id'=>$order_id,
                   'user_id' =>$id,
                   'amount' => $value,
                   'notes' =>"Commission Payment"
                 )
         );

    }


    function create_coinpayment_post_manual( $id, $value )
    {
        global $wpdb;
        $user_info            = new WP_User($id); //Get member info
        $name                 = $user_info->first_name . ' ' . $user_info->last_name;
        $order_id             = '400000';
        $post_title           = "Coin Payment for New FoundingMembership";
        $postdate             = '2016-12-01 00:00:00'; // date('Y - m - d H:i:s');

        $new_coinpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'coins_payment'
        );

        //delete_existing_user_coins($order_id,$sponsor_id);

        if($value != 0)
        $value = round($value,2);

        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => 302,
                   'status_id' => 2,
                   'date_created' => $postdate,
                   'order_id'=>$order_id,
                   'user_id' =>$id,
                   'amount' => $value,
                   'notes' =>"Coin Payment"
                 )
         );

    }


    function create_coinpayment_post_manual_prev( $id, $value )
    {
        global $wpdb;
        $user_info            = new WP_User($id); //Get member info
        $name                 = $user_info->first_name . ' ' . $user_info->last_name;
        $order_id             = '28423';
        $post_title           = "Coin Payment for New FoundingMembership";
        $postdate             = '2016-03-01 00:00:00'; // date('Y - m - d H:i:s');

        $new_coinpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'coins_payment'
        );

        //delete_existing_user_coins($order_id,$sponsor_id);
        $this->change_posts_to_tranpost();

        if($value != 0)
        $value = round($value,2);


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => 302,
                   'status_id' => 2,
                   'date_created' => $postdate,
                   'order_id'=>$order_id,
                   'user_id' =>$id,
                   'amount' => $value,
                   'notes' =>"Coin Payment"
                 )
         );

    }



    function create_commission_post_manual($userid, $value, $remark = 'Manual Commission')
    {
        $user_info           = new WP_User($userid); //Get member info
        $user_name           = $user_info->display_name; // $sponsor_info->first_name . ' ' . $sponsor_info->last_name;
        $post_title          = "Manual Commission for ".$user_name;

        $new_commission_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => date('Y-m-d H:i:s'),
            'post_author'=> $userid,
            'post_type'  => 'commissions',

        );
        $date =  date('Y-m-d H:i:s');
        $this->change_posts_to_tranpost();

        $status             = $this->get_commission_status("completed");


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type($remark),
                   'status_id' => 2,
                   'date_created' =>$date,
                   'user_id' =>$user_info->ID,
                   'amount' => $value,
                   'notes' => $remark
                 )
         );


        //  Sync_Member_Data($userid);
    }

    function create_commission_post_vested_founder($userid, $value, $founderpostid, $founderlevel )
    {
        $user_info           = new WP_User($userid); //Get member info
        $user_name           = $user_info->display_name; // $sponsor_info->first_name . ' ' . $sponsor_info->last_name;
        $post_title          = "Monthly Bonus (Vested Founding Members ".$founderlevel.")";
        $remark              = $post_title;
        $new_commission_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => date('Y-m-d H:i:s'),
            'post_author'=> $userid,
            'post_type'  => 'commissions',

        );
     $date =  date('Y-m-d H:i:s');
        $this->change_posts_to_tranpost();
       // $commission_post_id = wp_insert_post($new_commission_post);
        $status             = $this->get_commission_status("completed");


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' =>108,
                   'status_id' => 2,
                   'date_created' => $date,
                   'user_id' =>$userid,
                   'amount' => $value,
                   'notes' => $remark
                 )
         );
        $this->change_posts_to_default();
        //Sync_Member_Data($userid);
        //die('done');
    }


    function create_coins_post_manual($userid, $value, $remark = 'Manual Coin')
    {
        $user_info      = new WP_User($userid); //Get member info
        $user_name      = $user_info->display_name; // $sponsor_info->first_name . ' ' . $sponsor_info->last_name;
        $post_title     = "Manual Coin for ".$user_name;
        $new_coins_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => date('Y-m-d H:i:s'),
            'post_author'=> $userid,
            'post_type'  => 'coins',

        );
     $date =  date('Y-m-d H:i:s');
        $this->change_posts_to_tranpost();

        $status        = $this->get_commission_status("completed");


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type($remark,'coin'),
                   'status_id' => 2,
                   'date_created' => $date,
                   'user_id' =>$user_info->ID,
                   'amount' => $value,
                   'notes' => $remark
                 )
         );

        $this->change_posts_to_default();
        //Sync_Member_Data($userid);
    }



    function add_coins_manually( $entry, $form )
    {
        if(isset($_GET['uid']) && !empty($_GET['uid'])){
            $uid   = sanitize_text_field($_GET['uid']);
            $value = $entry[2];
            $this->create_coins_post_manual($uid, $value);
        }
    }


    function add_commission_manually( $entry, $form )
    {
        if(isset($_GET['uid']) && !empty($_GET['uid'])){
            $uid   = sanitize_text_field($_GET['uid']);
            $value = $entry[2];
            $this->create_commission_post_manual($uid, $value);
        }
    }

    function create_davidcrookpayment_post( $userid, $amount, $remark )
    {
        global $wpdb;
        $user_info                  = new WP_User($userid); //Get member info
        $name                       = $user_info->first_name . ' ' . $user_info->last_name;

        $post_title                 = "David Crook Payment for ".$userid;
        $postdate                   = date('Y-m-d H:i:s');

        $new_davidcrookpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'davidcrook_payment'
        );

        //delete_existing_user_commissions($order_id,$sponsor_id);
        $this->change_posts_to_tranpost();
        $davidcrookpayment_post_id = wp_insert_post($new_davidcrookpayment_post);

        if($value != 0)
        $value = wc_price($amount,2);

        add_post_meta($davidcrookpayment_post_id, 'davidcrookpayment_userid', $userid);
        add_post_meta($davidcrookpayment_post_id, 'davidcrookpayment_name', $name);
        add_post_meta($davidcrookpayment_post_id, 'davidcrookpayment_amount', $amount);
        add_post_meta($davidcrookpayment_post_id, 'davidcrookpayment_remark', $remark);
        add_post_meta($davidcrookpayment_post_id, 'davidcrookpayment_status', "Completed");
        add_post_meta($davidcrookpayment_post_id, 'davidcrookpayment_date', $postdate);
        $this->change_posts_to_default();
        //Sync_Member_Data($userid);
        return true;
    }

    function create_withdrawal_post( $userid, $amount, $remark, $remark2 )
    {
        global $wpdb;
        $user_info           = new WP_User($userid); //Get member info
        $name                = $user_info->first_name . ' ' . $user_info->last_name;

        $post_title          = "Withdrawal Request for ".$userid;
        $postdate            = date('Y-m-d H:i:s');

        $new_withdrawal_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'withdrawals'
        );

        //delete_existing_user_commissions($order_id,$sponsor_id);

        $withdrawal_post_id = wp_insert_post($new_withdrawal_post);

        if($value != 0)
        $value = wc_price($amount,2);

        add_post_meta($withdrawal_post_id, 'withdrawal_userid', $userid);
        add_post_meta($withdrawal_post_id, 'withdrawal_name', $name);
        add_post_meta($withdrawal_post_id, 'withdrawal_amount', $amount);
        add_post_meta($withdrawal_post_id, 'withdrawal_remark', $remark);
        add_post_meta($withdrawal_post_id, 'withdrawal_email', $remark2);
        add_post_meta($withdrawal_post_id, 'withdrawal_status', "Pending");
        add_post_meta($withdrawal_post_id, 'withdrawal_date', $postdate);
        //Sync_Member_Data($userid);


        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'type_id' => $this->get_transaction_type($remark),
                   'status_id' => 1,
                   'date_created' => $postdate,
                   'user_id' =>$userid,
                   'amount' => $amount,
                   'notes' => $remark2
                 )
         );

        return true;
    }




    function transfer_coins_to_commission( $id, $value,$total )
    {
        global $wpdb;
        $userp = get_user_meta($id,'pending_payment_done',true);
        echo $userp.'hello';
        if($userp == '1'){
            echo "created already";
            return;
        }
        $customer_user_info = new WP_User( $id );
        $customer_name      = $customer_user_info->first_name . " " . $customer_user_info->last_name;

        $user_info          = new WP_User($id);



        $order_id           = $this->create_commission_post( $customer_name, $id, $id, $value, $value, '0', 'Coin Transfer' );
        $this->create_coinpayment_post( $id, $value, $order_id );
        $this->create_withdrawal_post( $id, $total, 'Pending payment to be paid by David Crookston' );
        update_user_meta($id,'pending_payment_done','1');
        echo " done";
    }

    function create_coinpayment_post( $id, $value, $order_id )
    {
        global $wpdb;
        $user_info            = new WP_User($id); //Get member info
        $name                 = $user_info->first_name . ' ' . $user_info->last_name;

        $post_title           = "Coin Payment for Order No.".$order_id;
        $postdate             = date('Y-m-d H:i:s');

        $new_coinpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'coins_payment'
        );

        refresh_user($id);
	 	$coins = $wpdb->get_var("SELECT coins FROM sfm_uap_affiliates WHERE user_id = $id");

        if($coins < $value*-1){
        	return;
        }

        $coinpayment_post_id = wp_insert_post($new_coinpayment_post);

        if($value != 0)
        $value = round($value,2);

        add_post_meta($coinpayment_post_id, 'coinpayment_payer_name', $name);
        add_post_meta($coinpayment_post_id, 'coinpayment_payer_id', $id);
        add_post_meta($coinpayment_post_id, 'coinpayment_value', $value);
        add_post_meta($coinpayment_post_id, 'coinpayment_order_id', $order_id);
        add_post_meta($coinpayment_post_id, 'coinpayment_remark', "Coin Payment");
        add_post_meta($coinpayment_post_id, 'coinpayment_date', $postdate);


        $order  = new WC_Order($order_id);
        $wpdb->insert(
             'sfm_uap_transactions',
                 array(
                   'date_created' => $order->order_date,
                   'type_id' => 302,
                   'status_id' => $this->get_commission_status_id($order->get_status(), $order_id),
                   'user_id' =>$id,
                   'order_id' => $order_id,
                   'from_user_id' => $id,
                   'amount' => $value,
                   'order_total'=> $total,
                   'notes' => $remark
                 )
         );

        refresh_user($id);
        //l($value);
        //$this->change_posts_to_default();
        //Sync_Member_Data($id);
    }

    function process_founding( $id, $value, $order_id )
    {
        global $wpdb;

        $user_info      = new WP_User($id); //Get member info
        $name           = $user_info->first_name . ' ' . $user_info->last_name;
        $sponsor_id     = $user_info->referral_id;
        $post_title     = "Founding Payment for Order No.".$order_id;
        $postdate       = date('Y-m-d H:i:s');

        $new_foundingpayment_post = array(
            'post_title' => $post_title,
            'post_status'=> 'publish',
            'post_date'  => $postdate,
            'post_author'=> $id,
            'post_type'  => 'founding_membership'
        );

        $foundingpayment_post_id = wp_insert_post($new_foundingpayment_post);

        if($value != 0)
        $value = round($value,2);

        add_post_meta($foundingpayment_post_id, 'foundingpayment_payer_name', $name);
        add_post_meta($foundingpayment_post_id, 'foundingpayment_payer_id', $id);
        add_post_meta($foundingpayment_post_id, 'foundingpayment_value', $value);
        add_post_meta($foundingpayment_post_id, 'foundingpayment_order_id', $order_id);
        add_post_meta($foundingpayment_post_id, 'foundingpayment_remark', "Founding Payment");
        add_post_meta($foundingpayment_post_id, 'foundingpayment_date', $postdate);

        $total = $value;
        $sponsor_bonus = ($total*0.10);
        $sponsor_cash_amount = ($sponsor_bonus*0.80);
        $sponsor_coin_amount = ($sponsor_bonus*0.20);

        $order  = new WC_Order($order_id);

        //Sponsor Cash Bonus
        $wpdb->insert(
            'sfm_uap_transactions',
            array(
                'date_created' => $order->order_date,
                'user_id' =>$sponsor_id,
                'type_id' => 106,
                'status_id' => $this->get_commission_status_id($order->get_status(), $order_id),
                'amount' => $sponsor_cash_amount,
                'order_id' => $order_id,
                'from_user_id' => $id,
                'order_total'=> $total,
            )
        );

        //Sponsor Coin Bonus
        $wpdb->insert(
            'sfm_uap_transactions',
            array(
                'date_created' => $order->order_date,
                'user_id' =>$sponsor_id,
                'type_id' => 206,
                'status_id' => $this->get_commission_status_id($order->get_status(), $order_id),
                'amount' => $sponsor_coin_amount,
                'order_id' => $order_id,
                'from_user_id' => $id,
                'order_total'=> $total,
            )
        );

        //refresh_user($sponsor_id);
    }

}




if ( !function_exists('l'))
{
function l($message) {

    echo "<pre>Writelog: ";

    var_dump($message);

    echo "</pre>";

    //$log = new Affiliate_WP_Logging;

    //$log->log('Log from Miscfunctions: '.$message );

}
}


if ( !function_exists('ld'))
{
function ld($message) {
    echo "<pre>Writelogdie: ";
    var_dump($message);
    echo "</pre>";
    // $log = new Affiliate_WP_Logging;
    // $log->log('Log from Miscfunctions: '.$message );
    die;

}
}