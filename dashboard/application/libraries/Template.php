<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template {
	
	public $site_theme = "default";
	public $data = array();

	public function __construct()
	{
		
        $this->data['content_view'] = 'content_view';
        $this->data['topbar'] = 'topbar';
        $this->data['header'] = 'header';
        $this->data['sidemenu'] = 'sidemenu';
        $this->data['footer'] = 'footer';
        $this->data['left_menu'] = "left_menu";
	}



	
    public function render($view, $data = array(), $return = '') {
        $ci = get_instance();
        $view_data = array_replace_recursive($this->data, $data);
        
        $ci->load->view($view,$view_data);
		if (!empty($return))	
		{
			ob_start();
			$output = $ci->load->view($view, $view_data);
			$buffer = ob_get_clean();
			return ($output !== NULL) ? $output : $buffer;
		}
		else  
		{
 			$ci->load->view($view, $view_data);
		}
    }

	public function get_partials()
	{
		     $ci = get_instance();
		$themepath =  $ci->config->theme_path. $ci->config->site_theme.'/';
		if (file_exists(VIEWPATH.$themepath.'pagehead.php'))
		{
			$this->data['pagehead'] = $ci->load->view($themepath.'pagehead.php',$this->data, true );
		}
		if (file_exists(VIEWPATH.$themepath.$this->data['header'].'.php'))
		{
			$this->data['header'] = $ci->load->view($themepath.$this->data['header'],$this->data, true );
		}
		if (file_exists(VIEWPATH.$themepath.$this->data['footer'].'.php'))
		{
			$this->data['footer'] = $ci->load->view($themepath.$this->data['footer'],$this->data, true );
		}
		if (file_exists(VIEWPATH.$themepath.$this->data['sidemenu'].'.php'))
		{
			$this->data['sidemenu'] = $ci->load->view($themepath.$this->data['sidemenu'],$this->data, true );
		}

	}
	
	
	public function render_view($viewpath,$viewdata = 0)
	{
        $ci = get_instance();
		if (!empty($viewdata))
		{
			$this->data = array_replace_recursive($this->data,$viewdata);			
		}
		$this->fetch_addition_data();
		$this->get_partials();


		if (isset($this->data['breadcrumbs']))
		{
			$pagetitle = ""; 
			if (isset($this->data['pageTitle'])) $pagetitle = $this->data['pageTitle']; 
			$this->data['breadcrumbs'] =  get_bread_crumbs($pagetitle);	
		}
			
		$this->site_theme = get_instance()->session->userdata('site_theme');

		if (isset($this->config['theme_path']))
			$this->theme_path=$this->config['theme_path'];
		else
			$this->theme_path='theme/';

		$themepath =  $ci->config->theme_path;

		if( $ci->config->item('override_route') != null)
		{
			if ( $ci->config->item('override_route') == MEMBERS_ROUTE)
			{
				$this->data['content'] = '
					<div id="page-content" class="p20 clearfix">'. $ci->load->view($themepath.$viewpath,$this->data, true ).'</div>';
				 $ci->load->view("member/layout/main", $this->data );		
				return;
			}
		}

		$this->data['content'] =  $ci->load->view($viewpath,$viewdata, true );	
	  // 	$ci->load->view($ci->config->theme_path. $ci->config->site_theme."/main", $this->data );

	   	$ci->load->view($themepath.$this->site_theme."/main", $viewdata );
	}
	
	public function render_content($viewpath)
	{
		$ci = get_instance();
		$this->load_viewdata();
    	$this->load->view($viewpath, $this->data );		
	}

	public function fetch_addition_data($value = '')
	{
		$this->data['title'] = '';
		$this->data['base'] = 'http';
		$this->data['description'] = '';
		$this->data['keywords'] = '';
		$this->data['links'] = '';
		$this->data['styles'] = '';
		$this->data['scripts'] = '';
		$this->data['lang'] = '';
		$this->data['direction'] = '';

		$this->data['text_order'] = lang('text_order');
		$this->data['text_processing_status'] = lang('text_processing_status');
		$this->data['text_complete_status'] = lang('text_complete_status');
		$this->data['text_return'] = lang('text_return');
		$this->data['text_customer'] = lang('text_customer');
		$this->data['text_online'] = lang('text_online');
		$this->data['text_approval'] = lang('text_approval');
		$this->data['text_product'] = lang('text_product');
		$this->data['text_stock'] = lang('text_stock');
		$this->data['text_review'] = lang('text_review');
		$this->data['text_affiliate'] = lang('text_affiliate');
		$this->data['text_store'] = lang('text_store');
		$this->data['text_front'] = lang('text_front');
		$this->data['text_help'] = lang('text_help');
		$this->data['text_homepage'] = lang('text_homepage');
		$this->data['text_documentation'] = lang('text_documentation');
		$this->data['text_support'] = lang('text_support');
		$this->data['text_logged'] = sprintf(lang('text_logged'), '');
		$this->data['text_logout'] = '';
		$this->data['logged'] = '';
		$this->data['home'] = '';
		$this->data['login_user'] = '';
	
		$this->data=array_replace_recursive( array(
			'sts_store_name' => CNF_APPNAME,
			'layout_design_site_theme' =>  '',
			'layout_design_header_name' =>  '',
			'layout_design_custom_css' => '',
			'css_body' => 'jroxMembersReports',
			'logo' => '',
			'top_menu' => '',
			'account_menu' => '1',
			'footer_menu' => '',
			'cart_items' => 'Cart',
			'enable_links' => '1',
			'enable_affiliate_marketing' => '1',
			'dashboard_name' => get_instance()->session->userdata('fid'),
			'sts_content_members_dashboard_enable_account_details' => '1',
			'sts_content_members_dashboard_enable_downloads' => '1',
			'sts_content_members_dashboard_enable_invoices' => '1',
			'sts_content_members_dashboard_enable_support' => '1',
			'sts_content_members_dashboard_enable_tools' => '1',
			'sts_content_members_dashboard_enable_reports' => '1',
			'sts_content_members_dashboard_enable_commissions' => '1',
			'sts_content_members_dashboard_enable_content' => '1',
			'sts_content_members_dashboard_enable_downline' => '1',
			'sts_content_members_dashboard_enable_memberships' => '1',
			'sts_content_members_dashboard_enable_coupons' => '1',
			'sts_content_members_dashboard_enable_traffic' => '1',
			'sts_affiliate_enable_profile_description' => '1',
			'sts_sec_enable_captcha_order_form' => '1',
			'captcha_field' => '',
			'captcha' => '',
			'lang_captcha_text' => '',
			'general_affiliate_link' =>  WP_URL.'/'.get_instance()->session->userdata('username'),
			'mailing_lists' => '1',
			'member_hide_list_box' => '1',
			'signup_affiliate_link' => WP_URL.'/'.get_instance()->session->userdata('username'),
			'show_articles' => '1',
			'articles' => array(),
			'dashboard_unpaid_invoices' => '$0.00',
			'dashboard_paid_invoices' => '$0.00',
			'dashboard_unpaid_commissions' => '$0.00',
			'dashboard_paid_commissions' => '$0.00',
			'dashboard_available_commissions' => '$0.00',
			'dashboard_unpaid_coins' => '$0.00',
			'dashboard_paid_coins' => '$0.00',
			'dashboard_available_coins' => '$0.00',
			'dashboard_unpaid_referrals' => '0',
			'dashboard_paid_referrals' => '0',
			'dashboard_total_downlines' => '0',
			
			'sts_support_enable' => '1',
			'dashboard_open_tickets' => '1',
			'dashboard_closed_tickets' => '1',
			'show_message' => '',
			'show_upload' => '0',
			'pagination_rows' => '',
			'pagehead' => '',
			'filterbar' => '',
			'customcontent' => '',
			'newbuttontext' => 'Create',
			'user_strict_location' => '0',
			'filter_location' => '',
			'filter_locations' => array(
									array(
										'location_id' => 1,
										'location_name' => 1,
									)),
			'filter_status' => '',
			'filter_statuses' => array(
									array(
										'status_id' => 1,
										'status_name' => 1,
									)),
			'filter_paymentmethod' => '',									
			'filter_paymentmethods' => array(
									array(
										'name' => 1,
										'title' => 1,
									)),
									
			'filter_type' => array(),

			'filter_order_dates' => array(),
			
			'filter_date' => '',			
			'filter_search' => '' ,			
			'filter_from' => '',
			'filter_to' => '',
			'show_response' => '',
			'support_priority' => '',
			'support_categories' => '',
			'breadcrumbs' => '',
			'' => '1',
			'ticket_id' => '1',

			's_ticket_status' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			'' => '1',
			
		), $this->data);


		
		
//		if(!is_null($this->input->get('search', true)))
//		{
//			$type = explode("|",$this->input->get('search', true));
//			if(count($type) >= 1)
//			{
//				foreach($type as $t)
//				{
//					$keys = explode(":",$t);
//					if ($keys[0]=='search')
//					{
//						$this->data['filter_search'] = $keys[1];
//					}
//				}
//			}
//
//		}

		
	}
		
}
