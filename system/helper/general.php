<?php
function token($length = 32) {
	// Create random token
	$string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	
	$max = strlen($string) - 1;
	
	$token = '';
	
	for ($i = 0; $i < $length; $i++) {
		$token .= $string[mt_rand(0, $max)];
	}	
	
	return $token;
}

/**
 * Backwards support for timing safe hash string comparisons
 * 
 * http://php.net/manual/en/function.hash-equals.php
 */

if(!function_exists('hash_equals')) {
	function hash_equals($known_string, $user_string) {
		$known_string = (string)$known_string;
		$user_string = (string)$user_string;

		if(strlen($known_string) != strlen($user_string)) {
			return false;
		} else {
			$res = $known_string ^ $user_string;
			$ret = 0;

			for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);

			return !$ret;
		}
	}
}

function l($strvalue) {
	echo "<pre>";
	var_dump($strvalue);
	echo "</pre>";
}

function ld($strvalue) {
	echo "<pre>";
	var_dump($strvalue);
	echo "</pre>";
	die;
}

function ci_logout($_this)
{
	$_COOKIE["ci_session"] =$_COOKIE["default"];	
	$sid = $_COOKIE["ci_session"];
	$db = $_this->db;
	$db->query("DELETE FROM ci_sessions WHERE id = '".$sid."'");
}


function ci_login($_this,$customer_id)
{
	// Autologin CI
	$_COOKIE["ci_session"] =$_COOKIE["default"];	
	$sid = $_COOKIE['ci_session'];
	$insert_data = '__ci_last_regenerate|i:'.time().';red_url|s:0:"";logged_in|b:1;uid|i:'.$customer_id.';client_user_id|i:'.$customer_id.';';
	$_this->db->query("DELETE FROM ci_sessions WHERE id = '".$sid."'");
	$_this->db->query("INSERT INTO ci_sessions (id,ip_address,timestamp,data) values ('".$sid."','".$_SERVER['REMOTE_ADDR']."','".time()."','".$insert_data."')");
	// End of code
}