<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/shopfreemart_oc/admin/');
define('HTTP_CATALOG', 'http://localhost/shopfreemart_oc/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/shopfreemart_oc/admin/');
define('HTTPS_CATALOG', 'http://localhost/shopfreemart_oc/');

// DIR
define('DIR_APPLICATION', 'C:/wamp64/www/shopfreemart_oc/admin/');
define('DIR_SYSTEM', 'C:/wamp64/www/shopfreemart_oc/system/');
define('DIR_IMAGE', 'C:/wamp64/www/shopfreemart_oc/image/');
define('DIR_LANGUAGE', 'C:/wamp64/www/shopfreemart_oc/admin/language/');
define('DIR_TEMPLATE', 'C:/wamp64/www/shopfreemart_oc/admin/view/template/');
define('DIR_CONFIG', 'C:/wamp64/www/shopfreemart_oc/system/config/');
define('DIR_CACHE', 'C:/wamp64/www/shopfreemart_oc/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/wamp64/www/shopfreemart_oc/system/storage/download/');
define('DIR_LOGS', 'C:/wamp64/www/shopfreemart_oc/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/wamp64/www/shopfreemart_oc/system/storage/modification/');
define('DIR_UPLOAD', 'C:/wamp64/www/shopfreemart_oc/system/storage/upload/');
define('DIR_CATALOG', 'C:/wamp64/www/shopfreemart_oc/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '192.168.21.219');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'main_shopfreemart_oc');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');